#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>


static void *loadWholeFile (const char *fname, long *osz) {
  FILE *fl = fopen(fname, "rb");
  long sz;
  void *res;
  if (!fl) return NULL;
  fseek(fl, 0, SEEK_END);
  sz = ftell(fl);
  fseek(fl, 0, SEEK_SET);
  res = malloc(sz+1);
  if (!res) { fclose(fl); return NULL; }
  *osz = sz;
  fread(res, sz, 1, fl);
  fclose(fl);
  return res;
}


int main (int argc, char *argv[]) {
  void *ibuf;
  unsigned char *obuf;
  long fsz;
  uLongf osz;
  FILE *fo;
  //
  if (sizeof(int) != 4) {
    fprintf(stderr, "DIEDIEDIE\n");
    return 1;
  }
  //
  if (argc != 3) {
    fprintf(stderr, "usage: %s infile outfile\n", argv[0]);
    return 1;
  }
  //
  if ((ibuf = loadWholeFile(argv[1], &fsz)) == NULL) {
    fprintf(stderr, "ERROR: can't open input file: %s\n", argv[1]);
    return 1;
  }
  //
  osz = compressBound(fsz)+32768;
  if ((obuf = malloc(osz+1)) == NULL) {
    fprintf(stderr, "ERROR: can't allocate memory\n");
    return 1;
  }
  //
  printf("compressing %d bytes...\n", (int)fsz);
  if (compress2(obuf, &osz, ibuf, fsz, Z_BEST_COMPRESSION) != Z_OK) {
    fprintf(stderr, "ERROR: compression failed\n");
    return 1;
  }
  //
  printf("compressed %d bytes to %d bytes\n", (int)fsz, (int)osz);
  //
  if ((fo = fopen(argv[2], "wb")) == NULL) {
    fprintf(stderr, "ERROR: can't create output file: %s\n", argv[2]);
    return 1;
  }
  //
  if (strrchr(argv[2], '.') != NULL && strcmp(strrchr(argv[2], '.'), ".c") == 0) {
    // write C file
    int cnt = 0;
    //
    fprintf(fo, "static int upSize = %d;\n", (int)fsz);
    fprintf(fo, "static int pkSize = %d;\n", (int)osz);
    fprintf(fo, "\n");
    fprintf(fo, "static unsigned char pkData[%d] = {", (int)osz);
    for (int f = 0; f < (int)osz; ++f) {
      if (cnt-- <= 0) {
        fputc('\n', fo);
        cnt = 16;
      }
      fprintf(fo, "0x%02x,", obuf[f]);
    }
    if (cnt > 0) fputc('\n', fo);
    fprintf(fo, "};\n");
  } else {
    //
    // dw unpacked size
    // dw packed size
    // ...packed data
    {
      uint32_t i = fsz;
      //
      fwrite(&i, 4, 1, fo);
      i = osz;
      fwrite(&i, 4, 1, fo);
    }
    fwrite(obuf, osz, 1, fo);
  }
  fclose(fo);
  //
  return 0;
}
