if {$::jim_internal} {
  load "jimstd/stdlib.tcl"
  load "jimstd/nshelper.tcl"
  load "jimstd/oo.tcl"
    load "jimstd/glob.tcl"
  load "jimstd/tclcompat.tcl"
  load "jimstd/binary.tcl"
  load "jimstd/tree.tcl"
  load "jimstd/jsonencode.tcl"

  #if {$::jim_unsafe_allowed} {
  #}
} else {
}
