################################################################################
proc _zxemut_init {} {
  rename _zxemut_init {}

  # Add to the standard auto_path
  lappend p "$::emudir/tcl/lib" "$::emudir/tcl/jimstd"
  lappend p {*}$::auto_path
  set ::auto_path $p
  #cputs "$::auto_path"
}

_zxemut_init


################################################################################
set ::romlist [dict create]

proc ::zxemut::cfg::romtypes::normal: {dsc} { dict set ::romlist $::romlist_current_model normal: $dsc }
proc ::zxemut::cfg::romtypes::betadisk: {dsc} { dict set ::romlist $::romlist_current_model betadisk: $dsc }
proc ::zxemut::cfg::romtypes::trdos: {dsc} { dict set ::romlist $::romlist_current_model trdos: $dsc }
proc ::zxemut::cfg::romtypes::gluck: {dsc} { dict set ::romlist $::romlist_current_model gluck: $dsc }
proc ::zxemut::cfg::romtypes::quickcmdr: {dsc} { dict set ::romlist $::romlist_current_model quickcmdr: $dsc }

proc ::zxemut::cfg::romtypes_common::do {model dsc} {
  set ::romlist_current_model $model
  namespace eval ::zxemut::cfg::romtypes $dsc
}

proc ::zxemut::cfg::roms::48: {dsc} { ::zxemut::cfg::romtypes_common::do 48: $dsc }
proc ::zxemut::cfg::roms::128: {dsc} { ::zxemut::cfg::romtypes_common::do 128: $dsc }
proc ::zxemut::cfg::roms::plus2: {dsc} { ::zxemut::cfg::romtypes_common::do plus2: $dsc }
proc ::zxemut::cfg::roms::plus2a: {dsc} { ::zxemut::cfg::romtypes_common::do plus2a: $dsc }
proc ::zxemut::cfg::roms::plus3: {dsc} { ::zxemut::cfg::romtypes_common::do plus3: $dsc }
proc ::zxemut::cfg::roms::pentagon: {dsc} { ::zxemut::cfg::romtypes_common::do pentagon: $dsc }
proc ::zxemut::cfg::roms::pentagon128: {dsc} { ::zxemut::cfg::romtypes_common::do pentagon128: $dsc }
proc ::zxemut::cfg::roms::pentagon512: {dsc} { ::zxemut::cfg::romtypes_common::do pentagon512: $dsc }
proc ::zxemut::cfg::roms::pentagon1024: {dsc} { ::zxemut::cfg::romtypes_common::do pentagon1024: $dsc }
proc ::zxemut::cfg::roms::scorpion: {dsc} { ::zxemut::cfg::romtypes_common::do scorpion: $dsc }

proc ::zxemut::cfg::roms::trdos: {dsc} { dict set ::romlist trdos: $dsc }
proc ::zxemut::cfg::roms::gluck: {dsc} { dict set ::romlist gluck: $dsc }
proc ::zxemut::cfg::roms::quickcmdr: {dsc} { dict set ::romlist quickcmdr: $dsc }
proc ::zxemut::cfg::roms::opense: {dsc} { dict set ::romlist opense: $dsc }


proc roms: {dsc} {
  set ::romlist [dict create]
  set ::romlist_current_model {}
  namespace eval ::zxemut::cfg::roms $dsc
  unset ::romlist_current_model
  #cputs "=== ROMS ==="
  #cputs "$::romlist"
}

# use this to replace roms in "autoexec.tcl"
proc user-roms: {dsc} {
  set ::romlist_current_model {}
  namespace eval ::zxemut::cfg::roms $dsc
  unset ::romlist_current_model
  #cputs "=== ROMS ==="
  #cputs "$::romlist"
}


################################################################################
proc binds: {lst} {
  for {set idx 0} {$idx < [llength $lst]-1} {incr idx 2} {
    set key [lindex $lst $idx]
    set act [lindex $lst $($idx+1)]
    #
    bind {*}$key $act
  }
}


proc zxbinds: {lst} {
  for {set idx 0} {$idx < [llength $lst]-1} {incr idx 2} {
    set key [lindex $lst $idx]
    set act [lindex $lst $($idx+1)]
    #
    #cputs "zxbind: <$key> <$act>"
    zxbind $key {*}$act
  }
}
