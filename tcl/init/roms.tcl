roms: {
  48: {
    normal: { "$::emudir/romz/48/48.rom" }
    # betadisk fallbacks to normal
    #betadisk: { "$::emudir/romz/48/48.rom" }
  }
  128: {
    normal: { "$::emudir/romz/128/128.rom:0" "$::emudir/romz/128/128.rom:1" }
  }
  plus2: {
    normal: { "$::emudir/romz/128/plus2.rom:0" "$::emudir/romz/128/plus2.rom:1" }
  }
  plus2a: {
    normal: { "$::emudir/romz/128/plus3/plus3-41-0.rom:0"
              "$::emudir/romz/128/plus3/plus3-41-1.rom:0"
              "$::emudir/romz/128/plus3/plus3-41-2.rom:0"
              "$::emudir/romz/128/plus3/plus3-41-3.rom:0" }
  }
  plus3: {
    normal: { "$::emudir/romz/128/plus3/plus3-41-0.rom:0"
              "$::emudir/romz/128/plus3/plus3-41-1.rom:0"
              "$::emudir/romz/128/plus3/plus3-41-2.rom:0"
              "$::emudir/romz/128/plus3/plus3-41-3.rom:0" }
  }
  pentagon: {
    normal: { "$::emudir/romz/pentagon/128_pentagon.rom:0" "$::emudir/romz/pentagon/128_pentagon.rom:1" }
    # trdos and gluck fallbacks to 'global'
  }
# pentagon128, pentagon512 and pentagon1024 fallbacks to pentagon
  #pentagon128: {
  #  normal: { "$::emudir/romz/pentagon/128_pentagon.rom:0" "$::emudir/romz/pentagon/128_pentagon.rom:1" }
  #}
  #pentagon512: {
  #  normal: { "$::emudir/romz/pentagon/128_pentagon.rom:0" "$::emudir/romz/pentagon/128_pentagon.rom:1" }
  #}
  #pentagon1024: {
  #  normal: { "$::emudir/romz/pentagon/128_pentagon.rom:0" "$::emudir/romz/pentagon/128_pentagon.rom:1" }
  #}
  scorpion: {
    normal: { "$::emudir/romz/scorpion/scorpion.rom:0"
              "$::emudir/romz/scorpion/scorpion.rom:1"
              "$::emudir/romz/scorpion/scorpion.rom:2" }
    trdos: { "$::emudir/romz/scorpion/scorpion.rom:3" }
  }
  #trdos: "$::emudir/romz/trdos/trdos505cc.rom"
  trdos: "$::emudir/romz/trdos/dos6_10e.rom"
  #gluck: "$::emudir/romz/other/gluck_6_3r.rom"
  gluck: "$::emudir/romz/other/gluck_6_61.rom"
  #quickcmdr: "$::emudir/romz/other/qc_3_05.rom"
  quickcmdr: "$::emudir/romz/other/qc_3_11.rom"
  #opense: "$::emudir/romz/opense/opense.rom"
  opense: "$::emudir/romz/opense/opense_48font.rom"
}
