binds: {
  {alt enter}  {fullscreen toggle}

  tilda {console toggle}
  {ctrl tilda}  {console toggle}

  {ctrl alt q}  {quit}

  {alt F1}  {pause toggle}
  {shift F1}  {keyhelp toggle}
  {ctrl F1}  {disk boot add}

  F2 {snapshot save snapshot.szx}
  F3 {snapshot load snapshot.szx}

  {F7}  {tape start}
  {alt F7}  {tape stop}
  {ctrl F7}  {tape rewind}

  F8 {memview toggle}
  {alt F8} {sprview toggle}

  F9 {speed toggle}
  F11 {debugger toggle}

  {ctrl F11}  {grab reset}
  {alt F11}  {grab set}

  {ctrl F12}  {reset}
  {alt F12}  {reset trdos}
}
