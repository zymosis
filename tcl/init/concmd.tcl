set ::concmdlist [dict create]
set ::concmdhelp [dict create]


proc ::con::showhelp {cn} {
  global concmdhelp
  if {[dict exists $concmdhelp $cn]} {
    # show help
    cputs "\x02$cn: $concmdhelp($cn)"
  }
}


proc ::con::helpcomplete {args} {
  global concmdhelp

  set cl [dict keys $concmdhelp]
  set lpx {}

  if {[llength $args] < 1} {
    cputs "\x01help is available for:"
    foreach cmd [lsort [dict keys $concmdhelp]] { cputs "\x01$cmd" }
    set lpx [tcl::prefix longest $cl ""]
  } else {
    set cmd [lindex $args 0]
    if {[dict exists $concmdhelp $cmd]} {
      # show help
      set lpx $cmd
    } else {
      set lst [tcl::prefix all $cl $cmd]
      if {[llength $lst] == 1} {
        set lpx [lindex $lst 0]
      } elseif {[llength $lst] > 1} {
        foreach c [lsort $lst] { cputs "\x01$c" }
        set lpx [tcl::prefix longest $lst $cmd]
      } else {
        set lpx $cmd
      }
    }
  }

  return "help $lpx"
}


dict set concmdlist help [lambda {args} {
  global concmdhelp

  #cputs "help: <$args>"
  if {[llength $args] < 2} {
    cputs "help is available for:"
    foreach cn [dict keys $concmdhelp] {
      cputs "\x01$cn"
    }
  } else {
    set cn [lindex $args 1]
    if {[dict exists $concmdhelp $cn]} {
      # show help
      ::con::showhelp $cn
    } else {
      # show autocomplete or help
      set cl [dict keys $concmdhelp]
      set lst [tcl::prefix all $cl $cn]
      if {[llength $lst] == 1} { tailcall ::con::showhelp $cn }
      if {[llength $lst] > 1} {
        set lpx [tcl::prefix longest $lst $cn]
        consetstr "help $lpx"
      }
    }
  }
}]


proc concmd_simple_command {cmd args} {
  if {[llength $args] > 0} { $cmd {*}$args } else { cputs "$cmd: [$cmd]" }
}

proc concmd_simple_command_noprint {cmd args} {
  $cmd {*}$args
}


proc concmd_info_command {cmd args} {
  cputs "$cmd: [$cmd {*}$args]"
}


proc concmdlist-register {handler lst} {
  set len [llength $lst]
  for {set idx 0} {$idx < $len} {} {
    set cn [lindex $lst $idx]
    incr idx
    dict set ::concmdlist $cn $handler
    if {$idx < $len && [lindex $lst $idx] eq "-help"} {
      incr idx
      if {$idx < $len} {
        dict set ::concmdhelp $cn [string trim [lindex $lst $idx]]
        incr idx
      }
    }
  }
}


# return result is not printed
concmdlist-register concmd_simple_command_noprint {
  load -help { load Tcl source }
  quit -help { quit ZXEmut }
  reset -help { reset emulator
reset [forced] [model] [issue] [memory] [trdos]
48k: [issue2|issue3|2|3]
pentagon: [128|512|1024]
}
  snapshot -help { load or save snapshot
snapshot <load|save> name
}
  zxbind
  zxunbind
  bind
  unbind
  poke
  wpoke
  tape -help { tape command ?...?
commands:
  insert filename
  save filename (not yet)
  eject
  clear
  start
  stop
  rewind
  list (not yet)
  option
    maxspeed      on/off/toggle
    detectloader  on/off/toggle
    autopause     on/off/toggle
}
  disk -help { disk command ?...?
commands:
  insert filename
  save filename
  eject
  format
  option autoadd_boot
  boot add [name] | remove | replace [name]
  protect
  unprotect
  info
  list
  extract
  add
if filename starts with '[A-D]:'
it means virtual drive number
}
  nmi
  bp
  label
  reffile
  colormode -help { set color mode:
 normal
 monochrome (black-and-white)
 green
}
  brightborder -help { control pentagon border brighness
off: none used
5|6|7: bit of ULA port used to control brightness
}
  autofire
  rzx
  fldebug -help { FlashLoad loader detector debugging
}
  tsmark
  tsdiff
  kbreset
  ayreset
}

# return result is printed when the command is called without arguments
concmdlist-register concmd_simple_command {
  console
  debugger
  memview
  sprview
  timings
  speed
  pause
  kbleds
  screenofs
  snow
  fullscreen
  noflic
  brightblack
  zxpalcolor
  filter
  keyhelp
  sound
  allowother128
  keymatrix
  issue
  grab
  maxspeedugly
  trdos
  fps
  swapbuttons
  kmouse
  kspanish
  kjoystick
  bad7ffd
  ay
  contention
  iocontention
  memcontention
  useplus3
  scrattrfill
  vid_scale
  vid_rescaler
  curblink
  opense
  gluck
  quickcommander
}

# result is always printed
concmdlist-register concmd_info_command {
  model
  peek
  wpeek
}



# this is called from `conexec`
# return "done" to stop processing
# other return values means "go on"
proc conexec_hook {args} {
  return "cont"
}


proc conexec {args} {
  global concmdlist
  if {[llength $args] > 0} {
    try {
      set ::con::executing 1
      if {[conexec_hook $args] != "done"} {
        #cputs [lindex $args 0]
        if {[dict exists $concmdlist [lindex $args 0]]} {
          set fn [dict get $concmdlist [lindex $args 0]]
          $fn [lindex $args 0] {*}[lrange $args 1 end]
        } else {
          cputs "unknown console command: [lindex $args 0]"
        }
      }
    } finally {
      set ::con::executing 0
    }
  }
}


# called when debugger breakpoint hit
# [lindex args 0] is breakpoint type (number)
proc breakpointhit {args} {
  # return "stop" or non-zero to activate debugger
  #cputs "BREAKPOINT HIT! [z80 getreg pc]"
  return "stop"
  #cputs "BREAKPOINT HIT!"
  #return "continue"
}


# called when a command received from unix socket
# return:
#   "close" to close connection
#   "done" to indicate that no further processing required
#   anything else to pass the command to console
proc usock_received {args} {
  return ""
}


# called when we need to autocomplete a command
proc conexec_ac {args} {
  #cputs "conexec_ac: [llength $args]"
  #foreach arg $args { cputs "  <$arg>" }
  #set cmd [lindex $args 0]
  try {
    set ::con::autocompletion 1
    #return [$cmd " ?ac" {*}[lrange $args 1 end]]
    #return [$cmd {*}[lrange $args 1 end]]
    #set res [{*}$args]
    #cputs "res: conexec_ac: [llength $res]"
    #foreach arg $args { cputs "  <$arg>" }
    #return $res
    return [{*}$args]
  } finally {
    set ::con::autocompletion 0
  }
}


# this is my first complex function in Tcl; enjoy!
proc conautocomplete {args} {
  global concmdlist

  set argl $args
  if {[llength $argl] == 0} { return "" }

  set cmd [string map {* _ ? _ . _} [lindex $argl 0]]
  #set lst [dict keys $concmdlist "$cmd*"]
  set cl [dict keys $concmdlist]
  set lst [tcl::prefix all $cl $cmd]
  set res ""
  set pos 0

  if {[llength $argl] > 1} {
    # command with args
    if {[llength $lst] == 1} {
      # call command autocompleter
      # FIXME: quote list items with spaces
      set cmd [lindex $lst 0]
      if {$cmd eq "help"} { tailcall ::con::helpcomplete {*}[lrange $argl 1 end] }
      return [conexec_ac $cmd {*}[lrange $argl 1 end]]
    }
    # shit! there is no such command!
    return ""
  }

  if {[llength $lst] == 1} {
    # only one command
    set res [lindex $lst 0]
    if {[string equal $res $cmd]} {
      if {$cmd eq "help"} { tailcall ::con::helpcomplete {*}[lrange $argl 1 end] }
      return [conexec_ac $cmd {*}[lrange $argl 1 end]]
    } else {
      return "$res "
    }
  }

  # many commands
  if {[llength $lst] > 1} {
    set res [tcl::prefix longest $lst $cmd]
    cputs "\x01===="
    foreach ccmd [lsort $lst] { cputs "\x01$ccmd" }
  }

  return $res
}
