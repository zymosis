#zxbind LALT "sym"
#zxbind RALT "sym"

# basic bindings
zxbinds: {
  a "a"
  b "b"
  c "c"
  d "d"
  e "e"
  f "f"
  g "g"
  h "h"
  i "i"
  j "j"
  k "k"
  l "l"
  m "m"
  n "n"
  o "o"
  p "p"
  q "q"
  r "r"
  s "s"
  t "t"
  u "u"
  v "v"
  w "w"
  x "x"
  y "y"
  z "z"
  0 "0"
  1 "1"
  2 "2"
  3 "3"
  4 "4"
  5 "5"
  6 "6"
  7 "7"
  8 "8"
  9 "9"
  SPACE "spc"
  RETURN "ent"
  KP_ENTER "ent"
  LSHIFT "cap"
  RSHIFT "sym"
}

# convenient compound bindings
zxbinds: {
  BACKSPACE   {"cap" "0"}
  ESCAPE      {"cap" "1"}
  COMMA       {"sym" "n"}
  PERIOD      {"sym" "m"}
  SLASH       {"sym" "v"}
  EQUALS      {"sym" "l"}
  MINUS       {"sym" "j"}
  QUOTE       {"sym" "p"}
  SEMICOLON   {"sym" "o"}
  TAB         {"sym" "cap"}
  KP_MINUS    {"sym" "j"}
  KP_PLUS     {"sym" "k"}
  KP_MULTIPLY {"sym" "b"}
  KP_DIVIDE   {"sym" "v"}
  KP_PERIOD   {"sym" "m"}
}

# kempston joystick (arrows)
zxbinds: {
  LEFT  "kleft"
  RIGHT "kright"
  UP    "kup"
  DOWN  "kdown"
  LCTRL "kfire"
}
#zxbind RCTRL "kfire"

# kempston joystick (numpad)
zxbinds: {
  KP4 "kleft"
  KP6 "kright"
  KP8 "kup"
  KP2 "kdown"
  KP0 "kfire"
}
