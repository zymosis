set win_x 0
set win_y 0

proc nw {} {
  set win [
   window: {
    scheme: red
    id: win1
    id: win0
    title: mytitle
    position: {$::win_x $::win_y}
    size: {180 120}
    margins: {2 1}
    #
    static: {
      id: static0
      caption: "&name:"
      position: {2 2}
      linked-to: led0
    }
    #
    lineedit: {
      id: led0
      position: {after static0}
      width: 60
      text: "text"
    }
    # yes, we can change margins 'in the middle'
    margins: {2 2}
    #
    checkbox: {
      id: cb1
      caption: "checkbox &1"
      position: {under static0}
      #state: 1
      checked
    }
    #
    margins: {2 1}
    #
    checkbox: {
      id: cbsound
      caption: "so&und"
      position: {under cb1}
      #state: 2
      question
      oninit: {
        if {[sound] eq "on"} {
          $me setstate 1
        } elseif {[sound] eq "off"} {
          $me setstate 0
        }
      }
    }
    #
    radio: {
      group: rgroup1
      id: g1r1
      caption: "r&adio 1"
      position: { {{x cbsound} + 4}  {{under cbsound} + 11} }
    }
    #
    radio: {
      group: rgroup1
      id: g1r2
      caption: "ra&dio 2"
      position: {under g1r1}
      radioactive
    }
    #
    frame: {
      id: frm
      caption: frame
      #framebox sets both position and size
      framebox: g1r1 g1r1
    }
    #
    scrollbar: {
      horiz
      id: hsb
      position: { {{x g1r1}}  {{y frm} + {height frm} + 4} }
      size: 64
    }
    #
    scrollbar: {
      vert
      id: vsb
      position: { {{x hsb} + {width hsb} + 4}  {{y hsb} - 32} }
      size: 60
      max: 32
      pos: 6
    }
    #
    listbox: {
      id: lsb
      position: {100 10}
      size: {60 90}
      vbar
      multi
      items: {
        "xitem 0"
        "xitem 1"
      }
      oninit: {
        #puts "listbox oninit <$me>"
        loop ic 0 22 { $me additem "item $ic" }
      }
    }
    # yes, we can change margins 'in the middle'
    margins: {2 1}
    #
    button: {
      id: ok
      caption: O&K
      position: {2  {{height window} - 9 - 12}}
      onclick: {
        set win [$me window]
        puts "=== OK ==="
        puts [$me type]
        puts [$win type]
        puts "<$me>"
        puts "<$win>"
        #
        puts "cb1: [[$win cb1] getstate]"
        puts "cbsound: [[$win cbsound] getstate]"
        puts "radiogroup 'rgroup1': [$win getradioactive rgroup1]"
        puts "g1r1 group: [[$win g1r1] getgroup]"
        puts "g1r1: [[$win g1r1] getradioactive]"
        puts "g1r2: [[$win g1r2] getradioactive]"
        puts "edt: \[[[$win led0] gettext]\]"
        #
        switch [[$win cbsound] getstate] {
          0 { sound off }
          1 { sound on }
        }
      }
      default
      active
    }
    #
    button: {
      id: cancel
      caption: Cancel
      position: {after ok}
      onclick: {
        set win [$me window]
        puts "=== CANCEL ==="
        puts [$me type]
        puts [$win type]
        [$me window] close
      }
      cancel
    }
   }]
  #
  ##set lsb [$win lsb]
  ##loop ic 0 22 {
  ##  $lsb additem "item $ic"
  ##}
  #
  set ::win_x $($::win_x + 16)
  set ::win_y $($::win_y + 16)
  if {$::win_x > 300} { set ::win_x 1 }
  if {$::win_y > 220} { set ::win_y 1 }
  #
  set ::win0 $win
}


proc cw {} {
  $::win0 close
  unset ::win0
}


nw
