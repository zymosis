set win_x 0
set win_y 0

proc nw {} {
  set win [winsys window "red" "win0" $::win_x $::win_y 180 120 "title"]
  puts "win=$win"
  #
  set ok [$win button "ok" 2 $([$win geth]-9-12) -1 -1 "O&K" [lambda {me} {
    set win [$me window]
    puts "=== OK ==="
    puts [$me type]
    puts [$win type]
    #
    puts -nonewline "cb1: "
    puts [[$win "cb1"] getstate]
    #
    puts -nonewline "cb2: "
    puts [[$win "cb2"] getstate]
    #
    puts -nonewline "radiogroup 'rgroup1': "
    puts [$win getradioactive "rgroup1"]
    #
    puts -nonewline "rb1 group: "
    puts [[$win "g1r1"] getgroup]
    #
    puts -nonewline "rb1: "
    puts [[$win "g1r1"] getradioactive]
    #
    puts -nonewline "rb2: "
    puts [[$win "g1r2"] getradioactive]
    #
    puts -nonewline "edt: \["
    puts -nonewline [[$win "led0"] gettext]
    puts "\]"
  }]]
  $ok setdefault
  $ok activate
  #
  set cancel [$win button "cancel" $([$ok getx]+[$ok getw]+2) [$ok gety] -1 -1 "Cancel" [lambda {me} {
    #puts [winsys type $me]
    puts "Cancel"
    [$me window] close
  }]]
  $cancel setcancel
  #
  set st [$win static "static_name" 2 2 -1 -1 "&name:"]
  #
  set le [$win lineedit "led0" $([$st getx]+[$st getw]+2) [$st gety] 60 -1 "text"]
  $st setlinked $le
  #
  set cb1 [$win checkbox "cb1" [$st getx] $([$st gety]+[$st geth]+1) -1 -1 "checkbox &1"]
  $cb1 setstate 1
  #
  set cb2 [$win checkbox "cb2" [$cb1 getx] $([$cb1 gety]+[$cb1 geth]+1) -1 -1 "checkbox &2"]
  $cb2 setstate 2
  #
  set rb1 [$win radio "rgroup1" "g1r1" $([$cb2 getx]+4) $([$cb2 gety]+[$cb2 geth]+12) -1 -1 "r&adio 1"]
  #
  set rb2 [$win radio "rgroup1" "g1r2" [$rb1 getx] $([$rb1 gety]+[$rb1 geth]+1) -1 -1 "ra&dio 2"]
  $rb2 setradioactive
  #
  set frm [$win frame "frm" $([$rb1 getx]-3) $([$rb1 gety]-9) 61 31 "frame"]
  #
  set hsb [$win scrollbar horiz "hsb" [$rb1 getx] $([$frm gety]+[$frm geth]+4) 64]
  #
  set vsb [$win scrollbar vert "vsb" $([$hsb getx]+[$hsb getw]+4) $([$hsb gety]-32) 60]
  $vsb setmax 32
  #
  set lsb [$win listbox "lsb" 100 10 60 90]
  $lsb setvbar 1
  $lsb setmulti 1
  #
  loop ic 0 22 {
    $lsb additem "item $ic"
  }
  #
  set ::win_x $($::win_x + 16)
  set ::win_y $($::win_y + 16)
  if {$::win_x > 300} { set ::win_x 1 }
  if {$::win_y > 220} { set ::win_y 1 }
  #
  set ::win0 $win
}

proc cw {} {
  $::win0 close
  unset ::win0
}
