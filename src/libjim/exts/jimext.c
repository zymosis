#include "../jim.h"


static int jim_k8_unsafe_allowed = 0;


void Jim_SetAllowUnsafeExtensions (int allowed) {
  jim_k8_unsafe_allowed = (allowed ? 1 : 0);
}


int Jim_GetAllowUnsafeExtensions (void) {
  return jim_k8_unsafe_allowed;
}


int Jim_InitStaticExtensions (Jim_Interp *interp) {
  extern int Jim_namespaceInit(Jim_Interp *);
  extern int Jim_packageInit(Jim_Interp *);

  extern int Jim_arrayInit(Jim_Interp *);
  extern int Jim_clockInit(Jim_Interp *);
  extern int Jim_jsonInit(Jim_Interp *);
  /*extern int Jim_loadInit(Jim_Interp *);*/
  extern int Jim_packInit(Jim_Interp *);
  extern int Jim_posixInit(Jim_Interp *);
  extern int Jim_readdirInit(Jim_Interp *);
  extern int Jim_regexpInit(Jim_Interp *);
  extern int Jim_tclprefixInit(Jim_Interp *);
  extern int Jim_zlibInit(Jim_Interp *);

  extern int Jim_aioInit(Jim_Interp *);
  extern int Jim_fileInit(Jim_Interp *);
  extern int Jim_execInit(Jim_Interp *);
  extern int Jim_signalInit(Jim_Interp *);
  extern int Jim_syslogInit(Jim_Interp *);


  Jim_packageInit(interp);
  Jim_namespaceInit(interp);

  Jim_arrayInit(interp);
  Jim_clockInit(interp);
  //Jim_fileInit(interp);
  Jim_jsonInit(interp);
  /*Jim_loadInit(interp);*/
  Jim_packInit(interp);
  Jim_posixInit(interp);
  Jim_readdirInit(interp);
  Jim_regexpInit(interp);
  Jim_tclprefixInit(interp);
  #ifdef HAVE_ZLIB
  Jim_zlibInit(interp);
  #endif

  /*if (jim_k8_unsafe_allowed)*/ {
    Jim_aioInit(interp);
    Jim_fileInit(interp);
    Jim_execInit(interp);
    Jim_signalInit(interp);
    Jim_syslogInit(interp);
  }

  return JIM_OK;
}
