/* coded by Ketmar // Invisible Vector ( ketmar@ketmar.no-ip.org )
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <unistd.h>

#include "../libfdc/libfdc.h"


static inline int isLeapYear (int y) { return (y%400 == 0 || (y%4 == 0 && y%100 != 0)); }
static inline unsigned bcd2num (uint8_t b) { return (b&0x0f)+((b>>4)&0x0f)*10; }


// a perfectly idiotic function
static void decodeDays (int days, int *year, int *month, int *day) {
  if (days < 0) abort();
  // calc year
  int y = 1978;
  for (;;) {
    const int ydays = 365+isLeapYear(y);
    if (days < ydays) break; // this year
    ++y;
    days -= ydays;
  }
  // calc month
  int m = 0;
  int mdays[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  mdays[1] += isLeapYear(y);
  while (days >= mdays[m]) {
    days -= mdays[m];
    ++m;
  }
  *year = y;
  *month = m+1;
  *day = days+1;
}


static void rawdir (P3DiskInfo *p3d) {
  P3DskGeometry *geom = &p3d->geom;
  for (int f = 0; f < geom->maxdirentries; ++f) {
    const uint8_t *de = p3dskGetDirEntryPtr(p3d, f);
    if (!de) abort();
    if (de[0] == 0xe5u /*|| de[0] == 33*/) continue; // ignore timestamps too
    printf("%3d:", f);
    // 0-15: user #n file
    // 16-31: user #n for P2DOS, password extent for CP/M 3+
    // 32: disk label
    // 33: time stamp
    if (de[0] == 33) {
      // decode timestamp
      if ((f&3) != 3) printf(" BAD");
      printf(" timestamp:\n");
      for (unsigned tt = 0; tt < 4; ++tt) {
        // creation date
        int cy, cm, cd;
        decodeDays(de[1+tt*10+0]|(de[1+tt*10+1]<<8), &cy, &cm, &cd);
        // modification date
        int my, mm, md;
        decodeDays(de[1+tt*10+4]|(de[1+tt*10+5]<<8), &my, &mm, &md);
        printf("    C:%4u/%2u/%2u %2u:%2u  M:%4u/%2u/%2u %2u:%2u\n",
          cy, cm, cd, bcd2num(de[1+tt*10+2]), bcd2num(de[1+tt*10+3]),
          my, mm, md, bcd2num(de[1+tt*10+6]), bcd2num(de[1+tt*10+7]));
      }
      // print allocation table
      printf("      ");
      for (unsigned c = 0; c < 16; ++c) printf("%3u ", de[16+c]);
      printf("\n");
      continue;
    }
    if (de[0] == 0xe5U || de[0] == 33 || (de[0] >= 16 && de[0] <= 31) || de[0] > 33) {
           if (de[0] == 33) printf("%3u: time:", de[0]);
      else if (de[0] == 0xe5U) printf("%3u: unused:", de[0]);
      else if (de[0] > 33) printf("%3u: unknown:", de[0]);
      else printf("%3u: pass:", de[0]);
      for (unsigned c = 1; c < 16; ++c) {
        const uint8_t ch = de[c];
        printf(" %02X:%c", ch, (char)((ch&0x7fU) > 32 ? ch&0x7fU : '.'));
      }
      printf("\n");
      // print allocation table
      printf("      ");
      for (unsigned c = 0; c < 16; ++c) printf("%3u ", de[16+c]);
      continue;
    }
    printf("%3u: name: [", de[0]);
    for (unsigned c = 0; c < 8; ++c) {
      uint8_t ch = de[1+c]&0x7fU;
      if (ch < 32) ch = '?';
      printf("%c", (char)ch);
    }
    printf(".");
    for (unsigned c = 0; c < 3; ++c) {
      uint8_t ch = de[9+c]&0x7fU;
      if (ch < 32) ch = '?';
      printf("%c", (char)ch);
    }
    printf("] ");
    if (de[9+0]&0x80u) printf("R"); else printf("w");
    if (de[9+1]&0x80u) printf("S"); else printf(".");
    if (de[9+2]&0x80u) printf("A"); else printf(".");
    printf("  ");
    printf("X=%2u %2u ", de[12], de[14]);
    printf("B=%3u ", (de[13] ? de[13] : 128u)); // the number of bytes in the last used record
    printf("R=%3u ", de[15]); // the number of 128 byte records of the last used logical extent
    for (unsigned c = 0; c < 16; ++c) printf("%3u ", de[16+c]);
    printf("\n");
  }

  uint8_t *bmp = p3dskBuildBlockBitmap(p3d->flp, &p3d->geom);
  if (bmp) {
    printf("BLOCK BITMAP: %u blocks of %u bytes\n", p3d->geom.maxblocks, p3d->geom.blocksize);
    for (unsigned bnum = 0; bnum < p3d->geom.maxblocks; bnum += 10) {
      printf("%4u:", bnum);
      for (unsigned c = 0; c < 10 && bnum+c < p3d->geom.maxblocks; ++c) {
        if (bmp[bnum+c]) printf("%4u", bnum+c); else printf(" ...");
      }
      printf("\n");
    }
    free(bmp);
  } else {
    printf("FUCK! CANNOT BUILD BLOCK BITMAP!\n");
  }
}


enum {
  CmdDir = 0,
  CmdRawDir,
  CmdRawExtract,
  CmdExtractAll,
  CmdExtract,
  CmdDelete,
  CmdPut,
  CmdPutCode,
};


// no sanity checks
static int extractFileIntr (FILE *fo, P3DskFileInfo *nfo) {
  uint8_t fbuf[61];
  size_t left = nfo->size;
  size_t fpos = 0;
  while (left) {
    //fprintf(stderr, "  reading: %u bytes still left at pos %u\n", (unsigned)left, (unsigned)fpos);
    int rd = p3dskReadFile(nfo, fbuf, fpos, (int)sizeof(fbuf));
    if (rd < 0) {
      fprintf(stderr, "  READ ERROR: %u bytes still left at pos %u!\n", (unsigned)left, (unsigned)fpos);
      return -1;
    }
    if (rd == 0) break;
    fwrite(fbuf, (unsigned)rd, 1, fo);
    left -= (unsigned)rd;
    fpos += (unsigned)rd;
  }
  if (left != 0) {
    fprintf(stderr, "  ERROR READING FILE! %u bytes still left at pos %u!\n", (unsigned)left, (unsigned)fpos);
    return -1;
  }
  return 0;
}


// no sanity checks
static int extractFile (P3DskFileInfo *nfo) {
  char fnstr[64];
  snprintf(fnstr, sizeof(fnstr), "_unpack/%s", nfo->name);
  FILE *fo = fopen(fnstr, "wb");
  if (!fo) return -1;
  int res = extractFileIntr(fo, nfo);
  fclose(fo);
  return res;
}


int main (int argc, char *argv[]) {
  Floppy *flp = flpCreate(0);

  // new disk?
  if (argc > 2 && strncmp(argv[1], "new", 3) == 0) {
    int dsktype = P3DSK_PCW_DS;
    switch (argv[1][3]) {
      case '0': dsktype = P3DSK_PCW_SS; break;
      case '1': dsktype = P3DSK_SYSTEM; break;
      case '2': dsktype = P3DSK_DATA; break;
      case '3': dsktype = P3DSK_PCW_DS; break;
    }
    if (p3dskFormatDisk(flp, dsktype) != FLPERR_OK) { fprintf(stderr, "cannot create disk\n"); return 1; }
    FILE *fo = fopen(argv[2], "wb");
    if (!fo) { fprintf(stderr, "cannot create disk file\n"); return 1; }
    if (dskSaveDSK(flp, fo) != FLPERR_OK) {
      unlink(argv[2]);
      fprintf(stderr, "error writing disk file\n");
      return 1;
    }
    fclose(fo);
    printf("create new disk image '%s'\n", argv[2]);
    return 0;
  }

  const char *dskImageFileName = NULL;
  int argp = 1;
  int command = CmdDir;
       if (argp < argc && strcmp(argv[argp], "rawdir") == 0) { ++argp; command = CmdRawDir; }
  else if (argp < argc && strcmp(argv[argp], "dir") == 0) { ++argp; command = CmdDir; }
  else if (argp < argc && strcmp(argv[argp], "rawextract") == 0) { ++argp; command = CmdRawExtract; }
  else if (argp < argc && strcmp(argv[argp], "extractall") == 0) { ++argp; command = CmdExtractAll; }
  else if (argp < argc && strcmp(argv[argp], "extract") == 0) { ++argp; command = CmdExtract; }
  else if (argp < argc && strcmp(argv[argp], "del") == 0) { ++argp; command = CmdDelete; }
  else if (argp < argc && strcmp(argv[argp], "era") == 0) { ++argp; command = CmdDelete; }
  else if (argp < argc && strcmp(argv[argp], "put") == 0) { ++argp; command = CmdPut; }
  else if (argp < argc && strcmp(argv[argp], "putcode") == 0) { ++argp; command = CmdPutCode; }

  if (argp < argc) dskImageFileName = argv[argp++];
  if (!dskImageFileName) dskImageFileName = "genesis.dsk";

  {
    FILE *fl = fopen(dskImageFileName, "rb");
    if (dskLoadDSK(flp, fl) != FLPERR_OK) { fprintf(stderr, "no disk image file\n"); return -1; }
    fclose(fl);
  }

  printf("DSK info: %d side%s, %d tracks\n", (flp->doubleSide ? 2 : 1), (flp->doubleSide ? "s" : ""), (flp->trk80 ? 80 : 40));

  P3DiskInfo p3d;
  p3d.flp = flp;
  if (p3dskDetectGeom(p3d.flp, &p3d.geom) < 0) {
    fprintf(stderr, "not a +3DOS disk!\n");
    return -1;
  }

  printf("sides: %u (successive=%u)\n", p3d.geom.sides, p3d.geom.successive);
  printf("cylinders: %u\n", p3d.geom.cyls);
  printf("first sector: %u\n", p3d.geom.firstsector);
  printf("sectors: %u\n", p3d.geom.sectors);
  printf("sector size: %u\n", p3d.geom.secsize);
  printf("reserved tracks: %u\n", p3d.geom.restracks);
  printf("block size: %u\n", p3d.geom.blocksize);
  printf("max blocks: %u\n", p3d.geom.maxblocks);
  printf("directory blocks: %u\n", p3d.geom.dirblocks);
  printf("max dir entries: %u\n", p3d.geom.maxdirentries);
  printf("checksum: %u\n", p3d.geom.checksum);

  if (command == CmdRawDir) {
    rawdir(&p3d);
  } else if (command == CmdDelete) {
    if (argp >= argc) { fprintf(stderr, "files?\n"); return -1; }
    printf("deleting files...\n");
    int needwrite = 0;
    for (; argp < argc; ++argp) {
      const char *pdfname = argv[argp];
      printf("deleting '%s'...\n", pdfname);
      int res = p3dskDeleteFiles(&p3d, pdfname);
           if (res == FLPERR_OK) needwrite = 1;
      else if (res == FLPERR_NOFILE) fprintf(stderr, "  file not found\n");
      else { fprintf(stderr, "  error!\n"); return -1; }
    }
    if (needwrite) {
      printf("flushing changes to '%s'...\n", dskImageFileName);
      FILE *fo = fopen(dskImageFileName, "wb");
      if (!fo) { fprintf(stderr, "cannot create file!\n"); return -1; }
      if (dskSaveDSK(flp, fo) != FLPERR_OK) {
        fclose(fo);
        unlink(dskImageFileName);
        fprintf(stderr, "cannot save disk image!\n");
        return -1;
      }
      fclose(fo);
    }
  } else if (command == CmdExtract) {
    if (argp >= argc) { fprintf(stderr, "files?\n"); return -1; }
    for (; argp < argc; ++argp) {
      const char *pdfname = argv[argp];
      P3DskFileInfo nfo;
      if (p3dskIsGlobMask(pdfname)) {
        printf("extracting files with glob '%s'...\n", pdfname);
        if (p3dskFindInit(&p3d, &nfo, pdfname) != 0) {
          fprintf(stderr, "cannot init finder with '%s'\n", pdfname);
          continue;
        }
        while (p3dskFindNext(&nfo) > 0) {
          printf("extracting '%s' (%u bytes)...\n", nfo.name, nfo.size);
          extractFile(&nfo);
        }
      } else {
        if (!p3dskIsValidFileName(pdfname)) fprintf(stderr, "SHIT!\n");
        if (p3dskOpenFile(&p3d, &nfo, pdfname) != FLPERR_OK) {
          fprintf(stderr, "cannot open +3DOS file '%s'\n", pdfname);
          continue;
        }
        printf("extracting file '%s' (%u bytes)...\n", nfo.name, nfo.size);
        extractFile(&nfo);
      }
    }
  } else if (command == CmdPut || command == CmdPutCode) {
    if (argp >= argc) { fprintf(stderr, "files?\n"); return -1; }
    int needupdate = 0;
    for (; argp < argc; ++argp) {
      const char *inname = argv[argp];
      printf("adding '%s'...\n", inname);
      // do it easy
      FILE *fl = fopen(inname, "rb");
      if (!fl) { fprintf(stderr, "cannot open file '%s'\n", inname); continue; }
      fseek(fl, 0, SEEK_END);
      long fsize = ftell(fl);
      fseek(fl, 0, SEEK_SET);
      if (fsize < 0 || fsize > 1024*1024) { fclose(fl); fprintf(stderr, "file '%s' is too big\n", inname); continue; }
      uint8_t *fdata = malloc(fsize+1);
      if (fsize) fread(fdata, (size_t)fsize, 1, fl);
      fclose(fl);
      // build cp/m 8.3 name
      const char *fne = strrchr(inname, '/');
      if (fne) inname = fne+1;
      #ifdef WIN32
      fne = strrchr(inname, '\\');
      if (fne) inname = fne+1;
      #endif
      if (!inname[0]) { fprintf(stderr, "shit!\n"); return -1; }
      char pdfname[32];
      snprintf(pdfname, sizeof(pdfname), "%s", inname);
      pdfname[8] = 0; // no extension yet
      char *pext = strchr(pdfname, '.');
      if (pext) *pext = 0;
      while (pdfname[0] && (pdfname[strlen(pdfname)-1]&0xffU) <= 32) pdfname[strlen(pdfname)-1] = 0;
      const char *fext = strchr(inname, '.');
      if (fext) {
        size_t plen = strlen(pdfname);
        for (int f = 0; f < 4; ++f) {
          if (!fext[0]) break;
          pdfname[plen++] = *fext++;
        }
        pdfname[plen] = 0;
      }
      while (pdfname[0] && (pdfname[strlen(pdfname)-1]&0xffU) <= 32) pdfname[strlen(pdfname)-1] = 0;
      if (!pdfname[0]) { fprintf(stderr, "cannot create +3DOS file name\n"); free(fdata); continue; }
      for (char *stmp = pdfname; *stmp; ++stmp) {
        if (*stmp >= 'a' && *stmp <= 'z') stmp[0] -= 32; // upcase
        if (*stmp == '.') continue;
             if (*stmp < 33 || *stmp > 127) *stmp = '_';
        else if (strchr("<>,;:=?*[]", *stmp)) *stmp = '_';
      }
      printf("  +3DOS name: '%s'\n", pdfname);
      int crerr = p3dskCreateFile(&p3d, pdfname);
      if (crerr < 0) {
        if (crerr == FLPERR_MANYFILES) { printf("NO FREE DIRECTORY ENTRIES (creating)!\n"); free(fdata); break; }
        if (crerr != FLPERR_FILEEXIST) { fprintf(stderr, "+3DOS disk error (creating)\n"); free(fdata); return -1; }
        p3dskDeleteFiles(&p3d, pdfname);
        crerr = p3dskCreateFile(&p3d, pdfname);
        if (crerr < 0) { fprintf(stderr, "+3DOS disk error (creating1)\n"); free(fdata); return -1; }
      }
      P3DskFileInfo nfo;
      if (p3dskOpenFile(&p3d, &nfo, pdfname) != FLPERR_OK) { fprintf(stderr, "cannot open created +3DOS file\n"); free(fdata); return -1; }
      if (fsize) {
        int wrres = p3dskWriteFile(&nfo, fdata, (command == CmdPut ? 0 : 128), (int)fsize);
        free(fdata);
        if (wrres == FLPERR_MANYFILES) { printf("NO FREE DIRECTORY ENTRIES (writing)!\n"); return -1; }
        if (wrres == FLPERR_NOSPACE) { printf("NO FREE SPACE (writing)!\n"); return -1; }
        if (wrres < 0) { fprintf(stderr, "+3DOS disk writing error (writing)\n"); return -1; }
      } else {
        free(fdata);
      }
      // write +3DOS header, if necessary
      if (command == CmdPutCode && fsize <= 65535) {
        P3DskFileHeader hdr;
        memset(&hdr, 0, sizeof(P3DskFileHeader));
        hdr.valid = 1;
        hdr.issue = 1;
        hdr.version = 0;
        hdr.filesize = (uint32_t)(fsize+128);
        hdr.bastype = 3; // code
        hdr.baslength = (uint16_t)fsize;
        if (fsize == 6144 || fsize == 6912) {
          hdr.basaddr = 16384;
        } else {
          hdr.basaddr = 0x6000; // arbitrary
        }
        if (p3dskWriteFileHeader(&nfo, &hdr) != FLPERR_OK) { fprintf(stderr, "error writing +3DOS file header\n"); return -1; }
      }
      needupdate = 1;
    }
    if (needupdate) {
      printf("flushing changes to '%s'...\n", dskImageFileName);
      FILE *fo = fopen(dskImageFileName, "wb");
      if (!fo) { fprintf(stderr, "cannot create file!\n"); return -1; }
      if (dskSaveDSK(flp, fo) != FLPERR_OK) {
        fclose(fo);
        unlink(dskImageFileName);
        fprintf(stderr, "cannot save disk image!\n");
        return -1;
      }
      fclose(fo);
    }
  } else if (command == CmdExtractAll || command == CmdRawExtract || command == CmdDir) {
    P3DskFileInfo nfo;
    if (p3dskFindInit(&p3d, &nfo, "*.*") != 0) { fprintf(stderr, "cannot init finder\n"); return -1; }
    while (p3dskFindNext(&nfo) > 0) {
      printf("%s.%s  size=%u; ext=%u; name=<%s>; r/o=%u; sys=%u; arc=%u\n", nfo.nameonly, nfo.extonly, nfo.size, nfo.firstextent,
        nfo.name, nfo.readonly, nfo.system, nfo.archive);
      P3DskFileHeader p3hdr;
      if (p3dskReadFileHeader(&nfo, &p3hdr) == FLPERR_OK) {
        if (!p3hdr.valid) abort(); // assertion
        char bbtmp[8];
        const char *btpstr;
        switch (p3hdr.bastype) {
          case 0: btpstr = "Program"; break;
          case 1: btpstr = "Numeric array"; break;
          case 2: btpstr = "Character array"; break;
          case 3: btpstr = "Code"; break;
          default: snprintf(bbtmp, sizeof(bbtmp), "%u", p3hdr.bastype); btpstr = bbtmp; break;
        }
        printf("    hdrv%d.%d; fsize=%u; bastype=%s; baslength=%u; basaddr=%u; basvarsofs=%u\n",
          p3hdr.version, p3hdr.issue, p3hdr.filesize,
          btpstr, p3hdr.baslength, p3hdr.basaddr, p3hdr.basvarsofs);
      }
      // extract file
      //p3dskGlobMatch(nfo.name, "readme.txt", P3DSK_GLOB_DEFAULT)
      if (command == CmdRawExtract) {
        int bblen = p3dskFindCalcBlockbufLength(&nfo);
        if (bblen < 0) abort();
        uint16_t *blist = malloc(bblen*sizeof(uint16_t));
        if (p3dskGetBlockbuf(&nfo, blist) < 0) abort();
        uint8_t *bbuf = malloc(p3d.geom.blocksize);
        size_t left = nfo.size;
        int bidx = 0;
        char fnstr[64];
        snprintf(fnstr, sizeof(fnstr), "_unpack/%s", nfo.name);
        FILE *fo = fopen(fnstr, "wb");
        if (fo) {
          while (left && bidx < bblen) {
            if (p3dskReadBlock(&p3d, blist[bidx], bbuf) < 0) abort();
            size_t wr = (left < p3d.geom.blocksize ? left : p3d.geom.blocksize);
            fwrite(bbuf, wr, 1, fo);
            left -= wr;
            ++bidx;
          }
          fclose(fo);
        }
        free(bbuf);
        free(blist);
      } else if (command == CmdExtractAll) {
        extractFile(&nfo);
      }
    }
  }


  /*
  int tnum = 0;
  int snum = p3d.geom.firstsector;

  uint8_t *sdata = flpGetSectorDataPtr(flp, tnum, snum);
  if (!sdata) { fprintf(stderr, "shit off\n"); abort(); }
  uint16_t ssize = flpGetSectorSize(flp, tnum, snum);
  printf("writing %u bytes\n", ssize);

  FILE *fo = fopen("zfuck.bin", "wb");
  fwrite(sdata, ssize, 1, fo);
  fclose(fo);
  */

  flpDestroy(flp);
  return 0;
}
