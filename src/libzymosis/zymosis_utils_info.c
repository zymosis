/*
 * Z80 CPU emulation engine v0.1.2.1
 * coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
#include <stdlib.h>
#include <string.h>

#include "zymosis_utils.h"

#if defined(__GNUC__)
# ifndef ZYMOSIS_INLINE
#  define ZYMOSIS_INLINE  static __attribute__((always_inline)) inline
# endif
# ifndef ZYMOSIS_PURE
#  define ZYMOSIS_PURE  __attribute__((pure))
# endif
# ifndef ZYMOSIS_CONST
#  define ZYMOSIS_CONST  __attribute__((const))
# endif
# ifndef ZYMOSIS_FUNC
#  define ZYMOSIS_FUNC  __attribute__((warn_unused_result))
# endif
#endif

#ifndef ZYMOSIS_INLINE
# define ZYMOSIS_INLINE  static
#endif
#ifndef ZYMOSIS_PURE
# define ZYMOSIS_PURE
#endif
#ifndef ZYMOSIS_CONST
# define ZYMOSIS_CONST
#endif
#ifndef ZYMOSIS_FUNC
# define ZYMOSIS_FUNC
#endif


/******************************************************************************/
/* very simple instruction info extractor */
/******************************************************************************/
#define Z80NFO_PC_NEXT  pc = (pc+1)&0xffff

#define Z80NFO_OPI_WPC  do { \
  tmpW = z80->mem_read(z80, pc, ZYM_MEMIO_OTHER)|(z80->mem_read(z80, (pc+1)&0xffff, ZYM_MEMIO_OTHER)<<8); \
  pc = (pc+2)&0xffff; \
} while (0)

#define CBX_REPEATED  (opcode&0x10u)


void zym_op_meminfo (zym_cpu_t *z80, zym_op_meminfo_t *nfo, uint16_t pc) {
  uint8_t opcode;
  uint16_t tmpW;
  uint16_t orgpc = pc;
  int ixy = -1, disp = 0, gotDD = 0; /* 0: ix; 1: iy */
  /***/
  nfo->inslen = 0;
  nfo->memrwword = 0;
  nfo->memread = nfo->memwrite = ZYM_MEMIO_NONE;
  nfo->jump = ZYM_JUMP_NONE;
  nfo->cond = ZYM_COND_NONE;
  nfo->portread = nfo->portwrite = ZYM_PORTIO_NONE;
  nfo->push = nfo->pop = ZYM_STK_NONE;
  nfo->disp = 0xffff;
  nfo->trap = -1;
  nfo->prefix = 0;
  /***/
  opcode = z80->mem_read(z80, pc, ZYM_MEMIO_OTHER);
  Z80NFO_PC_NEXT;
  /***/
  if (opcode == 0xdd || opcode == 0xfd) {
    /* `static` is more restrective here, simple `const` should generate better code */
    /*static*/ const uint32_t withIndexBmp[8] = {0x00,0x700000,0x40404040,0x40bf4040,0x40404040,0x40404040,0x0800,0x00};
    /* IX/IY prefix */
    ixy = (opcode == 0xfd);
    opcode = z80->mem_read(z80, pc, ZYM_MEMIO_OTHER);
    Z80NFO_PC_NEXT;
    if (withIndexBmp[opcode>>5]&(1<<(opcode&0x1f))) {
      nfo->prefix = (ixy ? ZYM_PFX_IY : ZYM_PFX_IX);
      /* 3rd byte is always DISP here */
      disp = z80->mem_read(z80, pc, ZYM_MEMIO_OTHER);
      if (disp > 127) disp -= 256; /* convert to int8_t */
      nfo->disp = disp;
      Z80NFO_PC_NEXT;
      nfo->memread = (ixy ? ZYM_MEMIO_IY : ZYM_MEMIO_IX);
    } else if (opcode == 0xdd || opcode == 0xfd) {
      /* double prefix */
      nfo->inslen = 1;
      return;
    }
    gotDD = 1;
  }
  /* instructions */
  if (opcode == 0xed) {
    nfo->prefix |= ZYM_PFX_ED;
    ixy = 0; /* ED-prefixed opcodes cannot use IX/IY */
    opcode = z80->mem_read(z80, pc, ZYM_MEMIO_OTHER);
    Z80NFO_PC_NEXT;
    switch (opcode) {
      /* LDI, LDIR, LDD, LDDR */
      case 0xa0: case 0xb0: case 0xa8: case 0xb8:
        nfo->memwrite = ZYM_MEMIO_DE;
        /* fallthrough */
      /* CPI, CPIR, CPD, CPDR */
      case 0xa1: case 0xb1: case 0xa9: case 0xb9:
        nfo->memread = ZYM_MEMIO_HL;
        if (CBX_REPEATED) { nfo->cond = ZYM_COND_BNZ; nfo->jump = orgpc; }
        break;
      /* INI, INIR, IND, INDR */
      case 0xa2: case 0xb2: case 0xaa: case 0xba:
      /* OUTI, OTIR, OUTD, OTDR */
      case 0xa3: case 0xb3: case 0xab: case 0xbb:
        if (opcode&0x01) nfo->portwrite = ZYM_PORTIO_BCM1; else nfo->portread = ZYM_PORTIO_BC;
        if (CBX_REPEATED) { nfo->cond = ZYM_COND_BNZ; nfo->jump = orgpc; }
        break;
      /* not strings, but some good instructions anyway */
      default: /* traps */
        if ((opcode&0xc0) == 0x40) {
          switch (opcode&0x07) {
            /* IN r8,(C) */
            case 0: nfo->portread = ZYM_PORTIO_BC; break;
            /* OUT (C),r8 */
            case 1: nfo->portwrite = ZYM_PORTIO_BC; break;
            /* SBC HL,rr/ADC HL,rr */
            /*case 2: break;*/
            /* LD (nn),rr/LD rr,(nn) */
            case 3:
              Z80NFO_OPI_WPC;
              if (opcode&0x08) nfo->memread = tmpW; else nfo->memwrite = tmpW;
              nfo->memrwword = 1;
              break;
            /* NEG */
            /*case 4: break;*/
            /* RETI/RETN */
            case 5:
              nfo->jump = ZYM_JUMP_RET;
              nfo->pop = ZYM_STK_PC;
              nfo->cond = (opcode&0x08 ? ZYM_COND_RETI : ZYM_COND_RETN);
              nfo->memread = ZYM_MEMIO_SP;
              nfo->memrwword = 1;
              break;
            /* IM n */
            /*case 6: break;*/
            /* specials */
            case 7:
              switch (opcode) {
                /* LD I,A */
                /*case 0x47: break;*/
                /* LD R,A */
                /*case 0x4f: break;*/
                /* LD A,I */
                /*case 0x57: break;*/
                /* LD A,R */
                /*case break;*/
                /* RRD */
                case 0x67:
                /* RLD */
                case 0x6F:
                  nfo->memread = nfo->memwrite = ZYM_MEMIO_HL;
                  break;
              }
          }
        } else {
          nfo->trap = opcode;
        }
        break;
    }
    /* 0xed done */
  } else if (opcode == 0xcb) {
    /* shifts and bit operations */
    nfo->prefix |= ZYM_PFX_CB;
    opcode = z80->mem_read(z80, pc, ZYM_MEMIO_OTHER);
    Z80NFO_PC_NEXT;
    if (!gotDD && (opcode&0x07) == 6) nfo->memread = nfo->memwrite = ZYM_MEMIO_HL;
    if ((opcode&0xc0) != 0x40) {
      if (gotDD) nfo->memwrite = nfo->memread; /* all except BIT writes back */
    } else {
      nfo->memwrite = ZYM_MEMIO_NONE;
    }
    /* 0xcb done */
  } else {
    /* normal things */
    switch (opcode&0xc0) {
      /* 0x00..0x3F */
      case 0x00:
        switch (opcode&0x07) {
          /* misc,DJNZ,JR,JR cc */
          case 0:
            if (opcode&0x30) {
              /* branches */
              if (opcode&0x20) nfo->cond = ((opcode>>3)&0x03); /* JR cc */
              else if ((opcode&0x08) == 0) nfo->cond = ZYM_COND_BNZ; /* DJNZ ; else -- JR */
              disp = z80->mem_read(z80, pc, ZYM_MEMIO_OTHER);
              if (disp > 127) disp -= 256; /* convert to int8_t */
              Z80NFO_PC_NEXT;
              nfo->jump = ((int32_t)pc+disp)&0xffff;
            } /* else EX AF,AF' or NOP */
            break;
          /* LD rr,nn/ADD HL,rr */
          case 1:
            if (!(opcode&0x08)) pc = (pc+2)&0xffff;
            break;
          /* LD xxx,xxx */
          case 2:
            switch ((opcode>>3)&0x07) {
              /* LD (BC),A */
              case 0: nfo->memwrite = ZYM_MEMIO_BC; break;
              /* LD A,(BC) */
              case 1: nfo->memread = ZYM_MEMIO_BC; break;
              /* LD (DE),A */
              case 2: nfo->memwrite = ZYM_MEMIO_DE; break;
              /* LD A,(DE) */
              case 3: nfo->memread = ZYM_MEMIO_DE; break;
              /* LD (nn),HL */
              case 4:
                Z80NFO_OPI_WPC;
                nfo->memwrite = tmpW;
                nfo->memrwword = 1;
                break;
              /* LD HL,(nn) */
              case 5:
                Z80NFO_OPI_WPC;
                nfo->memread = tmpW;
                nfo->memrwword = 1;
                break;
              /* LD (nn),A */
              case 6:
                Z80NFO_OPI_WPC;
                nfo->memwrite = tmpW;
                break;
              /* LD A,(nn) */
              case 7:
                Z80NFO_OPI_WPC;
                nfo->memread = tmpW;
                break;
            }
            break;
          /* INC rr/DEC rr */
          /*case 3: break;*/
          /* INC r8 */
          case 4:
          /* DEC r8 */
          case 5:
            if (((opcode>>3)&0x07) == 6) {
              /* (HL) or (IXY+n) */
              if (gotDD) nfo->memwrite = nfo->memread;
              else nfo->memwrite = nfo->memread = ZYM_MEMIO_HL;
            }
            break;
          /* LD r8,n */
          case 6:
            Z80NFO_PC_NEXT;
            if (((opcode>>3)&0x07) == 6) {
              if (!gotDD) nfo->memwrite = ZYM_MEMIO_HL;
              else { nfo->memwrite = nfo->memread; nfo->memread = ZYM_MEMIO_NONE; }
            }
            break;
          /* swim-swim-hungry */
          /*case 7: break;*/
        }
        break;
      /* 0x40..0x7F (LD r8,r8) */
      case 0x40:
        if (opcode != 0x76) {
          if (!gotDD && (opcode&0x07) == 6) nfo->memread = ZYM_MEMIO_HL;
          if (((opcode>>3)&0x07) == 6) { nfo->memwrite = (gotDD ? nfo->memread : ZYM_MEMIO_HL); nfo->memread = ZYM_MEMIO_NONE; }
        }
        break;
      /* 0x80..0xBF (ALU A,r8) */
      case 0x80:
        if (!gotDD && (opcode&0x07) == 6) nfo->memread = ZYM_MEMIO_HL;
        break;
      /* 0xC0..0xFF */
      case 0xC0:
        switch (opcode&0x07) {
          /* RET cc */
          case 0:
            nfo->jump = ZYM_JUMP_RET;
            nfo->pop = ZYM_STK_PC;
            nfo->cond = ((opcode>>3)&0x07);
            nfo->memread = ZYM_MEMIO_SP;
            nfo->memrwword = 1;
            break;
          /* POP rr/special0 */
          case 1:
            if (opcode&0x08) {
              /* special 0 */
              switch ((opcode>>4)&0x03) {
                /* RET */
                case 0:
                  nfo->jump = ZYM_JUMP_RET;
                  nfo->pop = ZYM_STK_PC;
                  nfo->memread = ZYM_MEMIO_SP;
                  nfo->memrwword = 1;
                  break;
                /* EXX */
                /*case 1: break;*/
                /* JP (HL) */
                case 2:
                  nfo->jump = (ixy < 0 ? ZYM_JUMP_HL : (ixy ? ZYM_JUMP_IY : ZYM_JUMP_IX));
                  break;
                /* LD SP,HL */
                /*case 3: break;*/
              }
            } else {
              /* POP rr */
              nfo->memread = ZYM_MEMIO_SP;
              nfo->memrwword = 1;
              switch ((opcode>>4)&0x03) {
                case 0: nfo->pop = ZYM_STK_BC; break;
                case 1: nfo->pop = ZYM_STK_DE; break;
                case 2: nfo->pop = (ixy < 0 ? ZYM_STK_HL : (ixy ? ZYM_STK_IY : ZYM_STK_IX)); break;
                case 3: nfo->pop = ZYM_STK_AF; break;
              }
            }
            break;
          /* JP cc,nn */
          case 2:
            Z80NFO_OPI_WPC;
            nfo->jump = tmpW;
            nfo->cond = ((opcode>>3)&0x07);
            break;
          /* special1/special3 */
          case 3:
            switch ((opcode>>3)&0x07) {
              /* JP nn */
              case 0:
                Z80NFO_OPI_WPC;
                nfo->jump = tmpW;
                break;
              /* OUT (n),A */
              case 2:
                nfo->portwrite = z80->mem_read(z80, pc, ZYM_MEMIO_OTHER);
                Z80NFO_PC_NEXT;
                break;
              /* IN A,(n) */
              case 3:
                nfo->portread = z80->mem_read(z80, pc, ZYM_MEMIO_OTHER);
                Z80NFO_PC_NEXT;
                break;
              /* EX (SP),HL */
              case 4:
                nfo->memread = nfo->memwrite = ZYM_MEMIO_SP;
                nfo->memrwword = 1;
                break;
              /* EX DE,HL */
              /*case 5: break;*/
              /* DI */
              /*case 6: break;*/
              /* EI */
              /*case 7: break;*/
            }
            break;
          /* CALL cc,nn */
          case 4:
            Z80NFO_OPI_WPC;
            nfo->jump = tmpW;
            nfo->push = ZYM_STK_PC;
            nfo->cond = ((opcode>>3)&0x07);
            nfo->memwrite = ZYM_MEMIO_SP;
            nfo->memrwword = 1;
            break;
          /* PUSH rr/special2 */
          case 5:
            if (opcode&0x08) {
              if (((opcode>>4)&0x03) == 0) {
                /* CALL */
                Z80NFO_OPI_WPC;
                nfo->jump = tmpW;
                nfo->push = ZYM_STK_PC;
                nfo->memwrite = ZYM_MEMIO_SP;
                nfo->memrwword = 1;
              }
            } else {
              /* PUSH rr */
              nfo->memwrite = ZYM_MEMIO_SP;
              nfo->memrwword = 1;
              switch ((opcode>>4)&0x03) {
                case 0: nfo->push = ZYM_STK_BC; break;
                case 1: nfo->push = ZYM_STK_DE; break;
                case 2: nfo->push = (ixy >= 0 ? (ixy ? ZYM_STK_IY : ZYM_STK_IX) : ZYM_STK_HL); break;
                case 3: nfo->push = ZYM_STK_AF; break;
              }
            }
            break;
          /* ALU A,n */
          case 6:
            Z80NFO_PC_NEXT;
            break;
          /* RST nnn */
          case 7:
            nfo->jump = (opcode&0x38);
            nfo->push = ZYM_STK_PC;
            nfo->memwrite = ZYM_MEMIO_SP;
            nfo->memrwword = 1;
            break;
        }
        break;
    }
  }
  nfo->inslen = (pc >= orgpc ? pc-orgpc : (uint32_t)pc+0x10000-orgpc);
}
