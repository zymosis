/*
 * Zymosis: Z80 CPU emulation engine v0.1.3.0
 * coded by Ketmar // Invisible Vector (ketmar@ketmar.no-ip.org)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
#ifndef ZYMOSIS_H
#define ZYMOSIS_H

/* WARNING! it is better to use the following flags with GCC: */
/* -Wno-misleading-indentation */
/* -fwrapv */
/* -fno-aggressive-loop-optimizations */
/* -fno-delete-null-pointer-checks */
/* -fno-strict-aliasing */

/* see the end of "zymosis.c" to understand how Zymosis does instruction decoding. */
/* i believe that this is mostly how real Z80 does it too. */

/* you can define either ZYMOSIS_LITTLE_ENDIAN or ZYMOSIS_BIG_ENDIAN, or */
/* you can leave it undefined for Zymosis to decide. */

/*
define this for FUSE test suite (i.e. you don't need it).
FUSE tests expect contention at the very first machine cycle,
and memory i/o after the very last. i don't know why they did it like that.
FUSE also doesn't read JR displacement on failed condition, contrary to the manual.
*/
/*#define ZYMOSIS_FUSE_TEST*/

/* define this to avoid *SOME* UB in the code (and making the code slower). */
/* this should not be required for a sane C compiler, */
/* but we don't have enough sane C compilers. */
/* GCC is definitely insane, for example. */
/* #define ZYMOSIS_TRY_TO_AVOID_UB */


/* the following defines are not intended to be user-serviceable */
#if !defined(ZYMOSIS_LITTLE_ENDIAN) && !defined(ZYMOSIS_BIG_ENDIAN)
# if defined(__GNUC__)
#  if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#   define ZYMOSIS_LITTLE_ENDIAN
#  elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#   define ZYMOSIS_BIG_ENDIAN
#  else
#   error Is this really PDP? C'mon, don't shit me!
#  endif
# else
#  error wtf?! Zymosis endiannes is not defined!
# endif
#endif

#if defined(ZYMOSIS_LITTLE_ENDIAN) && defined(ZYMOSIS_BIG_ENDIAN)
# error wtf?! Zymosis endiannes double defined! are you nuts?
#endif

#if defined(__GNUC__)
# ifndef ZYMOSIS_PACKED
#  define ZYMOSIS_PACKED  __attribute__((packed)) __attribute__((gcc_struct))
# endif
#endif

#ifndef ZYMOSIS_PACKED
# define ZYMOSIS_PACKED
#endif


#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* for convenience; please, *ALWAYS* use `zym_bool` for callback declaraions! */
typedef uint8_t zym_bool;

/* flag masks */
#define ZYM_FLAG_C  0x01u
#define ZYM_FLAG_N  0x02u
#define ZYM_FLAG_PV 0x04u
#define ZYM_FLAG_3  0x08u
#define ZYM_FLAG_H  0x10u
#define ZYM_FLAG_5  0x20u
#define ZYM_FLAG_Z  0x40u
#define ZYM_FLAG_S  0x80u

#define ZYM_FLAG_35  (ZYM_FLAG_3|ZYM_FLAG_5)
#define ZYM_FLAG_S35 (ZYM_FLAG_S|ZYM_FLAG_3|ZYM_FLAG_5)


typedef union ZYMOSIS_PACKED {
  uint16_t w;
#ifdef ZYMOSIS_LITTLE_ENDIAN
  struct ZYMOSIS_PACKED { uint8_t c, b; };
  struct ZYMOSIS_PACKED { uint8_t e, d; };
  struct ZYMOSIS_PACKED { uint8_t l, h; };
  struct ZYMOSIS_PACKED { uint8_t f, a; };
  struct ZYMOSIS_PACKED { uint8_t xl, xh; };
  struct ZYMOSIS_PACKED { uint8_t yl, yh; };
#else
  struct ZYMOSIS_PACKED { uint8_t b, c; };
  struct ZYMOSIS_PACKED { uint8_t d, e; };
  struct ZYMOSIS_PACKED { uint8_t h, l; };
  struct ZYMOSIS_PACKED { uint8_t a, f; };
  struct ZYMOSIS_PACKED { uint8_t xh, xl; };
  struct ZYMOSIS_PACKED { uint8_t yh, yl; };
#endif
} zym_regpair_t;


/* operation type for memory read and write callbacks */
/* reading opcode */
#define ZYM_MEMIO_OPCODE  0x00u
/* 'ext' opcode (after CB/ED/DD/FD prefix) */
#define ZYM_MEMIO_OPCEXT  0x01u
/* opcode in halted state; the actual PC is after the current opcode (HALT), but it points to HALT here */
#define ZYM_MEMIO_OPHLT   0x02u
/* opcode argument (jump destination, register value, etc) */
#define ZYM_MEMIO_OPCARG  0x03u
/* reading/writing data */
#define ZYM_MEMIO_DATA    0x04u
/* other 'internal' reads (for memptr, etc; don't do contention, breakpoints or so) */
#define ZYM_MEMIO_OTHER   0x05u
#define ZYM_MEMIO_MASK    0x0fu
typedef uint8_t zym_memio_t;

/* values for memory contention */
#define ZYM_MREQ_NONE  0x00u
#define ZYM_MREQ_WRITE 0x10u
#define ZYM_MREQ_READ  0x20u
#define ZYM_MREQ_MASK  0xf0u
typedef uint8_t zym_memreq_t;

/* operation type for port reading and writing callbacks */
/* normal call in Z80 execution loop */
#define ZYM_PORTIO_NORMAL   0x00u
/* call from a debugger or other place outside of Z80 execution loop */
#define ZYM_PORTIO_INTERNAL 0x01u
#define ZYM_PORTIO_MASK     0x0fu
typedef uint8_t zym_portio_t;

/* flags for port contention */
/* performing "in" if set */
#define ZYM_PORTIO_FLAG_IN    0x10u
/* "early" port contetion, if set */
#define ZYM_PORTIO_FLAG_EARLY 0x20u
typedef uint8_t zym_portio_flags_t;


typedef struct zym_cpu_s zym_cpu_t;

/*
this callback will be called when memory contention is necessary.
when it is called, `z80->tstates` is *BEFORE* T3 (so, it is never 0).
Zilog manual says that additional wait states are inserted between
T2 and T3, by activating /WAIT. as our `tstates` shows the current
tstate (i.e. for Z80 it is "next"), it is logically `T3` when
contention is applied.

advance tstates by the number of wait states inserted (it can be 0).

Zymosis will never call this CB for ZYM_MEMIO_OTHER memory accessing

mio: ZYM_MEMIO_xxx | ZYM_MREQ_xxx
*/
typedef void (*zym_memcontention_fn) (zym_cpu_t *z80, uint16_t addr,
                                      zym_memio_t mio, zym_memreq_t mreq);

/* this can be used as callback when no contention required */
void zym_no_mem_contention_cb (zym_cpu_t *z80, uint16_t addr,
                               zym_memio_t mio, zym_memreq_t mreq);

/* will be called when port contention is necessary */
/* must increase z80->tstates to at least 'tstates' arg */
/* pio: can contain only ZYM_PORTIO_FLAG_xxx flags */
/* `tstates` is always 1 when ZYM_PORTIO_FLAG_EARLY is set and 2 otherwise */
typedef void (*zym_portcontention_fn) (zym_cpu_t *z80, uint16_t port, int tstates,
                                       zym_portio_flags_t pflags);

/* miot: only ZYM_MEMIO_xxx, no need in masking */
typedef uint8_t (*zym_memread_fn) (zym_cpu_t *z80, uint16_t addr, zym_memio_t miot);
typedef void (*zym_memwrite_fn) (zym_cpu_t *z80, uint16_t addr, uint8_t value,
                                 zym_memio_t miot);

/* pio: only ZYM_PORTIO_xxx, no need in masking */
typedef uint8_t (*zym_portread_fn) (zym_cpu_t *z80, uint16_t port, zym_portio_t pio);
typedef void (*zym_portwrite_fn) (zym_cpu_t *z80, uint16_t port, uint8_t value,
                                  zym_portio_t pio);

/* return !0 to exit immediately */
typedef zym_bool (*zym_trap_fn) (zym_cpu_t *z80, uint8_t trap_code);

/*
this is called on "HALT". return !0 to exit the loop.
PC is after the "HALT".
*/
typedef zym_bool (*zym_trap_halt) (zym_cpu_t *z80);


/* `dd` pointer will be set in execution loop;
 * you can use it in port/memory i/o callbacks if you want to (but why?) */
struct zym_cpu_s {
  /* registers */
  zym_regpair_t bc, de, hl, af, sp, ix, iy;
  /* alternate registers */
  zym_regpair_t bcx, dex, hlx, afx;
  zym_regpair_t memptr;
  zym_regpair_t *dd; /* pointer to current HL/IX/IY (inside this struct) for the current command */
  uint16_t pc; /* program counter */
  uint16_t prev_pc; /* first byte of the previous command (prvious `org_pc`) */
  uint16_t org_pc; /* first byte of the current command */
  uint8_t regI; /* Z80 I register */
  uint8_t regR; /* Z80 R register (automatically updated on execution) */
  uint8_t im; /* IM (interrupt mode) (0-2) */
  zym_bool iff1, iff2; /* interrupt flip-flop flags */
  zym_bool evenM1; /* emulate 128K/Scorpion M1 contention? */
  zym_bool cmos; /* emulate CMOS Z80 models? (default is NMOS, closer to the original in undocumented features) */
  zym_bool halted; /* is CPU halted? main progam must manually reset this flag when it's appropriate */
  int8_t prev_was_EIDDR; /* 1: previous instruction was EI/FD/DD? (they blocks /INT); -1: prev vas LD A,I or LD A,R */
                         /* Zymosis will reset this flag only if it executed at least one instruction */
                         /* (or interrupt request API was called, and the value is negative) */
  int32_t tstates; /* t-states passed; incremented by executor; set to 0 by `zym_reset()` */
  int32_t next_event_tstate; /* zym_exec() will exit when `tstates>=next_event_tstate`; set to -1 to ignore */
  /* memory i/o callbacks; not optional */
  zym_memread_fn mem_read;
  zym_memwrite_fn mem_write;
  /* memory contention callback, optional */
  zym_memcontention_fn mem_contention; /* CANNOT be NULL; use `zym_no_mem_contention_cb` for "no contention" */
  /* port i/o callbacks; not optional */
  /* port i/o functions should add 4 t-states by themselves */
  int32_t port_read_tstates; /* Zymosis perform `port_read()` call after all contentions; */
                             /* this is *real* TS when the port should be read; always <= tstates) */
  zym_portread_fn port_read; /* in: +3; do read; +1 */
  zym_portwrite_fn port_write; /* out: +1; do out; +3 */
  /* port contention callback, optional */
  zym_portcontention_fn port_contention; /* can be NULL */
  /* all trap callbacks are optional */
  /* HALT trap */
  /* return !0 to exit the loop */
  zym_trap_halt trap_halt;
  /* RETI/RETN traps; called with opcode, *AFTER* iff changed and return address set; */
  /* return !0 to stop execution (tstates won't be rolled back!) */
  /* note that on calling this, Zymosis already spent 8ts on reading ED prefix */
  zym_trap_fn trap_reti;
  zym_trap_fn trap_retn;
  /* * */
  /* called when invalid ED command found */
  /* PC points to the next byte after ED (trap code) */
  /* return !0 to stop execution (tstates won't be rolled back!) */
  /* note that on calling this, Zymosis will rollback tstates (i.e. trap cost is 0) */
  /* contention function may be called (because the read was done), but tstate counter */
  /* will be rolled back to the value before fetching ED */
  /* trap_code=0xFB: */
  /*   .SLT trap */
  /*    HL: address to load; */
  /*    A: A --> level number */
  /*    return: CARRY complemented --> error */
  zym_trap_fn trap_edfb; /* can be NULL */
  /* ED FE trap, specific to ZXEmuT */
  /* `tstates` will be rewound as if trap command wasn't read */
  /* `trap_ts` will be set to tstates executed in the current loop (after rewinding) */
  int32_t trap_ts; /* set before ED trap, to the number of states executed in loop */
  zym_trap_fn trap_edfe; /* can be NULL */
  /*
   * Debugger can set this flag to true in memRead/memWrite/portRead/portWrite/etc.
   * to stop emulation loop. If memRead was called with MemIO.OPCODE, CPI's PC will not be
   * changed, all tstate changes will be rolled back (to compensate possible contention), and
   * no instruction will be executed.
   * Zymosis will automatically reset `bp_hit` flag and set `bp_was_hit` flag before interrupting
   * the execution loop.
   * Zymosis will never reset `bp_was_hit` flag (so take care of this).
   * Althrough both flags are "boolean", they can hold any value you like, only 0 is "false".
   */
  zym_bool bp_hit;
  zym_bool bp_was_hit;
  /*
   * `mem_read()` can set this flag to refetch current command (due to paging, for example).
   * It's effective only for ZYM_MEMIO_OPCODE.
   * All tstate changes will be rolled back (to compensate possible contention).
   * I.e. set `refetch` to `1` to pretend that no reads were happened.
   * This flag will be automatically reset by CPU execution loop *before* fetching the new opcode.
   * I.e. you cannot "set it to be used later".
   */
  zym_bool refetch;
  /* SCF/CCF emulation */
  uint8_t flags_q; /* presumably, internal register where Z80 assembles the new */
                   /* content of the F register, before moving it back to F */
  /* *** */
  /* arbitrary user data. Zymosis will never touch this. */
  void *user;
};


/******************************************************************************/
/* `Z80InitTables()` should be called before anyting else! */
/* it is safe to call it several times. while it will be automatically */
/* called by `zym_reset()`, you may need to manually call it in case of */
/* having several threads all running their own Z80 CPUs (as the tables */
/* are shared between all CPUs). */
/* WARNING! this is NOT thread-safe! call it once at program startup, */
/* before starting other threads! */
/* if you are using GCC with `constructor` attribute support, this */
/* will be automatically called on program startup. */
void zym_init_tables (void);

/* clear all flags and callbacks; will reset (set to NULL) 'z80->user' too. */
/* use this to init newly allocated (or static) CPU instance. */
void zym_init (zym_cpu_t *z80);

/* should be called on new instance of zym_cpu_t to clear all callbacks */
void zym_clear_callbacks (zym_cpu_t *z80);

/* reset emulated cpu; set registers to some predefined values, and zeroes `tstates` */
void zym_reset (zym_cpu_t *z80);

/* execute emulated cpu instructions until `z80->tstates` reaches */
/* `z80->next_event_tstate`, or at least `tscount` states passed. */
/* passing `-1` in `tscount` means "no stop counter". if next event */
/* is not set too, you'll have the infinite loop. */
/* returns number of tstates passed (on this call). */
int32_t zym_exec_ex (zym_cpu_t *z80, int32_t tscount);

/* execute emulated cpu instructions until tstates reaches next_event_tstate */
#define zym_exec(z80_)  zym_exec_ex((z80_), -1)

/* execute one instruction, returns number of ticks taken; ignore next_event_tstate */
int32_t zym_exec_step (zym_cpu_t *z80);

/* result!=0: interrupt was accepted (returns # of t-states eaten) */
/* changes z80->tstates if interrupt occurs */
int32_t zym_intr (zym_cpu_t *z80);

/* result!=0: interrupt was accepted (returns # of t-states eaten) */
/* changes z80->tstates if interrupt occurs */
int32_t zym_nmi (zym_cpu_t *z80);

/* without contention, using ZYM_MEMIO_OTHER, doesn't affect MEMPTR */
uint16_t zym_pop (zym_cpu_t *z80);

/* without contention, using ZYM_MEMIO_OTHER, doesn't affect MEMPTR */
void zym_push (zym_cpu_t *z80, const uint16_t value);

/* swap normal and alternate register sets (except AF/AF') */
void zym_exx (zym_cpu_t *z80);

/* swap normal and alternate AF */
void zym_exaf (zym_cpu_t *z80);


#ifdef __cplusplus
}
#endif
#endif
