/*
 * Z80 CPU emulation engine v0.1.2.1
 * coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
#include <stdlib.h>
#include <string.h>

#include "zymosis_utils.h"

#if defined(__GNUC__)
# ifndef ZYMDIS_INLINE
#  define ZYMDIS_INLINE  /*inline*/
# endif
# ifndef ZYMDIS_FORCE_INLINE
#  define ZYMDIS_FORCE_INLINE  __attribute__((always_inline)) inline
# endif
# ifndef ZYMDIS_PURE
#  define ZYMDIS_PURE  __attribute__((pure))
# endif
# ifndef ZYMDIS_CONST
#  define ZYMDIS_CONST  __attribute__((const))
# endif
# ifndef ZYMDIS_FUNC
#  define ZYMDIS_FUNC  __attribute__((warn_unused_result))
# endif
#endif

#ifndef ZYMDIS_INLINE
# define ZYMDIS_INLINE
#endif
#ifndef ZYMDIS_FORCE_INLINE
# define ZYMDIS_FORCE_INLINE
#endif
#ifndef ZYMDIS_PURE
# define ZYMDIS_PURE
#endif
#ifndef ZYMDIS_CONST
# define ZYMDIS_CONST
#endif
#ifndef ZYMDIS_FUNC
# define ZYMDIS_FUNC
#endif


zym_disasm_getlabel_fn zym_disasm_getlabel = NULL;

const char *zym_disasm_mnemo_end = NULL;
const char *zym_disasm_num_start = NULL;
const char *zym_disasm_num_end = NULL;


/******************************************************************************/
/* very simple disassembler */
/******************************************************************************/
typedef struct {
  zym_cpu_t *z80;
  const uint8_t *mem;
  zym_disop_t *nfo;
  uint16_t stpc;
  uint16_t pc;
  unsigned mempos;
  uint32_t dbpos;
  uint32_t dbstpos;
  uint8_t curarg;
  int argstarted;
  zym_bool gotDD;
  const char *DD;
  int disp;
  int tst[2]; // 0: normal/not taken; 1: add for taken
} ZymDisWorkArea;


static uint8_t zym_dis_read_byte (ZymDisWorkArea *wkp) {
  uint8_t res;
  if (wkp->z80) {
    res = wkp->z80->mem_read(wkp->z80, wkp->pc, ZYM_MEMIO_OTHER);
  } else {
    res = wkp->mem[wkp->mempos++];
  }
  wkp->pc = (wkp->pc+1)&0xffff;
  return res;
}


static ZYMDIS_INLINE uint16_t zym_dis_read_word (ZymDisWorkArea *wkp) {
  uint16_t tmptmpb0 = zym_dis_read_byte(wkp);
  uint16_t tmptmpb1 = zym_dis_read_byte(wkp);
  return tmptmpb0|(tmptmpb1<<8);
}


static ZYMDIS_FORCE_INLINE void zym_dis_putc (ZymDisWorkArea *wkp, const char ch) {
  if (wkp->dbpos+2 < sizeof(wkp->nfo->disbuf)) wkp->nfo->disbuf[wkp->dbpos++] = ch;
}


static void zym_dis_put_str (ZymDisWorkArea *wkp, const char *s, const unsigned lomask) {
  if (!s) return;
  while (*s) {
    char ch = *s++;
    if (!ch) return;
    if (wkp->nfo->flags&lomask) {
      if (ch >= 'A' && ch <= 'Z') ch = ch-'A'+'a';
    } else {
      if (ch >= 'a' && ch <= 'z') ch = ch-'a'+'A';
    }
    zym_dis_putc(wkp, ch);
  }
}


static void zym_dis_put_str_as_is (ZymDisWorkArea *wkp, const char *s) {
  if (!s) return;
  while (*s) {
    const char ch = *s++;
    if (!ch) return;
    zym_dis_putc(wkp, ch);
  }
}


static ZYMDIS_INLINE void zym_dis_end_mnemo (ZymDisWorkArea *wkp) {
  /*if (nfo->mnemo.length) assert(0, "internal error");*/
  zym_dis_put_str_as_is(wkp, (zym_disasm_mnemo_end ? zym_disasm_mnemo_end : "\t"));
  wkp->nfo->mnemo = wkp->nfo->disbuf;
  wkp->nfo->mnemolen = wkp->dbpos;
  wkp->dbstpos = wkp->dbpos;
}


static ZYMDIS_INLINE void zym_dis_end_arg (ZymDisWorkArea *wkp) {
  /*if (curarg >= nfo->args.length) assert(0, "internal error");*/
  wkp->nfo->args[wkp->curarg] = wkp->nfo->disbuf+wkp->dbstpos;
  wkp->nfo->argslen[wkp->curarg] = (int)(wkp->dbpos-wkp->dbstpos);
  ++wkp->curarg;
  wkp->dbstpos = wkp->dbpos;
  wkp->argstarted = 0;
}


static ZYMDIS_FORCE_INLINE void zym_dis_start_arg (ZymDisWorkArea *wkp) {
  if (!wkp->argstarted && wkp->curarg) zym_dis_put_str_as_is(wkp, ",");
  wkp->argstarted = 1;
}


static ZYMDIS_FORCE_INLINE void zym_dis_put_mnemo (ZymDisWorkArea *wkp, const char *s) {
  zym_dis_put_str(wkp, s, ZYM_DIS_FLAG_LOCASE_MNEMO);
  zym_dis_end_mnemo(wkp);
}


static ZYMDIS_INLINE void zym_dis_put_arg_part (ZymDisWorkArea *wkp, const char *s) {
  if (s && s[0]) {
    if (!wkp->argstarted && wkp->curarg) zym_dis_put_str_as_is(wkp, ",");
    wkp->argstarted = 1;
    zym_dis_put_str(wkp, s, ZYM_DIS_FLAG_LOCASE_ARGS);
  }
}


static ZYMDIS_FORCE_INLINE void zym_dis_put_arg (ZymDisWorkArea *wkp, const char *s) {
  zym_dis_put_arg_part(wkp, s);
  zym_dis_end_arg(wkp);
}


static void zym_dis_put_uint_dec (ZymDisWorkArea *wkp, int n) {
  uint32_t ntmp = (uint32_t)(n&0xffffU);
  char buf[16];
  unsigned bpos = (unsigned)sizeof(buf);
  buf[--bpos] = 0;
  do { buf[--bpos] = (char)('0'+ntmp%10); } while ((ntmp /= 10) != 0);
  if (zym_disasm_num_start) zym_dis_put_str_as_is(wkp, zym_disasm_num_start);
  zym_dis_put_str_as_is(wkp, buf+bpos);
  if (zym_disasm_num_end) zym_dis_put_str_as_is(wkp, zym_disasm_num_end);
}


static void zym_dis_put_uint_hex_len (ZymDisWorkArea *wkp, int n, int len) {
  uint32_t ntmp = (uint32_t)(n&0xffffU);
  int lentmp = len;
  char buf[17];
  unsigned bpos = (unsigned)sizeof(buf);
  if (lentmp > 12) lentmp = 12;
  buf[--bpos] = 0;
  do { buf[--bpos] = (char)('0'+ntmp%16+(ntmp%16 > 9 ? 7 : 0)); } while ((ntmp /= 16) != 0);
  while (sizeof(buf)-bpos < lentmp+1) buf[--bpos] = '0';
  if (wkp->nfo->flags&ZYM_DIS_FLAG_HEX_CLIKE) {
    buf[--bpos] = 'x';
    buf[--bpos] = '0';
  } else {
    buf[--bpos] = '#';
  }
  if (zym_disasm_num_start) zym_dis_put_str_as_is(wkp, zym_disasm_num_start);
  zym_dis_put_str(wkp, buf+bpos, ZYM_DIS_FLAG_LOCASE_HEX);
  if (zym_disasm_num_end) zym_dis_put_str_as_is(wkp, zym_disasm_num_end);
}


static ZYMDIS_FORCE_INLINE void zym_dis_put_uint_hexlen (ZymDisWorkArea *wkp, int n, int len) {
  if (wkp->nfo->flags&ZYM_DIS_FLAG_DECIMAL) {
    zym_dis_put_uint_dec(wkp, n);
  } else {
    zym_dis_put_uint_hex_len(wkp, n, len);
  }
}


static ZYMDIS_INLINE void zym_dis_put_uint_addr_pc (ZymDisWorkArea *wkp, int n, int len) {
  const char *lbl = (zym_disasm_getlabel ? zym_disasm_getlabel(n&0xffff, ZYM_DIS_ATYPE_PC_ADDR) : NULL);
       if (lbl) zym_dis_put_str_as_is(wkp, lbl);
  else if (wkp->nfo->flags&ZYM_DIS_FLAG_DECIMAL) zym_dis_put_uint_dec(wkp, n);
  else zym_dis_put_uint_hex_len(wkp, n, len);
}


static ZYMDIS_INLINE void zym_dis_put_uint_addr_data (ZymDisWorkArea *wkp, int n, int len) {
  const char *lbl = (zym_disasm_getlabel ? zym_disasm_getlabel(n&0xffff, ZYM_DIS_ATYPE_DATA_ADDR) : NULL);
       if (lbl) zym_dis_put_str_as_is(wkp, lbl);
  else if (wkp->nfo->flags&ZYM_DIS_FLAG_DECIMAL) zym_dis_put_uint_dec(wkp, n);
  else zym_dis_put_uint_hex_len(wkp, n, len);
}


static ZYMDIS_INLINE void zym_dis_put_uint_data_byte (ZymDisWorkArea *wkp, int n, int len) {
  const char *lbl = (zym_disasm_getlabel ? zym_disasm_getlabel(n&0xff, ZYM_DIS_ATYPE_BYTE) : NULL);
       if (lbl) zym_dis_put_str_as_is(wkp, lbl);
  else if (wkp->nfo->flags&ZYM_DIS_FLAG_DECIMAL) zym_dis_put_uint_dec(wkp, n);
  else zym_dis_put_uint_hex_len(wkp, n, len);
}


static ZYMDIS_INLINE void zym_dis_put_uint_data_word (ZymDisWorkArea *wkp, int n, int len) {
  const char *lbl = (zym_disasm_getlabel ? zym_disasm_getlabel(n&0xffff, ZYM_DIS_ATYPE_WORD) : NULL);
       if (lbl) zym_dis_put_str_as_is(wkp, lbl);
  else if (wkp->nfo->flags&ZYM_DIS_FLAG_DECIMAL) zym_dis_put_uint_dec(wkp, n);
  else zym_dis_put_uint_hex_len(wkp, n, len);
}


static ZYMDIS_INLINE void zym_dis_put_uint_port_byte (ZymDisWorkArea *wkp, int n, int len) {
  const char *lbl = (zym_disasm_getlabel ? zym_disasm_getlabel(n&0xff, ZYM_DIS_ATYPE_PORT8) : NULL);
       if (lbl) zym_dis_put_str_as_is(wkp, lbl);
  else if (wkp->nfo->flags&ZYM_DIS_FLAG_DECIMAL) zym_dis_put_uint_dec(wkp, n);
  else zym_dis_put_uint_hex_len(wkp, n, len);
}


static ZYMDIS_INLINE void zym_dis_put_disp (ZymDisWorkArea *wkp, int n) {
  const int nnn = n;
  if (nnn < 0) {
    const char *lbl = (zym_disasm_getlabel ? zym_disasm_getlabel((0x10000+n)&0xffff, ZYM_DIS_ATYPE_OFFSET) : NULL);
    if (lbl) {
      zym_dis_put_str_as_is(wkp, "+");
      zym_dis_put_str_as_is(wkp, lbl);
      return;
    }
    if (zym_disasm_num_start) zym_dis_put_str_as_is(wkp, zym_disasm_num_start);
    zym_dis_put_str_as_is(wkp, "-");
    if (zym_disasm_num_end) zym_dis_put_str_as_is(wkp, zym_disasm_num_end);
    zym_dis_put_uint_dec(wkp, -nnn);
  } else if (nnn > 0) {
    const char *lbl = (zym_disasm_getlabel ? zym_disasm_getlabel(n&0xffff, ZYM_DIS_ATYPE_OFFSET) : NULL);
    if (lbl) {
      zym_dis_put_str_as_is(wkp, "+");
      zym_dis_put_str_as_is(wkp, lbl);
      return;
    }
    /*if (zym_disasm_num_start) zym_dis_put_str_as_is(wkp, zym_disasm_num_start);*/
    zym_dis_put_str_as_is(wkp, "+");
    /*if (zym_disasm_num_end) zym_dis_put_str_as_is(wkp, zym_disasm_num_end);*/
    zym_dis_put_uint_dec(wkp, nnn);
  }
}


static ZYMDIS_FORCE_INLINE void zym_dis_put_cc (ZymDisWorkArea *wkp, const unsigned n) {
  const char *conds[8] = {"NZ", "Z", "NC", "C", "PO", "PE", "P", "M"};
  zym_dis_put_arg(wkp, conds[n&0x07]);
}


static ZYMDIS_INLINE void zym_dis_put_r8 (ZymDisWorkArea *wkp, const char *hlspec, unsigned n) {
  switch (n) {
    case 0: zym_dis_put_arg(wkp, "B"); break;
    case 1: zym_dis_put_arg(wkp, "C"); break;
    case 2: zym_dis_put_arg(wkp, "D"); break;
    case 3: zym_dis_put_arg(wkp, "E"); break;
    case 4: zym_dis_put_arg(wkp, "H"); break;
    case 5: zym_dis_put_arg(wkp, "L"); break;
    case 6:
      if (hlspec && hlspec[0]) {
        zym_dis_put_arg(wkp, hlspec);
      } else {
        zym_dis_put_arg_part(wkp, "(");
        zym_dis_put_arg_part(wkp, wkp->DD);
        if (wkp->gotDD) zym_dis_put_disp(wkp, wkp->disp);
        zym_dis_put_arg(wkp, ")");
      }
      break;
    case 7: zym_dis_put_arg(wkp, "A"); break;
  }
}


static ZYMDIS_INLINE void zym_dis_put_r16sp (ZymDisWorkArea *wkp, const char *hlspec, unsigned n) {
  switch (n) {
    case 0: zym_dis_put_arg(wkp, "BC"); break;
    case 1: zym_dis_put_arg(wkp, "DE"); break;
    case 2: if (hlspec && hlspec[0]) zym_dis_put_arg(wkp, hlspec); else zym_dis_put_arg(wkp, wkp->DD); break;
    case 3: zym_dis_put_arg(wkp, "SP"); break;
  }
}


static ZYMDIS_INLINE void zym_dis_put_r16af (ZymDisWorkArea *wkp, unsigned n) {
  switch (n) {
    case 0: zym_dis_put_arg(wkp, "BC"); break;
    case 1: zym_dis_put_arg(wkp, "DE"); break;
    case 2: zym_dis_put_arg(wkp, wkp->DD); break;
    case 3: zym_dis_put_arg(wkp, "AF"); break;
  }
}


static ZYMDIS_INLINE void zym_finish (ZymDisWorkArea *wkp) {
  wkp->nfo->inslen = (wkp->pc < wkp->stpc ? wkp->pc+0x10000-wkp->stpc : wkp->pc-wkp->stpc);
  /* remove trailing blanks if we have no operands */
  if (wkp->dbstpos > 0 && (unsigned)(wkp->nfo->disbuf[wkp->dbstpos-1]&0xffU) <= 32) --wkp->dbstpos;
  wkp->nfo->disbuf[wkp->dbstpos] = 0;
  wkp->nfo->tstates[0] = wkp->tst[0]+wkp->tst[1];
  wkp->nfo->tstates[1] = wkp->tst[0];
}


// ////////////////////////////////////////////////////////////////////////// //
// main code

void zym_disasm_init (zym_disop_t *nfo) {
  memset(nfo, 0, sizeof(*nfo));
  nfo->mnemo = NULL;
  nfo->args[0] = nfo->args[1] = nfo->args[2] = NULL;
}


static void zym_disasm_one_internal (zym_cpu_t *z80, zym_disop_t *nfo, uint16_t origpc, const uint8_t mem[8]) {
  uint8_t tmpB;
  uint16_t tmpW;

  ZymDisWorkArea wk;

  wk.z80 = z80;
  wk.mem = mem;
  wk.nfo = nfo;
  wk.pc = origpc;
  wk.stpc = origpc;
  wk.mempos = 0;
  wk.dbpos = 0;
  wk.dbstpos = 0;
  wk.curarg = 0;
  wk.argstarted = 0;
  wk.tst[0] = wk.tst[1] = 0;

  nfo->inslen = 0;
  nfo->mnemo = NULL;
  nfo->mnemolen = 0;
  nfo->args[0] = nfo->args[1] = nfo->args[2] = NULL;
  nfo->argslen[0] = nfo->argslen[1] = nfo->argslen[2] = 0;
  nfo->tstates[0] = nfo->tstates[1] = 0;

  wk.tst[0] = 4; // opcode read
  uint8_t opcode = zym_dis_read_byte(&wk);
  wk.disp = 0;
  wk.gotDD = 0/*false*/;
  wk.DD = "HL";

  // check for I[XY] prefix
  if (opcode == 0xdd || opcode == 0xfd) {
    const uint32_t withIndexBmp[8] = {0x00,0x700000,0x40404040,0x40bf4040,0x40404040,0x40404040,0x0800,0x00};
    // IX/IY prefix
    wk.DD = (opcode == 0xdd ? "IX" : "IY");
    // read opcode -- OCR(4)
    opcode = zym_dis_read_byte(&wk);
    wk.tst[0] += 4; // opcode read
    // test if this instruction have (HL)
    if (withIndexBmp[opcode>>5]&(1<<(opcode&0x1f))) {
      // 3rd byte is always DISP here
      wk.disp = zym_dis_read_byte(&wk);
      if (wk.disp > 127) wk.disp -= 256;
      wk.tst[0] += 3; // disp reading, 3ts
    } else if (opcode == 0xdd || opcode == 0xfd) {
      // double prefix; put special NOP
      --wk.pc; // rollback for correct length
      // but don't undo opcode reading (i believe)
      zym_dis_put_mnemo(&wk, "NOP");
      if (opcode == 0xdd) zym_dis_put_arg(&wk, "#DD"); else zym_dis_put_arg(&wk, "#FD");
      zym_finish(&wk);
      return;
    }
    wk.gotDD = 1/*true*/;
  }
  // ED-prefixed instructions
  if (opcode == 0xed) {
    wk.DD = "HL"; // ED-prefixed opcodes cannot use IX/IY
    // read opcode -- OCR(4)
    opcode = zym_dis_read_byte(&wk);
    wk.tst[0] += 4; // opcode read; now 8ts
    // 16/21, common count
    wk.tst[0] += 8;
    if (opcode&0x10u) wk.tst[1] = 5; // repeated
    switch (opcode) {
      // LDI, LDIR, LDD, LDDR
      case 0xa0: zym_dis_put_mnemo(&wk, "LDI"); zym_finish(&wk); return;
      case 0xb0: zym_dis_put_mnemo(&wk, "LDIR"); zym_finish(&wk); return;
      case 0xa8: zym_dis_put_mnemo(&wk, "LDD"); zym_finish(&wk); return;
      case 0xb8: zym_dis_put_mnemo(&wk, "LDDR"); zym_finish(&wk); return;
      // CPI, CPIR, CPD, CPDR
      case 0xa1: zym_dis_put_mnemo(&wk, "CPI"); zym_finish(&wk); return;
      case 0xb1: zym_dis_put_mnemo(&wk, "CPIR"); zym_finish(&wk); return;
      case 0xa9: zym_dis_put_mnemo(&wk, "CPD"); zym_finish(&wk); return;
      case 0xb9: zym_dis_put_mnemo(&wk, "CPDR"); zym_finish(&wk); return;
      // OUTI, OTIR, OUTD, OTDR
      case 0xa3: zym_dis_put_mnemo(&wk, "OUTI"); zym_finish(&wk); return;
      case 0xb3: zym_dis_put_mnemo(&wk, "OTIR"); zym_finish(&wk); return;
      case 0xab: zym_dis_put_mnemo(&wk, "OUTD"); zym_finish(&wk); return;
      case 0xbb: zym_dis_put_mnemo(&wk, "OTDR"); zym_finish(&wk); return;
      // INI, INIR, IND, INDR
      case 0xa2: zym_dis_put_mnemo(&wk, "INI"); zym_finish(&wk); return;
      case 0xb2: zym_dis_put_mnemo(&wk, "INIR"); zym_finish(&wk); return;
      case 0xaa: zym_dis_put_mnemo(&wk, "IND"); zym_finish(&wk); return;
      case 0xba: zym_dis_put_mnemo(&wk, "INDR"); zym_finish(&wk); return;
      // not strings, but some good instructions anyway
      default:
        if ((opcode&0xc0) == 0x40) {
          // 0x40...0x7f
          switch (opcode&0x07) {
            case 0: // IN r8,(C)
            case 1: // OUT (C),r8
              wk.tst[0] += 4; // port read
              if (opcode&0x07) { zym_dis_put_mnemo(&wk, "OUT"); zym_dis_put_arg(&wk, "(C)"); } else zym_dis_put_mnemo(&wk, "IN");
              if (opcode&0x07) zym_dis_put_r8(&wk, "0", ((opcode>>3)&0x07)); else zym_dis_put_r8(&wk, "F", ((opcode>>3)&0x07)); // 0 on NMOS, 255 on CMOS
              if ((opcode&0x07) == 0) zym_dis_put_arg(&wk, "(C)");
              zym_finish(&wk);
              return;
            // SBC HL,rr/ADC HL,rr
            case 2:
              wk.tst[0] += 7; // math
              zym_dis_put_mnemo(&wk, opcode&0x08 ? "ADC" : "SBC");
              zym_dis_put_arg(&wk, "HL");
              zym_dis_put_r16sp(&wk, "HL", ((opcode>>4)&0x03));
              zym_finish(&wk);
              return;
            // LD (nn),rr/LD rr,(nn)
            case 3:
              wk.tst[0] += 12; // mem i/o
              zym_dis_put_mnemo(&wk, "LD");
              if ((opcode&0x08) == 0) {
                // LD (nn),rr
                zym_dis_put_arg_part(&wk, "(");
                tmpW = zym_dis_read_word(&wk);
                zym_dis_put_uint_addr_data(&wk, tmpW, 4);
                zym_dis_put_arg(&wk, ")");
              }
              zym_dis_put_r16sp(&wk, "HL", ((opcode>>4)&0x03));
              if ((opcode&0x08) != 0) {
                // LD rr,(nn)
                zym_dis_put_arg_part(&wk, "(");
                tmpW = zym_dis_read_word(&wk);
                zym_dis_put_uint_addr_data(&wk, tmpW, 4);
                zym_dis_put_arg(&wk, ")");
              }
              zym_finish(&wk);
              return;
            // NEG
            case 4:
              zym_dis_put_mnemo(&wk, "NEG");
              zym_finish(&wk);
              return;
            // RETI/RETN
            case 5:
              // RETI: 0x4d, 0x5d, 0x6d, 0x7d
              // RETN: 0x45, 0x55, 0x65, 0x75
              wk.tst[0] += 6; // stack, etc.
              if (opcode&0x08) zym_dis_put_mnemo(&wk, "RETI"); else zym_dis_put_mnemo(&wk, "RETN");
              zym_finish(&wk);
              return;
            // IM n
            case 6:
              zym_dis_put_mnemo(&wk, "IM");
              switch (opcode) {
                case 0x56: case 0x76: zym_dis_put_arg(&wk, "1"); break;
                case 0x5e: case 0x7e: zym_dis_put_arg(&wk, "2"); break;
                default: zym_dis_put_arg(&wk, "0"); break;
              }
              zym_finish(&wk);
              return;
            // specials
            case 7:
              switch (opcode) {
                case 0x47: // LD I,A
                  wk.tst[0] += 1; // register transfer
                  zym_dis_put_mnemo(&wk, "LD");
                  zym_dis_put_arg(&wk, "I");
                  zym_dis_put_arg(&wk, "A");
                  zym_finish(&wk);
                  return;
                case 0x4f: // LD R,A
                  wk.tst[0] += 1; // register transfer
                  zym_dis_put_mnemo(&wk, "LD");
                  zym_dis_put_arg(&wk, "R");
                  zym_dis_put_arg(&wk, "A");
                  zym_finish(&wk);
                  return;
                case 0x57: // LD A,I
                  wk.tst[0] += 1; // register transfer
                  zym_dis_put_mnemo(&wk, "LD");
                  zym_dis_put_arg(&wk, "A");
                  zym_dis_put_arg(&wk, "I");
                  zym_finish(&wk);
                  return;
                case 0x5f: // LD A,R
                  wk.tst[0] += 1; // register transfer
                  zym_dis_put_mnemo(&wk, "LD");
                  zym_dis_put_arg(&wk, "A");
                  zym_dis_put_arg(&wk, "R");
                  zym_finish(&wk);
                  return;
                case 0x67: // RRD
                  wk.tst[0] += 10; // register transfer, mem i/o
                  zym_dis_put_mnemo(&wk, "RRD");
                  zym_finish(&wk);
                  return;
                case 0x6F: // RLD
                  wk.tst[0] += 10; // register transfer, mem i/o
                  zym_dis_put_mnemo(&wk, "RLD");
                  zym_finish(&wk);
                  return;
              }
          }
        } else {
          // slt and other traps
          if (opcode == 0xFB) {
            zym_dis_put_mnemo(&wk, "TSLT");
          } else {
            zym_dis_put_mnemo(&wk, "TRAP");
            zym_dis_start_arg(&wk);
            zym_dis_put_uint_hex_len(&wk, opcode, 2);
            zym_dis_end_arg(&wk);
          }
        }
        zym_finish(&wk);
        return;
    }
    // NOP
    zym_dis_put_mnemo(&wk, "NOP");
    zym_dis_put_arg(&wk, "#ED");
    zym_dis_start_arg(&wk);
    zym_dis_put_uint_hex_len(&wk, opcode, 2);
    zym_dis_end_arg(&wk);
    zym_finish(&wk);
    return;
  } // 0xed done
  // CB-prefixed instructions
  if (opcode == 0xcb) {
    // shifts and bit operations
    // read opcode
    opcode = zym_dis_read_byte(&wk);
    wk.tst[0] += 4; // opcode read; now 8ts
    if (wk.gotDD || (opcode&0x07) == 6) wk.tst[0] += 4; // mem read
    // mnemonics
    switch ((opcode>>3)&0x1f) {
      case 0: zym_dis_put_mnemo(&wk, "RLC"); break;
      case 1: zym_dis_put_mnemo(&wk, "RRC"); break;
      case 2: zym_dis_put_mnemo(&wk, "RL"); break;
      case 3: zym_dis_put_mnemo(&wk, "RR"); break;
      case 4: zym_dis_put_mnemo(&wk, "SLA"); break;
      case 5: zym_dis_put_mnemo(&wk, "SRA"); break;
      case 6: zym_dis_put_mnemo(&wk, "SLS"); break;
      case 7: zym_dis_put_mnemo(&wk, "SRL"); break;
      default:
        switch ((opcode>>6)&0x03) {
          case 1: zym_dis_put_mnemo(&wk, "BIT"); break;
          case 2: zym_dis_put_mnemo(&wk, "RES"); break;
          case 3: zym_dis_put_mnemo(&wk, "SET"); break;
        }
        zym_dis_start_arg(&wk);
        zym_dis_put_uint_dec(&wk, (opcode>>3)&0x07);
        zym_dis_end_arg(&wk);
        break;
    }
    // no BITs
    if ((opcode&0xc0u) != 0x40u) {
      if (wk.gotDD) {
        if ((opcode&0x07u) != 6) wk.tst[0] += 3;
      } else {
        if ((opcode&0x07u) == 6) wk.tst[0] += 4;
      }
    }
    // R8X arg
    if (wk.gotDD) {
      zym_dis_put_arg_part(&wk, "(");
      zym_dis_put_arg_part(&wk, wk.DD);
      zym_dis_put_disp(&wk, wk.disp);
      zym_dis_put_arg(&wk, ")");
      if ((opcode&0x07) != 6) zym_dis_put_r8(&wk, "", (opcode&0x07));
    } else {
      zym_dis_put_r8(&wk, "(HL)", (opcode&0x07));
    }
    zym_finish(&wk);
    return;
  } // 0xcb done
  // normal things
  switch (opcode&0xc0) {
    // 0x00..0x3F
    case 0x00:
      switch (opcode&0x07) {
        // misc,DJNZ,JR,JR cc
        case 0:
          if (opcode&0x30) {
            // branches
            if (opcode&0x20) {
              // JR cc
              zym_dis_put_mnemo(&wk, "JR");
              zym_dis_put_cc(&wk, (opcode>>3)&0x03);
            } else {
              // DJNZ/JR
              zym_dis_put_mnemo(&wk, opcode&0x08 ? "JR" : "DJNZ");
              wk.tst[0] += 1;
            }
            wk.tst[0] += 3;
            wk.tst[1] = 5; // for taken
            wk.disp = zym_dis_read_byte(&wk);
            if (wk.disp > 127) wk.disp -= 256; // convert to int8_t
            uint16_t addr = (uint16_t)(wk.pc+wk.disp);
            zym_dis_start_arg(&wk);
            zym_dis_put_uint_addr_pc(&wk, addr, 4);
            zym_dis_end_arg(&wk);
          } else {
            // EX AF,AF' or NOP
            if (opcode != 0) {
              zym_dis_put_mnemo(&wk, "EX");
              zym_dis_put_arg(&wk, "AF");
              zym_dis_put_arg(&wk, "AF'");
            } else {
              zym_dis_put_mnemo(&wk, "NOP");
            }
          }
          zym_finish(&wk);
          return;
        // LD rr,nn/ADD HL,rr
        case 1:
          if (opcode&0x08) {
            // ADD HL,rr
            wk.tst[0] += 7;
            zym_dis_put_mnemo(&wk, "ADD");
            zym_dis_put_arg(&wk, wk.DD);
            zym_dis_put_r16sp(&wk, "", ((opcode>>4)&0x03));
          } else {
            // LD rr,nn
            wk.tst[0] += 6;
            zym_dis_put_mnemo(&wk, "LD");
            zym_dis_put_r16sp(&wk, "", ((opcode>>4)&0x03));
            tmpW = zym_dis_read_word(&wk);
            zym_dis_start_arg(&wk);
            zym_dis_put_uint_data_word(&wk, tmpW, 4);
            zym_dis_end_arg(&wk);
          }
          zym_finish(&wk);
          return;
        // LD xxx,xxx
        case 2:
          zym_dis_put_mnemo(&wk, "LD");
          switch ((opcode>>3)&0x07) {
            // LD (BC),A
            case 0: zym_dis_put_arg(&wk, "(BC)"); zym_dis_put_arg(&wk, "A"); wk.tst[0] += 3; break;
            // LD A,(BC)
            case 1: zym_dis_put_arg(&wk, "A"); zym_dis_put_arg(&wk, "(BC)"); wk.tst[0] += 3; break;
            // LD (DE),A
            case 2: zym_dis_put_arg(&wk, "(DE)"); zym_dis_put_arg(&wk, "A"); wk.tst[0] += 3; break;
            // LD A,(DE)
            case 3: zym_dis_put_arg(&wk, "A"); zym_dis_put_arg(&wk, "(DE)"); wk.tst[0] += 3; break;
            // LD (nn),HL
            case 4:
              zym_dis_put_arg_part(&wk, "(");
              tmpW = zym_dis_read_word(&wk);
              zym_dis_put_uint_addr_data(&wk, tmpW, 4);
              zym_dis_put_arg(&wk, ")");
              zym_dis_put_arg(&wk, wk.DD);
              wk.tst[0] += 12;
              break;
            // LD HL,(nn)
            case 5:
              zym_dis_put_arg(&wk, wk.DD);
              zym_dis_put_arg_part(&wk, "(");
              tmpW = zym_dis_read_word(&wk);
              zym_dis_put_uint_addr_data(&wk, tmpW, 4);
              zym_dis_put_arg(&wk, ")");
              wk.tst[0] += 12;
              break;
            // LD (nn),A
            case 6:
              zym_dis_put_arg_part(&wk, "(");
              tmpW = zym_dis_read_word(&wk);
              zym_dis_put_uint_addr_data(&wk, tmpW, 4);
              zym_dis_put_arg(&wk, ")");
              zym_dis_put_arg(&wk, "A");
              wk.tst[0] += 9;
              break;
            // LD A,(nn)
            case 7:
              zym_dis_put_arg(&wk, "A");
              zym_dis_put_arg_part(&wk, "(");
              tmpW = zym_dis_read_word(&wk);
              zym_dis_put_uint_addr_data(&wk, tmpW, 4);
              zym_dis_put_arg(&wk, ")");
              wk.tst[0] += 9;
              break;
          }
          zym_finish(&wk);
          return;
        // INC rr/DEC rr
        case 3:
          wk.tst[0] += 2;
          zym_dis_put_mnemo(&wk, opcode&0x08 ? "DEC" : "INC");
          zym_dis_put_r16sp(&wk, "", ((opcode>>4)&0x03));
          zym_finish(&wk);
          return;
        // INC/DEC r8; LD r8,n
        case 4: // INC r8
        case 5: // DEC r8
        case 6: // LD r8,n
          switch (opcode&0x07) {
            case 4: zym_dis_put_mnemo(&wk, "INC"); break;
            case 5: zym_dis_put_mnemo(&wk, "DEC"); break;
            case 6: zym_dis_put_mnemo(&wk, "LD"); wk.tst[0] += 3; break;
          }
          switch ((opcode>>3)&0x07) {
            case 0: zym_dis_put_arg(&wk, "B"); break;
            case 1: zym_dis_put_arg(&wk, "C"); break;
            case 2: zym_dis_put_arg(&wk, "D"); break;
            case 3: zym_dis_put_arg(&wk, "E"); break;
            case 4: if (wk.gotDD) zym_dis_put_arg_part(&wk, wk.DD); zym_dis_put_arg(&wk, "H"); break;
            case 5: if (wk.gotDD) zym_dis_put_arg_part(&wk, wk.DD); zym_dis_put_arg(&wk, "L"); break;
            case 6:
              zym_dis_put_arg_part(&wk, "(");
              zym_dis_put_arg_part(&wk, wk.DD);
              if (wk.gotDD) { wk.tst[0] += 2+((opcode&0x07) == 6 ? 0 : 3); zym_dis_put_disp(&wk, wk.disp); }
              zym_dis_put_arg(&wk, ")");
              wk.tst[0] += 3+((opcode&0x07) == 6 ? 0 : 3+1);
              break;
            case 7: zym_dis_put_arg(&wk, "A"); break;
          }
          // LD?
          if ((opcode&0x07) == 6) {
            tmpB = zym_dis_read_byte(&wk);
            zym_dis_start_arg(&wk);
            zym_dis_put_uint_data_byte(&wk, tmpB, 2);
            zym_dis_end_arg(&wk);
          }
          zym_finish(&wk);
          return;
        // swim-swim-hungry
        case 7:
          switch ((opcode>>3)&0x07) {
            case 0: zym_dis_put_mnemo(&wk, "RLCA"); break;
            case 1: zym_dis_put_mnemo(&wk, "RRCA"); break;
            case 2: zym_dis_put_mnemo(&wk, "RLA"); break;
            case 3: zym_dis_put_mnemo(&wk, "RRA"); break;
            case 4: zym_dis_put_mnemo(&wk, "DAA"); break;
            case 5: zym_dis_put_mnemo(&wk, "CPL"); break;
            case 6: zym_dis_put_mnemo(&wk, "SCF"); break;
            case 7: zym_dis_put_mnemo(&wk, "CCF"); break;
          }
          zym_finish(&wk);
          return;
      }
      /*assert(0);*/
      abort();
    // 0x40..0x7F: LD r8,r8
    case 0x40:
      if (opcode == 0x76) { zym_dis_put_mnemo(&wk, "HALT"); zym_finish(&wk); return; }
      zym_dis_put_mnemo(&wk, "LD");
      const uint8_t rsrc = (opcode&0x07);
      const uint8_t rdst = ((opcode>>3)&0x07);
      switch (rdst) {
        case 0: zym_dis_put_arg(&wk, "B"); break;
        case 1: zym_dis_put_arg(&wk, "C"); break;
        case 2: zym_dis_put_arg(&wk, "D"); break;
        case 3: zym_dis_put_arg(&wk, "E"); break;
        case 4: if (wk.gotDD && rsrc != 6) zym_dis_put_arg_part(&wk, wk.DD); zym_dis_put_arg(&wk, "H"); break;
        case 5: if (wk.gotDD && rsrc != 6) zym_dis_put_arg_part(&wk, wk.DD); zym_dis_put_arg(&wk, "L"); break;
        case 6:
          wk.tst[0] += 3;
          zym_dis_put_arg_part(&wk, "(");
          zym_dis_put_arg_part(&wk, wk.DD);
          if (wk.gotDD) { wk.tst[0] += 5; zym_dis_put_disp(&wk, wk.disp); }
          zym_dis_put_arg(&wk, ")");
          break;
        case 7: zym_dis_put_arg(&wk, "A"); break;
      }
      switch (rsrc) {
        case 0: zym_dis_put_arg(&wk, "B"); break;
        case 1: zym_dis_put_arg(&wk, "C"); break;
        case 2: zym_dis_put_arg(&wk, "D"); break;
        case 3: zym_dis_put_arg(&wk, "E"); break;
        case 4: if (wk.gotDD && rdst != 6) zym_dis_put_arg_part(&wk, wk.DD); zym_dis_put_arg(&wk, "H"); break;
        case 5: if (wk.gotDD && rdst != 6) zym_dis_put_arg_part(&wk, wk.DD); zym_dis_put_arg(&wk, "L"); break;
        case 6:
          wk.tst[0] += 3;
          zym_dis_put_arg_part(&wk, "(");
          zym_dis_put_arg_part(&wk, wk.DD);
          if (wk.gotDD) { wk.tst[0] += 5; zym_dis_put_disp(&wk, wk.disp); }
          zym_dis_put_arg(&wk, ")");
          break;
        case 7: zym_dis_put_arg(&wk, "A"); break;
      }
      zym_finish(&wk);
      return;
    // 0x80..0xBF: ALU A,r8
    case 0x80:
      switch ((opcode>>3)&0x07) {
        case 0: zym_dis_put_mnemo(&wk, "ADD"); zym_dis_put_arg(&wk, "A"); break;
        case 1: zym_dis_put_mnemo(&wk, "ADC"); zym_dis_put_arg(&wk, "A"); break;
        case 2: zym_dis_put_mnemo(&wk, "SUB"); break;
        case 3: zym_dis_put_mnemo(&wk, "SBC"); zym_dis_put_arg(&wk, "A"); break;
        case 4: zym_dis_put_mnemo(&wk, "AND"); break;
        case 5: zym_dis_put_mnemo(&wk, "XOR"); break;
        case 6: zym_dis_put_mnemo(&wk, "OR"); break;
        case 7: zym_dis_put_mnemo(&wk, "CP"); break;
      }
      if ((opcode&0x07) == 6) {
        wk.tst[0] += 3;
        if (wk.gotDD) wk.tst[0] += 5;
      }
      zym_dis_put_r8(&wk, "", (opcode&0x07));
      zym_finish(&wk);
      return;
    // 0xC0..0xFF
    case 0xC0:
      switch (opcode&0x07) {
        // RET cc
        case 0:
          wk.tst[0] += 1;
          wk.tst[1] += 6;
          zym_dis_put_mnemo(&wk, "RET");
          zym_dis_put_cc(&wk, (opcode>>3)&0x07);
          break;
        // POP rr/special0
        case 1:
          if (opcode&0x08) {
            // special 0
            switch ((opcode>>4)&0x03) {
              // RET
              case 0: zym_dis_put_mnemo(&wk, "RET"); wk.tst[0] += 6; break;
              // EXX
              case 1: zym_dis_put_mnemo(&wk, "EXX"); break;
              // JP (HL)
              case 2: zym_dis_put_mnemo(&wk, "JP"); zym_dis_put_arg_part(&wk, "("); zym_dis_put_arg_part(&wk, wk.DD); zym_dis_put_arg(&wk, ")"); break;
              // LD SP,HL
              case 3: wk.tst[0] += 2; zym_dis_put_mnemo(&wk, "LD"); zym_dis_put_arg(&wk, "SP"); zym_dis_put_arg(&wk, wk.DD); break;
            }
          } else {
            // POP rr
            wk.tst[0] += 6;
            zym_dis_put_mnemo(&wk, "POP");
            zym_dis_put_r16af(&wk, (opcode>>4)&0x03);
          }
          break;
        // JP cc,nn
        case 2:
          wk.tst[0] += 6;
          zym_dis_put_mnemo(&wk, "JP");
          zym_dis_put_cc(&wk, (opcode>>3)&0x07);
          tmpW = zym_dis_read_word(&wk);
          zym_dis_start_arg(&wk);
          zym_dis_put_uint_addr_pc(&wk, tmpW, 4);
          zym_dis_end_arg(&wk);
          break;
        // special1/special3
        case 3:
          switch ((opcode>>3)&0x07) {
            // JP nn
            case 0: wk.tst[0] += 6; zym_dis_put_mnemo(&wk, "JP"); tmpW = zym_dis_read_word(&wk); zym_dis_start_arg(&wk); zym_dis_put_uint_addr_pc(&wk, tmpW, 4); zym_dis_end_arg(&wk); break;
            // OUT (n),A
            case 2: wk.tst[0] += 7; zym_dis_put_mnemo(&wk, "OUT"); zym_dis_put_arg_part(&wk, "("); tmpB = zym_dis_read_byte(&wk); zym_dis_put_uint_port_byte(&wk, tmpB, 2); zym_dis_put_arg(&wk, ")"); zym_dis_put_arg(&wk, "A"); break;
            // IN A,(n)
            case 3: wk.tst[0] += 7; zym_dis_put_mnemo(&wk, "IN"); zym_dis_put_arg(&wk, "A"); zym_dis_put_arg_part(&wk, "("); tmpB = zym_dis_read_byte(&wk); zym_dis_put_uint_port_byte(&wk, tmpB, 2); zym_dis_put_arg(&wk, ")"); break;
            // EX (SP),HL
            case 4: wk.tst[0] += 15; zym_dis_put_mnemo(&wk, "EX"); zym_dis_put_arg(&wk, "(SP)"); zym_dis_put_arg(&wk, wk.DD); break;
            // EX DE,HL
            case 5: zym_dis_put_mnemo(&wk, "EX"); zym_dis_put_arg(&wk, "DE"); zym_dis_put_arg(&wk, "HL"); break;
            // DI
            case 6: zym_dis_put_mnemo(&wk, "DI"); break;
            // EI
            case 7: zym_dis_put_mnemo(&wk, "EI"); break;
          }
          break;
        // CALL cc,nn
        case 4:
          wk.tst[0] += 6;
          wk.tst[1] = 7;
          zym_dis_put_mnemo(&wk, "CALL");
          zym_dis_put_cc(&wk, (opcode>>3)&0x07);
          tmpW = zym_dis_read_word(&wk);
          zym_dis_start_arg(&wk);
          zym_dis_put_uint_addr_pc(&wk, tmpW, 4);
          zym_dis_end_arg(&wk);
          break;
        // PUSH rr/special2
        case 5:
          if (opcode&0x08) {
            if (((opcode>>4)&0x03) == 0) {
              // CALL
              wk.tst[0] += 6+7;
              zym_dis_put_mnemo(&wk, "CALL");
              tmpW = zym_dis_read_word(&wk);
              zym_dis_start_arg(&wk);
              zym_dis_put_uint_addr_pc(&wk, tmpW, 4);
              zym_dis_end_arg(&wk);
            }
          } else {
            // PUSH rr
            wk.tst[0] += 7;
            zym_dis_put_mnemo(&wk, "PUSH");
            zym_dis_put_r16af(&wk, (opcode>>4)&0x03);
          }
          break;
        // ALU A,n
        case 6:
          switch ((opcode>>3)&0x07) {
            case 0: zym_dis_put_mnemo(&wk, "ADD"); zym_dis_put_arg(&wk, "A"); break;
            case 1: zym_dis_put_mnemo(&wk, "ADC"); zym_dis_put_arg(&wk, "A"); break;
            case 2: zym_dis_put_mnemo(&wk, "SUB"); break;
            case 3: zym_dis_put_mnemo(&wk, "SBC"); zym_dis_put_arg(&wk, "A"); break;
            case 4: zym_dis_put_mnemo(&wk, "AND"); break;
            case 5: zym_dis_put_mnemo(&wk, "XOR"); break;
            case 6: zym_dis_put_mnemo(&wk, "OR"); break;
            case 7: zym_dis_put_mnemo(&wk, "CP"); break;
          }
          wk.tst[0] += 3;
          tmpB = zym_dis_read_byte(&wk);
          zym_dis_start_arg(&wk);
          zym_dis_put_uint_data_byte(&wk, tmpB, 2);
          zym_dis_end_arg(&wk);
          break;
        // RST nnn
        case 7:
          wk.tst[0] += 7;
          zym_dis_put_mnemo(&wk, "RST");
          zym_dis_start_arg(&wk);
          zym_dis_put_uint_hexlen(&wk, opcode&0x38, 2);
          zym_dis_end_arg(&wk);
          break;
      }
      break;
  } // end switch
  zym_finish(&wk);
}


void zym_disasm_one (zym_cpu_t *z80, zym_disop_t *nfo, uint16_t pc) {
  if (!z80) abort();
  zym_disasm_one_internal(z80, nfo, pc, NULL);
}


void zym_disasm_one_ex (zym_disop_t *nfo, const uint8_t mem[8], uint16_t pc) {
  if (!mem) abort();
  zym_disasm_one_internal(NULL, nfo, pc, mem);
}
