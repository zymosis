/* This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "zymosis.h"
#include "zymosis_utils.h"
#include "../liburasm/liburasm.h"


static zym_cpu_t z80;
static uint8_t memory[65536], memsave[65536];
static char descr[8192];


static int doInfoDump = 1;
static int use_urasm = 0;


static void z80Contention (zym_cpu_t *z80, uint16_t addr, zym_memio_t mio, zym_memreq_t mreq) {
  printf("%5d MC %04x\n", z80->tstates, addr);
  /*z80->tstates += tstates;*/
}


static const char *memStr (int mem) {
  static char res[12];
  //
  switch (mem) {
    case ZYM_MEMIO_SP: return "(SP)";
    case ZYM_MEMIO_BC: return "(BC)";
    case ZYM_MEMIO_DE: return "(DE)";
    case ZYM_MEMIO_IY: return "(IY)";
    case ZYM_MEMIO_IX: return "(IX)";
    case ZYM_MEMIO_HL: return "(HL)";
  }
  sprintf(res, "(#%04X)", mem);
  return res;
}


static const char *pushStr (int mem) {
  switch (mem) {
    case ZYM_STK_BC: return "BC";
    case ZYM_STK_DE: return "DE";
    case ZYM_STK_HL: return "HL";
    case ZYM_STK_AF: return "AF";
    case ZYM_STK_IX: return "IX";
    case ZYM_STK_IY: return "IY";
    case ZYM_STK_PC: return "PC";
    default: abort();
  }
}


static const char *portStr (int mem) {
  static char res[12];
  //
  switch (mem) {
    case ZYM_PORTIO_BC: return "(BC)";
    case ZYM_PORTIO_BCM1: return "([B-1]C)";
  }
  sprintf(res, "([A]#%02X)", mem);
  return res;
}


static uint8_t z80MemRead (zym_cpu_t *z80, uint16_t addr, zym_memio_t mio) {
  if (mio != ZYM_MEMIO_OTHER) {
    printf("%5d MR %04x %02x\n", z80->tstates, addr, memory[addr]);
    if (doInfoDump && mio == ZYM_MEMIO_OPCODE) {
      char *t;
      char dstr[128];
      zym_op_meminfo_t nfo;
      uint8_t mem[8];
      zym_disop_t disop;
      /*zym_disasm_init(&disop);*/
      disop.flags = 0;
      for (unsigned f = 0; f < 8; ++f) mem[f] = memory[(addr+f)&0xffff];
      zym_disasm_one_ex(&disop, mem, addr);
      int len = disop.inslen;
      int len1 = urasm_disasm_opdisasm_ex(dstr, addr, mem);
      if (len != len1) {
        fprintf(stderr, "*** FUCK! *** (len=%d; len1=%d)\n", len, len1);
      }
      //
      zym_op_meminfo(z80, &nfo, addr);
      if (len != nfo.inslen) fprintf(stderr, "****************** (%d:%d)\n", len, nfo.inslen);
      else fprintf(stderr, "------------------\n");
      // zymosis disasm
      if (!use_urasm) {
        fprintf(stderr, "#%04X:", addr);
        for (int f = 0; f < nfo.inslen; ++f) fprintf(stderr, "%02X", memory[(addr+f)&0xffff]);
        for (int f = nfo.inslen; f < 5; ++f) fprintf(stderr, "  ");
        //
        t = strchr(disop.disbuf, '\t');
        if (t != NULL) *t++ = 0;
        fprintf(stderr, "%s", disop.disbuf);
        if (t != NULL && *t) {
          for (int f = (int)strlen(disop.disbuf); f < 5; ++f) fputc(' ', stderr);
          fprintf(stderr, "%s", t);
        }
        fputc('\n', stderr);
      } else {
        // urasm disasm
        fprintf(stderr, "#%04X:", addr);
        for (int f = 0; f < nfo.inslen; ++f) fprintf(stderr, "%02X", memory[(addr+f)&0xffff]);
        for (int f = nfo.inslen; f < 5; ++f) fprintf(stderr, "  ");
        //
        t = strchr(dstr, '\t');
        if (t != NULL) *t++ = 0;
        fprintf(stderr, "%s", dstr);
        if (t != NULL && *t) {
          for (int f = (int)strlen(dstr); f < 5; ++f) fputc(' ', stderr);
          fprintf(stderr, "%s", t);
        }
        fputc('\n', stderr);
      }
      //
      if (nfo.trap >= 0) fprintf(stderr, "DISP: %d\n", nfo.disp);
      if (nfo.disp != 0xffff) fprintf(stderr, "DISP: %d\n", nfo.disp);
      if (nfo.memread != ZYM_MEMIO_NONE) {
        fprintf(stderr, " MR: %s %s\n", memStr(nfo.memread), (nfo.memrwword ? "word" : "byte"));
      }
      if (nfo.memwrite != ZYM_MEMIO_NONE) {
        fprintf(stderr, " MW: %s %s\n", memStr(nfo.memwrite), (nfo.memrwword ? "word" : "byte"));
      }
      if (nfo.jump != ZYM_JUMP_NONE) {
        fprintf(stderr, " JUMP: ");
        switch (nfo.jump) {
          case ZYM_JUMP_RET: fprintf(stderr, "RET"); break;
          case ZYM_JUMP_IY: fprintf(stderr, "(IY)"); break;
          case ZYM_JUMP_IX: fprintf(stderr, "(IX)"); break;
          case ZYM_JUMP_HL: fprintf(stderr, "(HL)"); break;
          default: fprintf(stderr, "#%04X", nfo.jump); break;
        }
        switch (nfo.cond) {
          case ZYM_COND_NONE: break;
          case ZYM_COND_NZ: fprintf(stderr, " [NZ]"); break;
          case ZYM_COND_Z: fprintf(stderr, " [Z]"); break;
          case ZYM_COND_NC: fprintf(stderr, " [NC]"); break;
          case ZYM_COND_C: fprintf(stderr, " [C]"); break;
          case ZYM_COND_PO: fprintf(stderr, " [PO]"); break;
          case ZYM_COND_PE: fprintf(stderr, " [PE]"); break;
          case ZYM_COND_P: fprintf(stderr, " [P]"); break;
          case ZYM_COND_M: fprintf(stderr, " [M]"); break;
          case ZYM_COND_BCNZ: fprintf(stderr, " [BCNZ]"); break;
          case ZYM_COND_BNZ: fprintf(stderr, " [BNZ]"); break;
          case ZYM_COND_RETI: fprintf(stderr, " [RETI]"); break;
          case ZYM_COND_RETN: fprintf(stderr, " [RETN]"); break;
          default: abort();
        }
        fprintf(stderr, "\n");
      } else if (nfo.cond != ZYM_COND_NONE) abort();
      if (nfo.push != ZYM_STK_NONE) fprintf(stderr, " PUSH: %s\n", pushStr(nfo.push));
      if (nfo.pop != ZYM_STK_NONE) fprintf(stderr, " POP: %s\n", pushStr(nfo.pop));
      if (nfo.portread != ZYM_PORTIO_NONE) fprintf(stderr, "PR: %s\n", portStr(nfo.portread));
      if (nfo.portwrite != ZYM_PORTIO_NONE) fprintf(stderr, "PW: %s\n", portStr(nfo.portwrite));
    }
  }
  return memory[addr];
}


static void z80MemWrite (zym_cpu_t *z80, uint16_t addr, uint8_t value, zym_memio_t mio) {
  if (mio != ZYM_MEMIO_OTHER) {
    printf("%5d MW %04x %02x\n", z80->tstates, addr, value);
  }
  memory[addr] = value;
}


static void z80PortContention (zym_cpu_t *z80, uint16_t port, int tstates, zym_portio_flags_t pflags) {
  if (pflags&ZYM_PORTIO_FLAG_EARLY) {
    if ((port&0xc000) == 0x4000) printf("%5d PC %04x\n", z80->tstates, port);
  } else {
    if (port&0x0001) {
      if ((port&0xc000) == 0x4000) {
        for (int f = 0; f < 3; ++f) printf("%5d PC %04x\n", z80->tstates+f, port);
      }
    } else {
      printf("%5d PC %04x\n", z80->tstates, port);
    }
  }
  z80->tstates += tstates;
}


// see the note for z80PortContention
static uint8_t z80PortIn (zym_cpu_t *z80, uint16_t port, zym_portio_t pio) {
  if (pio != ZYM_PORTIO_INTERNAL) {
    printf("%5d PR %04x %02x\n", z80->tstates, port, port>>8);
  }
  return port>>8;
}


static void z80PortOut (zym_cpu_t *z80, uint16_t port, uint8_t value, zym_portio_t pio) {
  if (pio != ZYM_PORTIO_INTERNAL) {
    printf("%5d PW %04x %02x\n", z80->tstates, port, value);
  }
}


// !0: error or done
static int readTestIn (FILE *fl) {
  unsigned int i, r, i1, i2, im, hlt, ts;
  unsigned int af, bc, de, hl, afx, bcx, dex, hlx, ix, iy, sp, pc, memptr;
  int done = 1, ch;
  //
  while (fgets(descr, sizeof(descr), fl) != NULL) {
    descr[sizeof(descr)-1] = '\0';
    for (char *t = descr+strlen(descr)-1; t >= descr && isspace(*t); --t) *t = '\0';
    if (descr[0]) { done = 0; break; }
  }
  //
  if (done) return -1;
  zym_reset(&z80);
  z80.evenM1 = 0;
  fscanf(fl, "%04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x",
    &af, &bc, &de, &hl, &afx, &bcx, &dex, &hlx, &ix, &iy, &sp, &pc, &memptr);
  z80.af.w = af;
  z80.bc.w = bc;
  z80.de.w = de;
  z80.hl.w = hl;
  z80.afx.w = afx;
  z80.bcx.w = bcx;
  z80.dex.w = dex;
  z80.hlx.w = hlx;
  z80.ix.w = ix;
  z80.iy.w = iy;
  z80.sp.w = sp;
  z80.pc = pc;
  //
  fscanf(fl, "%02x %02x %d %d %d %d %d", &i, &r, &i1, &i2, &im, &hlt, &ts);
  z80.regI = i;
  z80.regR = r;
  z80.iff1 = i1;
  z80.iff2 = i2;
  z80.im = im;
  z80.tstates = 0;
  z80.next_event_tstate = ts;
  //z80.tstates_per_frame = 70000; //FIXME
  //
  //memset(memory, 0, sizeof(memory));
  for (int f = 0; f < 65536; f += 4) {
    memory[f+0] = 0xde;
    memory[f+1] = 0xad;
    memory[f+2] = 0xbe;
    memory[f+3] = 0xef;
  }
  //
  for (;;) {
    for (;;) {
      if ((ch = fgetc(fl)) == EOF) return -1;
      if (isspace(ch)) continue;
      if (ch == '-') {
        while ((ch = fgetc(fl)) != EOF) if (isspace(ch)) break;
        goto done;
      }
      ungetc(ch, fl);
      fscanf(fl, "%04x", &pc);
      break;
    }
    for (;;) {
      if ((ch = fgetc(fl)) == EOF) return -1;
      if (isspace(ch)) continue;
      if (ch == '-') {
        while ((ch = fgetc(fl)) != EOF) if (isspace(ch)) break;
        ungetc(ch, fl);
        break;
      }
      ungetc(ch, fl);
      fscanf(fl, "%02x", &r);
      memory[pc] = r;
      //printf("pc=0x%04x; byte=0x%02x\n", pc, r);
      pc = (pc+1)&0xffff;
    }
  }
done:
  memcpy(memsave, memory, 65536);
  return 0;
}


static void dumpState (void) {
  printf("%04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x\n",
    z80.af.w, z80.bc.w, z80.de.w, z80.hl.w, z80.afx.w, z80.bcx.w, z80.dex.w, z80.hlx.w, z80.ix.w, z80.iy.w, z80.sp.w, z80.pc, z80.memptr.w);
  printf("%02x %02x %d %d %d %d %d\n", z80.regI, z80.regR, z80.iff1, z80.iff2, z80.im, z80.halted, z80.tstates);
  //
  for (int f = 0; f < 65536; ++f) {
    if (memory[f] != memsave[f]) {
      printf("%04x ", (unsigned)f);
      while (f < 65536 && memory[f] != memsave[f]) printf("%02x ", memory[f++]);
      printf("-1\n");
    }
  }
  printf("\n");
}


int main (int argc, char *argv[]) {
  if (argc < 2) {
    fprintf(stderr, "usage: zytest_ex path/to/tests.in\n");
    return 1;
  }
  use_urasm = 0;
  for (int f = 2; f < argc; ++f) {
    if (strcmp(argv[f], "--urasm") == 0) {
      use_urasm = 1;
      for (int c = f+1; c < argc; ++c) argv[c-1] = argv[c];
      --argc;
      --f;
    }
  }
  FILE *fl;
  //
  zym_clear_callbacks(&z80);
  z80.mem_read = z80MemRead;
  z80.mem_write = z80MemWrite;
  z80.mem_contention = z80Contention;
  z80.port_read = z80PortIn;
  z80.port_write = z80PortOut;
  z80.port_contention = z80PortContention;
  //
  fl = fopen(/*"testdata/tests.in"*/argv[1], "r");
  if (!fl) { fprintf(stderr, "WTF?!\n"); return 1; }
  while (readTestIn(fl) == 0) {
    if (argc > 2) {
      int found = 0;
      //
      for (int f = 2; f < argc; ++f) if (strcmp(argv[f], descr) == 0) { found = 1; break; }
      if (!found) continue;
    }
    if (argc > 2) {
      printf("%s\n", descr);
      dumpState();
    }
    printf("%s\n", descr);
    zym_exec(&z80);
    dumpState();
  }
  return 0;
}
