/*
 * Zymosis: Z80 CPU emulation engine v0.1.2.1
 * coded by Ketmar // Invisible Vector (ketmar@ketmar.no-ip.org)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
#ifndef ZYMOSIS_UTILS_H
#define ZYMOSIS_UTILS_H

#include "zymosis.h"


/******************************************************************************/
/* very simple instruction info extractor */
/******************************************************************************/
enum {
  ZYM_COND_NONE = -1,
  ZYM_COND_NZ,
  ZYM_COND_Z,
  ZYM_COND_NC,
  ZYM_COND_C,
  ZYM_COND_PO,
  ZYM_COND_PE,
  ZYM_COND_P,
  ZYM_COND_M,
  ZYM_COND_BCNZ,
  ZYM_COND_BNZ,
  ZYM_COND_RETI,
  ZYM_COND_RETN
};


enum {
  ZYM_MEMIO_NONE = -666,
  ZYM_MEMIO_SP = -6,
  ZYM_MEMIO_BC = -5,
  ZYM_MEMIO_DE = -4,
  ZYM_MEMIO_IY = -3,
  ZYM_MEMIO_IX = -2,
  ZYM_MEMIO_HL = -1
};


enum {
  ZYM_JUMP_NONE = -666,
  ZYM_JUMP_RET = -4,
  ZYM_JUMP_IY = -3,
  ZYM_JUMP_IX = -2,
  ZYM_JUMP_HL = -1
};


enum {
  ZYM_PORTIO_NONE = -666,
  ZYM_PORTIO_BC = -1,
  ZYM_PORTIO_BCM1 = -2  /* (B-1)C, for OUT* */
};


enum {
  ZYM_STK_NONE = -666,
  ZYM_STK_BC = 0,
  ZYM_STK_DE,
  ZYM_STK_HL,
  ZYM_STK_AF,
  ZYM_STK_IX,
  ZYM_STK_IY,
  ZYM_STK_PC
};


/* there can be DD/FD prefix followed by ED/CB, hence the bitset */
/* note that ED totally ignores IX/IY prefix, and CB with IX/IY is a strange beast */
enum {
  ZYM_PFX_NONE = 0u,
  ZYM_PFX_IX = 1u<<0,
  ZYM_PFX_IY = 1u<<1,
  ZYM_PFX_ED = 1u<<2,
  ZYM_PFX_CB = 1u<<3,
};


/* registers affected by the instruction. not fully tested yet, may be buggy */
/* note that EXX and EX AF,AFX doesn't "affect" registers, and have separate flags*/
/* NOT IMPLEMENTED YET! */
/*
enum {
  ZYM_REGMOD_NONE = 0u,
  ZYM_REGMOD_B = 1u<<0,
  ZYM_REGMOD_C = 1u<<1,
  ZYM_REGMOD_D = 1u<<2,
  ZYM_REGMOD_E = 1u<<3,
  ZYM_REGMOD_H = 1u<<4,
  ZYM_REGMOD_L = 1u<<5,
  ZYM_REGMOD_F = 1u<<6,
  ZYM_REGMOD_A = 1u<<7,
  ZYM_REGMOD_XH = 1u<<8,
  ZYM_REGMOD_XL = 1u<<9,
  ZYM_REGMOD_YH = 1u<<10,
  ZYM_REGMOD_YL = 1u<<11,
  ZYM_REGMOD_SP = 1u<<12,
  // EXX
  ZYM_REGMOD_EXX = 1u<<13,
  // EX AF,AFX
  ZYM_REGMOD_EXAF = 1u<<14,
  ZYM_REGMOD_I = 1u<<15,
  ZYM_REGMOD_R = 1u<<16,

  // convenient masks
  ZYM_REGMOD_BC = ZYM_REGMOD_B|ZYM_REGMOD_C,
  ZYM_REGMOD_DE = ZYM_REGMOD_D|ZYM_REGMOD_E,
  ZYM_REGMOD_HL = ZYM_REGMOD_H|ZYM_REGMOD_L,
  ZYM_REGMOD_AF = ZYM_REGMOD_A|ZYM_REGMOD_F,
  ZYM_REGMOD_IX = ZYM_REGMOD_XH|ZYM_REGMOD_XL,
  ZYM_REGMOD_IY = ZYM_REGMOD_YH|ZYM_REGMOD_YL,
};
*/

typedef struct {
  int inslen; /* instruction length */
  zym_bool memrwword; /* !0: reading word (if reading anything at all) */
  int memread; /* ZYM_MEMIO_xxx or addr */
  int memwrite; /* ZYM_MEMIO_xxx or addr */
  int jump; /* Z80_JUMP_xxx or addr */
  int cond; /* Z80_COND_xxx */
  int portread; /* Z80_PORTIO_xxx or addr; if addr is specified, high byte must be taken from A */
  int portwrite; /* Z80_PORTIO_xxx or addr; if addr is specified, high byte must be taken from A */
  int push; /* Z80_STK_xxx; CALL/RST will set ZYM_STK_PC */
  int pop; /* Z80_STK_xxx; RET will set ZYM_STK_PC */
  int disp; /* for (IX+n) / (IY+n), else 0xffff */
  int trap; /* slt and other trap opcode or -1 */
  unsigned prefix; /* ZYM_PFX_XXX */
  /* NOT IMPLEMENTED YET! */
  /*unsigned regread;*/ /* registers read by this instruction */
  /*unsigned regwrite;*/ /* registers wrote by this instruction */
} zym_op_meminfo_t;


void zym_op_meminfo (zym_cpu_t *z80, zym_op_meminfo_t *nfo, uint16_t pc);


/******************************************************************************/
/* very simple disassembler */
/******************************************************************************/
// disasm flags
enum {
  ZYM_DIS_FLAGS_DEFAULT     = 0u,
  // use lower-cased text for mnemonics
  ZYM_DIS_FLAG_LOCASE_MNEMO = 1u<<0,
  // use lower-cased text for arguments
  ZYM_DIS_FLAG_LOCASE_ARGS  = 1u<<1,
  // use lower-cased text for hex numbers
  ZYM_DIS_FLAG_LOCASE_HEX   = 1u<<2,
  // use decimal numbers instead of hex?
  ZYM_DIS_FLAG_DECIMAL      = 1u<<3,
  // if hex, use "0x" instead of "#"?
  ZYM_DIS_FLAG_HEX_CLIKE    = 1u<<4,
};

typedef struct  {
  int inslen; // instruction length in bytes (NOT disassembly string length!)
  char disbuf[128]; // buffer that holds disassembled text; mnemonic is separated by '\t'
  const char *mnemo; // points into disbuf
  int mnemolen;
  const char *args[3]; // points into disbuf
  int argslen[3];
  int tstates[2]; // tstates for taken/not taken; equal for non-branch commands
  // disassembler options
  unsigned flags; // see above
} zym_disop_t;


/* size: 1 or 2 -- size in bytes */
/* asaddr: !0 if this is for CALL/JP */
enum {
  ZYM_DIS_ATYPE_BYTE = 0, /* immediate byte value */
  ZYM_DIS_ATYPE_WORD = 1, /* immediate word value */
  ZYM_DIS_ATYPE_PC_ADDR = 2, /* CALL/JP/JR/DJNZ address */
  ZYM_DIS_ATYPE_DATA_ADDR = 3, /* LD (nnn) address */
  ZYM_DIS_ATYPE_PORT8 = 4,
  ZYM_DIS_ATYPE_OFFSET = 5, /* LD (ix+n) address */
};

// for offsets, value is signed 2-complement
typedef const char *(*zym_disasm_getlabel_fn) (uint16_t value, int type/*ZYM_DIS_ATYPE_XXX*/);
extern zym_disasm_getlabel_fn zym_disasm_getlabel;

extern const char *zym_disasm_mnemo_end; /* default is NULL ("\t") */
extern const char *zym_disasm_num_start; /* default is NULL */
extern const char *zym_disasm_num_end; /* default is NULL */

/* you may skip calling this, and only set `flags` */
void zym_disasm_init (zym_disop_t *nfo);

void zym_disasm_one_ex (zym_disop_t *nfo, const uint8_t mem[8], uint16_t pc);
void zym_disasm_one (zym_cpu_t *z80, zym_disop_t *nfo, uint16_t pc);


#ifdef __cplusplus
}
#endif
#endif
