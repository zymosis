// URASM Z80 assembler
// coded by Ketmar // Invisible Vector
// GPLv3 or later
//
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "zymosis_utils.h"

static int urasm_allow_zxnext = 0;


static uint8_t memory[65536];


static uint8_t getByte (uint16_t addr) {
  return memory[addr];
}


static const char *cmdName (const char *s) {
  static char cmd[16];
  const char *t = strchr(s, '\t');
  if (!t) return s;
  memset(cmd, 0, sizeof(cmd));
  memmove(cmd, s, t-s);
  while (strlen(cmd) < 5+(urasm_allow_zxnext ? 3 : 0)) strcat(cmd, " ");
  return cmd;
}


static const char *cmdArgs (const char *s) {
  const char *t = strchr(s, '\t');
  if (!t) return s+strlen(s);
  return t+1;
}


int main (int argc, char *argv[]) {
  uint16_t pc = 0x0100U;//0xECC8UL;
  int maxlen = 1416;
  int len, len1, f;
  for (int aidx = 1; aidx < argc; ++aidx) {
    if (strcmp(argv[aidx], "--") == 0) {
      for (int c = aidx+1; c < argc; ++c) argv[c-1] = argv[c];
      --argc;
      break;
    }
    if (strcmp(argv[aidx], "--z80") == 0 || strcmp(argv[aidx], "--z80a") == 0) {
      for (int c = aidx+1; c < argc; ++c) argv[c-1] = argv[c];
      --argc;
      urasm_allow_zxnext = 0;
      --aidx;
      continue;
    }
    if (strcmp(argv[aidx], "--z80n") == 0 || strcmp(argv[aidx], "--z80next") == 0 ||
        strcmp(argv[aidx], "--zxnext") == 0)
    {
      for (int c = aidx+1; c < argc; ++c) argv[c-1] = argv[c];
      --argc;
      urasm_allow_zxnext = 1;
      --aidx;
      continue;
    }
  }
  FILE *fl = fopen(argc>1?argv[1]:"zout_0100.bin", "rb");
  if (!fl) { fprintf(stderr, "ERROR: can't open input file!\n"); return 1; }
  if (argc > 2) pc = (strtol(argv[2], NULL, 0))&0xffff;
  maxlen = fread(memory+pc, 1, 65536-pc, fl);
  fclose(fl);
  //
  //urasm_getbyte = getByte;
  while (maxlen > 0) {
    if (pc >= 65535-16) break;
    /*
    idx = urasm_disasm_opfind(pc);
    //if (idx < 0) break;
    len = urasm_disasm_oplen(idx);
    len1 = urasm_disasm_opdisasm(dstr, pc);
    printf("%04X: [", pc);
    for (f = 0; f < len1; ++f) { if (f) putchar(' '); printf("%02X", memory[pc+f]); }
    for (; f < 4; ++f) { if (f) putchar(' '); printf("  "); }
    printf("] idx=%3d; len=%d; len1=%d; [%s%s]\n", idx, len, len1, cmdName(dstr), cmdArgs(dstr));
    if (len != len1) abort();
    if (len <= 0) abort();
    */
    uint8_t mem[8];
    zym_disop_t disop;
    /*zym_disasm_init(&disop);*/
    disop.flags = 0;
    for (unsigned f = 0; f < 8; ++f) mem[f] = getByte((pc+f)&0xffff);
    zym_disasm_one_ex(&disop, mem, pc);
    len1 = disop.inslen;
    len = len1;
    printf("%04X: [", pc);
    for (f = 0; f < len1; ++f) { if (f) putchar(' '); printf("%02X", memory[pc+f]); }
    for (; f < 4; ++f) { if (f) putchar(' '); printf("  "); }
    printf("] len=%d; len1=%d; [%s%s]\n", len, len1, cmdName(disop.disbuf), cmdArgs(disop.disbuf));
    if (len != len1) abort();
    if (len <= 0) abort();
    pc += len1; maxlen -= len1;
  }

  return 0;
}
