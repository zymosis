/*
 * Z80 CPU emulation engine v0.1.2.1
 * coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
#include <stdlib.h>
#include <string.h>

#include "zymosis.h"

#if defined(__GNUC__)
# ifndef ZYMOSIS_INLINE
#  define ZYMOSIS_INLINE  static __attribute__((always_inline)) inline
# endif
# ifndef ZYMOSIS_PURE
#  define ZYMOSIS_PURE  __attribute__((pure))
# endif
# ifndef ZYMOSIS_CONST
#  define ZYMOSIS_CONST  __attribute__((const))
# endif
# ifndef ZYMOSIS_FUNC
#  define ZYMOSIS_FUNC  __attribute__((warn_unused_result))
# endif
#endif

#ifndef ZYMOSIS_INLINE
# define ZYMOSIS_INLINE  static
#endif
#ifndef ZYMOSIS_PURE
# define ZYMOSIS_PURE
#endif
#ifndef ZYMOSIS_CONST
# define ZYMOSIS_CONST
#endif
#ifndef ZYMOSIS_FUNC
# define ZYMOSIS_FUNC
#endif


/* casting negative integer to unsigned is UB; sigh */
#ifdef ZYMOSIS_TRY_TO_AVOID_UB
# define ZADD_WX(n,v)  ((uint16_t)(((v) >= 0 ? ((uint32_t)(n)+(uint32_t)(v)) : ((uint32_t)(n)-(uint32_t)(-v)))))
#else
# define ZADD_WX(n,v)  ((uint16_t)((uint16_t)(n) + (uint16_t)(v)))
#endif

#define INC_R  (z80->regR = ((z80->regR+1u)&0x7fu)|(z80->regR&0x80u))
#define DEC_R  (z80->regR = ((z80->regR-1u)&0x7fu)|(z80->regR&0x80u))


/******************************************************************************/
/* some funny tables */
static int32_t tables_inited = 0;
static uint8_t parity_tbl[256];
static uint8_t sz53_tbl[256];  /* bits 3, 5 and 7 of result, Z flag */
static uint8_t sz53p_tbl[256]; /* bits 3, 5 and 7 of result, Z and P flags */


//**************************************************************************
//
// various initialisations
//
//**************************************************************************

// initialise internal flag tables
// WARNING! this is NOT thread-safe!
void zym_init_tables (void) {
  if (tables_inited) return;
  for (unsigned f = 0; f < 256; ++f) {
    unsigned n, p;
    sz53_tbl[f] = (f&ZYM_FLAG_S35);
    for (n = f, p = 0; n != 0; n >>= 1) p ^= n&0x01u;
    parity_tbl[f] = (p ? 0 : ZYM_FLAG_PV);
    sz53p_tbl[f] = (sz53_tbl[f]|parity_tbl[f]);
  }
  sz53_tbl[0] |= ZYM_FLAG_Z;
  sz53p_tbl[0] |= ZYM_FLAG_Z;
  tables_inited = 1;
}


#if defined(__GNUC__)
static __attribute__((constructor)) void zym_init_tables_ctor (void) {
  zym_init_tables();
}
#endif


// "no memory contention" callback
void zym_no_mem_contention_cb (zym_cpu_t *z80, uint16_t addr,
                               zym_memio_t mio, zym_memreq_t mreq)
{
}


// should be called on new instance of zym_cpu_t to clear all callbacks
void zym_clear_callbacks (zym_cpu_t *z80) {
  if (!tables_inited) zym_init_tables();
  z80->mem_read = NULL;
  z80->mem_write = NULL;
  z80->mem_contention = &zym_no_mem_contention_cb;
  z80->port_read = NULL;
  z80->port_write = NULL;
  z80->port_contention = NULL;
  z80->trap_halt = NULL;
  z80->trap_reti = NULL;
  z80->trap_retn = NULL;
  z80->trap_edfb = NULL;
  z80->trap_edfe = NULL;
}


// clear all flags and callbacks; will reset 'z80->user' too
void zym_init (zym_cpu_t *z80) {
  if (!tables_inited) zym_init_tables();
  memset(z80, 0, sizeof(*z80));
  // there is no guarantee that NULL is 0 in the standard (sigh)
  zym_clear_callbacks(z80);
  z80->user = NULL;
  z80->next_event_tstate = -1;
}


// resets Z80 CPU
// seems that all regs (and memptr) should be set to 'all 1' here, but i don't care
void zym_reset (zym_cpu_t *z80) {
  if (!tables_inited) zym_init_tables();
  z80->bc.w = z80->de.w = z80->hl.w = z80->af.w = z80->sp.w = z80->ix.w = z80->iy.w = 0;
  z80->bcx.w = z80->dex.w = z80->hlx.w = z80->afx.w = 0;
  z80->pc = z80->prev_pc = z80->org_pc = 0;
  z80->memptr.w = 0;
  z80->regI = z80->regR = 0;
  z80->iff1 = z80->iff2 = 0;
  z80->im = 0;
  z80->halted = 0;
  z80->prev_was_EIDDR = 0;
  z80->tstates = 0;
  z80->dd = &z80->hl;
  z80->flags_q = 0;
}


//**************************************************************************
//
// alternate register set functions
//
//**************************************************************************

ZYMOSIS_INLINE void zym_exx_internal (zym_cpu_t *z80) {
  uint16_t t = z80->bc.w; z80->bc.w = z80->bcx.w; z80->bcx.w = t;
  t = z80->de.w; z80->de.w = z80->dex.w; z80->dex.w = t;
  t = z80->hl.w; z80->hl.w = z80->hlx.w; z80->hlx.w = t;
}


ZYMOSIS_INLINE void zym_exaf_internal (zym_cpu_t *z80) {
  const uint16_t t = z80->af.w; z80->af.w = z80->afx.w; z80->afx.w = t;
}


// swap normal and alternate register sets (except AF/AF')
void zym_exx (zym_cpu_t *z80) {
  zym_exx_internal(z80);
}


// swap normal and alternate AF
void zym_exaf (zym_cpu_t *z80) {
  zym_exaf_internal(z80);
}


/******************************************************************************/
/* simulate contented memory access */
/* (tstates = tstates+contention+1)*cnt */
/* (zym_cpu_t *z80, uint16_t addr, int tstates, zym_memio_t mio) */
#define z80_contention(z80_,addr_,mio_,mrq_)  do { \
  (z80_)->mem_contention((z80_), (addr_), (mio_), (mrq_)); \
} while (0)


#define z80_contention_by1ts(z80_,addr_,cnt_)  do { \
  const uint16_t xaddr_ = (addr_); \
  uint32_t fcnt_ = (cnt_); \
  do { \
    fcnt_ -= 1; \
    (z80_)->mem_contention((z80_), xaddr_, ZYM_MEMIO_OTHER, ZYM_MREQ_NONE); \
    (z80_)->tstates += 1; \
  } while (fcnt_ != 0); \
} while (0)


#define z80_contention_by1ts_ir(z80_,cnt_)  z80_contention_by1ts((z80_), (((uint16_t)(z80_)->regI)<<8)|((z80_)->regR), (cnt_))
#define z80_contention_by1ts_pc(z80_,cnt_)  z80_contention_by1ts((z80_), (z80_)->pc, (cnt_))


/******************************************************************************/
ZYMOSIS_INLINE uint8_t z80_port_read (zym_cpu_t *z80, const uint16_t port) {
  uint8_t value;
  /* i don't really know: FUSE tester seems to do port read in-between contentions,
     but the actual emulator code does it after all contentions are done.
     i guess that doing it in-between is the right way.
     note: ok, it broke floating bus emulation in ZXEmuT. oopsie!
   */
  if (z80->port_contention != NULL) {
    z80->port_contention(z80, port, 1, ZYM_PORTIO_FLAG_IN|ZYM_PORTIO_FLAG_EARLY);
    z80->port_read_tstates = z80->tstates;
    #ifdef ZYMOSIS_FUSE_TEST
    /* for FUSE tests, it should be here */
    value = z80->port_read(z80, port, ZYM_PORTIO_NORMAL);
    #endif
    z80->port_contention(z80, port, 2, ZYM_PORTIO_FLAG_IN);
    #ifndef ZYMOSIS_FUSE_TEST
    /* for ZXEmuT floating bus emulation, it should be here */
    value = z80->port_read(z80, port, ZYM_PORTIO_NORMAL);
    #endif
  } else {
    z80->tstates += 3;
    z80->port_read_tstates = z80->tstates-2;
    value = z80->port_read(z80, port, ZYM_PORTIO_NORMAL);
  }
  ++z80->tstates;
  return value;
}


ZYMOSIS_INLINE void z80_port_write (zym_cpu_t *z80, const uint16_t port, const uint8_t value) {
  if (z80->port_contention != NULL) {
    z80->port_contention(z80, port, 1, ZYM_PORTIO_FLAG_EARLY);
    z80->port_write(z80, port, value, ZYM_PORTIO_NORMAL);
    z80->port_contention(z80, port, 2, 0);
    ++z80->tstates;
  } else {
    ++z80->tstates;
    z80->port_write(z80, port, value, ZYM_PORTIO_NORMAL);
    z80->tstates += 3;
  }
}


/******************************************************************************/
#define z80_peekb_i(z80_,addr_)  (z80_)->mem_read((z80_), (addr_), ZYM_MEMIO_OTHER)
#define z80_peekb(z80_,addr_)    (z80_)->mem_read((z80_), (addr_), ZYM_MEMIO_DATA)

#define z80_pokeb_i(z80_,addr_,byte_)  (z80_)->mem_write((z80_), (addr_), (byte_), ZYM_MEMIO_OTHER)
#define z80_pokeb(z80_,addr_,byte_)    (z80_)->mem_write((z80_), (addr_), (byte_), ZYM_MEMIO_DATA)


#ifdef ZYMOSIS_TRY_TO_AVOID_UB
# define EVEN_M1_TO_T3()  do { \
  if (z80->evenM1) { \
    if ((z80->tstates&0x01) != 0) z80->tstates += 1; \
  } \
  z80->tstates += 2; \
while (0)
#else
# define EVEN_M1_TO_T3()  do { \
  if (z80->evenM1) { \
    z80->tstates = (int32_t)((uint32_t)(z80->tstates + 1) & ~(uint32_t)1); \
  } \
  z80->tstates += 2; \
} while (0)
#endif

/* read opcode after prefix */
/* read opcode -- OCR(4) */
/* t1: setting /MREQ & /RD */
/* t2: memory read */
/* t3: sampilng the data bus, command decoding, refresh, increment R */
/* t4: command decoding, refresh */
ZYMOSIS_INLINE uint8_t zym_get_opcode_ext (zym_cpu_t *z80) {
  #ifdef ZYMOSIS_FUSE_TEST
  /* for FUSE tests, it should be here */
  z80_contention(z80, z80->pc, ZYM_MEMIO_OPCEXT, ZYM_MREQ_READ);
  z80->tstates += 4;
  const uint8_t opc = z80->mem_read(z80, z80->pc, ZYM_MEMIO_OPCEXT);
  #else
  EVEN_M1_TO_T3();
  z80_contention(z80, z80->pc, ZYM_MEMIO_OPCEXT, ZYM_MREQ_READ);
  const uint8_t opc = z80->mem_read(z80, z80->pc, ZYM_MEMIO_OPCEXT);
  z80->tstates += 2; // skip t3 and t4
  #endif
  INC_R;
  z80->pc += 1;
  return opc;
}


// t1: setting /MREQ & /RD
// t2: wait
// t3: Z80 sampling the data bus
ZYMOSIS_INLINE uint8_t z80_mr_3ts (zym_cpu_t *z80, const uint16_t addr) {
  #ifdef ZYMOSIS_FUSE_TEST
  /* for FUSE tests, it should be here */
  z80_contention(z80, addr, ZYM_MEMIO_DATA, ZYM_MREQ_READ);
  z80->tstates += 3;
  const uint8_t res = z80->mem_read(z80, addr, ZYM_MEMIO_DATA);
  #else
  z80->tstates += 2; // at t3
  z80_contention(z80, addr, ZYM_MEMIO_DATA, ZYM_MREQ_READ);
  const uint8_t res = z80->mem_read(z80, addr, ZYM_MEMIO_DATA);
  z80->tstates += 1; // skip t3
  #endif
  return res;
}

// MR cycle for instruction arguments
ZYMOSIS_INLINE uint8_t z80_mr_3ts_args (zym_cpu_t *z80, const uint16_t addr) {
  #ifdef ZYMOSIS_FUSE_TEST
  /* for FUSE tests, it should be here */
  z80_contention(z80, addr, ZYM_MEMIO_OPCARG, ZYM_MREQ_READ);
  z80->tstates += 3;
  const uint8_t res = z80->mem_read(z80, addr, ZYM_MEMIO_OPCARG);
  #else
  z80->tstates += 2; // at t3
  z80_contention(z80, addr, ZYM_MEMIO_OPCARG, ZYM_MREQ_READ);
  const uint8_t res = z80->mem_read(z80, addr, ZYM_MEMIO_OPCARG);
  z80->tstates += 1; // skip t3
  #endif
  return res;
}

// 16-bit memory read: two MRs
ZYMOSIS_INLINE uint16_t z80_peekw_6ts (zym_cpu_t *z80, const uint16_t addr) {
  const uint16_t res = z80_mr_3ts(z80, addr);
  return (uint16_t)(res|(((uint16_t)z80_mr_3ts(z80, addr+1u))<<8));
}

// t1: setting /MREQ & /WR
// t2: wait (the data bus is set)
// t3: t3 ;-)
ZYMOSIS_INLINE void z80_pokeb_3ts (zym_cpu_t *z80, const uint16_t addr, const uint8_t value) {
  #ifdef ZYMOSIS_FUSE_TEST
  /* for FUSE tests, it should be here */
  z80_contention(z80, addr, ZYM_MEMIO_DATA, ZYM_MREQ_WRITE);
  z80->tstates += 3;
  z80_pokeb(z80, addr, value);
  #else
  z80->tstates += 2; // at t3
  z80_contention(z80, addr, ZYM_MEMIO_DATA, ZYM_MREQ_WRITE);
  z80_pokeb(z80, addr, value);
  z80->tstates += 1; // skip t3
  #endif
}

ZYMOSIS_INLINE void z80_pokew_6ts (zym_cpu_t *z80, const uint16_t addr, const uint16_t value) {
  z80_pokeb_3ts(z80, addr, value&0xffu);
  z80_pokeb_3ts(z80, addr+1u, (value>>8)&0xffu);
}

ZYMOSIS_INLINE void z80_pokew_6ts_inverted (zym_cpu_t *z80, const uint16_t addr, const uint16_t value) {
  z80_pokeb_3ts(z80, addr+1u, (value>>8)&0xffu);
  z80_pokeb_3ts(z80, addr, value&0xffu);
}

// read 16-bit instruction argument
ZYMOSIS_INLINE uint16_t z80_getpcw (zym_cpu_t *z80, const zym_bool wait1) {
  uint16_t res = z80_mr_3ts_args(z80, z80->pc);
  z80->pc += 1u;
  res |= ((uint16_t)z80_mr_3ts_args(z80, z80->pc))<<8;
  if (wait1) z80_contention_by1ts_pc(z80, 1);
  z80->pc += 1u;
  return res;
}


ZYMOSIS_INLINE uint16_t z80_pop_6ts (zym_cpu_t *z80) {
  uint16_t res = z80_mr_3ts(z80, z80->sp.w);
  z80->sp.w += 1u;
  res |= ((uint16_t)z80_mr_3ts(z80, z80->sp.w))<<8;
  z80->sp.w += 1u;
  return res;
}

/* 3 T states write high byte of PC to the stack and decrement SP */
/* 3 T states write the low byte of PC and jump to #0066 */
ZYMOSIS_INLINE void z80_push_6ts (zym_cpu_t *z80, uint16_t value) {
  z80->sp.w -= 1u;
  z80_pokeb_3ts(z80, z80->sp.w, (value>>8)&0xffu);
  z80->sp.w -= 1u;
  z80_pokeb_3ts(z80, z80->sp.w, value&0xffu);
}


/******************************************************************************/
/* you are not expected to understand the following bit-mess */
/* the only thing you want to know that IT WORKS; just believe me and the testing suite */
/* ok, you can consult Z80 manuals to find the affected flags */
/* but believe me, it is unnecessary */

ZYMOSIS_INLINE void ZYM_ADC_A (zym_cpu_t *z80, const uint8_t b) {
  const uint16_t o = z80->af.a;
  uint16_t newv;
  z80->af.a = (newv = o+b+(z80->af.f&ZYM_FLAG_C))&0xffu; /* ZYM_FLAG_C is 0x01u, so it's safe */
  z80->flags_q = z80->af.f =
    sz53_tbl[newv&0xffu]|
    (newv > 0xffu ? ZYM_FLAG_C : 0)|
    ((o^(~b))&(o^newv)&0x80u ? ZYM_FLAG_PV : 0)|
    ((o&0x0fu)+(b&0x0fu)+(z80->af.f&ZYM_FLAG_C) >= 0x10u ? ZYM_FLAG_H : 0);
}

ZYMOSIS_INLINE void ZYM_SBC_A (zym_cpu_t *z80, const uint8_t b) {
  const uint16_t o = z80->af.a;
  uint16_t newv;
  z80->af.a = (newv = ((uint32_t)o-(uint32_t)b-(uint32_t)(z80->af.f&ZYM_FLAG_C))&0xffffu)&0xffu; /* ZYM_FLAG_C is 0x01u, so it's safe */
  z80->flags_q = z80->af.f =
    ZYM_FLAG_N|
    sz53_tbl[newv&0xffu]|
    (newv > 0xffu ? ZYM_FLAG_C : 0)|
    ((o^b)&(o^newv)&0x80u ? ZYM_FLAG_PV : 0)|
    ((int32_t)(o&0x0fu)-(int32_t)(b&0x0fu)-(int32_t)(z80->af.f&ZYM_FLAG_C) < 0 ? ZYM_FLAG_H : 0);
}

ZYMOSIS_INLINE void ZYM_ADD_A (zym_cpu_t *z80, const uint8_t b) {
  z80->af.f &= ~ZYM_FLAG_C;
  ZYM_ADC_A(z80, b);
}

ZYMOSIS_INLINE void ZYM_SUB_A (zym_cpu_t *z80, const uint8_t b) {
  z80->af.f &= ~ZYM_FLAG_C;
  ZYM_SBC_A(z80, b);
}

ZYMOSIS_INLINE void ZYM_CP_A (zym_cpu_t *z80, const uint8_t b) {
  const uint8_t o = z80->af.a;
  const uint8_t newv = ((uint32_t)o-(uint32_t)b)&0xffu;
  z80->flags_q = z80->af.f =
    ZYM_FLAG_N|
    (newv&ZYM_FLAG_S)|
    (b&ZYM_FLAG_35)|
    (newv == 0 ? ZYM_FLAG_Z : 0)|
    (o < b ? ZYM_FLAG_C : 0)|
    ((o^b)&(o^newv)&0x80u ? ZYM_FLAG_PV : 0)|
    ((int32_t)(o&0x0fu)-(int32_t)(b&0x0fu) < 0 ? ZYM_FLAG_H : 0);
}


#define ZYM_AND_A(z80_,b_)  ((z80_)->flags_q = (z80_)->af.f = sz53p_tbl[(z80_)->af.a&=(b_)]|ZYM_FLAG_H)
#define ZYM_OR_A(z80_,b_)   ((z80_)->flags_q = (z80_)->af.f = sz53p_tbl[(z80_)->af.a|=(b_)])
#define ZYM_XOR_A(z80_,b_)  ((z80_)->flags_q = (z80_)->af.f = sz53p_tbl[(z80_)->af.a^=(b_)])


/* carry unchanged */
ZYMOSIS_INLINE uint8_t ZYM_DEC8 (zym_cpu_t *z80, const uint8_t b) {
  z80->af.f &= ZYM_FLAG_C;
  z80->af.f |= ZYM_FLAG_N|
    (b == 0x80u ? ZYM_FLAG_PV : 0)|
    (b&0x0fu ? 0 : ZYM_FLAG_H)|
    sz53_tbl[(((uint32_t)b)-1u)&0xffu];
  z80->flags_q = z80->af.f;
  return (((uint32_t)b)-1u)&0xffu;
}

/* carry unchanged */
ZYMOSIS_INLINE uint8_t ZYM_INC8 (zym_cpu_t *z80, const uint8_t b) {
  z80->af.f &= ZYM_FLAG_C;
  z80->af.f |=
    (b == 0x7fu ? ZYM_FLAG_PV : 0)|
    ((b+1u)&0x0fu ? 0 : ZYM_FLAG_H)|
    sz53_tbl[(b+1u)&0xffu];
  z80->flags_q = z80->af.f;
  return ((b+1u)&0xffu);
}

/* cyclic, carry reflects shifted bit */
ZYMOSIS_INLINE void ZYM_RLCA (zym_cpu_t *z80) {
  const uint8_t c = ((z80->af.a>>7)&0x01u);
  z80->af.a = (z80->af.a<<1)|c;
  z80->flags_q = z80->af.f = c|(z80->af.a&ZYM_FLAG_35)|(z80->af.f&(ZYM_FLAG_PV|ZYM_FLAG_Z|ZYM_FLAG_S));
}

/* cyclic, carry reflects shifted bit */
ZYMOSIS_INLINE void ZYM_RRCA (zym_cpu_t *z80) {
  const uint8_t c = (z80->af.a&0x01u);
  z80->af.a = (z80->af.a>>1)|(c<<7);
  z80->flags_q = z80->af.f = c|(z80->af.a&ZYM_FLAG_35)|(z80->af.f&(ZYM_FLAG_PV|ZYM_FLAG_Z|ZYM_FLAG_S));
}

/* cyclic thru carry */
ZYMOSIS_INLINE void ZYM_RLA (zym_cpu_t *z80) {
  const uint8_t c = ((z80->af.a>>7)&0x01u);
  z80->af.a = (z80->af.a<<1)|(z80->af.f&ZYM_FLAG_C);
  z80->flags_q = z80->af.f = c|(z80->af.a&ZYM_FLAG_35)|(z80->af.f&(ZYM_FLAG_PV|ZYM_FLAG_Z|ZYM_FLAG_S));
}

/* cyclic thru carry */
ZYMOSIS_INLINE void ZYM_RRA (zym_cpu_t *z80) {
  const uint8_t c = (z80->af.a&0x01u);
  z80->af.a = (z80->af.a>>1)|((z80->af.f&ZYM_FLAG_C)<<7);
  z80->flags_q = z80->af.f = c|(z80->af.a&ZYM_FLAG_35)|(z80->af.f&(ZYM_FLAG_PV|ZYM_FLAG_Z|ZYM_FLAG_S));
}

/* cyclic thru carry */
ZYMOSIS_INLINE uint8_t ZYM_RL (zym_cpu_t *z80, uint8_t b) {
  const uint8_t c = (b>>7)&ZYM_FLAG_C;
  z80->flags_q = z80->af.f = sz53p_tbl[(b = ((b<<1)&0xffu)|(z80->af.f&ZYM_FLAG_C))]|c;
  return b;
}

ZYMOSIS_INLINE uint8_t ZYM_RR (zym_cpu_t *z80, uint8_t b) {
  const uint8_t c = (b&0x01u);
  z80->flags_q = z80->af.f = sz53p_tbl[(b = (b>>1)|((z80->af.f&ZYM_FLAG_C)<<7))]|c;
  return b;
}

/* cyclic, carry reflects shifted bit */
ZYMOSIS_INLINE uint8_t ZYM_RLC (zym_cpu_t *z80, uint8_t b) {
  const uint8_t c = ((b>>7)&ZYM_FLAG_C);
  z80->flags_q = z80->af.f = sz53p_tbl[(b = ((b<<1)&0xffu)|c)]|c;
  return b;
}

/* cyclic, carry reflects shifted bit */
ZYMOSIS_INLINE uint8_t ZYM_RRC (zym_cpu_t *z80, uint8_t b) {
  const uint8_t c = (b&0x01u);
  z80->flags_q = z80->af.f = sz53p_tbl[(b = (b>>1)|(c<<7))]|c;
  return b;
}

/* shift left arithmetic, sets bit 0 to zero, carry reflects shifted bit */
ZYMOSIS_INLINE uint8_t ZYM_SLA (zym_cpu_t *z80, uint8_t b) {
  uint8_t c = ((b>>7)&0x01u);
  z80->flags_q = z80->af.f = sz53p_tbl[(b <<= 1)]|c;
  return b;
}

/* shift right arithmetic, sets bit 6 to bit 7, carry reflects shifted bit */
ZYMOSIS_INLINE uint8_t ZYM_SRA (zym_cpu_t *z80, uint8_t b) {
  const uint8_t c = (b&0x01u);
  z80->flags_q = z80->af.f = sz53p_tbl[(b = (b>>1)|(b&0x80u))]|c;
  return b;
}

/* shift left "logic" (set), sets bit 0 to one, carry reflects shifted bit */
ZYMOSIS_INLINE uint8_t ZYM_SLS (zym_cpu_t *z80, uint8_t b) {
  const uint8_t c = ((b>>7)&0x01u);
  z80->flags_q = z80->af.f = sz53p_tbl[(b = (b<<1)|0x01u)]|c;
  return b;
}

/* shift right logic, sets bit 7 to zero, carry reflects shifted bit */
ZYMOSIS_INLINE uint8_t ZYM_SRL (zym_cpu_t *z80, uint8_t b) {
  const uint8_t c = (b&0x01u);
  z80->flags_q = z80->af.f = sz53p_tbl[(b >>= 1)]|c;
  return b;
}

/* ddvalue+value */
ZYMOSIS_INLINE uint16_t ZYM_ADD_DD (zym_cpu_t *z80, const uint16_t value, const uint16_t ddvalue) {
  /* `static` is more restrective here, simple `const` should generate better code */
  /*static*/ const uint8_t hct[8] = { 0, ZYM_FLAG_H, ZYM_FLAG_H, ZYM_FLAG_H, 0, 0, 0, ZYM_FLAG_H };
  const uint32_t res = (uint32_t)value+(uint32_t)ddvalue;
  const uint8_t b = ((value&0x0800u)>>11)|((ddvalue&0x0800u)>>10)|((res&0x0800u)>>9);
  z80->memptr.w = ddvalue+1u;
  z80->flags_q = z80->af.f =
    (z80->af.f&(ZYM_FLAG_PV|ZYM_FLAG_Z|ZYM_FLAG_S))|
    (res > 0xffffu ? ZYM_FLAG_C : 0)|
    ((res>>8)&ZYM_FLAG_35)|
    hct[b];
  return res;
}

/* ddvalue+value */
ZYMOSIS_INLINE uint16_t ZYM_ADC_DD (zym_cpu_t *z80, const uint16_t value, const uint16_t ddvalue) {
  const uint8_t c = (z80->af.f&ZYM_FLAG_C);
  const uint32_t newv = (uint32_t)value+(uint32_t)ddvalue+(uint32_t)c;
  const uint16_t res = (uint16_t)newv;
  z80->memptr.w = ddvalue+1u;
  z80->flags_q = z80->af.f =
    ((res>>8)&ZYM_FLAG_S35)|
    (res == 0 ? ZYM_FLAG_Z : 0)|
    (newv > 0xffffu ? ZYM_FLAG_C : 0)|
    ((value^((~ddvalue)&0xffffu))&(value^newv)&0x8000u ? ZYM_FLAG_PV : 0)|
    ((value&0x0fffu)+(ddvalue&0x0fffu)+c >= 0x1000u ? ZYM_FLAG_H : 0);
  return res;
}

/* ddvalue-value */
ZYMOSIS_INLINE uint16_t ZYM_SBC_DD (zym_cpu_t *z80, const uint16_t value, const uint16_t ddvalue) {
  const uint8_t tmpB = z80->af.a;
  //FIXME: MEMPTR docs says that it is still +1 here; why?
  //FIXME: is this a typo, or is it really working like this?
  z80->memptr.w = ddvalue+1u;
  z80->af.a = ddvalue&0xffu;
  ZYM_SBC_A(z80, value&0xffu);
  uint16_t res = z80->af.a;
  z80->af.a = (ddvalue>>8)&0xffu;
  ZYM_SBC_A(z80, (value>>8)&0xffu);
  res |= (z80->af.a<<8);
  z80->af.a = tmpB;
  z80->flags_q = z80->af.f = (res ? z80->af.f&(~ZYM_FLAG_Z) : z80->af.f|ZYM_FLAG_Z);
  return res;
}

ZYMOSIS_INLINE void ZYM_BIT (zym_cpu_t *z80, const uint8_t bit, const uint8_t num, const zym_bool mptr) {
  z80->af.f =
    ZYM_FLAG_H|
    (z80->af.f&ZYM_FLAG_C)|
    (num&ZYM_FLAG_35)|
    (num&(1<<bit) ? 0 : ZYM_FLAG_PV|ZYM_FLAG_Z)|
    (bit == 7 ? num&ZYM_FLAG_S : 0);
  if (mptr) z80->af.f = (z80->af.f&~ZYM_FLAG_35)|(z80->memptr.h&ZYM_FLAG_35);
  z80->flags_q = z80->af.f;
}

/* we can do this with table lookup, but why bother? */
ZYMOSIS_INLINE void ZYM_DAA (zym_cpu_t *z80) {
  uint8_t tmpI = 0, tmpC = (z80->af.f&ZYM_FLAG_C), tmpA = z80->af.a;
  if ((z80->af.f&ZYM_FLAG_H) || (tmpA&0x0fu) > 9) tmpI = 6;
  if (tmpC != 0 || tmpA > 0x99u) tmpI |= 0x60u;
  if (tmpA > 0x99u) tmpC = ZYM_FLAG_C;
  if (z80->af.f&ZYM_FLAG_N) ZYM_SUB_A(z80, tmpI); else ZYM_ADD_A(z80, tmpI);
  z80->flags_q = z80->af.f = (z80->af.f&~(ZYM_FLAG_C|ZYM_FLAG_PV))|tmpC|parity_tbl[z80->af.a];
}

ZYMOSIS_INLINE void ZYM_RRD_A (zym_cpu_t *z80) {
  const uint8_t tmpB = z80_mr_3ts(z80, z80->hl.w);
  /*IOP(4)*/
  z80->memptr.w = z80->hl.w+1u;
  z80_contention_by1ts(z80, z80->hl.w, 4);
  z80_pokeb_3ts(z80, z80->hl.w, (z80->af.a<<4)|(tmpB>>4));
  z80->af.a = (z80->af.a&0xf0u)|(tmpB&0x0fu);
  z80->flags_q = z80->af.f = (z80->af.f&ZYM_FLAG_C)|sz53p_tbl[z80->af.a];
}

ZYMOSIS_INLINE void ZYM_RLD_A (zym_cpu_t *z80) {
  const uint8_t tmpB = z80_mr_3ts(z80, z80->hl.w);
  /*IOP(4)*/
  z80->memptr.w = z80->hl.w+1u;
  z80_contention_by1ts(z80, z80->hl.w, 4);
  z80_pokeb_3ts(z80, z80->hl.w, (tmpB<<4)|(z80->af.a&0x0fu));
  z80->af.a = (z80->af.a&0xf0u)|(tmpB>>4);
  z80->flags_q = z80->af.f = (z80->af.f&ZYM_FLAG_C)|sz53p_tbl[z80->af.a];
}

ZYMOSIS_INLINE void ZYM_LD_A_IR (zym_cpu_t *z80, const uint8_t ir) {
  z80->af.a = ir;
  z80->prev_was_EIDDR = -1;
  z80_contention_by1ts_ir(z80, 1);
  z80->flags_q = z80->af.f = sz53_tbl[z80->af.a]|(z80->af.f&ZYM_FLAG_C)|(z80->iff2 ? ZYM_FLAG_PV : 0);
}


/******************************************************************************/

/* reference code:
#define SET_TRUE_CC()  do { \
  switch ((opcode>>3)&0x07u) { \
    case 0: trueCC = (z80->af.f&ZYM_FLAG_Z) == 0; break; \
    case 1: trueCC = (z80->af.f&ZYM_FLAG_Z) != 0; break; \
    case 2: trueCC = (z80->af.f&ZYM_FLAG_C) == 0; break; \
    case 3: trueCC = (z80->af.f&ZYM_FLAG_C) != 0; break; \
    case 4: trueCC = (z80->af.f&ZYM_FLAG_PV) == 0; break; \
    case 5: trueCC = (z80->af.f&ZYM_FLAG_PV) != 0; break; \
    case 6: trueCC = (z80->af.f&ZYM_FLAG_S) == 0; break; \
    case 7: trueCC = (z80->af.f&ZYM_FLAG_S) != 0; break; \
  } \
} while (0)
*/

/* branch-less (assuming that `!` is branch-less) code */
static const uint8_t ccmask[4] = {
  ZYM_FLAG_Z,
  ZYM_FLAG_C,
  ZYM_FLAG_PV,
  ZYM_FLAG_S,
};
#define SET_TRUE_CC()  trueCC = ((!(z80->af.f&ccmask[(opcode>>4)&0x03u]))^((opcode>>3)&0x01u))


#define CBX_REPEATED  (opcode&0x10u)
#define CBX_BACKWARD  (opcode&0x08u)


/* this is 256-bit bitmap indicates if the corresponding base opcode is using `(HL)` */
static const uint32_t withIndexBmp[8] = {
  0x00000000u,0x00700000u,0x40404040u,0x40bf4040u,
  0x40404040u,0x40404040u,0x00000800u,0x00000000u
};


int32_t zym_exec_ex (zym_cpu_t *z80, int32_t tscount) {
  uint8_t opcode;
  zym_bool gotDD, trueCC;
  int32_t disp;
  int32_t tsts; /* trap tstates */
  int32_t tstart = z80->tstates;
  uint8_t tmpB, tmpC, rsrc, rdst;
  uint16_t tmpW = 0; /* shut up the compiler; it's wrong but stubborn */
  /* main loop */
  while ((z80->next_event_tstate < 0 || z80->tstates < z80->next_event_tstate) &&
         (tscount < 0 || z80->tstates-tstart <= tscount))
  {
    /* note that real Z80 process prefixes by setting internal flags,
       blocking /INT for the next opc read, and reading the next opcode.
       we cannot do that, because for us prefixed instructions are not "flagged".
       this doesn't matter in practice for anything except the double IX/IY prefixes.
       currently, we're doing a weird hack for such prefixes (rolling back).
       double prefixes aren't used in any real code, tho, so this doesn't matter much.
     */
    z80->prev_pc = z80->org_pc;
    tsts = z80->tstates; /* rememeber to compensate trap cost */
    /* we should perform opcode fetching in loop, in case we need to
       refetch the opcode, or rollback on breakpoint.
       the exception is "halted" state: in this state Z80 fetches opcode
       at "HALTPC+1", but performs "NOP" internally. we will emulate this
       by advancing PC before fetching, and rolling it back.
       note that pager, memory reading and contention callbacks should check
       "halted" state if they want the real PC, and decrement it. yet this is
       required only if you have some kind of breakpoints implementation there.
       otherwise, the address (and PC) is correct for contention and paging.
     */
    for (;;) {
      z80->refetch = 0;
      if (z80->bp_hit) { z80->bp_was_hit = 1; z80->bp_hit = 0; return z80->tstates - tstart; }
      z80->org_pc = z80->pc; /* for callbacks */
      const int32_t ots = z80->tstates;
      #ifdef ZYMOSIS_FUSE_TEST
      /* for FUSE tests, we should do nothing here */
      #else
      EVEN_M1_TO_T3();
      #endif
      /* this is prolly wrong: a pager should be called for "halted" state. but 'cmon! */
      if (z80->halted) {
        z80_contention(z80, z80->pc+1u, ZYM_MEMIO_OPHLT, ZYM_MREQ_READ);
        /* we are not interested in opcode */
        #ifdef ZYMOSIS_FUSE_TEST
        /* for FUSE tests, it should be here */
        z80->tstates += 4;
        #endif
        (void)z80->mem_read(z80, z80->pc+1u, ZYM_MEMIO_OPHLT);
        opcode = 0u;
      } else {
        z80_contention(z80, z80->pc, ZYM_MEMIO_OPCODE, ZYM_MREQ_READ);
        #ifdef ZYMOSIS_FUSE_TEST
        /* for FUSE tests, it should be here */
        z80->tstates += 4;
        #endif
        opcode = z80->mem_read(z80, z80->pc, ZYM_MEMIO_OPCODE);
      }
      /* do we need to perform a rollback? */
      if ((z80->bp_hit|z80->refetch) == 0) {
        #ifdef ZYMOSIS_FUSE_TEST
        /* for FUSE tests, we should do nothing here */
        #else
        z80->tstates += 2; // skip t3 and t4
        #endif
        z80->pc += !z80->halted;
        INC_R;
        break;
      }
      /* rollback tstates */
      z80->tstates = ots;
    }
    /* previous Q for SCF/CCF */
    const uint8_t lastq = z80->flags_q;
    z80->flags_q = 0;
    z80->prev_was_EIDDR = 0;
    disp = gotDD = 0;
    z80->dd = &z80->hl;
    /* "HALT" simply executes "NOP" until interrupted */
    if (z80->halted) { /*z80->pc -= 1u;*/ continue; }
    /* check for I[XY] prefix */
    if (opcode == 0xddu || opcode == 0xfdu) {
      /* IX/IY prefix */
      z80->dd = (opcode == 0xddu ? &z80->ix : &z80->iy);
      /* we may need to rollback on double prefix */
      #if 0
      const int32_t ots = z80->tstates;
      /* read opcode -- OCR(4) */
      opcode = zym_get_opcode_ext(z80);
      /* test if this instruction have (HL) */
      if (withIndexBmp[opcode>>5]&(1u<<(opcode&0x1fu))) {
        /* 3rd byte is always DISP here */
        disp = z80_mr_3ts_args(z80, z80->pc); if (disp > 127) disp -= 256;
        ++z80->pc;
        z80->memptr.w = ZADD_WX(z80->dd->w, disp);
      } else if (opcode == 0xddu || opcode == 0xfdu) {
        /* rollback; this is not how real Z80 works, but meh... */
        /* read second prefix again */
        --z80->pc;
        /* rollback tstates */
        z80->tstates = ots;
        /* rollback R register */
        DEC_R;
        /* double prefix works as NOP, and blocks /INT */
        z80->prev_was_EIDDR = 1;
        /*FIXME: logically this should reset our Q value, but i am not sure; anyway, this works*/
        z80->flags_q = 0;
        /* restart main loop */
        continue;
      }
      #else
      opcode = z80->mem_read(z80, z80->pc, ZYM_MEMIO_OTHER);
      if (opcode == 0xddu || opcode == 0xfdu) {
        /* double prefix works as NOP, and blocks /INT */
        z80->prev_was_EIDDR = 1;
        /*FIXME: logically this should reset our Q value, but i am not sure; anyway, this works*/
        z80->flags_q = 0;
        /* restart main loop */
        continue;
      }
      /* read opcode -- OCR(4) */
      opcode = zym_get_opcode_ext(z80);
      /* test if this instruction have (HL) */
      if (withIndexBmp[opcode>>5]&(1u<<(opcode&0x1fu))) {
        /* 3rd byte is always DISP here */
        disp = z80_mr_3ts_args(z80, z80->pc); if (disp > 127) disp -= 256;
        z80->pc += 1u;
        z80->memptr.w = ZADD_WX(z80->dd->w, disp);
      }
      #endif
      gotDD = 1;
    }
    /* ED-prefixed instructions */
    if (opcode == 0xedu) {
      z80->dd = &z80->hl; /* ED-prefixed opcodes cannot use IX/IY */
      /* read opcode -- OCR(4) */
      opcode = zym_get_opcode_ext(z80);
      switch (opcode) {
        /* LDI, LDIR, LDD, LDDR */
        case 0xa0u: case 0xb0u: case 0xa8u: case 0xb8u:
          tmpB = z80_mr_3ts(z80, z80->hl.w);
          z80_pokeb_3ts(z80, z80->de.w, tmpB);
          /*MWR(5)*/
          z80_contention_by1ts(z80, z80->de.w, 2);
          --z80->bc.w;
          tmpB = (tmpB+z80->af.a)&0xffu;
          z80->flags_q = z80->af.f = /* BOO! FEAR THE MIGHTY BITS! */
            (tmpB&ZYM_FLAG_3)|(z80->af.f&(ZYM_FLAG_C|ZYM_FLAG_Z|ZYM_FLAG_S))|
            (z80->bc.w != 0 ? ZYM_FLAG_PV : 0)|
            (tmpB&0x02u ? ZYM_FLAG_5 : 0);
          if (CBX_REPEATED) {
            if (z80->bc.w != 0) {
              /*IOP(5)*/
              z80_contention_by1ts(z80, z80->de.w, 5);
              /* do it again */
              z80->pc -= 2;
              z80->memptr.w = z80->pc+1u;
              /* this seems to be required too: bits 11 and 13 of PC goes to bits 3 and 5 of F */
              z80->flags_q = z80->af.f = (z80->af.f&~(ZYM_FLAG_3|ZYM_FLAG_5))|
                ((z80->pc>>8)&(ZYM_FLAG_3|ZYM_FLAG_5));
            }
          }
          if (!CBX_BACKWARD) { ++z80->hl.w; ++z80->de.w; } else { --z80->hl.w; --z80->de.w; }
          break;
        /* CPI, CPIR, CPD, CPDR */
        case 0xa1u: case 0xb1u: case 0xa9u: case 0xb9u:
          tmpB = z80_mr_3ts(z80, z80->hl.w);
          /*IOP(5)*/
          z80_contention_by1ts(z80, z80->hl.w, 5);
          --z80->bc.w;
          z80->af.f = /* BOO! FEAR THE MIGHTY BITS! */
            ZYM_FLAG_N|
            (z80->af.f&ZYM_FLAG_C)|
            (z80->bc.w != 0 ? ZYM_FLAG_PV : 0)|
            ((int32_t)(z80->af.a&0x0fu)-(int32_t)(tmpB&0x0fu) < 0 ? ZYM_FLAG_H : 0);
          tmpB = ((uint32_t)z80->af.a-(uint32_t)tmpB)&0xffu;
          z80->af.f |= (tmpB == 0 ? ZYM_FLAG_Z : 0)|(tmpB&ZYM_FLAG_S);
          if (z80->af.f&ZYM_FLAG_H) tmpB = ((uint16_t)tmpB-1u)&0xffu;
          z80->af.f |= (tmpB&ZYM_FLAG_3)|(tmpB&0x02u ? ZYM_FLAG_5 : 0);
          z80->flags_q = z80->af.f;
          tmpW = (CBX_BACKWARD ? 0xffffU : 0x0001U); /* increment */
          if (CBX_REPEATED) {
            /* repeated */
            if ((z80->af.f&(ZYM_FLAG_Z|ZYM_FLAG_PV)) == ZYM_FLAG_PV) {
              /*IOP(5)*/
              z80_contention_by1ts(z80, z80->hl.w, 5);
              /* do it again */
              z80->pc -= 2;
              z80->memptr.w = z80->pc+1u;
              /* this seems to be required too: bits 11 and 13 of PC goes to bits 3 and 5 of F */
              z80->flags_q = z80->af.f = (z80->af.f&~(ZYM_FLAG_3|ZYM_FLAG_5))|
                ((z80->pc>>8)&(ZYM_FLAG_3|ZYM_FLAG_5));
            } else {
              /* new MEMPTR code */
              //if (CBX_BACKWARD) --z80->memptr.w; else ++z80->memptr.w;
              z80->memptr.w += tmpW;
            }
          } else {
            /* new MEMPTR code */
            //if (CBX_BACKWARD) --z80->memptr.w; else ++z80->memptr.w;
            z80->memptr.w += tmpW;
          }
          //if (CBX_BACKWARD) --z80->hl.w; else ++z80->hl.w;
          z80->hl.w += tmpW;
          break;
        /* OUTI, OTIR, OUTD, OTDR */
        case 0xa3u: case 0xb3u: case 0xabu: case 0xbbu:
          --z80->bc.b;
          /* fallthru */
        /* INI, INIR, IND, INDR */
        case 0xa2u: case 0xb2u: case 0xaau: case 0xbau:
          z80->memptr.w = z80->bc.w;
          if (CBX_BACKWARD) --z80->memptr.w; else ++z80->memptr.w;
          /*OCR(5)*/
          z80_contention_by1ts_ir(z80, 1);
          if (opcode&0x01u) {
            /* OUT* */
            tmpB = z80_mr_3ts(z80, z80->hl.w);/*MRD(3)*/
            z80_port_write(z80, z80->bc.w, tmpB);
            tmpW = z80->hl.w;
            if (CBX_BACKWARD) --tmpW; else ++tmpW;
            tmpC = (tmpB+tmpW)&0xffu;
          } else {
            /* IN* */
            tmpB = z80_port_read(z80, z80->bc.w);
            z80_pokeb_3ts(z80, z80->hl.w, tmpB);/*MWR(3)*/
            --z80->bc.b;
            tmpC = (CBX_BACKWARD ? ((uint32_t)tmpB+(uint32_t)z80->bc.c-1u) : (tmpB+z80->bc.c+1u))&0xffu;
          }
          z80->flags_q = z80->af.f =
            (tmpB&0x80u ? ZYM_FLAG_N : 0)|
            (tmpC < tmpB ? ZYM_FLAG_H|ZYM_FLAG_C : 0)|
            parity_tbl[(tmpC&0x07u)^z80->bc.b]|
            sz53_tbl[z80->bc.b];
          if (CBX_REPEATED) {
            /* repeating commands */
            if (z80->bc.b != 0) {
              uint16_t a = (opcode&0x01u ? z80->bc.w : z80->hl.w);
              /*IOP(5)*/
              z80_contention_by1ts(z80, a, 5);
              /* do it again */
              z80->pc -= 2;
              /* this seems to be required too: bits 11 and 13 of PC goes to bits 3 and 5 of F */
              /*z80->flags_q =*/ z80->af.f = (z80->af.f&~(ZYM_FLAG_3|ZYM_FLAG_5))|
                ((z80->pc>>8)&(ZYM_FLAG_3|ZYM_FLAG_5));
              /* there are more; see https://github.com/hoglet67/Z80Decoder/wiki/Undocumented-Flags
                 if (CF) {
                   if (data & 0x80) {
                     PF = PF ^ Parity((B - 1) & 0x7) ^ 1;
                     HF = (B & 0x0F) == 0x00;
                   } else {
                     PF = PF ^ Parity((B + 1) & 0x7) ^ 1;
                     HF = (B & 0x0F) == 0x0F;
                   }
                 } else {
                   PF = PF ^ Parity(B & 0x7) ^ 1;
                 }
              */
              if (z80->af.f&ZYM_FLAG_C) {
                if (tmpB&0x80u) {
                  z80->af.f ^= parity_tbl[(z80->bc.b-1u)&0x07u]^ZYM_FLAG_PV;
                  z80->af.f = (z80->af.f&~ZYM_FLAG_H)|((z80->bc.b&0x0fu) == 0x00u ? ZYM_FLAG_H : 0);
                } else {
                  z80->af.f ^= parity_tbl[(z80->bc.b+1u)&0x07u]^ZYM_FLAG_PV;
                  z80->af.f = (z80->af.f&~ZYM_FLAG_H)|((z80->bc.b&0x0fu) == 0x0fu ? ZYM_FLAG_H : 0);
                }
              } else {
                z80->af.f ^= parity_tbl[z80->bc.b&0x07u]^ZYM_FLAG_PV;
              }
              z80->flags_q = z80->af.f;
            }
          }
          if (CBX_BACKWARD) --z80->hl.w; else ++z80->hl.w;
          break;
        /* not strings, but some good instructions anyway */
        default:
          if ((opcode&0xc0u) == 0x40u) {
            /* 0x40...0x7f */
            switch (opcode&0x07u) {
              /* IN r8,(C) */
              case 0:
                z80->memptr.w = z80->bc.w+1u;
                tmpB = z80_port_read(z80, z80->bc.w);
                z80->flags_q = z80->af.f = sz53p_tbl[tmpB]|(z80->af.f&ZYM_FLAG_C);
                switch ((opcode>>3)&0x07u) {
                  case 0: z80->bc.b = tmpB; break;
                  case 1: z80->bc.c = tmpB; break;
                  case 2: z80->de.d = tmpB; break;
                  case 3: z80->de.e = tmpB; break;
                  case 4: z80->hl.h = tmpB; break;
                  case 5: z80->hl.l = tmpB; break;
                  case 7: z80->af.a = tmpB; break;
                  /* 6 affects only flags */
                }
                break;
              /* OUT (C),r8 */
              case 1:
                z80->memptr.w = z80->bc.w+1u;
                switch ((opcode>>3)&0x07u) {
                  case 0: tmpB = z80->bc.b; break;
                  case 1: tmpB = z80->bc.c; break;
                  case 2: tmpB = z80->de.d; break;
                  case 3: tmpB = z80->de.e; break;
                  case 4: tmpB = z80->hl.h; break;
                  case 5: tmpB = z80->hl.l; break;
                  case 7: tmpB = z80->af.a; break;
                  /* 6 usually means (HL), but here it is 0xff for CMOS, and 0 for NMOS */
                  default: tmpB = (!z80->cmos ? 0x00u : 0xffu); break; /* `!` must tell the predictor our preferred path, NMOS */
                }
                z80_port_write(z80, z80->bc.w, tmpB);
                break;
              /* SBC HL,rr/ADC HL,rr */
              case 2:
                /*IOP(4),IOP(3)*/
                z80_contention_by1ts_ir(z80, 7);
                switch ((opcode>>4)&0x03u) {
                  case 0: tmpW = z80->bc.w; break;
                  case 1: tmpW = z80->de.w; break;
                  case 2: tmpW = z80->hl.w; break;
                  default: tmpW = z80->sp.w; break;
                }
                z80->hl.w = (opcode&0x08u ? ZYM_ADC_DD(z80, tmpW, z80->hl.w) : ZYM_SBC_DD(z80, tmpW, z80->hl.w));
                break;
              /* LD (nn),rr/LD rr,(nn) */
              case 3:
                tmpW = z80_getpcw(z80, 0);
                z80->memptr.w = tmpW+1u;
                if (opcode&0x08u) {
                  /* LD rr,(nn) */
                  switch ((opcode>>4)&0x03u) {
                    case 0: z80->bc.w = z80_peekw_6ts(z80, tmpW); break;
                    case 1: z80->de.w = z80_peekw_6ts(z80, tmpW); break;
                    case 2: z80->hl.w = z80_peekw_6ts(z80, tmpW); break;
                    case 3: z80->sp.w = z80_peekw_6ts(z80, tmpW); break;
                  }
                } else {
                  /* LD (nn),rr */
                  switch ((opcode>>4)&0x03u) {
                    case 0: z80_pokew_6ts(z80, tmpW, z80->bc.w); break;
                    case 1: z80_pokew_6ts(z80, tmpW, z80->de.w); break;
                    case 2: z80_pokew_6ts(z80, tmpW, z80->hl.w); break;
                    case 3: z80_pokew_6ts(z80, tmpW, z80->sp.w); break;
                  }
                }
                break;
              /* NEG */
              case 4:
                tmpB = z80->af.a;
                z80->af.a = 0;
                ZYM_SUB_A(z80, tmpB);
                break;
              /* RETI/RETN */
              case 5:
                /*RETI: 0x4d, 0x5d, 0x6d, 0x7d*/
                /*RETN: 0x45, 0x55, 0x65, 0x75*/
                z80->iff1 = z80->iff2;
                z80->memptr.w = z80->pc = z80_pop_6ts(z80);
                if (opcode&0x08u) {
                  /* RETI */
                  if (z80->trap_reti != NULL) {
                    z80->trap_ts = z80->tstates - tstart;
                    if (z80->trap_reti(z80, opcode)) return z80->tstates - tstart;
                  }
                } else {
                  /* RETN */
                  if (z80->trap_retn != NULL) {
                    z80->trap_ts = z80->tstates - tstart;
                    if (z80->trap_retn(z80, opcode)) return z80->tstates - tstart;
                  }
                }
                break;
              /* IM n */
              case 6:
                switch (opcode) {
                  case 0x56u: case 0x76u: z80->im = 1; break;
                  case 0x5eu: case 0x7eu: z80->im = 2; break;
                  default: z80->im = 0; break;
                }
                break;
              /* specials */
              case 7:
                switch (opcode) {
                  /* LD I,A */
                  case 0x47u:
                    /*OCR(5)*/
                    z80_contention_by1ts_ir(z80, 1);
                    z80->regI = z80->af.a;
                    break;
                  /* LD R,A */
                  case 0x4fu:
                    /*OCR(5)*/
                    z80_contention_by1ts_ir(z80, 1);
                    z80->regR = z80->af.a;
                    break;
                  /* LD A,I */
                  case 0x57u: ZYM_LD_A_IR(z80, z80->regI); break;
                  /* LD A,R */
                  case 0x5fu: ZYM_LD_A_IR(z80, z80->regR); break;
                  /* RRD */
                  case 0x67u: ZYM_RRD_A(z80); break;
                  /* RLD */
                  case 0x6F: ZYM_RLD_A(z80); break;
                }
            }
          } else {
            /* slt and other traps */
            switch (opcode) {
              case 0xfb: // SLT trap
                if (z80->trap_edfb(z80, opcode)) return z80->tstates - tstart;
                break;
              case 0xfe: // ZXEmuT trap
                z80->tstates = tsts; /* compensate trap cost */
                z80->trap_ts = z80->tstates - tstart;
                if (z80->trap_edfb(z80, opcode)) return z80->tstates - tstart;
              default: break;
            }
          }
          break;
      }
      continue;
    } /* 0xed done */
    /* CB-prefixed instructions */
    if (opcode == 0xcbu) {
      /* shifts and bit operations */
      /* read opcode -- OCR(4) */
      if (!gotDD) {
        opcode = zym_get_opcode_ext(z80);
      } else {
        #ifdef ZYMOSIS_FUSE_TEST
        /* for FUSE tests, it should be here */
        z80_contention(z80, z80->pc, ZYM_MEMIO_OPCEXT, ZYM_MREQ_READ);
        z80->tstates += 3;
        opcode = z80->mem_read(z80, z80->pc, ZYM_MEMIO_OPCEXT);
        z80_contention_by1ts(z80, z80->pc, 2);
        z80->pc += 1u;
        #else
        //FIXME: check if it really skips `INC_R`
        opcode = zym_get_opcode_ext(z80);
        //DEC_R;
        #endif
      }
      if (gotDD) {
        tmpW = ZADD_WX(z80->dd->w, disp);
        tmpB = z80_mr_3ts(z80, tmpW);
        z80_contention_by1ts(z80, tmpW, 1);
      } else {
        switch (opcode&0x07u) {
          case 0: tmpB = z80->bc.b; break;
          case 1: tmpB = z80->bc.c; break;
          case 2: tmpB = z80->de.d; break;
          case 3: tmpB = z80->de.e; break;
          case 4: tmpB = z80->hl.h; break;
          case 5: tmpB = z80->hl.l; break;
          case 6: tmpB = z80_mr_3ts(z80, z80->hl.w);
                  //k8: i don't know why it is here, but it seems to be required
                  //    to properly emulate contended access.
                  z80->mem_contention(z80, z80->hl.w, ZYM_MEMIO_DATA, ZYM_MREQ_READ);
                  z80->tstates += 1;
                  break;
          case 7: tmpB = z80->af.a; break;
        }
      }
      switch ((opcode>>3)&0x1fu) {
        case 0: tmpB = ZYM_RLC(z80, tmpB); break;
        case 1: tmpB = ZYM_RRC(z80, tmpB); break;
        case 2: tmpB = ZYM_RL(z80, tmpB); break;
        case 3: tmpB = ZYM_RR(z80, tmpB); break;
        case 4: tmpB = ZYM_SLA(z80, tmpB); break;
        case 5: tmpB = ZYM_SRA(z80, tmpB); break;
        case 6: tmpB = ZYM_SLS(z80, tmpB); break;
        case 7: tmpB = ZYM_SRL(z80, tmpB); break;
        default:
          switch ((opcode>>6)&0x03u) {
            case 1: ZYM_BIT(z80, (opcode>>3)&0x07u, tmpB, (gotDD || (opcode&0x07u) == 6)); break;
            case 2: tmpB &= ~(1<<((opcode>>3)&0x07u)); break; /* RES */
            case 3: tmpB |= (1<<((opcode>>3)&0x07u)); break; /* SET */
          }
          break;
      }
      if ((opcode&0xc0u) != 0x40u) {
        /* BITs are not welcome here */
        if (gotDD) {
          /* tmpW was set earlier */
          if ((opcode&0x07u) != 6) z80_pokeb_3ts(z80, tmpW, tmpB);
        }
        switch (opcode&0x07u) {
          case 0: z80->bc.b = tmpB; break;
          case 1: z80->bc.c = tmpB; break;
          case 2: z80->de.d = tmpB; break;
          case 3: z80->de.e = tmpB; break;
          case 4: z80->hl.h = tmpB; break;
          case 5: z80->hl.l = tmpB; break;
          case 6: z80_pokeb_3ts(z80, ZADD_WX(z80->dd->w, disp), tmpB); break;
          case 7: z80->af.a = tmpB; break;
        }
      }
      continue;
    } /* 0xcb done */
    /* normal things */
    switch (opcode&0xc0u) {
      /* 0x00..0x3F */
      case 0x00u:
        switch (opcode&0x07u) {
          /* misc,DJNZ,JR,JR cc */
          case 0:
            if (opcode&0x30u) {
              /* branches */
              if (opcode&0x20u) {
                /* JR cc */
                /*
                switch ((opcode>>3)&0x03u) {
                  case 0: trueCC = (z80->af.f&ZYM_FLAG_Z) == 0; break;
                  case 1: trueCC = (z80->af.f&ZYM_FLAG_Z) != 0; break;
                  case 2: trueCC = (z80->af.f&ZYM_FLAG_C) == 0; break;
                  case 3: trueCC = (z80->af.f&ZYM_FLAG_C) != 0; break;
                  default: trueCC = 0; break;
                }
                */
                trueCC = ((!(z80->af.f&ccmask[(opcode>>4)&0x01u]))^((opcode>>3)&0x01u));
              } else {
                /* DJNZ/JR */
                if ((opcode&0x08u) == 0) {
                  /* DJNZ */
                  /*OCR(5)*/
                  z80_contention_by1ts_ir(z80, 1);
                  --z80->bc.b;
                  trueCC = (z80->bc.b != 0);
                } else {
                  /* JR */
                  trueCC = 1;
                }
              }
              /* `disp` is always read, but FUSE tests require it this way; sigh */
              #ifdef ZYMOSIS_FUSE_TEST
              /* for FUSE tests, it should be like this */
              if (trueCC) {
                disp = z80_mr_3ts_args(z80, z80->pc);
              } else {
                z80_contention(z80, z80->pc, ZYM_MEMIO_OPCARG, ZYM_MREQ_READ);
                z80->tstates += 3;
                disp = 0;
              }
              #else
              disp = z80_mr_3ts_args(z80, z80->pc);
              #endif
              if (trueCC) {
                /* execute branch (relative) */
                /*IOP(5)*/
                if (disp > 127) disp -= 256; /* convert to int8_t */
                z80_contention_by1ts_pc(z80, 5);
                z80->pc += 1u;
                z80->memptr.w = z80->pc = ZADD_WX(z80->pc, disp);
              } else {
                z80->pc += 1u;
              }
            } else {
              /* EX AF,AF' or NOP */
              if (opcode != 0) zym_exaf_internal(z80);
            }
            break;
          /* LD rr,nn/ADD HL,rr */
          case 1:
            if (opcode&0x08u) {
              /* ADD HL,rr */
              /*IOP(4),IOP(3)*/
              z80_contention_by1ts_ir(z80, 7);
              switch ((opcode>>4)&0x03u) {
                case 0: z80->dd->w = ZYM_ADD_DD(z80, z80->bc.w, z80->dd->w); break;
                case 1: z80->dd->w = ZYM_ADD_DD(z80, z80->de.w, z80->dd->w); break;
                case 2: z80->dd->w = ZYM_ADD_DD(z80, z80->dd->w, z80->dd->w); break;
                case 3: z80->dd->w = ZYM_ADD_DD(z80, z80->sp.w, z80->dd->w); break;
              }
            } else {
              /* LD rr,nn */
              tmpW = z80_getpcw(z80, 0);
              switch ((opcode>>4)&0x03u) {
                case 0: z80->bc.w = tmpW; break;
                case 1: z80->de.w = tmpW; break;
                case 2: z80->dd->w = tmpW; break;
                case 3: z80->sp.w = tmpW; break;
              }
            }
            break;
          /* LD xxx,xxx */
          case 2:
            switch ((opcode>>3)&0x07u) {
              /* LD (BC),A */
              case 0: z80_pokeb_3ts(z80, z80->bc.w, z80->af.a); z80->memptr.l = (z80->bc.w+1u)&0xffu; z80->memptr.h = z80->af.a; break;
              /* LD A,(BC) */
              case 1: z80->af.a = z80_mr_3ts(z80, z80->bc.w); z80->memptr.w = z80->bc.w+1u; break;
              /* LD (DE),A */
              case 2: z80_pokeb_3ts(z80, z80->de.w, z80->af.a); z80->memptr.l = (z80->de.w+1u)&0xffu; z80->memptr.h = z80->af.a; break;
              /* LD A,(DE) */
              case 3: z80->af.a = z80_mr_3ts(z80, z80->de.w); z80->memptr.w = z80->de.w+1u; break;
              /* LD (nn),HL */
              case 4:
                tmpW = z80_getpcw(z80, 0);
                z80->memptr.w = tmpW+1u;
                z80_pokew_6ts(z80, tmpW, z80->dd->w);
                break;
              /* LD HL,(nn) */
              case 5:
                tmpW = z80_getpcw(z80, 0);
                z80->memptr.w = tmpW+1u;
                z80->dd->w = z80_peekw_6ts(z80, tmpW);
                break;
              /* LD (nn),A */
              case 6:
                tmpW = z80_getpcw(z80, 0);
                z80->memptr.l = (tmpW+1u)&0xffu;
                z80->memptr.h = z80->af.a;
                z80_pokeb_3ts(z80, tmpW, z80->af.a);
                break;
              /* LD A,(nn) */
              case 7:
                tmpW = z80_getpcw(z80, 0);
                z80->memptr.w = tmpW+1u;
                z80->af.a = z80_mr_3ts(z80, tmpW);
                break;
            }
            break;
          /* INC rr/DEC rr */
          case 3:
            /*OCR(6)*/
            z80_contention_by1ts_ir(z80, 2);
            if (opcode&0x08u) {
              /*DEC*/
              switch ((opcode>>4)&0x03u) {
                case 0: --z80->bc.w; break;
                case 1: --z80->de.w; break;
                case 2: --z80->dd->w; break;
                case 3: --z80->sp.w; break;
              }
            } else {
              /*INC*/
              switch ((opcode>>4)&0x03u) {
                case 0: ++z80->bc.w; break;
                case 1: ++z80->de.w; break;
                case 2: ++z80->dd->w; break;
                case 3: ++z80->sp.w; break;
              }
            }
            break;
          /* INC r8 */
          case 4:
            switch ((opcode>>3)&0x07u) {
              case 0: z80->bc.b = ZYM_INC8(z80, z80->bc.b); break;
              case 1: z80->bc.c = ZYM_INC8(z80, z80->bc.c); break;
              case 2: z80->de.d = ZYM_INC8(z80, z80->de.d); break;
              case 3: z80->de.e = ZYM_INC8(z80, z80->de.e); break;
              case 4: z80->dd->h = ZYM_INC8(z80, z80->dd->h); break;
              case 5: z80->dd->l = ZYM_INC8(z80, z80->dd->l); break;
              case 6:
                if (gotDD) { --z80->pc; z80_contention_by1ts_pc(z80, 5); ++z80->pc; }
                tmpW = ZADD_WX(z80->dd->w, disp);
                tmpB = z80_mr_3ts(z80, tmpW);
                z80_contention_by1ts(z80, tmpW, 1);
                tmpB = ZYM_INC8(z80, tmpB);
                z80_pokeb_3ts(z80, tmpW, tmpB);
                break;
              case 7: z80->af.a = ZYM_INC8(z80, z80->af.a); break;
            }
            break;
          /* DEC r8 */
          case 5:
            switch ((opcode>>3)&0x07u) {
              case 0: z80->bc.b = ZYM_DEC8(z80, z80->bc.b); break;
              case 1: z80->bc.c = ZYM_DEC8(z80, z80->bc.c); break;
              case 2: z80->de.d = ZYM_DEC8(z80, z80->de.d); break;
              case 3: z80->de.e = ZYM_DEC8(z80, z80->de.e); break;
              case 4: z80->dd->h = ZYM_DEC8(z80, z80->dd->h); break;
              case 5: z80->dd->l = ZYM_DEC8(z80, z80->dd->l); break;
              case 6:
                if (gotDD) { --z80->pc; z80_contention_by1ts_pc(z80, 5); ++z80->pc; }
                tmpW = ZADD_WX(z80->dd->w, disp);
                tmpB = z80_mr_3ts(z80, tmpW);
                z80_contention_by1ts(z80, tmpW, 1);
                tmpB = ZYM_DEC8(z80, tmpB);
                z80_pokeb_3ts(z80, tmpW, tmpB);
                break;
              case 7: z80->af.a = ZYM_DEC8(z80, z80->af.a); break;
            }
            break;
          /* LD r8,n */
          case 6:
            tmpB = z80_mr_3ts_args(z80, z80->pc);
            ++z80->pc;
            switch ((opcode>>3)&0x07u) {
              case 0: z80->bc.b = tmpB; break;
              case 1: z80->bc.c = tmpB; break;
              case 2: z80->de.d = tmpB; break;
              case 3: z80->de.e = tmpB; break;
              case 4: z80->dd->h = tmpB; break;
              case 5: z80->dd->l = tmpB; break;
              case 6:
                if (gotDD) { --z80->pc; z80_contention_by1ts_pc(z80, 2); ++z80->pc; }
                tmpW = ZADD_WX(z80->dd->w, disp);
                z80_pokeb_3ts(z80, tmpW, tmpB);
                break;
              case 7: z80->af.a = tmpB; break;
            }
            break;
          /* swim-swim-hungry */
          case 7:
            switch ((opcode>>3)&0x07u) {
              case 0: ZYM_RLCA(z80); break;
              case 1: ZYM_RRCA(z80); break;
              case 2: ZYM_RLA(z80); break;
              case 3: ZYM_RRA(z80); break;
              case 4: ZYM_DAA(z80); break;
              case 5: /* CPL */
                z80->af.a ^= 0xffu;
                z80->flags_q = z80->af.f = (z80->af.a&ZYM_FLAG_35)|(ZYM_FLAG_N|ZYM_FLAG_H)|(z80->af.f&(ZYM_FLAG_C|ZYM_FLAG_PV|ZYM_FLAG_Z|ZYM_FLAG_S));
                break;
              case 6: /* SCF */
                /* `!` must tell the predictor our preferred path, NMOS */
                z80->flags_q = z80->af.f = (z80->af.f&(ZYM_FLAG_PV|ZYM_FLAG_Z|ZYM_FLAG_S))|((z80->af.a|(!z80->cmos ? lastq^z80->af.f : 0))&ZYM_FLAG_35)|ZYM_FLAG_C;
                break;
              case 7: /* CCF */
                /* `!` must tell the predictor our preferred path, NMOS */
                z80->flags_q = z80->af.f = (z80->af.f&(ZYM_FLAG_PV|ZYM_FLAG_Z|ZYM_FLAG_S))|((z80->af.a|(!z80->cmos ? lastq^z80->af.f : 0))&ZYM_FLAG_35)|(z80->af.f&ZYM_FLAG_C ? ZYM_FLAG_H : ZYM_FLAG_C);
                break;
            }
            break;
        }
        break;
      /* 0x40..0x7F (LD r8,r8) */
      case 0x40u:
        if (opcode == 0x76u) {
          /* HALT */
          z80->halted = 1;
          /* z80->pc -= 1u;*/
          if (z80->trap_halt != NULL && z80->trap_halt(z80)) return z80->tstates - tstart;
          continue;
        }
        rsrc = (opcode&0x07u);
        rdst = ((opcode>>3)&0x07u);
        switch (rsrc) {
          case 0: tmpB = z80->bc.b; break;
          case 1: tmpB = z80->bc.c; break;
          case 2: tmpB = z80->de.d; break;
          case 3: tmpB = z80->de.e; break;
          case 4: tmpB = (gotDD && rdst == 6 ? z80->hl.h : z80->dd->h); break;
          case 5: tmpB = (gotDD && rdst == 6 ? z80->hl.l : z80->dd->l); break;
          case 6:
            if (gotDD) { --z80->pc; z80_contention_by1ts_pc(z80, 5); ++z80->pc; }
            tmpW = ZADD_WX(z80->dd->w, disp);
            tmpB = z80_mr_3ts(z80, tmpW);
            break;
          case 7: tmpB = z80->af.a; break;
        }
        switch (rdst) {
          case 0: z80->bc.b = tmpB; break;
          case 1: z80->bc.c = tmpB; break;
          case 2: z80->de.d = tmpB; break;
          case 3: z80->de.e = tmpB; break;
          case 4: if (gotDD && rsrc == 6) z80->hl.h = tmpB; else z80->dd->h = tmpB; break;
          case 5: if (gotDD && rsrc == 6) z80->hl.l = tmpB; else z80->dd->l = tmpB; break;
          case 6:
            if (gotDD) { --z80->pc; z80_contention_by1ts_pc(z80, 5); ++z80->pc; }
            tmpW = ZADD_WX(z80->dd->w, disp);
            z80_pokeb_3ts(z80, tmpW, tmpB);
            break;
          case 7: z80->af.a = tmpB; break;
        }
        break;
      /* 0x80..0xBF (ALU A,r8) */
      case 0x80u:
        switch (opcode&0x07u) {
          case 0: tmpB = z80->bc.b; break;
          case 1: tmpB = z80->bc.c; break;
          case 2: tmpB = z80->de.d; break;
          case 3: tmpB = z80->de.e; break;
          case 4: tmpB = z80->dd->h; break;
          case 5: tmpB = z80->dd->l; break;
          case 6:
            if (gotDD) { --z80->pc; z80_contention_by1ts_pc(z80, 5); ++z80->pc; }
            tmpW = ZADD_WX(z80->dd->w, disp);
            tmpB = z80_mr_3ts(z80, tmpW);
            break;
          case 7: tmpB = z80->af.a; break;
        }
        switch ((opcode>>3)&0x07u) {
          case 0: ZYM_ADD_A(z80, tmpB); break;
          case 1: ZYM_ADC_A(z80, tmpB); break;
          case 2: ZYM_SUB_A(z80, tmpB); break;
          case 3: ZYM_SBC_A(z80, tmpB); break;
          case 4: ZYM_AND_A(z80, tmpB); break;
          case 5: ZYM_XOR_A(z80, tmpB); break;
          case 6: ZYM_OR_A(z80, tmpB); break;
          case 7: ZYM_CP_A(z80, tmpB); break;
        }
        break;
      /* 0xC0..0xFF */
      case 0xC0:
        switch (opcode&0x07u) {
          /* RET cc */
          case 0:
            z80_contention_by1ts_ir(z80, 1);
            SET_TRUE_CC();
            if (trueCC) z80->memptr.w = z80->pc = z80_pop_6ts(z80);
            break;
          /* POP rr/special0 */
          case 1:
            if (opcode&0x08u) {
              /* special 0 */
              switch ((opcode>>4)&0x03u) {
                /* RET */
                case 0: z80->memptr.w = z80->pc = z80_pop_6ts(z80); break;
                /* EXX */
                case 1: zym_exx_internal(z80); break;
                /* JP (HL) */
                case 2: z80->pc = z80->dd->w; break;
                /* LD SP,HL */
                case 3:
                  /*OCR(6)*/
                  z80_contention_by1ts_ir(z80, 2);
                  z80->sp.w = z80->dd->w;
                  break;
              }
            } else {
              /* POP rr */
              tmpW = z80_pop_6ts(z80);
              switch ((opcode>>4)&0x03u) {
                case 0: z80->bc.w = tmpW; break;
                case 1: z80->de.w = tmpW; break;
                case 2: z80->dd->w = tmpW; break;
                case 3: z80->af.w = tmpW; break;
              }
            }
            break;
          /* JP cc,nn */
          case 2:
            SET_TRUE_CC();
            z80->memptr.w = z80_getpcw(z80, 0);
            if (trueCC) z80->pc = z80->memptr.w;
            break;
          /* special1/special3 */
          case 3:
            switch ((opcode>>3)&0x07u) {
              /* JP nn */
              case 0: z80->memptr.w = z80->pc = z80_getpcw(z80, 0); break;
              /* OUT (n),A */
              case 2:
                tmpW = z80_mr_3ts_args(z80, z80->pc);
                ++z80->pc;
                z80->memptr.l = (tmpW+1u)&0xffu;
                z80->memptr.h = z80->af.a;
                tmpW |= (((uint16_t)(z80->af.a))<<8);
                z80_port_write(z80, tmpW, z80->af.a);
                break;
              /* IN A,(n) */
              case 3:
                tmpB = z80_mr_3ts_args(z80, z80->pc);
                tmpW = (uint16_t)((((uint16_t)(z80->af.a))<<8)|tmpB);
                ++z80->pc;
                z80->memptr.l = 0;
                z80->memptr.h = z80->af.a;
                z80->memptr.w += tmpB+1u;
                z80->af.a = z80_port_read(z80, tmpW);
                break;
              /* EX (SP),HL */
              case 4:
                /*SRL(3),SRH(4)*/
                tmpW = z80_peekw_6ts(z80, z80->sp.w);
                z80_contention_by1ts(z80, z80->sp.w+1u, 1);
                /*SWL(3),SWH(5)*/
                z80_pokew_6ts_inverted(z80, z80->sp.w, z80->dd->w);
                z80_contention_by1ts(z80, z80->sp.w, 2);
                z80->memptr.w = z80->dd->w = tmpW;
                break;
              /* EX DE,HL */
              case 5:
                tmpW = z80->de.w;
                z80->de.w = z80->hl.w;
                z80->hl.w = tmpW;
                break;
              /* DI */
              case 6: z80->iff1 = z80->iff2 = 0; break;
              /* EI */
              case 7: z80->iff1 = z80->iff2 = 1; z80->prev_was_EIDDR = 1; break;
            }
            break;
          /* CALL cc,nn */
          case 4:
            SET_TRUE_CC();
            /* MEMPTR docs says that it is like JP */
            z80->memptr.w = z80_getpcw(z80, trueCC);
            if (trueCC) {
              z80_push_6ts(z80, z80->pc);
              z80->pc = z80->memptr.w;
            }
            break;
          /* PUSH rr/special2 */
          case 5:
            if (opcode&0x08u) {
              if (((opcode>>4)&0x03u) == 0) {
                /* CALL */
                z80->memptr.w = tmpW = z80_getpcw(z80, 1);
                z80_push_6ts(z80, z80->pc);
                z80->pc = tmpW;
              }
            } else {
              /* PUSH rr */
              /*OCR(5)*/
              z80_contention_by1ts_ir(z80, 1);
              switch ((opcode>>4)&0x03u) {
                case 0: tmpW = z80->bc.w; break;
                case 1: tmpW = z80->de.w; break;
                case 2: tmpW = z80->dd->w; break;
                default: tmpW = z80->af.w; break;
              }
              z80_push_6ts(z80, tmpW);
            }
            break;
          /* ALU A,n */
          case 6:
            tmpB = z80_mr_3ts_args(z80, z80->pc);
            ++z80->pc;
            switch ((opcode>>3)&0x07u) {
              case 0: ZYM_ADD_A(z80, tmpB); break;
              case 1: ZYM_ADC_A(z80, tmpB); break;
              case 2: ZYM_SUB_A(z80, tmpB); break;
              case 3: ZYM_SBC_A(z80, tmpB); break;
              case 4: ZYM_AND_A(z80, tmpB); break;
              case 5: ZYM_XOR_A(z80, tmpB); break;
              case 6: ZYM_OR_A(z80, tmpB); break;
              case 7: ZYM_CP_A(z80, tmpB); break;
            }
            break;
          /* RST nnn */
          case 7:
            /*OCR(5)*/
            z80_contention_by1ts_ir(z80, 1);
            z80_push_6ts(z80, z80->pc);
            z80->memptr.w = z80->pc = opcode&0x38u;
            break;
        }
        break;
    } /* end switch */
  }
  return z80->tstates - tstart;
}


int32_t zym_exec_step (zym_cpu_t *z80) {
  const int32_t one = z80->next_event_tstate;
  z80->next_event_tstate = -1;
  const int32_t res = zym_exec_ex(z80, 1);
  z80->next_event_tstate = one;
  return res;
}


/******************************************************************************/
/* changes z80->tstates if interrupt occurs */
int32_t zym_intr (zym_cpu_t *z80) {
  uint16_t a;
  const int32_t ots = z80->tstates;
  /* emulate NMOS Z80 bug: interruptet LD A,<I|R> copies cleared flag */
  /*FIXME: what is the state of `z80->flags_q` here? */
  if (z80->prev_was_EIDDR < 0) {
    z80->prev_was_EIDDR = 0;
    if (!z80->cmos) z80->flags_q = (z80->af.f &= ~ZYM_FLAG_PV);
  }
  if (z80->prev_was_EIDDR || !z80->iff1) return 0; /* not accepted */
  z80->flags_q = 0; /* we cannot do it earlier, because ignored interrupt won't reset flags */
  /* skip "HALT" */
  //if (z80->halted) { z80->halted = 0; /* z80->pc += 1u;*/ }
  z80->halted = 0;
  z80->iff1 = z80->iff2 = 0; /* disable interrupts */
  switch ((z80->im &= 0x03u)) {
    case 3: /* ??? */ z80->im = 0; /* fallthru */
    case 0: /* take instruction from the bus (for now we assume that reading from bus always returns 0xff) */
      /* with a CALL nnnn on the data bus, it takes 19 cycles: */
      /* M1 cycle: 7 T to acknowledge interrupt (where exactly data bus reading occurs?) */
      /* M2 cycle: 3 T to read low byte of 'nnnn' from data bus */
      /* M3 cycle: 3 T to read high byte of 'nnnn' and decrement SP */
      /* M4 cycle: 3 T to write high byte of PC to the stack and decrement SP */
      /* M5 cycle: 3 T to write low byte of PC and jump to 'nnnn' */
      z80->tstates += 6;
      /* fallthru */
    case 1: /* just do RST #38 */
      INC_R;
      z80->tstates += 7; /* M1 cycle: 7 T to acknowledge interrupt and decrement SP */
      /* M2 cycle: 3 T states write high byte of PC to the stack and decrement SP */
      /* M3 cycle: 3 T states write the low byte of PC and jump to #0038 */
      z80_push_6ts(z80, z80->pc);
      z80->memptr.w = z80->pc = 0x38u;
      break;
    case 2:
      INC_R;
      z80->tstates += 7; /* M1 cycle: 7 T to acknowledge interrupt and decrement SP */
      /* M2 cycle: 3 T states write high byte of PC to the stack and decrement SP */
      /* M3 cycle: 3 T states write the low byte of PC */
      z80_push_6ts(z80, z80->pc);
      /* M4 cycle: 3 T to read high byte from the interrupt vector */
      /* M5 cycle: 3 T to read low byte from bus and jump to interrupt routine */
      a = (((uint16_t)z80->regI)<<8)|0xffu;
      z80->memptr.w = z80->pc = z80_peekw_6ts(z80, a);
      break;
  }
  return z80->tstates-ots; /* accepted */
}


/* changes z80->tstates if interrupt occurs */
int32_t zym_nmi (zym_cpu_t *z80) {
  const int32_t ots = z80->tstates;
  /* emulate NMOS Z80 bug: interruptet LD A,<I|R> copies cleared flag */
  /*FIXME: what is the state of `z80->flags_q` here? */
  if (z80->prev_was_EIDDR < 0) {
    z80->prev_was_EIDDR = 0;
    /* i was informed that NMI doesn't do this; dunno, let's fix it. ;-)
    if (!z80->cmos) z80->flags_q = (z80->af.f &= ~ZYM_FLAG_PV);
    */
  }
  if (z80->prev_was_EIDDR) return 0;
  z80->flags_q = 0; /* we cannot do it earlier, because ignored interrupt won't reset flags */
  /* skip "HALT" */
  //if (z80->halted) { z80->halted = 0; /* z80->pc += 1;*/ }
  z80->halted = 0;
  INC_R;
  z80->iff1 = 0; /* IFF2 is not changed */
  z80->tstates += 5; /* M1 cycle: 5 T states to do an opcode read and decrement SP */
  /* M2 cycle: 3 T states write high byte of PC to the stack and decrement SP */
  /* M3 cycle: 3 T states write the low byte of PC and jump to #0066 */
  z80_push_6ts(z80, z80->pc);
  z80->memptr.w = z80->pc = 0x66u;
  return z80->tstates-ots;
}


/******************************************************************************/
uint16_t zym_pop (zym_cpu_t *z80) {
  uint16_t res = z80_peekb_i(z80, z80->sp.w);
  ++z80->sp.w;
  res |= ((uint16_t)z80_peekb_i(z80, z80->sp.w))<<8;
  ++z80->sp.w;
  return res;
}


void zym_push (zym_cpu_t *z80, const uint16_t value) {
  --z80->sp.w;
  z80_pokeb_i(z80, z80->sp.w, (value>>8)&0xffu);
  --z80->sp.w;
  z80_pokeb_i(z80, z80->sp.w, value&0xffu);
}


/******************************************************************************
instruction decoding
====================
00 00y 000 -- special
00 01g 000 -- DJNZ/JR
00 1cc 000 -- JR  cc,n
00 rr0 001 -- LD  rr,nn
00 rr1 001 -- ADD HL,rr
00 000 010 -- LD  (BC),A
00 001 010 -- LD  A,(BC)
00 010 010 -- LD  (DE),A
00 011 010 -- LD  A,(DE)
00 100 010 -- LD  (nn),HL
00 101 010 -- LD  HL,(nn)
00 110 010 -- LD  (nn),A
00 111 010 -- LD  A,(nn)
00 rr0 011 -- INC rr
00 rr1 011 -- DEC rr
00 rrr 100 -- INC r8
00 rrr 101 -- DEC r8
00 rrr 110 -- LD  r8,n
00 xxx 111 -- special

y:
 0 - NOP
 1 - EX AF,AF'
g:
 0 - DJNZ
 1 - JR
cc:
 00 - NZ
 01 - Z
 10 - NC
 11 - C
xxx:
 000 - RLCA
 001 - RRCA
 010 - RLA
 011 - RRA
 100 - DAA
 101 - CPL
 110 - SCF
 111 - CCF
rrr:
 000 - B
 001 - C
 010 - D
 011 - E
 100 - H
 101 - L
 110 - (HL)
 111 - A
rr:
 00 - BC
 01 - DE
 10 - HL
 11 - SP

---------------------------------

01 rrr rrr - LD r8,r8 (lo: src)

---------------------------------

10 alu rrr - ALU a,r8
alu:
 000 - ADD
 001 - ADC
 010 - SUB
 011 - SBC
 100 - AND
 101 - XOR
 110 - OR
 111 - CP

---------------------------------

11 ccc 000 -- RET  cc
11 rr0 001 -- POP  rr
11 xx1 001 -- special0
11 ccc 010 -- JP   cc,nn
11 yyy 011 -- special1
11 ccc 100 -- CALL cc,nn
11 rr0 101 -- PUSH rr
11 zz1 101 -- special2
11 alu 110 -- alu  A,n
11 aaa 111 -- RST  nnn

rr:
 00 - BC
 01 - DE
 10 - HL
 11 - AF
aaa: RST addr=aaa<<3
ccc:
  000 - NZ
  001 - Z
  010 - NC
  011 - C
  100 - PO
  101 - PE
  110 - P
  111 - M
alu:
 000 - ADD
 001 - ADC
 010 - SUB
 011 - SBC
 100 - AND
 101 - XOR
 110 - OR
 111 - CP
xx:
  00 - RET
  01 - EXX
  10 - JP HL
  11 - LD SP,HL
yyy:
  000 - JP  nn
  001 - CB
  010 - OUT (n),A
  011 - IN  A,(n)
  100 - EX  (SP),HL
  101 - EX  DE,HL
  110 - DI
  111 - EI
zz:
  00 - CALL nn
  01 - DD
  10 - ED
  11 - FD


---------------------------------

ED:
01 rrr 000 -- IN  r8,(C)
01 rrr 001 -- OUT (C),r8
01 rr0 010 -- SBC HL,rr
01 rr1 010 -- ADC HL,rr
01 rr0 011 -- LD  (nn),rr
01 rr1 011 -- LD  rr,(nn)

01 000 100 -- NEG
01 001 100 -- NEG
01 010 100 -- NEG
01 011 100 -- NEG
01 100 100 -- NEG
01 101 100 -- NEG
01 110 100 -- NEG
01 111 100 -- NEG

01 000 101 -- RETN
01 001 101 -- RETI
01 010 101 -- RETN
01 011 101 -- RETI
01 100 101 -- RETN
01 101 101 -- RETI
01 110 101 -- RETN
01 111 101 -- RETI

01 000 110 -- IM 0
01 001 110 -- IM 0/1
01 010 110 -- IM 1
01 011 110 -- IM 2
01 100 110 -- IM 0
01 101 110 -- IM 0
01 111 110 -- IM 2

01 000 111 -- LD I,A
01 001 111 -- LD R,A
01 010 111 -- LD A,I
01 011 111 -- LD A,R
01 100 111 -- RRD
01 101 111 -- RLD
01 110 111 -- NOP
01 111 111 -- NOP


10 1rd 0tt
r: repeating instruction? (1: yes)
d: direction (0:inc; 1:dec)
tt: instruction type
  00: LD
  01: CP
  10: IN
  11: OUT

10 100 000 -- LDI
10 110 000 -- LDIR
10 101 000 -- LDD
10 111 000 -- LDDR

10 100 001 -- CPI
10 110 001 -- CPIR
10 101 001 -- CPD
10 111 001 -- CPDR

10 100 010 -- INI
10 110 010 -- INIR
10 101 010 -- IND
10 111 010 -- INDR

10 100 011 -- OUTI
10 110 011 -- OTIR
10 101 011 -- OUTD
10 111 011 -- OTDR


---------------------------------
CB:
zz xxx rrr

rrr: the usual r8

zz:
  00 -- see below
  01 -- BIT n, r8
  10 -- RES n, r8
  11 -- SET n, r8

for bitops, xxx is bit number

xxx:
  000 - RLC r8
  001 - RRC r8
  010 - RL r8
  011 - RR r8
  100 - SLA r8
  101 - SRA r8
  110 - SLS r8
  111 - SRL r8

for DD/FD prefix, 3rd byte is always disp,
and the result is always written to (I<X|Y>+disp)


command reading cycles
======================
the following is not 100% correct, but the position
of the contention cycles is right

m1 cycle (accoding to the official Zilog manual)
 t1: setting /MREQ & /RD
 t2: memory read
  here comes contetion (AFTER t2!)
 t3,t4: decode command, increment R

memory read/write cycles (accoding to the official Zilog manual)
t1,t2: memory address select
  here comes contetion (AFTER t2!)
t3: bus read/write

port i/o cycles (accoding to the official Zilog manual)
t1,t2: port address select
tw: automatic wait tick
  here comes contetion (AFTER t2!)
t3: bus read/write


flag register bits
==================
bit 0: carry
bit 1: flagN (see below)
bit 2: parity/overflow
bit 3: bit3
bit 4: half-carry
bit 5: bit5
bit 6: zero
bit 7: sign

flagN:
  set if the last operation was a subtraction (used by DAA).

P/V - parity or overflow
  parity set if even number of bits set
  overflow set if the 2-complement result does not fit in the register

*******************************************************************************/
