/* This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
/* define this for FUSE test suite */
#define ZYMOSIS_FUSE_TEST

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "zymosis.h"
#include "zymosis.c"


static zym_cpu_t z80;
static uint8_t memory[65536], memsave[65536];
static char descr[8192];


static void z80Contention (zym_cpu_t *z80, uint16_t addr, zym_memio_t mio, zym_memreq_t mreq) {
  printf("%5d MC %04x\n", z80->tstates, addr);
}


static uint8_t z80MemRead (zym_cpu_t *z80, uint16_t addr, zym_memio_t mio) {
  if (mio != ZYM_MEMIO_OTHER) {
    printf("%5d MR %04x %02x\n", z80->tstates, addr, memory[addr]);
    // FUSE test hack for HALT
    if (mio == ZYM_MEMIO_OPCODE && memory[addr] == 0x76) z80->pc -= 1u;
  }
  return memory[addr];
}


static void z80MemWrite (zym_cpu_t *z80, uint16_t addr, uint8_t value, zym_memio_t mio) {
  if (mio != ZYM_MEMIO_OTHER) {
    printf("%5d MW %04x %02x\n", z80->tstates, addr, value);
  }
  memory[addr] = value;
}


static void z80PortContention (zym_cpu_t *z80, uint16_t port, int tstates, zym_portio_flags_t pflags) {
  if (pflags&ZYM_PORTIO_FLAG_EARLY) {
    if ((port&0xc000) == 0x4000) printf("%5d PC %04x\n", z80->tstates, port);
  } else {
    if (port&0x0001) {
      if ((port&0xc000) == 0x4000) {
        for (int f = 0; f < 3; ++f) printf("%5d PC %04x\n", z80->tstates+f, port);
      }
    } else {
      printf("%5d PC %04x\n", z80->tstates, port);
    }
  }
  z80->tstates += tstates;
}


// see the note for z80PortContention
static uint8_t z80PortIn (zym_cpu_t *z80, uint16_t port, zym_portio_t pio) {
  if (pio != ZYM_PORTIO_INTERNAL) {
    printf("%5d PR %04x %02x\n", z80->tstates, port, port>>8);
  }
  return port>>8;
}


static void z80PortOut (zym_cpu_t *z80, uint16_t port, uint8_t value, zym_portio_t pio) {
  if (pio != ZYM_PORTIO_INTERNAL) {
    printf("%5d PW %04x %02x\n", z80->tstates, port, value);
  }
}


// !0: error or done
static int readTestIn (FILE *fl) {
  unsigned int i, r, i1, i2, im, hlt, ts;
  unsigned int af, bc, de, hl, afx, bcx, dex, hlx, ix, iy, sp, pc, memptr;
  int done = 1, ch;
  while (fgets(descr, sizeof(descr), fl) != NULL) {
    descr[sizeof(descr)-1] = '\0';
    for (char *t = descr+strlen(descr)-1; t >= descr && isspace(*t); --t) *t = '\0';
    if (descr[0]) { done = 0; break; }
  }
  if (done) return -1;
  zym_reset(&z80);
  z80.evenM1 = 0;
  fscanf(fl, "%04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x",
    &af, &bc, &de, &hl, &afx, &bcx, &dex, &hlx, &ix, &iy, &sp, &pc, &memptr);
  z80.af.w = af;
  z80.bc.w = bc;
  z80.de.w = de;
  z80.hl.w = hl;
  z80.afx.w = afx;
  z80.bcx.w = bcx;
  z80.dex.w = dex;
  z80.hlx.w = hlx;
  z80.ix.w = ix;
  z80.iy.w = iy;
  z80.sp.w = sp;
  z80.pc = pc;
  z80.memptr.w = memptr;
  fscanf(fl, "%02x %02x %d %d %d %d %d", &i, &r, &i1, &i2, &im, &hlt, &ts);
  z80.regI = i;
  z80.regR = r;
  z80.iff1 = i1;
  z80.iff2 = i2;
  z80.im = im;
  z80.tstates = 0;
  z80.next_event_tstate = ts;
  //z80.tstates_per_frame = 70000; //FIXME
  for (int f = 0; f < 65536; f += 4) {
    memory[f+0] = 0xde;
    memory[f+1] = 0xad;
    memory[f+2] = 0xbe;
    memory[f+3] = 0xef;
  }
  for (;;) {
    for (;;) {
      if ((ch = fgetc(fl)) == EOF) return -1;
      if (isspace(ch)) continue;
      if (ch == '-') {
        while ((ch = fgetc(fl)) != EOF) if (isspace(ch)) break;
        goto done;
      }
      ungetc(ch, fl);
      fscanf(fl, "%04x", &pc);
      break;
    }
    for (;;) {
      if ((ch = fgetc(fl)) == EOF) return -1;
      if (isspace(ch)) continue;
      if (ch == '-') {
        while ((ch = fgetc(fl)) != EOF) if (isspace(ch)) break;
        ungetc(ch, fl);
        break;
      }
      ungetc(ch, fl);
      fscanf(fl, "%02x", &r);
      memory[pc] = r;
      //printf("pc=0x%04x; byte=0x%02x\n", pc, r);
      pc = (pc+1)&0xffff;
    }
  }
done:
  memcpy(memsave, memory, 65536);
  return 0;
}


static void dumpState (void) {
  printf("%04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x %04x\n",
    z80.af.w, z80.bc.w, z80.de.w, z80.hl.w, z80.afx.w, z80.bcx.w, z80.dex.w, z80.hlx.w, z80.ix.w, z80.iy.w, z80.sp.w, z80.pc, z80.memptr.w);
  printf("%02x %02x %d %d %d %d %d\n", z80.regI, z80.regR, z80.iff1, z80.iff2, z80.im, z80.halted, z80.tstates);
  for (int f = 0; f < 65536; ++f) {
    if (memory[f] != memsave[f]) {
      printf("%04x ", (unsigned)f);
      while (f < 65536 && memory[f] != memsave[f]) printf("%02x ", memory[f++]);
      printf("-1\n");
    }
  }
  printf("\n");
}


int main (int argc, char *argv[]) {
  if (argc < 2) {
    fprintf(stderr, "usage: zytest path/to/tests.in\n");
    return 1;
  }
  FILE *fl;
  zym_init(&z80);
  //zym_clear_callbacks(&z80); // zym_init does this
  z80.mem_read = z80MemRead;
  z80.mem_write = z80MemWrite;
  z80.mem_contention = z80Contention;
  z80.port_read = z80PortIn;
  z80.port_write = z80PortOut;
  z80.port_contention = z80PortContention;
  fl = fopen(/*"testdata/tests.in"*/argv[1], "r");
  if (!fl) { fprintf(stderr, "WTF?!\n"); return 1; }
  while (readTestIn(fl) == 0) {
    if (argc > 2) {
      int found = 0;
      //
      for (int f = 2; f < argc; ++f) if (strcmp(argv[f], descr) == 0) { found = 1; break; }
      if (!found) continue;
    }
    if (argc > 2) {
      printf("%s\n", descr);
      dumpState();
    }
    printf("%s\n", descr);
    zym_exec(&z80);
    dumpState();
  }
  return 0;
}
