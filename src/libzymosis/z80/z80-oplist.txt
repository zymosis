           Full Z80 Opcode List Including Undocumented Opcodes
           ===================================================
             mdfs.net/Docs.Comp.Z80.Z80OpList - Update: 0.12
                 Author: J.G.Harston - Date: 15-04-1998
                           # = not on 8080

nn nn              DD nn           CB nn         FD CB dd nn      ED nn
------------------------------------------------------------------------------
00 NOP             -               RLC B         rlc (iy+d),b     -
01 LD   BC,&0000   -               RLC C         rlc (iy+d),c     -
02 LD   (BC),A     -               RLC D         rlc (iy+d),d     -
03 INC  BC         -               RLC E         rlc (iy+d),e     -
04 INC  B          -               RLC H         rlc (iy+d),h     -
05 DEC  B          -               RLC L         rlc (iy+d),l     -
06 LD   B,&00      -               RLC (HL)      rlc (IY+d)       -
07 RLCA            -               RLC A         rlc (iy+d),a     -
08#EX   AF,AF'     -               RRC B         rrc (iy+d),b     -
09 ADD  HL,BC      ADD  IX,BC      RRC C         rrc (iy+d),c     -
0A LD   A,(BC)     -               RRC D         rrc (iy+d),d     -
0B DEC  BC         -               RRC E         rrc (iy+d),e     -
0C INC  C          -               RRC H         rrc (iy+d),h     -
0D DEC  C          -               RRC L         rrc (iy+d),l     -
0E LD   C,&00      -               RRC (HL)      rrc (IY+d)       -
0F RRCA            -               RRC A         rrc (iy+d),a     -
10#DJNZ disp       -               RL   B        rl  (iy+d),b     -
11 LD   DE,&0000   -               RL   C        rl  (iy+d),c     -
12 LD   (DE),A     -               RL   D        rl  (iy+d),d     -
13 INC  DE         -               RL   E        rl  (iy+d),e     -
14 INC  D          -               RL   H        rl  (iy+d),h     -
15 DEC  D          -               RL   L        rl  (iy+d),l     -
16 LD   D,&00      -               RL   (HL)     RL  (IY+d)       -
17 RLA             -               RL   A        rl  (iy+d),a     -
18#JR   disp       -               RR   B        rr  (iy+d),b     -
19 ADD  HL,DE      ADD  IX,DE      RR   C        rr  (iy+d),c     -
1A LD   A,(DE)     -               RR   D        rr  (iy+d),d     -
1B DEC  DE         -               RR   E        rr  (iy+d),e     -
1C INC  E          -               RR   H        rr  (iy+d),h     -
1D DEC  E          -               RR   L        rr  (iy+d),l     -
1E LD   E,&00      -               RR   (HL)     RR  (IY+d)       -
1F RRA             -               RR   A        rr  (iy+d),a     -
20#JR   NZ,disp    -               SLA  B        sla (iy+d),b     -
21 LD   HL,&0000   LD   IX,&0000   SLA  C        sla (iy+d),c     -
22 LD   (&0000),HL LD   (&0000),IX SLA  D        sla (iy+d),d     -
23 INC  HL         INC  IX         SLA  E        sla (iy+d),e     -
24 INC  H          INC  IXH        SLA  H        sla (iy+d),h     -
25 DEC  H          DEC  IXH        SLA  L        sla (iy+d),l     -
26 LD   H,&00      LD   IXH,&00    SLA  (HL)     SLA (IY+d)       -
27 DAA             -               SLA  A        sla (iy+d),a     -
28#JR   Z,disp     -               SRA  B        sra (iy+d),b     -
29 ADD  HL,HL      ADD  IX,IX      SRA  C        sra (iy+d),c     -
2A LD   HL,(&0000) LD   IX,(&0000) SRA  D        sra (iy+d),d     -
2B DEC  HL         DEC  IX         SRA  E        sra (iy+d),e     -
2C INC  L          INC  IXL        SRA  H        sra (iy+d),h     -
2D DEC  L          DEC  IXL        SRA  L        sra (iy+d),l     -
2E LD   L,&00      LD   IXL,&00    SRA  (HL)     SRA (IY+d)       -
2F CPL             -               SRA  A        sra (iy+d),a     -
30#JR   NC,disp    -               sls  b        sls (iy+d),b     -
31 LD   SP,&0000   -               sls  c        sls (iy+d),c     -
32 LD   (&0000),A  -               sls  d        sls (iy+d),d     -
33 INC  SP         -               sls  e        sls (iy+d),e     -
34 INC  (HL)       INC  (IX+d)     sls  h        sls (iy+d),h     -
35 DEC  (HL)       DEC  (IX+d)     sls  l        sls (iy+d),l     -
36 LD   (HL),&00   LD  (IX+d),&00  sls  (hl)     sls (iy+d)       -
37 SCF             -               sls  a        sls (iy+d),a     -
38#JR   C,disp     -               SRL  B        srl (iy+d),b     -
39 ADD  HL,SP      ADD  IX,SP      SRL  C        srl (iy+d),c     -
3A LD   A,(&0000)  -               SRL  D        srl (iy+d),d     -
3B DEC  SP         -               SRL  E        srl (iy+d),e     -
3C INC  A          -               SRL  H        srl (iy+d),h     -
3D DEC  A          -               SRL  L        srl (iy+d),l     -
3E LD   A,&00      -               SRL  (HL)     SRL (IY+d)       -
3F CCF             -               SRL  A        srl (iy+d),a     -
40 LD   B,B        -               BIT  0,B      bit 0,(iy+d)     IN   B,(C)
41 LD   B,C        -               BIT  0,C      bit 0,(iy+d)     OUT  (C),B
42 LD   B,D        -               BIT  0,D      bit 0,(iy+d)     SBC  HL,BC
43 LD   B,E        -               BIT  0,E      bit 0,(iy+d)     LD   (&0000),BC
44 LD   B,H        LD   B,IXH      BIT  0,H      bit 0,(iy+d)     NEG
45 LD   B,L        LD   B,IXL      BIT  0,L      bit 0,(iy+d)     RETN
46 LD   B,(HL)     LD   B,(IX+d)   BIT  0,(HL)   BIT 0,(IY+d)     IM   0
47 LD   B,A        -               BIT  0,A      bit 0,(iy+d)     LD   I,A
48 LD   C,B        -               BIT  1,B      bit 1,(iy+d)     IN   C,(C)
49 LD   C,C        -               BIT  1,C      bit 1,(iy+d)     OUT  (C),C
4A LD   C,D        -               BIT  1,D      bit 1,(iy+d)     ADC  HL,BC
4B LD   C,E        -               BIT  1,E      bit 1,(iy+d)     LD   BC,(&0000)
4C LD   C,H        LD   C,IXH      BIT  1,H      bit 1,(iy+d)     neg
4D LD   C,L        LD   C,IXL      BIT  1,L      bit 1,(iy+d)     RETI
4E LD   C,(HL)     LD   C,(IX+d)   BIT  1,(HL)   BIT 1,(IY+d)     im   0
4F LD   C,A        -               BIT  1,A      bit 1,(iy+d)     LD   R,A
50 LD   D,B        -               BIT  2,B      bit 2,(iy+d)     IN   D,(C)
51 LD   D,C        -               BIT  2,C      bit 2,(iy+d)     OUT  (C),D
52 LD   D,D        -               BIT  2,D      bit 2,(iy+d)     SBC  HL,DE
53 LD   D,E        -               BIT  2,E      bit 2,(iy+d)     LD   (&0000),DE
54 LD   D,H        LD   D,IXH      BIT  2,H      bit 2,(iy+d)     neg
55 LD   D,L        LD   D,IXL      BIT  2,L      bit 2,(iy+d)     retn
56 LD   D,(HL)     LD   D,(IX+d)   BIT  2,(HL)   BIT 2,(IY+d)     IM   1
57 LD   D,A        -               BIT  2,A      bit 2,(iy+d)     LD   A,I
58 LD   E,B        -               BIT  3,B      bit 3,(iy+d)     IN   E,(C)
59 LD   E,C        -               BIT  3,C      bit 3,(iy+d)     OUT  (C),E
5A LD   E,D        -               BIT  3,D      bit 3,(iy+d)     ADC  HL,DE
5B LD   E,E        -               BIT  3,E      bit 3,(iy+d)     LD   DE,(&0000)
5C LD   E,H        LD   E,IXH      BIT  3,H      bit 3,(iy+d)     neg
5D LD   E,L        LD   E,IXL      BIT  3,L      bit 3,(iy+d)     retn
5E LD   E,(HL)     LD   E,(IX+d)   BIT  3,(HL)   BIT 3,(IY+d)     IM   2
5F LD   E,A        -               BIT  3,A      bit 3,(iy+d)     LD   A,R
60 LD   H,B        LD   IXH,B      BIT  4,B      bit 4,(iy+d)     IN   H,(C)
61 LD   H,C        LD   IXH,C      BIT  4,C      bit 4,(iy+d)     OUT  (C),H
62 LD   H,D        LD   IXH,D      BIT  4,D      bit 4,(iy+d)     SBC  HL,HL
63 LD   H,E        LD   IXH,E      BIT  4,E      bit 4,(iy+d)     LD   (&0000),HL
64 LD   H,H        LD   IXH,IXH    BIT  4,H      bit 4,(iy+d)     neg
65 LD   H,L        LD   IXH,IXL    BIT  4,L      bit 4,(iy+d)     retn
66 LD   H,(HL)     LD   H,(IX+d)   BIT  4,(HL)   BIT 4,(IY+d)     im   0
67 LD   H,A        LD   IXH,A      BIT  4,A      bit 4,(iy+d)     RRD
68 LD   L,B        LD   IXL,B      BIT  5,B      bit 5,(iy+d)     IN   L,(C)
69 LD   L,C        LD   IXL,C      BIT  5,C      bit 5,(iy+d)     OUT  (C),L
6A LD   L,D        LD   IXL,D      BIT  5,D      bit 5,(iy+d)     ADC  HL,HL
6B LD   L,E        LD   IXL,E      BIT  5,E      bit 5,(iy+d)     LD   HL,(&0000)
6C LD   L,H        LD   IXL,IXH    BIT  5,H      bit 5,(iy+d)     neg
6D LD   L,L        LD   IXL,IXL    BIT  5,L      bit 5,(iy+d)     retn
6E LD   L,(HL)     LD   L,(IX+d)   BIT  5,(HL)   BIT 5,(IY+d)     im   0
6F LD   L,A        LD   IXL,A      BIT  5,A      bit 5,(iy+d)     RLD
70 LD   (HL),B     LD   (IX+d),B   BIT  6,B      bit 6,(iy+d)     IN   F,(C)
71 LD   (HL),C     LD   (IX+d),C   BIT  6,C      bit 6,(iy+d)     OUT  (C),F
72 LD   (HL),D     LD   (IX+d),D   BIT  6,D      bit 6,(iy+d)     SBC  HL,SP
73 LD   (HL),E     LD   (IX+d),E   BIT  6,E      bit 6,(iy+d)     LD   (&0000),SP
74 LD   (HL),H     LD   (IX+d),H   BIT  6,H      bit 6,(iy+d)     neg
75 LD   (HL),L     LD   (IX+d),L   BIT  6,L      bit 6,(iy+d)     retn
76 HALT            -               BIT  6,(HL)   BIT 6,(IY+d)     im   1
77 LD   (HL),A     LD   (IX+d),A   BIT  6,A      bit 6,(iy+d)     ld   i,i
78 LD   A,B        -               BIT  7,B      bit 7,(iy+d)     IN   A,(C)
79 LD   A,C        -               BIT  7,C      bit 7,(iy+d)     OUT  (C),A
7A LD   A,D        -               BIT  7,D      bit 7,(iy+d)     ADC  HL,SP
7B LD   A,E        -               BIT  7,E      bit 7,(iy+d)     LD   SP,(&0000)
7C LD   A,H        LD   A,IXH      BIT  7,H      bit 7,(iy+d)     neg
7D LD   A,L        LD   A,IXL      BIT  7,L      bit 7,(iy+d)     retn
7E LD   A,(HL)     LD   A,(IX+d)   BIT  7,(HL)   BIT 7,(IY+d)     im   2
7F LD   A,A        -               BIT  7,A      bit 7,(iy+d)     ld   r,r
80 ADD  A,B        -               RES  0,B      res 0,(iy+d),b   -
81 ADD  A,C        -               RES  0,C      res 0,(iy+d),c   -
82 ADD  A,D        -               RES  0,D      res 0,(iy+d),d   -
83 ADD  A,E        -               RES  0,E      res 0,(iy+d),e   -
84 ADD  A,H        ADD  A,IXH      RES  0,H      res 0,(iy+d),h   -
85 ADD  A,L        ADD  A,IXL      RES  0,L      res 0,(iy+d),l   -
86 ADD  A,(HL)     ADD  A,(IX+d)   RES  0,(HL)   RES 0,(IY+d)     -
87 ADD  A,A        -               RES  0,A      res 0,(iy+d),a   -
88 ADC  A,B        -               RES  1,B      res 1,(iy+d),b   -
89 ADC  A,C        -               RES  1,C      res 1,(iy+d),c   -
8A ADC  A,D        -               RES  1,D      res 1,(iy+d),d   -
8B ADC  A,E        -               RES  1,E      res 1,(iy+d),e   -
8C ADC  A,H        ADC  A,IXH      RES  1,H      res 1,(iy+d),h   -
8D ADC  A,L        ADC  A,IXL      RES  1,L      res 1,(iy+d),l   -
8E ADC  A,(HL)     ADC  A,(IX+d)   RES  1,(HL)   RES 1,(IY+d)     -
8F ADC  A,A        -               RES  1,A      res 1,(iy+d),a   -
90 SUB  A,B        -               RES  2,B      res 2,(iy+d),b   -
91 SUB  A,C        -               RES  2,C      res 2,(iy+d),c   -
92 SUB  A,D        -               RES  2,D      res 2,(iy+d),d   -
93 SUB  A,E        -               RES  2,E      res 2,(iy+d),e   -
94 SUB  A,H        SUB  A,IXH      RES  2,H      res 2,(iy+d),h   -
95 SUB  A,L        SUB  A,IXL      RES  2,L      res 2,(iy+d),l   -
96 SUB  A,(HL)     SUB  A,(IX+d)   RES  2,(HL)   RES 2,(IY+d)     -
97 SUB  A,A        -               RES  2,A      res 2,(iy+d),a   -
98 SBC  A,B        -               RES  3,B      res 3,(iy+d),b   -
99 SBC  A,C        -               RES  3,C      res 3,(iy+d),c   -
9A SBC  A,D        -               RES  3,D      res 3,(iy+d),d   -
9B SBC  A,E        -               RES  3,E      res 3,(iy+d),e   -
9C SBC  A,H        SBC  A,IXH      RES  3,H      res 3,(iy+d),h   -
9D SBC  A,L        SBC  A,IXL      RES  3,L      res 3,(iy+d),l   -
9E SBC  A,(HL)     SBC  A,(IX+d)   RES  3,(HL)   RES 3,(IY+d)     -
9F SBC  A,A        -               RES  3,A      res 3,(iy+d),a   -
A0 AND  B          -               RES  4,B      res 4,(iy+d),b   LDI
A1 AND  C          -               RES  4,C      res 4,(iy+d),c   CPI
A2 AND  D          -               RES  4,D      res 4,(iy+d),d   INI
A3 AND  E          -               RES  4,E      res 4,(iy+d),e   OTI
A4 AND  H          AND  IXH        RES  4,H      res 4,(iy+d),h   -
A5 AND  L          AND  IXL        RES  4,L      res 4,(iy+d),l   -
A6 AND  (HL)       AND  (IX+d)     RES  4,(HL)   RES 4,(IY+d)     -
A7 AND  A          -               RES  4,A      res 4,(iy+d),a   -
A8 XOR  B          -               RES  5,B      res 5,(iy+d),b   LDD
A9 XOR  C          -               RES  5,C      res 5,(iy+d),c   CPD
AA XOR  D          -               RES  5,D      res 5,(iy+d),d   IND
AB XOR  E          -               RES  5,E      res 5,(iy+d),e   OTD
AC XOR  H          XOR  IXH        RES  5,H      res 5,(iy+d),h   -
AD XOR  L          XOR  IXL        RES  5,L      res 5,(iy+d),l   -
AE XOR  (HL)       XOR  (IX+d)     RES  5,(HL)   RES 5,(IY+d)     -
AF XOR  A          -               RES  5,A      res 5,(iy+d),a   -
B0 OR   B          -               RES  6,B      res 6,(iy+d),b   LDIR
B1 OR   C          -               RES  6,C      res 6,(iy+d),c   CPIR
B2 OR   D          -               RES  6,D      res 6,(iy+d),d   INIR
B3 OR   E          -               RES  6,E      res 6,(iy+d),e   OTIR
B4 OR   H          OR   IXH        RES  6,H      res 6,(iy+d),h   -
B5 OR   L          OR   IXL        RES  6,L      res 6,(iy+d),l   -
B6 OR   (HL)       OR   (IX+d)     RES  6,(HL)   RES 6,(IY+d)     -
B7 OR   A          -               RES  6,A      res 6,(iy+d),a   -
B8 CP   B          -               RES  7,B      res 7,(iy+d),b   LDDR
B9 CP   C          -               RES  7,C      res 7,(iy+d),c   CPDR
BA CP   D          -               RES  7,D      res 7,(iy+d),d   INDR
BB CP   E          -               RES  7,E      res 7,(iy+d),e   OTDR
BC CP   H          CP   IXH        RES  7,H      res 7,(iy+d),h   -
BD CP   L          CP   IXL        RES  7,L      res 7,(iy+d),l   -
BE CP   (HL)       CP   (IX+d)     RES  7,(HL)   RES 7,(IY+d)     -
BF CP   A          -               RES  7,A      res 7,(iy+d),a   -
C0 RET  NZ         -               SET  0,B      set 0,(iy+d),b   -
C1 POP  BC         -               SET  0,C      set 0,(iy+d),c   -
C2 JP   NZ,&0000   -               SET  0,D      set 0,(iy+d),d   -
C3 JP   &0000      -               SET  0,E      set 0,(iy+d),e   -
C4 CALL NZ,&0000   -               SET  0,H      set 0,(iy+d),h   -
C5 PUSH BC         -               SET  0,L      set 0,(iy+d),l   -
C6 ADD  A,&00      -               SET  0,(HL)   SET 0,(IY+d)     -
C7 RST  &00        -               SET  0,A      set 0,(iy+d),a   -
C8 RET  Z          -               SET  1,B      set 1,(iy+d),b   -
C9 RET             -               SET  1,C      set 1,(iy+d),c   -
CA JP   Z,&0000    -               SET  1,D      set 1,(iy+d),d   -
CB#**** CB ****    -               SET  1,E      set 1,(iy+d),e   -
CC CALL Z,&0000    -               SET  1,H      set 1,(iy+d),h   -
CD CALL &0000      -               SET  1,L      set 1,(iy+d),l   -
CE ADC  A,&00      -               SET  1,(HL)   SET 1,(IY+d)     -
CF RST  &08        -               SET  1,A      set 1,(iy+d),a   -
D0 RET  NC         -               SET  2,B      set 2,(iy+d),b   -
D1 POP  DE         -               SET  2,C      set 2,(iy+d),c   -
D2 JP   NC,&0000   -               SET  2,D      set 2,(iy+d),d   -
D3 OUT  (&00),A    -               SET  2,E      set 2,(iy+d),e   -
D4 CALL NC,&0000   -               SET  2,H      set 2,(iy+d),h   -
D5 PUSH DE         -               SET  2,L      set 2,(iy+d),l   -
D6 SUB  A,&00      -               SET  2,(HL)   SET 2,(IY+d)     -
D7 RST  &10        -               SET  2,A      set 2,(iy+d),a   -
D8 RET  C          -               SET  3,B      set 3,(iy+d),b   -
D9#EXX             -               SET  3,C      set 3,(iy+d),c   -
DA JP   C,&0000    -               SET  3,D      set 3,(iy+d),d   -
DB IN   A,(&00)    -               SET  3,E      set 3,(iy+d),e   -
DC CALL C,&0000    -               SET  3,H      set 3,(iy+d),h   -
DD#**** DD ****    -               SET  3,L      set 3,(iy+d),l   -
DE SBC  A,&00      -               SET  3,(HL)   SET 3,(IY+d)     -
DF RST  &18        -               SET  3,A      set 3,(iy+d),a   -
E0 RET  PO         -               SET  4,B      set 4,(iy+d),b   -
E1 POP  HL         POP  IX         SET  4,C      set 4,(iy+d),c   -
E2 JP   PO,&0000   -               SET  4,D      set 4,(iy+d),d   -
E3 EX   (SP),HL    EX   (SP),IX    SET  4,E      set 4,(iy+d),e   -
E4 CALL PO,&0000   -               SET  4,H      set 4,(iy+d),h   -
E5 PUSH HL         PUSH IX         SET  4,L      set 4,(iy+d),l   -
E6 AND  &00        -               SET  4,(HL)   SET 4,(IY+d)     -
E7 RST  &20        -               SET  4,A      set 4,(iy+d),a   -
E8 RET  PE         -               SET  5,B      set 5,(iy+d),b   -
E9 JP   (HL)       JP   (IX)       SET  5,C      set 5,(iy+d),c   -
EA JP   PE,&0000   -               SET  5,D      set 5,(iy+d),d   -
EB EX   DE,HL      -               SET  5,E      set 5,(iy+d),e   -
EC CALL PE,&0000   -               SET  5,H      set 5,(iy+d),h   -
ED#**** ED ****    -               SET  5,L      set 5,(iy+d),l   -
EE XOR  &00        -               SET  5,(HL)   SET 5,(IY+d)     -
EF RST  &28        -               SET  5,A      set 5,(iy+d),a   -
F0 RET  P          -               SET  6,B      set 6,(iy+d),b   MOS_WRINF
F1 POP  AF         -               SET  6,C      set 6,(iy+d),c   MOS_RDINF
F2 JP   P,&0000    -               SET  6,D      set 6,(iy+d),d   MOS_SYS
F3 DI              -               SET  6,E      set 6,(iy+d),e   MOS_MISC
F4 CALL P,&0000    -               SET  6,H      set 6,(iy+d),h   MOS_FIND
F5 PUSH AF         -               SET  6,L      set 6,(iy+d),l   MOS_GBPB
F6 OR   &00        -               SET  6,(HL)   SET 6,(IY+d)     MOS_BPUT
F7 RST  &30        -               SET  6,A      set 6,(iy+d),a   MOS_BGET
F8 RET  M          -               SET  7,B      set 7,(iy+d),b   MOS_ARGS  UNUSED
F9 LD   SP,HL      LD   SP,IX      SET  7,C      set 7,(iy+d),c   MOS_FILE  Z80_COPY
FA JP   M,&0000    -               SET  7,D      set 7,(iy+d),d   MOS_RDCH  Z80_RDMEM
FB EI              -               SET  7,E      set 7,(iy+d),e   MOS_WRCH  Z80_WRMEM
FC CALL M,&0000    -               SET  7,H      set 7,(iy+d),h   MOS_WORD  Z80_SERIN
FD#**** FD ****    -               SET  7,L      set 7,(iy+d),l   MOS_BYTE  Z80_SEROUT
FE CP   &00        -               SET  7,(HL)   SET 7,(IY+d)     MOS_CLI   Z80_WRROM
FF RST  &38        -               SET  7,A      set 7,(iy+d),a   MOS_QUIT  Z80_QUIT

Unofficial (otherwise known as undocumented) instructions are shown in lower
case.

Notes on index registers
------------------------
Where DD and IX are mentioned, FD and IY may be substituted and vis versa.
If a DD or FD opcode prefixes an instruction that does not use the HL
register, then the DD or FD opcode acts as a NOP and the base instruction is
executed. For example, DD FF does a RST &38.

Notes on Indexed Shift/Bit Operations
-------------------------------------
A shift or bit operation on an indexed byte in memory is done by prefixing a
CB opcode refering to (HL) with DD or FD to specify (IX+n) or (IY+n).  If
the CB opcode does not refer to (HL), slightly differing things happen. The
majority of Z80 CPUs execute them as shown; the shift or bit operation is
done on the indexed byte in memory, and then if the opcode does not specify
(HL) originally, the resultant byte is copied into the specified register.
This is summarised with this example:

       CB 0x    RLC r          FD CB nn 0x     RLC (IY+nn),r
       for x=0..5,7 for r=B,C,D,E,H,L,A

Most CPUs allow access to the high and low halves of the index register. In
this example, if x is 4 or 5, the operation does RLC IYH or RLC IYH.

       CB 04   RLC H           FD CB nn 04     RLC IYH
       CB 05   RLC L           FD CB nn 05     RLC IYL

Some emulation systems treat all the subcodes as accessing the indexed byte
and nothing else:

       CB 0x   RLC r           FD CB nn 0x     RLC (IY+nn)
       for all x=0..7

Notes on ED opcodes
-------------------
The opcodes ED 77 and ED 7F appear to have no effect other than setting the
P/V flag according to the interupt status. As such, they appear to be
similar to the LD A,I and LD A,R instructions. From the layout of the opcode
map, it would seem that ED 77 is LD I,I and ED 7F is LD R,R.

Opcodes 00 to 3F and C0 to FF (other than the block instructions) just
increment the R register by 2, and act as a two-byte NOP.

J.G.Harston's !Z80Tube Z80 CoPro emulator includes the extra opcodes EDF0 to
EDFF to interface with the host.

G.A.Lunter's Z80 Spectrum emulator includes the extra opcodes EDF8 to EDFF
to interface to the host.

References
----------
Harston J.G., Investigative research.

Harston J.G., Z80Tube documentation.
  http://mdfs.net/Apps/Emulators/Tube/Z80Tube/

Lunter G.A., Z80 Spectrum emulator documentation.
  http://mdfs.net/Apps/Emulators/Spectrum/Z80/v304/
