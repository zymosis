the following is not 100% correct, but the position
of the contention cycles is right

m1 cycle (accoding to the official Zilog manual)
 t1: setting /MREQ & /RD (t1/1 -- second half of the clock)
 t2: memory read
  here comes contetion (AFTER t2!)
 t3,t4: decode command, increment R (/M1 inactive, /RFSH active)

memory read/write cycles (accoding to the official Zilog manual)
t1,t2: memory address select
  here comes contetion (AFTER t2!)
t3: bus read/write

port i/o cycles (accoding to the official Zilog manual)
t1,t2: port address select
tw: automatic wait tick
  here comes contetion (AFTER t2!)
  ??? Zilog manual says that it is BEFORE tw.
t3: bus read/write


what Zilog diagram says: /WAIT is raised DURING t2. that is, wait
cycles are actually inserted BEFORE finishing t2. actual memory
sampling happens on the rising edge of t3 (t3/0). during t3 and
t4, lower 7 bits of the address bus contains refresh address.

this is confirmed by figure 4.0-1A in the manual: Tw states are
inserted BEFORE t3 goes active. i.e. "wait" is a very-very long
t2.

that is, contention tables should be calculated as if we are not
leaved the Ts yet.

the similar picture is for memory reads and writes (only they
don't have t3, and no refresh logic applied).


for port input:
/IORQ activates on t2/0.
wait states are after t2.
t3 is port read, and that's where the data bus is sampled. i guess
that this is the exact time when we could catch ULA leftovers.

for port output we have automatic wait cycle before t3. the diagram
insists that wait cycles are inserted after t2, not before t3, so
they are before tw. output value is on the data bus from t1/0 to the
end of the output (end of t3).
