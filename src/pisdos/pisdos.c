/***************************************************************************
 *
 *  pisDOS disk file extractor
 *
 *  Written by Ketmar Dark <ketmar@ketmar.no-ip.org>
 *  Used some information from HalfElf XiSD FAR plugin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "../libfusefdc/libfusefdc.h"


//==========================================================================
//
//  cprintLibFDC
//
//==========================================================================
static void cprintLibFDC (int type, const char *msg) {
  switch (type) {
    case LIBFDC_MSG_DEBUG: fprintf(stderr, "LIBFDC[debug]: %s\n", msg); break;
    case LIBFDC_MSG_WARNING: fprintf(stderr, "LIBFDC[warn]: %s\n", msg); break;
    case LIBFDC_MSG_ERROR: fprintf(stderr, "LIBFDC[error]: %s\n", msg); break;
    default: fprintf(stderr, "LIBFDC[???]: %s\n", msg); break; // the thing that should not be
  }
  fflush(stderr);
}


//==========================================================================
//
//  pisdos_print_dir_entry
//
//==========================================================================
static int pisdos_print_dir_entry (const PisDOS_DirEnt *de, void *udata) {
  const char *month_names[12] = {"jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"};
  PisDOSDateTime dt;
  pisdos_unpack_date(&dt, de->date);
  if (dt.month > 11) dt.month = 11;
  char name[16];
  pisdos_get_name(name, de);
  if (pisdos_is_dir(de)) {
    if (de->attr&PISDOS_FLAG_DIR) strcat(name, "/"); else strcat(name, "*");
    fprintf(stdout, "%-13s", name);
    fprintf(stdout, "%04u/%s/%02u --:--:--", dt.year, month_names[dt.month], dt.day+1);
    if (de->attr&PISDOS_FLAG_HIDDEN) fprintf(stdout, " [HIDDEN]");
    if ((de->attr&PISDOS_FLAG_CONTINUOUS) == 0) fprintf(stdout, " [SEGMENTED]");
    fprintf(stdout, "\n");
  } else {
    pisdos_unpack_time(&dt, de->file.time);
    fprintf(stdout, "%-13s", name);
    fprintf(stdout, "%04u/%s/%02u %02u:%02u:%02u", dt.year, month_names[dt.month], dt.day+1,
            dt.hour, dt.minute, dt.second);
    fprintf(stdout, " %8u", pisdos_get_entry_size(de));
    if (de->attr&PISDOS_FLAG_HIDDEN) fprintf(stdout, " [HIDDEN]");
    if ((de->attr&PISDOS_FLAG_CONTINUOUS) == 0) fprintf(stdout, " [SEGMENTED]");
    fprintf(stdout, "\n");
  }
  return 0;
}


//==========================================================================
//
//  pisdos_list_dir
//
//==========================================================================
static void pisdos_list_dir (disk_t *dsk, const PisDOS_DirEnt *dirde) {
  pisdos_dir_foreach(dsk, dirde, &pisdos_print_dir_entry, NULL);
  fflush(stdout);
}


//==========================================================================
//
//  main
//
//==========================================================================
int main (int argc, char **argv) {
  disk_t dsk;

  libfdcMessageCB = &cprintLibFDC;

  int dump_dpb = 0;
  const char *diskimg_fname = NULL;
  int file_start = 0;

  for (int f = 1; f < argc; ++f) {
    const char *arg = argv[f];
    if (!arg || !arg[0]) continue;

    if (strcmp(arg, "--") == 0) {
      fprintf(stderr, "wtf?\n");
      return 1;
    }

    if (arg[0] == '-') {
      if (strcmp(arg, "--dpb") == 0) { dump_dpb = 1; continue; }
      if (strcmp(arg, "--help") == 0) { diskimg_fname = NULL; break; }
      if (strcmp(arg, "-h") == 0) { diskimg_fname = NULL; break; }
      fprintf(stderr, "FATAL: unknown option '%s'\n", arg);
      return 1;
    }

    if (!diskimg_fname) {
      diskimg_fname = argv[f];
      continue;
    }

    // file names
    file_start = f;
    break;
  }

  if (!diskimg_fname) {
    fprintf(stderr, "usage: pisdos diskimg [path]\n");
    return 1;
  }

  disk_init_struct(&dsk);

  const int dopres = disk_open(&dsk, diskimg_fname);
  if (dopres != DISK_OK) {
    fprintf(stderr, "FATAL: cannot open disk image '%s'! (error: %s)\n",
            diskimg_fname, disk_strerror(dopres));
    disk_close(&dsk);
    return 1;
  }

  fflush(stderr);
  fprintf(stdout, "*** DISK INFO: %d heads, %d cylinders; %d BPT ***\n",
          dsk.sides, dsk.cylinders, dsk.bpt);
  fflush(stdout);

  {
    char dsktitle[16];
    pisdos_get_title(dsktitle, pisdos_get_dpb(&dsk));
    if (dsktitle[0]) fprintf(stdout, "*** DISK TITLE: %s ***\n", dsktitle);
  }

  if (dump_dpb) {
    #if 0
    disk_write(&dsk, "_z00.fdi");
    #endif
    pisdos_debug_dump_dpb(&dsk);
    disk_close(&dsk);
    return 0;
  }

  #if 0
  {
    const uint8_t *blk = pisdos_block_ptr(&dsk, 43);
    fprintf(stderr, "frag_count : %u\n", blk[0]);
    fprintf(stderr, "frag_start : %u\n", blk[1]|(blk[2]<<8));
    fprintf(stderr, "frag_length: %u\n", blk[3]);
    exit(0);
  }
  #endif

  if (!file_start) {
    pisdos_list_dir(&dsk, NULL);
    disk_close(&dsk);
    return 0;
  }

  PisDOSFindInfo fi;
  char outfname[16];


  for (int f = file_start; f < argc; ++f) {
    const PisDOS_DirEnt *de = pisdos_findfirst(&fi, &dsk, argv[f]);
    if (!de) {
      fprintf(stderr, "ERROR: cannot find files with mask '%s'!\n", argv[f]);
      continue;
    }

    while (de) {
      memset(outfname, 0, sizeof(outfname));
      pisdos_get_name(outfname+1, de);
      outfname[0] = '_';

      if (pisdos_is_dir(de)) {
        fprintf(stdout, "===== %s/ =====\n", outfname+1);
        pisdos_list_dir(&dsk, de);
        fprintf(stdout, "------------\n");
        fflush(stdout);
      } else {
        PisDOSFragList frags;
        if (pisdos_build_frags(&dsk, de, &frags) != 0) {
          fprintf(stderr, "ERROR: cannot build fragment list for file '%s'\n", outfname+1);
          continue;
        }

        FILE *fo = fopen(outfname, "w");
        if (!fo) {
          fprintf(stderr, "ERROR: cannot create host file '%s'!\n", outfname);
          disk_close(&dsk);
          return 1;
        }
        uint32_t pos = 0;
        uint32_t left = pisdos_get_entry_size(de);
        while (left) {
          uint32_t rd = (left > 256 ? 256 : left);
          const void *bp = pisdos_next_block(&dsk, &frags);
          if (!bp) {
            fprintf(stderr, "ERROR reading pisdos file at position %u (%u bytes left)\n",
                    pos, left);
            break;
          }
          if (fwrite(bp, rd, 1, fo) != 1) {
            fprintf(stderr, "ERROR writing host file at position %u (%u bytes left)\n",
                    pos, left);
            break;
          }
          pos += rd;
          left -= rd;
        }
        fclose(fo);
        fprintf(stdout, "extracted '%s'.\n", outfname);
        fflush(stdout);
      }

      de = pisdos_findnext(&fi);
    }
  }

  disk_close(&dsk);

  return 0;
}
