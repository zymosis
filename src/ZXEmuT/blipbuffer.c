/*
 * Copyright (C) 2003-2006 Shay Green. This module is free software; you
 * can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version. This
 * module is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details. You should have received a copy of the GNU Lesser General
 * Public License along with this module; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Original C++ source:
 * Blip_Buffer 0.4.0. http://www.slack.net/~ant/
 *
 * partially reimplemented in C by Gergely Szasz for FUSE
 * some code cleanups and small fixes/optimisations by Ketmar // Invisible Vector
 */
/*
 * assumptions code makes about implementation-defined features
 * right shift of negative value preserves sign
 * casting to smaller signed type truncates bits and extends sign
 */
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "blipbuffer.h"


// ////////////////////////////////////////////////////////////////////////// //
/* Internal */
#define BLIP_WIDEST_IMPULSE_  (16)
#define BLIP_RES              (1<<BLIP_PHASE_BITS)

#define BUFFER_EXTRA  ((BLIP_WIDEST_IMPULSE_)+2)

#define BLIP_SAMPLE_BITS  (30)

#define BLIP_EQ_DEF_CUTOFF  (0)

//#define BLIP_DEFAULT_EQ_SAMPLE_RATE  (44100)
//k8: changed
#define BLIP_DEFAULT_EQ_SAMPLE_RATE  (48000)

/* Low-pass equalization parameters */
typedef struct blip_eq_s {
  double treble;
  int32_t rolloff_freq;
  int32_t sample_rate;
  int32_t cutoff_freq;
} blip_eq_t;

static void blipbuf_priv_synth_treble_eq (Blip_Synth_Priv *synth_, const blip_eq_t *eq);
static void blipbuf_priv_synth_volume_unit (Blip_Synth_Priv *synth_, double v);
static void blipbuf_priv_synth_init (Blip_Synth_Priv *synth_, blip_imp_t *impulses);

/* End of public interface */

#define BLIP_SYNTH_WIDTH BLIP_SYNTH_QUALITY  /* `quality' of synth as `width' of synth_ */


//==========================================================================
//
//  blip_buffer_set_clock_rate
//
//==========================================================================
void blip_buffer_set_clock_rate (Blip_Buffer *buff, uint32_t cps) {
  if (!buff) return;
  buff->clock_rate_ = cps;
  buff->factor_ = blip_buffer_clock_rate_factor(buff, cps);
}


//==========================================================================
//
//  blip_buffer_get_clock_rate
//
//==========================================================================
uint32_t blip_buffer_get_clock_rate (Blip_Buffer *buff) {
  return (buff ? buff->clock_rate_ : 0);
}


//==========================================================================
//
//  blip_buffer_get_sample_rate
//
//==========================================================================
uint32_t blip_buffer_get_sample_rate (Blip_Buffer *buff) {
  return (buff ? buff->sample_rate_ : 0);
}


//==========================================================================
//
//  blip_buffer_samples_avail
//
//==========================================================================
inline uint32_t blip_buffer_samples_avail (Blip_Buffer *buff) {
  return (uint32_t)(buff ? (buff->offset_>>BLIP_BUFFER_ACCURACY) : 0);
}


//==========================================================================
//
//  blip_buffer_samples_max
//
//==========================================================================
uint32_t blip_buffer_samples_max (Blip_Buffer *buff) {
  return (uint32_t)(buff ? buff->buffer_size_ : 0);
}


//==========================================================================
//
//  blip_synth_set_output
//
//==========================================================================
void blip_synth_set_output (Blip_Synth *synth, Blip_Buffer *b) {
  if (!synth) return;
  synth->impl.buf = b;
  synth->impl.last_amp = 0;
}


//==========================================================================
//
//  blip_synth_get_output
//
//==========================================================================
Blip_Buffer *blip_synth_get_output (Blip_Synth *synth) {
  return (synth ? synth->impl.buf : NULL);
}


//==========================================================================
//
//  blip_synth_set_volume
//
//==========================================================================
void blip_synth_set_volume (Blip_Synth *synth, double v) {
  if (!synth) return;
  blipbuf_priv_synth_volume_unit(&synth->impl, v*(1.0/BLIP_SYNTH_RANGE));
}


//==========================================================================
//
//  blip_synth_set_volume_amp_range
//
//==========================================================================
void blip_synth_set_volume_amp_range (Blip_Synth *synth, double v, int amprange) {
  if (amprange < 0) {
    amprange = -amprange;
    if (amprange < 0) __builtin_trap(); // oops
  } else if (!amprange) {
    amprange = 65535; // because why not
  }
  blipbuf_priv_synth_volume_unit(&synth->impl, v*(1.0/amprange));
}


#define BLIP_FWD(i) \
  t0 = i0*delta+buf[fwd+i]; \
  t1 = imp[BLIP_RES*(i+1)]*delta+buf[fwd+1+i]; \
  i0 = imp[BLIP_RES*(i+2)]; \
  buf[fwd+i] = t0; \
  buf[fwd+1+i] = t1

#define BLIP_REV(r) \
  t0 = i0*delta+buf[rev-r]; \
  t1 = imp[BLIP_RES*r]*delta+buf[rev+1-r]; \
  i0 = imp[BLIP_RES*(r-1)]; \
  buf[rev-r] = t0; \
  buf[rev+1-r] = t1


//==========================================================================
//
//  blip_synth_offset_resampled
//
//==========================================================================
void blip_synth_offset_resampled (Blip_Synth *synth, blip_resampled_time_t time,
                                  int delta, Blip_Buffer *blip_buf)
{
  if (!synth) return;

  int32_t t0, t1;

  delta *= synth->impl.delta_factor;

  const int phase = (int)(time>>(BLIP_BUFFER_ACCURACY-BLIP_PHASE_BITS)&(BLIP_RES-1));
  blip_imp_t *imp = synth->impulses+BLIP_RES-phase;
  int32_t *buf = blip_buf->buffer_+(time>>BLIP_BUFFER_ACCURACY);
  int32_t i0 = *imp;

  const int fwd = (BLIP_WIDEST_IMPULSE_-BLIP_SYNTH_QUALITY)/2;
  const int rev = fwd+BLIP_SYNTH_QUALITY-2;

  BLIP_FWD(0);
  if (BLIP_SYNTH_QUALITY > 8) {
    BLIP_FWD(2);
  }
  if (BLIP_SYNTH_QUALITY > 12) {
    BLIP_FWD(4);
  }

  const int mid = BLIP_SYNTH_QUALITY/2-1;
  t0 = i0*delta+buf[fwd+mid-1];
  t1 = imp[BLIP_RES*mid]*delta+buf[fwd+mid];
  imp = synth->impulses+phase;
  i0 = imp[BLIP_RES*mid];
  buf[fwd+mid-1] = t0;
  buf[fwd+mid] = t1;

  if (BLIP_SYNTH_QUALITY > 12) {
    BLIP_REV(6);
  }
  if (BLIP_SYNTH_QUALITY > 8) {
    BLIP_REV(4);
  }
  BLIP_REV(2);

  t0 = i0*delta+buf[rev];
  t1 = *imp*delta+buf[rev+1];
  buf[rev] = t0;
  buf[rev+1] = t1;
}

#undef BLIP_FWD
#undef BLIP_REV


//==========================================================================
//
//  blip_synth_update
//
//==========================================================================
/*
void blip_synth_update (Blip_Synth *synth, blip_time_t t, int amp) {
  const int delta = amp-synth->impl.last_amp;
  synth->impl.last_amp = amp;
  blip_synth_offset_resampled(synth,
                              t*synth->impl.buf->factor_+synth->impl.buf->offset_,
                              delta, synth->impl.buf);
}
*/


//==========================================================================
//
//  blipbuf_priv_synth_impulses_size
//
//==========================================================================
static inline int blipbuf_priv_synth_impulses_size (Blip_Synth_Priv *synth_) {
  return BLIP_RES/2*BLIP_SYNTH_WIDTH+1;
}


//==========================================================================
//
//  blip_synth_set_treble_eq
//
//==========================================================================
void blip_synth_set_treble_eq (Blip_Synth *synth, double treble) {
  if (!synth) return;
  blip_eq_t eq = { 0.0, 0, BLIP_DEFAULT_EQ_SAMPLE_RATE, 0 };
  eq.treble = treble;
  //k8: fix sample rate, if we know it
  if (synth->impl.buf && synth->impl.buf->sample_rate_) {
    eq.sample_rate = synth->impl.buf->sample_rate_;
  }
  blipbuf_priv_synth_treble_eq(&synth->impl, &eq);
}


//==========================================================================
//
//  blip_buffer_init
//
//==========================================================================
static void blip_buffer_init (Blip_Buffer *buff) {
  buff->factor_ = LONG_MAX;
  buff->offset_ = 0;
  buff->buffer_ = NULL;
  buff->buffer_size_ = 0;
  buff->sample_rate_ = 0;
  buff->reader_accum = 0;
  buff->bass_shift = 0;
  buff->clock_rate_ = 0;
  buff->bass_freq_ = 16;
  buff->length_ = 0;
}


//==========================================================================
//
//  blip_buffer_end
//
//==========================================================================
static void blip_buffer_end (Blip_Buffer *buff) {
  if (buff->buffer_) free(buff->buffer_);
  buff->buffer_ = NULL;
}


//==========================================================================
//
//  new_Blip_Buffer
//
//==========================================================================
Blip_Buffer *new_Blip_Buffer (void) {
  Blip_Buffer *ret = malloc(sizeof(Blip_Buffer));
  if (ret) blip_buffer_init(ret);
  return ret;
}


//==========================================================================
//
//  delete_Blip_Buffer
//
//==========================================================================
void delete_Blip_Buffer (Blip_Buffer ** buff) {
  if (!buff || !*buff) return;
  blip_buffer_end(*buff);
  free(*buff);
  *buff = NULL;
}


//==========================================================================
//
//  blip_synth_reinit
//
//  reinit already created synth
//
//==========================================================================
void blip_synth_reinit (Blip_Synth *synth) {
  if (!synth || !synth->impulses) return;
  Blip_Buffer *buf = synth->impl.buf;
  const double ovol = synth->impl.volume_unit_;
  blipbuf_priv_synth_init(&synth->impl, synth->impulses);
  blip_synth_set_output(synth, buf);
  //TODO: can this work with NULL `buf`?
  synth->impl.volume_unit_ = ovol+1.0; // force volume update
  blipbuf_priv_synth_volume_unit(&synth->impl, ovol);
}


//==========================================================================
//
//  blip_synth_init
//
//==========================================================================
static void blip_synth_init (Blip_Synth *synth) {
  synth->impulses = malloc((BLIP_RES*(BLIP_SYNTH_QUALITY/2)+1)*sizeof(blip_imp_t)*4);
  if (synth->impulses) {
    blipbuf_priv_synth_init(&synth->impl, synth->impulses);
  }
}


//==========================================================================
//
//  blip_synth_end
//
//==========================================================================
static void blip_synth_end (Blip_Synth *synth) {
  if (synth->impulses) {
    free(synth->impulses);
    synth->impulses = NULL;
  }
}


//==========================================================================
//
//  new_Blip_Synth
//
//==========================================================================
Blip_Synth *new_Blip_Synth (void) {
  Blip_Synth *ret = malloc(sizeof(Blip_Synth));
  if (ret) {
    blip_synth_init(ret);
    if (!ret->impulses) {
      free(ret);
      return NULL;
    }
  }
  return ret;
}


//==========================================================================
//
//  delete_Blip_Synth
//
//==========================================================================
void delete_Blip_Synth (Blip_Synth **synth) {
  if (!synth || !*synth) return;
  blip_synth_end(*synth);
  free(*synth);
  *synth = NULL;
}


//==========================================================================
//
//  blip_buffer_clear
//
//==========================================================================
void blip_buffer_clear (Blip_Buffer *buff, blip_bool_t entire_buffer) {
  if (!buff) return;
  if (buff->buffer_) {
    const uint32_t count = (entire_buffer ? buff->buffer_size_ : blip_buffer_samples_avail(buff));
    memset(buff->buffer_, 0, (count+BUFFER_EXTRA)*sizeof(blip_buf_t));
  }
  buff->offset_ = 0;
  buff->reader_accum = 0;
}


//==========================================================================
//
//  blip_buffer_set_sample_rate
//
//==========================================================================
blip_status_t blip_buffer_set_sample_rate (Blip_Buffer *buff, uint32_t new_rate, int msec) {
  if (!buff) return -2;
  if (!new_rate) new_rate = 48000; /*k8: just in case */
  if (msec < 0) msec = 0; /*k8: just in case */
  if (msec > 2000) msec = 2000; /*k8: this is still wrong!*/

  /* start with maximum length that resampled time can represent */
  int32_t new_size = (ULONG_MAX>>BLIP_BUFFER_ACCURACY)-BUFFER_EXTRA-64;

  if (msec != BLIP_MAX_LENGTH) {
    const int32_t s = (new_rate*(msec+1)+999)/1000;
    if (s > 0 && s < new_size) new_size = s;
  }

  if (buff->buffer_size_ != new_size) {
    void *p = realloc(buff->buffer_, (new_size+BUFFER_EXTRA)*sizeof(blip_buf_t));
    if (!p) return -1; // out of memory
    buff->buffer_ = (blip_buf_t *)p;
  }

  buff->buffer_size_ = new_size;

  /* update things based on the sample rate */
  buff->sample_rate_ = new_rate;
  buff->length_ = new_size*1000/new_rate-1;
  if (buff->clock_rate_) blip_buffer_set_clock_rate(buff, buff->clock_rate_);
  blip_buffer_set_bass_freq(buff, buff->bass_freq_);

  blip_buffer_clear(buff, BLIP_BUFFER_DEF_ENTIRE_BUFF);

  return 0; /* success */
}


//==========================================================================
//
//  blip_buffer_clock_rate_factor
//
//==========================================================================
blip_resampled_time_t blip_buffer_clock_rate_factor (Blip_Buffer *buff, uint32_t clock_rate) {
  if (!buff) return 0;
  const double ratio = (double)buff->sample_rate_/clock_rate;
  const int32_t factor = (int32_t)floor(ratio*(1L<<BLIP_BUFFER_ACCURACY)+0.5);
  return (blip_resampled_time_t)factor;
}


//==========================================================================
//
//  blip_buffer_count_clocks
//
//==========================================================================
blip_time_t blip_buffer_count_clocks (Blip_Buffer *buff, int32_t count) {
  if (!buff) return 0;
  if (count > buff->buffer_size_) count = buff->buffer_size_;
  const blip_resampled_time_t time = (blip_resampled_time_t)count<<BLIP_BUFFER_ACCURACY;
  return (blip_time_t)((time-buff->offset_+buff->factor_-1)/buff->factor_);
}


//==========================================================================
//
//  blip_buffer_output_latency
//
//==========================================================================
int blip_buffer_output_latency (Blip_Buffer *buff) {
  return BLIP_WIDEST_IMPULSE_/2;
}


//==========================================================================
//
//  blip_buffer_count_samples
//
//==========================================================================
int32_t blip_buffer_count_samples (Blip_Buffer *buff, blip_time_t t) {
  if (!buff) return 0;
  blip_resampled_time_t rst = t*buff->factor_+buff->offset_;
  //uint32_t last_sample = resampled_time( t ) >> BLIP_BUFFER_ACCURACY;
  const uint32_t last_sample = rst>>BLIP_BUFFER_ACCURACY;
  const uint32_t first_sample = buff->offset_>>BLIP_BUFFER_ACCURACY;
  return (int32_t)(last_sample-first_sample);
}


//==========================================================================
//
//  blip_buffer_mix_samples
//
//==========================================================================
void blip_buffer_mix_samples (Blip_Buffer *buff, const blip_sample_t *buf, uint32_t count) {
  if (!buff) return;
  blip_buf_t *out = buff->buffer_+(buff->offset_>>BLIP_BUFFER_ACCURACY)+BLIP_WIDEST_IMPULSE_/2;
  const int sample_shift = BLIP_SAMPLE_BITS-16;
  int32_t prev = 0;
  while (count--) {
    int32_t s = ((int32_t)*buf++)<<sample_shift;
    *out += s-prev;
    prev = s;
    ++out;
  }
  *out -= prev;
}


//==========================================================================
//
//  blip_buffer_set_bass_freq
//
//==========================================================================
void blip_buffer_set_bass_freq (Blip_Buffer *buff, int freq) {
  if (!buff) return;
  int shift = 31;
  buff->bass_freq_ = freq;
  if (freq > 0) {
    shift = 13;
    int32_t f = (freq<<16)/buff->sample_rate_;
    while ((f >>= 1) && --shift) {}
  }
  buff->bass_shift = shift;
}


//==========================================================================
//
//  blip_buffer_end_frame
//
//==========================================================================
void blip_buffer_end_frame (Blip_Buffer *buff, blip_time_t t) {
  if (!buff) return;
  buff->offset_ += t*buff->factor_;
}


//==========================================================================
//
//  blip_buffer_remove_silence
//
//==========================================================================
inline void blip_buffer_remove_silence (Blip_Buffer *buff, uint32_t count) {
  if (!buff) return;
  const blip_resampled_time_t rst = (blip_resampled_time_t)count<<BLIP_BUFFER_ACCURACY;
  if (buff->offset_ >= rst) buff->offset_ -= rst; else buff->offset_ = 0;
}


//==========================================================================
//
//  blip_buffer_remove_samples
//
//==========================================================================
inline void blip_buffer_remove_samples (Blip_Buffer *buff, uint32_t count) {
  if (!buff) return;
  if (count) {
    blip_buffer_remove_silence(buff, count);
    /* copy remaining samples to beginning and clear old samples */
    const uint32_t remain = blip_buffer_samples_avail(buff)+BUFFER_EXTRA;
    /*
      k8: if the emulator doesn't consume the whole buffer, the bytes will accumulate
      k8: this will eventually lead to memory corruption
      k8: make sure that you discard unused frame samples, or something
     */
    memmove(buff->buffer_, buff->buffer_+count, remain*sizeof(blip_buf_t));
    memset(buff->buffer_+remain, 0, count*sizeof(blip_buf_t));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
//  Blip_Synth_Priv
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  blipbuf_priv_synth_init
//
//==========================================================================
static void blipbuf_priv_synth_init (Blip_Synth_Priv *synth_, blip_imp_t *p) {
  synth_->impulses = p;
  synth_->volume_unit_ = 0.0;
  synth_->kernel_unit = 0;
  synth_->buf = NULL;
  synth_->last_amp = 0;
  synth_->delta_factor = 0;
}

#ifdef PI
# undef PI
#endif
#define PI  (3.1415926535897932384626433832795029)


//==========================================================================
//
//  gen_sinc
//
//==========================================================================
static void gen_sinc (float *out, int count, double oversample, double treble, double cutoff) {
  if (cutoff > 0.999) cutoff = 0.999;
  if (treble < -300.0) treble = -300.0;
  if (treble > 5.0) treble = 5.0;

  const double maxh = 4096.0;
  const double rolloff = pow(10.0, 1.0/(maxh*20.0)*treble/(1.0-cutoff));
  const double pow_a_n = pow(rolloff, maxh-maxh*cutoff);
  const double to_angle = PI/2/maxh/oversample;

  for (int i = 0; i < count; ++i) {
    const double angle = ((i-count)*2+1)*to_angle;
    double c = rolloff*cos((maxh-1.0)*angle)-cos(maxh*angle);
    const double cos_nc_angle = cos(maxh*cutoff*angle);
    const double cos_nc1_angle = cos((maxh*cutoff-1.0)*angle);
    const double cos_angle = cos(angle);

    c = c*pow_a_n-rolloff*cos_nc1_angle+cos_nc_angle;
    const double d = 1.0+rolloff*(rolloff-cos_angle-cos_angle);
    const double b = 2.0-cos_angle-cos_angle;
    const double a = 1.0-cos_angle-cos_nc_angle+cos_nc1_angle;

    out[i] = (float)((a*d+c*b)/(b*d)); /* a / b + c / d */
  }
}


//==========================================================================
//
//  blip_eq_generate
//
//==========================================================================
static void blip_eq_generate (const blip_eq_t *eq, float *out, int count) {
  /* lower cutoff freq for narrow kernels with their wider transition band
     (8 points->1.49, 16 points->1.15) */
  double oversample = BLIP_RES*2.25/count+0.85;

  const double half_rate = eq->sample_rate*0.5;

  if (eq->cutoff_freq) oversample = half_rate/eq->cutoff_freq;

  const double cutoff = eq->rolloff_freq*oversample/half_rate;

  gen_sinc(out, count, BLIP_RES*oversample, eq->treble, cutoff);

  /* apply (half of) hamming window */
  const double to_fraction = PI/(count-1);
  for (int i = count; i--; /**/) out[i] *= 0.54-0.46*cos(i*to_fraction);
}


//==========================================================================
//
//  blipbuf_priv_synth_adjust_impulse
//
//==========================================================================
static void blipbuf_priv_synth_adjust_impulse (Blip_Synth_Priv *synth_) {
  /* sum pairs for each phase and add error correction to end of first half */
  const int size = blipbuf_priv_synth_impulses_size(synth_);

  for (int p = BLIP_RES; p-- >= BLIP_RES/2; /**/) {
    int error = synth_->kernel_unit;
    const int p2 = BLIP_RES-2-p;
    for (int i = 1; i < size; i += BLIP_RES) {
      error -= synth_->impulses[i+p];
      error -= synth_->impulses[i+p2];
    }

    if (p == p2) error /= 2; /* phase = 0.5 impulse uses same half for both sides */

    synth_->impulses[size-BLIP_RES+p] += error;
  }
}


//==========================================================================
//
//  blipbuf_priv_synth_treble_eq
//
//==========================================================================
static void blipbuf_priv_synth_treble_eq (Blip_Synth_Priv *synth_, const blip_eq_t *eq) {
  float fimpulse[BLIP_RES/2*(BLIP_WIDEST_IMPULSE_-1)+BLIP_RES*2];
  const int half_size = BLIP_RES/2*(BLIP_SYNTH_WIDTH-1);
  blip_eq_generate(eq, &fimpulse[BLIP_RES], half_size);

  /* need mirror slightly past center for calculation */
  for (int i = BLIP_RES; i--; /**/) {
    fimpulse[BLIP_RES+half_size+i] = fimpulse[BLIP_RES+half_size-1-i];
  }

  /* starts at 0 */
  for (int i = 0; i < BLIP_RES; ++i) fimpulse[i] = 0.0f;

  /* find rescale factor */
  double total = 0.0;
  for (int i = 0; i < half_size; ++i) total += fimpulse[BLIP_RES+i];

  /* double const base_unit = 44800.0-128*18;  allows treble up to +0 dB
     double const base_unit = 37888.0;  allows treble to +5 dB */
  const double base_unit = 32768.0; /* necessary for blip_unscaled to work */
  const double rescale = base_unit/2/total;
  synth_->kernel_unit = (int32_t)base_unit;

  /* integrate, first difference, rescale, convert to int */
  double sum = 0.0;
  double next = 0.0;
  const int impulses_size = blipbuf_priv_synth_impulses_size(synth_);

  for (int i = 0; i < impulses_size; ++i) {
    synth_->impulses[i] = (blip_imp_t)floor((next-sum)*rescale+0.5);
    sum += fimpulse[i];
    next += fimpulse[i+BLIP_RES];
  }

  blipbuf_priv_synth_adjust_impulse(synth_);

  /* volume might require rescaling */
  const double vol = synth_->volume_unit_;
  synth_->volume_unit_ = vol+1.0;
  blipbuf_priv_synth_volume_unit(synth_, vol);
}


//==========================================================================
//
//  blipbuf_priv_synth_volume_unit
//
//==========================================================================
static void blipbuf_priv_synth_volume_unit (Blip_Synth_Priv *synth_, double new_unit) {
  if (new_unit != synth_->volume_unit_) {
    /* use default eq if it hasn't been set yet */
    if (!synth_->kernel_unit) {
      blip_eq_t eq = { -8.0, 0, BLIP_DEFAULT_EQ_SAMPLE_RATE, 0 };
      //k8: fix sample rate, if we know it
      if (synth_->buf && synth_->buf->sample_rate_) {
        eq.sample_rate = synth_->buf->sample_rate_;
      }
      blipbuf_priv_synth_treble_eq(synth_, &eq);
    }

    synth_->volume_unit_ = new_unit;
    double factor = new_unit*(1L<<BLIP_SAMPLE_BITS)/synth_->kernel_unit;

    if (factor > 0.0) {
      int shift = 0;

      /* if unit is really small, might need to attenuate kernel */
      while (factor < 2.0) {
        ++shift;
        factor *= 2.0;
      }

      if (shift) {
        /* keep values positive to avoid round-towards-zero of sign-preserving
         * right shift for negative values */
        const int32_t offset = 0x8000+(1<<(shift-1));
        const int32_t offset2 = 0x8000>>shift;

        synth_->kernel_unit >>= shift;

        for (int i = blipbuf_priv_synth_impulses_size(synth_); i--; /**/) {
          synth_->impulses[i] = (blip_imp_t)(((synth_->impulses[i]+offset)>>shift)-offset2);
        }

        blipbuf_priv_synth_adjust_impulse(synth_);
      }
    }

    synth_->delta_factor = (int)floor(factor+0.5);
  }
}


//==========================================================================
//
//  blip_buffer_read_samples
//
//==========================================================================
uint32_t blip_buffer_read_samples (Blip_Buffer *buff, blip_sample_t *out,
                                   uint32_t max_samples, int write_mode)
{
  if (!buff) return 0;
  uint32_t count = blip_buffer_samples_avail(buff);
  if (count > max_samples) count = max_samples;

  if (!count) return 0;

  const int sample_shift = BLIP_SAMPLE_BITS-16;
  const int my_bass_shift = buff->bass_shift;

  blip_buf_t *in = buff->buffer_;
  int32_t accum = buff->reader_accum;

  // pasta, to avoid condition checking
  if (write_mode <= BLIP_READ_FAKE_STEREO) {
    for (uint32_t n = count; n--; out += 2) {
      const int32_t s = accum>>sample_shift;
      accum -= accum>>my_bass_shift;
      accum += *in++;
      /* clamp sample */
      if ((blip_sample_t)s == s) {
        out[0] = out[1] = (blip_sample_t)s;
      } else {
        out[0] = out[1] = (blip_sample_t)(0x7FFF-(s>>24));
      }
    }
  } else {
    const unsigned pincr = (write_mode ? 2 : 1);
    for (uint32_t n = count; n--; out += pincr) {
      const int32_t s = accum>>sample_shift;
      accum -= accum>>my_bass_shift;
      accum += *in++;
      /* clamp sample */
      *out = ((blip_sample_t)s == s ? (blip_sample_t)s : (blip_sample_t)(0x7FFF-(s>>24)));
    }
  }

  buff->reader_accum = accum;
  blip_buffer_remove_samples(buff, count);

  return count;
}


// ////////////////////////////////////////////////////////////////////////// //
/*
Blip_Buffer 0.4.0 Notes
-----------------------
Author : Shay Green <hotpop.com@blargg>
Website: http://www.slack.net/~ant/
Forum  : http://groups.google.com/group/blargg-sound-libs

Overview
--------
Blip_Buffer buffers samples at the current sampling rate until they are read
out. Blip_Synth adds waveforms into a Blip_Buffer, specified by amplitude
changes at times given in the source clock rate. To generate sound, setup one
or more Blip_Buffers and Blip_Synths, add sound waves, then read samples as
needed.

Waveform amplitude changes are specified to Blip_Synth in time units at the
source clock rate, relative to the beginning of the current time frame. When a
time frame is ended at time T, what was time T becomes time 0, and all samples
before that are made available for reading from the Blip_Buffer using
read_samples(). Time frames can be whatever length is convenient, and can
differ in length from one frame to the next. Also, samples don't need to be
read immediately after each time frame; unread samples accumulate in the buffer
until they're read (but also reduce the available free space for new
synthesis).

This sets up a Blip_Buffer at a 1MHz input clock rate, 44.1kHz output sample
rate:

  Blip_Buffer *buf = new_Blip_Buffer();
  blip_buffer_set_clock_rate(buf, 1000000);
  if (blip_buffer_set_sample_rate(buf, 44100) != 0) out_of_memory();

This sets up a Blip_Synth with good sound quality, an amplitude range of 20
(for a waveform that goes from -10 to 10), at 50% volume, outputting to buf:

  Blip_Synth *synth = new_Blip_Synth();
  // order matters!
  blip_synth_set_output(synth, buf);
  blip_synth_set_volume_amp_range(synth, 0.50, 20); // this also sets amplitude range


Treble and Bass Equalization
----------------------------
Treble and bass frequency equalization can be adjusted.

`blip_synth_set_treble_eq(treble_dB)` sets the treble level (in dB),
where 0.0 dB gives normal treble; -200.0 dB is quite muffled, and 5.0 dB
emphasizes treble for an extra crisp sound.

`blip_buffer_set_bass_freq(freq_hz)` sets the frequency where bass response
starts to diminish; 15 Hz is normal, 0 Hz gives maximum bass, and 15000 Hz
removes all bass.

Bass    Treble      Type
- - - - - - - - - - - - - - - - - - - - - - - -
1 Hz     0.0 dB     Flat equalization
1 Hz    +5.0 dB     Extra crisp sound
16 Hz   -8.0 dB     Default equalization
180 Hz  -8.0 dB     TV Speaker
2000 Hz -47.0 dB    Handheld game speaker

For example, to simulate a TV speaker, call:

  blip_buffer_set_bass_freq(buf, 180);
  blip_synth_set_treble_eq(buf, -8.0);

The best way to find parameters is to write a program which allows interactive
control over bass and treble.


Limitations
-----------
The length passed to `blip_buffer_set_sample_rate()` specifies the length of
the buffer in milliseconds. At the default time resolution, the resulting
buffer currently can't be more than about 65000 samples, which works out to
almost 1500 milliseconds at the common 44.1kHz sample rate. This is much more
than necessary for most uses.

The output sample rate should be around 44-48kHz for best results. Since
synthesis is band-limited, there's almost no reason to use a higher sample
rate.

The ratio of input clock rate to output sample rate has limited precision (at
the default time resolution, rounded to nearest 1/65536th), so you won't get
the exact sample rate you requested. However, it is *exact*, so whatever the
input/output ratio is internally rounded to, it will generate exactly that many
output samples for each second of input. For example if you set the clock rate
to 768000 Hz and the sample rate to 48000 Hz (a ratio it can represent
exactly), there will always be exactly one sample generated for every 16 clocks
of input.

For an example of rounding, setting a clock rate of 1000000Hz (1MHz) and sample
rate of 44100 Hz results in an actual sample rate of 44097.9 Hz, causing an
unnoticeable shift in frequency. If you're running 60 frames of sound per
second and expecting exactly 735 samples each frame (to keep synchronized with
graphics), your code will require some changes. This isn't a problem in
practice because the computer's sound output itself probably doesn't run at
*exactly* the claimed sample rate, and it's better to synchronize graphics with
sound rather than the other way around. Put another way, even if this library
could generate exactly 735 samples per frame, every frame, you would still have
audio problems (try generating a sine wave manually and see). Post to the forum
if you'd like to discuss this issue further.


Internal Operation
------------------
Understanding the basic internal operation might help in proper use of
Blip_Synth. There are two main aspects: what Blip_Synth does, and how samples
are stored internally to Blip_Buffer. A description of the core algorithm and
example code showing the essentials is available on the web:

  http://www.slack.net/~ant/bl-synth/

When adding a band-limited amplitude transition, the waveform differs based on
the relative position of the transition with respect to output samples. Adding
a transition between two samples requires a different waveform than one
centered on one sample. Blip_Synth stores the waveforms at several small
increments of differing alignment and selects the proper one based on the
source time.

Blip_Synth adds step waveforms, which start at zero and end up at the final
amplitude, extending infinitely into the future. This would be hard to handle
given a finite buffer size, so the sample buffer holds the *differences*
between each sample, rather than the absolute sample values. For example, the
waveform 0, 0, 1, 1, 1, 0, 0 is stored in a Blip_Buffer as 0, 0, +1, 0, 0, -1,
0. With this scheme, adding a change in amplitude at any position in the buffer
simply requires adding the proper values around that position; no other samples
in the buffer need to be modified. The extension of the final amplitude occurs
when reading samples out, by keeping a running sum of all samples up to
present.

The above should make it clearer how `blip_synth_offset()` gets its flexibility,
and that there is no penalty for making full use of this by adding amplitude
transitions in whatever order is convenient. `blip_synth_update()` simply keeps
track of the current amplitude and calls offset() with the change, so it's no
worse except being limited to a single waveform.


Thanks
------
Thanks to Jsr (FamiTracker author), the Mednafen team (multi-system emulator),
and ShizZie (Nhes GMB author) for using and giving feedback for the library.
Thanks to Disch for his interest and discussions about the synthesis algorithm
itself, and for writing his own implementation of it (Schpune). Thanks to
Xodnizel for Festalon, whose sound quality got me interested in video game
sound emulation in the first place, and where I first came up with the
algorithm while optimizing its brute-force filter.
*/
