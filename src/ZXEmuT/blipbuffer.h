/*
 * Band-limited sound synthesis and buffering
 *
 * Blip_Buffer 0.4.0
 *
 * Original C++ source:
 * Blip_Buffer 0.4.0. http://www.slack.net/~ant/
 *
 * Copyright (C) 2003-2006 Shay Green. This module is free software; you
 * can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version. This
 * module is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details. You should have received a copy of the GNU Lesser General
 * Public License along with this module; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * partially reimplemented in C by Gergely Szasz
 * some code cleanups and small fixes/optimisations by Ketmar // Invisible Vector
 *
 * There is a lot of ambiguous thing in the original C++ source, some of them `rectified'
 * (e.g. `int const blahblah = xxx` in the header translated to #define ), others not
 * (e.g. blip_sample_t/imp_t/short or Blip_Synth_ in Blip_Synth, or the whole
 * blip_eq_t struct ...).
 *
 * Functions originally implemented in the header are moved to .c.
 * Classes converted to structs, member functions prefixed with: blip_buff_
 * blipbuf_priv_synth_ and blip_synth_ resp.
 *
 * set_treble_eq() implemented with last argument as `double` instead
 * of `blip_eq_t` because this function always call just with treble value,
 * and C lack of `plain' type -> `struct' magic conversion/construction
 * capabilities.
 *
 * The source now C99.
 */
#ifndef BLIP_BUFFER_H
#define BLIP_BUFFER_H

#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
  Status code returned from some API functions.
  `0` means "success", negative means "error".
*/
typedef int blip_status_t;

/* For readability. */
typedef int blip_bool_t;

/* Time unit at source clock rate */
typedef uint32_t blip_time_t; /*k8: was signed; why?*/

/* Output samples are 16-bit signed, with a range of -32767 to 32767 */
typedef int16_t blip_sample_t;

/* Some functions returns error message on error. */
typedef const char *blargg_err_t;

typedef int32_t blip_buf_t;

typedef uint32_t blip_resampled_time_t;

typedef int16_t blip_imp_t;


#define BLIP_BUFFER_DEF_MSEC_LENGTH  (1000/4)
#define BLIP_BUFFER_DEF_ENTIRE_BUFF  (1)


typedef struct Blip_Buffer_s {
  uint32_t factor_;
  blip_resampled_time_t offset_;
  blip_buf_t *buffer_;
  int32_t buffer_size_;
  int32_t reader_accum;
  int bass_shift;
  int32_t sample_rate_;
  int32_t clock_rate_;
  int bass_freq_;
  int length_; /* (unchecked) length of the buffer, in milliseconds */
} Blip_Buffer;

/*
  Create new Blip Buffer. Returns `NULL` on error.
*/
Blip_Buffer *new_Blip_Buffer (void);

/*
  Delete Blip Buffer, set `buff` to `NULL`.
*/
void delete_Blip_Buffer (Blip_Buffer **buff);

/* Use this as `msec_length` value to get maximum possible buffer */
#define BLIP_MAX_LENGTH  (0)

/*
  Set output sample rate and buffer length in milliseconds (1/1000 sec, defaults
  to 1/4 second), then clear buffer. Returns 0 on success, otherwise if there
  isn't enough memory, returns negative value without affecting current buffer setup.
  Note that requested number of milliseconds can be clamped if Blip Buffer cannot
  hold enough samples internally. This is not an error, and the function will still
  return success.
*/
blip_status_t blip_buffer_set_sample_rate (Blip_Buffer *buff, uint32_t sample_rate,
                                           int msec_length);

/*
  Get sample rate set by `blip_buffer_set_sample_rate()`
*/
uint32_t blip_buffer_get_sample_rate (Blip_Buffer *buff);

/*
  Set number of source time units per second.
*/
void blip_buffer_set_clock_rate (Blip_Buffer *buff, uint32_t rate);

/*
  Get number of source time units per second.
*/
uint32_t blip_buffer_get_clock_rate (Blip_Buffer *buff);

/*
  End current time frame of specified duration and make its samples available
  (along with any still-unread samples) for reading with read_samples(). Begins
  a new time frame at the end of the current frame.
*/
void blip_buffer_end_frame (Blip_Buffer *buff, blip_time_t time);

/* Read modes for `blip_buffer_read_samples()` */
/* fill samples without holes; useful for mono sound */
#define BLIP_READ_MONO         (0)
/* fill each 2nd sample; useful to render one channel of stereo sound */
#define BLIP_READ_INTERLEAVE   (1)
/* fill 2 samples with the same value; useful to render mono sound in stereo buffer */
/* note that in this case the function will write `max_samples*2` samples! */
#define BLIP_READ_FAKE_STEREO  (-1) /* WARNING! this MUST be `-1`! */

/*
  Read at most 'max_samples' out of buffer into 'dest', removing them from from
  the buffer. Returns number of samples actually read and removed.
  See constants above for the possible `write_mode` values.
*/
uint32_t blip_buffer_read_samples (Blip_Buffer *buff, blip_sample_t *dest,
                                   uint32_t max_samples, int write_mode);


/* Additional optional features */

/*
  Set frequency high-pass filter frequency, where higher values reduce bass more
*/
void blip_buffer_set_bass_freq (Blip_Buffer *buff, int frequency);

/*
  Remove all available samples and clear buffer to silence. If 'entire_buffer' is
  false, just clears out any samples waiting rather than the entire buffer.
*/
void blip_buffer_clear (Blip_Buffer *buff, blip_bool_t entire_buffer);

/*
  Number of samples available for reading with read_samples().
*/
uint32_t blip_buffer_samples_avail (Blip_Buffer *buff);

/*
  Maximum number of samples allowed in buffer.
*/
uint32_t blip_buffer_samples_max (Blip_Buffer *buff);

/*
  Remove 'count' samples from those waiting to be read.
*/
void blip_buffer_remove_samples (Blip_Buffer *buff, uint32_t count);

/*
  Count number of clocks needed until 'count' samples will be available.
  If buffer can't even hold 'count' samples, returns number of clocks until
  buffer becomes full.
*/
blip_time_t blip_buffer_count_clocks (Blip_Buffer *buff, int32_t count);

/*
  Number of samples delay from synthesis to samples read out.
*/
int blip_buffer_output_latency (Blip_Buffer *buff);

/*
  Number of raw samples that can be mixed within frame of specified duration.
*/
int32_t blip_buffer_count_samples (Blip_Buffer *buff, blip_time_t t);

/*
  Mix 'count' samples from 'buf' into buffer. This can be used like this:

  // generate 1000 clocks of square wave
  int length = 1000;
  int amplitude = 1;
  for (int time = 0; time < length; time += 10) {
    blip_synth_update(synth, time, amplitude);
    amplitude = -amplitude;
  }

  // find out how many samples of sine wave to generate
  int count = blip_buffer_count_samples(length);
  blip_sample_t temp[4096];
  for (int i = 0; i < count; ++i) {
    double y = sin(i*(3.14159/100));
    temp[i] = y*0.30*32767; // convert to blip_sample_t's range
  }

  // mix sine wave's samples into Blip_Buffer
  blip_buffer_mix_samples(buf, temp, count);

  // end frame and show samples
  buf.end_frame(length);
*/
void blip_buffer_mix_samples (Blip_Buffer *buff, const blip_sample_t *buf, uint32_t count);


/* Experimental features */

void blip_buffer_remove_silence (Blip_Buffer *buff, uint32_t count);

blip_resampled_time_t blip_buffer_clock_rate_factor (Blip_Buffer *buff, uint32_t clock_rate);


/*
  Number of bits in resample ratio fraction. Higher values give a more accurate ratio
  but reduce maximum buffer size.
*/
#ifndef BLIP_BUFFER_ACCURACY
# define BLIP_BUFFER_ACCURACY  (16)
#endif

/*
  Number bits in phase offset. Fewer than 6 bits (64 phase offsets) results in
  noticeable broadband noise when synthesizing high frequency square waves.
  Affects size of Blip_Synth objects since they store the waveform directly.
*/
#ifndef BLIP_PHASE_BITS
# define BLIP_PHASE_BITS  (6)
#endif


/* This is private structure, do not touch it! */
typedef struct Blip_Synth_Priv_s {
  double volume_unit_;
  blip_imp_t *impulses;
  int32_t kernel_unit;

  Blip_Buffer *buf;
  int last_amp;
  int delta_factor;
} Blip_Synth_Priv;


/* Quality level. Start with blip_good_quality. */
#define BLIP_MED_QUALITY    (8)
#define BLIP_GOOD_QUALITY  (12)
#define BLIP_HIGH_QUALITY  (16)

/* Sadly, this cannot be changed at runtime yet. */
#ifndef BLIP_SYNTH_QUALITY
# define BLIP_SYNTH_QUALITY  BLIP_GOOD_QUALITY
#endif

/* This is Blip_Synth amplitude range: [-32767..32767] */
#define BLIP_SYNTH_RANGE  (65535)

/*
  Range specifies the greatest expected change in amplitude. Calculate it
  by finding the difference between the maximum and minimum expected
  amplitudes (max - min).
*/


typedef struct Blip_Synth_s {
  blip_imp_t *impulses;
  Blip_Synth_Priv impl;
} Blip_Synth;

/* Get/set Blip_Buffer used for output */
void blip_synth_set_output (Blip_Synth *synth, Blip_Buffer *b);

/* Return attached Blip_Buffer or NULL */
Blip_Buffer *blip_synth_get_output (Blip_Synth *synth);

/* It is better to call `blip_synth_set_output()` before calling any following API */

/* Set synth volume, [0..1] (but slight overamplification is allowed too). */
void blip_synth_set_volume (Blip_Synth *synth, double v);

/*
  Set synth volume, [0..1] (but slight overamplification is allowed too).
  This allows to set explicit amplitude range.
*/
void blip_synth_set_volume_amp_range (Blip_Synth *synth, double v, int amprange);

/* Configure low-pass filter (see notes.txt)*/
void blip_synth_set_treble_eq (Blip_Synth *synth, double treble);


/* Low-level interface */

/*
  Works directly in terms of fractional output samples.
*/
void blip_synth_offset_resampled (Blip_Synth *synth, blip_resampled_time_t time,
                                  int delta, Blip_Buffer *buff);

/*
  Add an amplitude transition of specified delta, optionally into specified buffer
  rather than the one set with output(). Delta can be positive or negative.
  The actual change in amplitude is `delta*(volume/range)`.
  I.e. this can be used to generate waveforms using delta values instead of absolute ones.
  Otherwise it is the same as `blip_synth_update()`.
  When used like this, Blip_Synth doesn't keep track of the waveform's current amplitude,
  so the offsets can be made in any order (and thus used to add multiple waveforms).
  Ugly inline for speed.
*/
static inline __attribute__((unused))
void blip_synth_offset (Blip_Synth *synth, blip_time_t time, int delta, Blip_Buffer *buff) {
  blip_synth_offset_resampled(synth,
                              time*synth->impl.buf->factor_+synth->impl.buf->offset_,
                              delta, synth->impl.buf);
}


/*
  Update amplitude of waveform at given time. Using this requires a separate
  Blip_Synth for each waveform.
  WARNING! make sure that you will pass a valid amplitude here (i.e. in range),
  otherwise the sound WILL degrade!
  Ugly inline for speed.
*/
static inline __attribute__((unused))
void blip_synth_update (Blip_Synth *synth, blip_time_t time, int amplitude) {
  const int delta = amplitude-synth->impl.last_amp;
  synth->impl.last_amp = amplitude;
  blip_synth_offset_resampled(synth,
                              time*synth->impl.buf->factor_+synth->impl.buf->offset_,
                              delta, synth->impl.buf);
}


Blip_Synth *new_Blip_Synth (void);

// reinit already created synth
void blip_synth_reinit (Blip_Synth *synth);

void delete_Blip_Synth (Blip_Synth **synth);


#ifdef __cplusplus
}
#endif
#endif
