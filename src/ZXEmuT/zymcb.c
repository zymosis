/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
// directly included from "main.c"

#define FNAME_PREFIX      "_zxemut_."
#define FNAME_ZX_MAX_LEN  (15)


typedef struct FileDirItem_s {
  char zxname[16]; // sanitized
  uint32_t fsize;
  //uint8_t flags; // currently, only bit 0 is defined, it means "read-only"
} FileDirItem;

static FileDirItem *fdirItems = NULL;
static uint32_t fdirItemsAlloted = 0;
static uint32_t fdirItemsCount = 0;


//==========================================================================
//
//  fopIsValidZXChar
//
//==========================================================================
static int fopIsValidZXChar (uint8_t byte) {
  if (!byte || byte >= 127) return 0;
  if (byte >= 'a' && byte <= 'z') return 1;
  if (byte >= 'A' && byte <= 'Z') return 1;
  if (byte >= '0' && byte <= '9') return 1;
  return !!strchr(" !@#$%.+-_", (char)byte);
}


//==========================================================================
//
//  fopIsValidZXName
//
//==========================================================================
static int fopIsValidZXName (const char *s) {
  if (!s || !s[0]) return 0;
  int len = 0;
  for (;;) {
    char ch = *s++;
    if (!ch) break;
    if (++len > FNAME_ZX_MAX_LEN) return 0;
    if (ch >= 'a' && ch <= 'z') continue;
    if (ch >= 'A' && ch <= 'Z') continue;
    if (ch >= '0' && ch <= '9') continue;
    if (!strchr(" !@#$%.+-_", ch)) return 0;
  }
  return 1;
}


//==========================================================================
//
//  fopRescanDir
//
//==========================================================================
static void fopRescanDir (void) {
  fdirItemsCount = 0;

  DIR *dir = opendir(".");
  if (!dir) return; // i don't care about errors for now

  const size_t pfxlen = strlen(FNAME_PREFIX);

  struct dirent *de;
  while ((de = readdir(dir)) != NULL) {
    size_t fnlen = strlen(de->d_name);
    if (fnlen < pfxlen || fnlen > pfxlen+FNAME_ZX_MAX_LEN) continue;
    if (!fopIsValidZXName(de->d_name+pfxlen)) continue;

    // looks like a valid file name
    if (fdirItemsCount == 0xffffU) break; // too many files

    struct stat st;
    if (stat(de->d_name, &st) != 0) continue; // oops
    if (st.st_size > 0x40000000U) continue; // file too big
    if (!S_ISREG(st.st_mode)) continue; // alas

    // record new file
    if (fdirItemsCount == fdirItemsAlloted) {
      uint32_t nsz = fdirItemsAlloted+4096;
      FileDirItem *ndi = realloc(fdirItems, sizeof(FileDirItem)*nsz);
      if (!ndi) break; // out of memory
      fdirItemsAlloted = nsz;
      fdirItems = ndi;
    }

    // guaranteed to fit
    strcpy(fdirItems[fdirItemsCount].zxname, de->d_name+pfxlen);
    fdirItems[fdirItemsCount].fsize = (uint32_t)st.st_size; // guaranteed to fit
    ++fdirItemsCount;
  }

  closedir(dir);
}


//==========================================================================
//
//  trapFileOps
//
//==========================================================================
static void trapFileOps (zym_cpu_t *z80, uint8_t tpc, uint16_t dataaddr, uint8_t datasize) {
  if (optFileTrapsMode == ZOPT_FTP_NONE) goto general_error;

  char diskname[128]; //FNAME_PREFIX+FNAME_ZX_MAX_LEN+4];

  // returns non-zero on success
  // builds disk file name into `diskname`
  int getFileName (uint16_t addr) {
    uint8_t ch = z80->mem_read(z80, addr, ZYM_MEMIO_OTHER);
    if (!fopIsValidZXChar(ch)) return 0;
    strcpy(diskname, FNAME_PREFIX);
    char *dest = diskname+strlen(FNAME_PREFIX);
    unsigned ofs = 0;
    for (;;) {
      if (ofs > FNAME_ZX_MAX_LEN) return 0;
      if ((uint32_t)addr+ofs > 0xffff) return 0;
      uint8_t ch = z80->mem_read(z80, (addr+ofs)&0xffff, ZYM_MEMIO_OTHER);
      if (!ch) break;
      if (!fopIsValidZXChar(ch)) return 0;
      dest[ofs++] = (char)ch;
    }
    dest[ofs] = 0;
    return 1;
  }


  // scan dir
  if (tpc == 0x10) {
    fopRescanDir();
    z80->hl.w = fdirItemsCount&0xffffU;
    z80->af.f &= ~ZYM_FLAG_C;
    return;
  }

  // get info
  if (tpc == 0x11) {
    const uint32_t fidx = z80->hl.w;
    if (fidx >= fdirItemsCount) goto general_error;
    if (z80->de.w < 0x4000) goto general_error; //FIXME: +3 can have RAM there!
    z80->af.f &= ~ZYM_FLAG_C;

    // 0: get file name
    if (z80->af.a == 0) {
      const char *zxname = fdirItems[fidx].zxname;
      unsigned f = 0;
      do {
        z80->mem_write(z80, (z80->de.w+f)&0xffff, (uint8_t)zxname[f], ZYM_MEMIO_OTHER);
      } while (zxname[f++] != 0);
      return;
    }

    // 1: get file name w/o extension
    if (z80->af.a == 1) {
      const char *zxname = fdirItems[fidx].zxname;
      const char *ee = strrchr(zxname, '.');
      if (!ee) ee = zxname+strlen(zxname);
      unsigned f = 0;
      while (zxname != ee) {
        z80->mem_write(z80, (z80->de.w+f)&0xffff, (uint8_t)zxname[f], ZYM_MEMIO_OTHER);
        ++f;
      }
      z80->mem_write(z80, (z80->de.w+f)&0xffff, 0, ZYM_MEMIO_OTHER);
      return;
    }

    // 2: get file extension
    if (z80->af.a == 2) {
      const char *zxname = fdirItems[fidx].zxname;
      const char *ee = strrchr(zxname, '.');
      if (!ee) {
        z80->mem_write(z80, z80->de.w, 0, ZYM_MEMIO_OTHER);
        return;
      }
      unsigned f = 0;
      do {
        z80->mem_write(z80, (z80->de.w+f)&0xffff, (uint8_t)ee[f], ZYM_MEMIO_OTHER);
      } while (ee[f++] != 0);
      return;
    }

    // 3: get file size
    if (z80->af.a == 3) {
      uint32_t sz = fdirItems[fidx].fsize;
      if (sz <= 0xffff) z80->af.f |= ZYM_FLAG_Z; else z80->af.f &= ~ZYM_FLAG_Z;
      for (unsigned f = 0; f < 4; ++f) {
        z80->mem_write(z80, (z80->de.w+f)&0xffff, sz&0xff, ZYM_MEMIO_OTHER);
        sz >>= 4;
      }
      return;
    }

    // 4: get file attributes
    if (z80->af.a == 4) {
      snprintf(diskname, sizeof(diskname), "%s%s", FNAME_PREFIX, fdirItems[fidx].zxname);
      if (access(diskname, R_OK) != 0) goto general_error;
      if (optFileTrapsMode == ZOPT_FTP_RW && access(diskname, W_OK) == 0) {
        z80->mem_write(z80, z80->de.w, 1, ZYM_MEMIO_OTHER);
      } else {
        z80->mem_write(z80, z80->de.w, 0, ZYM_MEMIO_OTHER);
      }
      return;
    }

    // unknown query
    goto general_error;
  }

  // read file, extended read file
  if (tpc == 0x12 || tpc == 0x13) {
    if (!getFileName(z80->hl.w)) goto general_error;
    if (z80->de.w < 0x4000) goto general_error; //FIXME: +3 can have RAM there!
    z80->af.f &= ~ZYM_FLAG_C;

    uint32_t rpos =
      (tpc == 0x12 ? 0 :
        z80->mem_read(z80, z80->de.w, ZYM_MEMIO_OTHER)|
        ((uint32_t)z80->mem_read(z80, (z80->de.w+1)&0xffff, ZYM_MEMIO_OTHER)<<8)|
        ((uint32_t)z80->mem_read(z80, (z80->de.w+2)&0xffff, ZYM_MEMIO_OTHER)<<16)|
        ((uint32_t)z80->mem_read(z80, (z80->de.w+3)&0xffff, ZYM_MEMIO_OTHER)<<24)
      );

    FILE *fl = fopen(diskname, "r");
    if (!fl) goto general_error;
    if (z80->bc.w == 0) { fclose(fl); return; }

    if (rpos && fseek(fl, (long)rpos, SEEK_SET) != 0) {
      fclose(fl);
      z80->bc.w = 0;
      return;
    }

    uint8_t *buf = malloc(z80->bc.w);
    if (!buf) { fclose(fl); goto general_error; } // out of memory

    ssize_t rd = fread(buf, 1, z80->bc.w, fl);
    fclose(fl);

    if (rd > 0) {
      for (unsigned f = 0; f < (unsigned)rd; ++f) {
        z80->mem_write(z80, (z80->de.w+f)&0xffff, buf[f], ZYM_MEMIO_OTHER);
      }
      z80->bc.w = (uint16_t)rd;
    } else {
      z80->bc.w = 0;
    }

    free(buf);
    return;
  }

  // create and write file
  if (tpc == 0x14) {
    fdirItemsCount = 0; // reset directory scan results
    if (optFileTrapsMode != ZOPT_FTP_RW) goto general_error;

    if (!getFileName(z80->hl.w)) goto general_error;
    //if (z80->de.w < 0x4000) goto general_error; //FIXME: +3 can have RAM there!
    z80->af.f &= ~ZYM_FLAG_C;

    FILE *fl = fopen(diskname, "w");
    if (!fl) goto general_error;
    if (z80->bc.w == 0) { fclose(fl); return; }

    uint16_t addr = z80->de.w;
    for (unsigned f = 0; f < z80->bc.w; ++f, ++addr) {
      uint8_t b = z80->mem_read(z80, addr, ZYM_MEMIO_OTHER);
      if (fwrite(&b, 1, 1, fl) != 1) {
        fclose(fl);
        goto general_error;
      }
    }

    fclose(fl);
    return;
  }

  // extended write file
  if (tpc == 0x15) {
    fdirItemsCount = 0; // reset directory scan results
    if (optFileTrapsMode != ZOPT_FTP_RW) goto general_error;

    if (!getFileName(z80->hl.w)) goto general_error;
    //if (z80->de.w < 0x4000) goto general_error; //FIXME: +3 can have RAM there!
    z80->af.f &= ~ZYM_FLAG_C;

    uint32_t rpos =
      (tpc == 0x12 ? 0 :
        z80->mem_read(z80, z80->de.w, ZYM_MEMIO_OTHER)|
        ((uint32_t)z80->mem_read(z80, (z80->de.w+1)&0xffff, ZYM_MEMIO_OTHER)<<8)|
        ((uint32_t)z80->mem_read(z80, (z80->de.w+2)&0xffff, ZYM_MEMIO_OTHER)<<16)|
        ((uint32_t)z80->mem_read(z80, (z80->de.w+3)&0xffff, ZYM_MEMIO_OTHER)<<24)
      );

    FILE *fl = fopen(diskname, "r+");
    if (!fl) goto general_error;
    if (z80->bc.w == 0) { fclose(fl); return; }

    if (rpos && fseek(fl, (long)rpos, SEEK_SET) != 0) {
      fclose(fl);
      goto general_error;
    }

    uint16_t addr = z80->de.w+4;
    for (unsigned f = 0; f < z80->bc.w; ++f, ++addr) {
      uint8_t b = z80->mem_read(z80, addr, ZYM_MEMIO_OTHER);
      if (fwrite(&b, 1, 1, fl) != 1) {
        fclose(fl);
        goto general_error;
      }
    }

    fclose(fl);
    return;
  }

  // delete file
  if (tpc == 0x16) {
    fdirItemsCount = 0; // reset directory scan results
    if (optFileTrapsMode != ZOPT_FTP_RW) goto general_error;

    if (!getFileName(z80->hl.w)) goto general_error;
    z80->af.f &= ~ZYM_FLAG_C;

    unlink(diskname);
    return;
  }

  // check if file exists and readable/writeable
  if (tpc == 0x17) {
    fdirItemsCount = 0; // reset directory scan results

    if (!getFileName(z80->hl.w)) goto general_error;
    z80->af.f &= ~ZYM_FLAG_C;

    if (access(diskname, R_OK) != 0) goto general_error;
    if (optFileTrapsMode == ZOPT_FTP_RW && access(diskname, W_OK) == 0) {
      z80->af.f = 1;
    } else {
      z80->af.f = 0;
    }
    return;
  }

  // extended read/write file
  if (tpc == 0x20) {
    if (z80->af.a > 2) goto general_error; // invalid operation

    fdirItemsCount = 0; // reset directory scan results
    if (z80->af.a != 0 && optFileTrapsMode != ZOPT_FTP_RW) goto general_error;

    if (!getFileName(z80->hl.w)) goto general_error;
    //if (z80->de.w < 0x4000) goto general_error; //FIXME: +3 can have RAM there!
    z80->af.f &= ~ZYM_FLAG_C;

    uint32_t rpos = (((uint32_t)z80->dex.w)<<16)|z80->hlx.w;

    FILE *fl;
    switch (z80->af.a) {
      case 0: fl = fopen(diskname, "r"); break; // read
      case 1: fl = fopen(diskname, "r+"); break; // write to existing
      case 2: // write, create if necessary
        fl = fopen(diskname, "r+");
        if (fl == NULL) fl = fopen(diskname, "w");
        break;
      default: goto general_error;
    }
    if (fl == NULL) goto general_error;
    if (z80->bc.w == 0) { fclose(fl); return; }

    if (rpos && fseek(fl, (long)rpos, SEEK_SET) != 0) {
      fclose(fl);
      goto general_error;
    }

    uint16_t addr = z80->de.w;

    uint8_t *buf = malloc(z80->bc.w);
    if (!buf) { fclose(fl); goto general_error; } // out of memory

    if (z80->af.a == 0) {
      // read
      ssize_t rd = fread(buf, 1, z80->bc.w, fl);
      if (rd != z80->bc.w && ferror(fl)) {
        free(buf);
        fclose(fl);
        goto general_error;
      }
      fclose(fl);

      for (unsigned f = 0; f < (unsigned)rd; ++f) {
        z80->mem_write(z80, (addr+f)&0xffff, buf[f], ZYM_MEMIO_OTHER);
      }

      z80->bc.w = (uint16_t)rd;
    } else {
      for (unsigned f = 0; f < (unsigned)z80->bc.w; ++f) {
        buf[f] = z80->mem_read(z80, (addr+f)&0xffff, ZYM_MEMIO_OTHER);
      }

      ssize_t wr = fwrite(buf, 1, z80->bc.w, fl);
      if (wr != z80->bc.w && ferror(fl)) {
        free(buf);
        fclose(fl);
        goto general_error;
      }
      fclose(fl);

      z80->bc.w = (uint16_t)wr;
    }

    free(buf);
    return;
  }

general_error:
  // general error
  z80->af.a = 1;
  z80->af.f |= ZYM_FLAG_C;
}


// ////////////////////////////////////////////////////////////////////////// //
static inline int64_t z80GetULACycle (int tstates) {
  if (tstates >= machineInfo.topleftpixts+zxLateTimings) {
    const int ts = tstates-(machineInfo.topleftpixts+zxLateTimings), line = ts/machineInfo.tsperline;
    if (line < machineInfo.vscreen) {
      const int ltp = ts%machineInfo.tsperline;
      if (ltp < machineInfo.hscreen) {
        int pos = ltp/4;
        return ((int64_t)line<<16)|((pos<<8)|(ltp&0x07));
      }
    }
  }
  return -1;
}


static inline int z80IsContendedAddr (uint16_t addr) {
  return
    (addr >= 0x4000 ?
      (!machineInfo.usePlus3Contention ?
        (zxMemoryBankNum[addr>>14]&0x01) != 0 : // normal
        (zxMemoryBankNum[addr>>14]&0x07) >= 4) : // +2A/+3
      (!machineInfo.usePlus3Contention ? 0 : zxLastPagedRAM0 >= 4));
  // +2A/+3: RAM pages 4, 5, 6 and 7 contended
}


// no contention on borders
static void z80Contention (zym_cpu_t *z80, uint16_t addr, zym_memio_t mio, zym_memreq_t mreq) {
  if (machineInfo.contention && z80->tstates < machineInfo.tsperframe) {
    // ULA snow effect
    if (optEmulateSnow && machineInfo.haveSnow && /*tstates == 4 &&*/
        mreq == ZYM_MREQ_READ && mio <= ZYM_MEMIO_OPCEXT &&
        (z80->regI>>6) == 1) {
      const int ots = z80->tstates+1;
      if (zxContentionTable[0][ots] == 4 || zxContentionTable[0][ots] == 5) {
        const int64_t lp = z80GetULACycle(ots);
        if (lp != -1) {
          const int pos = (lp>>8)&0xff/*, ltp = lp&0x0f*/, line = (lp>>16)&0xff;
          zxUlaSnow[line][pos] = (uint16_t)(zxScreenBank[((zxScrLineOfs[line]+pos+1)&0xff00)|z80->regR])+1;
          zxWasUlaSnow = 1;
        }
      }
    }
    if (z80IsContendedAddr(addr)) {
      z80->tstates += zxContentionTable[mreq == ZYM_MREQ_NONE][z80->tstates];
    }
  }
}


// FIXME: issue check should be here, i believe
static void z80PortContention (zym_cpu_t *z80, uint16_t port, int tstates, zym_portio_flags_t pflags) {
  if (machineInfo.iocontention && z80->tstates < machineInfo.tsperframe) {
    // don't care if this is input our output; check for it with (pflags&ZYM_PORTIO_FLAG_IN) if necessary
    if (pflags&ZYM_PORTIO_FLAG_EARLY) {
      // early, tstates is ALWAYS 1
      if (z80IsContendedAddr(port)) z80->tstates += zxContentionTable[1][z80->tstates]; // ULA bug
      ++z80->tstates;
    } else {
      // later, tstates is ALWAYS 2
      if ((port&0x01) == 0) {
        // ULA port
        z80->tstates += zxContentionTable[1][z80->tstates];
        z80->tstates += 2;
      } else {
        // non-ULA port
        if (z80IsContendedAddr(port)) {
          z80->tstates += zxContentionTable[1][z80->tstates]; ++z80->tstates;
          z80->tstates += zxContentionTable[1][z80->tstates]; ++z80->tstates;
          z80->tstates += zxContentionTable[1][z80->tstates];
        } else {
          z80->tstates += 2;
        }
      }
    }
  } else {
    z80->tstates += tstates;
  }
}


static uint8_t z80MemRead (zym_cpu_t *z80, uint16_t addr, zym_memio_t mio) {
  /*
  if (mio != ZYM_MEMIO_OTHER) {
    //fprintf(stderr, "M%c: %5d; addr=0x%04x; v=0x%02x\n", (mio == ZYM_MEMIO_OPCODE ? (z80->evenM1 ? 'c' : 'C') : 'R'), z80->tstates, addr, zxMemory[addr>>14][addr&0x3fff]);
    if (optEmulateSnow && machineInfo.haveSnow && (mio == ZYM_MEMIO_OPCODE || mio == ZYM_MEMIO_OPCEXT) && z80->regI >= 0x40 && z80->regI <= 0x7f) {
      // 'ULA snow effect'
      int64_t lp = z80GetULACycle(z80->tstates);
      //
      if (lp != -1) {
        int pos = (lp>>8)&0xff, ltp = lp&0x0f, line = (lp>>16)&0xff;
        //
        switch (ltp) {
          case 1: case 2: case 4: case 5:
          //default:
            zxUlaSnow[line][pos] = (uint16_t)(zxScreenBank[((zxScrLineOfs[line]+pos)&0xff00)|z80->regR])+1;
            zxWasUlaSnow = 1;
            //zxRealiseScreen(z80->tstates);
            break;
        }
      }
    }
  }
  */

  if (mio == ZYM_MEMIO_OPCODE) {
    if (addr >= 0x4000) zxWasRAMExec = 1; // for Mr.Gluck Reset Service
    // PAGING START
    if (optTRDOSenabled > 0 && optTRDOSROMIndex >= 0) {
      if (zxTRDOSPagedIn) {
        if (addr >= 0x4000) zxTRDOSpageout();
      } else {
        if (zxModel != ZX_MACHINE_48K) {
          // !48k
          if ((addr&0xff00) == 0x3d00 && zxLastPagedRAM0 < 0 && zxLastPagedROM == 1) zxTRDOSpagein();
        } else {
          // 48k
          if ((addr&0xfe00) == 0x3c00) zxTRDOSpagein();
        }
      }
      // TR-DOS traps
      if (optTRDOSTraps && zxTRDOSPagedIn) {
        #if 0
        // TR-DOS short track seek trap
        if (addr == 0x3dfd) {
          // check bytes
          if (memcmp(&zxMemory[0][addr], "\x3e\x50\x0e\xff\x0d\x20\xfd\x3d\x20\xf8\xc9", 11) == 0) {
            if (optTRDOSTrapDebug) cprintf("DBG: TR-DOS short track seek trap\n");
            z80->bc.c = 0;
            z80->af.a = 0;
            z80->af.f |= ZYM_FLAG_Z; // just in case
            z80->pc = 0x3e07; // "ret" address
            return 0xc9; // "ret"
          }
        }
        // TR-DOS long track seek trap
        if (addr == 0x3ea0) {
          // check bytes
          if (memcmp(&zxMemory[0][addr], "\x06\x03\x3e\xff\xcd\xff\x3d\x10\xf9\xc9", 10) == 0) {
            if (optTRDOSTrapDebug) cprintf("DBG: TR-DOS long track seek trap\n");
            z80->bc.w = 0;
            z80->af.a = 0;
            z80->af.f |= ZYM_FLAG_Z; // just in case
            z80->pc = 0x3ea9; // "ret" address
            return 0xc9; // "ret"
          }
        }
        // TR-DOS delay trap
        if (addr == 0x3dff) {
          // check bytes
          if (memcmp(&zxMemory[0][addr], "\x0e\xff\x0d\x20\xfd\x3d\x20\xf8\xc9", 9) == 0) {
            if (optTRDOSTrapDebug) cprintf("DBG: TR-DOS delay trap\n");
            z80->bc.c = 1;
            z80->af.a = 1;
            z80->af.f |= ZYM_FLAG_Z; // just in case
            z80->pc = 0x3e07; // "ret" address
            return 0xc9; // "ret"
          }
        }
        #else
        // TR-DOS delay trap
        if (addr == 0x3e01) {
          // check bytes
          if (memcmp(&zxMemory[0][addr], "\x0d\x20\xfd\x3d\x20\xf8\xc9", 7) == 0) {
            if (optTRDOSTrapDebug) cprintf("DBG: TR-DOS delay trap\n");
            z80->bc.c = 1;
            z80->af.a = 1;
          }
        } else if (addr == 0x3e04) {
          // check bytes
          if (memcmp(&zxMemory[0][0x3e01], "\x0d\x20\xfd\x3d\x20\xf8\xc9", 7) == 0) {
            if (optTRDOSTrapDebug) cprintf("DBG: TR-DOS short delay trap\n");
            z80->af.a = 1;
          }
        }
        #endif
      }
    }
    // PAGING END
    if (z80CheckBP(addr, DBG_BP_EXEC|DBG_BP_EXECONE)) z80->bp_hit = 1;
  } else if (mio == ZYM_MEMIO_DATA) {
    if (z80CheckBP(addr, DBG_BP_READ)) z80->bp_hit = 1;
  }

  // for +2A/+3 floating bus
  if (mio != ZYM_MEMIO_OTHER && !z80->bp_hit &&
      machineInfo.floatingBus > 1 && machineInfo.contention && z80->tstates < machineInfo.tsperframe)
  {
    //if (addr < 0x8000) fprintf(stderr, "RD: #%04X (%d); pg=%d; addr=#%04X\n", addr, z80IsContendedAddr(addr), zxMemoryBankNum[addr>>14], addr&0x3fff);
    if (z80IsContendedAddr(addr)) {
      zxLastContendedMemByte = zxMemory[addr>>14][addr&0x3fff];
      //fprintf(stderr, "+3CRD: #%04X; ts=%5d; lb=#%02X\n", addr, z80->tstates, zxLastContendedMemByte);
    }
  }

  return zxMemory[addr>>14][addr&0x3fff];
}


static void z80MemWrite (zym_cpu_t *z80, uint16_t addr, uint8_t value, zym_memio_t mio) {
  //if (mio != ZYM_MEMIO_OTHER) fprintf(stderr, "MW: %5d; addr=0x%04x; v=0x%02x\n", z80->tstates, addr, value);
  //
  if (addr >= 0x4000) {
    if ((addr&0x3fff) < 6912) {
      switch (zxMemoryBankNum[addr>>14]) {
        case 5: case 7:
          zxRealiseScreen(z80->tstates);
          break;
      }
    }
    zxMemory[addr>>14][addr&0x3fff] = value;
    if (mio != ZYM_MEMIO_OTHER) zxMemBanksDirty[zxMemoryBankNum[addr>>14]] = 1;
  } else {
    if (!zxTRDOSPagedIn && zxLastPagedRAM0 >= 0) {
      zxMemory[0][addr] = value;
      if (mio != ZYM_MEMIO_OTHER) zxMemBanksDirty[zxMemoryBankNum[0]] = 1;
    }
  }
  if (mio == ZYM_MEMIO_DATA) {
    if (z80CheckBP(addr, DBG_BP_WRITE)) z80->bp_hit = 1;
  }

  // for +2A/+3 floating bus
  if (mio != ZYM_MEMIO_OTHER && !z80->bp_hit &&
      machineInfo.floatingBus > 1 && machineInfo.contention && z80->tstates < machineInfo.tsperframe)
  {
    if (z80IsContendedAddr(addr)) {
      zxLastContendedMemByte = value;
      //fprintf(stderr, "+3CRW: #%04X; ts=%5d; lb=#%02X\n", addr, z80->tstates, zxLastContendedMemByte);
    }
  }
}


static uint8_t z80PortInInexistant (zym_cpu_t *z80, uint16_t port) {
  // floating bus emulation
  if (machineInfo.floatingBus) {
    if (machineInfo.floatingBus == 1) {
      // "normal" floating bus
      int64_t lp = z80GetULACycle(z80->tstates);
      if (lp != -1) {
        int pos = (lp>>8)&0xff, ltp = lp&0x0f, line = (lp>>16)&0xff;
        switch (ltp) {
          case 2: case 4: // bitmap
            return zxScreenBank[zxScrLineOfs[line]+pos];
          case 3: case 5: // attrs
            return zxScreenBank[6144+line/8*32+pos];
        }
      }
    } else {
      // +2A/+3 floating bus
      // it doesn't work in 48K mode, and for ports with the given pattern:
      //   00xx xxxx xxxx xx01
      if ((port&0xC003) == 1 && !zx7ffdLocked) {
        int64_t lp = z80GetULACycle(z80->tstates);
        //fprintf(stderr, "+3FB: #%04X; ts=%5d; lb=#%02X lp=%lld\n", port, z80->tstates, zxLastContendedMemByte, lp);
        if (lp != -1) {
          // bit 0 is always set
          int pos = (lp>>8)&0xff, ltp = lp&0x0f, line = (lp>>16)&0xff;
          //fprintf(stderr, "+3FB: #%04X; ltp=%d; line=%d; pos=%d\n", port, ltp, pos, line);
          switch (ltp) {
            case 2: case 4: // bitmap
              return zxScreenBank[zxScrLineOfs[line]+pos]|0x01u;
            case 3: case 1: // attrs
              return zxScreenBank[6144+line/8*32+pos]|0x01u;
          }
        }
        return zxLastContendedMemByte;
      }
    }
  }
  //
  return 0xff;
}


static uint8_t z80PortIn (zym_cpu_t *z80, uint16_t port, zym_portio_t pio) {
  //fprintf(stderr, "PI: %5d; port=#%04x\n", z80->tstates, port);
  if (pio == ZYM_PORTIO_NORMAL) {
    if (z80CheckBP(port, DBG_BP_PORTIN)) z80->bp_hit = 1;
  }
  if (zxRZX != NULL) {
    libspectrum_byte value;
    /*
    fprintf(stderr, " rzx trying to in: port=0x%04x\n", port);
    fprintf(stderr, "  rts=%u; ", (unsigned)libspectrum_rzx_tstates(zxRZX));
    fprintf(stderr, "  zts=%u; ", (unsigned)z80->tstates);
    fprintf(stderr, "  ic=%u\n", (unsigned)libspectrum_rzx_instructions(zxRZX));
    */
    if (libspectrum_rzx_playback(zxRZX, &value) != LIBSPECTRUM_ERROR_NONE) {
      cprintf("RZX playback failed!\n");
      conMessage("RZX playback failed!");
      lssnap_rzx_stop();
      lssnap_slt_clear();
    } else {
      //fprintf(stderr, " rzx in: port=0x%04x, value=0x%02x\n", port, value);
      return value;
    }
  }
  for (int f = phCount-1; f >= 0; --f) {
    if ((portHandlers[f].machine == ZX_MACHINE_MAX || portHandlers[f].machine == zxModel) &&
        (port&portHandlers[f].mask) == portHandlers[f].value && portHandlers[f].portInCB != NULL)
    {
      //fprintf(stderr, "PC=#%04X  port=#%04X\n", z80->pc, port);
      uint8_t res;
      if (portHandlers[f].portInCB(z80, port, &res)) return res;
    }
  }
  return z80PortInInexistant(z80, port);
}


static int phULAout (zym_cpu_t *z80, uint16_t port, uint8_t value);

static void z80PortOut (zym_cpu_t *z80, uint16_t port, uint8_t value, zym_portio_t pio) {
  //fprintf(stderr, "PW: %5d; port=0x%04x; v=0x%02x\n", z80->tstates, port, value);
  if (pio == ZYM_PORTIO_NORMAL) {
    if (z80CheckBP(port, DBG_BP_PORTOUT)) z80->bp_hit = 1;
  }
  int wasULA = 0;
  for (int f = phCount-1; f >= 0; --f) {
    if ((portHandlers[f].machine == ZX_MACHINE_MAX || portHandlers[f].machine == zxModel) &&
        (port&portHandlers[f].mask) == portHandlers[f].value && portHandlers[f].portOutCB != NULL) {
      if (portHandlers[f].portOutCB == phULAout) wasULA = 1;
      int res = (portHandlers[f].portOutCB(z80, port, value));
      // vanilla leaks to border select here, WFT?!
      if (res) break;
    }
  }
  if (!wasULA && !zxTRDOSPagedIn /*&& (zxModel == ZX_MACHINE_128K || zxModel == ZX_MACHINE_PLUS2 || zxModel == ZX_MACHINE_PENTAGON)*/) {
    if ((port&0x01) == 0) phULAout(z80, port, value);
  }
}


//==========================================================================
//
//  zxemutTrapPrintChar
//
//==========================================================================
static void zxemutTrapPrintChar (uint8_t ch) {
  if (ch == 13 || ch == 10) { cprintf("\n"); return; }
  if (ch < 32 || ch == 127) ch = '.';
  uint8_t s[2];
  s[0] = ch;
  s[1] = 0;
  cprintf("%s", (const char *)s);
}


//==========================================================================
//
//  zxemutTraps
//
//==========================================================================
static int zxemutTraps (zym_cpu_t *z80, uint8_t code, uint16_t dataaddr, uint8_t datasize) {
  switch (code) {
    case 0: // reset tick counter
      // check for subcommands
      if (datasize > 0) {
        //cprintf("PC=$%04X: EBAL: %llu (p=%d)\n", z80->pc, dbgTickCounter, dbtTickCounterPaused);
        const uint8_t sbc = z80->mem_read(z80, dataaddr, ZYM_MEMIO_OTHER);
        // data byte should be subtracted too
        const uint8_t subts = (datasize > 1 ? z80->mem_read(z80, dataaddr+1, ZYM_MEMIO_OTHER) : 0);
        // -8 to compensate trap cost
        switch (sbc) {
          case 1: dbtTickCounterPaused = 1; dbgTickCounter -= subts; break; // pause
          case 2: dbtTickCounterPaused = 0; dbgTickCounter -= subts; break; // resume
        }
      } else {
        // reset tick counter
        dbgTickCounter = 0;
        //cprintf("PC=$%02X; HUI: %llu (p=%d)\n", z80->pc, dbgTickCounter, dbtTickCounterPaused);
      }
      break;
    case 1: // show tick counter
      cprintf("PC=$%04X: ticks: %llu\n", /*dataaddr-5*/z80->pc, dbgTickCounter + z80->trap_ts);
      break;
    case 2: // activate debugger
      dbgSetActive(1);
      return 1; // exit NOW
    case 3: // console printing
      if (datasize > 0) {
        switch (z80->mem_read(z80, dataaddr, ZYM_MEMIO_OTHER)) {
          case 0: // print char in A
            zxemutTrapPrintChar(z80->af.a);
            break;
          case 1: // print string in HL, BC bytes
            for (unsigned f = 0; f < z80->bc.w; ++f) {
              zxemutTrapPrintChar(z80->mem_read(z80, z80->hl.w+f, ZYM_MEMIO_OTHER));
            }
            break;
          case 2: // print asciiz string in HL
            for (unsigned f = 0; f < 65536; ++f) {
              uint8_t b = z80->mem_read(z80, z80->hl.w+f, ZYM_MEMIO_OTHER);
              if (!b) break;
              zxemutTrapPrintChar(b);
            }
            break;
          case 3: // print trap data
            for (unsigned f = 1; f < datasize; ++f) {
              uint8_t b = z80->mem_read(z80, dataaddr+f, ZYM_MEMIO_OTHER);
              if (!b) break;
              zxemutTrapPrintChar(b);
            }
            break;
          case 4: // print hex byte from A
            cprintf("%02X", z80->af.a);
            break;
          case 5: // print hex word from HL
            cprintf("%02X", z80->hl.w);
            break;
          case 6: // print dec byte from A
            cprintf("%03u", z80->af.a);
            break;
          case 7: // print dec word from HL
            cprintf("%05u", z80->hl.w);
            break;
        }
      }
      break;
    case 0x10: // emulator control functions
      if (datasize > 0) {
        switch (z80->mem_read(z80, dataaddr, ZYM_MEMIO_OTHER)) {
          case 0x00: // pause the emulation
            emuSetPaused(1);
            break;
          case 0x01: // maxspeed on
            emuSetMaxSpeed(1);
            break;
          case 0x02: // maxspeed off
            emuSetMaxSpeed(0);
            break;
          case 0x60: // kmouse on, absolute
            optKMouse = 1;
            optKMouseAbsolute = 1;
            emuHideRealMouseCursor();
            emuSetKMouseAbsCoords();
            z80->af.a = 1;
            break;
          case 0x61: // kmouse normal
            optKMouseAbsolute = 0;
            break;
          case 0x62: // hide system mouse cursor
            emuHideRealMouseCursor();
            break;
          case 0x63: // show system mouse cursor
            emuShowRealMouseCursor();
            break;
        }
      }
      break;
    case 0x13: // file API
      if (datasize > 0) {
        trapFileOps(z80, z80->mem_read(z80, dataaddr, ZYM_MEMIO_OTHER),
                    dataaddr+1, datasize-1);
      } else {
        // general error
        z80->af.a = 1;
        z80->af.f |= ZYM_FLAG_C;
      }
      break;
    case 0xff: // check version
      z80->af.f &= ~ZYM_FLAG_Z;
      z80->af.a = 0x01;
      switch (optFileTrapsMode) {
        case ZOPT_FTP_RO: z80->af.a |= 0x02; break;
        case ZOPT_FTP_RW: z80->af.a |= 0x02|0x04; break;
      }
      // emulator ID
      z80->bc.w = 0x585a;
      z80->de.w = 0x6d45;
      z80->hl.w = 0x5475;
      // emulator version
      z80->afx.a = 0;
      break;
  }
  return 0;
}


// return !0 to exit immediately
// called when invalid ED command found
// PC points to the next instruction
// trapCode=0xFB:
//   .SLT trap
//    HL: address to load;
//    A: A --> level number
//    return: CARRY complemented --> error
static zym_bool z80_SLT_Trap (zym_cpu_t *z80, uint8_t trapCode) {
  if (zxSLT != NULL) {
    libspectrum_byte *slt = libspectrum_snap_slt(zxSLT, z80->af.a);
    if (slt != NULL) {
      size_t len = libspectrum_snap_slt_length(zxSLT, z80->af.a);
      uint16_t addr = z80->hl.w;
      while (len-- > 0) {
        z80->mem_write(z80, addr, *slt++, ZYM_MEMIO_OTHER);
        addr = (addr+1)&0xffff;
      }
      return 0;
    }
  }
  z80->af.f ^= ZYM_FLAG_C; // complement carry to indicate that there was an error
  return 0;
}


// ZXEmuT-specific trap
static zym_bool z80_ZXEmuT_Trap (zym_cpu_t *z80, uint8_t trapCode) {
  if (optAllowZXEmuTraps && z80->mem_read(z80, (z80->pc)&0xffff, ZYM_MEMIO_OTHER) == 0x18) {
    uint8_t opcsize = z80->mem_read(z80, (z80->pc+1)&0xffff, ZYM_MEMIO_OTHER);
    if (opcsize > 0 && opcsize <= 0x7f) {
      // 0xed, 0xfe, 0x18 (JR), and then # of data bytes (hehe, JR will jump over 'em)
      const uint16_t addr = z80->pc+3; // data address
      z80->pc += opcsize+2;
      if (zxemutTraps(z80, z80->mem_read(z80, addr-1, ZYM_MEMIO_OTHER), addr, opcsize-1)) {
        return 1; // exit NOW (used to activate the debugger)
      }
    }
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
// kempstons (joystick & mouse)
static int iclamp (int val, int vmin, int vmax) {
  return (val < vmin ? vmin : val > vmax ? vmax : val);
}

static int phKempstonIn (zym_cpu_t *z80, uint16_t port, uint8_t *res) {
  //fprintf(stderr, "KEPMSTON: PC=#%04X  port=#%04X\n", z80->pc, port);
  // kempston mouse
  // #00DF check for some Spanish games
  // ((port&0xffu) == 0xdfu && (port&0xff00u) != 0)
  // kmouse:
  //  0xfadf: buttons (hiword bits 0,2 are zero)
  //  0xfbdf: buttons (hiword bit 2 is zero)
  //  0xffdf: buttons (not hiword bits are zero)
  if (optKMouse && (port&0xffu) == 0xdfu &&
      (optKMouseStrict ? (port&0xfa00u) == 0xfa00u : (port&0xf000u) == 0xf000u))
  {
    //fprintf(stderr, "  kmouse!\n");
    // 0xfadf, buttons
    if ((port|0xfa00) == 0xfadf) {
      // bit 0: left; bit 1: right; bit 2: middle; pressed if bit reset
      const uint8_t mbt =
        (zxKMouseButtons&MS_BUTTON_LEFT ? (optKMouseSwapButtons ? 0x02 : 0x01) : 0)|
        (zxKMouseButtons&MS_BUTTON_RIGHT ? (optKMouseSwapButtons ? 0x01 : 0x02) : 0)|
        (zxKMouseButtons&MS_BUTTON_MIDDLE ? 0x04 : 0);
      *res = (mbt^0x0f)|((zxKMouseWheel<<4)&0xf0);
      return 1;
    }
    // 0xfbdf, x
    if ((port|0xfa00u) == 0xfbdfu) {
      *res = (optKMouseAbsolute ? iclamp(kmouseAbsX, 0, 255) : zxKMouseDX);
      return 1;
    }
    // 0xffdf, y
    if ((port|0xfa00u) == 0xffdfu) {
      *res = (optKMouseAbsolute ? iclamp(kmouseAbsY, 0, 191) : zxKMouseDY);
      return 1;
    }
  }
  //
  if (optKJoystick) {
    // kempston joystick
    int af = isAutofireKempston();
    *res = ((~zxKeyboardState[8])&0x1f)|af;
    return 1;
  }
  //
  return 0;
}


static const PortHandlers phKempston[] = {
  // joystick
  { ZX_MACHINE_MAX, 0x0020, 0x0000, phKempstonIn, NULL },
  // mouse
  /*
  { ZX_MACHINE_PENTAGON, 0x0521, 0x0501, phKempstonIn, NULL },
  { ZX_MACHINE_PENTAGON, 0x0521, 0x0101, phKempstonIn, NULL },
  { ZX_MACHINE_PENTAGON, 0x0121, 0x0001, phKempstonIn, NULL },
  */
  { 0, 0, 0, NULL, NULL }};


////////////////////////////////////////////////////////////////////////////////
#define FUSE_RD(name_) \
static int emu_##name_ (zym_cpu_t *z80, uint16_t port, uint8_t *res) { \
  if (zxTRDOSPagedIn /*&& zxDiskIf && difGetHW(zxDiskIf) == DIF_BDI*/) { \
    *res = name_(port); \
    diskLastActivity = timerGetMS(); \
    return 1; \
  } \
  return 0; \
}


#define FUSE_WR(name_) \
static int emu_##name_ (zym_cpu_t *z80, uint16_t port, uint8_t value) { \
  if (zxTRDOSPagedIn /*&& zxDiskIf && difGetHW(zxDiskIf) == DIF_BDI*/) { \
    name_(port, value); \
    diskLastActivity = timerGetMS(); \
    return 1; \
  } \
  return 0; \
}

FUSE_RD(beta_sr_read)
FUSE_RD(beta_tr_read)
FUSE_RD(beta_sec_read)
FUSE_RD(beta_dr_read)
FUSE_RD(beta_sp_read)

FUSE_WR(beta_cr_write)
FUSE_WR(beta_tr_write)
FUSE_WR(beta_sec_write)
FUSE_WR(beta_dr_write)
FUSE_WR(beta_sp_write)


static const PortHandlers phTRDOS[] = {
  { ZX_MACHINE_MAX, 0x00ff, 0x001f, emu_beta_sr_read, emu_beta_cr_write },
  { ZX_MACHINE_MAX, 0x00ff, 0x003f, emu_beta_tr_read, emu_beta_tr_write },
  { ZX_MACHINE_MAX, 0x00ff, 0x005f, emu_beta_sec_read, emu_beta_sec_write },
  { ZX_MACHINE_MAX, 0x00ff, 0x007f, emu_beta_dr_read, emu_beta_dr_write },
  { ZX_MACHINE_MAX, 0x00ff, 0x00ff, emu_beta_sp_read, emu_beta_sp_write },
  { 0, 0, 0, NULL, NULL }
};


////////////////////////////////////////////////////////////////////////////////
// uPD765 ports
static int phUPD765_status (zym_cpu_t *z80, uint16_t port, uint8_t *res) {
  if (!upd765_fdc) return 0;
  diskLastActivity = timerGetMS();
  *res = upd_fdc_read_status(upd765_fdc);
  return 1;
}


static int phUPD765_read (zym_cpu_t *z80, uint16_t port, uint8_t *res) {
  if (!upd765_fdc) return 0;
  diskLastActivity = timerGetMS();
  *res = upd_fdc_read_data(upd765_fdc);
  return 1;
}


static int phUPD765_write (zym_cpu_t *z80, uint16_t port, uint8_t value) {
  if (!upd765_fdc) return 0;
  diskLastActivity = timerGetMS();
  upd_fdc_write_data(upd765_fdc, value);
  return 1;
}


static int phUPD765_dummy (zym_cpu_t *z80, uint16_t port, uint8_t value) {
  return 0;
}


// allow uPD765 for any machine
static const PortHandlers phUPD765[] = {
  { ZX_MACHINE_MAX, 0xf002, 0x3000, phUPD765_read, phUPD765_write }, // fdc r/w
  { ZX_MACHINE_MAX, 0xf002, 0x2000, phUPD765_status, phUPD765_dummy }, // status
  { 0, 0, 0, NULL, NULL }
};


////////////////////////////////////////////////////////////////////////////////
// 128k memory mode port
static int ph7FFDout (zym_cpu_t *z80, uint16_t port, uint8_t value) {
  if (!zx7ffdLocked) {
    //fprintf(stderr, "O: 7FFD; port=#%04X; value=#%02X\n", port, value);
    zxLastOut7ffd = value;
    emuRealizeMemoryPaging();
    return 1;
  }
  //
  return 0;
}


static int ph7FFDoutP1 (zym_cpu_t *z80, uint16_t port, uint8_t value) {
  if (zxPentagonMemory > 512) return 0;
  return ph7FFDout(z80, port, value);
}


static int ph7FFDoutP2 (zym_cpu_t *z80, uint16_t port, uint8_t value) {
  if (zxPentagonMemory < 1024) return 0;
  return ph7FFDout(z80, port, value);
}


static int ph7FFDin (zym_cpu_t *z80, uint16_t port, uint8_t *res) {
  if (!zx7ffdLocked) {
    //128k bug with 0x7ffd
    //fprintf(stderr, "I: 7FFD; port=#%04X\n", port);
    *res = z80PortInInexistant(z80, port);
    // ignore +2A/+3
    if (machineInfo.port7ffDbug) ph7FFDout(z80, port, *res);
    return 1;
  }
  return 0;
}


// +3/Scorpion/Pentagon512/Pentagon1024 memory mode port
// for Pentagon1024, this is actually #EFFD port (A3, A12)
static int ph1FFDout (zym_cpu_t *z80, uint16_t port, uint8_t value) {
  if (!zx7ffdLocked && machineInfo.port1ffd >= 0) {
    //fprintf(stderr, "O: 1FFD; port=#%04X; value=#%02X\n", port, value);
    zxLastOut1ffd = value;
    emuRealizeMemoryPaging();
    // uPD motors
    if (upd765_fdc) {
      fdd_motoron(upd765_fdc->drive[0], (value&0x08));
      fdd_motoron(upd765_fdc->drive[1], (value&0x08));
    }
    return 1;
  }
  return 0;
}


static int ph7FFDoutBad (zym_cpu_t *z80, uint16_t port, uint8_t value) {
  if (optFDas7FFD && !zx7ffdLocked) {
    //fprintf(stderr, "O: 7FFD; port=#%04X; value=#%02X\n", port, value);
    zxLastOut7ffd = value;
    emuRealizeMemoryPaging();
    return 1;
  }
  return 0;
}


static const PortHandlers phMemory[] = {
  // 128
  { ZX_MACHINE_128K, 0x8002, 0x0000, ph7FFDin, ph7FFDout },
  { ZX_MACHINE_PLUS2, 0x8002, 0x0000, ph7FFDin, ph7FFDout },
  // +2A
  { ZX_MACHINE_PLUS2A, 0x8002, 0x0000, ph7FFDin, ph7FFDout },
  { ZX_MACHINE_PLUS2A, 0xf002, 0x1000, NULL, ph1FFDout },
  // +3
  { ZX_MACHINE_PLUS3, 0x8002, 0x0000, ph7FFDin, ph7FFDout },
  { ZX_MACHINE_PLUS3, 0xf002, 0x1000, NULL, ph1FFDout },
  // scorpion
  { ZX_MACHINE_SCORPION, 0xc002, 0x4000, NULL, ph7FFDout },
  { ZX_MACHINE_SCORPION, 0xf002, 0x1000, NULL, ph1FFDout },
  // pentagon
  { ZX_MACHINE_PENTAGON, 0x8002, 0x0000, NULL, ph7FFDoutP1 },
  // pentagon1024
  { ZX_MACHINE_PENTAGON, 0xc002, 0x4000, NULL, ph7FFDoutP2 },
  { ZX_MACHINE_PENTAGON, 0xf008, 0xe000, NULL, ph1FFDout },
  // for optFDas7FFD
  { ZX_MACHINE_MAX, 0x8002, 0x0000, NULL, ph7FFDoutBad },
  { 0, 0, 0, NULL, NULL }};


////////////////////////////////////////////////////////////////////////////////
// AY ports
static int phAYinR (zym_cpu_t *z80, uint16_t port, uint8_t *res) {
  if (optAYEnabled) {
    // register port
    uint8_t val = zxAYRegs[zxLastOutFFFD&0x0f], val7 = zxAYRegs[7];
    //fprintf(stderr, "I: AYR; port=#%04X\n", port);
         if (zxLastOutFFFD == 14) *res = (val7&0x40 ? 0xbf&val : 0xbf);
    else if (zxLastOutFFFD == 15 && !(val7&0x80)) *res = 0xff;
    else *res = val;
    return 1;
  }
  return 0;
}


static int phAYoutR (zym_cpu_t *z80, uint16_t port, uint8_t value) {
  if (optAYEnabled) {
    // register port
    //fprintf(stderr, "O: AYR; port=#%04X\n", port);
    zxLastOutFFFD = (value&0x0f);
    return 1;
  }
  return 0;
}


static int phAYoutD (zym_cpu_t *z80, uint16_t port, uint8_t value) {
  if (optAYEnabled) {
    //fprintf(stderr, "O: AYD; port=#%04X\n", port);
    switch (zxLastOutFFFD) {
      case 1: case 3: case 5: case 13: value &= 0x0f; break;
      case 6: case 8: case 9: case 10: value &= 0x1f; break;
    }
    zxAYRegs[zxLastOutFFFD] = value;
    soundAYWrite(zxLastOutFFFD, value, z80->tstates);
  }
  return 0;
}


static const PortHandlers phAY[] = {
  { ZX_MACHINE_MAX, 0xc002, 0xc000, phAYinR, phAYoutR },
  { ZX_MACHINE_MAX, 0xc002, 0x8000, NULL, phAYoutD },
  // full: {0xffff, 0xfffd}, {0xffff, 0xbffd}
  { 0, 0, 0, NULL, NULL }};


////////////////////////////////////////////////////////////////////////////////
// ULA ports
static int phULAin (zym_cpu_t *z80, uint16_t port, uint8_t *res) {
  uint8_t r = zxUlaOut, p1 = (port>>8);
  uint8_t ks[8];
  int done;
  int detectlde = 1;
  int af = isAutofireKeyboard();
  //
  for (int f = 0; f < 8; ++f) ks[f] = zxKeyboardState[f];
  if (af) ks[af>>8] &= ~(af&0xff);
  // emulate keyboard matrix effect
  if (optEmulateMatrix) {
    do {
      done = 1;
      for (int k = 0; k < 7; ++k) {
        for (int j = k+1; j < 8; ++j) {
          if ((ks[k]|ks[j]) != 0xff && ks[k] != ks[j]) {
            ks[k] = ks[j] = (ks[k]&ks[j]);
            done = 0;
          }
        }
      }
    } while (!done);
  }
  //
  for (int f = 0; f < 8; ++f) if ((p1&(1<<f)) == 0) r &= ks[f];
  // check for known tape loaders
  if (optFlashLoad &&
      !zxTRDOSPagedIn &&
      zxCurTape != NULL && !tapNeedRewind &&
      // oooh...
      (zxModel == ZX_MACHINE_PLUS2A || zxModel == ZX_MACHINE_PLUS3 ? zxLastPagedROM == 3 : (zxModel == ZX_MACHINE_48K || zxLastPagedROM == 1)))
  {
    if (emuTapeFlashLoad() == 0) {
      detectlde = 0;
      return r;
    }
  }
  //
  if (optTapeDetector && detectlde) {
    emuTapeLoaderDetector();
  }
  switch (emuTapeGetCurEdge()) {
    case 0: r &= ~0x40; break;
    case 1: r |= 0x40; break;
  }
  //FIXME
  /*if (optTapePlaying && optTapeSound) soundBeeper((r>>2)&0x10, z80->tstates);*/
  //
  *res = r;
  return 1;
}


static int phULAout (zym_cpu_t *z80, uint16_t port, uint8_t value) {
  uint8_t bc = (value&0x07);
  //
  if (optBrightBorder > 0) bc |= ((value>>optBrightBorder)&0x01)<<3;
  if (zxBorder != bc) {
    // issue check for BorderTrix (FIXME: dumb hack!)
    zxRealiseScreen(z80->tstates - (optZXIssue ? 1 : 0));
    zxBorder = bc;
  }
  /*if (!optTapePlaying || !optTapeSound)*/ soundBeeper(/*value&0x10*/((value&0x10)>>3), z80->tstates);
  //
  if (zxModel == ZX_MACHINE_48K) {
    zxUlaOut =
      (optZXIssue == 0 ?
        (value&0x18 ? 0xff : 0xbf) : //issue2
        (value&0x10 ? 0xff : 0xbf)); //issue3, 128k
  } else {
    //128k
    zxUlaOut = (value&0x10 ? 0xff : 0xbf);
    //zxUlaOut = 0xbf; //+3
  }
  //
  return 1;
}


static const PortHandlers phULA[] = {
  { ZX_MACHINE_MAX, 0x0001, 0x0000, phULAin, phULAout },
  { 0, 0, 0, NULL, NULL }};


////////////////////////////////////////////////////////////////////////////////
// REMEMBER, THAT HIGHER PRIORITY DEVICES SHOULD BE ADDED LAST!
// THIS IS SO FOR PORT MASK/VALUES TOO!
static void emuAddPortHandlers (void) {
  phAdd(phULA);
  phAdd(phMemory);
  phAdd(phKempston);
  phAdd(phAY);
  phAdd(phTRDOS);
  phAdd(phUPD765);
}
