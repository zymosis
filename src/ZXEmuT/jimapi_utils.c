/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#define ABBREV_CI              (0)
#define ABBREV_CASE_SENSITIVE  (1)


////////////////////////////////////////////////////////////////////////////////
static const char *parseAbbrevArray (const char *str, const char *array[], int casesens, int *fullCommand) {
  const char *found = NULL;
  if (fullCommand) *fullCommand = 0;
  if (str != NULL && str[0] && (unsigned)(str[0]&0xffu) > 32) {
    int l0 = (int)strlen(str);
    /* remove trailing spaces */
    while (l0 && (unsigned)(str[l0-1]&0xffu) <= 32) --l0;
    if (!l0) return NULL;
    const char *s;
    for (unsigned f = 0; (s = array[f]) != NULL; ++f) {
      const int l1 = (int)strlen(s);
      if (l0 <= l1) {
        const int cmp = (casesens ? strncmp(s, str, l0) : strncasecmp(s, str, l0));
        if (cmp == 0) {
          if (l0 == l1) {
            if (fullCommand) *fullCommand = 1;
            return s;
          }
          if (found == NULL) {
            found = s;
          } else {
            found = NULL;
            break;
          }
        }
      }
    }
  }
  return found;
}


static __attribute__ ((sentinel)) const char *parseAbbrev (const char *str, ...) {
  const char *xarray[128];
  size_t xapos = 0;
  const char *s;
  va_list va;
  va_start(va, str);
  while ((s = va_arg(va, const char *)) != NULL) {
    if (xapos+2 < 128) xarray[xapos++] = s;
    va_end(va);
  }
  xarray[xapos] = NULL;
  return parseAbbrevArray(str, xarray, ABBREV_CI, NULL);
}


////////////////////////////////////////////////////////////////////////////////
/*
 * ���������� �� ������ ������, ������� ��������
 * ���� � ����� 0: �����, ������, �������, ����, ������, ����������; ���������� �� ��������
 * ���� � ����� 1: �������, ���������� ���� 1, ������� � ����� ������
 * ���� � ����� �����: �������� �������� ������� �������, ����������; ������ �������� ��� ��������� �������
 * ��������� �� ������ NULL, � ��� ���� free()
 * returned string must be `free()`d
 */
static char *completeAbbrevArray (const char *str, const char *array[], int casesens) {
  int showdivider = 1;
  if (str != NULL && str[0]) {
    const char *found = NULL, *s;
    int foundlen = 0;
    int pfxcount = 0;
    int slen = (int)strlen(str);
    char *res;
    // ������ ������: ������� ��������, ���������� ����� ������� ��������
    for (unsigned f = 0; (s = array[f]) != NULL; ++f) {
      const int l0 = (int)strlen(s);
      if (slen <= l0 && (casesens ? strncmp : strncasecmp)(s, str, slen) == 0) {
        //fprintf(stderr, "slen=%d; l0=%d; s=<%s>\n", slen, l0, s);
        // �����
        if (l0 > foundlen) {
          found = s;
          foundlen = l0;
        }
        ++pfxcount;
      }
    }
    if (!pfxcount) return strdup(str); // �� ����� ������ ������, ����� ������, ������!
    if (pfxcount == 1) {
      // ����� ����, �������, ��� � ���Σ�
      char *sres = malloc(strlen(found)+2);
      strcpy(sres, found);
      strcat(sres, " ");
      return sres;
    }
    // ����� ������, ��� ����������
    // ���� ����� ������� �������, ������ �������� �ӣ, ��� �����
    //fprintf(stderr, "found=<%s>\n", found);
    res = strdup(found); // ������� �� ������, � ���������� ���
    if (showdivider) cprintf("\2======\n");
    for (unsigned f = 0; (s = array[f]) != NULL; ++f) {
      const int l0 = (int)strlen(s);
      if (slen <= l0 && (casesens ? strncmp : strncasecmp)(s, str, slen) == 0) {
        // ����, �������� � ������ �������
        char *t = res;
        cprintf("\1%s\n", s);
        if (casesens) {
          while (*t && *s && *t == *s) { ++t; ++s; }
        } else {
          while (*t && *s && tolower(*t) == tolower(*s)) { ++t; ++s; }
        }
        *t = '\0'; // �� ������, ��� � �������
      }
    }
    // ���, ���������� ��������
    return res;
  } else {
    // ���� ���������� ��� ���������
    const char *s;
    for (int f = 0; (s = array[f]) != NULL; ++f) {
      if (showdivider) { cprintf("\2======\n"); showdivider = 0; }
      cprintf("\1%s\n", s);
    }
    return strdup("");
  }
}


static __attribute__ ((sentinel)) char *completeAbbrev (const char *str, ...) {
  const char *xarray[128];
  size_t xapos = 0;
  const char *s;
  va_list va;
  va_start(va, str);
  while ((s = va_arg(va, const char *)) != NULL) {
    if (xapos+2 < 128) xarray[xapos++] = s;
    va_end(va);
  }
  xarray[xapos] = NULL;
  return completeAbbrevArray(str, xarray, ABBREV_CI);
}


static int isFullyCompleted (const char *s) {
  if (!s || !s[0]) return 0;
  const size_t slen = strlen(s);
  return (s[slen-1] == ' ');
}


////////////////////////////////////////////////////////////////////////////////
//TODO: parse path
static char *completeFilesWithExts (const char *path, const char *extarray[]) {
  int asz = 0, acnt = 0;
  char **array = NULL;
  DIR *d;
  struct dirent *de;
  char *res;

  char *dirpath = strdup(path ? path : "");
  char *edp = strrchr(dirpath, '/');
  if (!edp) dirpath[0] = 0; else *edp = 0;
  if (!dirpath[0]) { free(dirpath); dirpath = strdup("."); }
  if (path && path[0] == '/' && dirpath[0] == '.' && !dirpath[1]) dirpath[0] = '/';

  d = opendir(dirpath);
  if (!d) { free(dirpath); return strdup(path != NULL ? path : ""); } // no such dir? hehe

  const int skip2chars = (dirpath[0] == '.' && !dirpath[1] && (!path || path[0] != '.' || path[1] != '/'));

  const char *namepfx = "";
  if (path) {
    const char *ep = strrchr(path, '/');
    if (ep) namepfx = ep+1;
  }
  const size_t pfxlen = strlen(namepfx);

  char *fullname = NULL;
  while ((de = readdir(d)) != NULL) {
    if (strlen(de->d_name) < pfxlen) continue;
    //fprintf(stderr, "000: (%s): de->d_name:<%s>; dir=<%s>(%d); pfx=<%s>(%u)\n", path, de->d_name, dirpath, skip2chars, namepfx, (unsigned)pfxlen);
    struct stat st;
    if (pfxlen > 0 && strncasecmp(de->d_name, namepfx, pfxlen) != 0) continue; // bad prefix
    //fprintf(stderr, "001: (%s): de->d_name:<%s>; dir=<%s>(%d); pfx=<%s>(%u)\n", path, de->d_name, dirpath, skip2chars, namepfx, (unsigned)pfxlen);
    // build full name
    if (fullname) free(fullname);
    if (dirpath[0] == '/' && !dirpath[1]) {
      fullname = strprintf("/%s", de->d_name);
    } else {
      fullname = (skip2chars ? strdup(de->d_name) : strprintf("%s/%s", dirpath, de->d_name));
    }
    if (stat(fullname, &st) != 0) continue;
    //fprintf(stderr, "002: (%s): de->d_name:<%s>; dir=<%s>(%d); fullname=<%s>\n", path, de->d_name, dirpath, skip2chars, fullname);
    if (S_ISDIR(st.st_mode)) {
      if (pfxlen == 0) continue;
      char *ts = strprintf("%s/", fullname);
      free(fullname);
      fullname = ts;
    } else {
      if (!S_ISREG(st.st_mode) && !S_ISLNK(st.st_mode)) continue;
      if (extarray != NULL) {
        int ok = 0;
        for (int f = 0; extarray[f] != NULL && !ok; ++f) if (isExtEqu(fullname, extarray[f])) ok = 1;
        if (!ok) continue;
      }
    }
    //fprintf(stderr, "003: (%s): de->d_name:<%s>; dir=<%s>(%d); fullname=<%s>\n", path, de->d_name, dirpath, skip2chars, fullname);
    if (acnt >= asz) {
      int newsz = asz+1025;
      char **newa = realloc(array, sizeof(char *)*newsz);
      if (newa == NULL) break;
      array = newa;
      asz = newsz-1;
    }
    array[acnt++] = strdup(fullname);
  }
  if (fullname) free(fullname);
  closedir(d);

  if (acnt == 0) return strdup(path != NULL ? path : ""); // no files here
  array[acnt] = NULL; // there is always room for one more item
  qsort(array, acnt, sizeof(char *), lambda(int, (const void *i0, const void *i1) {
    //fprintf(stderr, "cmp: [%s] [%s] %d\n", *(const char **)i0, *(const char **)i1, strcoll(*(const char **)i0, *(const char **)i1));
    return strcoll(*(const char **)i0, *(const char **)i1);
  }));
  //qsort(array, acnt, sizeof(char *), strcoll);
  res = completeAbbrevArray(path, (const char **)array, 1);
  for (int f = acnt-1; f >= 0; --f) free(array[f]);
  if (array != NULL) free(array);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
static char *completeLabelName (const char *pfx) {
  int asz = 0, acnt = 0;
  char **array = NULL;
  char *res;

  // count
  void *it = dbgFirstLabelByPrefix(pfx);
  while (it) {
    ++asz;
    it = dbgNextLabelByPrefix(it, pfx);
  }
  dbgIteratorFree(it);
  if (asz == 0) return strdup(pfx ? pfx : "");

  // create array
  array = malloc(sizeof(char *)*(asz+1));
  it = dbgFirstLabelByPrefix(pfx);
  while (it) {
    array[acnt++] = strdup(dbgIteratorLabelName(it));
    it = dbgNextLabelByPrefix(it, pfx);
  }
  dbgIteratorFree(it);

  array[acnt] = NULL; // there is always room for one more item
  qsort(array, acnt, sizeof(char *), lambda(int, (const void *i0, const void *i1) {
    //fprintf(stderr, "cmp: [%s] [%s] %d\n", *(const char **)i0, *(const char **)i1, strcoll(*(const char **)i0, *(const char **)i1));
    return strcoll(*(const char **)i0, *(const char **)i1);
  }));
  //qsort(array, acnt, sizeof(char *), strcoll);

  res = completeAbbrevArray(pfx, (const char **)array, 0);
  for (int f = acnt-1; f >= 0; --f) free(array[f]);
  if (array != NULL) free(array);
  return res;
}


static const char *parseLabelName (const char *pfx) {
  if (!pfx || !pfx[0] || (unsigned)(pfx[0]&0xff) <= 32) return NULL;

  int asz = 0, acnt = 0;
  char **array = NULL;

  // count
  void *it = dbgFirstLabelByPrefix(pfx);
  while (it) {
    ++asz;
    it = dbgNextLabelByPrefix(it, pfx);
  }
  dbgIteratorFree(it);
  if (asz == 0) return strdup(pfx ? pfx : "");

  // create array
  array = malloc(sizeof(char *)*(asz+1));
  it = dbgFirstLabelByPrefix(pfx);
  while (it) {
    array[acnt++] = strdup(dbgIteratorLabelName(it));
    it = dbgNextLabelByPrefix(it, pfx);
  }
  dbgIteratorFree(it);
  array[acnt] = NULL; // there is always room for one more item

  const char *res = parseAbbrevArray(pfx, (const char **)array, 0, NULL);
  for (int f = acnt-1; f >= 0; --f) free(array[f]);
  if (array != NULL) free(array);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
typedef struct FloppyFileInfo_t {
  char name[9]; // 0-terminated
  char ext[5]; // three chars for TR-DOS (always starts with "$" for HoBeta)
  uint32_t size; // max 65535 for TR-DOS
  uint16_t index;
} FloppyFileInfo;


////////////////////////////////////////////////////////////////////////////////
static char normFloppyNameChar (char ch) {
  unsigned char n = (unsigned char)ch;
  if (n == '\\') n = '/';
  if (n < 33 || n > 126) n = '_';
  return (char)n;
}


static void normFloppyNameStr (char *s) {
  if (!s) return;
  for (; *s; ++s) *s = normFloppyNameChar(*s);
}


static void trdn2str (const TRFile *fi, char *name, char *ext) {
  int ne;
  for (ne = 8; ne > 0; --ne) if (!isspace(fi->name[ne-1])) break;
  if (ne) {
    memcpy(name, fi->name, (size_t)ne);
    name[ne] = 0;
    normFloppyNameStr(name);
  } else {
    name[0] = 0;
  }
  ext[0] = '.';
  ext[1] = '$';
  if (!isspace(fi->ext)) {
    ext[2] = normFloppyNameChar((char)fi->ext);
    ext[3] = 0;
  } else {
    ext[2] = 0;
  }
}


// return # of files
// disk type must be TR-DOS (ensured by a caller)
// may return `NULL` (and `*outcount` is always zero in this case)
static FloppyFileInfo *trdiskBuildDir (disk_t *flp, int *outcount) {
  TRFile flist[FLP_TRDOS_DIRCOUNT_MAX];
  FloppyFileInfo *array = calloc(FLP_TRDOS_DIRCOUNT_MAX, sizeof(FloppyFileInfo));
  int fcount = flpGetDirectoryTRD(flp, flist);
  int count = 0;
  for (int f = 0; f < fcount; ++f) {
    if (flist[f].name[0] == 0) break; // no more
    trdn2str(&flist[f], array[count].name, array[count].ext);
    array[count].index = (unsigned)f;
    array[count].size = flist[f].slen*256;
    ++count;
  }
  *outcount = count;
  if (!count) { free(array); array = NULL; }
  return array;
}


////////////////////////////////////////////////////////////////////////////////
// return # of files
// disk type must be +3DOS (ensured by a caller)
// may return `NULL` (and `*outcount` is always zero in this case)
static FloppyFileInfo *p3diskBuildDir (disk_t *flp, int *outcount) {
  P3DiskInfo p3d;
  p3d.flp = flp;
  if (p3dskDetectGeom(p3d.flp, &p3d.geom) != FLPERR_OK) {
    *outcount = 0;
    return NULL;
  }
  FloppyFileInfo *array = calloc(p3d.geom.maxdirentries, sizeof(FloppyFileInfo));
  P3DskFileInfo nfo;
  if (p3dskFindInit(&p3d, &nfo, "*.*") != FLPERR_OK) {
    free(array);
    *outcount = 0;
    return NULL;
  }
  uint32_t fidx = 0;
  int count = 0;
  while (p3dskFindNext(&nfo) > 0) {
    if (nfo.nameonly[0] || nfo.extonly[0]) {
      strcpy(array[count].name, nfo.nameonly);
      size_t nlen = strlen(array[count].name);
      while (nlen > 0 && array[count].name[nlen-1] == ' ') {
        --nlen;
        array[count].name[nlen] = 0;
      }
      strcpy(array[count].ext+1, nfo.extonly);
      array[count].ext[0] = '.';
      nlen = strlen(array[count].ext);
      while (nlen > 0 && array[count].ext[nlen-1] == ' ') {
        --nlen;
        array[count].ext[nlen] = 0;
      }
      normFloppyNameStr(array[count].name);
      array[count].size = nfo.size;
      array[count].index = fidx;
      ++count;
    }
    ++fidx;
  }
  *outcount = count;
  if (!count) { free(array); array = NULL; }
  return array;
}


////////////////////////////////////////////////////////////////////////////////
static FloppyFileInfo *anyDiskBuildDir (disk_t *flp, int *outcount) {
  if (!flp) { *outcount = 0; return NULL; }
  const FloppyDiskType dtype = flpDetectDiskType(flp);
  if (dtype == FLP_DISK_TYPE_TRDOS) return trdiskBuildDir(flp, outcount);
  if (dtype == FLP_DISK_TYPE_P3DOS) return p3diskBuildDir(flp, outcount);
  *outcount = 0;
  return NULL;
}


////////////////////////////////////////////////////////////////////////////////
//TODO: rename duplicates
static char *completeTRDiskFiles (const char *name, disk_t *flp) {
  int count;
  FloppyFileInfo *dir = anyDiskBuildDir(flp, &count);
  if (count == 0) return strdup(name ? name : "");

  char **array = malloc(sizeof(char *)*(count+1));
  for (int f = 0; f < count; ++f) array[f] = strprintf("%s%s", dir[f].name, dir[f].ext);
  array[count] = NULL; // there is always room for one more item
  free(dir); // we don't need it anymore

  qsort(array, count, sizeof(char *), lambda(int, (const void *i0, const void *i1) {
    //fprintf(stderr, "cmp: [%s] [%s] %d\n", *(const char **)i0, *(const char **)i1, strcoll(*(const char **)i0, *(const char **)i1));
    return strcoll(*(const char **)i0, *(const char **)i1);
  }));

  char *res = completeAbbrevArray(name, (const char **)array, ABBREV_CI);
  for (int f = 0; f < count; ++f) free(array[f]);
  free(array);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
static int jimapiFromConsole (Jim_Interp *interp) {
  Jim_Obj *acv = Jim_GetGlobalVariableStr(interp, "con::executing", JIM_NONE);
  int acflag = 0;
  if (acv && Jim_GetBoolean(interp, acv, &acflag) != JIM_OK) acflag = 0;
  return (acflag != 0);
}


static int jimapiWantsCompletion (Jim_Interp *interp, int argc, Jim_Obj *const *argv) {
  Jim_Obj *acv = Jim_GetGlobalVariableStr(interp, "con::autocompletion", JIM_NONE);
  int acflag = 0;
  if (acv && Jim_GetBoolean(interp, acv, &acflag) != JIM_OK) acflag = 0;
  return (acflag != 0);
}


static int jimapiDisableCompletion (Jim_Interp *interp, int argc, Jim_Obj *const *argv) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_SetResultString(interp, "", 0);
    return 1;
  }
  return 0;
  /*
  if (argc > 1 && strcmp(Jim_String(argv[1]), " ?ac") == 0) {
    Jim_SetResultString(interp, "", 0);
    return 1;
  }
  return 0;
  */
}


static inline char dig2hex (char dig) {
  if ((dig = (dig&0x0f)+'0') > '9') dig += 'a'-'9'-1;
  return dig;
}


static void jimapiStrAppendCStr (Jim_Interp *interp, Jim_Obj *stro, const char *str, int slen) {
  if (str != NULL && str[0] && slen != 0) {
    int needq = (str[0] == '\''), newlen;
    //
    if (slen < 0) slen = (int)strlen(str);
    newlen = slen;
    for (int f = 0; f < slen; ++f) {
      switch (str[f]) {
        case '\a': case '\b': case '\f': case '\n':
        case '\r': case '\t': case '\v': case 27: // '\e'
        case '\\': case '"': case 127: // '\~'
          needq = 1;
          newlen += 1;
          break;
        case ' ': needq = 1; break;
        default:
          if (str[f] < 32) {
            needq = 1;
            newlen += 3;
          }
          break;
      }
    }
    if (needq) {
      // need to quote this
      char *newstr = calloc(1, newlen+3), *p = newstr;
      //
      *p++ = '"';
      for (int f = 0; f < slen; ++f) {
        switch (str[f]) {
          case '\a': *p++ = '\\'; *p++ = 'a'; break;
          case '\b': *p++ = '\\'; *p++ = 'b'; break;
          case '\f': *p++ = '\\'; *p++ = 'f'; break;
          case '\n': *p++ = '\\'; *p++ = 'n'; break;
          case '\r': *p++ = '\\'; *p++ = 'r'; break;
          case '\t': *p++ = '\\'; *p++ = 't'; break;
          case '\v': *p++ = '\\'; *p++ = 'v'; break;
          case 27: *p++ = '\\'; *p++ = 'e'; break;
          case 127: *p++ = '\\'; *p++ = '~'; break;
          case '\\': case '"': *p++ = '\\'; *p++ = str[f]; break;
          default:
            if (str[f] < 32) {
              *p++ = '\\';
              *p++ = 'x';
              *p++ = dig2hex(str[f]>>4);
              *p++ = dig2hex(str[f]);
            } else {
              *p++ = str[f];
            }
            break;
        }
      }
      *p++ = '"';
      if (newlen+2 != p-newstr) abort();
      Jim_AppendString(interp, stro, newstr, newlen+2);
      free(newstr);
    } else {
      // just do it
      Jim_AppendString(interp, stro, str, slen);
    }
  }
}


static void jimapiStrAppendObj (Jim_Interp *interp, Jim_Obj *str, Jim_Obj *appobj) {
  if (str != NULL && appobj != NULL) jimapiStrAppendCStr(interp, str, Jim_String(appobj), -1);
}


static void jimapiStrAppendSpace (Jim_Interp *interp, Jim_Obj *str) {
  const char *ss = Jim_String(str);
  const int needSpace = (ss && ss[0] && (unsigned)(ss[strlen(ss)-1]&0xffu) > 32);
  if (needSpace) Jim_AppendString(interp, str, " ", -1);
}


static void jimapiStrAppendArgs (Jim_Interp *interp, Jim_Obj *str, int from, int argc, Jim_Obj *const *argv, int addspaceifemtpy) {
  const char *ss = Jim_String(str);
  int needSpace = (ss && ss[0] && (unsigned)(ss[strlen(ss)-1]&0xffu) > 32);
  if (from < argc) {
    for (int f = from; f < argc; ++f) {
      if (needSpace) Jim_AppendString(interp, str, " ", -1); else needSpace = 1;
      jimapiStrAppendObj(interp, str, argv[f]);
    }
  } else if (addspaceifemtpy && needSpace) {
    Jim_AppendString(interp, str, " ", -1);
  }
}

#define jimapiAppendRestArgs(res_,starg_) jimapiStrAppendArgs(interp, (res_), (starg_), argc, argv, 0)


static void jimapiStrAppendAbbrev (Jim_Interp *interp, Jim_Obj *str, const char *abr) {
  const char *ss = Jim_String(str);
  int needSpace = (ss && ss[0] && (unsigned)(ss[strlen(ss)-1]&0xffu) > 32);
  if (needSpace) Jim_AppendString(interp, str, " ", -1);
  if (!abr || !abr[0] || ((unsigned)(abr[0]&0xffu) <= 32 && !abr[1])) return;
  int alen = (int)strlen(abr);
  if ((unsigned)(abr[alen-1]&0xffu) <= 32) {
    jimapiStrAppendCStr(interp, str, abr, alen-1);
    Jim_AppendString(interp, str, " ", -1);
  } else {
    jimapiStrAppendCStr(interp, str, abr, alen);
  }
}


////////////////////////////////////////////////////////////////////////////////
static __attribute__((format(printf,2,3))) void jim_SetResStrf (Jim_Interp *interp, const char *fmt, ...) {
  va_list va;
  char *str;
  //
  va_start(va, fmt);
  str = strprintfVA(fmt, va);
  va_end(va);
  //
  Jim_SetResultString(interp, str, -1);
  free(str);
}


////////////////////////////////////////////////////////////////////////////////
// : invalid
#define  JIM_INVALID_INTVAL  (-2147483648)

static int strTryHex (const char *s, int allowpgaddr) {
  if (!s || !s[0]) return JIM_INVALID_INTVAL;
  size_t pos;
  int suffix = 0;
       if (s[0] == '#' || s[0] == '$') pos = 1;
  else if (s[0] == '0' && (s[1] == 'x' || s[1] == 'X')) pos = 2;
  else if (s[0] == '&' && (s[1] == 'h' || s[1] == 'H')) pos = 2;
  else {
    size_t slen = strlen(s);
    if (slen >= 2 && (s[slen-1] == 'h' || s[slen-1] == 'H')) { suffix = 1; pos = 0; }
    else return JIM_INVALID_INTVAL;
  }
  if (digitInBase(s[pos], 16) < 0) return JIM_INVALID_INTVAL;
  uint32_t addr = 0;
  int dcount = 0;
  for (;;) {
    if (s[pos] == '_') { ++pos; continue; }
    const int d = digitInBase(s[pos], 16);
    if (d < 0) break;
    ++dcount;
    /*if (addr <= 65535)*/ addr = addr*16U+(unsigned)d;
    ++pos;
  }
  if (suffix && (s[pos] == 'h' || s[pos] == 'H')) ++s;
  if (!s[pos]) {
    //if (addr < 0 || addr > 65535) return JIM_INVALID_INTVAL;
    if (!allowpgaddr && addr > 65535) return JIM_INVALID_INTVAL;
    if (dcount > 4) {
      // with a page
      uint32_t pg = addr>>16;
      addr &= 0xffff;
      if (addr > 0x3fff) return JIM_INVALID_INTVAL;
      if (pg >= emuGetRAMPages()) return JIM_INVALID_INTVAL;
      addr |= (pg+4)<<14;
    }
    return (int)addr;
  }
  return JIM_INVALID_INTVAL;
}


static void *strFindZ80RegPtr (const char *s, int *regbytes) {
  int tmp = 0;
  if (!regbytes) regbytes = &tmp;
  *regbytes = 2;
  if (!s || !s[0]) return NULL;
  // 16-bit registers
  if (strEquCI(s, "pc") || strEquCI(s, "$")) return &z80.pc;
  if (strEquCI(s, "sp")) return &z80.sp.w;
  if (strEquCI(s, "bc")) return &z80.bc.w;
  if (strEquCI(s, "de")) return &z80.de.w;
  if (strEquCI(s, "hl")) return &z80.hl.w;
  if (strEquCI(s, "bc'") || strEquCI(s, "bcx")) return &z80.bcx.w;
  if (strEquCI(s, "de'") || strEquCI(s, "dex")) return &z80.dex.w;
  if (strEquCI(s, "hl'") || strEquCI(s, "hlx")) return &z80.hlx.w;
  if (strEquCI(s, "ix")) return &z80.ix.w;
  if (strEquCI(s, "iy")) return &z80.iy.w;
  if (strEquCI(s, "memptr")) return &z80.memptr.w;
  // 8-bit registers
  *regbytes = 1;
  if (strEquCI(s, "ixl")) return &z80.ix.l;
  if (strEquCI(s, "iyl")) return &z80.iy.l;
  if (strEquCI(s, "ixh")) return &z80.ix.h;
  if (strEquCI(s, "iyh")) return &z80.iy.h;
  if (strEquCI(s, "b")) return &z80.bc.b;
  if (strEquCI(s, "c")) return &z80.bc.c;
  if (strEquCI(s, "d")) return &z80.de.d;
  if (strEquCI(s, "e")) return &z80.de.e;
  if (strEquCI(s, "h")) return &z80.hl.h;
  if (strEquCI(s, "l")) return &z80.hl.l;
  if (strEquCI(s, "a")) return &z80.af.a;
  if (strEquCI(s, "f")) return &z80.af.f;
  if (strEquCI(s, "bx") || strEquCI(s, "b'")) return &z80.bcx.b;
  if (strEquCI(s, "cx") || strEquCI(s, "c'")) return &z80.bcx.c;
  if (strEquCI(s, "dx") || strEquCI(s, "d'")) return &z80.dex.d;
  if (strEquCI(s, "ex") || strEquCI(s, "e'")) return &z80.dex.e;
  if (strEquCI(s, "hx") || strEquCI(s, "h'")) return &z80.hlx.h;
  if (strEquCI(s, "lx") || strEquCI(s, "l'")) return &z80.hlx.l;
  if (strEquCI(s, "ax") || strEquCI(s, "a'")) return &z80.afx.a;
  if (strEquCI(s, "fx") || strEquCI(s, "f'")) return &z80.afx.f;
  return NULL;
}


static int strTryZ80Reg (const char *s) {
  int bytes;
  void *regptr = strFindZ80RegPtr(s, &bytes);
  if (!regptr) return JIM_INVALID_INTVAL;
  if (bytes == 1) return *((uint8_t *)regptr);
  if (bytes == 2) return *((uint16_t *)regptr);
  return JIM_INVALID_INTVAL;
}


int jimGetIntValEx (Jim_Interp *interp, Jim_Obj *obj, int allowpgaddr) {
  if (!interp || !obj) return JIM_INVALID_INTVAL;
  long addr = -1;
  const char *lname = Jim_String(obj);
  /* try Z80 registers */
  addr = strTryZ80Reg(lname);
  if (addr != JIM_INVALID_INTVAL) return (int)addr;
  /* try address */
  if (Jim_GetLong(interp, obj, &addr) == JIM_OK) {
    if (addr < -32768 || addr > 65535) return JIM_INVALID_INTVAL;
    return (int)addr;
  }
  /* try label name or #nnnn */
  addr = strTryHex(lname, allowpgaddr);
  if (addr != JIM_INVALID_INTVAL) return (int)addr;
  /* label name */
  addr = dbgFindLabelByName(lname);
  if (addr < -32767 || addr > 65535) return JIM_INVALID_INTVAL;
  return (int)addr;
}


int jimGetIntVal (Jim_Interp *interp, Jim_Obj *obj) {
  return jimGetIntValEx(interp, obj, 0);
}


// <0: invalid
static int jimGetAddrOrLabel (Jim_Interp *interp, Jim_Obj *obj) {
  int addr = jimGetIntVal(interp, obj);
  if (addr < 0 || addr > 65535) {
    if (obj) cprintf("\x4invalid address: %s\n", Jim_String(obj));
    return -1;
  }
  return addr;
}


////////////////////////////////////////////////////////////////////////////////
// : invalid
static int jimGetIntLitVal (Jim_Interp *interp, Jim_Obj *obj) {
  if (!interp || !obj) return JIM_INVALID_INTVAL;
  long addr = -1;
  const char *lname = Jim_String(obj);
  /* try address */
  if (Jim_GetLong(interp, obj, &addr) == JIM_OK) {
    if (addr < -32768 || addr > 65535) return JIM_INVALID_INTVAL;
    return (int)addr;
  }
  /* try label name or #nnnn */
  return strTryHex(lname, 0/*allowpgaddr*/);
}


////////////////////////////////////////////////////////////////////////////////
static int jim_onoff_option (int *opt, Jim_Interp *interp, int argc, Jim_Obj *const *argv, const char *onstr, const char *offstr, int allowyesno) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), onstr, offstr, "toggle", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc < 1 || argc > 2) { Jim_WrongNumArgs(interp, 1, argv, "string"); return JIM_ERR; }
  if (argc > 1) {
    const char *a0 = parseAbbrev(Jim_String(argv[1]), onstr, offstr, "toggle", (allowyesno ? "yes" : NULL), (allowyesno ? "no" : NULL), (allowyesno ? "tan" : NULL), (allowyesno ? "ona" : NULL), NULL);
    if (a0 != NULL) {
           if (strEqu(a0, onstr) || (allowyesno && (strEqu(a0, "yes") || strEqu(a0, "tan")))) *opt = 1;
      else if (strEqu(a0, offstr) || (allowyesno && (strEqu(a0, "no") || strEqu(a0, "ona")))) *opt = 0;
      else if (strEqu(a0, "toggle")) *opt = !(*opt);
    } else {
      jim_SetResStrf(interp, "%s: invalid mode: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
      return JIM_ERR;
    }
  } else {
    if (jimapiFromConsole(interp)) cprintf("%s: %s\n", Jim_String(argv[0]), (*opt ? onstr : offstr));
  }
  Jim_SetResultString(interp, (*opt ? onstr : offstr), -1);
  return JIM_OK;
}


static int jim_AlphaArg (int *onoff, int *alpha, Jim_Interp *interp, int argc, Jim_Obj *const *argv) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "on", "off", "toggle", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  long val = *alpha;
  if (argc < 1 || argc > 2) { Jim_WrongNumArgs(interp, 1, argv, "alpha"); return JIM_ERR; }
  if (argc > 1) {
    val = jimGetIntLitVal(interp, argv[1]);
    if (val == JIM_INVALID_INTVAL) {
      if (onoff != NULL) {
        int res = jim_onoff_option(onoff, interp, argc, argv, "on", "off", 1);
        if (res != JIM_OK) jim_SetResStrf(interp, "%s: alpha value or state expected", Jim_String(argv[0]));
        return res;
      } else {
        jim_SetResStrf(interp, "%s: alpha value expected", Jim_String(argv[0]));
      }
      return JIM_ERR;
    }
    if (val < 0) val = 0; else if (val > 255) val = 255;
    *alpha = val;
  }
  Jim_SetResultInt(interp, val);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////

// <0: error
static int jim_AddrArg (Jim_Interp *interp, Jim_Obj *cmd, Jim_Obj *obj) {
  return jimGetAddrOrLabel(interp, obj);
}


static int jim_BWArg (Jim_Interp *interp, Jim_Obj *cmd, Jim_Obj *obj, int size) {
  int val = jimGetIntVal(interp, obj);
  if (val == JIM_INVALID_INTVAL) {
    jim_SetResStrf(interp, "%s: integer expected, got '%s'", Jim_String(cmd), Jim_String(obj));
    return -1;
  }
  if (size == 1) {
    if (val < -128 || val > 255) {
      jim_SetResStrf(interp, "%s: value %d out of range", Jim_String(cmd), val);
      return -1;
    }
    val &= 0xff;
  } else {
    if (val < -32768 || val > 65535) {
      jim_SetResStrf(interp, "%s: value %d out of range", Jim_String(cmd), val);
      return -1;
    }
    val &= 0xffff;
  }
  return val;
}


static void jim_completeAddr (Jim_Interp *interp, Jim_Obj *res, int argc, Jim_Obj *const *argv) {
  if (argc >= 2) {
    const char *lbl = parseLabelName(Jim_String(argv[2]));
    if (lbl) {
      char *a0 = completeLabelName(Jim_String(argv[2]));
      jimapiStrAppendCStr(interp, res, a0, -1);
      jimapiAppendRestArgs(res, 3);
      return;
    }
  }
  jimapiAppendRestArgs(res, 2);
}


////////////////////////////////////////////////////////////////////////////////
// why Jim doesn't have this handy shit?!
// returns object with 0 refcount (i.e. you have to lock it manually)
static Jim_Obj *jimRemoveComments (Jim_Interp *interp, Jim_Obj *obj) {
  int len = Jim_Length(obj);
  if (len == 0) return obj;
  const char *src = Jim_String(obj);
  char *res = Jim_Alloc(len+4);
  char *dest = res;
  int instr = 0;
  int wasskip = 0;
  while (len > 0) {
    if (instr) {
      *dest++ = *src++;
      --len;
      if (src[-1] == '\\') {
        if (len) {
          *dest++ = *src++;
          --len;
        }
      } else if (src[-1] == instr) {
        instr = 0;
      }
    } else {
      // skip a comment
      if (*src == '#') {
        wasskip = 1;
        ++src;
        --len;
        while (len > 0 && *src != '\n') {
          if (*src == '\r' && (len == 1 || src[1] == '\n')) break;
          ++src;
          --len;
        }
        if (!len) break;
        *dest++ = *src++;
        --len;
      } else if (*src == '\\') {
        *dest++ = *src++;
        --len;
        if (!len) break;
        *dest++ = *src++;
        --len;
      } else {
        *dest++ = *src++;
        --len;
        if (src[-1] == '"') instr = src[-1];
      }
    }
  }
  if (!wasskip) {
    Jim_Free(res);
    return obj;
  }
  *dest = 0;
  len = (int)(ptrdiff_t)(dest-res);
  return Jim_NewStringObjNoAlloc(interp, res, len);
}
