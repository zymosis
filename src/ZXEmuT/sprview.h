/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_SPRVIEW_H
#define ZXEMUT_SPRVIEW_H

#include <SDL.h>
#include "../libzymosis/zymosis.h"


extern void sprviewInit (void);
extern void sprviewSetActive (int st);

extern void sprviewDraw (void);
extern int sprviewKeyEvent (SDL_KeyboardEvent *key);


// this should cover most data formats
// data format with "2 bytes of bitmap, 2 bytes of mask" is missing, tho
enum {
  // zero flags means "no mask"
  // "first" means that mask data comes first
  // "interleaved" means that mask and sprite data is interleaved
  SFLAG_STANDARD = 0u,

  // is mask present at all?
  SFLAG_MASK_PRESENT = 1u<<0,
  // set if mask data precedes bitmap data
  SFLAG_MASK_FIRST   = 1u<<1,
  // set if mask data is interleaved with bitmap data
  SFLAG_MASK_ILV     = 1u<<2,

  SFLAG_BITMASK_FOR_MASK = 0x07,

  // sprite data come in "stripes": line forward, line backward
  SFLAG_H_STRIPED  = 1u<<8,
  // each data word in the first line is byte-swapped
  SFLAG_BSWAP_L0   = 1u<<9,
  // each data word in the second line is byte-swapped
  SFLAG_BSWAP_L1   = 1u<<10,

  // vertical stripes, 8xheight. TODO!
  //SFLAG_V_STRIPED  = 1u<<11,

  // the sprite is upside-down (Ultimate games uses this)
  // this is toggle
  SFLAG_UPSIDE_DOWN= 1u<<15,

  SFLAG_BITMASK_FOR_TYPE = 0x7F00,

  // special flag for `getSpritePixel()` only
  SFLAG_GET_NORMAL = 0u,
  SFLAG_GET_MASK = 1u<<30,
};

// type of the current sprite
extern uint32_t spr_type;
extern uint32_t spr_staddr;
extern int spr_wdt;
extern int spr_hgt;

#endif
