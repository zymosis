/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
static const int16_t pattern_pl2[] = {
  -15,
  0xAF, // XOR A
  0x37, // SCF
  0x14, // INC D
  0x08, // EX AF,AF'
  0x15, // DEC D
  0x3E, -32666, // LD A,xx
  0xD3, 0xFE, // OUT (#FE),A
  0x21, -32666, -32666, // LD HL,xxx
  0xE5, // PUSH HL
  0, 6, 0xCD, // CALL LD-EDGE2
  0, 21, 0xCD, // CALL LD-EDGE
  // check constants
  0, 26, 0x069C, // LD B,#9C
  0, 33, 0x3EC6, // LD A,#C6
  0, 41, 0x06C9, // LD B,#C9
  0, 49, 0xFE, 0xD4, // CP #D4
  0, 63, 0x067B, // LD B,#7B
  0, 96, 0x067D, // LD B,#7D
  0, 104, 0x3E8E, // LD A,#8E
  0, 109, 0x067B, // LD B,#7B
  // exit
  0, 121,
  0x7C, // LD A,H
  0xFE, 0x01, // CP #01
  0xC9, // RET
  // end
  0, 0
};


static int do_pl2 (void) {
  const tape_loader_info_t loader_info = {
    .pilot_minlen = 807,
    .pilot = 2168,
    .sync1 = 667,
    .sync2 = 735,
    .bit0 = 560,
    .bit1 = 1120
  };
  return emuTapeDoFlashLoadEx(&loader_info);
}


////////////////////////////////////////////////////////////////////////////////
__attribute__((constructor)) static void fl_loader_ctor_pl2 (void) {
  const fl_loader_info_t nfo = {
    .name="Players2 Loader",
    .pattern=pattern_pl2,
    .detectFn=NULL,
    .accelFn=do_pl2,
    .exitOfs=121,
  };
  fl_register_loader(&nfo);
}
