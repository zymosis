/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_LSSNAP_H
#define ZXEMUT_LSSNAP_H

#include <stdint.h>
#include <SDL.h>
#include <libspectrum.h>

#include "../libzymosis/zymosis.h"
#include "emucommon.h"
#include "emuvars.h"

#include "libzxsnap/zxsnap.h"


////////////////////////////////////////////////////////////////////////////////
extern int snapRealize (zxs_t *snap);
extern int snapRemember (zxs_t *snap); // remember current state in snapshot


////////////////////////////////////////////////////////////////////////////////
extern int szxSave (FILE *fl);
extern int szxLoad (FILE *fl);

extern int snaLoad (FILE *fl);
extern int snaSave (FILE *fl);


////////////////////////////////////////////////////////////////////////////////
enum {
  SNAPLOAD_ANY = 0x0f,
  SNAPLOAD_TAPES = 0x01,
  SNAPLOAD_DISKS = 0x02,
  SNAPLOAD_SNAPS = 0x04,
  SNAPLOAD_OTHER = 0x08,
  SNAPLOAD_TYPEMASK = 0x0f,
  SNAPLOAD_FLPNOSHIFT = 8
};


////////////////////////////////////////////////////////////////////////////////
// WARNING! non-threadsafe! (returned name may use static buffer)
extern char *loadWholeFile (const char *fname, size_t *size, const char **newname);

extern int addAutoBoot (int diskid, int forced, int simpleAutorun);

extern int loadSnapshot (const char *name, int flags);
extern int saveSnapshot (const char *name);

extern int lssnap_slt_clear (void);

extern int lssnap_rzx_first_frame (void);
extern int lssnap_rzx_next_frame (void); // <0: error; >0: complete
extern int lssnap_rzx_stop (void);


#endif
