/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include "keyled.h"
#include "libvideo/video.h"
#include "emuvars.h"


static VOverlay *kledOverlay = NULL;


// bitCnt < 0: reverse order
static void keyledDrawByte (int sx, int sy, int bitCnt, uint8_t b, Uint8 fc, Uint8 bc) {
  if (bitCnt != 0) {
    if (bitCnt > 0) sx += bitCnt;
    while (bitCnt != 0) {
      if (bitCnt < 0) {
        putPixelVO(kledOverlay, sx, sy, (b&0x01 ? fc : bc));
        b >>= 1;
        ++sx;
        ++bitCnt;
      } else {
        --sx;
        --bitCnt;
        putPixelVO(kledOverlay, sx, sy, (b&0x01 ? fc : bc));
        b >>= 1;
      }
    }
  }
}


static void keyledBuildOvr (void) {
  clearVO(kledOverlay, 5);
  drawFrameVO(kledOverlay, 0, 0, kledOverlay->w, kledOverlay->h, 0);
  for (int y = 0; y <= 3; ++y) {
    keyledDrawByte(2, y*2+2, -5, zxKeyboardState[3-y], 0, 15);
    keyledDrawByte(8, y*2+2, 5, zxKeyboardState[y+4], 0, 15);
  }
  keyledDrawByte(2, 4*2+2+1, -5, zxKeyboardState[8], 0, 15);
}


void keyledBlit (int x0, int y0) {
  if (kledOverlay == NULL) kledOverlay = createVO(15, 14);
  keyledBuildOvr();
  blitVO(kledOverlay, x0, y0, klAlpha);
}
