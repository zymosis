/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_Z80EXEC_H
#define ZXEMUT_Z80EXEC_H

#include <stdint.h>
#include <SDL.h>
#include <libspectrum.h>

#include "../libzymosis/zymosis.h"
#include "emucommon.h"
#include "emuvars.h"


////////////////////////////////////////////////////////////////////////////////
extern int z80_was_frame_end; // can't rely on tstates counter due to RZX

extern int tsmark_active;
extern int tsmark_frames;
extern int tsmark_tstart;

//extern void z80FrameEnd (void); // not public anymore

// return # of executed ticks
// will call z80FrameEnd() on end of frame and initiate interrupt after it
// will do ++z80_was_frame_end if z80FrameEnd() was called
// if `maxtstates` less than 0, execute until the breakpoint, or
// until the interrupt (or some other event the emu should process)
// (immediately) exits is debugger is activated, or the emu is paused
extern int z80Step (int maxtstates);

extern int isAutofireKempston (void); // !0: bit to set
extern int isAutofireKeyboard (void); // !0: lo byte: bit to reset (0x01), hi byte: port number (0-7)


#endif
