/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#define MV_FAST_RECODE

#include "../libzymosis/zymosis.h"
#include "libvideo/video.h"
#include "memview.h"
#include "emucommon.h"
#include "emuvars.h"
#include "emuutils.h"
#include "console.h"


// ////////////////////////////////////////////////////////////////////////// //
#define CC_INK      (0x10)
#define CC_PAPER    (0x11)
#define CC_FLASH    (0x12)
#define CC_BRIGHT   (0x13)
#define CC_INVERSE  (0x14)
#define CC_OVER     (0x15)
#define CC_AT       (0x16)
#define CC_TAB      (0x17)

#define CC_FONT     (0x19)


// ////////////////////////////////////////////////////////////////////////// //
#ifdef MV_FAST_RECODE
static const uint8_t cp1251to866[256] = {
  0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
  0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,
  0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,
  0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,
  0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,
  0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x5b,0x5c,0x5d,0x5e,0x5f,
  0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,
  0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x7b,0x7c,0x7d,0x7e,0x7f,
  0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,
  0xd0,0xd1,0xd2,0xd3,0xd4,0xd5,0xd6,0xd7,0xd8,0xd9,0xda,0xdb,0xdc,0xdd,0xde,0xdf,
  0xe0,0xe1,0xe2,0xe3,0xe4,0xe5,0xe6,0xe7,0xe8,0xe9,0xea,0xeb,0xec,0xed,0xee,0xef,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xf0,0xf1,0xf2,0xf3,0xf4,0xf5,0xf6,0xf7,0xf8,0xf9,0xfa,0xfb,0xfc,0xfd,0xfe,0xff,
  0xa8,0xb8,0xaa,0xba,0xaf,0xbf,0xa1,0xa2,0xb0,0x00,0xb7,0x00,0xb9,0xa4,0x00,0xa0,
};

#else

static const uint16_t charMap1251[128] = {
  0x0402,0x0403,0x201A,0x0453,0x201E,0x2026,0x2020,0x2021,0x20AC,0x2030,0x0409,0x2039,0x040A,0x040C,0x040B,0x040F,
  0x0452,0x2018,0x2019,0x201C,0x201D,0x2022,0x2013,0x2014,0x003F,0x2122,0x0459,0x203A,0x045A,0x045C,0x045B,0x045F,
  0x00A0,0x040E,0x045E,0x0408,0x00A4,0x0490,0x00A6,0x00A7,0x0401,0x00A9,0x0404,0x00AB,0x00AC,0x00AD,0x00AE,0x0407,
  0x00B0,0x00B1,0x0406,0x0456,0x0491,0x00B5,0x00B6,0x00B7,0x0451,0x2116,0x0454,0x00BB,0x0458,0x0405,0x0455,0x0457,
  0x0410,0x0411,0x0412,0x0413,0x0414,0x0415,0x0416,0x0417,0x0418,0x0419,0x041A,0x041B,0x041C,0x041D,0x041E,0x041F,
  0x0420,0x0421,0x0422,0x0423,0x0424,0x0425,0x0426,0x0427,0x0428,0x0429,0x042A,0x042B,0x042C,0x042D,0x042E,0x042F,
  0x0430,0x0431,0x0432,0x0433,0x0434,0x0435,0x0436,0x0437,0x0438,0x0439,0x043A,0x043B,0x043C,0x043D,0x043E,0x043F,
  0x0440,0x0441,0x0442,0x0443,0x0444,0x0445,0x0446,0x0447,0x0448,0x0449,0x044A,0x044B,0x044C,0x044D,0x044E,0x044F,
};

static const uint16_t charMap866[128] = {
  0x0410,0x0411,0x0412,0x0413,0x0414,0x0415,0x0416,0x0417,0x0418,0x0419,0x041A,0x041B,0x041C,0x041D,0x041E,0x041F,
  0x0420,0x0421,0x0422,0x0423,0x0424,0x0425,0x0426,0x0427,0x0428,0x0429,0x042A,0x042B,0x042C,0x042D,0x042E,0x042F,
  0x0430,0x0431,0x0432,0x0433,0x0434,0x0435,0x0436,0x0437,0x0438,0x0439,0x043A,0x043B,0x043C,0x043D,0x043E,0x043F,
  0x2591,0x2592,0x2593,0x2502,0x2524,0x2561,0x2562,0x2556,0x2555,0x2563,0x2551,0x2557,0x255D,0x255C,0x255B,0x2510,
  0x2514,0x2534,0x252C,0x251C,0x2500,0x253C,0x255E,0x255F,0x255A,0x2554,0x2569,0x2566,0x2560,0x2550,0x256C,0x2567,
  0x2568,0x2564,0x2565,0x2559,0x2558,0x2552,0x2553,0x256B,0x256A,0x2518,0x250C,0x2588,0x2584,0x258C,0x2590,0x2580,
  0x0440,0x0441,0x0442,0x0443,0x0444,0x0445,0x0446,0x0447,0x0448,0x0449,0x044A,0x044B,0x044C,0x044D,0x044E,0x044F,
  0x0401,0x0451,0x0404,0x0454,0x0407,0x0457,0x040E,0x045E,0x00B0,0x2219,0x00B7,0x221A,0x2116,0x00A4,0x25A0,0x00A0,
};


static uint8_t table1251to866[65536];
static int tableRCInited = 0;


//==========================================================================
//
//  initTableRC
//
//==========================================================================
static void initTableRC (void) {
  tableRCInited = 1;
  memset(table1251to866, '.', sizeof(table1251to866));
  for (uint16_t f = 0; f < 128; ++f) table1251to866[f] = (uint8_t)f;
  for (uint16_t f = 0; f < 128; ++f) {
    const uint16_t code = charMap1251[f];
    for (uint16_t c = 0; c < 128; ++c) {
      if (charMap866[c] == code) {
        table1251to866[f+128U] = (uint8_t)(c+128U);
      }
    }
  }
}
#endif


// ////////////////////////////////////////////////////////////////////////// //
int memviewActive = 0;
uint8_t mv_pagemap[256];

// >0xffff: pages
uint32_t mv_staddr = 0x5B00;
int mv_encoding = MV_ENC_7BIT;
int mv_viewmode = MV_MODE_HEX;
int mv_textwidth = 64;
uint8_t mv_textcolor = 7; // paper is ignored
int mv_colortext_mode = MV_CLRTEXT_NONE;
int mv_colortext_ignore = 0;
int mv_skip_trailing_spaces = 1;

#define BOOKMARK_NONE  (~((uint32_t)0))

static uint32_t bookmarks[10];

static int inaddrpos = -1;
static int mmeditpos = 0;
static int mmeditactive = 0;

static vt_simple_menu *current_menu = NULL;
static void (*menu_sel_cb) (uint32_t sel) = NULL;
static vt_simple_menu wkmenu;

static int evtCtrlKeyDown = 0;
static int evtAltKeyDown = 0;
static int evtShiftKeyDown = 0;


//==========================================================================
//
//  recodeByte
//
//==========================================================================
static inline uint16_t recodeByte (uint16_t b) {
  if (mv_encoding == MV_ENC_1251) {
    #ifdef MV_FAST_RECODE
    b = cp1251to866[b];
    #else
    if (!tableRCInited) initTableRC();
    b = table1251to866[b];
    #endif
  }
  if (mv_encoding == MV_ENC_7BIT) b &= 0x7f;
       if (b < 32 || b == 127) b = '.'|0x8000;
  else if (b == 32 && mv_viewmode == MV_MODE_HEX) b = '.'|0x4000;
  return b;
}


//==========================================================================
//
//  isAddrEditActive
//
//==========================================================================
static __attribute__((always_inline)) inline int isAddrEditActive (void) {
  return (inaddrpos >= 0);
}


//==========================================================================
//
//  isMemEditActive
//
//==========================================================================
static __attribute__((always_inline)) inline int isMMEditActive (void) {
  return mmeditactive;
}


//==========================================================================
//
//  getByte
//
//==========================================================================
static __attribute__((always_inline)) inline uint8_t getByte (uint32_t addr) {
  return
    addr <= 0xffffU ?
    z80.mem_read(&z80, addr, ZYM_MEMIO_OTHER) :
    zxMemoryBanks[mv_pagemap[((addr>>14)-4)%(unsigned)emuGetRAMPages()]][addr&0x3fffU];
}


//==========================================================================
//
//  memviewResetMMap
//
//==========================================================================
static void memviewResetMMap (int inorder) {
  for (uint32_t f = 0; f < 256; ++f) mv_pagemap[f] = f;
  if (!inorder) {
    mv_pagemap[1] = 5;
    mv_pagemap[5] = 1;
  }
}


//==========================================================================
//
//  memviewInit
//
//==========================================================================
void memviewInit (void) {
  memviewResetMMap(1);
  for (unsigned f = 0; f < 10; ++f) bookmarks[f] = BOOKMARK_NONE;
}


//==========================================================================
//
//  memviewSetActive
//
//==========================================================================
void memviewSetActive (int st) {
  memviewActive = !!st;
}


//==========================================================================
//
//  isGoodRSTInkColorCode
//
//==========================================================================
static inline int isGoodRSTInkColorCode (uint8_t b) {
  return
    (b > 0 && b <= 7) ||
    (b >= '0' && b <= '8') ||
    (b >= '@' && b <= 'G');
}


//==========================================================================
//
//  isGoodRSTFontColorCode
//
//==========================================================================
static inline int isGoodRSTFontColorCode (uint8_t b) {
  return (b >= '0' && b <= '3');
}


//==========================================================================
//
//  isGood1BCode
//
//==========================================================================
static inline int isGood1BCode (uint8_t b) {
  return (b >= '0' && b <= '7');
}


//==========================================================================
//
//  isGood1BCodeBright
//
//==========================================================================
static inline int isGood1BCodeBright (uint8_t b) {
  return (b >= '0' && b <= '1');
}


//==========================================================================
//
//  colorCodeLen
//
//==========================================================================
static inline uint16_t colorCodeLen (void) {
  switch (mv_colortext_mode) {
    case MV_CLRTEXT_CODE10: return 2;
    case MV_CLRTEXT_CODE1B: return 4;
  }
  return 0;
}


//==========================================================================
//
//  checkColor
//
//==========================================================================
static inline int checkColor (uint32_t addr) {
  if (mv_colortext_mode == MV_CLRTEXT_CODE10) {
    switch (getByte(addr)) {
      case CC_INK:
        return isGoodRSTInkColorCode(getByte(addr+1U));
      case CC_FONT:
        return isGoodRSTFontColorCode(getByte(addr+1U));
    }
  } else if (mv_colortext_mode == MV_CLRTEXT_CODE1B) {
    return
      getByte(addr) == 0x1b &&
      isGood1BCode(getByte(addr+1U)) &&
      isGood1BCode(getByte(addr+2U)) &&
      isGood1BCodeBright(getByte(addr+3U));
  }
  return 0;
}


//==========================================================================
//
//  interpretColor
//
//  `addr` is at the args
//
//  return number of args+1
//
//==========================================================================
static uint16_t interpretColor (uint32_t addr, uint8_t *attrp) {
  uint8_t cc = getByte(addr);
  switch (mv_colortext_mode) {
    case MV_CLRTEXT_CODE10:
      switch (cc) {
        case CC_INK:
          cc = getByte(addr+1U);
          if (isGoodRSTInkColorCode(cc)) {
            if ((cc&0x08) == 0) {
              *attrp &= 0xf0;
              *attrp |= (cc&07);
              if (cc >= 0x40) *attrp += 8;
            } else {
              *attrp = mv_textcolor;
            }
            return 2;
          }
          break;
        case CC_FONT:
          if (isGoodRSTFontColorCode(getByte(addr+1U))) {
            return 2;
          }
          break;
      }
      break;
    case MV_CLRTEXT_CODE1B:
      if (cc == 0x1B) {
        const uint8_t i = getByte(addr+1U);
        const uint8_t p = getByte(addr+2U);
        const uint8_t b = getByte(addr+3U);
        if ((i >= '0' && i <= '7') &&
            (p >= '0' && p <= '7') &&
            (b >= '0' && b <= '1'))
        {
          *attrp = ((p-'0')<<4)|(i-'0');
          if (b == '1') *attrp |= 0x88;
          return 4;
        }
      }
      break;
  }
  return 0;
}


//==========================================================================
//
//  checkNLFwd
//
//  return 0, or newline size
//
//==========================================================================
static inline uint8_t checkNLFwd (uint32_t addr) {
  switch (getByte(addr)) {
    case 10: return 1;
    case 13: return (getByte(addr+1U) == 10 ? 2 : 1);
  }
  return 0;
}


//==========================================================================
//
//  checkNLBack
//
//  return 0, or newline size
//
//==========================================================================
static inline uint8_t checkNLBack (uint32_t addr) {
  switch (getByte(addr)) {
    case 10: return (addr && getByte(addr-1U) == 13 ? 2 : 1);
    case 13: return 1;
  }
  return 0;
}


//==========================================================================
//
//  goLineUp
//
//==========================================================================
static uint32_t goLineUp (uint32_t addr) {
  if (!addr) return 0;

  uint8_t eolsize = checkNLBack(addr-1U);
  addr -= eolsize;

  int zerostate = -1; // <0:haven't seen; 0:non-zero seen; 1:only zero seen

  // skip trailing spaces
  if (eolsize && mv_skip_trailing_spaces) {
    while (addr && getByte(addr-1U) == 32) {
      --addr;
      zerostate = 0;
    }
  }

  const uint16_t clen = colorCodeLen();
  int count = mv_textwidth;

  while (addr && count) {
    if (checkNLBack(addr-1U)) return addr;
    if (getByte(addr-1U) == 0) {
      if (zerostate == 0) return addr; // seen non-zero
      zerostate = 1; // only zero seen
    } else {
      if (zerostate == 1) return addr;
      zerostate = 0; // seen non-zero
      // colored text
      if (clen && addr >= clen && checkColor(addr-clen)) {
        addr -= clen;
        continue;
      }
    }
    --addr;
    --count;
  }

  // color codes
  while (clen && addr >= clen && checkColor(addr-clen)) addr -= clen;

  return addr;
}


//==========================================================================
//
//  goLineDown
//
//==========================================================================
static uint32_t goLineDown (uint32_t addr) {
  const uint16_t clen = colorCodeLen();
  int zerostate = -1; // <0:haven't seen; 0:non-zero seen; 1:only zero seen
  int count = mv_textwidth;

  while (addr && count) {
    const uint8_t eolsize = checkNLFwd(addr);
    if (eolsize) return addr+eolsize;
    if (getByte(addr) == 0) {
      if (zerostate == 0) return addr; // seen non-zero
      zerostate = 1; // only zero seen
    } else {
      if (zerostate == 1) return addr;
      zerostate = 0; // seen non-zero
      // colored text
      if (clen && checkColor(addr)) {
        addr += clen;
        continue;
      }
    }
    ++addr;
    --count;
  }

  // check for trailing spaces
  if (mv_skip_trailing_spaces) {
    while (getByte(addr) == 32) ++addr;
  }

  return addr+checkNLFwd(addr);
}


//==========================================================================
//
//  drawAllChars
//
//==========================================================================
static __attribute__((unused)) void drawAllChars (void) {
  for (int y = 0; y < 16; ++y) {
    for (int x = 0; x < 16; ++x) {
      vt_writechar(x, y, y*16+x, 0x0f);
    }
  }
  for (int x = 0; x < 16; ++x) {
    vt_writechar(x, VID_TEXT_HEIGHT-1, '*', (x<<4)|(x < 8 ? 15 : 0));
  }
}


//==========================================================================
//
//  rescanPages
//
//==========================================================================
static void rescanPages (void) {
  for (int f = 0; f < zxMaxMemoryBank; ++f) {
    zxMemBanksDirty[f] = 0;
    if (f >= emuGetRAMPages()) continue;
    const uint8_t *mbb = zxMemoryBanks[f];
    for (uint32_t pos = 0; pos < 0x4000; ++pos, ++mbb) {
      if (*mbb) {
        zxMemBanksDirty[f] = 1;
        break;
      }
    }
  }
}


//==========================================================================
//
//  drawMMap
//
//==========================================================================
static void drawMMap (void) {
  int xofs = 26;
  int yofs = 10;

  vt_draw_frame_ex(xofs-2, yofs-1, 3*8+3, 10, 0x50,
                   VID_FRAME_DOUBLE|VID_FRAME_OVERWRITE|VID_FRAME_FILL|VID_FRAME_SHADOW,
                   " Page Map ");

  const int maxpg = emuGetRAMPages();
  if (mmeditpos/2 >= maxpg) mmeditpos = maxpg*2-1;

  for (int y = 0; y < 8; ++y) {
    for (int x = 0; x < 8; ++x) {
      if (y*8+x >= maxpg) break;
      uint8_t attr;
      if (y*8+x == mmeditpos/2) {
        attr = 0x1F;
      } else {
        attr = (zxMemBanksDirty[mv_pagemap[y*8+x]] ? 0x50 : 0x51);
      }
      vt_writef(xofs+x*3, yofs+y, attr, "%02X", mv_pagemap[y*8+x]);
    }
  }

  const int cx = mmeditpos%16;
  const int cy = mmeditpos/16;
  vt_writeattr(xofs+(cx/2)*3+(cx%2), yofs+cy, emuGetCursorColor(0xF0, 0x70));
}


//==========================================================================
//
//  writeAddrAt
//
//==========================================================================
static void writeAddrAt (int x, int y, uint32_t addr) {
  uint8_t pg;
  char buf[16];
  if (addr <= 0xffffU) {
    snprintf(buf, sizeof(buf), ".. %04X", addr);
    pg = zxMemoryBankNum[addr>>14];
  } else {
    pg = ((addr>>14)-4)%(unsigned)emuGetRAMPages();
    snprintf(buf, sizeof(buf), "%02X %04X", pg, addr&0x3fffU);
    pg = mv_pagemap[pg];
  }
  uint8_t attr = 0x05;
  for (unsigned f = 0; f < 10; ++f) {
    if (bookmarks[f] == addr) {
      attr |= 0x10;
      break;
    }
  }
  vt_writestrcnt(x, y, buf, 7, attr);
  if (!zxMemBanksDirty[pg]) vt_writeattrs(x, y, 2, 0x03|(attr&0xf0));
}


//==========================================================================
//
//  drawHexView
//
//==========================================================================
static void drawHexView (void) {
  char buf[256];
  const int mhgt = VID_TEXT_HEIGHT-2;
  uint32_t addr = mv_staddr;
  vt_draw_frame(0, 0, 80, mhgt+2, 15, VID_FRAME_SINGLE);
  for (int dy = 1; dy <= mhgt; ++dy) {
    writeAddrAt(1, dy, addr);

    for (int dx = 0; dx < 16; ++dx) {
      if (addr <= 0xffffU && addr+(unsigned)dx > 0xffffU) break;
      uint8_t b = getByte(addr+(unsigned)dx);
      snprintf(buf, sizeof(buf), "%02X", b);
      vt_writestrz(10+dx*3+(dx >= 8), dy, buf, 5+2*(dy&1));
    }

    for (int dx = 0; dx < 16; ++dx) {
      if (addr <= 0xffffU && addr+(unsigned)dx > 0xffffU) break;
      uint16_t b = recodeByte(getByte(addr+(unsigned)dx));
      vt_writechar(10+16*3+2+dx+(dx >= 8), dy, (char)b,
                   (b <= 0xff ? mv_textcolor : (b&0x8000 ? 5 : mv_textcolor)+(b&0x4000 ? 0x10 : 0)));
    }

    addr += 16;
  }
}


//==========================================================================
//
//  drawTextView
//
//==========================================================================
static void drawTextView (void) {
  uint32_t addr = mv_staddr;
  vt_draw_frame(0, 0, 80, VID_TEXT_HEIGHT, 15, VID_FRAME_SINGLE);
  uint8_t attr = mv_textcolor;
  for (int y = 1; y <= VID_TEXT_HEIGHT-2; ++y) {
    writeAddrAt(1, y, addr);
    uint32_t eaddr = goLineDown(addr);
    int xxofs = (MV_MAX_TEXT_WIDTH-mv_textwidth)/2;
    uint32_t x = 0;
    while (addr < eaddr) {
      if (checkNLFwd(addr)) break;
      // text coloring
      const uint16_t skip = interpretColor(addr, &attr);
      if (skip) {
        if (mv_colortext_ignore) attr = mv_textcolor;
        //if (attr == 0 || attr > 15) attr = mv_textcolor;
        if (attr == 0 || attr == 0x08 || attr == 0x80 || attr == 0x88) {
          attr = mv_textcolor;
        }
        addr += skip;
        continue;
      }
      if (x < mv_textwidth) {
        // not a color code, print it
        uint16_t b = recodeByte(getByte(addr));
        vt_writechar(10+xxofs+x, y, (char)b,
                     (b <= 0xff ? attr : (b&0x8000 ? 5 : attr)+(b&0x4000 ? 0x10 : 0)));
        ++x;
      }
      ++addr;
    }
    // skip trailing spaces (if there are any)
    if (mv_skip_trailing_spaces) {
      while (getByte(addr) == 32) ++addr;
    }
    addr += checkNLFwd(addr);
  }
  vt_writef(7, 0, 0x05, "[wdt:%d]", mv_textwidth);
}


//==========================================================================
//
//  memviewDraw
//
//==========================================================================
void memviewDraw (void) {
  //vt_cls(0xb0, 0x01);
  vt_cls(32, 0x07);

  if (mv_viewmode == MV_MODE_HEX) drawHexView(); else drawTextView();

  switch (mv_encoding) {
    case MV_ENC_7BIT: vt_writestrz(2+70, 0, "[7bit]", 4); break;
    case MV_ENC_1251: vt_writestrz(2+68, 0, "[cp1251]", 4); break;
    case MV_ENC_866: vt_writestrz(2+69, 0, "[cp866]", 4); break;
  }

  if (mv_viewmode == MV_MODE_TEXT && mv_skip_trailing_spaces) {
    vt_writestrz(65, 0, "[TS]", 5);
  }

  // draw physical page
  {
    uint8_t pg;
    if (mv_staddr <= 0xffffU) {
      pg = zxMemoryBankNum[mv_staddr>>14];
    } else {
      pg = mv_pagemap[((mv_staddr>>14)-4)%(unsigned)emuGetRAMPages()];
    }
    uint8_t attr = (zxMemBanksDirty[pg] ? 0x04 : 0x03);
    vt_writef(0, 0, attr, "[%02X]", pg);
  }

  if (isAddrEditActive()) {
    vt_writeattr(1+inaddrpos+(inaddrpos >= 2), 1, emuGetCursorColor(0xF0, 0x70));
  } else if (isMMEditActive()) {
    drawMMap();
  }

  if (current_menu) vt_smm_draw(current_menu);

  #if 0
  drawAllChars();
  #endif
}


//==========================================================================
//
//  doLeft
//
//==========================================================================
static void doLeft (void) {
  if (isAddrEditActive()) {
    if (inaddrpos) --inaddrpos;
    return;
  }
  if (isMMEditActive()) {
    if (mmeditpos > 0) {
      if (evtCtrlKeyDown) {
        mmeditpos = (mmeditpos&0xfe)-1;
        if (mmeditpos < 1) mmeditpos = 1;
      } else {
        --mmeditpos;
      }
    }
    return;
  }
  if (mv_staddr) --mv_staddr;
}


//==========================================================================
//
//  doRight
//
//==========================================================================
static void doRight (void) {
  if (isAddrEditActive()) {
    if (inaddrpos < 5) ++inaddrpos;
    return;
  }
  if (isMMEditActive()) {
    if (evtCtrlKeyDown) {
      mmeditpos = (mmeditpos&0xfe)+3;
      if (mmeditpos >= emuGetRAMPages()*2) mmeditpos = emuGetRAMPages()*2-1;
    } else {
      if (mmeditpos < emuGetRAMPages()*2-1) ++mmeditpos;
    }
    return;
  }
  ++mv_staddr;
}


//==========================================================================
//
//  doUp
//
//==========================================================================
static void doUp (void) {
  if (isAddrEditActive()) {
    if (inaddrpos >= 2) inaddrpos = 0;
    return;
  }
  if (isMMEditActive()) {
    if (mmeditpos >= 8*2) mmeditpos -= 8*2;
    return;
  }
  if (mv_viewmode == MV_MODE_TEXT) {
    uint32_t nn = goLineUp(mv_staddr);
    if (mv_staddr > 0xffffU && nn <= 0xffffU) nn = 0x010000U;
    mv_staddr = nn;
    return;
  }
  if (mv_staddr <= 0xffffU) {
    if (mv_staddr < 16) mv_staddr = 0; else mv_staddr -= 16;
  } else {
    mv_staddr -= 16;
  }
}


//==========================================================================
//
//  doDown
//
//==========================================================================
static void doDown (void) {
  if (isAddrEditActive()) {
    if (inaddrpos < 2) inaddrpos = 2;
    return;
  }
  if (isMMEditActive()) {
    if (mmeditpos+8*2 < 64*2) {
      mmeditpos += 8*2;
      if (mmeditpos >= emuGetRAMPages()*2) mmeditpos = emuGetRAMPages()*2-1;
    }
    return;
  }
  if (mv_viewmode == MV_MODE_TEXT) {
    mv_staddr = goLineDown(mv_staddr);
    return;
  }
  mv_staddr += 16;
}


//==========================================================================
//
//  doPageUp
//
//==========================================================================
static void doPageUp (void) {
  if (isAddrEditActive()) {
    if (mv_staddr > 0xffffU) {
      uint32_t pg = (mv_staddr>>14)-4;
      if (pg > 0) --pg;
      mv_staddr = (pg+4)<<14;
    }
    return;
  }
  if (isMMEditActive()) return;
  if (mv_viewmode == MV_MODE_TEXT) {
    for (int f = 0; f < VID_TEXT_HEIGHT-3; ++f) {
      uint32_t nn = goLineUp(mv_staddr);
      if (mv_staddr > 0xffffU && nn <= 0xffffU) nn = 0x010000U;
      mv_staddr = nn;
    }
    return;
  }
  if (mv_staddr <= 0xffffU) {
    if (mv_staddr < 16*(VID_TEXT_HEIGHT-2)) mv_staddr = 0; else mv_staddr -= 16*(VID_TEXT_HEIGHT-2);
  } else {
    mv_staddr -= 16*(VID_TEXT_HEIGHT-2);
  }
}


//==========================================================================
//
//  doPageDown
//
//==========================================================================
static void doPageDown (void) {
  if (isAddrEditActive()) {
    if (mv_staddr <= 0xffffU) {
      mv_staddr = 0x010000U;
    } else {
      uint32_t pg = (mv_staddr>>14)-4;
      if (pg+1 < (uint32_t)emuGetRAMPages()) mv_staddr = (pg+5)<<14;
    }
    return;
  }
  if (isMMEditActive()) return;
  if (mv_viewmode == MV_MODE_TEXT) {
    for (int f = 0; f < VID_TEXT_HEIGHT-3; ++f) {
      mv_staddr = goLineDown(mv_staddr);
    }
    return;
  }
  if (mv_staddr <= 0xffffU) {
    if (mv_staddr+16*(VID_TEXT_HEIGHT-2) > 0xffffU) mv_staddr = 0x10000U-16; else mv_staddr += 16*(VID_TEXT_HEIGHT-2);
  } else {
    mv_staddr += 16*(VID_TEXT_HEIGHT-2);
  }
}


//==========================================================================
//
//  menu_set_encoding
//
//==========================================================================
static void menu_set_encoding (uint32_t sel) {
  mv_encoding = (int)sel;
}


//==========================================================================
//
//  menu_set_text_width
//
//==========================================================================
static void menu_set_text_width (uint32_t sel) {
  const int wdts[5] = {32, 42, 51, 64, MV_MAX_TEXT_WIDTH};
  mv_textwidth = wdts[sel];
}


//==========================================================================
//
//  menu_set_text_bright
//
//==========================================================================
static void menu_set_text_bright (uint32_t sel) {
  mv_textcolor &= 0x07;
  mv_textcolor |= (sel ? 0x08 : 0x00);
}


//==========================================================================
//
//  menu_set_text_color
//
//==========================================================================
static void menu_set_text_color (uint32_t sel) {
  mv_textcolor = (mv_textcolor&0x08)|(sel+1);
}


//==========================================================================
//
//  menu_set_view_mode
//
//==========================================================================
static void menu_set_view_mode (uint32_t sel) {
  mv_viewmode = (int)sel;
}


//==========================================================================
//
//  menu_set_text_colormode
//
//==========================================================================
static void menu_set_text_colormode (uint32_t sel) {
  switch (sel) {
    case 0: mv_colortext_mode = MV_CLRTEXT_NONE; break;
    case 1: mv_colortext_mode = MV_CLRTEXT_CODE10; mv_colortext_ignore = 0; break;
    case 2: mv_colortext_mode = MV_CLRTEXT_CODE10; mv_colortext_ignore = 1; break;
    case 3: mv_colortext_mode = MV_CLRTEXT_CODE1B; mv_colortext_ignore = 0; break;
    case 4: mv_colortext_mode = MV_CLRTEXT_CODE1B; mv_colortext_ignore = 1; break;
  }
}


//==========================================================================
//
//  memviewKeyEvent
//
//==========================================================================
int memviewKeyEvent (SDL_KeyboardEvent *key) {
  if (current_menu) {
    int mrs = vt_smm_process_key(current_menu, key);
    switch (mrs) {
      case VID_SMM_ESCAPE:
        vt_smm_deinit(current_menu);
        current_menu = NULL;
        return 1;
      case VID_SMM_NOTMINE:
        break;
      case VID_SMM_EATEN:
        return 1;
      case VID_SMM_SELECTED:
        if (menu_sel_cb) {
          const uint32_t cy = current_menu->cursor_y;
          vt_smm_deinit(current_menu);
          current_menu = NULL;
          menu_sel_cb(cy);
        } else {
          vt_smm_deinit(current_menu);
          current_menu = NULL;
        }
        return 1;
    }
    return 1;
  }

  if (key->type != SDL_KEYDOWN) return 1;

  evtCtrlKeyDown = !!(key->keysym.mod&KMOD_CTRL);
  evtAltKeyDown = !!(key->keysym.mod&KMOD_ALT);
  evtShiftKeyDown = !!(key->keysym.mod&KMOD_SHIFT);

  if (isAddrEditActive() && key->keysym.unicode > 32 && key->keysym.unicode < 127) {
    int d = digitInBase((char)key->keysym.unicode, 16);
    if (d >= 0) {
      if (inaddrpos < 2) {
        uint32_t pg = (mv_staddr > 0xffffU ? (mv_staddr>>14)-4 : 0);
        pg &= (inaddrpos ? 0xf0U : 0x0fU);
        pg |= ((unsigned)d)<<((1-inaddrpos)*4);
        if (pg < emuGetRAMPages()) {
          mv_staddr = (mv_staddr&0x3fffU)|((pg+4)<<14);
        }
      } else {
        if (mv_staddr > 0xffffU && inaddrpos == 2 && d > 3) return 1;
        if (mv_staddr > 0xffffU) {
          uint32_t pg = (mv_staddr>>14);
          mv_staddr &= ~(0x000fU<<((5-inaddrpos)*4));
          mv_staddr |= ((unsigned)d)<<((5-inaddrpos)*4);
          mv_staddr |= pg<<14;
        } else {
          mv_staddr &= ~(0x000fU<<((5-inaddrpos)*4));
          mv_staddr |= ((unsigned)d)<<((5-inaddrpos)*4);
        }
      }
      if (inaddrpos < 5) ++inaddrpos;
      return 1;
    }
  }

  if (isMMEditActive() && key->keysym.unicode > 32 && key->keysym.unicode < 127) {
    int d = digitInBase((char)key->keysym.unicode, 16);
    if (d >= 0) {
      const int mmindex = mmeditpos/2;
      uint8_t v = mv_pagemap[mmindex];
      if (mmeditpos%2 == 0) {
        v &= 0x0F;
        v |= d<<4;
      } else {
        v &= 0xF0;
        v |= d;
      }
      if (mmindex < emuGetRAMPages() && v < emuGetRAMPages()) {
        // swap
        if (mv_pagemap[mmindex] != v) {
          int swidx = 0;
          while (swidx < 64 && mv_pagemap[swidx] != v) ++swidx;
          if (swidx < 64) {
            uint8_t vtmp = mv_pagemap[swidx];
            mv_pagemap[swidx] = mv_pagemap[mmindex];
            mv_pagemap[mmindex] = vtmp;
          } else {
            mv_pagemap[mmindex] = v;
          }
        }
        ++mmeditpos;
        if (mmeditpos >= emuGetRAMPages()*2) mmeditpos = emuGetRAMPages()*2-1;
      }
      return 1;
    }
  }

  if (!isAddrEditActive() && !isMMEditActive() &&
      key->keysym.sym >= SDLK_0 && key->keysym.sym <= SDLK_9)
  {
    int dig = (int)(key->keysym.sym-SDLK_0);
    if (evtShiftKeyDown) {
      if (mv_viewmode == MV_MODE_TEXT) {
        if (dig < 5) {
          const int twdta[5] = {MV_MAX_TEXT_WIDTH,32,42,51,64};
          mv_textwidth = twdta[dig];
        }
      }
    } else if (evtCtrlKeyDown) {
      bookmarks[dig] = mv_staddr;
    } else if (evtAltKeyDown) {
      if (bookmarks[dig] != BOOKMARK_NONE) mv_staddr = bookmarks[dig];
    }
    return 1;
  }

  switch (key->keysym.sym) {
    case SDLK_ESCAPE:
    case SDLK_q:
    case SDLK_x:
           if (isAddrEditActive()) inaddrpos = -1;
      else if (isMMEditActive()) mmeditactive = 0;
      else memviewSetActive(0);
      return 1;

    case SDLK_RETURN:
      if (isMMEditActive()) {
        // go to the selected physical page
        mv_staddr = (((mmeditpos/2)+4)<<14);
      }
      inaddrpos = -1;
      mmeditactive = 0;
      return 1;

    case SDLK_DELETE: case SDLK_KP_PERIOD:
      if (isAddrEditActive()) {
        if (inaddrpos < 2) mv_staddr &= 0xffffU;
      } else if (isMMEditActive()) {
             if (evtShiftKeyDown) memviewResetMMap(0);
        else if (evtCtrlKeyDown) memviewResetMMap(1);
        else if (evtAltKeyDown) {
          memviewResetMMap(1);
          // 00 01 03 04 06 07 02 05
          mv_pagemap[0] = 0x00;
          mv_pagemap[1] = 0x01;
          mv_pagemap[2] = 0x03;
          mv_pagemap[3] = 0x04;
          mv_pagemap[4] = 0x06;
          mv_pagemap[5] = 0x07;
          mv_pagemap[6] = 0x02;
          mv_pagemap[7] = 0x05;
        } else if (!evtShiftKeyDown && !evtCtrlKeyDown && !evtAltKeyDown) {
          const int mmindex = mmeditpos/2;
          uint8_t v = (uint8_t)mmindex;
          if (mmindex < emuGetRAMPages() && mv_pagemap[mmindex] != v) {
            int swidx = 0;
            while (swidx < 64 && mv_pagemap[swidx] != v) ++swidx;
            if (swidx < 64) {
              uint8_t vtmp = mv_pagemap[swidx];
              mv_pagemap[swidx] = mv_pagemap[mmindex];
              mv_pagemap[mmindex] = vtmp;
            } else {
              mv_pagemap[mmindex] = v;
            }
          }
        }
      }
      return 1;

    case SDLK_F6:
      mv_viewmode = !mv_viewmode; // we have only two for now
      return 1;

    case SDLK_LEFT: case SDLK_KP4: doLeft(); return 1;
    case SDLK_RIGHT: case SDLK_KP6: doRight(); return 1;
    case SDLK_UP: case SDLK_KP8: doUp(); return 1;
    case SDLK_DOWN: case SDLK_KP2: doDown(); return 1;
    case SDLK_PAGEUP: case SDLK_KP9: case SDLK_o: doPageUp(); return 1;
    case SDLK_PAGEDOWN: case SDLK_KP3: case SDLK_p: doPageDown(); return 1;
    case SDLK_BACKSPACE: if (isAddrEditActive()) doLeft(); return 1;

    case SDLK_SPACE:
      if (isAddrEditActive()) return 1;
      if (mv_viewmode == MV_MODE_TEXT) {
             if (evtCtrlKeyDown) mv_skip_trailing_spaces = !mv_skip_trailing_spaces;
        else if (evtShiftKeyDown) doPageUp(); else doPageDown();
      }
      return 1;

    case SDLK_HOME: case SDLK_KP7:
      if (isAddrEditActive()) inaddrpos = (inaddrpos > 2 ? 2 : 0);
      return 1;
    case SDLK_END: case SDLK_KP1:
      if (isAddrEditActive()) inaddrpos = (inaddrpos < 2 ? 2 : 5);
      return 1;

    case SDLK_b:
      if (!isAddrEditActive() && !isMMEditActive()) {
        if (evtAltKeyDown) {
          vt_smm_init(&wkmenu, "Text brightness", (const char *[]){"off", "on", NULL});
          menu_sel_cb = &menu_set_text_bright;
          wkmenu.cursor_y = (mv_textcolor >= 0x08 ? 1 : 0);
          current_menu = &wkmenu;
        } else {
          mv_textcolor ^= 0x08;
        }
      }
      return 1;

    case SDLK_c:
      if (!isAddrEditActive() && !isMMEditActive()) {
        if (evtAltKeyDown) {
          vt_smm_init(&wkmenu, "Text color", (const char *[]){
            "blue", "red", "magenta", "green", "cyan", "yellow", "white",
            NULL});
          menu_sel_cb = &menu_set_text_color;
          wkmenu.cursor_y = (mv_textcolor&0x07)-1;
          current_menu = &wkmenu;
        } else if (evtCtrlKeyDown && evtShiftKeyDown) {
          for (unsigned f = 0; f < 10; ++f) bookmarks[f] = BOOKMARK_NONE;
        } else {
          mv_textcolor = ((mv_textcolor+1)&0x07)|(mv_textcolor&0x08);
               if (!mv_textcolor) mv_textcolor = 1;
          else if (mv_textcolor == 8) mv_textcolor = 9;
        }
      }
      return 1;

    case SDLK_e:
      if (!isMMEditActive() && !isAddrEditActive()) {
        if (evtAltKeyDown) {
          vt_smm_init(&wkmenu, "Encoding", (const char *[]){"7 bit", "CP 866", "CP 1251", NULL});
          menu_sel_cb = &menu_set_encoding;
          wkmenu.cursor_y = (uint32_t)mv_encoding;
          current_menu = &wkmenu;
        } else {
          memviewSetActive(0);
        }
      }
      return 1;

    case SDLK_g:
      if (!isAddrEditActive() && !isMMEditActive()) inaddrpos = 2;
      return 1;

    case SDLK_i:
      if (!isAddrEditActive() && !isMMEditActive()) {
        if (evtAltKeyDown && mv_viewmode == MV_MODE_TEXT) {
          vt_smm_init(&wkmenu, "Color Codes", (const char *[]){
                      "mono",
                      "CODE #10", "CODE #10/ignore",
                      "CODE #1B", "CODE #1B/ignore",
                      NULL});
          menu_sel_cb = &menu_set_text_colormode;
               if (mv_colortext_mode == MV_CLRTEXT_NONE) wkmenu.cursor_y = 0;
          else if (mv_colortext_mode == MV_CLRTEXT_CODE10) wkmenu.cursor_y = 1+!!mv_colortext_ignore;
          else if (mv_colortext_mode == MV_CLRTEXT_CODE1B) wkmenu.cursor_y = 3+!!mv_colortext_ignore;
          else wkmenu.cursor_y = 0;
          current_menu = &wkmenu;
        }
      }
      return 1;

    case SDLK_m:
      if (!isAddrEditActive() && !isMMEditActive()) {
        if (evtAltKeyDown) {
          vt_smm_init(&wkmenu, "View mode", (const char *[]){"hex", "text", NULL});
          menu_sel_cb = &menu_set_view_mode;
          wkmenu.cursor_y = (uint32_t)mv_viewmode;
          current_menu = &wkmenu;
        }
      }
      return 1;

    case SDLK_r:
      if (isMMEditActive() && evtCtrlKeyDown) {
        rescanPages();
      }
      return 1;

    case SDLK_w:
      if (!isAddrEditActive() && !isMMEditActive()) {
        if (evtAltKeyDown && mv_viewmode == MV_MODE_TEXT) {
          vt_smm_init(&wkmenu, "Text width", (const char *[]){"32", "42", "51", "64", "max", NULL});
          menu_sel_cb = &menu_set_text_width;
               if (mv_textwidth <= 32) wkmenu.cursor_y = 0;
          else if (mv_textwidth <= 42) wkmenu.cursor_y = 1;
          else if (mv_textwidth <= 51) wkmenu.cursor_y = 2;
          else if (mv_textwidth <= 64) wkmenu.cursor_y = 3;
          else wkmenu.cursor_y = 4;
          current_menu = &wkmenu;
        }
      }
      return 1;

    case SDLK_F7:
      if (isAddrEditActive()) return 1;
      if (!isMMEditActive()) mmeditactive = 1;
      return 1;

    case SDLK_F9:
      if (!isAddrEditActive() && !isMMEditActive()) {
        if (evtAltKeyDown) {
          int v = optMaxSpeed;
          emuSetMaxSpeed(!v);
        }
      }
      return 1;

    case SDLK_BACKQUOTE:
      conVisible = 1;
      return 1;

    case SDLK_F1:
      if (!isAddrEditActive() && !isMMEditActive()) {
        if (evtAltKeyDown) {
          int v = ((optPaused&PAUSE_PERMANENT_MASK) == 0);
          emuSetPaused(v ? PAUSE_PERMANENT_SET : PAUSE_PERMANENT_RESET);
          return 1;
        }
      }
      /* fallthrough */
    case SDLK_h:
      /*if ((key->keysym.mod&KMOD_ALT) != 0)*/ {
        if (isAddrEditActive()) {
          vt_smm_init_textview(&wkmenu, "Small help (MM)",
            (const char *[]){
              "dunno, cursor keys. pgup/pgdn moves by pages.",
              "del on page number clears page index.",
              NULL
            });
        } else if (isMMEditActive()) {
          vt_smm_init_textview(&wkmenu, "Small help (AE)",
            (const char *[]){
              "\x01""C-Del", "  restore pages in order",
              "\x01""S-Del", "  restore pages in 502 order",
              "\x01""M-Del", "  restore pages in 201 order",
              "\x01""Del", "  make current page ordered",
              "\x01""Enter", "  go to the selected page",
              "\x01""C-R", "  analyze pages, mark pages filled with zeroes as unused",
              NULL
            });
        } else {
          vt_smm_init_textview(&wkmenu, "Small help",
            (const char *[]){
              "\x01""M-F1", "  toggle pause",
              "\x01""M-F9", "  toggle maxspeed",
              "\x01""F6", "  switch view mode",
              "\x01""F7", "  page mapping editor",
              //"\x01""F8", "  switch encoding",
              "\x01""G", "  input address",
              "\x01""C", "  cycle text colors",
              "\x01""B", "  toggle text brightness",
              "\x01""M-I", "  text color codes mode",
              "\x01""M-C", "  select text color",
              "\x01""M-B", "  select text brightness",
              "\x01""0-4", "  set text width",
              "\x01""M-E", "  select encoding",
              "\x01""M-W", "  select text width",
              "\x01""M-M", "  select view mode",
              "\x01""S-<digit>", "  maximum text width (in text mode)",
              "\x01""C-<digit>", "  set bookmark",
              "\x01""M-<digit>", "  go to bookmark",
              "\x01""C-S-C", "  clear bookmarks",
              "\x01""C-Spc", "  toggle \"skip trailing spaces\" mode",
              NULL
            });
        }
        menu_sel_cb = NULL;
        current_menu = &wkmenu;
      }
      return 1;

    default: break;
  }

  return 1;
}
