/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef LIBBOOTS_H
#define LIBBOOTS_H

#include <stdint.h>


extern const uint8_t bootph[273];
extern const uint8_t boot6[1809];
extern const uint8_t bootsmall[273];
extern const uint8_t kmboot[4881];
extern const uint8_t sboot32[1809];
extern const uint8_t sboot54[1809];
extern const uint8_t unrealboot[1297];


#endif
