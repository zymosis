/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef _BSD_SOURCE
# define _BSD_SOURCE
#endif
#ifndef _DEFAULT_SOURCE
# define _DEFAULT_SOURCE
#endif

#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/select.h>

#include <SDL.h>

#include <libspectrum.h>


////////////////////////////////////////////////////////////////////////////////
#include "libtinf/tinf.h"
#include "../libzymosis/zymosis.h"


////////////////////////////////////////////////////////////////////////////////
#include "emucommon.h"
#include "../libfusefdc/libfusefdc.h"
#include "emuvars.h"
#include "emuutils.h"
#include "emuexec.h"
#include "libvideo/video.h"
#include "debugger.h"
#include "memview.h"
#include "sprview.h"
#include "console.h"
#include "zxscrdraw.h"
#include "machines.h"
#include "snd_alsa.h"
#include "tapes.h"
#include "lssnap.h"
#include "zxkeyinfo.h"

#include "jimapi.h"

#include "keyled.h"
#include "diskled.h"


////////////////////////////////////////////////////////////////////////////////
extern const unsigned char keyHelpScr[];


////////////////////////////////////////////////////////////////////////////////
char binMyDir[8192];


static void initMyDir (void) {
  if (readlink("/proc/self/exe", binMyDir, sizeof(binMyDir)-1) < 0) {
    strcpy(binMyDir, ".");
  } else {
    char *p = (char *)strrchr(binMyDir, '/');
    //
    if (p == NULL) strcpy(binMyDir, "."); else *p = '\0';
  }
}


////////////////////////////////////////////////////////////////////////////////
const char *snapshotExtensions[] = {
  ".z80",
  ".sna",
  ".snp",
  ".sp",
  ".szx",
  ".slt",
  ".zxs",
  ".tap",
  ".tzx",
  ".spc",
  ".sta",
  ".ltp",
  ".pzx",
  ".trd",
  ".scl",
  ".fdi",
  ".udi",
  ".td0",
  ".dsk",
  ".dmb",
  ".rzx",
  ".zip",
  NULL
};


////////////////////////////////////////////////////////////////////////////////
typedef struct PortHandlers {
  int machine; // ZX_MACHINE_MAX: any
  uint16_t mask;
  uint16_t value;
  int (*portInCB) (zym_cpu_t *z80, uint16_t port, uint8_t *res);
  int (*portOutCB) (zym_cpu_t *z80, uint16_t port, uint8_t value);
} PortHandlers;


#define MAX_PORTHANDLER  (64)
static PortHandlers portHandlers[MAX_PORTHANDLER];
static int phCount = 0;


static void phAdd (const PortHandlers *ph) {
  if (ph != NULL) {
    while (ph->portInCB || ph->portOutCB) {
      if (phCount >= MAX_PORTHANDLER) { fprintf(stderr, "FATAL: too many port handlers!\n"); abort(); }
      portHandlers[phCount++] = *ph++;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
#include "zymcb.c"


////////////////////////////////////////////////////////////////////////////////
static int usock_stdin = 0;
static int usock_srv = -1;
static int usock_client = -1;
static char usock_command[1024];
static size_t usock_command_pos = 0;


static size_t unixsock_create_address (struct sockaddr_un *addr, const char *name) {
  if (!addr || !name || !name[0] || strlen(name) > 100) return 0;
  memset((void *)addr, 0, sizeof(*addr));
  addr->sun_family = AF_UNIX;
  addr->sun_path[0] = 0; /* abstract unix socket */
  strcpy(addr->sun_path+1, name);
  return strlen(addr->sun_path+1)+sizeof(addr->sun_family)+1u;
}


static void sock_make_non_blocking (int fd) {
  if (fd >= 0) {
    int flags = fcntl(fd, F_GETFL, 0);
    flags |= O_NONBLOCK;
    fcntl(fd, F_SETFL, flags);
  }
}


static void unixsock_drop_client (void) {
  if (usock_client >= 0) {
    close(usock_client);
    usock_client = -1;
  }
  usock_command_pos = 0;
}


static void unixsock_stop_server (void) {
  unixsock_drop_client();
  if (usock_srv >= 0) {
    close(usock_srv);
    usock_srv = -1;
  }
}


static void unixsock_start_server (const char *name) {
  unixsock_stop_server();

  /* use stdin */
  if (name && strcmp(name, "-") == 0) {
    if (usock_stdin) {
      cprintf("\4ERROR: stdin already closed!\n");
      usock_srv = -1;
      return;
    }
    usock_srv = -1;
    usock_client = 0;
    usock_stdin = 1;
    sock_make_non_blocking(usock_srv);
    return;
  }

  usock_srv = socket(AF_UNIX, SOCK_STREAM, 0);
  if (usock_srv < 0) {
    cprintf("\4ERROR: cannot create server socket!\n");
    usock_srv = -1;
    return;
  }
  sock_make_non_blocking(usock_srv);

  struct sockaddr_un serv_addr;
  size_t servlen = unixsock_create_address(&serv_addr, name);
  if (bind(usock_srv, (struct sockaddr *)&serv_addr, servlen) < 0) {
    cprintf("\4ERROR: cannot bind server socket!\n");
    close(usock_srv);
    usock_srv = -1;
    return;
  }

  if (listen(usock_srv, 1) < 0) {
    cprintf("\4ERROR: cannot listen on server socket!\n");
    close(usock_srv);
    usock_srv = -1;
    return;
  }

  cprintf("\1created unix server socket '%s'\n", name);
}


static void unixsock_handle (void) {
  if (usock_srv < 0 && usock_client < 0) return;
  struct timeval tout;
  fd_set rds;
  tout.tv_sec = 0;
  tout.tv_usec = 0;
  /* can accept new client? */
  if (usock_client < 0) {
    /* yeah, check for new client */
    FD_ZERO(&rds);
    FD_SET(usock_srv, &rds);
    int res = select(usock_srv+1, &rds, NULL, NULL, &tout);
    if (res <= 0) return; /* nothing */
    /* accept new client */
    struct sockaddr_un cli_addr;
    socklen_t clilen = (socklen_t)sizeof(cli_addr);
    int newsockfd = accept(usock_srv, (struct sockaddr *)&cli_addr, &clilen);
    if (newsockfd < 0) {
      cprintf("\4ERROR: error accepting client connection!\n");
      return;
    }
    usock_client = newsockfd;
    cprintf("\1USOCK: accepted new client\n");
  }
  /* check if we can read from client socket */
  for (int maxread = 4096; maxread > 0; --maxread) {
    char ch;
    FD_ZERO(&rds);
    FD_SET(usock_client, &rds);
    int res = select(usock_client+1, &rds, NULL, NULL, &tout);
    if (res <= 0) break; /* nothing */
    ssize_t rd = read(usock_client, &ch, 1);
    if (rd == 0) {
      cprintf("\2USOCK: client closed the connection\n");
      unixsock_drop_client();
      return;
    }
    if (rd < 0) {
      cprintf("\4ERROR: error reading from client connection!\n");
      unixsock_drop_client();
      return;
    }
    if (usock_command_pos >= sizeof(usock_command)-2) {
      cprintf("\4ERROR: too long command from client!\n");
      unixsock_drop_client();
      return;
    }
    if (ch == '\n') ch = 0;
    usock_command[usock_command_pos++] = ch;
    if (!ch) {
      /* command complete */
      usock_command_pos = 0;
      char *cmd = strprintf("::usock_received %s", usock_command);
      if (Jim_Eval(jim, cmd) == JIM_OK) {
        free(cmd);
        Jim_Obj *res = Jim_GetResult(jim);
        const char *resstr = Jim_String(res);
        if (resstr && strcasecmp(resstr, "close") == 0) {
          cprintf("closing client connection (client request).\n");
          unixsock_drop_client();
          return;
        }
        if (resstr && strcasecmp(resstr, "done") == 0) {
          continue;
        }
      } else {
        free(cmd);
        Jim_MakeErrorMessage(jim);
        cprintf("\4=== JIM ERROR ===\n%s\n=================\n", Jim_GetString(Jim_GetResult(jim), NULL));
      }
      /* close client connection? */
      /*
      if (strcmp(usock_command, "close") == 0) {
        cprintf("closing client connection (client request).\n");
        unixsock_drop_client();
        return;
      }
      */
      cprintf("\2USOCK COMMAND:<%s>\n", usock_command);
      conExecute(usock_command, 0);
      continue;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static inline void zxDoKeyUpDown (uint16_t portmask, int isdown) {
  if (portmask != 0) {
    if (isdown) {
      zxKeyboardState[(portmask>>8)&0xff] &= ~(portmask&0xff);
    } else {
      zxKeyboardState[(portmask>>8)&0xff] |= (portmask&0xff);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static void emuInitMemory (void) {
  zxMaxMemoryBank = 64;
  for (int f = 0; f < zxMaxMemoryBank; ++f) {
    if ((zxMemoryBanks[f] = calloc(1, 16384)) == NULL) {
      fprintf(stderr, "FATAL: out of memory!\n");
      abort();
    }
  }
  //
  zxMaxROMMemoryBank = 16;
  for (int f = 0; f < zxMaxROMMemoryBank; ++f) {
    if ((zxROMBanks[f] = calloc(1, 16384)) == NULL) {
      fprintf(stderr, "FATAL: out of memory!\n");
      abort();
    }
  }
}


static void emuInit (void) {
  int lineNo = 0;
  //
  for (int t = 0; t < 3; ++t) {
    for (int y = 0; y < 8; ++y) {
      for (int z = 0; z < 8; ++z) {
        zxScrLineOfs[lineNo++] = (t<<11)|(z<<8)|(y<<5);
      }
    }
  }
  //
  memset(zxKeyBinds, 0, sizeof(zxKeyBinds));
  memset(zxScreen, 0, sizeof(zxScreen));
  memset(zxUlaSnow, 0, sizeof(zxUlaSnow));
  //
  zxScreenCurrent = 0;
  zxWasUlaSnow = 0;
  zxBorder = 0;
  zxScreenBank = NULL;
  zxFlashTimer = 0;
  zxFlashState = 0;
  zxOldScreenTS = 0;
  emuFrameCount = 0;
  emuFrameStartTime = 0;
  emuLastFPSText[0] = '\0';
  //
  emuInitMemory();
  emuAddPortHandlers();
  //
  soundAYReset();
  //
  emuInitDisks();
  //
  zym_init(&z80);
  //zym_clear_callbacks(&z80);
  z80.mem_read = z80MemRead;
  z80.mem_write = z80MemWrite;
  z80.mem_contention = z80Contention;
  z80.port_read = z80PortIn;
  z80.port_write = z80PortOut;
  z80.port_contention = z80PortContention;
  z80.trap_edfb = z80_SLT_Trap;
  z80.trap_edfe = z80_ZXEmuT_Trap;
  z80.evenM1 = machineInfo.evenM1;
}


////////////////////////////////////////////////////////////////////////////////
// defattr < 0: use screen$ attr
static void paintZXScr (int x0, int y0, const void *ptr, int defattr) {
  const uint8_t *buf = (const uint8_t *)ptr;
  //
  for (int y = 0; y < 192; ++y) {
    int scrA = zxScrLineOfs[y], attrA = 6144+(y/8)*32;
    //
    for (int x = 0; x < 32; ++x) {
      uint8_t bt = buf[scrA++];
      uint8_t attr = (defattr >= 0 && defattr <= 255 ? defattr : buf[attrA++]);
      uint8_t paper = (attr>>3)&0x0f, ink = (attr&0x07)+(paper&0x08);
      //
      if (attr&0x80 && zxFlashState) { uint8_t t = paper; paper = ink; ink = t; }
      for (int f = 0; f < 8; ++f) {
        putPixel(x0+x*8+f, y0+y, (bt&0x80 ? ink : paper));
        bt = (bt&0x7f)<<1;
      }
    }
  }
}


static void paintZXScrZ (int x0, int y0) {
  static uint8_t scr[6912];
  unsigned int outlen = 6912;
  static int unpacked = 0;
  //
  if (!unpacked) {
    if (tinf_zlib_uncompress(scr, &outlen, keyHelpScr, 2153) == TINF_OK && outlen == 6912) {
      unpacked = 1;
    } else {
      return;
    }
  }
  if (unpacked) {
    clearScreen(0);
    paintZXScr(x0, y0, scr, -1);
  }
}


static void emuDrawFPS (void) {
  if (emuFrameStartTime > 0 && emuFrameCount) {
    int64_t tt = timerGetMS();
    if (tt-emuFrameStartTime >= 1000) {
      int fps = emuFrameCount/((tt-emuFrameStartTime)/1000.0)+0.5;
      if (fps < 0) fps = 0;
      //sprintf(emuLastFPSText, "%.15f %d %3d%%", ((double)emuFrameCount/((double)(tt-emuFrameStartTime)/1000)), fps, fps*100/50);
      sprintf(emuLastFPSText, "%d %3d%%", fps, fps*100/50);
      emuFrameStartTime = tt;
      emuFrameCount = 0;
    }
  }
  if (emuLastFPSText[0]) {
    drawStr6Outline(emuLastFPSText, frameSfc->w-6*strlen(emuLastFPSText)-2, 2, 12, 0);
  }
}


////////////////////////////////////////////////////////////////////////////////
static void zxPaintOverlaysAndLeds (void) {
  // key help
  if (optKeyHelpVisible) paintZXScrZ(32-zxScreenOfs, 24);

  // various leds
  if (optDrawKeyLeds) keyledBlit(1, frameSfc->h-15);
  if (optTapePlaying) emuTapeDrawCounter();
  diskBlit(frameSfc->w-26, frameSfc->h-26);

  // "max speed" mark
  if (optMaxSpeed) drawStr6Outline("max", frameSfc->w-3*6-2, frameSfc->h-10, 12, 0);

  // "paused" or FPS
  if (optPaused) {
    //drawStr8Outline("paused", frameSfc->w-6*8-2, 2, 12, 0);
    int w = drawStrZ("paused", 0, 0, 255, 255);
    drawStrZ("paused", 320-w-1, 0, 66, 67);
  } else if (optDrawFPS) {
    emuDrawFPS();
  }

  // "late timings" mark
  if (zxLateTimings) drawStr6Outline("LT", 2, 2, 12, 0);

  // ui overlays
  uiovlDraw();

  // debugger window
  if (debuggerActive) dbgDraw();

  // memory view window
  if (memviewActive) memviewDraw();

  // sprite view window
  if (sprviewActive) sprviewDraw();

  const int otherTextActive = (debuggerActive || memviewActive || sprviewActive);

  // console
  if (conVisible) {
    if (!otherTextActive) {
      vid_textsrc_height = CON_HEIGHT;
      vt_cls(32, 0x07);
    }
    conDraw();
  } else {
    vid_textsrc_height = 0;
  }
  if (debuggerActive && !dbgIsHidden()) vid_textsrc_height = 0;

  vid_textscr_active = ((debuggerActive && !dbgIsHidden()) || conVisible || memviewActive || sprviewActive);

  conDrawMessage();

  // rzx progress
  if (zxRZX) {
    static char buf[1024];
    Uint8 fg = ((zxBorder&0x07) < 6 ? 15 : 0);
    if (zxRZXFrames > 0) {
      int prc = 100*zxRZXCurFrame/zxRZXFrames;
      int secs = (int)((zxRZXFrames-zxRZXCurFrame)/(50.0*optSpeed/100.0));
      if (prc > 100) prc = 100;
      if (secs < 0) secs = 0;
      sprintf(buf, "RZX:%3d%% (%d:%02d left)", prc, secs/60, secs%60);
    } else {
      strcpy(buf, "RZX");
    }
    drawStr6(buf, 1, 1, fg, 255);
  }

  Jim_CollectIfNeeded(jim);
}


static void zxPaintZXScreen (void) {
  for (int y = 0; y < 240; ++y) {
    int lp = (y+24)*352+16+zxScreenOfs;
    // the real Speccy screen is not 'centered', as aowen says; 10px seems to be the right value
    if (optNoFlic > 0 || optNoFlicDetected) {
      Uint8 *d = (((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch));
      for (int x = 0; x < 320; ++x, ++lp, d += 4) {
        //putPixelMix(x, y, zxScreen[zxScreenCurrent][lp], zxScreen[zxScreenCurrent^1][lp]);
        Uint8 c0 = zxScreen[zxScreenCurrent][lp], c1 = zxScreen[zxScreenCurrent^1][lp];
        d[vidRIdx] = (palRGB[c0][0]+palRGB[c1][0])/2;
        d[vidGIdx] = (palRGB[c0][1]+palRGB[c1][1])/2;
        d[vidBIdx] = (palRGB[c0][2]+palRGB[c1][2])/2;
      }
    } else {
      Uint32 *dst = (Uint32 *)(((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch));
      if (optSlowShade && optSpeed < 100) {
        int shadeStart = zxScreenLineShadeStart(z80.tstates);
        if (lp >= shadeStart) {
          // the whole line is shaded
          for (int x = 0; x < 320; ++x) {
            putPixelMix(x, y, zxScreen[zxScreenCurrent][lp++], 0);
            //*dst++ = palette[zxScreen[zxScreenCurrent][lp++]];
          }
        } else if (lp < shadeStart && lp+319 >= shadeStart) {
          // line part is shaded
          for (int x = 0; x < 320; ++x) {
            if (lp < shadeStart) {
              *dst++ = palette[zxScreen[zxScreenCurrent][lp++]];
            } else {
              putPixelMix(x, y, zxScreen[zxScreenCurrent][lp++], 0);
            }
          }
        } else {
          // not shaded at all
          for (int x = 0; x < 320; ++x) {
            //putPixel(x, y, zxScreen[zxScreenCurrent][lp++]);
            *dst++ = palette[zxScreen[zxScreenCurrent][lp++]];
          }
        }
      } else {
        for (int x = 0; x < 320; ++x) {
          //putPixel(x, y, zxScreen[zxScreenCurrent][lp++]);
          *dst++ = palette[zxScreen[zxScreenCurrent][lp++]];
        }
      }
    }
  }
}


static void zxPaintScreen (void) {
  zxPaintZXScreen();
  zxPaintOverlaysAndLeds();
}


////////////////////////////////////////////////////////////////////////////////
static int msCursorDrawn = 0;
static int msRealCursorVisible = 1;
static int msLastGrabState = 0;


void emuHideRealMouseCursor (void) {
  if (msRealCursorVisible) {
    SDL_ShowCursor(0);
    msRealCursorVisible = 0;
  }
}


void emuShowRealMouseCursor (void) {
  if (!msRealCursorVisible) {
    SDL_ShowCursor(1);
    msRealCursorVisible = 1;
  }
}


void emuRealizeRealMouseCursorState (void) {
  if (optFullscreen) {
    emuHideRealMouseCursor();
    if (!msLastGrabState) {
      msLastGrabState = 1;
      zxMouseWasMoved = 0;
      zxMouseLastX = zxMouseLastY = 0;
      zxMouseFracX = zxMouseFracY = 0;
    }
  } else if (SDL_WM_GrabInput(SDL_GRAB_QUERY) == SDL_GRAB_OFF) {
    // no grab
    if (!msCursorDrawn) emuShowRealMouseCursor(); else emuHideRealMouseCursor();
    msLastGrabState = 0;
  } else {
    // grab
    emuHideRealMouseCursor();
    if (!msLastGrabState) {
      msLastGrabState = 1;
      zxMouseWasMoved = 0;
      zxMouseLastX = zxMouseLastY = 0;
      zxMouseFracX = zxMouseFracY = 0;
    }
  }
}


void emuFullScreenChanged (void) {
  emuRealizeRealMouseCursorState();
}


static void zxFrameCB (void) {
  if (!debuggerActive && !optPaused) {
    if (optMaxSpeed) {
      // doing maxspeed
      const int msscroff = (optMaxSpeedBadScreen || optMaxSpeedBadScreenTape);
      if (msscroff) ++optNoScreenReal;
      const int64_t stt = timerGetMS();
      //fprintf(stderr, "!!!\n");
      do {
        z80Step(-1);
      } while (!debuggerActive && !optPaused && timerGetMS()-stt < 20);
      //fprintf(stderr, "tst=%d; xtime=%u\n", tst, (uint32_t)(timerGetMS()-stt));
      z80_was_frame_end = 0;
      // redraw screen; prevent border flicking
      if (msscroff) {
        --optNoScreenReal;
        zxOldScreenTS = 0;
        zxRealiseScreen(machineInfo.tsperframe);
      } else {
        zxRealiseScreen(z80.tstates);
      }
    } else if (optSpeed != 100) {
      // not 100% speed
      int tickstodo = machineInfo.tsperframe*optSpeed/100+1;
      const int64_t stt = timerGetMS();
      if (emuPrevFCB == 0) {
        emuPrevFCB = stt;
      } else {
        if (stt-emuPrevFCB < 20) SDL_Delay(20-(stt-emuPrevFCB));
        emuPrevFCB = timerGetMS();
      }
      if (optSpeed < 100) {
        // slower than the original
        //fprintf(stderr, "speed=%d; tstates=%d; todo=%d; end=%d; frame=%d\n", optSpeed, z80.tstates, tickstodo, z80.tstates+tickstodo, machineInfo.tsperframe);
        z80Step(tickstodo);
      } else {
        // faster than the original
        int nomore = 0;
        // do incomplete frame
        tickstodo -= z80Step(tickstodo);
        z80_was_frame_end = 0;
        // execute full frames, checking for timeout
        while (tickstodo >= machineInfo.tsperframe && !debuggerActive && !optPaused) {
          // do one frame
          tickstodo -= z80Step(tickstodo);
          z80_was_frame_end = 0;
          if (timerGetMS()-stt >= 20) { nomore = 1; break; }
        }
        // do the rest
        if (!nomore) {
          while (tickstodo > 0 && !debuggerActive && !optPaused) {
            tickstodo -= z80Step(tickstodo);
          }
        }
      }
      z80_was_frame_end = 0;
    } else {
      // normal, do one frame
      z80Step(-1);
      if (z80_was_frame_end) {
        z80_was_frame_end = 0;
        if (!debuggerActive && !optPaused) {
          // normal execution; draw frame, play sound
          zxPaintScreen();
          forcedShowFrame();
          soundWrite();
          return;
        }
      }
    }
  }
  zxRealiseScreen(z80.tstates);
  zxPaintScreen();
}


////////////////////////////////////////////////////////////////////////////////
static void zxMouseSetButtons (int buttons) {
  if (buttons&MS_BUTTON_WHEELUP) zxKMouseWheel = ((int)zxKMouseWheel-1)&0x0f;
  if (buttons&MS_BUTTON_WHEELDOWN) zxKMouseWheel = ((int)zxKMouseWheel+1)&0x0f;
  zxKMouseButtons = buttons&(MS_BUTTON_LEFT|MS_BUTTON_RIGHT|MS_BUTTON_MIDDLE);
}


void emuSetKMouseAbsCoords (void) {
  int x = msRealX, y = msRealY;
  //fprintf(stderr, "MS: scale=%d, x=%d; y=%d\n", vidScale, x / vidScale, y / vidScale);
  x /= vidScale;
  y /= vidScale;
  x -= 32 + zxScreenOfs;
  y -= 24;
  //fprintf(stderr, "MS:   x=%d; y=%d\n", x, y);
  kmouseAbsX = x; kmouseAbsY = y;
}


static void zxMouseCB (int x, int y, int xrel, int yrel, int buttons) {
  if (/*widgetsProcessMouse(x, y, 0, buttons) ||*/ msCursorDrawn) return;
  //fprintf(stderr, "mouse: x=%d; y=%d; xrel=%d; yrel=%d; btn=#%02X\n", x, y, xrel, yrel, buttons);

  if (optKMouse && optKMouseAbsolute) {
    emuSetKMouseAbsCoords();
    return;
  }

  if (!msCursorDrawn && vidWindowActivated && SDL_WM_GrabInput(SDL_GRAB_QUERY) == SDL_GRAB_ON) {
    #if 0
    zxKMouseDXAccum += xrel;
    zxKMouseDYAccum += yrel;
    int speed = zxKMouseSpeed;
    if (speed < 1) speed = 1; else if (speed > 256) speed = 256;
    xrel = yrel = 0;
    while (zxKMouseDXAccum >= speed) { xrel += 1; zxKMouseDXAccum -= speed; }
    while (zxKMouseDXAccum <= -speed) { xrel -= 1; zxKMouseDXAccum += speed; }
    while (zxKMouseDYAccum >= speed) { yrel += 1; zxKMouseDYAccum -= speed; }
    while (zxKMouseDYAccum <= -speed) { yrel -= 1; zxKMouseDYAccum += speed; }
    zxKMouseDX = (((int)zxKMouseDX+xrel)&0xff);
    zxKMouseDY = (((int)zxKMouseDY-yrel)&0xff);
    #elif 0
    //int lp = (y+24)*352+16+zxScreenOfs;
    int dx = 0, dy = 0;
    if (zxMouseWasMoved) {
      dx = x-zxMouseLastX;
      dy = y-zxMouseLastY;
    } else {
      zxMouseLastX = x;
      zxMouseLastY = y;
      zxMouseWasMoved = 1;
    }
    int speed = zxKMouseSpeed;
    if (speed < 1) speed = 1; else if (speed > 256) speed = 256;
    dx /= speed;
    dy /= speed;
    if (dx != 0) {
      zxKMouseDX = (((int)zxKMouseDX+dx)&0xff);
      zxMouseLastX = x;
    }
    if (dy != 0) {
      zxKMouseDY = (((int)zxKMouseDY-dy)&0xff);
      zxMouseLastY = y;
    }
    #else
    if (!zxMouseWasMoved) {
      zxMouseLastX = zxMouseLastY = 0;
      zxMouseFracX = zxMouseFracY = 0;
      zxMouseWasMoved = 1;
    } else {
      int speed = zxKMouseSpeed;
      if (speed < 1) speed = 1; else if (speed > 256) speed = 256;
      zxMouseFracX += xrel;
      zxMouseFracY -= yrel;
      int dx = 0;
      int dy = 0;
      while (zxMouseFracX <= -speed) { --dx; zxMouseFracX += speed; }
      while (zxMouseFracX >= speed) { ++dx; zxMouseFracX -= speed; }
      while (zxMouseFracY <= -speed) { --dy; zxMouseFracY += speed; }
      while (zxMouseFracY >= speed) { ++dy; zxMouseFracY -= speed; }
      //if (abs(dx) >= 6) { if (dx < 0) dx -= (-dx)/2; else dx += dx/2; }
      //if (abs(dy) >= 6) { if (dy < 0) dy -= (-dy)/2; else dy += dy/2; }
      if (dx && zxMouseFracX <= 2) zxMouseFracX = 0;
      if (dy && zxMouseFracY <= 2) zxMouseFracY = 0;
      if (abs(dx) >= 6) dx *= 2;
      if (abs(dy) >= 6) dy *= 2;
      zxMouseLastX += dx;
      zxMouseLastY += dy;
      zxKMouseDX = (((int)zxKMouseDX+dx)&0xff);
      zxKMouseDY = (((int)zxKMouseDY+dy)&0xff);
    }
    #endif
    zxMouseSetButtons(buttons);
  } else {
    zxMouseWasMoved = 0;
  }
}


static void zxMouseButtonCB (int x, int y, int btn, int buttons) {
  if (/*widgetsProcessMouse(x, y, btn, buttons) ||*/ msCursorDrawn) return;
  if (!msCursorDrawn && vidWindowActivated) {
    //fprintf(stderr, "buttons=0x%02x\n", buttons);
    if (optKMouse && optKMouseAbsolute) {
      zxMouseSetButtons(buttons);
      return;
    }
    if (optKMouse && buttons == 0 && btn == (MS_BUTTON_LEFT|MS_BUTTON_DEPRESSED) &&
        SDL_WM_GrabInput(SDL_GRAB_QUERY) != SDL_GRAB_ON)
    {
      SDL_WM_GrabInput(SDL_GRAB_ON);
      emuRealizeRealMouseCursorState();
      zxMouseWasMoved = 0;
      return; // ignore this click
    }
    if (SDL_WM_GrabInput(SDL_GRAB_QUERY) == SDL_GRAB_ON) {
      zxMouseSetButtons(buttons);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static void zxKeyCB (SDL_KeyboardEvent *key) {
  if (conVisible) {
    if (conKeyEvent(key)) return;
    // skip debugger check here, i want it to be transparent
    if (conVisible || memviewActive || sprviewActive) return;
  }
  //if (msCursorDrawn) return;
  if (debuggerActive && dbgKeyEvent(key)) return;
  if (memviewActive && memviewKeyEvent(key)) return;
  if (sprviewActive && sprviewKeyEvent(key)) return;
  if (uiovlKey(key)) return;

  // skip debugger check here, i want it to be transparent
  if (conVisible || memviewActive || sprviewActive) return;

  if (key->type == SDL_KEYDOWN) {
    SDLJimBinding *bind;
    if ((bind = sdlFindKeyBind(sdlJimBindings, key->keysym.sym, key->keysym.mod)) != NULL && bind->action != NULL) {
      Jim_Obj *eres = NULL/*, *dupl*/;
      // duplicate 'action', 'cause Jim can return the same shared object
      // if there is nothing to 'expand' in it; it's safe to omit
      // duplication here, but future versions of Jim can check if the
      // object is 'shared' here, so let's do it right
      //!dupl = Jim_DuplicateObj(jim, bind->action);
      //!Jim_IncrRefCount(dupl); // we need to do this after Jim_DuplicateObj()
      if (Jim_SubstObj(jim, /*dupl*/bind->action, &eres, 0) == JIM_OK) {
        Jim_IncrRefCount(eres);
        if (Jim_EvalObjList(jim, eres) != JIM_OK) {
          Jim_MakeErrorMessage(jim);
          cprintf("\4=== JIM ERROR ===\n%s\n=================\n", Jim_GetString(Jim_GetResult(jim), NULL));
        }
        Jim_DecrRefCount(jim, eres);
      }
      //!Jim_DecrRefCount(jim, dupl);
      return;
    }
  }
  //
  if (/*vidWindowActivated &&*/ msCursorDrawn) return;
  //
  if (zxKeyBinds[key->keysym.sym]) {
    zxDoKeyUpDown(zxKeyBinds[key->keysym.sym]&0xffff, (key->type == SDL_KEYDOWN));
    zxDoKeyUpDown((zxKeyBinds[key->keysym.sym]>>16)&0xffff, (key->type == SDL_KEYDOWN));
    return;
  }
}


////////////////////////////////////////////////////////////////////////////////
#define PUSH_BACK(_c)  (*ress)[dpos++] = (_c)
#define DECODE_TUPLE(tuple,bytes) \
  for (tmp = bytes; tmp > 0; tmp--, tuple = (tuple & 0x00ffffff)<<8) \
    PUSH_BACK((char)((tuple >> 24)&0xff))

// returns ress length
static int ascii85Decode (char **ress, const char *srcs/*, int start, int length*/) {
  static uint32_t pow85[5] = { 85*85*85*85UL, 85*85*85UL, 85*85UL, 85UL, 1UL };
  const uint8_t *data = (const uint8_t *)srcs;
  int len = (int)strlen(srcs);
  uint32_t tuple = 0;
  int count = 0, c = 0;
  int dpos = 0;
  int start = 0, length = len;
  int tmp;
  //
  if (start < 0) start = 0; else { len -= start; data += start; }
  if (length < 0 || len < length) length = len;
  /*
  if (length > 0) {
    int xlen = 4*((length+4)/5);
    kstringReserve(ress, xlen);
  }
  */
  //
  *ress = (char *)calloc(1, len+1);
  for (int f = length; f > 0; --f, ++data) {
    c = *data;
    if (c <= ' ') continue; // skip blanks
    switch (c) {
      case 'z': // zero tuple
      if (count != 0) {
        //fprintf(stderr, "%s: z inside ascii85 5-tuple\n", file);
        free(*ress);
        *ress = NULL;
        return -1;
      }
      PUSH_BACK('\0');
      PUSH_BACK('\0');
      PUSH_BACK('\0');
      PUSH_BACK('\0');
      break;
    case '~': // '~>': end of sequence
      if (f < 1 || data[1] != '>') { free(*ress); return -2; } // error
      if (count > 0) { f = -1; break; }
    default:
      if (c < '!' || c > 'u') {
        //fprintf(stderr, "%s: bad character in ascii85 region: %#o\n", file, c);
        free(*ress);
        return -3;
      }
      tuple += ((uint8_t)(c-'!'))*pow85[count++];
      if (count == 5) {
        DECODE_TUPLE(tuple, 4);
        count = 0;
        tuple = 0;
      }
      break;
    }
  }
  // write last (possibly incomplete) tuple
  if (count-- > 0) {
    tuple += pow85[count];
    DECODE_TUPLE(tuple, count);
  }
  return dpos;
}

#undef PUSH_BACK
#undef DECODE_TUPLE


static void decodeBA (char *str, int len) {
  char pch = 42;
  //
  for (int f = 0; f < len; ++f, ++str) {
    char ch = *str;
    //
    ch = (ch-f-1)^pch;
    *str = ch;
    pch = ch;
  }
}


static void printEC (const char *txt) {
  char *dest;
  int len;
  //
  if ((len = ascii85Decode(&dest, txt)) >= 0) {
    decodeBA(dest, len);
    fprintf(stderr, "%s\n", dest);
    free(dest);
  }
}


static int isStr85Equ (const char *txt, const char *str) {
  char *dest;
  int len, res = 0;
  //
  if ((len = ascii85Decode(&dest, txt)) >= 0) {
    res = (strcmp(dest, str) == 0);
    free(dest);
  }
  return res;
}


static int checkEGG (const char *str) {
  if (isStr85Equ("06:]JASq", str) || isStr85Equ("0/i", str)) {
    printEC(
      "H8lZV&6)1>+AZ>m)Cf8;A1/cP+CnS)0OJ`X.QVcHA4^cc5r3=m1c%0D3&c263d?EV6@4&>"
      "3DYQo;c-FcO+UJ;MOJ$TAYO@/FI]+B?C.L$>%:oPAmh:4Au)>AAU/H;ZakL2I!*!%J;(AK"
      "NIR#5TXgZ6c'F1%^kml.JW5W8e;ql0V3fQUNfKpng6ppMf&ip-VOX@=jKl;#q\"DJ-_>jG"
      "8#L;nm]!q;7c+hR6p;tVY#J8P$aTTK%c-OT?)<00,+q*8f&ff9a/+sbU,:`<H*[fk0o]7k"
      "^l6nRkngc6Tl2Ngs!!P2I%KHG=7n*an'bsgn>!*8s7TLTC+^\\\"W+<=9^%Ol$1A1eR*Be"
      "gqjEag:M0OnrC4FBY5@QZ&'HYYZ#EHs8t4$5]!22QoJ3`;-&=\\DteO$d6FBqT0E@:iu?N"
      "a5ePUf^_uEEcjTDKfMpX/9]DFL8N-Ee;*8C5'WgbGortZuh1\\N0;/rJB6'(MSmYiS\"6+"
      "<NK)KDV3e+Ad[@).W:%.dd'0h=!QUhghQaNNotIZGrpHr-YfEuUpsKW<^@qlZcdTDA!=?W"
      "Yd+-^`'G8Or)<0-T&CT.i+:mJp(+/M/nLaVb#5$p2jR2<rl7\"XlngcN`mf,[4oK5JLr\\"
      "m=X'(ue;'*1ik&/@T4*=j5t=<&/e/Q+2=((h`>>uN(#>&#i>2/ajK+=eib1coVe3'D)*75"
      "m_h;28^M6p6*D854Jj<C^,Q8Wd\"O<)&L/=C$lUAQNN<=eTD:A6kn-=EItXSss.tAS&!;F"
      "EsgpJTHIYNNnh'`kmX^[`*ELOHGcWbfPOT`J]A8P`=)AS;rYlR$\"-.RG440lK5:Dg?G'2"
      "['dE=nEm1:k,,Se_=%-6Z*L^J[)EC"
    );
    return 1;
  }
  if (isStr85Equ("04Jj?B)", str)) {
    printEC(
      "IPaSa(`c:T,o9Bq3\\)IY++?+!-S9%P0/OkjE&f$l.OmK'Ai2;ZHn[<,6od7^8;)po:HaP"
      "m<'+&DRS:/1L7)IA7?WI$8WKTUB2tXg>Zb$.?\"@AIAu;)6B;2_PB5M?oBPDC.F)606Z$V"
      "=ONd6/5P*LoWKTLQ,d@&;+Ru,\\ESY*rg!l1XrhpJ:\"WKWdOg?l;=RHE:uU9C?aotBqj]"
      "=k8cZ`rp\"ZO=GjkfD#o]Z\\=6^]+Gf&-UFthT*hN"
    );
    return 1;
  }
  if (isStr85Equ("04o69A7Tr", str)) {
    printEC(
      "Ag7d[&R#Ma9GVV5,S(D;De<T_+W).?,%4n+3cK=%4+0VN@6d\")E].np7l?8gF#cWF7SS_m"
      "4@V\\nQ;h!WPD2h#@\\RY&G\\LKL=eTP<V-]U)BN^b.DffHkTPnFcCN4B;]8FCqI!p1@H*_"
      "jHJ<%g']RG*MLqCrbP*XbNL=4D1R[;I(c*<FuesbWmSCF1jTW+rplg;9[S[7eDVl6YsjT"
    );
    return 1;
  }
  return 0;
}


static void addBoots (int simpleAutorun) {
  for (int f = 0; f < 4; ++f) {
    if ((snapWasDisk&(1<<f)) && !(snapWasCPCDisk&(1<<f))) addAutoBoot(f, 1, simpleAutorun);
  }
  snapWasDisk = 0;
  snapWasCPCDisk = 0;
}


typedef enum {
  ARUN_UNDEFINED,
  ARUN_NONE,
  ARUN_TRDOS,
  ARUN_TRDOS48,
  ARUN_TRDOS128,
  ARUN_TRDOSPENT,
  ARUN_TAP,
  ARUN_TAP48,
  ARUN_TAP128,
  ARUN_TAPPENT,
  ARUN_TAPP2A,
  ARUN_TAPP3,
  ARUN_P3DOS2A,
  ARUN_P3DOS3,
} cli_arun_e;


static cli_arun_e cli_autorun = ARUN_UNDEFINED;

static void show_help (void) {
  printf(
    "options:\n"
    "  --48              use 48k model\n"
    "  --128             use 128k model\n"
    "  -A  --no-autorun  don't autorun file\n"
  );
  exit(0);
}


static char *strappend (char *s, const char *s1) {
  if (!s) s = strdup("");
  if (!s1 || !s1[0]) return s;
  char *res = strprintf("%s%s", s, s1);
  free(s);
  return res;
}


enum {
  CLI_MODEL_AUTO,
  CLI_MODEL_48,
  CLI_MODEL_128,
  CLI_MODEL_PENT,
  CLI_MODEL_PLUS2A,
  CLI_MODEL_PLUS3,
};


static void processOptions (int argc, char *argv[], int onlydbg) {
  int nomoreopts = 0, oldABoot = optAutoaddBoot;
  int do48 = CLI_MODEL_AUTO;
  optAutoaddBoot = 0;
  snapWasDisk = 0;
  snapWasCPCDisk = 0;
  snapWasTape = 0;
  for (int f = 1; f < argc; ++f) {
    if (checkEGG(argv[f])) exit(1);
    if (!nomoreopts) {
      if (strcmp(argv[f], "--") == 0) { nomoreopts = 1; continue; }
      if (strcmp(argv[f], "+") == 0) continue; // console command separator
      if (argv[f][0] == '+') {
        if (onlydbg) continue;
        optAutoaddBoot = oldABoot;
        if (strchr(argv[f], ' ') != NULL) {
          conExecute(argv[f]+1, 0);
        } else {
          // collect console command
          char *cmd = strdup(argv[f]+1);
          for (++f; f < argc; ++f) {
            if (argv[f][0] == '+') break;
            if (argv[f][0] == '-' && argv[f][1] == '-') break;
            cmd = strappend(cmd, " ");
            cmd = strappend(cmd, argv[f]);
          }
          --f; // compensate 'for'
          conExecute(cmd, 0);
          free(cmd);
        }
        if (oldABoot != optAutoaddBoot) {
          if (oldABoot) addBoots(0);
          oldABoot = optAutoaddBoot;
          snapWasDisk = 0;
          snapWasCPCDisk = 0;
        }
        optAutoaddBoot = 0;
        continue;
      }
      if (argv[f][0] == '-') {
        if (argv[f][1] == '-') {
               if (strcmp(argv[f], "--help") == 0) show_help();
          else if (strcmp(argv[f], "--48") == 0) do48 = CLI_MODEL_48;
          else if (strcmp(argv[f], "--128") == 0) do48 = CLI_MODEL_128;
          else if (strcmp(argv[f], "--pent") == 0 || strcmp(argv[f], "--pentagon") == 0) do48 = CLI_MODEL_PENT;
          else if (strcmp(argv[f], "--plus2a") == 0) do48 = CLI_MODEL_PLUS2A;
          else if (strcmp(argv[f], "--plus3") == 0) do48 = CLI_MODEL_PLUS3;
          else if (strcmp(argv[f], "--no-autorun") == 0) cli_autorun = ARUN_NONE;
          else if (strcmp(argv[f], "--opense") == 0) {
            if (!onlydbg) {
              if (!optOpenSE) { optOpenSE = 1; emuSetModel(zxModel, 1); }
            }
          }
          else if (strcmp(argv[f], "--usock") == 0 || strcmp(argv[f], "--usocket") == 0) {
            ++f;
            if (f >= argc) { fprintf(stderr, "option '%s' expects socket name!\n", argv[f-1]); exit(1); }
            if (!onlydbg) unixsock_start_server(argv[f]);
          }
          else if (strcmp(argv[f], "--unsafe-tcl") == 0) { if (onlydbg) Jim_SetAllowUnsafeExtensions(1); }
          else if (strcmp(argv[f], "--no-unsafe-tcl") == 0) { if (onlydbg) Jim_SetAllowUnsafeExtensions(0); }
          else if (strcmp(argv[f], "--tcl-verbose-loading") == 0) { jim_verbose_loading = 1; }
          else if (strcmp(argv[f], "--tcl-silent-loading") == 0) { jim_verbose_loading = 0; }
          else { fprintf(stderr, "unknown command line option: '%s'\n", argv[f]); exit(1); }
          continue;
        }
        for (const char *a = argv[f]+1; *a; ++a) {
          switch (*a) {
            case 'h': show_help(); break;
            case 'A': cli_autorun = ARUN_NONE; break;
            case 'D':
              if (onlydbg) {
                fprintf(stderr, "console dump enabled\n");
                optConDump = 1;
              }
              break;
            case 'q': /* do not dump consote text to stdout */
              optConDumpToStdout = 0;
              break;
            case 'S':
              if (onlydbg) {
                fprintf(stderr, "sound debug enabled\n");
                optSndSyncDebug = 1;
              }
              break;
            default: fprintf(stderr, "unknown command line option: '%c'\n", *a); exit(1);
          }
        }
        continue; // jff
      }
    }
    if (onlydbg) continue;
    // snapshot name
    if (loadSnapshot(argv[f], SNAPLOAD_ANY) == 0) {
      cprintf("'%s' loaded\n", argv[f]);
      if (cli_autorun != ARUN_NONE) {
        if (snapWasDisk) {
          cli_autorun = (snapWasCPCDisk ? ARUN_P3DOS2A : ARUN_TRDOS);
          if (do48 == CLI_MODEL_AUTO || do48 == CLI_MODEL_128) do48 = CLI_MODEL_PENT;
        } else if (snapWasTape) {
          cli_autorun = ARUN_TAP;
        } else {
          cli_autorun = ARUN_UNDEFINED;
        }
      }
      //cprintf("wasdisk=%d; wascpc=%d; ar=%d\n", snapWasDisk, snapWasCPCDisk, cli_autorun);
    } else {
      cprintf("failed to load '%s'\n", argv[f]);
    }
    snapWasTape = 0;
  }

  optAutoaddBoot = oldABoot;
  if (!onlydbg) {
    if (oldABoot && !snapWasCPCDisk) addBoots(1);
    if (cli_autorun > ARUN_NONE) {
      if (cli_autorun != ARUN_P3DOS2A && cli_autorun != ARUN_P3DOS3) {
        switch (do48) {
          case CLI_MODEL_AUTO:
            switch (cli_autorun) {
              case ARUN_TAP: cli_autorun = ARUN_TAP48; break;
              case ARUN_TRDOS: cli_autorun = ARUN_TRDOSPENT; break;
              default: break;
            }
            break;
          case CLI_MODEL_48:
            cli_autorun = (cli_autorun == ARUN_TRDOS ? ARUN_TRDOS48 : ARUN_TAP48);
            break;
          case CLI_MODEL_128:
            cli_autorun = (cli_autorun == ARUN_TRDOS ? ARUN_TRDOS128 : ARUN_TAP128);
            break;
          case CLI_MODEL_PENT:
            cli_autorun = (cli_autorun == ARUN_TRDOS ? ARUN_TRDOSPENT : ARUN_TAPPENT);
            break;
          case CLI_MODEL_PLUS2A:
            cli_autorun = (cli_autorun == ARUN_P3DOS3 ? ARUN_P3DOS2A : ARUN_TAPP2A);
            break;
          case CLI_MODEL_PLUS3:
            cli_autorun = (cli_autorun == ARUN_P3DOS3 ? ARUN_P3DOS3 : ARUN_TAPP3);
            break;
          default:
            cprintf("\1UNKNOWN CLI MODEL!\n");
            cli_autorun = (cli_autorun == ARUN_TRDOS ? ARUN_TRDOS48 : ARUN_TAP48);
            break;
        }
      }
      switch (cli_autorun) {
        case ARUN_TRDOS: conExecute("reset trdos", 0); break;
        case ARUN_TRDOS48: conExecute("reset 48k trdos", 0); break;
        case ARUN_TRDOS128: conExecute("reset 128k trdos", 0); break;
        case ARUN_TRDOSPENT: conExecute("reset pentagon 512 trdos", 0); break;
        case ARUN_TAP48: conExecute("reset 48k", 0); goto do_tape_autoload;
        case ARUN_TAP128: conExecute("reset 128k", 0); goto do_tape_autoload;
        case ARUN_TAPPENT: conExecute("reset pentagon 512", 0); goto do_tape_autoload;
        case ARUN_TAPP2A: conExecute("reset plus2a", 0); goto do_tape_autoload;
        case ARUN_TAPP3: conExecute("reset plus3", 0); goto do_tape_autoload;
        case ARUN_P3DOS2A: conExecute("reset plus2a", 0); break;
        case ARUN_P3DOS3: conExecute("reset plus3", 0); break;
        case ARUN_TAP:
do_tape_autoload:
          conExecute("tape _autoload", 0);
          break;
        default: ;
      }
    }
  }
}


static void xMainLoop (void) {
  const int mcsInFrame = 20*1000;
  static int64_t mcsFrameEndWanted;
  int eres = 1;
  mcsFrameEndWanted = timerGetMicroSeconds()+mcsInFrame;
  while (eres >= 0) {
    int64_t mcsCurFrameEnd;
    eres = processEvents(0);
    buildFrame();
    unixsock_handle();
    mcsCurFrameEnd = timerGetMicroSeconds();
    if (optMaxSpeed && !debuggerActive && !optPaused) {
      mcsFrameEndWanted = mcsCurFrameEnd+mcsInFrame;
      continue;
    }
    if (mcsCurFrameEnd > 0) {
      int mcsSleep = (mcsFrameEndWanted-mcsCurFrameEnd);
      //fprintf(stderr, "0: wait=%.15g\n", ((double)mcsSleep)/1000.0);
      if (mcsSleep > 0) {
        // less than 20 ms
        //fprintf(stderr, "SLEEP: %.15g\n", ((double)mcsSleep)/1000.0);
        usleep(mcsSleep);
        //mcsCurFrameEnd = timerGetMicroSeconds();
        //fprintf(stderr, "1:few=%d; cfe=%d; few-cfe=%.15g\n", (int)mcsFrameEndWanted, (int)mcsCurFrameEnd, ((double)(mcsInFrame-(mcsFrameEndWanted-mcsCurFrameEnd)))/1000);
        mcsFrameEndWanted += mcsInFrame;
      } else {
        fprintf(stderr, "DESYNC! (%d)\n", mcsSleep);
        //mcsFrameEndWanted = timerGetMicroSeconds()+mcsInFrame;
        mcsFrameEndWanted = mcsCurFrameEnd+mcsInFrame;
      }
    } else {
      //FIXME
      // reinit timer
      timerReinit();
      mcsFrameEndWanted = timerGetMicroSeconds()+mcsInFrame;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static void cprintLibFDC (int type, const char *msg) {
  switch (type) {
    case LIBFDC_MSG_DEBUG: cprintf("\3LIBFDC[debug]: %s\n", msg); break;
    case LIBFDC_MSG_WARNING: cprintf("\2LIBFDC[warn]: %s\n", msg); break;
    case LIBFDC_MSG_ERROR: cprintf("\4LIBFDC[error]: %s\n", msg); break;
    default: cprintf("\3LIBFDC[???]: %s\n", msg); break; // the thing that should not be
  }
}


////////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  Jim_SetAllowUnsafeExtensions(1);
  tinf_init();
  conInit();
  initMyDir();
  //
  libfdcMessageCB = &cprintLibFDC;
  //
  processOptions(argc, argv, 1);
  //
  if (libspectrum_init() != LIBSPECTRUM_ERROR_NONE) {
    fprintf(stderr, "FATAL: can't init libspectrum!\n");
    return 1;
  }
  cprintf("===================================\n");
  cprintf("using libspectrum v%s\n", libspectrum_version());
  //
  switch (timerInit()) {
    case TIMER_ERROR: abort();
    case TIMER_HPET: break;
    default:
      cprintf(
        "\2WARNING: please, set your clock source to HPET!\n"
        "you can do this by issuing the following command:\n"
        "======\n"
        "\1sudo echo 'hpet' > /sys/devices/system/clocksource/clocksource0/current_clocksource\n"
        "======\n"
        "\2this is not a critical issue, but hpet clock will\n"
        "\2give you slightly better emulation.\n"
      );
      break;
  }
  //
  if (!Jim_GetAllowUnsafeExtensions()) {
    cprintf("\2WARNING: Disabled unsafe Tcl extensions.\n");
  } else {
    cprintf("Unsafe Tcl extensions enabled ('--no-unsafe-tcl' to disable).\n");
  }
  //
  //zxwinInit();
  jimInit();
  //
  emuInit();
  jimEvalFile("init/init.tcl", 0);
  //
  jimEvalFile("init/roms.tcl", 0);
  emuSetModel(zxModel, 1);
  //
  dbgInit();
  memviewInit();
  sprviewInit();
  //
  emuInitBindings();
  jimEvalFile("init/concmd.tcl", 0);
  //
  sdlInit();
  initVideo();
  //
  frameCB = zxFrameCB;
  keyCB = zxKeyCB;
  mouseCB = zxMouseCB;
  mouseButtonCB = zxMouseButtonCB;
  //
  //jimEvalFile("init/widgets/init.tcl", 1);
  //
  jimEvalFile("autoexec.tcl", 1);
  emuSetModel(zxModel, 1); // in case something vital was changed (like "opense on")
  //
  processOptions(argc, argv, 0);
  //
  jimEvalFile1("./.zxemutrc.tcl");
  jimEvalFile1("./.zxemut.tcl");
  jimEvalFile1("./zxemutrc.tcl");
  jimEvalFile1("./zxemut.tcl");
  //
#ifdef USE_SOUND
  if (sndSampleRate < 0 && initSound() != 0) {
    fprintf(stderr, "WARNING: can't initialize sound!\n");
    cprintf("\4WARNING: can't initialize sound!\n");
    sndSampleRate = 0;
  }
  sndAllowUseToggle = 0;
  if (sndSampleRate <= 0) cprintf("NOSOUND mode");
  //
  frameCB = zxFrameCB;
  keyCB = zxKeyCB;
  mouseCB = zxMouseCB;
  mouseButtonCB = zxMouseButtonCB;
  //
  uiovlInit();
  //
  if (sndSampleRate > 0) {
    while (processEvents(0) >= 0) {
      buildFrame();
      unixsock_handle();
      // optMaxSpeed implies sndUsing==0
      if (!optMaxSpeed && optSpeed == 100 && (debuggerActive || optPaused)) soundWrite();
    }
  } else {
    xMainLoop();
  }
#else
  sndAllowUseToggle = 0;
  xMainLoop();
#endif
  //
  unixsock_stop_server();
  uiovlDeinit();
  //zxwinDeinit();
  beta_end();
  deinitSound();
  jimDeinit();
  if (condumpfl != NULL) fclose(condumpfl);
  return 0;
}
