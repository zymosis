/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_DEBUGGER_H
#define ZXEMUT_DEBUGGER_H

#include <SDL.h>
#include "../libzymosis/zymosis.h"


enum {
  DBG_BP_NONE    = 0x00u,
  DBG_BP_EXEC    = 0x01u,
  DBG_BP_READ    = 0x02u,
  DBG_BP_WRITE   = 0x04u,
  DBG_BP_PORTIN  = 0x08u,
  DBG_BP_PORTOUT = 0x10u,
  DBG_BP_EXECONE = 0x80u,
};


extern void dbgInit (void);
extern void dbgSetActive (int st);
extern int z80CheckBP (uint16_t addrport, uint8_t type);

// to view original screen
extern int dbgIsHidden (void);

extern void dbgDraw (void);
extern int dbgKeyEvent (SDL_KeyboardEvent *key);

extern void dbgSetUnasmAddr (uint16_t addr);

extern uint8_t dbgGetBPType (uint16_t addr);
extern void dbgSetBPType (uint16_t addr, uint8_t type);
extern void dbgBPClear (void);
extern const char *dbgGetBPTypeStr (uint8_t type);

extern void dbgClearLabels (void);
extern void dbgAddLabel (int addr, const char *name, int asoffset);
extern void dbgRemoveLabel (const char *name);
/* <0: not found */
extern int dbgFindLabelByName (const char *name);
/* NULL: not found */
extern const char *dbgFindLabelByVal (int val, int asoffset);

/* NULL: no */
extern void *dbgFirstLabelByPrefix (const char *pfx);
/* NULL: no more */
extern void *dbgNextLabelByPrefix (void *it, const char *pfx);
extern const char *dbgIteratorLabelName (const void *it);
extern void dbgIteratorFree (void *it);

/* !0: error */
extern int dbgLoadRefFile (const char *fname);
/* !0: error */
extern int dbgSaveRefFile (const char *fname);

/* !0: error */
extern int dbgBPSaveToFile (const char *fname);
/* !0: error */
extern int dbgBPLoadFromFile (const char *fname);


#endif
