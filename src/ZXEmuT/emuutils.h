/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_UTILS_H
#define ZXEMUT_UTILS_H

#include <stdint.h>
#include <SDL.h>
#include <libspectrum.h>

#include "../libzymosis/zymosis.h"
#include "../libfusefdc/libfusefdc.h"
#include "emucommon.h"
#include "emuvars.h"


////////////////////////////////////////////////////////////////////////////////
extern void zxPageROM (int newpg);
extern void zxPageRAM (int pageno, int newpg);
extern void zxSetScreen (int newpg);
extern void zxTRDOSpagein (void);
extern void zxTRDOSpageout (void);


////////////////////////////////////////////////////////////////////////////////
extern void emuUPD765Reset (void);
extern void emuInitDisks (void);


////////////////////////////////////////////////////////////////////////////////
extern void emuBuildContentionTable (void);
extern void emuRealizeMemoryPaging (void);

extern void emuSetMaxSpeed (int maxsp);
extern void emuSetSpeed (int prc);
extern void emuSetPaused (int on);
extern void emuSetLateTimings (int late);

extern int emuSetModel (int model, int forced);
extern void emuReset (int flags); // bit0: to tr-dos
extern void emuResetAY (void); // reset and mute AY
extern void emuResetKeyboard (void); // "unpress" all ZX keys
extern void emuClearRAM (void);

extern void emuSetMaxSpeed (int maxsp);
extern void emuSetSpeed (int prc);
extern void emuSetPaused (int on);
extern void emuSetDrawFPS (int on);

static inline EMU_MAYBE_UNUSED int emuIsPlus3DOSModel (int model) {
  return (model >= 0 && model < ZX_MACHINE_MAX ? basicMachineInfo[model].p3dos : 0);
}

static inline EMU_MAYBE_UNUSED int emuGetModelFDCType (int model) {
  return (emuIsPlus3DOSModel(model) ? DIF_P3DOS : DIF_BDI);
}

static inline int emuIsUPD765 (void) {
  return (upd765_fdc && emuIsPlus3DOSModel(zxModel));
}


void emuDiskEject (int diskidx);
void emuDiskInsert (int diskidx);

typedef int (*emuDiskReaderFn) (disk_t *d, const void *data, int size);

// returns 0 on ok, something that is not zero on error
int emuDiskInsertWithReader (int diskidx, const void *buf, int size, emuDiskReaderFn reader);

// returns 0 on ok, something that is not zero on error
int emuSaveDiskToFile (int diskidx, const char *fname);


static __attribute__((unused)) inline int emuGetRAMPages (void) {
  switch (zxModel) {
    case ZX_MACHINE_48K:
      return 3;
    case ZX_MACHINE_128K:
    case ZX_MACHINE_PLUS2:
    case ZX_MACHINE_PLUS2A:
    case ZX_MACHINE_PLUS3:
      return 8;
    case ZX_MACHINE_PENTAGON:
      return zxPentagonMemory>>4;
    case ZX_MACHINE_SCORPION:
      return 16; // 256KB
  }
  return 3;
}


// ////////////////////////////////////////////////////////////////////////// //
void emuHideRealMouseCursor (void);
void emuShowRealMouseCursor (void);
void emuFullScreenChanged (void);
void emuRealizeRealMouseCursorState (void);
void emuSetKMouseAbsCoords (void);

uint8_t emuGetCursorColor (uint8_t c0, uint8_t c1);


// ////////////////////////////////////////////////////////////////////////// //
static __attribute__((always_inline)) __attribute__((unused))
inline int digitInBase (char ch, int base) {
  if (!ch || base < 1) return -1;
  if (ch >= '0' && ch <= '9') {
    ch -= '0';
    return (ch < base ? ch : -1);
  }
  if (base <= 10) return -1;
       if (ch >= 'a' && ch <= 'z') ch -= 'a';
  else if (ch >= 'A' && ch <= 'Z') ch -= 'A';
  else return -1;
  ch += 10;
  return (ch < base ? ch : -1);
}


static __attribute__((always_inline)) __attribute__((unused))
inline int strEqu (const char *s0, const char *s1) {
  if (!s0) s0 = "";
  if (!s1) s1 = "";
  if (s0 == s1) return 1;
  return (strcmp(s0, s1) == 0);
}


static __attribute__((unused)) inline int strEquCI (const char *s0, const char *s1) {
  if (!s0) s0 = "";
  if (!s1) s1 = "";
  if (s0 == s1) return 1;
  const size_t s0len = strlen(s0);
  const size_t s1len = strlen(s1);
  if (s0len != s1len) return 0;
  for (size_t f = s0len; f--; ++s0, ++s1) {
    uint8_t c0 = *((const uint8_t *)s0);
    uint8_t c1 = *((const uint8_t *)s1);
    // try the easiest case first
    if (c0 == c1) continue;
    c0 |= 0x20; // convert to ascii lowercase
    if (c0 < 'a' || c0 > 'z') return 0; // it wasn't a letter, no need to check the second char
    // c0 is guaranteed to be a lowercase ascii here
    // c1 will become a lowercase ascii only if it was uppercase/lowercase ascii
    if (c0 != (c1|0x20)) return 0;
  }
  return 1;
}


#endif
