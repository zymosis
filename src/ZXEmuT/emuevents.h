/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_EVENTS_H
#define ZXEMUT_EVENTS_H

#include <stdint.h>

#include "emucommon.h"


#define EMU_EV_NONE  (~((uint32_t)0))


typedef struct {
  uint32_t tstates;
  uint32_t type; // event type
  void *udata; // user data
} EmuEvent;


typedef void (*EmuEvHandlerCB) (const EmuEvent *ev);

// `last_tstates` is always 0; used for FUSE compatibility
typedef void (*EmuEvHandlerTpCB) (uint32_t event, void *user_data);

// register new event handler
void emuEvRegisterHandler (EmuEvHandlerCB cb);

// register new event; never returns 0
// `name` is strictly for debugging
uint32_t emuEvRegister (EmuEvHandlerTpCB cb, const char *name);

// get event name by its type
const char *emuEvNameByType (uint32_t type);

// clear all events
void emuEvClear (void);

// pass time, call event handlers
// it is ok to add new events from handlers
// added events with zero timeout will be executed
// on the next call to `emuTimePassed()` with
// non-zero `tskip`
void emuTimePassed (uint32_t tskip);

// remove all events with the given type
void emuEvRemoveAllWithType (uint32_t type);

// remove all events with the given type and userdata
void emuEvRemoveAllWithTypeUData (uint32_t type, void *udata);

// append new event
// tstates is tstates from the current event time
void emuEvAdd (uint32_t tstates, uint32_t type, void *udata);

// peek next event time; returns `EMU_EV_NONE` if there are no events
uint32_t emuEvNextTime (void);

// get next event
// returns zero if there are no more event
int emuEvGet (EmuEvent *ev);


// last time we called event system on the given frame
extern uint32_t emuEvLastFrameCallTS;


#endif
