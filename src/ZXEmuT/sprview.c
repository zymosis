/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include "../libzymosis/zymosis.h"
#include "libvideo/video.h"
#include "sprview.h"
#include "emucommon.h"
#include "emuvars.h"
#include "emuutils.h"
#include "console.h"


// ////////////////////////////////////////////////////////////////////////// //
#define SCREEN_WIDTH   (320)
#define SCREEN_HEIGHT  (240)

int sprviewActive = 0;

// type of the current sprite
uint32_t spr_type = SFLAG_STANDARD;
uint32_t spr_staddr = 0x5800;
int spr_wdt = 4;//6;//10;
int spr_hgt = 64;

static int inaddrpos = -1;

static int evtCtrlKeyDown = 0;
static int evtAltKeyDown = 0;
static int evtShiftKeyDown = 0;

static vt_simple_menu *current_menu = NULL;
static void (*menu_sel_cb) (uint32_t sel) = NULL;
static vt_simple_menu wkmenu;


//==========================================================================
//
//  getByte
//
//==========================================================================
static __attribute__((always_inline)) inline uint8_t getByte (uint32_t addr) {
  return
    addr <= 0xffffU ?
    z80.mem_read(&z80, addr, ZYM_MEMIO_OTHER) :
    zxMemoryBanks[((addr>>14)-4)%(unsigned)emuGetRAMPages()][addr&0x3fffU];
}


//==========================================================================
//
//  getSpritePixel
//
//  the most complex function: get sprite/mask pixel
//  always returns 0 or 1
//
//  for out-of-bounds `x` and `y` 0 returned for sprite bitmap, and
//  1 for mask data
//
//  `cwdt` is sprite width in bytes
//  `hgt` is sprite height in pixels
//
//  `flags` is `SFLAG_xxx` (possibly "or"ed with `SFLAG_GET_MASK`
//  to return mask pixel instead of sprite pixel)
//
//==========================================================================
static uint8_t getSpritePixel (uint32_t pc, int x, int y,
                               uint16_t cwdt, uint16_t hgt, uint32_t flags)
{
  // oob check
  if (x < 0 || y < 0 || x >= cwdt*8 || y >= hgt) return (flags&SFLAG_GET_MASK ? 1 : 0);

  // do we want mask pixel from the sprite without a mask?
  if ((flags&(SFLAG_MASK_PRESENT|SFLAG_GET_MASK)) == SFLAG_GET_MASK) return 1;

  if (flags&SFLAG_UPSIDE_DOWN) y = hgt-y-1;

  // adjust x for lines in reverse byte order
  if ((y&1) && (flags&SFLAG_H_STRIPED)) {
    int xofs = x&7;
    x >>= 3; // div 8
    x = (int)cwdt-x-1;
    x = (x<<3)+xofs;
  }

  // adjust x, if bytes are swapped
  // note that bits are not reversed
  if (((y&1) == 0 && (flags&SFLAG_BSWAP_L0)) ||
      ((y&1) != 0 && (flags&SFLAG_BSWAP_L1)))
  {
    // bytes on this line are swapped, adjust x
    if ((x&0x0f) < 8) {
      // x in the first byte, so move it to the second
      x += 8;
      if (x >= cwdt*8) return 0;
    } else {
      // x in the second byte, so move it to the first
      x -= 8;
      if (x < 0) return 0;
    }
  }

  if (flags&SFLAG_MASK_PRESENT) {
    if (flags&SFLAG_MASK_ILV) {
      // for interleaved mask, adjust size and x
      cwdt *= 2;
      x *= 2;
      if (!!(flags&SFLAG_GET_MASK) != !!(flags&SFLAG_MASK_FIRST)) {
        // skip bitmap or mask
        x += 8;
      }
    } else {
      // for non-interleaved mask, adjust starting address
      if (!!(flags&SFLAG_GET_MASK) != !!(flags&SFLAG_MASK_FIRST)) {
        // skip bitmap or mask
        pc += cwdt*hgt;
      }
    }
  }

  // calculate byte data address, and bit position
  const uint32_t ladr = pc+(uint32_t)y*cwdt+(((uint32_t)x)>>3);
  const uint8_t bitmask = 0x80>>(((uint8_t)x)&7);

  return (getByte(ladr)&bitmask ? 1 : 0);
}


//==========================================================================
//
//  sprDataSize
//
//  `wdt` and `hgt` in pixels
//
//==========================================================================
static __attribute__((unused)) int sprDataSize (const void *spr) {
  const uint8_t *data = (const uint8_t *)spr;
  return (uint32_t)data[0]*(uint32_t)data[1]+2;
}


//==========================================================================
//
//  isAddrEditActive
//
//==========================================================================
static __attribute__((always_inline)) inline int isAddrEditActive (void) {
  return (inaddrpos >= 0);
}


//==========================================================================
//
//  sprviewInit
//
//==========================================================================
void sprviewInit (void) {
}


//==========================================================================
//
//  sprviewSetActive
//
//==========================================================================
void sprviewSetActive (int st) {
  sprviewActive = !!st;
}


//==========================================================================
//
//  drawSpriteAt
//
//  `orflags`: `SFLAG_GET_NORMAL` or `SFLAG_GET_MASK`
//
//==========================================================================
static void drawSpriteAt (uint32_t addr, int x, int y, uint32_t orflags) {
  if (!(spr_wdt|spr_hgt)) return;
  const uint8_t clrset = (orflags & SFLAG_GET_MASK ? 5 : 7);
  for (uint16_t dy = 0; dy < (uint16_t)spr_hgt; ++dy) {
    for (uint16_t dx = 0; dx < (uint16_t)spr_wdt; ++dx) {
      for (uint8_t xofs = 0; xofs < 8; ++xofs) {
        int spx = (int)(dx*8)+(int)xofs;
        int spy = (int)dy;
        uint8_t pix = getSpritePixel(addr, spx, spy,
                                     (uint16_t)spr_wdt, (uint16_t)spr_hgt,
                                     spr_type|orflags);
        putPixel(x+spx, y+spy, (pix ? clrset : 0));
      }
    }
  }
}


//==========================================================================
//
//  drawMaskedSpriteAt
//
//==========================================================================
static void drawMaskedSpriteAt (uint32_t addr, int x, int y) {
  if (!(spr_wdt|spr_hgt)) return;
  for (uint16_t dy = 0; dy < (uint16_t)spr_hgt; ++dy) {
    for (uint16_t dx = 0; dx < (uint16_t)spr_wdt; ++dx) {
      for (uint8_t xofs = 0; xofs < 8; ++xofs) {
        int spx = (int)(dx*8)+(int)xofs;
        int spy = (int)dy;
        uint8_t pix = getSpritePixel(addr, spx, spy,
                                     (uint16_t)spr_wdt, (uint16_t)spr_hgt,
                                     spr_type|SFLAG_GET_NORMAL);
        uint8_t msk = getSpritePixel(addr, spx, spy,
                                     (uint16_t)spr_wdt, (uint16_t)spr_hgt,
                                     spr_type|SFLAG_GET_MASK);
        uint8_t clr = 3;
        switch ((pix ? 1 : 0) | (msk ? 2 : 0)) {
          case 0: clr = 0; break; // pixel:0, mask:0
          case 1: clr = 7; break; // pixel:1, mask:0
          case 2: clr = 1; break; // pixel:0, mask:1
          case 3: clr = 5; break; // pixel:1, mask:1
        }
        putPixel(x+spx, y+spy, clr);
      }
    }
  }
}


//==========================================================================
//
//  skipWholePage
//
//==========================================================================
static void skipWholePage (int dir) {
  const int spw = (int)spr_wdt*(spr_type&SFLAG_MASK_PRESENT ? 16+4 : 8);
  int wasOneY = 0;
  for (int spy = 2; !wasOneY || spy < SCREEN_HEIGHT-(int)spr_hgt; spy += (int)(spr_hgt+6)) {
    wasOneY = 1;
    int wasOneX = 0;
    for (int spx = 2; !wasOneX || spx < SCREEN_WIDTH-spw; spx += spw+6) {
      wasOneX = 1;
      if (dir < 0) spr_staddr -= spr_wdt*spr_hgt; else spr_staddr += spr_wdt*spr_hgt;
      if (spr_type&SFLAG_MASK_PRESENT) {
        if (dir < 0) spr_staddr -= spr_wdt*spr_hgt; else spr_staddr += spr_wdt*spr_hgt;
      }
    }
  }
  if (spr_staddr >= 0x3fffffffU) spr_staddr = 0;
}


//==========================================================================
//
//  drawScreenPhase0
//
//==========================================================================
static void drawScreenPhase0 (void) {
  static char buf[1024];

  drawBar(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 1);

  const int spw = (int)spr_wdt*(spr_type&SFLAG_MASK_PRESENT ? 16+4 : 8);

  uint32_t addr = spr_staddr;

  // draw sprites
  int wasOneY = 0;
  for (int spy = 2; !wasOneY || spy < SCREEN_HEIGHT-(int)spr_hgt; spy += (int)(spr_hgt+6)) {
    wasOneY = 1;
    int wasOneX = 0;
    for (int spx = 2; !wasOneX || spx < SCREEN_WIDTH-spw; spx += spw+6) {
      wasOneX = 1;
      // draw sprite
      if (spr_type & SFLAG_MASK_PRESENT) {
        drawMaskedSpriteAt(addr, spx, spy);
        // draw mask
        drawSpriteAt(addr, spx+spr_wdt*8+4, spy, SFLAG_GET_MASK);
      } else {
        drawSpriteAt(addr, spx, spy, SFLAG_GET_NORMAL);
      }
      addr += spr_wdt*(spr_type&SFLAG_MASK_PRESENT ? 2 : 1)*spr_hgt;
    }
  }


  if (vidScale < 4) {
    if (spr_staddr <= 0xffffU) {
      snprintf(buf, sizeof(buf), "..|%04X", spr_staddr);
    } else {
      snprintf(buf, sizeof(buf), "%02X|%04X", ((spr_staddr>>14)-4)%emuGetRAMPages(), spr_staddr&0x3fffU);
    }
    //sprintf(buf, "addr: #%04X", spr_staddr);
    drawStr6(buf, 320-6*strlen(buf), SCREEN_HEIGHT-8, 13, 0);

    sprintf(buf, "%dx%d", spr_wdt, spr_hgt);
    drawStr6(buf, 320-6*strlen(buf), SCREEN_HEIGHT-16, 6, 0);
  } else {
    if (spr_staddr <= 0xffffU) {
      vt_writef(VID_TEXT_WIDTH-4, VID_TEXT_HEIGHT-1, 15, "%04X", spr_staddr);
      vt_writestrz(VID_TEXT_WIDTH-7, VID_TEXT_HEIGHT-1, "..|", 5);
    } else {
      vt_writef(VID_TEXT_WIDTH-4, VID_TEXT_HEIGHT-1, 15, "%04X", spr_staddr&0x3fffU);
      vt_writestrz(VID_TEXT_WIDTH-5, VID_TEXT_HEIGHT-1, "|", 5);
      vt_writef(VID_TEXT_WIDTH-7, VID_TEXT_HEIGHT-1, 7, "%02X", ((spr_staddr>>14)-4)%emuGetRAMPages());
    }
    sprintf(buf, "%dx%d", spr_wdt, spr_hgt);
    vt_writestrz(VID_TEXT_WIDTH-strlen(buf), VID_TEXT_HEIGHT-2, buf, 15);
  }


  int ty = SCREEN_HEIGHT-8;
  strcpy(buf, "MASK:");
  // print sprite type
  if (spr_type&SFLAG_MASK_PRESENT) {
    if (spr_type&SFLAG_MASK_ILV) strcat(buf, "interleaved,");
    strcat(buf, (spr_type&SFLAG_MASK_FIRST ? "first" : "last"));
  } else {
    strcat(buf, "none");
  }
  drawStr6Outline(buf, 0, ty, 6, 0);
  drawStr6("MASK:", 0, ty, 7, 255);

  ty -= 8;
  strcpy(buf, "SPRT:");
  int needcomma = 0;
  if (spr_type&SFLAG_H_STRIPED) { strcat(buf, "striped"); needcomma = 1; }
  if (spr_type&SFLAG_BSWAP_L0) { if (needcomma) strcat(buf, ","); strcat(buf, "l0swap"); needcomma = 1; }
  if (spr_type&SFLAG_BSWAP_L1) { if (needcomma) strcat(buf, ","); strcat(buf, "l1swap"); needcomma = 1; }
  if (!needcomma) strcat(buf, "linear");
  drawStr6Outline(buf, 0, ty, 6, 0);
  drawStr6("SPRT:", 0, ty, 7, 255);
}


//==========================================================================
//
//  sprviewDraw
//
//==========================================================================
void sprviewDraw (void) {
  //vt_cls(0xb0, 0x01);
  vt_cls(0x20, 0x00);

  drawScreenPhase0();

  if (isAddrEditActive()) {
    int xofs = (VID_TEXT_WIDTH-9)/2;
    int yofs = (VID_TEXT_HEIGHT-3)/2;
    vt_draw_frame(xofs, yofs, 9, 3, 0x50, VID_FRAME_DOUBLE|VID_FRAME_OVERWRITE);
    vt_writechars(xofs+1, yofs+1, 7, ' ', 0x50);
    char buf[32];
    if (spr_staddr <= 0xffffU) {
      snprintf(buf, sizeof(buf), ".. %04X", spr_staddr);
    } else {
      snprintf(buf, sizeof(buf), "%02X %04X", ((spr_staddr>>14)-4)%emuGetRAMPages(), spr_staddr&0x3fffU);
    }
    vt_writestrz(xofs+1, yofs+1, buf, 0x50);
    vt_writeattr(xofs+1+inaddrpos+(inaddrpos >= 2), yofs+1, emuGetCursorColor(0xF0, 0x70));
  }

  if (current_menu) vt_smm_draw(current_menu);
}


//==========================================================================
//
//  doLeft
//
//==========================================================================
static void doLeft (void) {
  if (isAddrEditActive()) {
    if (inaddrpos) --inaddrpos;
    return;
  }
  spr_staddr -= (evtCtrlKeyDown ? spr_wdt : 1);
  if (spr_staddr >= 0x3fffffffU) spr_staddr = 0;
}


//==========================================================================
//
//  doRight
//
//==========================================================================
static void doRight (void) {
  if (isAddrEditActive()) {
    if (inaddrpos < 5) ++inaddrpos;
    return;
  }
  spr_staddr += (evtCtrlKeyDown ? spr_wdt : 1);
}


//==========================================================================
//
//  doUp
//
//==========================================================================
static void doUp (void) {
  if (isAddrEditActive()) {
    if (inaddrpos >= 2) inaddrpos = 0;
    return;
  }
  spr_staddr -= (evtCtrlKeyDown ? spr_wdt*8 : spr_wdt);
  if (spr_staddr >= 0x3fffffffU) spr_staddr = 0;
}


//==========================================================================
//
//  doDown
//
//==========================================================================
static void doDown (void) {
  if (isAddrEditActive()) {
    if (inaddrpos < 2) inaddrpos = 2;
    return;
  }
  spr_staddr += (evtCtrlKeyDown ? spr_wdt*8 : spr_wdt);
}


//==========================================================================
//
//  doPageUp
//
//==========================================================================
static void doPageUp (void) {
  if (isAddrEditActive()) {
    if (spr_staddr > 0xffffU) {
      uint32_t pg = (spr_staddr>>14)-4;
      if (pg > 0) --pg;
      spr_staddr = (pg+4)<<14;
    }
    return;
  }
  if (evtCtrlKeyDown) skipWholePage(-1);
  else spr_staddr -= spr_wdt*spr_hgt/(evtShiftKeyDown ? 2 : 1);
  if (spr_staddr >= 0x3fffffffU) spr_staddr = 0;
}


//==========================================================================
//
//  doPageDown
//
//==========================================================================
static void doPageDown (void) {
  if (isAddrEditActive()) {
    if (spr_staddr <= 0xffffU) {
      spr_staddr = 0x010000U;
    } else {
      uint32_t pg = (spr_staddr>>14)-4;
      if (pg+1 < (uint32_t)emuGetRAMPages()) spr_staddr = (pg+5)<<14;
    }
    return;
  }
  if (evtCtrlKeyDown) skipWholePage(1);
  else spr_staddr += spr_wdt*spr_hgt/(evtShiftKeyDown ? 2 : 1);
  if (spr_staddr >= 0x3fffffffU) spr_staddr = 0;
}


// ////////////////////////////////////////////////////////////////////////// //
static const char *sprTypesMenu[] = {
  "standard",
  "striped",
  "striped, line 0 bytes swapped",
  "striped, line 1 bytes swapped",
  "striped, line 0 and 1 bytes swapped",
  "line 0 bytes swapped",
  "line 1 bytes swapped",
  "line 0 and 1 bytes swapped",
  NULL,
};

static const uint32_t sprTypesMenuFlags[] = {
  SFLAG_STANDARD,
  SFLAG_H_STRIPED,
  SFLAG_H_STRIPED|SFLAG_BSWAP_L0,
  SFLAG_H_STRIPED|SFLAG_BSWAP_L1,
  SFLAG_H_STRIPED|SFLAG_BSWAP_L0|SFLAG_BSWAP_L1,
  SFLAG_BSWAP_L0,
  SFLAG_BSWAP_L1,
  SFLAG_BSWAP_L0|SFLAG_BSWAP_L1,
};


static const char *maskTypesMenu[] = {
  "none",
  "normal, after",
  "normal, before",
  "interleaved, after bmp",
  "interleaved, before bmp",
  NULL,
};

static const uint32_t maskTypesFlags[] = {
  0,
  SFLAG_MASK_PRESENT,
  SFLAG_MASK_PRESENT|SFLAG_MASK_FIRST,
  SFLAG_MASK_PRESENT|SFLAG_MASK_ILV,
  SFLAG_MASK_PRESENT|SFLAG_MASK_FIRST|SFLAG_MASK_ILV,
};


//==========================================================================
//
//  menu_set_type
//
//==========================================================================
static void menu_set_type (uint32_t sel) {
  spr_type = (spr_type&~SFLAG_BITMASK_FOR_TYPE)|sprTypesMenuFlags[sel];
}


//==========================================================================
//
//  menu_set_mask
//
//==========================================================================
static void menu_set_mask (uint32_t sel) {
  spr_type = (spr_type&~SFLAG_BITMASK_FOR_MASK)|maskTypesFlags[sel];
}


//==========================================================================
//
//  write_xte
//
//==========================================================================
static void write_xte (void) {
  FILE *fo = fopen("zsprdump_xte.dula", "a");
  if (!fo) {
    cprintf("\x04""ERROR: cannot open dump file!\n");
    return;
  }
  fprintf(fo, "-- %dx%d\n", spr_wdt, spr_hgt);
  fprintf(fo, "DECLARE SPRITE A_%04X\n", spr_staddr&0xFFFF);
  fprintf(fo, "  LEGEND . EMPTY\n");
  fprintf(fo, "  LEGEND # FILL\n");
  fprintf(fo, "  LEGEND : MASK\n");
  fprintf(fo, "  LEGEND ! FILLNOMASK\n");
  fprintf(fo, "  FRAME\n");
  for (uint16_t dy = 0; dy < (uint16_t)spr_hgt; ++dy) {
    fprintf(fo, "    ");
    for (uint16_t dx = 0; dx < (uint16_t)spr_wdt; ++dx) {
      for (uint8_t xofs = 0; xofs < 8; ++xofs) {
        int spx = (int)(dx*8)+(int)xofs;
        int spy = (int)dy;
        uint8_t bmp = getSpritePixel(spr_staddr, spx, spy, (uint16_t)spr_wdt, (uint16_t)spr_hgt,
                                     spr_type|SFLAG_GET_NORMAL);
        uint8_t msk = getSpritePixel(spr_staddr, spx, spy, (uint16_t)spr_wdt, (uint16_t)spr_hgt,
                                     spr_type|SFLAG_GET_MASK);
        if (msk) {
          fprintf(fo, "%c", (bmp ? '#' : ':'));
        } else {
          fprintf(fo, "%c", (bmp ? '!' : '.'));
        }
      }
    }
    fputc('\n', fo);
  }
  fprintf(fo, "  END FRAME\n");
  fprintf(fo, "END SPRITE\n\n");
  fclose(fo);
  cprintf("XTE sprite dumped.\n");
}


//==========================================================================
//
//  write_urf
//
//==========================================================================
static void write_urf (void) {
  FILE *fo = fopen("zsprdump_urf.f", "a");
  if (!fo) {
    cprintf("\x04""ERROR: cannot open dump file!\n");
    return;
  }
  fprintf(fo, "( wdt) %d ( hgt) %d SPRITE A_%04X\n", spr_wdt, spr_hgt, spr_staddr&0xFFFF);
  fprintf(fo, "  ;; LEGEND . EMPTY\n");
  fprintf(fo, "  ;; LEGEND # FILL\n");
  if (spr_type & SFLAG_MASK_PRESENT) {
    fprintf(fo, "  ;; LEGEND : MASK\n");
    fprintf(fo, "  ;; LEGEND ! FILLNOMASK\n");
  }
  fprintf(fo, "  ;;;;;;;; data ;;;;;;;;\n");
  for (uint16_t dy = 0; dy < (uint16_t)spr_hgt; ++dy) {
    fprintf(fo, "  ROW ");
    for (uint16_t dx = 0; dx < (uint16_t)spr_wdt; ++dx) {
      for (uint8_t xofs = 0; xofs < 8; ++xofs) {
        int spx = (int)(dx*8)+(int)xofs;
        int spy = (int)dy;
        uint8_t bmp = getSpritePixel(spr_staddr, spx, spy, (uint16_t)spr_wdt, (uint16_t)spr_hgt,
                                     spr_type|SFLAG_GET_NORMAL);
        if (spr_type & SFLAG_MASK_PRESENT) {
          uint8_t msk = getSpritePixel(spr_staddr, spx, spy, (uint16_t)spr_wdt, (uint16_t)spr_hgt,
                                       spr_type|SFLAG_GET_MASK);
          if (msk) {
            fprintf(fo, "%c", (bmp ? '!' : '.'));
          } else {
            fprintf(fo, "%c", (bmp ? '#' : ':'));
          }
        } else {
          fprintf(fo, "%c", (bmp ? '#' : '.'));
        }
      }
    }
    fputc('\n', fo);
  }
  fprintf(fo, "END-SPRITE\n\n");
  fclose(fo);
  cprintf("UrF sprite dumped.\n");
}


//==========================================================================
//
//  write_asm_norm
//
//==========================================================================
static void write_asm_norm (void) {
  FILE *fo = fopen("zsprdump_norm.zas", "a");
  if (!fo) {
    cprintf("\x04""ERROR: cannot open dump file!\n");
    return;
  }
  fprintf(fo, ";; %dx%d\n", spr_wdt, spr_hgt);
  for (int phase = 0; phase < 2; ++phase) {
    if (phase == 0) fprintf(fo, ";--bitmap--\n"); else fprintf(fo, ";--mask--\n");
    for (uint16_t dy = 0; dy < (uint16_t)spr_hgt; ++dy) {
      fprintf(fo, "  defb ");
      for (uint16_t dx = 0; dx < (uint16_t)spr_wdt; ++dx) {
        if (dx) fprintf(fo, ",%%"); else fprintf(fo, "%%");
        for (uint8_t xofs = 0; xofs < 8; ++xofs) {
          int spx = (int)(dx*8)+(int)xofs;
          int spy = (int)dy;
          uint8_t pix = getSpritePixel(spr_staddr, spx, spy, (uint16_t)spr_wdt, (uint16_t)spr_hgt,
                                       spr_type|(phase ? SFLAG_GET_MASK : SFLAG_GET_NORMAL));
          fprintf(fo, "%c", (pix ? '1' : '0'));
        }
      }
      fputc('\n', fo);
    }
  }
  fprintf(fo, "\n\n");
  fclose(fo);
  cprintf("\"normasm\" sprite dumped.\n");
}


//==========================================================================
//
//  sprviewKeyEvent
//
//==========================================================================
int sprviewKeyEvent (SDL_KeyboardEvent *key) {
  if (current_menu) {
    int mrs = vt_smm_process_key(current_menu, key);
    switch (mrs) {
      case VID_SMM_ESCAPE:
        vt_smm_deinit(current_menu);
        current_menu = NULL;
        return 1;
      case VID_SMM_NOTMINE:
        break;
      case VID_SMM_EATEN:
        return 1;
      case VID_SMM_SELECTED:
        if (menu_sel_cb) {
          const uint32_t cy = current_menu->cursor_y;
          vt_smm_deinit(current_menu);
          current_menu = NULL;
          menu_sel_cb(cy);
        } else {
          vt_smm_deinit(current_menu);
          current_menu = NULL;
        }
        return 1;
    }
    return 1;
  }

  if (key->type != SDL_KEYDOWN) return 1;

  evtCtrlKeyDown = !!(key->keysym.mod&KMOD_CTRL);
  evtAltKeyDown = !!(key->keysym.mod&KMOD_ALT);
  evtShiftKeyDown = !!(key->keysym.mod&KMOD_SHIFT);

  if (isAddrEditActive() && key->keysym.unicode > 32 && key->keysym.unicode < 127) {
    int d = digitInBase((char)key->keysym.unicode, 16);
    if (d >= 0) {
      if (inaddrpos < 2) {
        uint32_t pg = (spr_staddr > 0xffffU ? (spr_staddr>>14)-4 : 0);
        pg &= (inaddrpos ? 0xf0U : 0x0fU);
        pg |= ((unsigned)d)<<((1-inaddrpos)*4);
        spr_staddr = (spr_staddr&0x3fffU)|((pg+4)<<14);
      } else {
        if (spr_staddr > 0xffffU && inaddrpos == 2 && d > 3) return 1;
        if (spr_staddr > 0xffffU) {
          uint32_t pg = (spr_staddr>>14);
          spr_staddr &= ~(0x000fU<<((5-inaddrpos)*4));
          spr_staddr |= ((unsigned)d)<<((5-inaddrpos)*4);
          spr_staddr |= pg<<14;
        } else {
          spr_staddr &= ~(0x000fU<<((5-inaddrpos)*4));
          spr_staddr |= ((unsigned)d)<<((5-inaddrpos)*4);
        }
      }
      if (inaddrpos < 5) ++inaddrpos;
      return 1;
    }
  }

  switch (key->keysym.sym) {
    case SDLK_ESCAPE:
           if (isAddrEditActive()) inaddrpos = -1;
      else sprviewSetActive(0);
      return 1;

    case SDLK_RETURN:
      inaddrpos = -1;
      return 1;

    case SDLK_DELETE: case SDLK_KP_PERIOD:
      if (isAddrEditActive() && inaddrpos < 2) {
        spr_staddr &= 0xffffU;
      }
      return 1;

    case SDLK_F6:
      if (!isAddrEditActive()) {
        if ((evtShiftKeyDown|evtAltKeyDown)) {
          vt_smm_init(&wkmenu, "Mask type", maskTypesMenu);
          menu_sel_cb = &menu_set_mask;
          for (size_t f = 0; f < sizeof(maskTypesFlags)/sizeof(maskTypesFlags[0]); ++f) {
            if ((spr_type&SFLAG_BITMASK_FOR_MASK) == maskTypesFlags[f]) {
              wkmenu.cursor_y = f;
              break;
            }
          }
          current_menu = &wkmenu;
        } else {
          vt_smm_init(&wkmenu, "Sprite type", sprTypesMenu);
          menu_sel_cb = &menu_set_type;
          for (size_t f = 0; f < sizeof(sprTypesMenuFlags)/sizeof(sprTypesMenuFlags[0]); ++f) {
            if ((spr_type&SFLAG_BITMASK_FOR_TYPE) == sprTypesMenuFlags[f]) {
              wkmenu.cursor_y = f;
              break;
            }
          }
          current_menu = &wkmenu;
        }
      }
      return 1;

    case SDLK_LEFT: case SDLK_KP4: doLeft(); return 1;
    case SDLK_RIGHT: case SDLK_KP6: doRight(); return 1;
    case SDLK_UP: case SDLK_KP8: doUp(); return 1;
    case SDLK_DOWN: case SDLK_KP2: doDown(); return 1;
    case SDLK_PAGEUP: case SDLK_KP9: case SDLK_o: doPageUp(); return 1;
    case SDLK_PAGEDOWN: case SDLK_KP3: case SDLK_p: doPageDown(); return 1;
    case SDLK_BACKSPACE: if (isAddrEditActive()) doLeft(); return 1;

    case SDLK_HOME: case SDLK_KP7:
      if (isAddrEditActive()) inaddrpos = (inaddrpos > 2 ? 2 : 0);
      return 1;
    case SDLK_END: case SDLK_KP1:
      if (isAddrEditActive()) inaddrpos = (inaddrpos < 2 ? 2 : 5);
      return 1;

    case SDLK_KP_MINUS:
      if (isAddrEditActive()) return 1;
      spr_wdt -= (evtCtrlKeyDown ? 2 : 1);
      if (spr_wdt < 1) spr_wdt = 1;
      break;
    case SDLK_KP_PLUS:
      if (isAddrEditActive()) return 1;
      spr_wdt += (evtCtrlKeyDown ? 2 : 1);
      if (spr_wdt > 40) spr_wdt = 40;
      break;

    case SDLK_q:
      if (isAddrEditActive()) return 1;
      spr_hgt -= (evtCtrlKeyDown || evtShiftKeyDown ? 1 : 8);
      if (spr_hgt < 1) spr_hgt = 1;
      break;
    case SDLK_a:
      if (isAddrEditActive()) return 1;
      spr_hgt += (evtCtrlKeyDown || evtShiftKeyDown ? 1 : 8);
      if (spr_hgt > SCREEN_HEIGHT) spr_hgt = SCREEN_HEIGHT;
      break;

    case SDLK_g:
      if (!isAddrEditActive()) inaddrpos = 2;
      return 1;

    case SDLK_u:
      spr_type ^= SFLAG_UPSIDE_DOWN;
      return 1;

    case SDLK_x:
      if (isAddrEditActive()) return 1;
      if (evtShiftKeyDown && !evtCtrlKeyDown && !evtAltKeyDown) {
        write_xte();
      }
      return 1;
    case SDLK_w:
      if (isAddrEditActive()) return 1;
      if (evtShiftKeyDown && !evtCtrlKeyDown && !evtAltKeyDown) {
        write_urf();
      }
      return 1;
    case SDLK_n:
      if (isAddrEditActive()) return 1;
      if (evtShiftKeyDown && !evtCtrlKeyDown && !evtAltKeyDown) {
        write_asm_norm();
      }
      return 1;

    case SDLK_F9:
      if (isAddrEditActive()) return 1;
      if (evtAltKeyDown == 0) {
        int v = optMaxSpeed;
        emuSetMaxSpeed(!v);
      }
      return 1;

    case SDLK_BACKQUOTE:
      conVisible = 1;
      return 1;

    case SDLK_F1:
      if (!isAddrEditActive() && evtAltKeyDown) {
        int v = ((optPaused&PAUSE_PERMANENT_MASK) == 0);
        emuSetPaused(v ? PAUSE_PERMANENT_SET : PAUSE_PERMANENT_RESET);
        return 1;
      }
      key->keysym.mod |= KMOD_ALT; /* hack! */
      /* fallthrough */
    case SDLK_h:
      if ((key->keysym.mod&KMOD_ALT) != 0) {
        vt_smm_init_textview(&wkmenu, "Small help",
          (const char *[]){
            "\x01""F6", "  select sprite mode",
            "\x01""M-F6", "  select mask mode",
            "\x01""G", "  input address",
            "\x01""Q/A", "  change sprite height (shift for smaller steps)",
            "\x01""+/-", "  change sprite width",
            "\x01""S-W", "  write (append) sprite data for UrForth parsing",
            "\x01""S-N", "  write (append) sprite data as urasm source (bitmap, then mask)",
            "\x01""S-X", "  write (append) sprite data as XTE compiler data",
            NULL
          });
        menu_sel_cb = NULL;
        current_menu = &wkmenu;
      }
      return 1;

    default: break;
  }

  return 1;
}
