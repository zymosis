/***************************************************************************
 *
 *  Zebra Project / ������ "�����"
 *  YM/AY emulator, v0.0.2
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef _ZEBRA_AY_H_
#define _ZEBRA_AY_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


////////////////////////////////////////////////////////////////////////////////
typedef struct ZebraAY_t ZebraAY; // ������� �������� ������


////////////////////////////////////////////////////////////////////////////////
enum {
  ZEBRA_AY_MAX_VOLUME = 20760, // ������������ ���������� �������� ������
  // ������� �� ���������
  ZEBRA_AY_DEFAULT_STEREO = 1,
  ZEBRA_AY_DEFAULT_VOLUME = 1
};


////////////////////////////////////////////////////////////////////////////////
// ������� �������������
extern const char *ZEBRA_AY_STEREO_TABLE_NAMES[9];
// ������� ����������
extern const char *ZEBRA_AY_VOLUME_TABLE_NAMES[14];

extern int zebraVolume; // 0..32767


////////////////////////////////////////////////////////////////////////////////
extern ZebraAY *zebraAYNew (void);

extern void zebraAYFree (ZebraAY *ay);

// srate: � �������-�-�������
// freq: � ������
extern int zebraAYInit (ZebraAY *ay, int srate, int freq);

// ������������������ ���
// srate: � �������-�-�������
// freq: � ������
// ��������! �������� `ay->fAYTick`, ��� ��� ����������, ���� ����!
extern int zebraAYReinit (ZebraAY *ay, int srate, int freq);

// ��������! �������� `ay->fAYTick`, ��� ��� ����������, ���� ����!
extern int zebraAYSetFreq (ZebraAY *ay, int freq);

// ������ ������� ���������
// �� ������� `ay->fAYTick`
extern int zebraAYSetSampleRate (ZebraAY *ay, int srate);

// ��������! �������� `ay->fAYTick`, ��� ��� ����������, ���� ����!
extern void zebraAYReset (ZebraAY *ay);

// vt ������ ��������� �� ����� 32 ���������
extern void zebraAYSetVolumeTable (ZebraAY *ay, const uint16_t *vt);

// st ������ ��������� 6 ���������: A_left,A_right,B_left,B_right,C_left,C_right [0..255]
extern void zebraAYSetStereoTable (ZebraAY *ay, const uint8_t *st);

// �����, ���� �������� ����� ���̣������ ������
// ���� ����� -- 4 �����, 2 �� ����� �����, 2 �� ������, �� ������
extern void zebraAYSetSoundBuffer (ZebraAY *ay, void *buffer);

// �����, ���� �������� ����� ���̣������ �������� ���������
// �� 16 ���� �� ���� ����� (��� �������� ������������)
extern void zebraAYSetRegBuffer (ZebraAY *ay, void *buffer);

// ���������, ��� ����� ������� � ����� ������� ��� ���

// �������� �������� � ������������ �������
// �������� ������� �� ��������
// �� ����� ������������? %-)
extern void zebraAYWriteReg (ZebraAY *ay, uint8_t reg, uint8_t val);

// ��������� �������� �� ������������� ��������
// �������� ������� �� ��������
extern uint8_t zebraAYReadReg (ZebraAY *ay, uint8_t reg);

// �������� ����������� ���������� �����
// ������� �������
extern void zebraAYSelectReg (ZebraAY *ay, uint8_t reg);

// ��������� �������� �� ��������� ��������
extern uint8_t zebraAYRead (ZebraAY *ay);

// �������� �������� � �������� �������
extern void zebraAYWrite (ZebraAY *ay, uint8_t val);

extern int zebraAYGetChannelEnabled (ZebraAY *ay, int idx);

extern void zebraAYSetChannelEnabled (ZebraAY *ay, int idx, int v);

// ��������� tickCnt ����� (�� �����������!)
// �������� ��� ����������� ����������
// ���� ������ ������� ��� ������ ��������� �� ������,
// �� ������� � ���� �� �������� -- ��� �������� �� �����
// ������, ���� ��������� ���������������� ������ -- NULL,
// �� ���� ������ ������ �� �����
// ���������� ���������� �����ң��� �� ��� ����� �������
extern int zebraAYDoTicks (ZebraAY *ay, int tickCnt);

// ��� ��������� ����� �������� � ������ ������� �����,
// ������ ����� �� ����������� %-)
// �������, ��� ����� ������� DoAY8Steps(),
// ���� fAYTick �� ���������� ������� fAYFreq
// ����������, DoAY8Steps() ����� �� ����
// �������� ������
// ���������� ���������� �����ң��� �� ��� ����� �������
extern int zebraAYNewFrame (ZebraAY *ay);


extern int zebraAYWillGenSampleOnNextTick (ZebraAY *ay);


#ifdef __cplusplus
}
#endif
#endif
