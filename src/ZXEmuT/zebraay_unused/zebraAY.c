/***************************************************************************
 *
 *  Zebra Project / ������ "�����"
 *  YM/AY emulator, v0.0.2
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
/*
 * ������� ���������� (����� FUSE) ��������������
 * �� UnrealSpeccy by STM. FUSE ����� �� FUSE. %-)
 */
#include "zebraAY.h"

#include <stdlib.h>
#include <stdint.h>
#include <string.h>


int zebraVolume = ZEBRA_AY_MAX_VOLUME;

// ������� �������������
const char *ZEBRA_AY_STEREO_TABLE_NAMES[9] = {"MONO","ABC","ACB","BAC","BCA","CAB","CBA","UNREAL","K8"};

// A_left,A_right,B_left,B_right,C_left,C_right [0..255]
static const uint8_t ZEBRA_AY_STEREO_TABLES[9][6] = {
    {168,168, 168,168, 168,168},  // MONO
    {255, 25, 168,168,  25,255},  // ABC
    {255, 25,  25,255, 168,168},  // ACB
    {168,168, 255, 25,  25,255},  // BAC
    { 25,255, 255, 25, 168,168},  // BCA
    {168,168,  25,255, 255, 25},  // CAB
    { 25,255, 168,168, 255, 25},  // CBA
    {229, 51, 168,168,  51,229},  // UNREAL
    {255,  0, 168,168, 255,  0}}; // K8

// ������� ����������
const char *ZEBRA_AY_VOLUME_TABLE_NAMES[14] = {
  "UnrealSpeccy AY",
  "UnrealSpeccy YM",
  "bulba AY",
  "bulba YM",
  "DelphiSpec YM",
  "X128/YASE AY",
  "FUSE AY",
  "ZXMAK YM",
  "Lion17 sch.0 AY",
  "Lion17 sch.1 AY",
  "ZX32 AY",
  "RealSpec AY",
  "RealSpec YM",
  "R80 AY"
};

static const uint16_t ZEBRA_AY_VOLUME_TABLES[14][32] = {
  // UnrealSpeccy AY
  {0x0000,0x0340,0x04C0,0x06F2,0x0A44,0x0F13,0x1510,0x227E,
   0x289F,0x414E,0x5B21,0x7258,0x905E,0xB550,0xD7A0,0xFFFF,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000},
  // UnrealSpeccy YM
  {0x0000,0x0000,0x00EF,0x01D0,0x0290,0x032A,0x03EE,0x04D2,
   0x0611,0x0782,0x0912,0x0A36,0x0C31,0x0EB6,0x1130,0x13A0,
   0x1751,0x1BF5,0x20E2,0x2594,0x2CA1,0x357F,0x3E45,0x475E,
   0x5502,0x6620,0x7730,0x8844,0xA1D2,0xC102,0xE0A2,0xFFFF},
  // bulba AY
  {0x0000,0x1111,0x2222,0x3333,0x4444,0x5555,0x6666,0x7777,
   0x8888,0x9999,0xAAAA,0xBBBB,0xCCCC,0xDDDD,0xEEEE,0xFFFF,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000},
  // bulba YM
  {0x0000,0x0000,0x041A,0x08D4,0x11A8,0x1A7B,0x234F,0x2C23,
   0x34F7,0x3DCB,0x469F,0x4F72,0x5846,0x611A,0x69EE,0x72C2,
   0x7B96,0x8469,0x8D3D,0x9611,0x9EE5,0xA7B9,0xB08D,0xB960,
   0xC234,0xCB08,0xD3DB,0xDCB0,0xE584,0xEE57,0xF72B,0xFFFF},
  // DelphiSpec YM
  {0x0136,0x0170,0x01B5,0x0208,0x026A,0x02DF,0x0369,0x040E,
   0x04D2,0x05BB,0x06CF,0x0818,0x099F,0x0B6F,0x0D97,0x1026,
   0x1332,0x16D0,0x1B1D,0x203A,0x264D,0x2D85,0x361A,0x404D,
   0x4C6C,0x5AD4,0x6BF3,0x804D,0x987C,0xB53B,0xD764,0xFFFF},
  // X128/YASE AY
  {0x0000,0x0200,0x0500,0x0700,0x0A00,0x0C00,0x1200,0x1600,
   0x1D00,0x2200,0x2600,0x2C00,0x3F00,0x3F00,0x3F00,0xFF00,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000},
  // FUSE AY
  {0x0000,0x0385,0x053D,0x0770,0x0AD7,0x0FD5,0x15B0,0x230C,
   0x2B4C,0x43C1,0x5A4B,0x732F,0x9204,0xAFF1,0xD921,0xFFFF,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000},
  // ZXMAK YM
  {0x0000,0x0000,0x00F8,0x01C2,0x029E,0x033A,0x03F2,0x04D7,
   0x0610,0x077F,0x090A,0x0A42,0x0C3B,0x0EC2,0x1137,0x13A7,
   0x1750,0x1BF9,0x20DF,0x2596,0x2C9D,0x3579,0x3E55,0x4768,
   0x54FF,0x6624,0x773B,0x883F,0xA1DA,0xC0FC,0xE094,0xFFFF},
  // Lion17 sch.0 AY
  {0x0000,0x0201,0x033C,0x04D7,0x0783,0x0CA6,0x133E,0x2395,
   0x2868,0x45D3,0x606A,0x76EA,0x97BC,0xB8A5,0xDC51,0xFFFF,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000},
  // Lion17 sch.1 AY
  {0x0000,0x01FD,0x0CA0,0x12FD,0x18DA,0x1F1E,0x2515,0x2C0A,
   0x395C,0x4ADC,0x6468,0x777F,0x9552,0xBCCA,0xDE18,0xFFFF,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000},
  // ZX32 AY
  {0x0000,0x1249,0x1861,0x1E79,0x2492,0x2AAA,0x30C2,0x3CF3,
   0x4924,0x5555,0x679E,0x7FFF,0x9860,0xB0C2,0xD554,0xFFFF,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000},
  // RealSpec AY
  {0x0000,0x0364,0x0500,0x0700,0x0A80,0x1000,0x1600,0x2300,
   0x2A00,0x4300,0x5C00,0x7300,0x9180,0xB400,0xD800,0xFFFF,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000},
  // RealSpec YM
  {0x0000,0x0000,0x00F8,0x01C2,0x029E,0x033A,0x03F2,0x04D7,
   0x0610,0x077F,0x090A,0x0A42,0x0C3B,0x0EC2,0x1137,0x13A7,
   0x1750,0x1BF9,0x20DF,0x2596,0x2C9D,0x3579,0x3E55,0x4768,
   0x54FF,0x6624,0x773B,0x883F,0xA1DA,0xC0FC,0xE094,0xFFFF},
  // R80 AY
  {0x0000,0x0201,0x033C,0x04D7,0x0783,0x0CA6,0x133E,0x2396,
   0x2868,0x45D4,0x606A,0x76EA,0x97BC,0xB8A6,0xDC52,0xFFFF,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
   0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000}};


struct ZebraAY_t {
  // ��������������
  int fSampleRate; // ������� ������� (������� �� �������)
  int fAYFreq; // ������� ���� (� ������)
  // �������� � �����
  int fAYTick; // ������� ��� ����
  uint8_t fSelectedReg; // ������� ��� Write()/Read()
  uint8_t fRegs[16]; // �������� ����
  // ������������ ������� ���������� � ���������������
  int fVolumeTable[32];
  int fStereoTable[6];
  // ������� ��������� ��� ������� �� ������� � �������
  int fVolumes[6][32];
  // �������
  int fToneLimit[3]; // ����� �����
  int fNoiseLimit; // ����� �������
  int fEnvLimit; // ����� ��������
  // ���������� �ޣ�����
  int fToneCnt[3]; // �����
  int fNoiseCnt; // �������
  int fEnvCnt; // ��������
  // ������� ���������� ��������
  int fEnv; // ������� ������� ��������
  int fEnvDelta; // ���������� ������ ��������
  // �������� ����� �������
  int fRNG;
  // ��������� ���� � ����
  int fToneState[3]; // ���� (bool)
  int fNoiseState; // ������� (bool)
  // ���������� �������� ��� ������ (������������ � 0 � ������ ����� �������)
  int fMixL; // ����� �����
  int fMixR; // ������ �����
  // ������� ���������, ���� �� ������� �����
  // (�.�. ������� �������� ������ �������� � fMixL � fMixR)
  int fMixCnt;
  // �� ������ ��� ���� ������������� �� fSampleRate
  // ��� ������ ������ ������ fAYFreq, �� ���� ������������ �����
  int fTickFrac;
  // ����������� ��������� �������� ������, ���� ��� ��� ���� ������
  // �� ���������; ������� �� ������ ��������� � ���������������
  int fMaxLevel;
  // �������������� �������� (����� �������� �� ����� � Do8Steps())
  // ���� R7, ���������� �� ����������
  int fDisableTone[3]; // bool
  int fDisableNoise[3]; // bool
  // ���� 4 �� R8/R9/R10
  int fUseEnvelope[3]; // bool
  // ���� 0-3 �� R8/R9/R10, ���������� ��� �������
  int fToneVolumes[3];
  // �����, ���� PutSample() ����� ���̣������ ������
  // ���� ����� -- 4 �����, 2 �� ����� �����, 2 �� ������
  uint8_t *fSoundBuffer;
  // �����, ���� PutSample() ����� ���̣������ �������� ���������
  // �� 16 ���� �� ���� ����� (��� �������� ������������)
  uint8_t *fRegBuffer;
  // ����� �� �������� ���� ��� ������?
  int fChannelEnabled[3]; // bool
};


////////////////////////////////////////////////////////////////////////////////
ZebraAY *zebraAYNew (void) {
  return calloc(1, sizeof(ZebraAY));
}


void zebraAYFree (ZebraAY *ay) {
  if (ay != NULL) free(ay);
}


////////////////////////////////////////////////////////////////////////////////
// ��������� fVolumes ��� ����� ������� fVolumeTable � fStereoTable
void zebraAYBuildVolumeTable (ZebraAY *ay) {
  int f, vol = zebraVolume;
  //
  if (vol < 0) vol = 0; else if (vol > 32767) vol = 32767;
  for (int j = 0; j < 6; ++j) {
    for (int i = 0; i < 32; ++i) {
      ay->fVolumes[j][i] =
        //Int64(ay->fVolumeTable[i])*ZEBRA_AY_MAX_VOLUME div 65535*ay->fStereoTable[j] div 255;
        //(((double)ay->fVolumeTable[i]*ZEBRA_AY_MAX_VOLUME/65535.0)*((double)ay->fStereoTable[j]/255.0));
        ((double)ay->fVolumeTable[i]*vol*ay->fStereoTable[j])/(65535.0*255.0);
      if (ay->fVolumes[j][i] < 0) abort();
    }
  }
  //
  ay->fMaxLevel = ay->fVolumes[0][31]+ay->fVolumes[2][31]+ay->fVolumes[4][31];
  f = ay->fVolumes[1][31]+ay->fVolumes[3][31]+ay->fVolumes[5][31];
  if (f > ay->fMaxLevel) ay->fMaxLevel = f;
  if (ay->fMaxLevel < 1) ay->fMaxLevel = 1;
  // ����������� ������ ���������
  for (int i = 0; i < 14; ++i) {
    uint8_t p = ay->fRegs[i];
    //
    zebraAYWriteReg(ay, i, p^1);
    zebraAYWriteReg(ay, i, p);
  }
}


// ��������� ����� �� ������ fMixL, fMixR, fMixCnt, ������� ��� � �����
// ������� � ����� ���������
// �������� ���������� �������
void zebraAYPutSample (ZebraAY *ay) {
  // ������
  if (ay->fSoundBuffer != NULL) {
    int t, vol = zebraVolume;
    //
    if (vol < 0) vol = 0; else if (vol > 32767) vol = 32767;
    // �� ������ ������ ��������
    if (ay->fMixCnt < 1) { ay->fMixL = ay->fMixR = 0; ay->fMixCnt = 1; }
    // ����� �����
    t = (((((double)ay->fMixL/ay->fMixCnt)/ay->fMaxLevel)-0.0)*vol);
    if (t <-32768 || t > 32767) abort(); // ����� �����
    *ay->fSoundBuffer++ = t&0xff;
    *ay->fSoundBuffer++ = (t>>8)&0xff;
    // ������ �����
    t = (((((double)ay->fMixR/ay->fMixCnt)/ay->fMaxLevel)-0.0)*vol);
    if (t <-32768 || t > 32767) abort(); // ����� �����
    *ay->fSoundBuffer++ = t&0xff;
    *ay->fSoundBuffer++ = (t>>8)&0xff;
  }
  // ��������
  if (ay->fRegBuffer != NULL) {
    for (int f = 0; f < 16; ++f) *ay->fRegBuffer++ = ay->fRegs[f];
  }
  // ������� ������� � �ޣ����
  ay->fMixL = ay->fMixR = 0;
  ay->fMixCnt = 0;
}


////////////////////////////////////////////////////////////////////////////////
// srate: � �������-�-�������
// freq: � ������
int zebraAYInit (ZebraAY *ay, int srate, int freq) {
  if (ay != NULL && srate > 0 && srate <= 190000 && freq >= srate*8 && freq <= 134217727) {
    memset(ay, 0, sizeof(*ay));
    ay->fChannelEnabled[0] = ay->fChannelEnabled[1] = ay->fChannelEnabled[2] = 1;
    zebraAYSetVolumeTable(ay, ZEBRA_AY_VOLUME_TABLES[ZEBRA_AY_DEFAULT_VOLUME]);
    zebraAYSetStereoTable(ay, ZEBRA_AY_STEREO_TABLES[ZEBRA_AY_DEFAULT_STEREO]);
    return zebraAYReinit(ay, srate, freq);
  }
  return -1;
}


void zebraAYReset (ZebraAY *ay) {
  if (ay != NULL) {
    ay->fAYTick = 0;
    ay->fTickFrac = 0;
    ay->fMixL = ay->fMixR = 0;
    ay->fMixCnt = 0;
    ay->fToneLimit[0] = ay->fToneLimit[1] = ay->fToneLimit[2] = 1;
    ay->fNoiseLimit = 0;
    ay->fEnvLimit = 0;
    ay->fToneCnt[0] = ay->fToneCnt[1] = ay->fToneCnt[2] = 0;
    ay->fNoiseCnt = 0;
    ay->fEnvCnt = 0;
    ay->fEnv = 0;
    ay->fEnvDelta = 0;
    ay->fRNG = 1;
    ay->fToneState[0] = ay->fToneState[1] = ay->fToneState[2] = 0;
    ay->fNoiseState = 0;
    ay->fDisableTone[0] = ay->fDisableTone[1] = ay->fDisableTone[2] = 1;
    ay->fDisableNoise[0] = ay->fDisableNoise[1] = ay->fDisableNoise[2] = 1;
    memset(ay->fRegs, 0, 16);
    ay->fRegs[7] = 0xff;
  }
}


// ������������������ ���
// srate: � �������-�-�������
// freq: � ������
int zebraAYReinit (ZebraAY *ay, int srate, int freq) {
  if (ay != NULL && srate > 0 && srate <= 190000 && freq >= srate*8 && freq <= 134217727) {
    ay->fSampleRate = srate;
    ay->fAYFreq = freq;
    zebraAYReset(ay);
    return 0;
  }
  return -1;
}


// ������ ������� ���������
int zebraAYSetSampleRate (ZebraAY *ay, int srate) {
  if (ay && srate > 0 && srate <= 190000 && ay->fAYFreq >= srate*8 && ay->fAYFreq <= 134217727) {
    ay->fSampleRate = srate;
    return 0;
  }
  return 1;
}


int zebraAYSetFreq (ZebraAY *ay, int freq) {
  if (ay != NULL && freq >= ay->fSampleRate*8 && freq <= 134217727) {
    ay->fAYFreq = freq;
    zebraAYReset(ay);
    return 0;
  }
  return -1;
}


void zebraAYSetVolumeTable (ZebraAY *ay, const uint16_t *vt) {
  if (vt[31] == 0) {
    // AY
    for (int f = 0; f < 16; ++f) ay->fVolumeTable[f*2+0] = ay->fVolumeTable[f*2+1] = vt[f];
  } else {
    // YM
    for (int f = 0; f < 32; ++f) ay->fVolumeTable[f] = vt[f];
  }
  zebraAYBuildVolumeTable(ay);
}


void zebraAYSetStereoTable (ZebraAY *ay, const uint8_t *st) {
  for (int f = 0; f < 6; ++f) ay->fStereoTable[f] = st[f];
  zebraAYBuildVolumeTable(ay);
}


void zebraAYSetSoundBuffer (ZebraAY *ay, void *buffer) {
  ay->fSoundBuffer = (uint8_t *)buffer;
}


void zebraAYSetRegBuffer (ZebraAY *ay, void *buffer) {
  ay->fRegBuffer = (uint8_t *)buffer;
}


////////////////////////////////////////////////////////////////////////////////
// �������� �������� � ������������ �������
// �������� ������� �� ��������
// �� ����� ������������? %-)
void zebraAYWriteReg (ZebraAY *ay, uint8_t reg, uint8_t val) {
  switch ((reg &= 0x0f)) {
    case 1: case 3: case 5: case 13: val &= 0x0f; break;
    case 6: case 8: case 9: case 10: val &= 0x1f; break;
  }
  ay->fRegs[reg] = val;
  switch (reg) {
    case 0: case 1: // tone A limit
      ay->fToneLimit[0] = ay->fRegs[0]+256*(ay->fRegs[1]&0x0F);
      if (ay->fToneLimit[0] == 0) ay->fToneLimit[0] = 1; // ��� � ���������
      break;
    case 2: case 3: // tone B limit
      ay->fToneLimit[1] = ay->fRegs[2]+256*(ay->fRegs[3]&0x0F);
      if (ay->fToneLimit[1] == 0) ay->fToneLimit[1] = 1; // ��� � ���������
      break;
    case 4: case 5: // tone C limit
      ay->fToneLimit[2] = ay->fRegs[4]+256*(ay->fRegs[5]&0x0F);
      if (ay->fToneLimit[2] == 0) ay->fToneLimit[2] = 1; // ��� � ���������
      break;
    case 6: // noise limit
      ay->fNoiseLimit = (ay->fRegs[6]&0x1f)*2; // "��������������" --> "���������"
      if (ay->fNoiseLimit == 0) ay->fNoiseLimit = 1; // � ��� �� ��������
      break;
    case 7: // mixer control
      ay->fDisableTone[0] = ((val&0x01) != 0);
      ay->fDisableTone[1] = ((val&0x02) != 0);
      ay->fDisableTone[2] = ((val&0x04) != 0);
      ay->fDisableNoise[0] = ((val&0x08) != 0);
      ay->fDisableNoise[1] = ((val&0x10) != 0);
      ay->fDisableNoise[2] = ((val&0x20) != 0);
      break;
    case 8: // volume A control
      ay->fUseEnvelope[0] = ((val&0x10) != 0);
      if ((val &= 0x0f) != 0) val = val*2+1;
      ay->fToneVolumes[0] = val;
      break;
    case 9: // volume B control
      ay->fUseEnvelope[1] = ((val&0x10) != 0);
      if ((val &= 0x0f) != 0) val = val*2+1;
      ay->fToneVolumes[1] = val;
      break;
    case 10: // volume C control
      ay->fUseEnvelope[2] = ((val&0x10) != 0);
      if ((val &= 0x0f) != 0) val = val*2+1;
      ay->fToneVolumes[2] = val;
      break;
    case 11: case 12: // envelope limit
      ay->fEnvLimit = ay->fRegs[11]+256*ay->fRegs[12];
      if (ay->fEnvLimit == 0) ay->fEnvLimit = 65536; // ��� ���� �� ��������
      break;
    case 13: // envelope type
      // ������ ������ ���� �������� ���� �������� �������
      ay->fEnvCnt = 0;
      if ((val&0x04) != 0) { ay->fEnv = 0; ay->fEnvDelta = 1; } // attack
      else { ay->fEnv = 31; ay->fEnvDelta = -1; } // decay
      break;
  }
}


// ��������� �������� �� ������������� ��������
// �������� ������� �� ��������
uint8_t zebraAYReadReg (ZebraAY *ay, uint8_t reg) {
  return (ay->fRegs[reg&0x0f]);
}


////////////////////////////////////////////////////////////////////////////////
// ���������, ��� ����� ������� � ����� ������� ��� ���
// �������� ����������� ���������� �����
// ������� �������
void zebraAYSelectReg (ZebraAY *ay, uint8_t reg) {
  ay->fSelectedReg = (reg&0x0f);
}


// ��������� �������� �� ��������� ��������
uint8_t zebraAYRead (ZebraAY *ay) {
  return (ay->fRegs[ay->fSelectedReg&0x0f]);
}


// �������� �������� � �������� �������
void zebraAYWrite (ZebraAY *ay, uint8_t val) {
  zebraAYWriteReg(ay, ay->fSelectedReg, val);
}


////////////////////////////////////////////////////////////////////////////////
int zebraAYGetChannelEnabled (ZebraAY *ay, int idx) {
  return (idx >= 0 && idx < 3 ? ay->fChannelEnabled[idx] : 0);
}


void zebraAYSetChannelEnabled (ZebraAY *ay, int idx, int v) {
  if (idx >= 0 && idx < 3) ay->fChannelEnabled[idx] = !!v;
}


////////////////////////////////////////////////////////////////////////////////
// "������������" cnt ���, ������ �� ���� ������, ���� ����
// �������� ��� ����������� ����������, � ��� ����� � fAYTick
// fAYTick ������� �� ������ ������ fAYFreq
// ���� ������ ������� ��� ������ ��������� �� ������,
// �� ������� � ���� �� �������� -- ��� �������� �� �����
// ������, ���� ��������� ���������������� ������ -- NULL,
// �� ���� ������ ������ �� �����
// ���������� ���������� �����ң��� �� ��� ����� �������
int zebraAYDo8Steps (ZebraAY *ay, int cnt) {
  int res = 0;
  int lims[3];
  int vol[3];
  int chE[3]; // bool
  //
  if (cnt < 1) return 0;
  // ���
  const int srate = ay->fSampleRate;
  int ayf = ay->fAYFreq;
  int scnt = ay->fTickFrac%ayf;
  int env = ay->fEnv;
  int envD = ay->fEnvDelta;
  for (int f = 0; f < 3; ++f) {
    vol[f] = (ay->fUseEnvelope[f] ? env : ay->fToneVolumes[f]);
    lims[f] = ay->fToneLimit[f];
    chE[f] = ay->fChannelEnabled[f];
  }
  int limE = ay->fEnvLimit;
  int limN = ay->fNoiseLimit;
  // ����������� �ޣ���� ����� (����� �� ���)
  ay->fAYTick = (ay->fAYTick+cnt*8)%ayf;
  ayf /= 8;
  while (cnt-- > 0) {
    // ��� �������
    if (++ay->fNoiseCnt >= limN) {
      // �������� �� MAME, ������ �� FUSE %-)
      int t = ay->fRNG;
      //
      if (((t>>1)^t)&0x01) ay->fNoiseState = !ay->fNoiseState;
      ay->fRNG = (t>>1)|((((t>>2)^t)&0x01)*0x10000);
      ay->fNoiseCnt = 0;
    }
    // ��� ��������
    if (++ay->fEnvCnt >= limE) {
      ay->fEnvCnt = 0;
      if (envD != 0) {
        env += envD;
        if (env < 0 || env > 31) {
          switch (ay->fRegs[13]&0x0f) {
            case 0 ... 7: case 9: case 15: // ������� �����
              env = 0;
              envD = 0;
              break;
            case 8: // ���� ������ ����
              env = 31;
              envD = -1;
              break;
            case 12: // ���� ����� �����
              env = 0;
              envD = 1;
              break;
            case 10: case 14: // ���������
              envD = -envD;
              env = (envD < 0 ? 30 : 1);
              break;
            default:  //11,13: ������� ������
              env = 31;
              envD = 0;
              break;
          }
        }
        if (ay->fUseEnvelope[0]) vol[0] = env;
        if (ay->fUseEnvelope[1]) vol[1] = env;
        if (ay->fUseEnvelope[2]) vol[2] = env;
      }
    }
    // �ޣ����� ����� � ������
    for (int f = 0; f < 3; ++f) {
      // �ޣ����
      if (++ay->fToneCnt[f] >= lims[f]) {
        ay->fToneCnt[f] = 0;
        ay->fToneState[f] = !ay->fToneState[f];
      }
      // ������
      if ((ay->fToneState[f] || ay->fDisableTone[f]) && (ay->fNoiseState || ay->fDisableNoise[f])) {
        if (chE[f]) {
          int t = vol[f];
          //
          ay->fMixL += ay->fVolumes[f*2+0][t];
          ay->fMixR += ay->fVolumes[f*2+1][t];
        }
      }
    }
    //
    ++ay->fMixCnt;
    // ���� ������ �����?
    if ((scnt += srate) >= ayf) {
      scnt %= ayf;
      zebraAYPutSample(ay);
      ++res; // ����� ������� ������
    }
  }
  // ������� ������������ ��������
  ay->fTickFrac = scnt;
  ay->fEnv = env;
  ay->fEnvDelta = envD;
  return res;
}


int zebraAYWillGenSampleOnNextTick (ZebraAY *ay) {
  return (ay->fTickFrac%ay->fAYFreq+ay->fSampleRate >= ay->fAYFreq);
}


// ��������� tickCnt ����� (�� �����������!)
// � ��������� -- ������ Do8Steps
// ���������� ���������� �����ң��� �������
int zebraAYDoTicks (ZebraAY *ay, int tickCnt) {
  int res = 0;
  //
  if (tickCnt > 0) {
    res = zebraAYDo8Steps(ay, tickCnt/8); // ����������
    // �������
    tickCnt %= 8;
    while (tickCnt-- > 0) {
      if ((++ay->fAYTick)%8 == 0) {
        // ������� ����������
        ay->fAYTick -= 8; // ������ ��� Do8Steps() ������ ���������
        res += zebraAYDo8Steps(ay, 1);
      }
    }
    ay->fAYTick %= ay->fAYFreq; // �� ������ ������
  }
  //
  return res;
}


// ��� ��������� ����� �������� � ������ ������� �����,
// ������ ����� �� ����������� %-)
// �������, ��� ����� ������� DoAY8Steps(),
// ���� fAYTick �� ���������� ������� fAYFreq
// ����������, DoAY8Steps() ����� �� ����
// �������� ������
// ���������� ���������� �����ң��� �� ��� ����� �������
int zebraAYNewFrame (ZebraAY *ay) {
  return zebraAYDoTicks(ay, ay->fAYFreq-ay->fAYTick); // �����-��! %-)
}
