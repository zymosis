/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
//#define USE_URASM_DISASM

#include "../libzymosis/zymosis.h"
#ifdef USE_URASM_DISASM
# include "../liburasm/liburasm.h"
#else
# include "../libzymosis/zymosis_utils.h"
#endif
#include "libvideo/video.h"
#include "debugger.h"
#include "emucommon.h"
#include "emuvars.h"
#include "emuutils.h"
#include "emuexec.h"
#include "zxscrdraw.h"
#include "console.h"
#include "tapes.h"


////////////////////////////////////////////////////////////////////////////////
int debuggerActive = 0;
int dbgAlpha = 220;

int dbgIntrHit = 0;
int dbgNMIHit = 0;

static int dbgAllowLabels = 0x03;

static uint8_t dbgBreakpoints[65536]; //FIXME: for 128


////////////////////////////////////////////////////////////////////////////////
//static VOverlay *dbgOverlay = NULL;
static uint16_t dbgUnasmTop = 0;
static uint16_t dbgAddrSkip = 0;
//static uint16_t dbgCurAddr = 0;
static uint16_t dbgPageAddrs[32];
static int dbgPageY = 0;
static int dbgHidden = 0;
static int offsetLabels = 1;
static int dbgMemMode = 1;


typedef int (*KeyEventCB) (SDL_KeyboardEvent *key);
typedef void (*DrawOverlayCB) (void);

static KeyEventCB dbgKeyEventFn = NULL;
static DrawOverlayCB dbgDrawOverlayFn = NULL;


#define DBG_UNASM_HEIGHT  (VID_TEXT_HEIGHT-5)


////////////////////////////////////////////////////////////////////////////////
typedef struct DebugLabelT {
  char *name; /* malloced */
  int value; /* [0..65535] */
  int asoffset;
  struct DebugLabelT *nextBucket; /* in bucket */
  struct DebugLabelT *nextName; /* list of all known labels */
} DebugLabel;

/*TODO: faster searching by name */
static DebugLabel *labelBuckets[65536];
static DebugLabel *labelHead = NULL;
static DebugLabel *labelTail = NULL;

static __attribute__((constructor)) void dbgInitLabelsCtor (void) {
  for (unsigned f = 0; f < 65536; ++f) labelBuckets[f] = NULL;
}


void dbgClearLabels (void) {
  for (unsigned f = 0; f < 65536; ++f) labelBuckets[f] = NULL;
  while (labelHead) {
    DebugLabel *l = labelHead;
    labelHead = l->nextName;
    free(l->name);
    free(l);
  }
  labelHead = labelTail = NULL;
}


static DebugLabel *dbgFindLabelByNameIntr (const char *name, DebugLabel **prevp) {
  if (prevp) *prevp = NULL;
  if (!name || !name[0]) return NULL;
  DebugLabel *prev = NULL;
  for (DebugLabel *curr = labelHead; curr; prev = curr, curr = curr->nextName) {
    if (strcasecmp(curr->name, name) == 0) {
      if (prevp) *prevp = prev;
      return curr;
    }
  }
  return NULL;
}


static void dbgRemoveLabelIntr (DebugLabel *prev, DebugLabel *curr) {
  if (!curr) return; // just in case
  /* remove from linked list */
  if (prev) prev->nextName = curr->nextName; else labelHead = curr->nextName;
  if (!curr->nextName) labelTail = prev;
  /* remove from bucket */
  prev = NULL;
  for (DebugLabel *cc = labelBuckets[curr->value]; cc; prev = cc, cc = cc->nextBucket) {
    if (cc == curr) {
      /* i found her! */
      if (prev) prev->nextBucket = cc->nextBucket; else labelBuckets[curr->value] = cc->nextBucket;
      break;
    }
  }
  /* we can free it here */
  free(curr->name);
  free(curr);
}


int dbgFindLabelByName (const char *name) {
  DebugLabel *lbl = dbgFindLabelByNameIntr(name, NULL);
  return (lbl ? lbl->value : -1);
}


const char *dbgFindLabelByVal (int val, int asoffset) {
  if (val < 0 || val > 65535) return NULL;
  for (DebugLabel *lbl = labelBuckets[val]; lbl; lbl = lbl->nextBucket) {
    if (lbl->value == val && lbl->asoffset == asoffset) return lbl->name;
  }
  return NULL;
}


void dbgAddLabel (int addr, const char *name, int asoffset) {
  if (addr < 0 || addr > 65535 || !name || !name[0]) return;
  DebugLabel *prev = NULL;
  DebugLabel *lbl = dbgFindLabelByNameIntr(name, &prev);
  if (lbl) {
    if (lbl->value == addr) {
      /* case */
      lbl->asoffset = asoffset;
      strcpy(lbl->name, name);
      return;
    }
    /* remove it, so it will be readded */
    dbgRemoveLabelIntr(prev, lbl);
    /* remove label with the same value, if there is any */
    /*
    lbl = dbgFindLabelByVal(addr);
    if (lbl) {
      lbl = dbgFindLabelByNameIntr(lbl->name, &prev);
      dbgRemoveLabelIntr(prev, lbl);
    }
    */
  }
  /* it is guaranteed to be a new label */
  lbl = malloc(sizeof(DebugLabel));
  lbl->name = strdup(name);
  lbl->value = addr;
  lbl->asoffset = asoffset;
  lbl->nextBucket = NULL;
  lbl->nextName = NULL;
  /* add to list */
  if (labelTail) labelTail->nextName = lbl; else labelHead = lbl;
  labelTail = lbl;
  /* add to bucket */
  prev = labelBuckets[addr];
  if (prev) {
    while (prev->nextBucket) prev = prev->nextBucket;
    prev->nextBucket = lbl;
  } else {
    labelBuckets[addr] = lbl;
  }
}


void dbgRemoveLabel (const char *name) {
  DebugLabel *prev;
  DebugLabel *lbl = dbgFindLabelByNameIntr(name, &prev);
  if (lbl) dbgRemoveLabelIntr(prev, lbl);
}


/* !0: error */
int dbgSaveRefFile (const char *fname) {
  if (!fname || !fname[0]) return -1;
  FILE *fo = fopen(fname, "w");
  if (!fo) return -2;
  for (DebugLabel *curr = labelHead; curr; curr = curr->nextName) {
    if (fprintf(fo, "#%04X  %s\n", curr->value, curr->name) < 0) {
      fclose(fo);
      return -3;
    }
  }
  return (fclose(fo) ? -4 : 0);
}


/* !0: error */
int dbgLoadRefFile (const char *fname) {
  if (!fname || !fname[0]) return -1;
  FILE *fl = fopen(fname, "r");
  if (!fl) return -2;
  dbgClearLabels();
  char line[512];
  int ignoreLine = 0;
  while (fgets(line, (int)sizeof(line)-2, fl) != NULL) {
    size_t slen = strlen(line);
    if (slen == 0) { ignoreLine = 1; continue; }
    const int eol = (line[slen-1] == '\n' || line[slen-1] == '\r');
    if (ignoreLine) { ignoreLine = !eol; continue; }
    ignoreLine = !eol;

    /* remove comments */
    for (;;) {
      char *cmt = strchr(line, ';');
      if (cmt) {
        *cmt = 0;
        slen = strlen(line);
        continue;
      }
      cmt = strstr(line, "//");
      if (cmt) {
        *cmt = 0;
        slen = strlen(line);
        continue;
      }
      break;
    }

    /* remove trailing spaces */
    while (slen > 0 && (unsigned)(line[slen-1]&0xff) <= 32) --slen;
    if (slen == 0) continue;
    line[slen] = 0;

    /* remove leading spaces */
    slen = 0;
    while (line[slen] && (unsigned)(line[slen]&0xff) <= 32) ++slen;
    if (!line[slen]) continue; /* empty line */
    memmove(line, line+slen, strlen(line+slen)+1);

    /* parse */
    size_t pos;
    int base;
    if (line[0] == '$' || line[0] == '#' || (line[0] == '0' && (line[1] == 'x' || line[1] == 'X'))) {
      /* hex */
      pos = (line[0] == '0' ? 2 : 1);
      base = 16;
    } else if (digitInBase(line[0], 10)) {
      pos = 0;
      base = 10;
    } else {
      continue;
    }
    if (digitInBase(line[pos], base) < 0) continue;
    int val = 0;
    while (line[pos]) {
      int d = digitInBase(line[pos], base);
      if (d < 0) break;
      val = val*base+d;
      if (val > 65535) break;
      ++pos;
    }
    if (val > 65535 || !line[pos] || (unsigned)(line[pos]&0xff) > 32) continue;
    while (line[pos] && (unsigned)(line[pos]&0xff) <= 32) ++pos;
    if (!line[pos]) continue; /* empty line */
    memmove(line, line+pos, strlen(line+pos)+1);

    pos = 1;
    while (line[pos] && (unsigned)(line[pos]&0xff) > 32) ++pos;
    const char occ = line[pos];
    line[pos] = 0;
    //fprintf(stderr, "NEW LABEL: #%04X <%s>\n", (unsigned)val, line);
    // ignore labels starting with "__", or ending with "_"
    // nope, add "X...__" labels as offsets (my convention)
    const char *lbltype = "";
    if (occ) {
      lbltype = line+pos+1;
      while (*lbltype && (unsigned)(*lbltype&0xff) <= 32) ++lbltype;
    }
    // ignore "equ" labels
    if (strcmp(lbltype, "equ") == 0) continue;
    #if 0
    if (pos > 3 && line[0] != '_' && line[pos-1] == '_' && line[pos-2] == '_') {
      dbgAddLabel(val, line, 1/*asoffset*/);
    }
    #endif
    if (pos >= 2 && line[0] == '_' && line[1] == '_') continue;
    if (pos == 1 && line[0] == '_') continue;
    if (pos > 1 && line[pos-1] == '_') continue;
    if (strcmp(lbltype, "stofs") == 0) {
      dbgAddLabel(val, line, 1/*asoffset*/);
    } else {
      dbgAddLabel(val, line, 0/*asoffset*/);
    }
  }
  fclose(fl);
  return 0;
}


void dbgIteratorFree (void *it) {
  /* nothing to do here */
}


static int xstrStartsWithCI (const char *s0, const char *s1) {
  if (!s0 || !s1 || !s0[0] || !s1[0]) return 0;
  while (*s1) {
    char c0 = *s0++;
    if (!c0) return 0;
    if (c0 >= 'a' && c0 <= 'z') c0 = c0-'a'+'A';
    char c1 = *s1++;
    if (c1 >= 'a' && c1 <= 'z') c1 = c1-'a'+'A';
    if (c0 != c1) return 0;
  }
  return 1;
}


/* NULL: no */
void *dbgFirstLabelByPrefix (const char *pfx) {
  if (!pfx || !pfx[0]) return NULL;
  for (DebugLabel *lbl = labelHead; lbl; lbl = lbl->nextName) {
    if (xstrStartsWithCI(lbl->name, pfx)) return lbl;
  }
  return NULL;
}

/* NULL: no more */
void *dbgNextLabelByPrefix (void *it, const char *pfx) {
  if (!it || !pfx || !pfx[0]) return NULL;
  DebugLabel *lbl = (DebugLabel *)it;
  for (lbl = lbl->nextName; lbl; lbl = lbl->nextName) {
    if (xstrStartsWithCI(lbl->name, pfx)) return lbl;
  }
  return NULL;
}


const char *dbgIteratorLabelName (const void *it) {
  if (!it) return NULL;
  const DebugLabel *lbl = (const DebugLabel *)it;
  return lbl->name;
}


#ifdef USE_URASM_DISASM
/* nothing */
#else
static const char *dbgZymGetLabel (uint16_t value, int type/*ZYM_DIS_ATYPE_XXX*/) {
  if ((dbgAllowLabels&1) == 0) return NULL;
  // it is safe to use static buffer here
  static char namebuf[128];
  if (type == ZYM_DIS_ATYPE_WORD || type == ZYM_DIS_ATYPE_PC_ADDR ||
      type == ZYM_DIS_ATYPE_DATA_ADDR)
  {
    /* \x01: label start; \x02: label end */
    const char *n = dbgFindLabelByVal(value, 0/*asoffset*/);
    if (n) {
      namebuf[0] = '\x02';
      strncpy(namebuf+1, n, sizeof(namebuf)-2);
      namebuf[sizeof(namebuf)-1] = 0;
      namebuf[sizeof(namebuf)-2] = 0;
      strcat(namebuf, "\x01");
      return namebuf;
    }
  } else if (type == ZYM_DIS_ATYPE_OFFSET && offsetLabels) {
    /* \x01: label start; \x02: label end */
    int ofs = 0;
    const char *n = dbgFindLabelByVal(value, 1/*asoffset*/);
    if (!n) { ofs = 1; n = dbgFindLabelByVal(value+1, 1/*asoffset*/); }
    if (!n) { ofs = 2; n = dbgFindLabelByVal(value+2, 1/*asoffset*/); }
    if (!n) { ofs = -1; n = dbgFindLabelByVal(value-1, 1/*asoffset*/); }
    if (!n) { ofs = -2; n = dbgFindLabelByVal(value-2, 1/*asoffset*/); }
    //if (!n) { ofs = 3; n = dbgFindLabelByVal(value+3, 1/*asoffset*/); }
    if (n) {
      if (ofs) {
        snprintf(namebuf, sizeof(namebuf), "\x02%s%d%s", (ofs < 0 ? "" : "+"), ofs, n);
      } else {
        snprintf(namebuf, sizeof(namebuf), "\x02%s", n);
      }
      /*
      namebuf[0] = '\x02';
      strncpy(namebuf+1, n, sizeof(namebuf)-2);
      */
      namebuf[sizeof(namebuf)-1] = 0;
      namebuf[sizeof(namebuf)-2] = 0;
      strcat(namebuf, "\x01");
      return namebuf;
    }
  }
  return NULL;
}
#endif


////////////////////////////////////////////////////////////////////////////////
static int dbgKeyEventMain (SDL_KeyboardEvent *key);
static int dbgKeyEventNewAddr (SDL_KeyboardEvent *key);


////////////////////////////////////////////////////////////////////////////////
static struct {
  uint16_t pc;
  zym_regpair_t bc, de, hl, af, sp, ix, iy;
  zym_regpair_t bcx, dex, hlx, afx;
  uint8_t regI;
  uint8_t regR;
  int iff1, iff2;
  uint8_t im;
} oldDState;


static void dbgSaveRegs (void) {
  oldDState.pc = z80.pc;
  oldDState.bc.w = z80.bc.w;
  oldDState.de.w = z80.de.w;
  oldDState.hl.w = z80.hl.w;
  oldDState.af.w = z80.af.w;
  oldDState.sp.w = z80.sp.w;
  oldDState.ix.w = z80.ix.w;
  oldDState.iy.w = z80.iy.w;
  oldDState.bcx.w = z80.bcx.w;
  oldDState.dex.w = z80.dex.w;
  oldDState.hlx.w = z80.hlx.w;
  oldDState.afx.w = z80.afx.w;
  oldDState.regI = z80.regI;
  oldDState.regR = z80.regR;
  oldDState.iff1 = z80.iff1;
  oldDState.iff2 = z80.iff2;
  oldDState.im = z80.im;
  //
  dbgIntrHit = 0;
  dbgNMIHit = 0;
}


void dbgInit (void) {
  memset(dbgBreakpoints, 0, sizeof(dbgBreakpoints));
}


////////////////////////////////////////////////////////////////////////////////
static void dbgDrawStack (int x, int y) {
  uint16_t addr = z80.sp.w;
  x /= 6;
  y /= 8;
  addr -= 8;
  vt_writechars(x, y+0, 5*3, ' ', 7);
  vt_writechars(x, y+1, 5*3, ' ', 7);
  vt_writechars(x, y+2, 5*3, ' ', 7);
  vt_writechars(x, y+3, 5*3, ' ', 7);
  for (int f = 0; f < 12; ++f) {
    uint8_t fc = (f < 2+4 ? 7: f > 2+4 ? 5 : 15);
    vt_writef(x+(f < 4 ? 0 : f < 8 ? 6 : 12), y+(f&0x03), fc,
              "%02X %02X", z80.mem_read(&z80, addr+1u, ZYM_MEMIO_OTHER),
                           z80.mem_read(&z80, addr, ZYM_MEMIO_OTHER));
    addr += 2;
  }
}


static void dbgDrawMemBytes (int x, int y, uint16_t addr) {
  addr -= 8;
  for (int dy = 0; dy < 4; ++dy) {
    uint8_t bc = (dy == 1 ? 1 : 0);
    vt_writef(x, y+dy, (0)|(5<<4), "%04X", addr);
    for (int dx = 0; dx < 8; ++dx) {
      uint8_t fc = (dx&1 ? 5 : 7);
      vt_writef(x+4+dx*2, y+dy, fc|(bc<<4),
                "%02X", z80.mem_read(&z80, addr, ZYM_MEMIO_OTHER));
      ++addr;
    }
  }
}


static void dbgDrawMemBytesChars (int x, int y, uint16_t addr) {
  addr -= 4;
  vt_writechars(x, y+0, 20, ' ', 0x07);
  vt_writechars(x, y+1, 20, ' ', 0x17);
  vt_writechars(x, y+2, 20, ' ', 0x07);
  vt_writechars(x, y+3, 20, ' ', 0x07);
  for (int dy = 0; dy < 4; ++dy) {
    uint8_t bc = (dy == 1 ? 1 : 0);
    vt_writef(x, y+dy, (0)|(5<<4), "%04X", addr);
    for (int dx = 0; dx < 4; ++dx) {
      uint8_t b = z80.mem_read(&z80, addr, ZYM_MEMIO_OTHER);
      uint8_t fc = (dx&1 ? 5 : 7);
      vt_writef(x+5+dx*2, y+dy, fc|(bc<<4), "%02X", b);
      vt_writechar(x+7+4*2+dx, y+dy, b, fc|(bc<<4));
      ++addr;
    }
  }
}


static void dbgDrawMemBytes6 (int x, int y, uint16_t addr) {
  addr -= 6;
  vt_writechars(x, y+0, 20, ' ', 0x07);
  vt_writechars(x, y+1, 20, ' ', 0x17);
  vt_writechars(x, y+2, 20, ' ', 0x07);
  vt_writechars(x, y+3, 20, ' ', 0x07);
  for (int dy = 0; dy < 4; ++dy) {
    uint8_t bc = (dy == 1 ? 1 : 0);
    vt_writef(x, y+dy, (0)|(5<<4), "%04X", addr);
    for (int dx = 0; dx < 6; ++dx) {
      uint8_t b = z80.mem_read(&z80, addr, ZYM_MEMIO_OTHER);
      uint8_t fc = (dx&1 ? 5 : 7);
      vt_writef(x+5+dx*2, y+dy, fc|(bc<<4), "%02X", b);
      ++addr;
    }
  }
}


static void dbgDrawHexW (int x, int y, uint16_t n, int hilight) {
  const uint8_t fc = (hilight > 0 ? 15 : 7);
  const uint8_t bc = (hilight < 0 ? 0 : 0);
  vt_writef(x/6, y/8, fc|(bc<<4), "%04X", n);
}


static void dbgDrawHexB (int x, int y, uint16_t n, int hilight) {
  const uint8_t fc = (hilight ? 15 : 7);
  vt_writef(x/6, y/8, fc, "%02X", n);
}


static void dbgDrawPortsFFD (void) {
  vt_writestrz(VID_TEXT_WIDTH-8, 2, "7FFD:", 0x06);
  vt_writestrz(VID_TEXT_WIDTH-8, 3, "1FFD:", 0x06);
  vt_writef(VID_TEXT_WIDTH-3, 2, 0x05, "#%02X", zxLastOut7ffd);
  vt_writef(VID_TEXT_WIDTH-3, 3, 0x05, "#%02X", zxLastOut1ffd);
}


static void dbgDrawROMs (void) {
  vt_writef(VID_TEXT_WIDTH-3, 0, 0x04, "R%02X", zxLastPagedROM);
}


static void dbgDrawPCPrevPC (void) {
  vt_writef(VID_TEXT_WIDTH-8, 0, 0x05, "%04X", z80.prev_pc);
  vt_writef(VID_TEXT_WIDTH-8, 1, 0x0F, "%04X", z80.org_pc);
}


static void dbgDrawPortsRegRPCs () {
  dbgDrawPortsFFD();
  dbgDrawROMs();
  dbgDrawPCPrevPC();
}


static void dbgDrawRegs (int x, int y) {
  const char *flgS[2] = { "sz.h.pnc", "SZ5H3PNC" };

  x /= 6;
  y /= 8;
  if (dbgIntrHit) vt_writechar(VID_TEXT_WIDTH-1, 0, 'I', 15);
  if (dbgNMIHit) vt_writechar(VID_TEXT_WIDTH-2, 0, 'N', 14);
  if (zxTRDOSPagedIn) vt_writestrz(VID_TEXT_WIDTH-3, 1, "TRD", 6+8);

  vt_writestrz(x, y+0, "AF:     AF'     SP:     IR: ", 5);
  vt_writestrz(x, y+1, "BC:     BC'     PC:     t: ", 5);
  vt_writestrz(x, y+2, "DE:     DE'     IX:     im ,IF", 5);
  vt_writestrz(x, y+3, "HL:     HL'     IY:     ", 5);

  x *= 6;
  y *= 8;
  dbgDrawHexW(x+3*6, y+8*0, z80.af.w, (z80.af.w != oldDState.af.w));
  dbgDrawHexW(x+3*6, y+8*1, z80.bc.w, (z80.bc.w != oldDState.bc.w));
  dbgDrawHexW(x+3*6, y+8*2, z80.de.w, (z80.de.w != oldDState.de.w));
  dbgDrawHexW(x+3*6, y+8*3, z80.hl.w, (z80.hl.w != oldDState.hl.w));

  dbgDrawHexW(x+11*6, y+8*0, z80.afx.w, (z80.afx.w != oldDState.afx.w));
  dbgDrawHexW(x+11*6, y+8*1, z80.bcx.w, (z80.bcx.w != oldDState.bcx.w));
  dbgDrawHexW(x+11*6, y+8*2, z80.dex.w, (z80.dex.w != oldDState.dex.w));
  dbgDrawHexW(x+11*6, y+8*3, z80.hlx.w, (z80.hlx.w != oldDState.hlx.w));

  dbgDrawHexW(x+19*6, y+8*0, z80.sp.w, (z80.sp.w != oldDState.sp.w));
  dbgDrawHexW(x+19*6, y+8*1, z80.pc, (z80.pc != oldDState.pc));
  dbgDrawHexW(x+19*6, y+8*2, z80.ix.w, (z80.ix.w != oldDState.ix.w));
  dbgDrawHexW(x+19*6, y+8*3, z80.iy.w, (z80.iy.w != oldDState.iy.w));

  dbgDrawHexB(x+28*6, y+8*0, z80.regI, (z80.regI != oldDState.regI));
  dbgDrawHexB(x+30*6, y+8*0, z80.regR, (z80.regR != oldDState.regR));

  vt_writef((x+27*6)/6, y/8+1, 0x07, "%5d", z80.tstates);

  x /= 6;
  y /= 8;
  vt_writechar(x+26, y+2, z80.im+'0', (z80.im != oldDState.im ? 15 : 7));
  vt_writechar(x+30, y+2, z80.iff1+'0', (z80.iff1 != oldDState.iff1 ? 15 : 7));
  vt_writechar(x+31, y+2, z80.iff2+'0', (z80.iff2 != oldDState.iff2 ? 15 : 7));

  for (int f = 0; f < 8; ++f) {
    uint8_t c;
         if (((z80.af.f>>(7-f))&0x01) != ((oldDState.af.f>>(7-f))&0x01)) c = 15;
    else if ((z80.af.f>>(7-f))&0x01) c = 7;
    else c = 5;
    vt_writechar(x+(24+f), y+3, flgS[(z80.af.f>>(7-f))&0x01][f], c);
  }
}


////////////////////////////////////////////////////////////////////////////////
static inline uint8_t dbgGetByte (uint16_t addr) {
  return z80.mem_read(&z80, addr, ZYM_MEMIO_OTHER);
}


static inline int dbgOpLenAtAddr (uint16_t addr) {
  #ifdef USE_URASM_DISASM
  const int idx = urasm_disasm_opfind(addr);
  return urasm_disasm_oplen(idx);
  #else
  zym_op_meminfo_t nfo;
  zym_op_meminfo(&z80, &nfo, addr);
  return nfo.inslen;
  #endif
}


static void dbgUnasmUp (void) {
  for (int f = 8; f > 0; --f) {
    uint16_t a = ((int32_t)dbgUnasmTop-f)&0xffff;
    const int inslen = dbgOpLenAtAddr(a);
    if (((a+inslen)&0xffff) == dbgUnasmTop) { dbgUnasmTop = a; return; }
  }
  dbgUnasmTop = ((int32_t)dbgUnasmTop-1)&0xffff;
}


static void dbgUnasmDown (void) {
  const int inslen = dbgOpLenAtAddr(dbgUnasmTop);
  dbgUnasmTop = (dbgUnasmTop+inslen)&0xffff;
}


static void dbgBuildPageInfo (void) {
  int a = dbgUnasmTop;
  for (int f = 0; f < DBG_UNASM_HEIGHT; ++f) {
    const int inslen = dbgOpLenAtAddr(a);
    dbgPageAddrs[f] = a;
    a = (a+inslen)&0xffff;
  }
}


static void dbgCenterUnasm (uint16_t addr) {
  dbgUnasmTop = addr;
  for (int f = 0; f < DBG_UNASM_HEIGHT/2-1; ++f) dbgUnasmUp();
  dbgPageY = 0;
  dbgBuildPageInfo();
  for (int f = 0; f < DBG_UNASM_HEIGHT; ++f) {
    if (dbgPageAddrs[f] == addr) { dbgPageY = f; return; }
  }
  dbgUnasmTop = z80.pc;
  dbgBuildPageInfo();
}


/* \x01: default color; \x02: label start; \x03: number start */
static void dbgDrawStrClr (const char *s, int x, int y, uint8_t fc, uint8_t bg) {
  if (!s || !s[0]) return;
  //VOverlay *v = dbgOverlay;
  int inLabel = 0;
  while (*s) {
    char ch = *s++;
    if (ch >= 1 && ch <= 3) { inLabel = ch-1; continue; }
    /*HACK!*/
    uint8_t cc =
      inLabel == 1 ? (bg >= 2 ? 5+8 : 5) :
      (bg < 7 && inLabel == 2) ? 4+8 :
      ch == ',' ? (bg < 2 ? 3 : 1) :
      fc;
    vt_writechar(x/6, y/8, ch, (cc&0x0f)|(bg<<4));
    x += 6;
  }
}


static int dbgStrLen (const char *s) {
  if (!s) return 0;
  int res = 0;
  while (*s) {
    char ch = *s++;
    if (ch >= 1 && ch <= 3) continue;
    ++res;
  }
  return res;
}


/* returns bytes in instruction */
static int dbgDrawUnasmLine (uint16_t addr, int x, int y, uint8_t fc, uint8_t bc) {
  char *t;
  #ifdef USE_URASM_DISASM
  const int inslen = dbgOpLenAtAddr(addr);
  char disbuf[128];
  urasm_disasm_opdisasm(disbuf, addr);
  #else
  zym_disop_t disop;
  zym_disasm_init(&disop);
  disop.flags = ZYM_DIS_FLAGS_DEFAULT;
  zym_disasm_one(&z80, &disop, addr);
  const int inslen = disop.inslen;
  char *disbuf = disop.disbuf;
  #endif

  // clear line
  vt_writechars(x/6, y/8, VID_TEXT_WIDTH, ' ', fc|(bc<<4));
  // draw address
  vt_writef(x/6, y/8, fc|(bc<<4), "%04X", addr);

  int opx = x/6+5;
  // draw opcode bytes
  for (int f = 0; f < inslen; ++f) {
    vt_writef(opx, y/8, (bc >= 7 ? fc : 4+(f&1))|(bc<<4), "%02X", dbgGetByte(addr+(uint16_t)f));
    opx += 2;
    if (inslen <= 3) ++opx;
  }

  // draw opcode
  if ((t = strchr(disbuf, '\t')) != NULL) *t++ = '\0';
  dbgDrawStrClr(disbuf, x+16*6, y, fc, bc);
  // draw operands
  if (t != NULL) {
    dbgDrawStrClr(t, x+21*6, y, fc, bc);
  }

  // draw address label
  if (dbgAllowLabels&2) {
    const char *lbl = dbgFindLabelByVal(addr, 0/*asoffset*/);
    if (lbl) {
      int lleft = VID_TEXT_WIDTH-(t ? 21+dbgStrLen(t) : dbgStrLen(disbuf));
      char lbuf[128];
      lbuf[0] = ';';
      strncpy(lbuf+1, lbl, sizeof(lbuf)-1);
      lbuf[sizeof(lbuf)-1] = 0;
      int nlen = (int)strlen(lbuf);
      if (nlen > lleft) { nlen = lleft; lbuf[lleft] = 0; }
      //drawStr6VO(dbgOverlay, lbuf, x+(52-nlen)*6, y, 4, bc);
      vt_writestrz(x/6+(VID_TEXT_WIDTH-nlen), y/8, lbuf, 4|(bc<<4));
    }
  }
  return inslen;
}


static void dbgDrawUnasm (int x, int y) {
  uint16_t addr = dbgUnasmTop;

  for (int f = 0; f < DBG_UNASM_HEIGHT; ++f) {
    uint8_t fc, bc;
    int inslen;

    if (addr == z80.pc) {
      if (dbgBreakpoints[addr]&DBG_BP_EXEC) {
        if (f == dbgPageY) {
          fc = 2;
          bc = 15;
        } else {
          fc = 2;
          bc = 7;
        }
      } else {
        fc = 0;
        bc = (f == dbgPageY ? 15 : 7);
      }
    } else {
      if (dbgBreakpoints[addr]&DBG_BP_EXEC) {
        if (f == dbgPageY) {
          fc = 15;
          bc = 2;
        } else {
          fc = 7;
          bc = 2;
        }
      } else {
        if (f == dbgPageY) {
          fc = 7;
          bc = 1;
        } else {
          fc = 7;
          bc = 0;
        }
      }
    }
    inslen = dbgDrawUnasmLine(addr, x, y+f*8, fc, bc);
    addr += (uint16_t)inslen;
  }
}


// -2: on screen, but invisible (must move left to fix)
// -1: must go up
// 0..DBG_UNASM_HEIGHT-1: ok
// DBG_UNASM_HEIGHT
static int dbgIsPCOnScreen (void) {
  for (int f = 0; f < DBG_UNASM_HEIGHT; ++f) {
    if (dbgPageAddrs[f] == z80.pc) return f;
  }
  if (z80.pc < dbgUnasmTop) {
    return -1;
  } else {
    int end = dbgUnasmTop;
    for (int f = 0; f < DBG_UNASM_HEIGHT; ++f) {
      const int inslen = dbgOpLenAtAddr(end&0xffff);
      if (end == z80.pc) return f;
      if (z80.pc > end && z80.pc < end+inslen) return -2; // invisible
      end += inslen;
    }
    return DBG_UNASM_HEIGHT;
  }
}


static void dbgToPC (void) {
  dbgBuildPageInfo();
  for (;;) {
    int pcpos = dbgIsPCOnScreen();
    if (pcpos == -2) {
      dbgUnasmTop = ((int32_t)dbgUnasmTop+1)&0xffff;
      dbgBuildPageInfo();
    } else if (pcpos < 0 || pcpos >= DBG_UNASM_HEIGHT) {
      dbgCenterUnasm(z80.pc);
    } else {
      dbgPageY = pcpos;
      break;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
// to view original screen
int dbgIsHidden (void) {
  return dbgHidden;
}


void dbgDraw (void) {
  //vt_cls(0xb0, 0x01);
  vt_cls(' ', 0x07);

  if (!dbgHidden) {
    dbgDrawRegs(0, 0);
    dbgDrawPortsRegRPCs();
    dbgDrawStack(32*6+6+6-6, 0);

         if (dbgMemMode == 0) dbgDrawMemBytes(51, 0, z80.pc);
    else if (dbgMemMode == 1) dbgDrawMemBytes6(51, 0, z80.pc);
    else dbgDrawMemBytesChars(51, 0, z80.pc);

    vt_writechars(0, 4, VID_TEXT_WIDTH, 0xc4, 5);
    for (int y = 0; y < 4; ++y) {
      vt_writechar(32, y, 0xb3, 5);
      vt_writechar(50, y, 0xb3, 5);
      vt_writechar(VID_TEXT_WIDTH-9, y, 0xb3, 5);
    }
    vt_writechar(32, 4, 0xc1, 5);
    vt_writechar(50, 4, 0xc1, 5);
    vt_writechar(VID_TEXT_WIDTH-9, 4, 0xc1, 5);

    dbgDrawUnasm(0, 4*8+5+8);
  }
  if (dbgDrawOverlayFn != NULL) dbgDrawOverlayFn();
}


////////////////////////////////////////////////////////////////////////////////
void dbgSetActive (int st) {
  st = !!st;
  if (st != debuggerActive) {
    if ((debuggerActive = st) != 0) {
      #ifdef USE_URASM_DISASM
      urasm_getbyte = dbgGetByte;
      #else
      zym_disasm_getlabel = dbgZymGetLabel;
      zym_disasm_mnemo_end = "\t";
      zym_disasm_num_start = "\x03";
      zym_disasm_num_end = "\x01";
      #endif
      dbgSaveRegs();
      dbgCenterUnasm(z80.pc);
      dbgKeyEventFn = dbgKeyEventMain;
      dbgHidden = 0;
      zxRealiseScreen(z80.tstates);
    } else {
      if (dbgBreakpoints[z80.pc]) {
        dbgAddrSkip = z80.pc;
      }
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
int dbgKeyEvent (SDL_KeyboardEvent *key) {
  if (dbgKeyEventFn != NULL) return dbgKeyEventFn(key);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static int dbgNewAddrPos;
static char dbgNewAddrBuf[8];
static KeyEventCB dbgNewAddrPrevKeyEventFn;
static DrawOverlayCB dbgNewAddrPrevDrawOverlayFn;


void dbgSetUnasmAddr (uint16_t addr) {
  dbgUnasmTop = addr;
  dbgPageY = 0;
  dbgBuildPageInfo();
}


static void dbgNewAddrDeinit (int setaddr) {
  dbgKeyEventFn = dbgNewAddrPrevKeyEventFn;
  dbgDrawOverlayFn = dbgNewAddrPrevDrawOverlayFn;
  if (setaddr) {
    dbgUnasmTop = 0;
    for (int f = 0; f < 4; ++f) dbgUnasmTop = (dbgUnasmTop*16)+dbgNewAddrBuf[f]-'0'-(dbgNewAddrBuf[f] > '9' ? 7 : 0);
    dbgPageY = 0;
    dbgBuildPageInfo();
  }
}


static void dbgDrawNewAddr (void) {
  #if 0
  drawStr6VO(dbgOverlay, dbgNewAddrBuf, 3, 4*8+5+3/*+dbgPageY*8*/, 0, 5+8);
  drawChar6VO(dbgOverlay, dbgNewAddrBuf[dbgNewAddrPos], 3+dbgNewAddrPos*6, 4*8+5+3/*+dbgPageY*8*/, 0, 6+8);
  #else
  vt_writestrz(0, 4+1, dbgNewAddrBuf, (5+8)<<4);
  vt_writechar(0+dbgNewAddrPos, 4+1, dbgNewAddrBuf[dbgNewAddrPos], (6+8)<<4);
  #endif
}


static int dbgKeyEventNewAddr (SDL_KeyboardEvent *key) {
  if (key->type == SDL_KEYDOWN && (key->keysym.mod&KMOD_CTRL) == 0) {
    if (key->keysym.unicode >= 32 && key->keysym.unicode < 127) {
      char ch = toupper(key->keysym.unicode);
      if ((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'F')) {
        dbgNewAddrBuf[dbgNewAddrPos++] = ch;
        if (dbgNewAddrPos > 3) dbgNewAddrPos = 3;
        return 1;
      }
    }

    switch (key->keysym.sym) {
      case SDLK_ESCAPE:
        dbgNewAddrDeinit(0);
        return 1;
      case SDLK_RETURN:
        dbgNewAddrDeinit(1);
        return 1;
      case SDLK_LEFT: case SDLK_KP4: case SDLK_BACKSPACE:
        if (dbgNewAddrPos > 0) --dbgNewAddrPos;
        return 1;
      case SDLK_RIGHT: case SDLK_KP6:
        if (dbgNewAddrPos < 3) ++dbgNewAddrPos;
        return 1;
      case SDLK_HOME: case SDLK_KP7:
        dbgNewAddrPos = 0;
        return 1;
      case SDLK_END: case SDLK_KP1:
        dbgNewAddrPos = 3;
        return 1;
      case SDLK_p:
        if ((key->keysym.mod&KMOD_SHIFT) != 0) {
          snprintf(dbgNewAddrBuf, sizeof(dbgNewAddrBuf), "%04X", z80.pc);
        }
        break;
      default: break;
    }
  }

  return 1;
}


static void dbgNewAddrInit (void) {
  dbgNewAddrPrevKeyEventFn = dbgKeyEventFn;
  dbgNewAddrPrevDrawOverlayFn = dbgDrawOverlayFn;
  dbgKeyEventFn = dbgKeyEventNewAddr;
  dbgDrawOverlayFn = dbgDrawNewAddr;
  dbgNewAddrPos = 0;
  snprintf(dbgNewAddrBuf, sizeof(dbgNewAddrBuf), "%04X", dbgUnasmTop);
}


////////////////////////////////////////////////////////////////////////////////
static int dbgKeyEventMain (SDL_KeyboardEvent *key) {
  if (key->type == SDL_KEYDOWN && (key->keysym.mod&KMOD_ALT) != 0) {
    switch (key->keysym.sym) {
      case SDLK_F5:
        dbgHidden = !dbgHidden;
        return 1;
      default: ;
    }
  }
  //
  if (key->type == SDL_KEYDOWN && (key->keysym.mod&KMOD_CTRL) != 0) {
    int ots;
    //
    switch (key->keysym.sym) {
      case SDLK_F5:
        ots = zxOldScreenTS;
        zxOldScreenTS = 0;
        zxRealiseScreen(machineInfo.tsperframe);
        zxOldScreenTS = ots;
        return 1;
      case SDLK_UP: case SDLK_KP8:
        if (dbgPageY < DBG_UNASM_HEIGHT-1) {
          dbgUnasmUp();
          dbgBuildPageInfo();
          ++dbgPageY;
        }
        return 1;
      case SDLK_DOWN: case SDLK_KP2:
        if (dbgPageY > 0) {
          dbgUnasmDown();
          dbgBuildPageInfo();
          --dbgPageY;
        }
        return 1;
      default: ;
    }
  }
  //
  if (key->type == SDL_KEYDOWN && (key->keysym.mod&KMOD_CTRL) == 0) {
    if (dbgHidden) {
      switch (key->keysym.sym) {
        case SDLK_ESCAPE: case SDLK_RETURN: case SDLK_SPACE:
          dbgHidden = 0;
          return 1;
        default: ;
      }
      return 1;
    }
    //
    switch (key->keysym.sym) {
      case SDLK_ESCAPE:
        dbgSetActive(0);
        return 1;
      case SDLK_UP: case SDLK_KP8:
        if (dbgPageY > 0) {
          --dbgPageY;
        } else {
          dbgUnasmUp();
          dbgBuildPageInfo();
        }
        return 1;
      case SDLK_DOWN: case SDLK_KP2:
        if (++dbgPageY >= DBG_UNASM_HEIGHT) {
          dbgPageY = DBG_UNASM_HEIGHT-1;
          dbgUnasmDown();
          dbgBuildPageInfo();
        }
        return 1;
      case SDLK_PAGEUP: case SDLK_KP9:
        for (int f = 0; f < DBG_UNASM_HEIGHT; ++f) dbgUnasmUp();
        dbgBuildPageInfo();
        return 1;
      case SDLK_PAGEDOWN: case SDLK_KP3:
        for (int f = 0; f < DBG_UNASM_HEIGHT; ++f) dbgUnasmDown();
        dbgBuildPageInfo();
        return 1;
      case SDLK_HOME: case SDLK_KP7:
        dbgPageY = 0;
        return 1;
      case SDLK_END: case SDLK_KP1:
        dbgPageY = DBG_UNASM_HEIGHT-1;
        return 1;
      case SDLK_LEFT: case SDLK_KP4:
        dbgUnasmTop = ((int32_t)dbgUnasmTop-1)&0xffff;
        dbgBuildPageInfo();
        return 1;
      case SDLK_RIGHT: case SDLK_KP6:
        dbgUnasmTop = (dbgUnasmTop+1)&0xffff;
        dbgBuildPageInfo();
        return 1;
      case SDLK_F7: // step
        dbgSaveRegs();
        dbgAddrSkip = z80.pc;
        debuggerActive = 0; // need to reset this, otherwise `z80Step()` will do nothing
        z80Step(1);
        debuggerActive = 1;
        //fprintf(stderr, "%d %d\n", dbgIntrHit, dbgNMIHit);
        zxRealiseScreen(z80.tstates);
        dbgAddrSkip = 0;
        dbgToPC();
        return 1;
      case SDLK_F8: // to next
        dbgSaveRegs();
        dbgAddrSkip = z80.pc;
        {
          int na = (dbgPageAddrs[dbgPageY]+dbgOpLenAtAddr(dbgPageAddrs[dbgPageY]))&0xffff;
          dbgBreakpoints[na] |= DBG_BP_EXECONE;
        }
        dbgSetActive(0);
        return 1;
      case SDLK_SPACE:
        dbgBreakpoints[dbgPageAddrs[dbgPageY]] ^= DBG_BP_EXEC;
        return 1;
      case SDLK_g:
        dbgNewAddrInit();
        return 1;
      case SDLK_d: // difference between PC and current address
        cprintf("PC=#%04X; diff=%d\n", z80.pc, (int)dbgPageAddrs[dbgPageY]-(int)z80.pc);
        break;
      case SDLK_l:
        if ((key->keysym.mod&KMOD_SHIFT) == 0) {
          dbgAllowLabels ^= 0x01;
        } else {
          dbgAllowLabels ^= 0x02;
        }
        break;
      case SDLK_m:
        dbgMemMode = (dbgMemMode+1)%3;
        break;
      case SDLK_x:
        offsetLabels = !offsetLabels;
        break;
      default: ;
    }
  }
  //
  return (dbgHidden ? 1 : 0);
}


////////////////////////////////////////////////////////////////////////////////
static int breakpointHit (uint8_t type) {
  char sbuf[128];
  snprintf(sbuf, sizeof(sbuf), "::breakpointhit %u", type);
  if (Jim_Eval(jim, sbuf) == JIM_OK) {
    Jim_Obj *res = Jim_GetResult(jim);
    long dostop = 0;
    /* process string results */
    const char *resstr = Jim_String(res);
    if (resstr && strcasecmp(resstr, "stop") == 0) dostop = 1;
    if (!dostop && Jim_GetLong(jim, res, &dostop) != JIM_OK) dostop = 0;
    if (!dostop) return 0;
  } else {
    Jim_MakeErrorMessage(jim);
    cprintf("\4=== JIM ERROR ===\n%s\n=================\n", Jim_GetString(Jim_GetResult(jim), NULL));
  }
  dbgSetActive(1);
  return 1;
}


int z80CheckBP (uint16_t addrport, uint8_t type) {
  if (type == DBG_BP_NONE) return 0;
  int res = 0;
  #if 0
  uint16_t addrport = z80.pc;
  Z80OpMemInfo nfo;
  Z80_GetOpMemInfo(&z80, &nfo, addrport);
  #endif
  if (debuggerActive >= 0) {
    if ((dbgBreakpoints[addrport]&type) != 0) {
      if (dbgAddrSkip == addrport && (type&(DBG_BP_EXEC|DBG_BP_EXECONE)) != 0) { dbgAddrSkip = 0; return 0; }
      type &= dbgBreakpoints[addrport];
      dbgBreakpoints[addrport] &= ~DBG_BP_EXECONE;
      res = breakpointHit(type);
    } else {
      /* check low port (0 means any) */
      type &= DBG_BP_PORTIN|DBG_BP_PORTOUT;
      if (type) {
        if ((dbgBreakpoints[addrport&0xff]&type) != 0 || (dbgBreakpoints[0]&type) != 0) {
          type &= dbgBreakpoints[addrport];
          res = breakpointHit(type);
        }
      }
    }
  }
  return res;
}


const char *dbgGetBPTypeStr (uint8_t type) {
  static char buf[16];
  size_t pos = 0;
  if (type&DBG_BP_EXEC) buf[pos++] = 'E';
  if (type&DBG_BP_READ) buf[pos++] = 'R';
  if (type&DBG_BP_WRITE) buf[pos++] = 'W';
  if (type&DBG_BP_PORTIN) buf[pos++] = 'I';
  if (type&DBG_BP_PORTOUT) buf[pos++] = 'O';
  if (!pos) return "";
  buf[pos] = 0;
  return buf;
}


uint8_t dbgGetBPType (uint16_t addr) {
  return dbgBreakpoints[addr];
}


void dbgSetBPType (uint16_t addr, uint8_t type) {
  dbgBreakpoints[addr] = type;
}


void dbgBPClear (void) {
  memset(dbgBreakpoints, 0, sizeof(dbgBreakpoints));
}


int dbgBPSaveToFile (const char *fname) {
  if (!fname || !fname[0]) return -1;
  FILE *fo = fopen(fname, "w");
  if (!fo) return -1;
  for (unsigned f = 0; f < 65536; ++f) {
    const uint8_t bpt = dbgGetBPType(f&0xffffu);
    if (bpt == DBG_BP_NONE) continue;
    if (fprintf(fo, "%02X  %02X\n", bpt, f) < 0) {
      fclose(fo);
      return -3;
    }
  }
  return (fclose(fo) == 0 ? 0 : -4);
}


int dbgBPLoadFromFile (const char *fname) {
  if (!fname || !fname[0]) return -1;
  FILE *fl = fopen(fname, "r");
  if (!fl) return -1;
  dbgBPClear();
  char line[512];
  int ignoreLine = 0;
  while (fgets(line, (int)sizeof(line)-2, fl) != NULL) {
    size_t slen = strlen(line);
    if (slen == 0) { ignoreLine = 1; continue; }
    const int eol = (line[slen-1] == '\n' || line[slen-1] == '\r');
    if (ignoreLine) { ignoreLine = !eol; continue; }
    ignoreLine = !eol;
    char *cmt = strchr(line, ';');
    if (cmt) {
      *cmt = 0;
      slen = strlen(line);
    }
    /* remove trailing spaces */
    while (slen > 0 && (unsigned)(line[slen-1]&0xff) <= 32) --slen;
    if (slen == 0) continue;
    line[slen] = 0;
    /* remove leading spaces */
    slen = 0;
    while (line[slen] && (unsigned)(line[slen]&0xff) <= 32) ++slen;
    if (!line[slen]) continue; /* empty line */
    memmove(line, line+slen, strlen(line+slen)+1);
    /* parse */
    if (digitInBase(line[0], 16) < 0) continue;
    size_t pos = 0;
    /* type */
    int type = 0;
    for (;;) {
      int d = digitInBase(line[pos], 16);
      if (d < 0) break;
      type = type*16+d;
      if (type > 255) break;
      ++pos;
    }
    if (type == 0 || type > 255) continue;
    if (!line[pos] || (unsigned)(line[pos]&0xff) > 32) continue;
    /* address */
    while (line[pos] && (unsigned)(line[pos]&0xff) <= 32) ++pos;
    if (digitInBase(line[pos], 16) < 0) continue;
    int addr = 0;
    for (;;) {
      int d = digitInBase(line[pos], 16);
      if (d < 0) break;
      addr = addr*16+d;
      if (addr > 65535) break;
      ++pos;
    }
    if (addr > 65535) continue;
    if (line[pos] && (unsigned)(line[pos]&0xff) > 32) continue;
    dbgSetBPType(addr&0xffffu, type&0xff);
  }
  fclose(fl);
  return 0;
}
