/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include "emuvars.h"

int optSndSyncDebug = 0;


Jim_Interp *jim;

int optConDump = 0;
int optConDumpToStdout = 1;

libspectrum_rzx *zxRZX = NULL;
int zxRZXRCount = 0;
int zxRZXFrames = 0;
int zxRZXCurFrame = 0;

libspectrum_snap *zxSLT = NULL;

libspectrum_tape *zxCurTape = NULL;
int optTapePlaying = 0;
int optTapeSound = 0;
int tapeAlpha = 128;

int tapNextEdgeTS; // can exceed tsperframe or be negative
int tapNextEdge;
int tapStopAfterNextEdge = 0; // <0: stop on 48k; >0: stop anyway
int tapCurEdge = 0; // 0: low; !0: high
int tapNeedRewind = 0;
//
int optTapeAutoPauseEmu = 0; // not reliable yet
int optTapeMaxSpeed = 1;
int optTapeDetector = 1;
EMU_MAYBE_UNUSED int optTapeAccelerator = 0;

int diskAlpha = 210;
int64_t diskLastActivity = 0;

uint8_t zxLastOut7ffd;
uint8_t zxLastOut1ffd;
uint8_t zxAYRegs[16];

upd_fdc *upd765_fdc = NULL;
fdd_t upd765_drives[2];
disk_t flpdisk[4];
int optAutoaddBoot = 1;

int kmouseAbsX = 0;
int kmouseAbsY = 0;

int optKJoystick = 1;
int optKMouse = 1;
int optKMouseAbsolute = 0;
int optKMouseStrict = 1;
int optKMouseSwapButtons = 0;
int zxMouseWasMoved = 0;
int zxMouseLastX = 0;
int zxMouseLastY = 0;
int zxMouseFracX = 0;
int zxMouseFracY = 0;
uint8_t zxKMouseDX = 0, zxKMouseDY = 0, zxKMouseWheel = 0;
uint8_t zxKMouseButtons = 0;
int zxKMouseSpeed = 2; // how much host pixels mouse should move before ZX mouse will be moved?

zym_cpu_t z80;
int z80GenerateNMI = 0;

int zxModel = ZX_MACHINE_48K;
int zxPentagonMemory = 128;

int optPaused = 0; // bit0: permanent pause; bit1: temporary pause
int optDrawFPS = 0;
int emuFrameCount;
int64_t emuFrameStartTime;
char emuLastFPSText[128];
int64_t emuPrevFCB = 0;

int optCurBlinkMS = 600;
int conVisible = 0;
int conAlpha = 190;
int optMaxSpeed = 0;
int optSpeed = 100; // in percents
int optNoScreenReal = 0;
int optSlowShade = 1; // "shade" screen if speed is < 100?
int zxLateTimings = 0;
int optDrawKeyLeds = 0;
int klAlpha = 128;
int zxScreenOfs = 0;
int optEmulateSnow = 0;
int optTRDOSenabled = 1;
int zxTRDOSactivated = 0;
int optTRDOSROMIndex = -1;
int zxTRDOSPagedIn = 0;
int optTRDOSTraps = 1;
int optTRDOSTrapDebug = 0;
int optKeyHelpVisible = 0;
int optAllowOther128 = 1;
int optEmulateMatrix = 1;
int optZXIssue = 0; // 0: issue2; 1: issue3; 2: +3
int optFDas7FFD = 0; // use only low byte to decode 7ffd
int zx7ffdLocked = 1;
uint8_t zxLastContendedMemByte = 0; // required for +2A/+3 floating bus emulation
int optSnapSetModel = 1;

int optNoFlic = -1; // autodetect
int optNoFlicDetected = 0; // bool
int emuLastScreenBank = 0;
int emuScreenBankSwitchCount = 0;
int emuCurrScreenBank = -1;
int emuNoflicMsgCooldown = 0;

int optBrightBlack = 0;
int optBrightBlackValue = 48;

int optAutofire = 0;
int optAutofireHold = 1;
int optAutofireDelay = 1;

int afireDown = 0;
int afireLeft = 0;
int afirePortBit = (8<<8)|0x10; // kfire

int sndSampleRate = -1;

int optSoundBeeper = 1;
int optSoundAY = 1;
int optSoundVolume = 100;
int optSoundVolumeBeeper = 100;
int optSoundVolumeAY = 100;
int zxLastOutFFFD = 0; // current register

int optAYEnabled = 1;

int zxGluckROM = -1;
int optUseGluck = 0;
int zxWasRAMExec = 0;

int zxQCmdrROM = -1;
int optUseQCmdr = 0;

int zxLineStartTS[400];
uint16_t zxUlaSnow[192][32];
int zxWasUlaSnow;
uint8_t zxUlaOut;
uint16_t zxScrLineOfs[192];
Uint8 zxScreen[2][400*352];
int zxScreenCurrent;
int zxOldScreenTS;
Uint8 zxBorder;
uint8_t *zxScreenBank;
int zxFlashTimer; // 0..15
int zxFlashState;
int zxMaxMemoryBank;
uint8_t *zxMemoryBanks[256]; // memory banks
uint8_t zxMemBanksDirty[256]; // non-zero if something was written in the bank
int zxMaxROMMemoryBank;
uint8_t *zxROMBanks[256]; // memory banks
uint8_t *zxMemory[4]; // paged in memory (page 0: ROM)
uint8_t zxMemoryBankNum[4]; // paged in memory (page 0: ROM)
uint8_t zxKeyboardState[9]; // 8: kempston
uint8_t zxContentionTable[2][80000]; //frame can never have more than 80000 tstates; [0]: MREQ

int zxLastPagedROM; // TR-DOS replaces this
int zxLastPagedRAM0; // <0: none

uint32_t zxKeyBinds[65536];

int optFileTrapsMode = ZOPT_FTP_RW;

ZXMachineInfo machineInfo;

uint64_t dbgTickCounter = 0;
int dbtTickCounterPaused = 1;
int optAllowZXEmuTraps = 1;

int snapWasDisk = 0; // bit flags for 4 drives
int snapWasCPCDisk = 0; // bit flags for 4 drives
int snapWasTape = 0;
int optBrightBorder = 0;
int optFlashLoad = 1;

int optMaxSpeedBadScreen = 0;
int optMaxSpeedBadScreenTape = 0;
int optTapeAllowMaxSpeedUgly = 1;

int optDebugFlashLoad = 0;

int optOpenSE = 0;

int sndAllowUseToggle = 1;
