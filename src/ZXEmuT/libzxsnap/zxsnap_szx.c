/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "zxsnap_bo.h"
#include "zxsnap_szx.h"
#ifdef HAVE_ZLIB
# include <zlib.h>
#endif


static const char *zxs_blk_name (const zxs_block_t *blk) {
  if (!blk) return "????";
  static char buf[4*4+4];
  size_t pos = 0;
  for (unsigned f = 0; f < 4; ++f) {
    uint8_t ch = blk->dwId[f]&0xff;
    if (ch < 32 || ch == 127) {
      snprintf(buf+pos, 6, "{%02X}", ch);
      pos += 4;
    } else {
      buf[pos++] = (char)ch;
    }
  }
  buf[pos] = 0;
  return buf;
}


////////////////////////////////////////////////////////////////////////////////
// -1: error
// =0: can't pack
// >0: packed size
static int zlib_pack (void **dest, const void *data, int len, int dontpack) {
  if (dontpack) {
    if ((*dest = malloc(len)) == NULL) return -1;
    memcpy(*dest, data, len);
    return 0;
  }
  #ifdef HAVE_ZLIB
  uLongf maxpklen = len+256;
  if ((*dest = malloc(maxpklen)) == NULL) return -1;
  if (compress2(*dest, &maxpklen, data, len, Z_BEST_COMPRESSION) == Z_OK) {
    if (maxpklen >= len) {
      memcpy(*dest, data, len);
      return 0;
    }
  } else {
    free(*dest);
    return -1;
  }
  return maxpklen;
  #else
  if ((*dest = malloc(len)) == NULL) return -1;
  memcpy(*dest, data, len);
  return 0;
  #endif
}


// return unpacked size
#ifdef HAVE_ZLIB
static int zlib_unpack (void *dest, int destsz, const void *pdata, int psize) {
  z_stream stream;
  int error, len;
  stream.zalloc = Z_NULL;
  stream.zfree = Z_NULL;
  stream.opaque = Z_NULL;
  stream.next_in = (void *)pdata;
  stream.avail_in = psize;
  stream.next_out = dest;
  stream.avail_out = destsz;
  if (inflateInit(&stream) != Z_OK) {
    //fprintf(stderr, "inflateInit() failed!\n");
    inflateEnd(&stream);
    return -1;
  }
  error = inflate(&stream, Z_FINISH);
  len = stream.next_out-((Bytef *)dest);
  if (error == Z_STREAM_END) {
    if (inflateEnd(&stream) != Z_OK) {
      //fprintf(stderr, "inflateEnd() failed! (len=%d)\n", len);
      return -1;
    }
    //fprintf(stderr, "unpacked len=%d\n", len);
    return len;
  }
  //fprintf(stderr, "inflate() failed! (%d) (len=%d)\n", error, len);
  inflateEnd(&stream);
  return -1;
}
#endif


////////////////////////////////////////////////////////////////////////////////
static int write_ram (FILE *fl, const void *ptr, int pgno, int dontpack) {
  zxs_ram_page_t ram;
  void *pkd;
  int pksz;
  memset(&ram, 0, sizeof(ram));
  memcpy(ram.blk.dwId, ZXS_RAM_PAGE_SIGN, 4);
  ram.blk.dwSize = sizeof(zxs_ram_page_t)-sizeof(zxs_block_t);
  ram.wFlags = 0;
  ram.chPageNo = pgno;
  switch ((pksz = zlib_pack(&pkd, ptr, 16384, dontpack))) {
    case -1: return -1;
    case 0: // can't pack
      pksz = 16384;
      //fprintf(stderr, "RAM%d: can't pack\n", pgno);
      break;
    default: // packed
      ram.wFlags |= ZXS_RAM_PAGE_FLAG_COMPRESSED;
      //fprintf(stderr, "RAM%d: packed to %d\n", pgno, pksz);
      break;
  }
  ram.blk.dwSize += pksz;
  #ifdef ZXS_BIG_ENDIAN
  ram.blk.dwSize = ZXS_SWAP_U32(ram.blk.dwSize);
  #endif
  if (fwrite(&ram, sizeof(ram), 1, fl) != 1) { free(pkd); return -1; }
  if (fwrite(pkd, pksz, 1, fl) != 1) { free(pkd); return -1; }
  free(pkd);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
// return 0 if ok
// return -1 on error, block skipped
// return -2 on inrecoverable error
static int read_ram (void *ptr, const zxs_ram_page_t *ram) {
  size_t sz = ram->blk.dwSize-(sizeof(zxs_ram_page_t)-sizeof(zxs_block_t));
  //fprintf(stderr, "read_ram: page=%u; flags=%u; sz=%d\n", ram->chPageNo, ram->wFlags, sz);
  if (sz > 1024*1024 || sz == 0) return -1; // skip
  if (ram->wFlags&ZXS_RAM_PAGE_FLAG_COMPRESSED) {
    #ifdef HAVE_ZLIB
    if (zlib_unpack(ptr, 16384, ram->chData, sz) != 16384) {
      return -1; // skip
    }
    return 0;
    #else
    return -1; // skip
    #endif
  }
  if (sz != 16384) return -1; // skip
  memcpy(ptr, ram->chData, 16384);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
// -2: fatal error
// -1: chunk skipped due to error
//  0: ok
typedef int (*chunk_parse_cb) (zxs_t *snap, zxs_block_t *blk);

typedef struct {
  const char sign[4];
  size_t size; // 0: don't read block data; else: size WITH zxs_block_t
  chunk_parse_cb parser;
} szx_loader_t;


////////////////////////////////////////////////////////////////////////////////
static int parse_block_store (zxs_t *snap, zxs_block_t *blk) {
  if (zxs_chunk_replace(snap, blk) == NULL) return -2;
  return 0;
}


static int parse_block_regs (zxs_t *snap, zxs_block_t *blk) {
  if (blk->dwSize < sizeof(zxs_z80regs_t)-sizeof(zxs_block_t)) return -2; // error
  if (blk->dwSize > sizeof(zxs_z80regs_t)-sizeof(zxs_block_t)) fprintf(stderr, "SZX block [%s] is bigger than expected by %u bytes\n", zxs_blk_name(blk), (unsigned)(sizeof(zxs_z80regs_t)-sizeof(zxs_block_t)-blk->dwSize));
  zxs_z80regs_t *regs = (zxs_z80regs_t *)blk;
  #ifdef ZXS_BIG_ENDIAN
  regs->AF = ZXS_SWAP_U16(regs->AF);
  regs->BC = ZXS_SWAP_U16(regs->BC);
  regs->DE = ZXS_SWAP_U16(regs->DE);
  regs->HL = ZXS_SWAP_U16(regs->HL);
  regs->AF1 = ZXS_SWAP_U16(regs->AF1);
  regs->BC1 = ZXS_SWAP_U16(regs->BC1);
  regs->DE1 = ZXS_SWAP_U16(regs->DE1);
  regs->HL1 = ZXS_SWAP_U16(regs->HL1);
  regs->IX = ZXS_SWAP_U16(regs->IX);
  regs->IY = ZXS_SWAP_U16(regs->IY);
  regs->SP = ZXS_SWAP_U16(regs->SP);
  regs->PC = ZXS_SWAP_U16(regs->PC);
  regs->wMemPtr = ZXS_SWAP_U16(regs->wMemPtr);
  regs->dwCyclesStart = ZXS_SWAP_U32(regs->dwCyclesStart);
  #endif
  regs->IFF1 = !!regs->IFF1;
  regs->IFF2 = !!regs->IFF2;
  if (regs->IM > 2) regs->IM = 2;
  if (snap->hdr.chMinorVersion < 1) regs->chFlags = 0;
  if (snap->hdr.chMinorVersion < 4) regs->wMemPtr = 0;
  if (zxs_chunk_replace(snap, blk) == NULL) return -2;
  return 0;
}


static int parse_block_ram (zxs_t *snap, zxs_block_t *blk) {
  if (blk->dwSize < sizeof(zxs_ram_page_t)-sizeof(zxs_block_t)) return -2; // error
  zxs_ram_page_t *ram = (zxs_ram_page_t *)blk;
  #ifdef ZXS_BIG_ENDIAN
  ram.wFlags = ZXS_SWAP_U16(ram.wFlags);
  #endif
  if (ram->chPageNo < 64) {
    zxs_page_t pg;
    int res = read_ram(pg.data, ram);
    //fprintf(stderr, "RAMP: page=%d; res=%d\n", (int)ram->chPageNo, res);
    if (res < 0) return res; // error
    pg.blk = *blk;
    pg.blk.dwSize = sizeof(zxs_page_t)-sizeof(zxs_block_t);
    pg.pageNo = ram->chPageNo;
    if (zxs_chunk_replace(snap, (void *)&pg) == NULL) return -2;
  } else {
    fprintf(stderr, "RAMP%u: do not want!\n", ram->chPageNo);
    return -1; // skipped
  }
  return 0;
}


static int parse_block_b128 (zxs_t *snap, zxs_block_t *blk) {
  if (blk->dwSize < sizeof(zxs_b128_t)-sizeof(zxs_block_t)) return -2; // error
  size_t sz = blk->dwSize-(sizeof(zxs_b128_t)-sizeof(zxs_block_t));
  if (sz == 0 || sz > 1024*1024) return -1; // skipped
  zxs_b128_t *b1 = (zxs_b128_t *)blk;
  #ifdef ZXS_BIG_ENDIAN
  b1->dwFlags = ZXS_SWAP_U32(b1->dwFlags);
  #endif
  if (b1->dwFlags&ZXS_B128_FLAG_CUSTOMROM) {
    // have custom rom
    zxs_b128_t *beta;
    if (sz < 1) return -1; // skipped
    if ((beta = malloc(sizeof(zxs_b128_t)+16384)) == NULL) return -1; // skipped
    *beta = *b1;
    if (b1->dwFlags&ZXS_B128_FLAG_COMPRESSED) {
      // compressed, it's hard
      #if HAVE_ZLIB
      if (zlib_unpack(beta->chRomData, 16384, b1->chRomData, sz) != 16384) {
        free(beta);
        return -1; // skipped
      }
      beta->blk.dwSize = sizeof(zxs_b128_t)-sizeof(zxs_block_t)+0x4000;
      #else
      free(beta);
      return -1; // skipped
      #endif
    } else {
      // uncompressed, it's easy
      if (sz != 16384) {
        free(beta);
        return -1; // skipped
      }
      memcpy(beta->chRomData, b1->chRomData, 0x4000);
    }
    if (zxs_chunk_replace(snap, (void *)beta) == NULL) { free(beta); return -2; }
    free(beta);
  } else {
    if (zxs_chunk_replace(snap, (void *)&b1) == NULL) return -2;
  }
  return 0;
}


static int parse_block_diskimg (zxs_t *snap, zxs_block_t *blk) {
  if (blk->dwSize < sizeof(zxs_diskimg_t)-sizeof(zxs_block_t)) return -2; // error
  if (blk->dwSize > 1024*1024*3) return -1; // skip
  zxs_diskimg_t *b1 = (zxs_diskimg_t *)blk;
  #ifdef ZXS_BIG_ENDIAN
  b1->dwFlags = ZXS_SWAP_U32(b1->dwFlags);
  #endif
  if (zxs_chunk_replace(snap, (void *)b1) == NULL) return -2;
  return 0;
}


static int parse_block_creator (zxs_t *snap, zxs_block_t *blk) {
  if (blk->dwSize < sizeof(zxs_creator_t)-sizeof(zxs_block_t)) return -2; // error
  if (blk->dwSize > 65536) return -1; // skip
  zxs_creator_t *b1 = (zxs_creator_t *)blk;
  #ifdef ZXS_BIG_ENDIAN
  b1->chMajorVersion = ZXS_SWAP_U16(b1->chMajorVersion);
  b1->chMinorVersion = ZXS_SWAP_U16(b1->chMinorVersion);
  #endif
  if (zxs_chunk_replace(snap, (void *)b1) == NULL) return -2;
  return 0;
}


static int parse_block_keyb (zxs_t *snap, zxs_block_t *blk) {
  #ifdef ZXS_BIG_ENDIAN
  zxs_keyb_t *b1 = (zxs_keyb_t *)blk;
  b1->dwFlags = ZXS_SWAP_U32(b1->dwFlags);
  if (zxs_chunk_replace(snap, blk) == NULL) return -2;
  return 0;
  #else
  return parse_block_store(snap, blk);
  #endif
}


static int parse_block_rom (zxs_t *snap, zxs_block_t *blk) {
  if (blk->dwSize < sizeof(zxs_rom_t)-sizeof(zxs_block_t)) return -2; // error
  if (blk->dwSize > 65536) return -1; // skip
  zxs_rom_t *b1 = (zxs_rom_t *)blk;
  #ifdef ZXS_BIG_ENDIAN
  b1->wFlags = ZXS_SWAP_U16(b1->wFlags);
  b1->dwUncompressedSize = ZXS_SWAP_U32(b1->dwUncompressedSize);
  #endif
  if (zxs_chunk_replace(snap, (void *)b1) == NULL) { free(b1); return -2; }
  free(b1);
  return 0;
}


static const szx_loader_t loaders[] = {
  {.sign=ZXS_Z80REGS_SIGN, .size=sizeof(zxs_z80regs_t), .parser=parse_block_regs},
  {.sign=ZXS_ZXREGS_SIGN, .size=sizeof(zxs_zxregs_t), .parser=parse_block_store},
  {.sign=ZXS_AYREGS_SIGN, .size=sizeof(zxs_ayregs_t), .parser=parse_block_store},
  {.sign=ZXS_KEYB_SIGN, .size=sizeof(zxs_keyb_t), .parser=parse_block_keyb},
  {.sign=ZXS_MOUSE_SIGN, .size=sizeof(zxs_mouse_t), .parser=parse_block_store},
  {.sign=ZXS_RAM_PAGE_SIGN, .size=0, .parser=parse_block_ram},
  {.sign=ZXS_ROM_SIGN, .size=0, .parser=parse_block_rom},
  {.sign=ZXS_B128_SIGN, .size=0, .parser=parse_block_b128},
  {.sign=ZXS_DISKIMG_SIGN, .size=0, .parser=parse_block_diskimg},
  {.sign=ZXS_CREATOR_SIGN, .size=0, .parser=parse_block_creator},
};


////////////////////////////////////////////////////////////////////////////////
int zxs_snap_load_szx (zxs_t *snap, FILE *fl, int flags) {
  if (fl == NULL || snap == NULL) return -1;
  zxs_clear(snap);
  if (fseek(fl, 0, SEEK_SET) != 0) goto error;
  if (fread(&snap->hdr, sizeof(snap->hdr), 1, fl) != 1) goto error;
  if (memcmp(snap->hdr.dwMagic, ZXS_HEADER_SIGN, 4) != 0) goto error;
  if (snap->hdr.chMachineId >= ZXS_MACH_MAX) goto error;
  //fprintf(stderr, "ZXST: %u.%u (%u)\n", snap->hdr.chMajorVersion, snap->hdr.chMinorVersion, snap->hdr.chMachineId);
  if (snap->hdr.chMajorVersion != 1) goto error;
  //int fpos = (int)sizeof(snap->hdr);
  //fprintf(stderr, "%d == %d\n", fpos, (int)ftell(fl));
  for (;;) {
    zxs_block_t blk;
    size_t rd = fread(&blk.dwId[0], 4, 1, fl);
    if (rd == 0) break; // no more
    if (rd != 1) goto error;
    rd = fread(&blk.dwSize, 4, 1, fl);
    if (rd == 0) break; // no more
    if (rd != 1) goto error;
    //fpos += 8;
    #ifdef ZXS_BIG_ENDIAN
    blk.dwSize = ZXS_SWAP_U32(blk.dwSize);
    #endif
    //fprintf(stderr, "BLOCK: [%s] (%u) (datapos=0x%08x; nbpos=0x%08x)\n", zxs_blk_name(&blk), blk.dwSize, (unsigned)fpos, (unsigned)fpos+blk.dwSize);
    if (blk.dwSize > 0x1fffffffu) goto error; // block too long
    //fpos += blk.dwSize;
    int blocknum = -1;
    for (size_t f = 0; f < sizeof(loaders)/sizeof(szx_loader_t); ++f) {
      if (memcmp(blk.dwId, loaders[f].sign, 4) == 0) { blocknum = (int)f; break; }
    }
    //fprintf(stderr, "BLOCK: [%s] (%u) (datapos=0x%08x; nbpos=0x%08x; blocknum=%d)\n", zxs_blk_name(&blk), blk.dwSize, (unsigned)fpos, (unsigned)fpos+blk.dwSize, blocknum);
    if (blocknum >= 0) {
      uint8_t *data;
      if ((data = malloc(blk.dwSize+sizeof(blk))) == NULL) goto error;
      memcpy(data, &blk, sizeof(blk));
      if (blk.dwSize != 0) { if (fread(data+sizeof(blk), blk.dwSize, 1, fl) != 1) { free(data); goto error; } }
      /*
      if (blk.dwSize != loaders[blocknum].size-sizeof(zxs_block_t)) {
        fprintf(stderr, "SZX loader: known block [%s] should be %u bytes, but it is %u bytes\n", zxs_blk_name(&blk), blk.dwSize, (unsigned)(loaders[blocknum].size-sizeof(zxs_block_t)));
      }
      */
      int err;
      if (loaders[blocknum].parser != NULL) {
        err = (loaders[blocknum].parser(snap, (void *)data) <= -2);
      } else {
        // just keep this one
        err = (zxs_chunk_replace(snap, (void *)data) == NULL);
      }
      free(data);
      if (err) goto error; // fatal error
      continue;
    }
    // unknown block
    if (flags&ZXS_SZX_KEEP_UNKNOWN_BLOCKS) {
      if (blk.dwSize <= 1024*1024*2) {
        uint8_t *data;
        if ((data = malloc(blk.dwSize+sizeof(zxs_block_t))) == NULL) goto error;
        memcpy(data, &blk, sizeof(blk));
        if (blk.dwSize != 0) { if (fread(data+sizeof(blk), blk.dwSize, 1, fl) != 1) { free(data); goto error; } }
        int err = (zxs_chunk_replace(snap, (void *)data) == NULL);
        free(data);
        if (err) goto error; // fatal error
        continue;
      }
      fprintf(stderr, "SZX loader: unknown block too big: [%s] (%u)\n", zxs_blk_name(&blk), blk.dwSize);
    }
    // we don't need this block
    //fprintf(stderr, "BLOCK SKIP: [%s] (%u) (blkpos=0x%08x)\n", zxs_blk_name(&blk), blk.dwSize, (unsigned)fpos);
    fseek(fl, blk.dwSize, SEEK_CUR);
  }
  return 0;
error:
  zxs_clear(snap);
  return -1;
}


#ifdef ZXS_BIG_ENDIAN
#define WRITE_BLOCK(blk_)  do { \
  const zxs_block_t *b_ = (const zxs_block_t *)(blk_); \
  if (b_ != NULL) { \
    uint32_t sz = ZXS_SWAP_U32(b_->dwSize); \
    if (fwrite(b_, 4, 1, fl) != 1) { waserror = 1; return -1; } \
    if (fwrite(&sz, 4, 1, fl) != 1) { waserror = 1; return -1; } \
    if (b_->dwSize > 0 && fwrite(((const uint8_t *)b_)+sizeof(zxs_block_t), b_->dwSize, 1, fl) != 1) { waserror = 1; return -1; } \
  } \
} while (0)
#define WRITE_BLOCK_FIX(blk_,fixer_)  do { \
  const zxs_block_t *b_ = (blk_); \
  if (b_ != NULL) { \
    zxs_block_t *bb_ = malloc(b_->dwSize+sizeof(zxs_block_t)); \
    if (bb_ == NULL) { waserror = 1; return -1; } \
    memcpy(bb_, b_, b_->dwSize+sizeof(zxs_block_t)); \
    bb_->dwSize = ZXS_SWAP_U32(bb_->dwSize); \
    (fixer_)((void *)bb_); \
    if (fwrite(bb_, bb_->dwSize+sizeof(zxs_block_t), 1, fl) != 1) { free(bb_); waserror = 1; return -1; } \
    free(bb_); \
  } \
} while (0)
#else
#define WRITE_BLOCK(blk_)  do { \
  const zxs_block_t *b_ = (blk_); \
  if (b_ != NULL) { \
    if (fwrite(b_, b_->dwSize+sizeof(zxs_block_t), 1, fl) != 1) { waserror = 1; return -1; } \
  } \
} while (0)
#define WRITE_BLOCK_FIX(blk_,xfixer_)  do { \
  const zxs_block_t *b_ = (blk_); \
  if (b_ != NULL) { \
    if (fwrite(b_, b_->dwSize+sizeof(zxs_block_t), 1, fl) != 1) { waserror = 1; return -1; } \
  } \
} while (0)
#endif


#ifdef ZXS_BIG_ENDIAN
static void blockfix_creator (zxs_creator_t *b1) {
  b1->chMajorVersion = ZXS_SWAP_U16(b1->chMajorVersion);
  b1->chMinorVersion = ZXS_SWAP_U16(b1->chMinorVersion);
}


static void blockfix_regs (zxs_z80regs_t *regs) {
  regs->AF = ZXS_SWAP_U16(regs->AF);
  regs->BC = ZXS_SWAP_U16(regs->BC);
  regs->DE = ZXS_SWAP_U16(regs->DE);
  regs->HL = ZXS_SWAP_U16(regs->HL);
  regs->AF1 = ZXS_SWAP_U16(regs->AF1);
  regs->BC1 = ZXS_SWAP_U16(regs->BC1);
  regs->DE1 = ZXS_SWAP_U16(regs->DE1);
  regs->HL1 = ZXS_SWAP_U16(regs->HL1);
  regs->IX = ZXS_SWAP_U16(regs->IX);
  regs->IY = ZXS_SWAP_U16(regs->IY);
  regs->SP = ZXS_SWAP_U16(regs->SP);
  regs->PC = ZXS_SWAP_U16(regs->PC);
  regs->wMemPtr = ZXS_SWAP_U16(regs->wMemPtr);
  regs->dwCyclesStart = ZXS_SWAP_U32(regs->dwCyclesStart);
}
#endif


int zxs_snap_save_szx (FILE *fl, zxs_t *snap, int flags) {
  int waserror = 0;
  if (fl == NULL || snap == NULL) return -1;
  if (fwrite(&snap->hdr, sizeof(snap->hdr), 1, fl) != 1) return -1;
  WRITE_BLOCK_FIX(zxs_chunk_find(snap, ZXS_CREATOR_SIGN), blockfix_creator);
  WRITE_BLOCK_FIX(zxs_chunk_find(snap, ZXS_Z80REGS_SIGN), blockfix_regs);
  WRITE_BLOCK(zxs_chunk_find(snap, ZXS_ZXREGS_SIGN));
  zxs_chunk_enum(snap, zxsnap_lambda(int, (zxs_t *snap, zxs_chunk_t *c, void *udata) {
    if (memcmp(c->data, ZXS_CREATOR_SIGN, 4) == 0 ||
        memcmp(c->data, ZXS_Z80REGS_SIGN, 4) == 0 ||
        memcmp(c->data, ZXS_ZXREGS_SIGN, 4) == 0)
    {
      return 0; // skip this blocks
    }
    if (memcmp(c->data, ZXS_RAM_PAGE_SIGN, 4) == 0) {
      // RAM block; special
      zxs_page_t *rpg = (zxs_page_t *)c->data;
      if (write_ram(fl, rpg->data, rpg->pageNo, (flags&ZXS_SZX_DONT_COMPRESS)) != 0) { waserror = 1; return -1; }
#ifdef HAVE_ZLIB
    } else if ((flags&ZXS_SZX_DONT_COMPRESS) == 0 && memcmp(c->data, ZXS_B128_SIGN, 4) == 0 &&
               (((const zxs_b128_t *)c->data)->dwFlags&(ZXS_B128_FLAG_CUSTOMROM|ZXS_B128_FLAG_COMPRESSED)) == ZXS_B128_FLAG_CUSTOMROM &&
               ((const zxs_b128_t *)c->data)->blk.dwSize == sizeof(zxs_b128_t)-sizeof(zxs_block_t)+0x4000)
    {
      // compress B128 block with custom ROM
      void *buf = NULL;
      int psz;
      zxs_b128_t beta;
      psz = zlib_pack(&buf, ((const zxs_b128_t *)c->data)->chRomData, 16384, 0);
      if (psz <= 0 || psz > 16383) { if (buf != NULL) free(buf); goto writeasis; }
      beta = *((zxs_b128_t *)c->data);
      beta.dwFlags |= ZXS_B128_FLAG_COMPRESSED;
      beta.blk.dwSize = (sizeof(beta)-sizeof(beta.blk))+psz;
#ifdef ZXS_BIG_ENDIAN
      beta.blk.dwSize = ZXS_SWAP_U32(beta.blk.dwSize);
      beta.dwFlags = ZXS_SWAP_U32(beta.dwFlags);
#endif
      if (fwrite(&beta, sizeof(beta), 1, fl) != 1) { waserror = 1; free(buf); return -1; }
      if (fwrite(buf, psz, 1, fl) != 1) { waserror = 1; free(buf); return -1; }
      free(buf);
#endif
    } else {
#ifdef HAVE_ZLIB
writeasis:
#endif
      // other blocks
#ifdef ZXS_BIG_ENDIAN
      if (memcmp(c->data, ZXS_B128_SIGN, 4) == 0) {
        // need to fix flags
        zxs_b128_t beta;
        beta = *((zxs_b128_t *)c->data);
        beta.blk.dwSize = ZXS_SWAP_U32(beta.blk.dwSize);
        beta.dwFlags = ZXS_SWAP_U32(beta.dwFlags);
        if (fwrite(&beta, sizeof(beta), 1, fl) != 1) { waserror = 1; return -1; }
        if (c->data->dwSize > sizeof(zxs_b128_t)) {
          if (fwrite(((const uint8_t *)c->data)+sizeof(zxs_b128_t), c->data->dwSize-sizeof(zxs_b128_t), 1, fl) != 1) { waserror = 1; return -1; }
        }
        return 0;
      }
      if (memcmp(c->data, ZXS_KEYB_SIGN, 4) == 0) {
        // need to fix flags
        zxs_keyb_t kb = *((zxs_keyb_t *)c->data);
        kb.dwFlags = ZXS_SWAP_U32(kb.dwFlags);
        WRITE_BLOCK(&kb);
        return 0;
      }
      if (memcmp(c->data, ZXS_ROM_SIGN, 4) == 0) {
        // need to fix flags
        zxs_rom_t rom = *((zxs_rom_t *)c->data);
        rom.blk.dwSize = ZXS_SWAP_U32(rom.blk.dwSize);
        rom.wFlags = ZXS_SWAP_U16(rom.wFlags);
        rom.dwUncompressedSize = ZXS_SWAP_U32(rom.dwUncompressedSize);
        if (fwrite(&rom, sizeof(rom), 1, fl) != 1) { waserror = 1; return -1; }
        if (c->data->dwSize > sizeof(zxs_rom_t)) {
          if (fwrite(((const uint8_t *)c->data)+sizeof(zxs_rom_t), c->data->dwSize-sizeof(zxs_rom_t), 1, fl) != 1) { waserror = 1; return -1; }
        }
        return 0;
      }
      if (memcmp(c->data, ZXS_DISKIMG_SIGN, 4) == 0) {
        // need to fix flags
        zxs_diskimg_t dimg = *((zxs_diskimg_t *)c->data);
        dimg.blk.dwSize = ZXS_SWAP_U32(dimg.blk.dwSize);
        dimg.dwFlags = ZXS_SWAP_U32(dimg.dwFlags);
        if (fwrite(&dimg, sizeof(dimg), 1, fl) != 1) { waserror = 1; return -1; }
        if (c->data->dwSize > sizeof(zxs_diskimg_t)) {
          if (fwrite(((const uint8_t *)c->data)+sizeof(zxs_diskimg_t), c->data->dwSize-sizeof(zxs_diskimg_t), 1, fl) != 1) { waserror = 1; return -1; }
        }
        return 0;
      }
#endif
      WRITE_BLOCK(c->data);
    }
    return 0;
  }), NULL, NULL);
  return (waserror ? -1 : 0);
}
