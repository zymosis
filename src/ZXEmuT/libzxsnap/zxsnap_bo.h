/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef LIBZXS_BO_H
#define LIBZXS_BO_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


//#define ZXS_BIG_ENDIAN


////////////////////////////////////////////////////////////////////////////////
/* this defines is not intended to be user-serviceable */
#if !defined(ZXS_LITTLE_ENDIAN) && !defined(ZXS_BIG_ENDIAN)
# if defined(__GNUC__)
#  if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#   define ZXS_LITTLE_ENDIAN
#  elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#   define ZXS_BIG_ENDIAN
#  else
#   error Is this really PDP? C'mon, don't shit me!
#  endif
# else
#  error wtf?! ZXSnap endiannes is not defined!
# endif
#endif

#if defined(ZXS_LITTLE_ENDIAN) && defined(ZXS_BIG_ENDIAN)
# error wtf?! ZXSnap endiannes double defined! are you nuts?
#endif


#ifdef ZXS_LITTLE_ENDIAN
# define ZXS_SWAP_U16(value)  (value)
# define ZXS_SWAP_U32(value)  (value)
#else
# define ZXS_SWAP_U16(value)  ((uint16_t)__builtin_bswap16((uint16_t)value))
# define ZXS_SWAP_U32(value)  ((uint32_t)__builtin_bswap32((uint16_t)value))
#endif


#ifdef __cplusplus
}
#endif
#endif
