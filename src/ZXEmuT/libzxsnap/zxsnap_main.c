/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "zxsnap_bo.h"
#include "zxsnap_main.h"


////////////////////////////////////////////////////////////////////////////////
int zxs_fread_buf (FILE *fl, void *buf, size_t bytes) {
  if (fl != NULL && buf != NULL) {
    if (bytes != 0) {
      if (fread(buf, bytes, 1, fl) != 1) return -1;
    }
    return 0;
  }
  return -1;
}


int zxs_fread_u8 (FILE *fl, void *buf) {
  if (fl != NULL && buf != NULL) {
    return (fread(buf, 1, 1, fl) == 1 ? 0 : -1);
  }
  return -1;
}


int zxs_fread_u16 (FILE *fl, void *buf) {
  if (fl != NULL && buf != NULL) {
#ifdef ZXS_BIG_ENDIAN
    uint16_t v;
    if (fread(&v, 2, 1, fl) != 1) return -1;
    v = ZXS_SWAP_U16(v);
    memcpy(buf, &v, 2);
    return 0;
#else
    return (fread(buf, 2, 1, fl) == 1 ? 0 : -1);
#endif
  }
  return -1;
}


int zxs_fread_u32 (FILE *fl, void *buf) {
  if (fl != NULL && buf != NULL) {
#ifdef ZXS_BIG_ENDIAN
    uint32_t v;
    if (fread(&v, 4, 1, fl) != 1) return -1;
    v = ZXS_SWAP_U32(v);
    memcpy(buf, &v, 4);
    return 0;
#else
    return (fread(buf, 4, 1, fl) == 1 ? 0 : -1);
#endif
  }
  return -1;
}


int zxs_fwrite_buf (FILE *fl, const void *buf, size_t bytes) {
  if (fl != NULL) {
    if (bytes != 0) {
      if (buf == NULL) return -1;
      if (fwrite(buf, bytes, 1, fl) != 1) return -1;
    }
    return 0;
  }
  return -1;
}


int zxs_fwrite_u8 (FILE *fl, uint8_t v) {
  if (fl != NULL) {
    return (fwrite(&v, 1, 1, fl) == 1 ? 0 : -1);
  }
  return -1;
}


int zxs_fwrite_u16 (FILE *fl, uint16_t v) {
  if (fl != NULL) {
#ifdef ZXS_BIG_ENDIAN
    v = ZXS_SWAP_U16(v);
#endif
    return (fwrite(&v, 2, 1, fl) == 1 ? 0 : -1);
  }
  return -1;
}


int zxs_fwrite_u32 (FILE *fl, uint32_t v) {
  if (fl != NULL) {
#ifdef ZXS_BIG_ENDIAN
    v = ZXS_SWAP_U32(v);
#endif
    return (fwrite(&v, 4, 1, fl) == 1 ? 0 : -1);
  }
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
zxs_t *zxs_alloc (void) {
  zxs_t *snap = calloc(1, sizeof(zxs_t));
  if (snap != NULL) {
    memcpy(snap->hdr.dwMagic, ZXS_HEADER_SIGN, 4);
    snap->hdr.chMajorVersion = 1;
    snap->hdr.chMinorVersion = 4;
  }
  return snap;
}


void zxs_clear (zxs_t *snap) {
  if (snap != NULL) {
    while (snap->first != NULL) {
      zxs_chunk_t *c = snap->first;
      snap->first = c->next;
      if (c->data != NULL) free(c->data);
      free(c);
    }
    memset(snap, 0, sizeof(zxs_t));
    memcpy(snap->hdr.dwMagic, ZXS_HEADER_SIGN, 4);
    snap->hdr.chMajorVersion = 1;
    snap->hdr.chMinorVersion = 4;
  }
}


void zxs_free (zxs_t *snap) {
  if (snap != NULL) {
    zxs_clear(snap);
    free(snap);
  }
}


zxs_chunk_t *zxs_chunk_enum (zxs_t *snap, zxs_chunk_enum_cb_t ccmp, void *udata, zxs_chunk_t **prev) {
  if (snap != NULL && ccmp != NULL) {
    zxs_chunk_t *p = NULL;
    for (zxs_chunk_t *c = snap->first; c != NULL; p = c, c = c->next) {
      int res = ccmp(snap, c, udata);
      if (res < 0) break; // stop right here
      if (res > 0) {
        // found
        if (prev) *prev = p;
        return c;
      }
      // go on
    }
  }
  if (prev) *prev = NULL;
  return NULL;
}


zxs_block_t *zxs_chunk_find (zxs_t *snap, const char sign[4]) {
  zxs_chunk_t *r = zxs_chunk_enum(snap, zxsnap_lambda(int, (zxs_t *snap, zxs_chunk_t *c, void *udata) {
    if (memcmp(c->data, sign, 4) == 0) return 1;
    return 0;
  }), NULL, NULL);
  return (r != NULL ? r->data : NULL);
}


zxs_page_t *zxs_chunk_findram (zxs_t *snap, int pgno) {
  zxs_chunk_t *r = zxs_chunk_enum(snap, zxsnap_lambda(int, (zxs_t *snap, zxs_chunk_t *c, void *udata) {
    if (memcmp(c->data, ZXS_RAM_PAGE_SIGN, 4) == 0 && ((const zxs_page_t *)c->data)->pageNo == pgno) return 1;
    return 0;
  }), NULL, NULL);
  return (r != NULL ? (zxs_page_t *)r->data : NULL);
}


static void snap_remove_chunk (zxs_t *snap, zxs_chunk_t *p, zxs_chunk_t *c) {
  if (p != NULL) p->next = c->next; else snap->first = c->next;
  if (snap->last == c) snap->last = p;
  free(c->data);
  free(c);
}


int zxs_chunk_remove (zxs_t *snap, const char sign[4]) {
  zxs_chunk_t *p, *c = zxs_chunk_enum(snap, zxsnap_lambda(int, (zxs_t *snap, zxs_chunk_t *c, void *udata) {
    if (memcmp(c->data, sign, 4) == 0) return 1;
    return 0;
  }), NULL, &p);
  if (c != NULL) {
    snap_remove_chunk(snap, p, c);
    return 1;
  }
  return 0;
}


int zxs_chunk_removeram (zxs_t *snap, int pgno) {
  zxs_chunk_t *p, *c = zxs_chunk_enum(snap, zxsnap_lambda(int, (zxs_t *snap, zxs_chunk_t *c, void *udata) {
    if (memcmp(c->data, ZXS_RAM_PAGE_SIGN, 4) == 0 && ((const zxs_page_t *)c->data)->pageNo == pgno) return 1;
    return 0;
  }), NULL, &p);
  if (c != NULL) {
    snap_remove_chunk(snap, p, c);
    return 1;
  }
  return 0;
}


zxs_chunk_t *zxs_chunk_replace (zxs_t *snap, const zxs_block_t *data) {
  if (snap != NULL) {
    zxs_block_t *dn = malloc(data->dwSize+sizeof(zxs_block_t));
    if (dn != NULL) {
      zxs_chunk_t *c;
      int isRAM = (memcmp(data, ZXS_RAM_PAGE_SIGN, 4) == 0);
      memcpy(dn, data, data->dwSize+sizeof(zxs_block_t));
      for (c = snap->first; c != NULL; c = c->next) {
        if (memcmp(c->data, data, 4) == 0 &&
            (!isRAM || ((const zxs_page_t *)c->data)->pageNo == ((const zxs_page_t *)data)->pageNo)) {
          free(c->data);
          c->data = dn;
          return c;
        }
      }
      // new chunk
      if ((c = calloc(1, sizeof(zxs_chunk_t))) != NULL) {
        c->data = dn;
        if (snap->last != NULL) snap->last->next = c; else snap->first = c;
        snap->last = c;
        return c;
      }
      free(dn);
    }
  }
  return NULL;
}


int zxs_make_page (zxs_page_t *rpg, const void *mem, int pgno) {
  if (rpg != NULL && pgno >= 0 && pgno <= 255) {
    memcpy(rpg->blk.dwId, ZXS_RAM_PAGE_SIGN, 4);
    rpg->blk.dwSize = sizeof(zxs_page_t)-sizeof(rpg->blk);
    rpg->pageNo = pgno;
    if (mem != NULL && mem != rpg->data) memmove(rpg->data, mem, 0x4000);
    return 0;
  }
  return -1;
}
