/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef LIBZXS_MAIN_H
#define LIBZXS_MAIN_H

#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif


////////////////////////////////////////////////////////////////////////////////
/* you can define HAVE_ZLIB to use ZLib */


////////////////////////////////////////////////////////////////////////////////
// all the following return 0 on ok and <0 on error/premature eof
extern int zxs_fread_buf (FILE *fl, void *buf, size_t bytes);
extern int zxs_fread_u8 (FILE *fl, void *buf);
extern int zxs_fread_u16 (FILE *fl, void *buf);
extern int zxs_fread_u32 (FILE *fl, void *buf);

extern int zxs_fwrite_buf (FILE *fl, const void *buf, size_t bytes);
extern int zxs_fwrite_u8 (FILE *fl, uint8_t v);
extern int zxs_fwrite_u16 (FILE *fl, uint16_t v);
extern int zxs_fwrite_u32 (FILE *fl, uint32_t v);


////////////////////////////////////////////////////////////////////////////////
#ifndef ZXS_PACKED
# define ZXS_PACKED  __attribute__((packed)) __attribute__((gcc_struct))
#endif


#define zxsnap_lambda(return_type, body_and_args) ({ \
  return_type __fn__ body_and_args \
  __fn__; \
})


////////////////////////////////////////////////////////////////////////////////
#define ZXS_HEADER_SIGN  "ZXST"

// Machine identifiers
enum {
  ZXS_MACH_16K,
  ZXS_MACH_48K,
  ZXS_MACH_128K,
  ZXS_MACH_PLUS2,
  ZXS_MACH_PLUS2A,
  ZXS_MACH_PLUS3,
  ZXS_MACH_PLUS3E,
  ZXS_MACH_PENTAGON128,
  ZXS_MACH_TC2048,
  ZXS_MACH_TC2068,
  ZXS_MACH_SCORPION,
  ZXS_MACH_SE,
  ZXS_MACH_TS2068,
  ZXS_MACH_PENTAGON512,
  ZXS_MACH_PENTAGON1024,
  ZXS_MACH_NTSC48K,
  ZXS_MACH_128KE,
  ZXS_MACH_MAX
};

// Flags (chFlags)
#define ZXS_MF_ALTERNATE_TIMINGS  (0x01)


typedef struct ZXS_PACKED {
  char dwMagic[4]; // "ZXST"
  uint8_t chMajorVersion; // 1
  uint8_t chMinorVersion; // 4
  uint8_t chMachineId; // ZXS_MACH_XXX
  uint8_t chFlags; // ZXS_MF_ALTERNATETIMINGS
} zxs_header_t;


typedef struct ZXS_PACKED {
  char dwId[4];
  uint32_t dwSize; // data, w/o this header
} zxs_block_t;


////////////////////////////////////////////////////////////////////////////////
#define ZXS_Z80REGS_SIGN  "Z80R"

#define ZXS_Z80REGS_FLAG_EILAST  (0x01)
#define ZXS_Z80REGS_FLAG_HALTED  (0x02)

typedef struct ZXS_PACKED {
  zxs_block_t blk;
  uint16_t AF, BC, DE, HL;
  uint16_t AF1, BC1, DE1, HL1;
  uint16_t IX, IY, SP, PC;
  uint8_t I;
  uint8_t R;
  uint8_t IFF1, IFF2;
  uint8_t IM;
  uint32_t dwCyclesStart;
  uint8_t chHoldIntReqCycles;
  uint8_t chFlags;
  uint16_t wMemPtr;
} zxs_z80regs_t;

/*
The chFlags, chBitReg and chReserved members were added in version 1.1.
The size of the block is unchanged.

The wMemPtr member was added in version 1.4. It replaces the chBitReg
and chReserved members. The size of the block is unchanged.
*/


////////////////////////////////////////////////////////////////////////////////
#define ZXS_ZXREGS_SIGN  "SPCR"

typedef struct ZXS_PACKED {
  zxs_block_t blk;
  uint8_t chBorder;
  uint8_t ch7ffd;
  union {
    uint8_t ch1ffd;
    uint8_t chEff7;
  };
  uint8_t chFe;
  uint32_t chReserved;
} zxs_zxregs_t;


////////////////////////////////////////////////////////////////////////////////
#define ZXS_AYREGS_SIGN  "AY\0\0"

// AY Block flags
#define ZXS_AYREGS_FLAG_FULLERBOX  (0x01)
#define ZXS_AYREGS_FLAG_128AY      (0x02)

// AY Block. Contains the AY register values
typedef struct ZXS_PACKED {
  zxs_block_t blk;
  uint8_t chFlags;
  uint8_t chCurrentRegister;
  uint8_t chAyRegs[16];
} zxs_ayregs_t;


////////////////////////////////////////////////////////////////////////////////
#define ZXS_B128_SIGN  "B128"

// Beta 128 disk interface used by the Pentagon and Scorpion
#define ZXS_B128_FLAG_CONNECTED   (0x01)
#define ZXS_B128_FLAG_CUSTOMROM   (0x02)
#define ZXS_B128_FLAG_PAGED       (0x04)
#define ZXS_B128_FLAG_AUTOBOOT    (0x08)
#define ZXS_B128_FLAG_SEEKLOWER   (0x10)
#define ZXS_B128_FLAG_COMPRESSED  (0x20)

typedef struct ZXS_PACKED {
  zxs_block_t blk;
  uint32_t dwFlags;
  uint8_t chNumDrives;
  uint8_t chSysReg;
  uint8_t chTrackReg;
  uint8_t chSectorReg;
  uint8_t chDataReg;
  uint8_t chStatusReg;
  uint8_t chRomData[0];
} zxs_b128_t;


////////////////////////////////////////////////////////////////////////////////
#define ZXS_DISKIMG_SIGN  "BDSK"

// Beta 128 disk image

// Flags
#define ZXS_DISKIMG_FLAG_EMBEDDED      (0x01)
#define ZXS_DISKIMG_FLAG_COMPRESSED    (0x02)
#define ZXS_DISKIMG_FLAG_WRITEPROTECT  (0x04)

// Disk image types
enum {
  ZXS_DISKIMG_TRD,
  ZXS_DISKIMG_SCL,
  ZXS_DISKIMG_FDI,
  ZXS_DISKIMG_UDI
};


typedef struct ZXS_PACKED {
  zxs_block_t blk;
  uint32_t dwFlags;
  uint8_t chDriveNum;
  uint8_t chCylinder;
  uint8_t chDiskType;
  union {
    char szFileName[0];
    uint8_t chDiskImage[0];
  };
} zxs_diskimg_t;


////////////////////////////////////////////////////////////////////////////////
#define ZXS_CREATOR_SIGN  "CRTR"

typedef struct ZXS_PACKED {
  zxs_block_t blk;
  char szCreator[32];
  uint16_t chMajorVersion;
  uint16_t chMinorVersion;
  uint8_t chData[0];
} zxs_creator_t;


////////////////////////////////////////////////////////////////////////////////
#define ZXS_KEYB_SIGN  "KEYB"

// Keyboard state (only 48k)
#define ZXS_KEYB_FLAG_ISSUE2  (0x01)

// Supported joystick types
enum {
  ZXS_JOY_KEMPSTON,
  ZXS_JOY_FULLER,
  ZXS_JOY_CURSOR,
  ZXS_JOY_SINCLAIR1,
  ZXS_JOY_SINCLAIR2,
  ZXS_JOY_SPECTRUMPLUS,
  ZXS_JOY_TIMEX1,
  ZXS_JOY_TIMEX2,
  ZXS_JOY_NONE
};

typedef struct ZXS_PACKED {
  zxs_block_t blk;
  uint32_t dwFlags;
  uint8_t chKeyboardJoystick;
} zxs_keyb_t;


////////////////////////////////////////////////////////////////////////////////
#define ZXS_MOUSE_SIGN  "AMXM"

// Mouse (currently AMX and Kempston)
enum {
  ZXS_MOUSE_NONE,
  ZXS_MOUSE_AMX,
  ZXS_MOUSE_KEMPSTON
};

typedef struct ZXS_PACKED {
  zxs_block_t blk;
  uint8_t chType;
  uint8_t chCtrlA[3];
  uint8_t chCtrlB[3];
} zxs_mouse_t;


////////////////////////////////////////////////////////////////////////////////
#define ZXS_RAM_PAGE_SIGN  "RAMP"

// ROM/RAM pages are compressed using Zlib
#define ZXS_RAM_PAGE_FLAG_COMPRESSED  (0x01)

// Standard 16kb Spectrum RAM page
typedef struct ZXS_PACKED {
  zxs_block_t blk;
  uint16_t wFlags;
  uint8_t chPageNo;
  uint8_t chData[0];
} zxs_ram_page_t;


////////////////////////////////////////////////////////////////////////////////
#define ZXS_ROM_SIGN  "ROM\0"

// Custom ROM for the current model
// WARNING: SZX loader WILL NOT DECOMPRESS IT!
// WARNING: SZX writer WILL NOT COMPRESS IT!
typedef struct ZXS_PACKED {
  zxs_block_t blk;
  uint16_t wFlags;
  uint32_t dwUncompressedSize;
  uint8_t chData[0];
} zxs_rom_t;


////////////////////////////////////////////////////////////////////////////////
typedef struct ZXS_PACKED {
  zxs_block_t blk;
  uint8_t pageNo;
  uint8_t data[16384];
} zxs_page_t;


typedef struct _chunk {
  struct _chunk *next;
  zxs_block_t *data;
} zxs_chunk_t;


typedef struct {
  zxs_chunk_t *first;
  zxs_chunk_t *last;
  zxs_header_t hdr;
} zxs_t;


////////////////////////////////////////////////////////////////////////////////
extern zxs_t *zxs_alloc (void);
extern void zxs_free (zxs_t *snap);
extern void zxs_clear (zxs_t *snap);
extern zxs_chunk_t *zxs_chunk_replace (zxs_t *snap, const zxs_block_t *data); // copies data
extern zxs_block_t *zxs_chunk_find (zxs_t *snap, const char sign[4]);
extern zxs_page_t *zxs_chunk_findram (zxs_t *snap, int pgno);
extern int zxs_chunk_remove (zxs_t *snap, const char sign[4]); // >0: found and removed
extern int zxs_chunk_removeram (zxs_t *snap, int pgno); // >0: found and removed

// <0: error; 0: ok
// mem: either NULL or 16384 bytes of data
extern int zxs_make_page (zxs_page_t *rpg, const void *mem, int pgno);

// return:
//  <0: stop iterating, nothing's found
//   0: skip this chunk and continue
//  >0: stop iterating, the chunk is found
typedef int (*zxs_chunk_enum_cb_t) (zxs_t *snap, zxs_chunk_t *c, void *udata);

// if (prev != NULL): set it
extern zxs_chunk_t *zxs_chunk_enum (zxs_t *snap, zxs_chunk_enum_cb_t ccmp, void *udata, zxs_chunk_t **prev);


#ifdef __cplusplus
}
#endif
#endif
