/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef LIBZXS_GUESS_H
#define LIBZXS_GUESS_H

#include "zxsnap_main.h"

#ifdef __cplusplus
extern "C" {
#endif


////////////////////////////////////////////////////////////////////////////////
typedef enum {
  ZXS_SNAP_TYPE_UNKNOWN = 0,
  ZXS_SNAP_TYPE_SZX,
  ZXS_SNAP_TYPE_SNA,
  ZXS_SNAP_TYPE_Z80
} zxs_snap_type;


extern zxs_snap_type zxs_snap_guess (FILE *fl, const char *fname);


#ifdef __cplusplus
}
#endif
#endif
