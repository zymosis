/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "zxsnap_bo.h"
#include "zxsnap_guess.h"


////////////////////////////////////////////////////////////////////////////////
static inline int is_ext_equ (const char *fname, const char *ext) {
  return (fname != NULL && ext != NULL &&
    strlen(fname) > strlen(ext) && strcasecmp(fname+strlen(fname)-strlen(ext), ext) == 0);
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  const char *ext;
  zxs_snap_type type;
  int score; // the lower the better
  int (*checkFn) (FILE *fl, off_t fsize); // >0: uncertain (score+res); <0: bad snapshot; 0: certain
} snap_info_t;


////////////////////////////////////////////////////////////////////////////////
static int check_szx (FILE *fl, off_t fsize) {
  zxs_header_t hdr;
  if (fread(&hdr, sizeof(hdr), 1, fl) != 1) return -1;
  if (memcmp(hdr.dwMagic, ZXS_HEADER_SIGN, 4) != 0) return -1;
  if (hdr.chMajorVersion != 1) return -1;
  if (hdr.chMachineId >= ZXS_MACH_MAX) return 1;
  return 0; // good
}


static int check_sna (FILE *fl, off_t fsize) {
  switch (fsize) {
    case 49179:
    case 131103:
    case 147487:
      return 1; // uncertain
  }
  return -1; // bad
}


static int check_z80 (FILE *fl, off_t fsize) {
  if (fsize > 30) {
    if (fseek(fl, 6, SEEK_SET) == 0) {
      uint16_t w;
      if (fread(&w, 2, 1, fl) == 1) return (w == 0 || w == 0xffff ? 1 : 2); // uncertain, score+x; (0xffff: z80stealth)
    }
  }
  return -1; // bad
}


////////////////////////////////////////////////////////////////////////////////
static const snap_info_t snapExtInfo[] = {
  {.ext=".szx", .type=ZXS_SNAP_TYPE_SZX, .score=10, .checkFn=check_szx},
  {.ext=".sna", .type=ZXS_SNAP_TYPE_SNA, .score=20, .checkFn=check_sna},
  {.ext=".z80", .type=ZXS_SNAP_TYPE_Z80, .score=20, .checkFn=check_z80},
  {.ext=".slt", .type=ZXS_SNAP_TYPE_Z80, .score=20, .checkFn=check_z80},
  /*
  {.ext=".zxs", .type=ZXS_SNAP_TYPE_ZXS, .score=10, .checkFn=check_zxs}, //SNAP
  {.ext=".snp", .type=ZXS_SNAP_TYPE_SNP, .score=20, .checkFn=check_snp},
  {.ext=".sp",  .type=ZXS_SNAP_TYPE_SP, .score=16, .checkFn=check_sp}, //\x53\x50\0
  */
};


////////////////////////////////////////////////////////////////////////////////
static int find_by_name (const char *fname) {
  if (fname != NULL) {
    for (unsigned int f = 0; f < sizeof(snapExtInfo)/sizeof(snap_info_t); ++f) {
      if (is_ext_equ(fname, snapExtInfo[f].ext)) return (int)f;
    }
  }
  return -1;
}


static zxs_snap_type check_by_name (const char *fname) {
  int f = find_by_name(fname);
  if (f >= 0) return snapExtInfo[f].type;
  return ZXS_SNAP_TYPE_UNKNOWN;
}


zxs_snap_type zxs_snap_guess (FILE *fl, const char *fname) {
  int tpn;
  off_t fsize;
  int score = 65535, idx = -1, certain = 0;
  if (fl == NULL) return check_by_name(fname);
  if (fseeko(fl, 0, SEEK_END) != 0) return ZXS_SNAP_TYPE_UNKNOWN;
  fsize = ftell(fl);
  if ((tpn = find_by_name(fname)) >= 0) {
    // first check by ext
    if (fseeko(fl, 0, SEEK_SET) == 0) {
      int res = snapExtInfo[tpn].checkFn(fl, fsize);
      //
      if (res >= 0) {
        certain = (res == 0);
        score = snapExtInfo[tpn].score+res;
        idx = tpn;
      }
    }
  }
  for (unsigned int f = 0; f < sizeof(snapExtInfo)/sizeof(snap_info_t); ++f) {
    if ((int)f != idx && fseeko(fl, 0, SEEK_SET) == 0) {
      int res = snapExtInfo[f].checkFn(fl, fsize);
      if (res == 0) {
        // certain
        if (!certain || score > snapExtInfo[f].score) {
          certain = 1;
          score = snapExtInfo[f].score;
          idx = (int)f;
        }
      } else if (res > 0) {
        // uncertain
        if (!certain && score > snapExtInfo[f].score+res) {
          certain = 1;
          score = snapExtInfo[f].score;
          idx = (int)f;
        }
      }
    }
  }
  if (fseeko(fl, 0, SEEK_SET) == 0 && idx >= 0) return snapExtInfo[idx].type;
  return ZXS_SNAP_TYPE_UNKNOWN;
}
