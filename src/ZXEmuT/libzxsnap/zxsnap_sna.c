/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "zxsnap_bo.h"
#include "zxsnap_sna.h"


int zxs_snap_load_sna (zxs_t *snap, FILE *fl, int flags) {
  off_t fsize;
  zxs_zxregs_t ports;
  zxs_page_t ram;
  zxs_z80regs_t regs;
  uint8_t *memory = NULL;
  if (fl == NULL || snap == NULL) return -1;
  zxs_clear(snap);
  /* snapshot type */
  if (fseek(fl, 0, SEEK_END) != 0) goto error;
  fsize = ftell(fl);
  if (fseek(fl, 0, SEEK_SET) != 0) goto error;
  switch (fsize) {
    case 49179: snap->hdr.chMachineId = ZXS_MACH_48K; break;
    case 131103: case 147487: snap->hdr.chMachineId = ZXS_MACH_PENTAGON128; break;
    default: goto error;
  }
  /* ports */
  memset(&ports, 0, sizeof(ports));
  memcpy(ports.blk.dwId, ZXS_ZXREGS_SIGN, 4);
  ports.blk.dwSize = sizeof(ports)-sizeof(ports.blk);
  ports.ch1ffd = 0x00;
  ports.chFe = 0xff;
  /* z80 state */
  memset(&regs, 0, sizeof(regs));
  memcpy(regs.blk.dwId, ZXS_Z80REGS_SIGN, 4);
  regs.blk.dwSize = sizeof(regs)-sizeof(regs.blk);
  if (zxs_fread_u8(fl, &regs.I)) goto error;
  if (zxs_fread_u16(fl, &regs.HL1)) goto error;
  if (zxs_fread_u16(fl, &regs.DE1)) goto error;
  if (zxs_fread_u16(fl, &regs.BC1)) goto error;
  if (zxs_fread_u16(fl, &regs.AF1)) goto error;
  if (zxs_fread_u16(fl, &regs.HL)) goto error;
  if (zxs_fread_u16(fl, &regs.DE)) goto error;
  if (zxs_fread_u16(fl, &regs.BC)) goto error;
  if (zxs_fread_u16(fl, &regs.IY)) goto error;
  if (zxs_fread_u16(fl, &regs.IX)) goto error;
  if (zxs_fread_u8(fl, &regs.IFF1)) goto error;
  regs.IFF1 = regs.IFF2 = (regs.IFF1&0x04 ? 1 : 0);
  if (zxs_fread_u8(fl, &regs.R)) goto error;
  if (zxs_fread_u16(fl, &regs.AF)) goto error;
  if (zxs_fread_u16(fl, &regs.SP)) goto error;
  if (zxs_fread_u8(fl, &regs.IM)) goto error;
  if (regs.IM > 2) regs.IM = 2;
  if (zxs_fread_u8(fl, &ports.chBorder)) goto error;
  ports.chBorder &= 0x07;
  /* read paged-in RAM */
  if ((memory = calloc(1, 65536)) == NULL) goto error;
  if (fread(memory+0x4000, 0x4000*3, 1, fl) != 1) goto error;
  /* other RAM */
  zxs_make_page(&ram, memory+0x4000, 5);
  if (zxs_chunk_replace(snap, (void *)&ram) == NULL) goto error;
  zxs_make_page(&ram, memory+0x8000, 2);
  if (zxs_chunk_replace(snap, (void *)&ram) == NULL) goto error;
  if (fsize == 49179) {
    // 48k snapshot
    zxs_make_page(&ram, memory+0xC000, 0);
    if (zxs_chunk_replace(snap, (void *)&ram) == NULL) goto error;
    ports.ch7ffd = 0x20;
    // get PC
    //fprintf(stderr, "SP=#%04X\n", regs.SP);
    regs.PC = memory[regs.SP]|(memory[(regs.SP+1)&0xffff]<<8);
    regs.SP = (regs.SP+2)&0xffff;
    //fprintf(stderr, "PC=#%04X\n", regs.PC);
  } else {
    uint8_t trdospi;
    // 128k snapshot
    if (zxs_fread_u16(fl, &regs.PC)) goto error;
    if (zxs_fread_u8(fl, &ports.ch7ffd)) goto error;
    if (zxs_fread_u8(fl, &trdospi)) goto error;
    zxs_make_page(&ram, memory+0x4000*3, ports.ch7ffd&0x07);
    if (zxs_chunk_replace(snap, (void *)&ram) == NULL) goto error;
    for (int f = 0; f < 8; ++f) {
      if (f == 2 || f == 5 || f == (ports.ch7ffd&0x07)) continue;
      if (fread(memory, 0x4000, 1, fl) != 1) goto error;
      zxs_make_page(&ram, memory, f);
      if (zxs_chunk_replace(snap, (void *)&ram) == NULL) goto error;
    }
    if (trdospi) {
      zxs_b128_t beta;
      memset(&beta, 0, sizeof(beta));
      memcpy(beta.blk.dwId, ZXS_B128_SIGN, 4);
      beta.blk.dwSize = sizeof(beta)-sizeof(beta.blk);
      beta.dwFlags = ZXS_B128_FLAG_CONNECTED|ZXS_B128_FLAG_PAGED;
      beta.chNumDrives = 4;
      if (zxs_chunk_replace(snap, (void *)&beta) == NULL) goto error;
    }
    //fprintf(stderr, "SP=#%04X\n", regs.SP);
    //fprintf(stderr, "PC=#%04X\n", regs.PC);
  }
  free(memory);
  //zxs_regs_fix_byteorder(&regs);
  if (zxs_chunk_replace(snap, (void *)&regs) == NULL) goto error;
  if (zxs_chunk_replace(snap, (void *)&ports) == NULL) goto error;
  return 0;
error:
  if (memory != NULL) free(memory);
  zxs_clear(snap);
  return -1;
}


int zxs_snap_save_sna (FILE *fl, zxs_t *snap, int flags) {
  int waserror = 0;
  zxs_z80regs_t *regs;
  const zxs_zxregs_t *ports;
  zxs_page_t *ram[8];
  zxs_page_t *spp0 = NULL, *spp1 = NULL;
  int spp0a = 0, spp1a = 0;
  uint8_t spp0b = 0, spp1b = 0;
  int is48k = 0;
  uint8_t t;
  if (fl == NULL || snap == NULL) return -1;
  switch (snap->hdr.chMachineId) {
    case ZXS_MACH_48K:
    case ZXS_MACH_NTSC48K:
      is48k = 1;
      break;
    case ZXS_MACH_128K:
    case ZXS_MACH_PLUS2:
    case ZXS_MACH_PLUS2A:
    case ZXS_MACH_PLUS3:
    case ZXS_MACH_PLUS3E:
    case ZXS_MACH_PENTAGON128:
    case ZXS_MACH_SE:
    case ZXS_MACH_128KE:
      is48k = 0;
      break;
    default: return -1;
  }
  regs = (zxs_z80regs_t *)zxs_chunk_find(snap, ZXS_Z80REGS_SIGN);
  ports = (const zxs_zxregs_t *)zxs_chunk_find(snap, ZXS_ZXREGS_SIGN);
  if (regs == NULL || ports == NULL) return -1;
  if (is48k) {
    uint8_t sp;
    if ((ram[5] = zxs_chunk_findram(snap, 5)) == NULL) return -1;
    if ((ram[2] = zxs_chunk_findram(snap, 2)) == NULL) return -1;
    if ((ram[0] = zxs_chunk_findram(snap, 0)) == NULL) return -1;
    // push PC
    sp = ((((int32_t)regs->SP)-2)&0xffff);
    spp0 = zxs_chunk_findram(snap, sp>>14);
    spp1 = zxs_chunk_findram(snap, ((sp+1)&0xffff)>>14);
    if (spp0 == NULL || spp1 == NULL) return -1;
    spp0a = (sp&0x3fff);
    spp1a = ((sp+1)&0x3fff);
    spp0b = spp0->data[spp0a];
    spp1b = spp0->data[spp1a];
    spp0->data[spp0a] = (regs->PC&0xff);
    spp0->data[spp1a] = ((regs->PC>>8)&0xff);
    regs->SP = sp;
  } else {
    for (int f = 0; f < 8; ++f) {
      if ((ram[f] = zxs_chunk_findram(snap, f)) == NULL) return -1;
    }
  }
  if (zxs_fwrite_u8(fl, regs->I)) goto error;
  if (zxs_fwrite_u16(fl, regs->HL1)) goto error;
  if (zxs_fwrite_u16(fl, regs->DE1)) goto error;
  if (zxs_fwrite_u16(fl, regs->BC1)) goto error;
  if (zxs_fwrite_u16(fl, regs->AF1)) goto error;
  if (zxs_fwrite_u16(fl, regs->HL)) goto error;
  if (zxs_fwrite_u16(fl, regs->DE)) goto error;
  if (zxs_fwrite_u16(fl, regs->BC)) goto error;
  if (zxs_fwrite_u16(fl, regs->IY)) goto error;
  if (zxs_fwrite_u16(fl, regs->IX)) goto error;
  t = (regs->IFF1 ? 0x04 : 0x00);
  if (zxs_fwrite_u8(fl, t)) goto error;
  if (zxs_fwrite_u8(fl, regs->R)) goto error;
  if (zxs_fwrite_u16(fl, regs->AF)) goto error;
  if (zxs_fwrite_u16(fl, regs->SP)) goto error;
  if (zxs_fwrite_u8(fl, regs->IM)) goto error;
  if (zxs_fwrite_u8(fl, ports->chBorder)) goto error;
  if (fwrite(ram[5]->data, 0x4000, 1, fl) != 1) goto error;
  if (fwrite(ram[2]->data, 0x4000, 1, fl) != 1) goto error;
  if (is48k) {
    if (fwrite(ram[0]->data, 0x4000, 1, fl) != 1) goto error;
  } else {
    const zxs_b128_t *beta = (const zxs_b128_t *)zxs_chunk_find(snap, ZXS_B128_SIGN);
    if (fwrite(ram[ports->ch7ffd&0x07]->data, 0x4000, 1, fl) != 1) goto error;
    if (zxs_fwrite_u16(fl, regs->PC)) goto error;
    if (zxs_fwrite_u8(fl, ports->ch7ffd)) goto error;
    t = (beta != NULL && (beta->dwFlags&ZXS_B128_FLAG_PAGED) ? 1 : 0);
    if (zxs_fwrite_u8(fl, t)) goto error;
    for (int f = 0; f < 8; ++f) {
      if (f == 2 || f == 5 || f == (ports->ch7ffd&0x07)) continue;
      if (fwrite(ram[f]->data, 0x4000, 1, fl) != 1) goto error;
    }
  }
done:
  if (spp0 != NULL) {
    spp0->data[spp0a] = spp0b;
    spp0->data[spp1a] = spp1b;
    regs->SP = ((regs->SP+2)&0xffff);
  }
  return (waserror ? -1 : 0);
error:
  waserror = 1;
  goto done;
}
