/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef LIBZXS_SZX_H
#define LIBZXS_SZX_H

#include "zxsnap_main.h"

#ifdef __cplusplus
extern "C" {
#endif


////////////////////////////////////////////////////////////////////////////////
// WARNING! ONLY KNOWN BLOCKS (THAT WHICH DEFINED IN zxsnap_main.h) WILL BE
//          BYTEORDER-FIXED!
//          ALTHRU blk.dwSize WILL BE FIXED ALWAYS.


////////////////////////////////////////////////////////////////////////////////
enum {
  ZXS_SZX_KEEP_UNKNOWN_BLOCKS = 0x01
};

extern int zxs_snap_load_szx (zxs_t *snap, FILE *fl, int flags);


////////////////////////////////////////////////////////////////////////////////
enum {
  ZXS_SZX_DONT_COMPRESS = 0x01
};

extern int zxs_snap_save_szx (FILE *fl, zxs_t *snap, int flags);


#ifdef __cplusplus
}
#endif
#endif
