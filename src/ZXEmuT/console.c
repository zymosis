/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include "console.h"
#include "debugger.h"
#include "emuutils.h"
#include "libvideo/video.h"

#include <ctype.h>
#include <unistd.h>


////////////////////////////////////////////////////////////////////////////////
char *strprintfVA (const char *fmt, va_list vaorig) {
  char *buf = NULL;
  int olen, len = 128;
  buf = malloc(len);
  if (buf == NULL) { fprintf(stderr, "\nFATAL: out of memory!\n"); abort(); }
  for (;;) {
    char *nb;
    va_list va;
    va_copy(va, vaorig);
    olen = vsnprintf(buf, len, fmt, va);
    va_end(va);
    if (olen >= 0 && olen < len) return buf;
    if (olen < 0) olen = len*2-1;
    nb = realloc(buf, olen+1);
    if (nb == NULL) { fprintf(stderr, "\nFATAL: out of memory!\n"); abort(); }
    buf = nb;
    len = olen+1;
  }
}


__attribute__((format(printf,1,2))) char *strprintf (const char *fmt, ...) {
  char *buf = NULL;
  va_list va;
  va_start(va, fmt);
  buf = strprintfVA(fmt, va);
  va_end(va);
  return buf;
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  unsigned char ch;
  Uint8 color;
} ConChar;

static ConChar conlines[MAX_CON_LINES][CON_WIDTH]; // not 0-terminated!
static int conTopLine;
int conpos;
static Uint8 concolor;
static char *concmdline = NULL;
static int concmdlinesize = 0;
static VOverlay *conOverlay = NULL;
static char *conhistory[MAX_CON_HISTORY];
static int conhistorypos;
FILE *condumpfl = NULL;
static int conistty = 1;


////////////////////////////////////////////////////////////////////////////////
static void conClearLine (int y) {
  if (y >= 0 && y < MAX_CON_LINES) {
    for (int x = 0; x < CON_WIDTH; ++x) {
      conlines[y][x].ch = ' ';
      conlines[y][x].color = CON_FG_COLOR;
    }
  }
}


void conInit (void) {
  conistty = isatty(STDOUT_FILENO);
  //
  conOverlay = createVO(CON_WIDTH*6+6, CON_HEIGHT*8+6);
  //
  for (int f = 0; f < MAX_CON_LINES; ++f) conClearLine(f);
  conpos = 0;
  concolor = CON_FG_COLOR;
  if (concmdline != NULL) free(concmdline);
  concmdline = NULL;
  conTopLine = MAX_CON_LINES-CON_HEIGHT+1;
  //
  conhistorypos = -1;
  for (int f = 0; f < MAX_CON_HISTORY; ++f) conhistory[f] = NULL;
}


////////////////////////////////////////////////////////////////////////////////
static void contopMove (int cnt) {
  if ((conTopLine += cnt) < 0) conTopLine = 0;
  else if (conTopLine > MAX_CON_LINES-CON_HEIGHT+1) conTopLine = MAX_CON_LINES-CON_HEIGHT+1;
}


////////////////////////////////////////////////////////////////////////////////
static void concmdlineAppendChar (char ch) {
  if ((unsigned char)ch >= 32) {
    int len = (int)strlen(concmdline != NULL ? concmdline : "");
    //
    if (len >= 8192) return;
    if (len+2 > concmdlinesize) {
      int newsz = len+1024;
      char *nb = realloc(concmdline, newsz);
      //
      if (nb == NULL) return;
      concmdlinesize = newsz;
      concmdline = nb;
    }
    concmdline[len++] = ch;
    concmdline[len] = 0;
  }
}


static void concmdlineClear (void) {
  if (concmdlinesize > 0) concmdline[0] = 0;
}


static void concmdlineChopChar (void) {
  if (concmdlinesize > 0) {
    int len = (int)strlen(concmdline);
    //
    if (len > 0) concmdline[--len] = 0;
  }
}


//FIXME: parse line and correctly kill quoted words?
static void concmdlineKillWord (void) {
  if (concmdlinesize > 0) {
    int len = (int)strlen(concmdline)-1;
    //
    while (len >= 0 && isspace(concmdline[len])) --len;
    while (len >= 0 && !isspace(concmdline[len])) --len;
    if (++len < 0) len = 0;
    concmdline[len] = 0;
  }
}


static void concmdlineSetString (const char *s) {
  if (s == NULL) s = "";
  concmdlineClear();
  while (*s) concmdlineAppendChar(*s++);
}


////////////////////////////////////////////////////////////////////////////////
static void conScroll (void) {
  for (int f = 0; f < MAX_CON_LINES-1; ++f) memcpy(conlines[f], conlines[f+1], sizeof(conlines[f]));
  conClearLine(MAX_CON_LINES-1);
}

static char constrdumpbuf[1024];
static size_t constrdumpbuflen = 0;


static inline void dumpToStdout (void) {
  //if (conistty)
  if (constrdumpbuflen) {
    write(STDOUT_FILENO, constrdumpbuf, (size_t)constrdumpbuflen);
  }
}


void conPutChar (char ch) {
  unsigned int c = (ch&0xff);

  int skipcdb = 0;

  if (ch == '\n' && optConDumpToStdout) {
    if (constrdumpbuflen < sizeof(constrdumpbuf)-1) {
      constrdumpbuf[constrdumpbuflen++] = ch;
      dumpToStdout();
    } else {
      dumpToStdout();
      constrdumpbuf[0] = '\n';
      constrdumpbuflen = 1;
      dumpToStdout();
    }
    constrdumpbuflen = 0;
    skipcdb = 1;
  }

  // flush
  if (constrdumpbuflen >= sizeof(constrdumpbuf)-2) {
    if (optConDumpToStdout) dumpToStdout();
    constrdumpbuflen = 0;
  }

  if (optConDump) {
    if (condumpfl == NULL) condumpfl = fopen("/tmp/zxemut.log", "a");
    if (condumpfl != NULL) {
      if (c) fputc(c, condumpfl);
    }
  }

  if (!skipcdb && ch) {
    const unsigned uch = (unsigned)(ch&0xffu);
    if (uch == 8 || uch == 9 || uch == '\n' || uch >= 32) {
      constrdumpbuf[constrdumpbuflen++] = ch;
    }
  }

  switch (c) {
    case 0: // normal
    case 31:
      concolor = CON_FG_COLOR;
      return;
    case 1: // white
      concolor = 15;
      return;
    case 2: // yellow
      concolor = 14;
      return;
    case 3: // gray
      concolor = 7;
      return;
    case 4: // light red
      concolor = 10;
      return;
    case 8:
      if (conpos > 0) --conpos;
      return;
    case 9:
      if (conpos >= CON_WIDTH) { conScroll(); conpos = 0; }
      if (conpos%CON_TAB_SIZE == 0) conPutChar(' ');
      while (conpos < CON_WIDTH && conpos%CON_TAB_SIZE) conPutChar(' ');
      return;
    case 10:
      if (concolor != 10) concolor = CON_FG_COLOR;
      if (conpos == 0 || conpos >= CON_WIDTH) { conpos = 0; conScroll(); } else conpos = CON_WIDTH;
      return;
    case 13:
      if (concolor != 10) concolor = CON_FG_COLOR;
      conpos = 0;
      return;
    default:
      if (c < 32) return;
  }

  if (conpos >= CON_WIDTH) { conScroll(); conpos = 0; }
  conlines[MAX_CON_LINES-1][conpos].ch = ch;
  conlines[MAX_CON_LINES-1][conpos].color = concolor;
  ++conpos;
}


void cprintfVA (const char *fmt, va_list va) {
  char *buf = strprintfVA(fmt, va);
  //
  for (const char *t = buf; *t; ++t) conPutChar(*t);
  conPutChar(0); // reset color
  free(buf);
}


__attribute__((format(printf,1,2))) void cprintf (const char *fmt, ...) {
  va_list va;
  //
  va_start(va, fmt);
  cprintfVA(fmt, va);
  va_end(va);
}


////////////////////////////////////////////////////////////////////////////////
static inline int x2dig (char ch) {
  if (ch > 0 && isxdigit(ch)) {
    if ((ch -= '0') > 9) if ((ch -= 39) < 10) ch += 39-7;
  } else {
    ch = -1;
  }
  return ch;
}


static Jim_Obj *conParseArgs (const char *s) {
  Jim_Obj *lst = Jim_NewListObj(jim, NULL, 0);
  //
  while (*s) {
    Jim_Obj *js;
    char qch;
    //
    while (*s && isspace(*s)) ++s;
    if (!s[0]) break;
    if ((qch = s[0]) == '"' || qch == '\'') {
      const char *pst = ++s;
      //
      js = Jim_NewStringObj(jim, NULL, 0);
      while (*s && *s != qch) {
        if (*s++ == '\\') {
          char ch;
          //
          if (pst+1 < s) Jim_AppendString(jim, js, pst, s-pst-1);
          switch (*s++) {
            case 'a': ch = '\a'; break;
            case 'b': ch = '\b'; break;
            case 'f': ch = '\f'; break;
            case 'n': ch = '\n'; break;
            case 'r': ch = '\r'; break;
            case 't': ch = '\t'; break;
            case 'v': ch = '\v'; break;
            case 'e': ch = 27; break;
            case '~': ch = 127; break;
            case 'x':
              ch = 0;
              if (x2dig(*s) >= 0) ch = 16*x2dig(*s++);
              if (x2dig(*s) >= 0) ch |= x2dig(*s++);
              if (ch == 0) ch = ' ';
              break;
            default:
              ch = s[-1];
              break;
          }
          Jim_AppendString(jim, js, &ch, 1);
          pst = s;
          continue;
        }
      }
      if (pst < s) Jim_AppendString(jim, js, pst, s-pst);
      //fprintf(stderr, "quoted: [%s]\n", Jim_String(js));
      if (*s == qch) ++s;
    } else {
      const char *e;
      //
      for (e = s; *e && !isspace(*e); ++e) ;
      js = Jim_NewStringObj(jim, s, e-s);
      s = e;
    }
    //fprintf(stderr, "[%s]\n", Jim_String(js));
    Jim_ListAppendElement(jim, lst, js);
  }
  return lst;
}


static void conAutocomplete (void) {
  if (concmdlinesize > 0 && concmdline[0]) {
    const char *s = concmdline;
    //
    while (*s && isspace(*s)) ++s;
    if (*s && isalnum(*s)) {
      Jim_Obj *lst = conParseArgs(concmdline), *jcmd[1];
      //
      Jim_IncrRefCount(lst);
      jcmd[0] = Jim_NewStringObj(jim, "::conautocomplete", -1);
      Jim_ListInsertElements(jim, lst, 0, 1, jcmd);
      //
      if (Jim_EvalObjList(jim, lst) != JIM_OK) {
        Jim_MakeErrorMessage(jim);
        cprintf("\4=== JIM ERROR ===\n%s\n=================\n", Jim_GetString(Jim_GetResult(jim), NULL));
      } else {
        s = Jim_String(Jim_GetResult(jim));
        if (s != NULL && s[0]) {
          free(concmdline);
          concmdline = strdup(s);
          concmdlinesize = (int)strlen(concmdline)+1;
        }
      }
      Jim_DecrRefCount(jim, lst);
    }
  }
}


void conSetInputString (const char *str) {
  if (concmdline != NULL) free(concmdline);
  concmdline = strdup((str != NULL && str[0] ? str :""));
  concmdlinesize = (int)strlen(concmdline)+1;
}


////////////////////////////////////////////////////////////////////////////////
void conExecute (const char *str, int astcl) {
  if (str != NULL && str[0]) {
    if (str[0] == '.' || astcl) {
      if (Jim_Eval(jim, str+(astcl ? 0 : 1)) != JIM_OK) {
        Jim_MakeErrorMessage(jim);
        cprintf("\4=== JIM ERROR ===\n%s\n=================\n", Jim_GetString(Jim_GetResult(jim), NULL));
      }
    } else {
      Jim_Obj *lst = conParseArgs(str), *jcmd[1];
      Jim_IncrRefCount(lst);
      jcmd[0] = Jim_NewStringObj(jim, "::conexec", -1);
      Jim_ListInsertElements(jim, lst, 0, 1, jcmd);
      if (Jim_EvalObjList(jim, lst) != JIM_OK) {
        Jim_MakeErrorMessage(jim);
        cprintf("\4=== JIM ERROR ===\n%s\n=================\n", Jim_GetString(Jim_GetResult(jim), NULL));
      }
      Jim_DecrRefCount(jim, lst);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static int conStrEqu (const void *str0, const void *str1) {
  if (!str0) str0 = "";
  if (!str1) str1 = "";
  const uint8_t *s0 = (const uint8_t *)str0;
  const uint8_t *s1 = (const uint8_t *)str1;
  while (s0[0] && s0[0] <= 32) ++s0;
  while (s1[0] && s1[0] <= 32) ++s1;
  //fprintf(stderr, "000: EQU: <%s> <%s>\n", str0, str1);
  while (*s0 || *s1) {
    while (s0[0] && s0[0] <= 32 && s0[1] <= 32) ++s0;
    while (s1[0] && s1[0] <= 32 && s1[1] <= 32) ++s1;
    //fprintf(stderr, "  <%s> <%s>\n", s0, s1);
    const uint8_t c0 = *s0++;
    const uint8_t c1 = *s1++;
    if (c0 != c1) return 0;
    if (!c0 || !c1) return (c0 == c1);
  }
  //fprintf(stderr, "!\n");
  return 1;
}


static void conRecordHistory (const char *str) {
  conhistorypos = -1;
  if (!str) return;

  // check for an empty string
  size_t pos = 0;
  while (str[pos] && ((const uint8_t *)str)[pos] <= 32) ++pos;
  if (!str[pos]) return;

  // find duplicate, if there is any
  for (int f = 0; conhistory[f] != NULL; ++f) {
    if (conStrEqu(conhistory[f], str)) {
      // duplicate found, move it to history top
      char *s = conhistory[f];
      for (int c = f; c > 0; --c) conhistory[c] = conhistory[c-1];
      conhistory[0] = s;
      return;
    }
  }

  // no duplicate, append to history
  if (conhistory[MAX_CON_HISTORY-1] != NULL) free(conhistory[MAX_CON_HISTORY-1]);
  for (int f = MAX_CON_HISTORY-1; f > 0; --f) conhistory[f] = conhistory[f-1];
  conhistory[0] = strdup(str);
}


static void conhistoryCopy (void) {
  if (conhistorypos >= 0) {
    concmdlineSetString(conhistory[conhistorypos]);
    conhistorypos = -1;
  }
}


////////////////////////////////////////////////////////////////////////////////
int conKeyEvent (SDL_KeyboardEvent *key) {
  int ctrleon = 0;
  //
  if (key->type == SDL_KEYDOWN && (key->keysym.mod&KMOD_CTRL) != 0) {
    int dontexit = 0;
    //
    switch (key->keysym.sym) {
      case SDLK_BACKSPACE: case SDLK_w: conhistoryCopy(); concmdlineKillWord(); return 1;
      case SDLK_y: conhistoryCopy(); concmdlineClear(); return 1;
      case SDLK_PAGEUP: contopMove(-(CON_HEIGHT-1)); return 1;
      case SDLK_PAGEDOWN: contopMove(CON_HEIGHT-1); return 1;
      case SDLK_RETURN: dontexit = 1; break;
      default: ;
    }
    if (!dontexit) return 0;
  }
  //
  if (key->type == SDL_KEYDOWN && (key->keysym.mod&(KMOD_ALT|KMOD_META)) == 0 && (key->keysym.mod&KMOD_CTRL) &&
      key->keysym.sym == SDLK_RETURN)
  {
    ctrleon = 1;
  }
  //
  if (key->type == SDL_KEYDOWN && (ctrleon || (key->keysym.mod&(KMOD_CTRL|KMOD_ALT|KMOD_META)) == 0)) {
    if (key->keysym.unicode == 96) {
      const char *cline = (conhistorypos < 0 ? concmdline : conhistorypos < MAX_CON_HISTORY ? conhistory[conhistorypos] : NULL);
      //
      if (cline == NULL || !cline[0]) {
        conVisible = 0;
        return 1;
      }
    }
    //
    if (key->keysym.unicode >= 32 && key->keysym.unicode < 127) {
      conhistoryCopy();
      concmdlineAppendChar(key->keysym.unicode);
      return 1;
    }
    //
    switch (key->keysym.sym) {
      case SDLK_ESCAPE: conVisible = 0; return 1;
      case SDLK_RETURN: {
        char *s = NULL;
        //
        if (conhistorypos < 0) {
          if (concmdlinesize > 0 && concmdline[0]) {
            s = strdup(concmdline);
            //conHistoryAdd(s);
          }
        } else if (conhistorypos < MAX_CON_HISTORY && conhistory[conhistorypos] != NULL) {
          s = strdup(conhistory[conhistorypos]);
        }
        //
        concmdlineClear();
        conRecordHistory(s);
        //
        if (s != NULL) {
          conExecute(s, ctrleon);
          free(s);
        }
        return 1; }
      case SDLK_TAB: conhistoryCopy(); conAutocomplete(); return 1;
      case SDLK_BACKSPACE: conhistoryCopy(); concmdlineChopChar(); return 1;
      case SDLK_PAGEUP: contopMove(-1); return 1;
      case SDLK_PAGEDOWN: contopMove(1); return 1;
      case SDLK_UP: if (++conhistorypos >= MAX_CON_HISTORY || conhistory[conhistorypos] == NULL) --conhistorypos; return 1;
      case SDLK_DOWN: if (--conhistorypos < -1) conhistorypos = -1; return 1;
      default: ;
    }
  }
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
void conDraw (void) {
  int cp;
  const char *cline;

  #if 0
  clearVO(conOverlay, CON_BG_COLOR);
  drawFrameVO(conOverlay, 1, 1, conOverlay->w-2, conOverlay->h-2, CON_FG_COLOR);
  // draw console text
  for (int y = 0; y < CON_HEIGHT-1; ++y) {
    int ln = conTopLine+y;//MAX_CON_LINES-CON_HEIGHT+y+1;
    //
    for (int x = 0; x < CON_WIDTH; ++x) {
      drawChar6VO(conOverlay, conlines[ln][x].ch, x*6+3, y*8+3, conlines[ln][x].color, 255);
    }
  }
  // draw command line
  cline = (conhistorypos < 0 ? concmdline : conhistorypos < MAX_CON_HISTORY ? conhistory[conhistorypos] : NULL);
  //
  if (cline != NULL) {
    int len = (int)strlen(cline);
    const char *cl = cline;
    char buf[CON_WIDTH+1];
    //
    //fprintf(stderr, "len=%d; MAX_CON_WIDTH=%d\n", len, MAX_CON_WIDTH);
    if (len > CON_WIDTH-1) {
      cl = cline+len-(CON_WIDTH-1);
      len = CON_WIDTH-1;
    }
    cp = len;
    memset(buf, 32, sizeof(buf));
    buf[sizeof(buf)-1] = '\0';
    strcpy(buf, cl);
    drawStr6VO(conOverlay, buf, 3, (CON_HEIGHT-1)*8+3, 14, 255);
    if (cl > cline) drawChar6VO(conOverlay, '<', 3, (CON_HEIGHT-1)*8+3, 13, CON_BG_COLOR);
  } else {
    cp = 0;
  }
  // draw cursor
  //TODO: add 'curblink' console command
  if (timerGetMS()%800 < 400) fillRectVO(conOverlay, cp*6+3, (CON_HEIGHT-1)*8+3, 6, 8, 15);
  //
  blitVO(conOverlay, (320-conOverlay->w)/2, 0, conAlpha);
  #else

  /*
  if (debuggerActive && !dbgIsHidden()) {
    for (int y = 0; y < CON_HEIGHT; ++y) {
      vt_writechars(0, y, VID_TEXT_WIDTH, ' ', 7);
    }
  } else {
    vt_cls(' ', 0x0a);
  }
  */

  // draw console text
  for (int y = 0; y < CON_HEIGHT-1; ++y) {
    vt_writechars(0, y, VID_TEXT_WIDTH, ' ', 0x0A);
    int ln = conTopLine+y;//MAX_CON_LINES-CON_HEIGHT+y+1;
    for (int x = 0; x < CON_WIDTH; ++x) {
      vt_writechar(x, y, conlines[ln][x].ch, conlines[ln][x].color);
    }
  }

  // draw command line
  cline = (conhistorypos < 0 ? concmdline : conhistorypos < MAX_CON_HISTORY ? conhistory[conhistorypos] : NULL);

  vt_writechars(0, CON_HEIGHT-1, VID_TEXT_WIDTH, ' ', 0x17);
  if (cline != NULL) {
    int len = (int)strlen(cline);
    const char *cl = cline;
    char buf[CON_WIDTH+1];
    if (len > CON_WIDTH-1) {
      cl = cline+len-(CON_WIDTH-1);
      len = CON_WIDTH-1;
    }
    cp = len;
    memset(buf, 32, sizeof(buf));
    buf[sizeof(buf)-1] = '\0';
    strcpy(buf, cl);
    vt_writestrz(0, CON_HEIGHT-1, buf, 0x1e);
    if (cl > cline) {
      vt_writechar(0, CON_HEIGHT-1, '<', 0x1d);
    }
  } else {
    cp = 0;
  }

  // draw cursor
  //TODO: add 'curblink' console command
  /*
  if (timerGetMS()%800 < 400) {
    fillRectVO(conOverlay, cp*6+3, (CON_HEIGHT-1)*8+3, 6, 8, 15);
  }
  */
  vt_writechar(cp, CON_HEIGHT-1, 0, emuGetCursorColor(0xF0, 0x70));
  #endif
}


////////////////////////////////////////////////////////////////////////////////
static char *msg = NULL;
static int msg_len = 0;
static int64_t msg_hide_time = 0;


__attribute__((format(printf,1,2))) void conMessage (const char *fmt, ...) {
  va_list va;
  if (msg != NULL) free(msg);
  if (fmt != NULL && fmt[0]) {
    va_start(va, fmt);
    msg = strprintfVA(fmt, va);
    va_end(va);
    msg_len = (int)strlen(msg);
    if (msg_len > 0) {
      msg_hide_time = timerGetMS()+4000; //TODO: make configurable
    } else {
      free(msg);
      msg = NULL;
      msg_hide_time = 0;
    }
  } else {
    msg = NULL;
    msg_hide_time = 0;
  }
}


void conDrawMessage (void) {
  if (msg_hide_time && msg != NULL) {
    int64_t ct = timerGetMS();
    if (ct < msg_hide_time) {
      int x = (frameSfc->w-msg_len*6)/2;
      drawStr6Outline(msg, x, frameSfc->h-10, 14, 0);
    } else {
      msg_hide_time = 0;
      free(msg);
      msg = NULL;
    }
  }
}
