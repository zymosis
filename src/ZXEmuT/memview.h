/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_MEMVIEW_H
#define ZXEMUT_MEMVIEW_H

#include <SDL.h>
#include "../libzymosis/zymosis.h"


extern void memviewInit (void);
extern void memviewSetActive (int st);

extern void memviewDraw (void);
extern int memviewKeyEvent (SDL_KeyboardEvent *key);


extern uint8_t mv_pagemap[256];


#define MV_MAX_TEXT_WIDTH  (68)


// >0xffff: pages
enum {
  MV_ENC_7BIT,
  MV_ENC_866,
  MV_ENC_1251,

  MV_ENC_MAX,
};

enum {
  MV_MODE_HEX,
  MV_MODE_TEXT,
};

enum {
  MV_CLRTEXT_NONE,
  MV_CLRTEXT_CODE10,
  MV_CLRTEXT_CODE1B,
};


extern uint32_t mv_staddr;
extern int mv_encoding;
extern int mv_viewmode;
extern int mv_textwidth;
extern int mv_colortext_mode;
extern int mv_colortext_ignore;
extern int mv_skip_trailing_spaces;
extern uint8_t mv_textcolor; // paper is ignored


#endif
