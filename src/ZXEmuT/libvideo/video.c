/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "video.h"

#include "zxpal.c"
#include "vdfont6x8.c"
#include "msxfont8x8.c"
#include "fonts_8_16.c"

#include "zfnt.c"

#include "scalexx/scale2x.h"
#include "scalexx/scale3x.h"
#include "scalexx/scalebit.h"

#include "hq3/hqx.h"


////////////////////////////////////////////////////////////////////////////////
FrameCB frameCB = NULL;
KeyCB keyCB = NULL;
MouseCB mouseCB = NULL;
MouseButtonCB mouseButtonCB = NULL;
WindowActivationCB windowActivationCB = NULL;
int vidScale = 3; // 1, 2, 3, 4; default is 3

void (*vidBeforeFlipCB) (SDL_Surface *sfc) = NULL;

int vid_scaler_type = VID_SCALER_NONE;

VidTChar vid_textscr[VID_TEXT_WIDTH*VID_TEXT_HEIGHT];
int vid_textscr_active = 0;
int vid_textsrc_height = 0;

//HACK! valid only for mouse handler.
int msRealX = 0;
int msRealY = 0;


// ////////////////////////////////////////////////////////////////////////// //
static int actualScale;


// ////////////////////////////////////////////////////////////////////////// //
static void screenFlip (void);


////////////////////////////////////////////////////////////////////////////////
int optFullscreen = 0;
int optTVScaler = 1;
SDL_Surface *screen = NULL;
SDL_Surface *screenReal = NULL;
Uint32 palette[256]; // converted
unsigned paletteUsed = 0;
static unsigned paletteUsedCurr = 0;
SDL_Surface *frameSfc = NULL;
int vidWindowActivated = 1;
int vidColorMode = 0; // 0: ok; 1: b/w; 2: green
static SurfaceLock frameSfcLock;
static int frameSfcRaped = 0;

static uint8_t pal8bit[256][4]; // [3] is unused
uint8_t palRGB[256][4]; // [3] is unused
int vidRIdx, vidGIdx, vidBIdx;


////////////////////////////////////////////////////////////////////////////////
static void initEventSystem (void);


////////////////////////////////////////////////////////////////////////////////
static void quitCleanupCB (void) {
  SDL_Quit();
}


////////////////////////////////////////////////////////////////////////////////
static struct timespec timerStart;
static int timerInitialized = 0;
static int timerSource = -1;
#define TIMER_CLOCK_TYPE  CLOCK_MONOTONIC_RAW
//#define TIMER_CLOCK_TYPE  CLOCK_MONOTONIC


static int timerCheckClockSource (void) {
  FILE *fl = fopen("/sys/devices/system/clocksource/clocksource0/current_clocksource", "r");
  char buf[128];
  //
  if (fl == NULL) {
    fprintf(stderr, "WARNING: can't determine clock source!\n");
    return TIMER_OTHER;
  }
  memset(buf, 0, sizeof(buf));
  if (fgets(buf, sizeof(buf)-1, fl) == NULL) {
    fprintf(stderr, "WARNING: can't determine clock source!\n");
    fclose(fl);
    return TIMER_OTHER;
  }
  fclose(fl);
  while (buf[0] && isspace(buf[strlen(buf)-1])) buf[strlen(buf)-1] = 0;
  //fprintf(stderr, "clock source: %s\n", buf);
  return (strcasecmp(buf, "hpet") == 0 ? TIMER_HPET : TIMER_OTHER);
}


int timerInit (void) {
  if (!timerInitialized) {
    struct timespec cres;
    //
    timerInitialized = 1;
    //
    if ((timerSource = timerCheckClockSource()) < 0) return timerSource;
    //
    if (clock_getres(TIMER_CLOCK_TYPE, &cres) != 0) {
      fprintf(stderr, "ERROR: can't get clock resolution!\n");
      return TIMER_ERROR;
    }
    //fprintf(stderr, "CLOCK_MONOTONIC: %ld:%ld\n", cres.tv_sec, cres.tv_nsec);
    //
    if (cres.tv_sec > 0 || cres.tv_nsec > (long)1000000*10 /*10 ms*/) {
      fprintf(stderr, "ERROR: real-time clock resolution is too low!\n");
      return TIMER_ERROR;
    }
    //
    if (clock_gettime(TIMER_CLOCK_TYPE, &timerStart) != 0) {
      fprintf(stderr, "ERROR: can't get real-time clock value!\n");
      return TIMER_ERROR;
    }
  }
  //
  return timerSource;
}


int timerReinit (void) {
  if (clock_gettime(TIMER_CLOCK_TYPE, &timerStart) != 0) {
    fprintf(stderr, "ERROR: can't get real-time clock value!\n");
    return -1;
  }
  //
  return 0;
}


int64_t timerGetMS (void) {
  struct timespec ts;
  //
  if (!timerInitialized) {
    if (timerInit() < 0) return -1;
    timerInitialized = 1;
  }
  //
  if (clock_gettime(TIMER_CLOCK_TYPE, &ts) != 0) {
    fprintf(stderr, "ERROR: can't get real-time clock value!\n");
    return -1;
  }
  // ah, ignore nanoseconds in timerStart here: we need only 'differential' time, and it can start with something weird
  return ((int64_t)(ts.tv_sec-timerStart.tv_sec))*1000+(ts.tv_nsec-timerStart.tv_nsec)/1000000;
}


int64_t timerGetMicroSeconds (void) {
  struct timespec ts;
  //
  if (!timerInitialized) {
    if (timerInit() < 0) return -1;
    timerInitialized = 1;
  }
  //
  if (clock_gettime(TIMER_CLOCK_TYPE, &ts) != 0) {
    fprintf(stderr, "ERROR: can't get real-time clock value!\n");
    return -1;
  }
  // ah, ignore nanoseconds in timerStart here: we need only 'differential' time, and it can start with something weird
  return ((int64_t)(ts.tv_sec-timerStart.tv_sec))*1000000+(ts.tv_nsec-timerStart.tv_nsec)/1000;
}


////////////////////////////////////////////////////////////////////////////////
void sdlInit (void) {
  if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER /*| SDL_INIT_AUDIO*/) < 0) {
    fprintf(stderr, "FATAL: can't set videomode!\n");
    exit(1);
  }
  SDL_WM_SetCaption("ZXEmuT", "ZXEmuT");
  SDL_EnableKeyRepeat(200, 25);
  SDL_EnableUNICODE(1);
  //SDL_EnableUNICODE(0);
  atexit(quitCleanupCB);
}


#define COLORCUBE_BITS       (6)
#define COLORCUBE_LINE_SIZE  (1u<<COLORCUBE_BITS)
static uint8_t colorcube[COLORCUBE_LINE_SIZE*COLORCUBE_LINE_SIZE*COLORCUBE_LINE_SIZE];
static uint8_t colorcube2[COLORCUBE_LINE_SIZE*COLORCUBE_LINE_SIZE*COLORCUBE_LINE_SIZE];
static int cc2built = 0;

static inline void ccubeSet (uint8_t palidx, uint8_t r, uint8_t g, uint8_t b) {
  #if COLORCUBE_BITS < 8
  r >>= 8-COLORCUBE_BITS;
  g >>= 8-COLORCUBE_BITS;
  b >>= 8-COLORCUBE_BITS;
  #endif
  colorcube[r*(COLORCUBE_LINE_SIZE*COLORCUBE_LINE_SIZE)+g*COLORCUBE_LINE_SIZE+b] = palidx;
}


static inline uint8_t ccubeGet (uint8_t r, uint8_t g, uint8_t b) {
  #if COLORCUBE_BITS < 8
  r >>= 8-COLORCUBE_BITS;
  g >>= 8-COLORCUBE_BITS;
  b >>= 8-COLORCUBE_BITS;
  #endif
  return colorcube[r*(COLORCUBE_LINE_SIZE*COLORCUBE_LINE_SIZE)+g*COLORCUBE_LINE_SIZE+b];
}


static inline uint8_t ccube2Get (uint8_t r, uint8_t g, uint8_t b) {
  #if COLORCUBE_BITS < 8
  r >>= 8-COLORCUBE_BITS;
  g >>= 8-COLORCUBE_BITS;
  b >>= 8-COLORCUBE_BITS;
  #endif
  return colorcube2[r*(COLORCUBE_LINE_SIZE*COLORCUBE_LINE_SIZE)+g*COLORCUBE_LINE_SIZE+b];
}


static inline int32_t rgbDistanceSquared (uint8_t r0, uint8_t g0, uint8_t b0, uint8_t r1, uint8_t g1, uint8_t b1) {
  const int32_t rmean = ((int32_t)r0+(int32_t)r1)/2;
  const int32_t r = (int32_t)r0-(int32_t)r1;
  const int32_t g = (int32_t)g0-(int32_t)g1;
  const int32_t b = (int32_t)b0-(int32_t)b1;
  return (((512+rmean)*r*r)/256)+4*g*g+(((767-rmean)*b*b)/256);
}


static void buildCC2 (void) {
  if (cc2built) return;
  for (int ir = 0; ir < COLORCUBE_LINE_SIZE; ++ir) {
    for (int ig = 0; ig < COLORCUBE_LINE_SIZE; ++ig) {
      for (int ib = 0; ib < COLORCUBE_LINE_SIZE; ++ib) {
        const int r = (int)(ir*255.0f/((float)(COLORCUBE_LINE_SIZE-1))/*+0.5f*/);
        const int g = (int)(ig*255.0f/((float)(COLORCUBE_LINE_SIZE-1))/*+0.5f*/);
        const int b = (int)(ib*255.0f/((float)(COLORCUBE_LINE_SIZE-1))/*+0.5f*/);
        int best_color = -1;
        int best_dist = 0x7fffffff;
        for (int i = 0; i < 255; ++i) {
          int32_t dist = rgbDistanceSquared(pal8bit[i][0], pal8bit[i][1], pal8bit[i][2], r, g, b);
          if (best_color < 0 || dist < best_dist) {
            best_color = i;
            best_dist = dist;
            if (!dist) break;
          }
        }
        colorcube2[ir*(COLORCUBE_LINE_SIZE*COLORCUBE_LINE_SIZE)+ig*COLORCUBE_LINE_SIZE+ib] = best_color;
      }
    }
  }
  cc2built = 1;
}


void vidResetPalette (void) {
  paletteUsedCurr = paletteUsed;
}


uint8_t vidAllocColor (int r, int g, int b) {
  const uint8_t r8 = (r < 0 ? 0 : r > 255 ? 255 : (uint8_t)r);
  const uint8_t g8 = (g < 0 ? 0 : g > 255 ? 255 : (uint8_t)g);
  const uint8_t b8 = (b < 0 ? 0 : b > 255 ? 255 : (uint8_t)b);
  uint8_t v = ccubeGet(r8, g8, b8);
  if (v != 255) return v;
  if (paletteUsedCurr < 255) {
    // alloc
    palette[paletteUsedCurr] = SDL_MapRGB(screen->format, r8, g8, b8);
    SDL_GetRGB(palette[paletteUsedCurr], screen->format, &palRGB[paletteUsedCurr][0], &palRGB[paletteUsedCurr][1], &palRGB[paletteUsedCurr][2]);
    pal8bit[paletteUsedCurr][0] = r8;
    pal8bit[paletteUsedCurr][1] = g8;
    pal8bit[paletteUsedCurr][2] = b8;
    ccubeSet(paletteUsedCurr, r8, g8, b8);
    return paletteUsedCurr++;
  }
  // can't alloc, find nearest
  buildCC2();
  return ccube2Get(r8, g8, b8);
}


static void buildColorCache (void) {
  unsigned f;
  memset(colorcube, 255, sizeof(colorcube));
  memset(pal8bit, 0, sizeof(pal8bit));
  cc2built = 0;
  for (f = 0; f < sizeof(zxPalette)/3; ++f) {
    palette[f] = SDL_MapRGB(screen->format, zxPalette[f*3+0], zxPalette[f*3+1], zxPalette[f*3+2]);
    ccubeSet(f, zxPalette[f*3+0], zxPalette[f*3+1], zxPalette[f*3+2]);
    pal8bit[f][0] = zxPalette[f*3+0];
    pal8bit[f][1] = zxPalette[f*3+1];
    pal8bit[f][2] = zxPalette[f*3+2];
  }
  paletteUsed = paletteUsedCurr = f;
  for (; f < 256; ++f) palette[f] = SDL_MapRGB(screen->format, 0, 0, 0);
  for (f = 0; f < 256; ++f) {
    SDL_GetRGB(palette[f], screen->format, &palRGB[f][0], &palRGB[f][1], &palRGB[f][2]);
    palRGB[f][3] = 0;
  }
}


void rebuildPalette (void) {
  if (screen) buildColorCache();
}


static void lvCreateSurfaces (void) {
  if (optFullscreen) {
    screenReal = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE | SDL_FULLSCREEN);
    actualScale = 2;
    screen = SDL_CreateRGBSurface(SDL_SWSURFACE, 320, 240, screenReal->format->BitsPerPixel, screenReal->format->Rmask, screenReal->format->Gmask, screenReal->format->Bmask, 0);
  } else {
    actualScale = vidScale;
    if (actualScale < 1) actualScale = 1; else if (actualScale > 4) actualScale = 4;
    screenReal = SDL_SetVideoMode(320*actualScale, 240*actualScale, 32, SDL_SWSURFACE/*|SDL_DOUBLEBUF*/);
    screen = SDL_CreateRGBSurface(SDL_SWSURFACE, 320, 240, screenReal->format->BitsPerPixel, screenReal->format->Rmask, screenReal->format->Gmask, screenReal->format->Bmask, 0);
  }
  if (screen == NULL) {
    fprintf(stderr, "FATAL: can't set videomode!\n");
    exit(1);
  }
  //
  if (screen->format->BitsPerPixel != 32 || (screenReal != NULL && screenReal->format->BitsPerPixel != 32) ||
      (screen->format->Rshift%8 || screen->format->Gshift%8 || screen->format->Bshift%8)) {
    fprintf(stderr, "FATAL: videomode initialization error!\n");
    //fprintf(stderr, "Rmask=0x%08x; Gmask=0x%08x; Bmask=0x%08x\n", screen->format->Rmask, screen->format->Gmask, screen->format->Bmask);
    SDL_Quit();
    exit(1);
  }
  //
  vidRIdx = screen->format->Rshift/8;
  vidGIdx = screen->format->Gshift/8;
  vidBIdx = screen->format->Bshift/8;
  //fprintf(stderr, "BPP: %u\n", screen->format->BytesPerPixel);
  //SDL_ShowCursor(0);
  buildColorCache();
}


void initVideo (void) {
  lvCreateSurfaces();
  initEventSystem();
}


void switchFullScreen (void) {
  const int needInit = !!screen;
  #if 0
  if (optFullscreen) {
    screenReal = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE/*|SDL_DOUBLEBUF*/);
  } else {
    screenReal = SDL_SetVideoMode(320*3, 240*3, 32, SDL_SWSURFACE|SDL_FULLSCREEN);
  }
  optFullscreen = !optFullscreen;
  if (screenReal == NULL || screenReal->format->BitsPerPixel != 32) {
    fprintf(stderr, "FATAL: can't switch videomode!\n");
    exit(1);
  }
  #else
  if (screen) { SDL_FreeSurface(screen); screen = NULL; }
  optFullscreen = !optFullscreen;
  if (needInit) lvCreateSurfaces();
  #endif
  if (needInit) screenFlip();
}


void updateScale (void) {
  const int needInit = !!screen;
  if (screen) { SDL_FreeSurface(screen); screen = NULL; }
  if (needInit) {
    lvCreateSurfaces();
    screenFlip();
  }
}


////////////////////////////////////////////////////////////////////////////////
void lockSurface (SurfaceLock *lock, SDL_Surface *s) {
  if (lock) {
    lock->s = s;
    lock->needUnlock = 0;
    if (s != NULL && SDL_MUSTLOCK(s)) {
      lock->needUnlock = (SDL_LockSurface(s) == 0);
    }
  }
}


void unlockSurface (SurfaceLock *lock) {
  if (lock && lock->s && lock->needUnlock) {
    SDL_UnlockSurface(lock->s);
    lock->needUnlock = 0;
  }
}


void clearScreen (Uint8 col) {
  if (col != 255) {
    Uint32 clr = palette[col];
    Uint32 *d = (Uint32 *)((Uint8 *)frameSfc->pixels);
    //
    for (int y = 0; y < frameSfc->h; ++y) {
      for (int x = 0; x < frameSfc->w; ++x) d[x] = clr;
      d = (Uint32 *)(((Uint8 *)d)+frameSfc->pitch);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
// surface must be locked!
void drawChar6 (char ch, int x, int y, Uint8 fg, Uint8 bg) {
  int pos = (ch&0xff)*8;
  //
  for (int dy = 0; dy < 8; ++dy) {
    uint8_t b = font6x8[pos++];
    //
    for (int dx = 0; dx < 6; ++dx) {
      Uint8 c = (b&0x80 ? fg : bg);
      //
      if (c != 255) putPixel(x+dx, y+dy, c);
      b = (b&0x7f)<<1;
    }
  }
}


void drawStr6 (const char *str, int x, int y, Uint8 fg, Uint8 bg) {
  if (str) {
    while (*str) {
      drawChar6(*str++, x, y, fg, bg);
      x += 6;
    }
  }
}


void drawChar8 (char ch, int x, int y, Uint8 fg, Uint8 bg) {
  int pos = (ch&0xff)*8;
  //
  for (int dy = 0; dy < 8; ++dy) {
    uint8_t b = msxfont8x8[pos++];
    //
    for (int dx = 0; dx < 8; ++dx) {
      Uint8 c = (b&0x80 ? fg : bg);
      //
      if (c != 255) putPixel(x+dx, y+dy, c);
      b = (b&0x7f)<<1;
    }
  }
}


void drawStr8 (const char *str, int x, int y, Uint8 fg, Uint8 bg) {
  if (str) {
    while (*str) {
      drawChar8(*str++, x, y, fg, bg);
      x += 8;
    }
  }
}


void drawStr6Outline (const char *str, int x, int y, Uint8 fg, Uint8 oc) {
  for (int dy = -1; dy < 2; ++dy) {
    for (int dx = -1; dx < 2; ++dx) {
      if (dx || dy) drawStr6(str, x+dx, y+dy, oc, 255);
    }
  }
  drawStr6(str, x, y, fg, 255);
}


void drawStr8Outline (const char *str, int x, int y, Uint8 fg, Uint8 oc) {
  for (int dy = -1; dy < 2; ++dy) {
    for (int dx = -1; dx < 2; ++dx) {
      if (dx || dy) drawStr8(str, x+dx, y+dy, oc, 255);
    }
  }
  drawStr8(str, x, y, fg, 255);
}


void drawBar (int x, int y, int w, int h, Uint8 c) {
  if (!frameSfc) return;
  if (w < 1 || h < 1 || x >= frameSfc->w || y >= frameSfc->h) return;

  if (x < 0) { w += x; if (w <= 0) return; x = 0; }
  if (y < 0) { h += y; if (h <= 0) return; y = 0; }

  if (x+w > frameSfc->w) w = frameSfc->w-x;
  if (y+h > frameSfc->h) h = frameSfc->h-y;

  Uint8 *dptr = (((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch)+x*4);
  for (int dy = 0; dy < h; ++dy) {
    Uint32 *d = (Uint32 *)dptr;
    for (int dx = 0; dx < w; ++dx) *d++ = palette[c];
    dptr += frameSfc->pitch;
  }
}


////////////////////////////////////////////////////////////////////////////////
/*
static void drawCharZI (const uint8_t *dta, int hgt, int x, int y, Uint8 c1, Uint8 c2) {
  int wdt = *dta++, yofs = ((wdt>>4)&0x0f);
  //
  wdt &= 0x0f;
  y += yofs;
  for (int dy = 0; dy < hgt; ++dy) {
    for (int dx = 0; dx < wdt; dx += 4) {
      uint8_t b = *dta++;
      //
      for (int c = 0; c < 4; ++c) {
        switch ((b>>6)&0x03) {
          case 0: break;
          case 1: putPixel(x+dx+c, y+dy, c1); break;
          case 2: putPixel(x+dx+c, y+dy, c2); break;
          case 3: break;
        }
        b = ((b&0x3f)<<2);
      }
    }
  }
}


void drawZFont (void) {
  const uint8_t *st = zfont;
  int x = 1, y = 1, hgt = zfont[0];
  //
  for (int f = 33; f <= 95; ++f) {
    const uint8_t *st = zfont+(f-32)*2;
    int ofs = st[0]+256*st[1], w;
    //
    st = zfont+ofs;
    w = (st[0]&0x0f);
    if (x+w > 319) { x = 1; y += hgt+2; }
    drawCharZI(st, hgt, x, y, 66, 67);
    x += w;
  }
}
*/


int drawCharZ (char ch, int x, int y, Uint8 c1, Uint8 c2) {
  if (ch >= 'a' && ch <= 'z') ch -= 32;
  if (ch < 33 || ch > 95) ch = '?';
  if (ch != ' ') {
    const uint8_t *st = zfont+(ch-32)*2;
    int ofs = st[0]+256*st[1], wdt, yofs, hgt = zfont[0];
    //
    st = zfont+ofs;
    wdt = *st++;
    yofs = ((wdt>>4)&0x0f);
    wdt &= 0x0f;
    if (c1 == 255 && c2 == 255) return wdt;
    y += yofs;
    while (hgt-- > 0) {
      int px = x;
      //
      for (int dx = 0; dx < wdt; dx += 4) {
        uint8_t b = *st++;
        //
        for (int c = 0; c < 4; ++c, ++px) {
          switch ((b>>6)&0x03) {
            case 0: break;
            case 1: putPixel(px, y, c1); break;
            case 2: putPixel(px, y, c2); break;
            case 3: /*putPixel(x+dx+c, y+dy, 2);*/ break;
          }
          b = ((b&0x3f)<<2);
        }
      }
      ++y;
    }
    return wdt;
  } else {
    return zfont[1];
  }
}


int drawStrZ (const char *str, int x, int y, Uint8 c1, Uint8 c2) {
  int wdt = 0;
  //
  while (*str) {
    int w = drawCharZ(*str++, x, y, c1, c2);
    //
    wdt += w;
    x += w;
  }
  return wdt;
}


static inline Uint32 fixColor (Uint32 clr) {
  if (vidColorMode) {
    Uint8 *ca = (Uint8 *)&clr;
    Uint8 r = ca[vidRIdx], g = ca[vidGIdx], b = ca[vidBIdx];
    //Uint32 lumi = (0.3*((double)r/255.0)+0.59*((double)g/255.0)+0.11*((double)b/255.0))*255.0;
    Uint32 lumi = (4915*r+9666*g+1802*b);
    //
    if (vidColorMode > 7) {
      if (lumi >= 128*16384) lumi /= 16384; else lumi /= 11000;
    } else {
      lumi /= 16384;
    }
    if (lumi > 255) lumi = 255;
    //
    switch (vidColorMode&7) {
      case 1: // b/w
        ca[vidRIdx] = ca[vidGIdx] = ca[vidBIdx] = lumi;
        break;
      case 2: // green
        ca[vidRIdx] = ca[vidBIdx] = 0;
        ca[vidGIdx] = lumi;
        break;
      case 3: // yellow
        ca[vidBIdx] = 0;
        ca[vidRIdx] = lumi;
        ca[vidGIdx] = lumi;
        break;
      case 4: // cyan
        ca[vidBIdx] = lumi;
        ca[vidRIdx] = 0;
        ca[vidGIdx] = lumi;
        break;
      case 5: // magenta
        ca[vidBIdx] = lumi;
        ca[vidRIdx] = lumi;
        ca[vidGIdx] = 0;
        break;
      case 6: // red
        ca[vidRIdx] = lumi;
        ca[vidBIdx] = ca[vidGIdx] = 0;
        break;
    }
  }
  return clr;
}


////////////////////////////////////////////////////////////////////////////////
static int hqx_inited = 0;


static void fixSourceColors (void) {
  if (!vidColorMode) return;
  uint8_t *sbuf = screen->pixels;
  for (uint32_t y = 0; y < 240; ++y) {
    uint32_t *buf = (uint32_t *)sbuf;
    for (uint32_t x = 0; x < 320; ++x, ++buf) {
      *buf = fixColor(*buf);
    }
    sbuf += screen->pitch;
  }
}


static void Blit4xInternal (void) {
  if (vid_scaler_type == VID_SCALER_nX) {
    fixSourceColors();
    scale(4, screenReal->pixels, screenReal->pitch,
             screen->pixels, screen->pitch,
             4, 320, 240);
    return;
  }
  if (vid_scaler_type == VID_SCALER_HQX) {
    if (!hqx_inited) { hqx_inited = 1; hqxInit(); }
    fixSourceColors();
    hq4x_32_rb(screen->pixels, screen->pitch,
               screenReal->pixels, screenReal->pitch,
               320, 240);
    return;
  }
  const Uint32 *scrB = screen->pixels; // 320x240
  Uint32 *scrR = screenReal->pixels; // 1280x960
  for (int y = 0; y < screen->h; ++y) {
    const Uint32 *s = scrB;
    Uint32 *d0 = scrR;
    Uint32 *d1 = (Uint32 *)((Uint8 *)d0+screenReal->pitch);
    Uint32 *d2 = (Uint32 *)((Uint8 *)d1+screenReal->pitch);
    Uint32 *d3 = (Uint32 *)((Uint8 *)d2+screenReal->pitch);
    for (int x = 0; x < screen->w; ++x) {
      const Uint32 clr = fixColor(*s++);
      d0[0] = d0[1] = d0[2] = d0[3] = clr;
      d1[0] = d1[1] = d1[2] = d1[3] = clr;
      d2[0] = d2[1] = d2[2] = d2[3] = clr;
      d3[0] = d3[1] = d3[2] = d3[3] = clr;
      d0 += 4;
      d1 += 4;
      d2 += 4;
      d3 += 4;
    }
    scrB = (Uint32 *)((Uint8 *)scrB+screen->pitch);
    scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch*4);
  }
}


static void TV4xInternal (void) {
  Uint32 *scrR = screenReal->pixels; // 1280x960
  scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch);
  for (int y = screenReal->h/4; y--; scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch*4)) {
    Uint32 *d0 = scrR;
    for (int x = 0; x < screenReal->w; ++x) {
      const Uint32 c0 = *d0;
      Uint32 c1 = (((c0&0x00ff00ff)*7)>>3)&0x00ff00ff;
      c1 |= (((c0&0x0000ff00)*7)>>3)&0x0000ff00;
      *d0++ = c1;
    }
    d0 = (Uint32 *)((Uint8 *)scrR+screenReal->pitch);
    for (int x = 0; x < screenReal->w; ++x) {
      const Uint32 c0 = *d0;
      Uint32 c1 = (((c0&0x00ff00ff)*7)>>3)&0x00ff00ff;
      c1 |= (((c0&0x0000ff00)*7)>>3)&0x0000ff00;
      *d0++ = c1;
    }
  }
}


static void Blit4x (void) {
  const int needUnlock = (SDL_MUSTLOCK(screenReal) ? SDL_LockSurface(screenReal) == 0 : 0);
  Blit4xInternal();
  if (needUnlock) SDL_UnlockSurface(screenReal);
}


static void Blit4xTV (void) {
  const int needUnlock = (SDL_MUSTLOCK(screenReal) ? SDL_LockSurface(screenReal) == 0 : 0);
  Blit4xInternal();
  /*FIXME: make this faster!*/
  TV4xInternal();
  if (needUnlock) SDL_UnlockSurface(screenReal);
}


static void Blit3xInternal (void) {
  if (vid_scaler_type == VID_SCALER_nX) {
    fixSourceColors();
    scale(3, screenReal->pixels, screenReal->pitch,
             screen->pixels, screen->pitch,
             4, 320, 240);
    return;
  }
  if (vid_scaler_type == VID_SCALER_HQX) {
    if (!hqx_inited) { hqx_inited = 1; hqxInit(); }
    fixSourceColors();
    hq3x_32_rb(screen->pixels, screen->pitch,
               screenReal->pixels, screenReal->pitch,
               320, 240);
    return;
  }
  const Uint32 *scrB = screen->pixels; // 320x240
  Uint32 *scrR = screenReal->pixels; // 960x720
  for (int y = 0; y < screen->h; ++y) {
    const Uint32 *s = scrB;
    Uint32 *d0 = scrR;
    Uint32 *d1 = (Uint32 *)((Uint8 *)d0+screenReal->pitch);
    Uint32 *d2 = (Uint32 *)((Uint8 *)d1+screenReal->pitch);
    for (int x = 0; x < screen->w; ++x) {
      const Uint32 clr = fixColor(*s++);
      d0[0] = d0[1] = d0[2] = clr;
      d1[0] = d1[1] = d1[2] = clr;
      d2[0] = d2[1] = d2[2] = clr;
      d0 += 3;
      d1 += 3;
      d2 += 3;
    }
    scrB = (Uint32 *)((Uint8 *)scrB+screen->pitch);
    scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch*3);
  }
}


static void TV3xInternal (void) {
  Uint32 *scrR = screenReal->pixels; // 960x720
  scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch);
  for (int y = screenReal->h/2; y--; scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch*2)) {
    Uint32 *d0 = scrR;
    for (int x = 0; x < screenReal->w; ++x) {
      const Uint32 c0 = *d0;
      Uint32 c1 = (((c0&0x00ff00ff)*7)>>3)&0x00ff00ff;
      c1 |= (((c0&0x0000ff00)*7)>>3)&0x0000ff00;
      *d0++ = c1;
    }
  }
}


static void Blit3x (void) {
  const int needUnlock = (SDL_MUSTLOCK(screenReal) ? SDL_LockSurface(screenReal) == 0 : 0);
  Blit3xInternal();
  if (needUnlock) SDL_UnlockSurface(screenReal);
}


static void Blit3xTV (void) {
  const int needUnlock = (SDL_MUSTLOCK(screenReal) ? SDL_LockSurface(screenReal) == 0 : 0);
  /*FIXME: make this faster!*/
  Blit3xInternal();
  TV3xInternal();
  if (needUnlock) SDL_UnlockSurface(screenReal);
}


static void Blit2x (void) {
  if (vid_scaler_type == VID_SCALER_nX) {
    fixSourceColors();
    scale(2, screenReal->pixels, screenReal->pitch,
             screen->pixels, screen->pitch,
             4, 320, 240);
    return;
  }
  if (vid_scaler_type == VID_SCALER_HQX) {
    if (!hqx_inited) { hqx_inited = 1; hqxInit(); }
    fixSourceColors();
    hq2x_32_rb(screen->pixels, screen->pitch,
               screenReal->pixels, screenReal->pitch,
               320, 240);
    return;
  }
  const int needUnlock = (SDL_MUSTLOCK(screenReal) ? SDL_LockSurface(screenReal) == 0 : 0);
  const Uint32 *scrB = screen->pixels; // 320x240
  Uint32 *scrR = screenReal->pixels; // 640x480
  for (int y = 0; y < screen->h; ++y) {
    const Uint32 *s = scrB;
    Uint32 *d = scrR, *d2 = (Uint32 *)((Uint8 *)d+screenReal->pitch);
    for (int x = 0; x < screen->w; ++x) {
      const Uint32 clr = fixColor(*s++);
      d[0] = d[1] = clr;
      d2[0] = d2[1] = clr;
      d += 2; d2 += 2;
      //++s;
    }
    scrB = (Uint32 *)((Uint8 *)scrB+screen->pitch);
    scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch*2);
  }
  if (needUnlock) SDL_UnlockSurface(screenReal);
}


static void Blit2xTV (void) {
  const int needUnlock = (SDL_MUSTLOCK(screenReal) ? SDL_LockSurface(screenReal) == 0 : 0);
  const Uint32 *scrB = screen->pixels; // 320x240
  Uint32 *scrR = screenReal->pixels; // 640x480
  for (int y = 0; y < screen->h; ++y) {
    const Uint32 *s = scrB;
    Uint32 *d = scrR, *d2 = (Uint32 *)((Uint8 *)d+screenReal->pitch);
    for (int x = 0; x < screen->w; ++x) {
      const Uint32 c0 = fixColor(*s);
      Uint32 c1 = (((c0&0x00ff00ff)*7)>>3)&0x00ff00ff;
      c1 |= (((c0&0x0000ff00)*7)>>3)&0x0000ff00;
      d[0] = d[1] = c0;
      d2[0] = d2[1] = c1;
      d += 2; d2 += 2;
      ++s;
    }
    scrB = (Uint32 *)((Uint8 *)scrB+screen->pitch);
    scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch*2);
  }
  if (needUnlock) SDL_UnlockSurface(screenReal);
}


static void Blit1x (void) {
  const int needUnlock = (SDL_MUSTLOCK(screenReal) ? SDL_LockSurface(screenReal) == 0 : 0);
  const Uint32 *scrB = screen->pixels; // 320x240
  Uint32 *scrR = screenReal->pixels; // 320x240
  for (int y = 0; y < screen->h; ++y) {
    const Uint32 *s = scrB;
    Uint32 *d = scrR;
    for (int x = 0; x < screen->w; ++x) {
      const Uint32 clr = fixColor(*s++);
      d[0] = clr;
      d += 1;
    }
    scrB = (Uint32 *)((Uint8 *)scrB+screen->pitch);
    scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch);
  }
  if (needUnlock) SDL_UnlockSurface(screenReal);
}


static void vidRenderTextScr16x2 (SDL_Surface *sfc) {
  // 8x16, x2 scaled
  Uint32 *sp = (Uint32 *)((Uint8 *)sfc->pixels);
  // move down a little
  /*
       if (sfc->h > 480*2+16*2) sp = (Uint32 *)((Uint8 *)sp+sfc->pitch*16);
  else if (sfc->h > 480*2) sp = (Uint32 *)((Uint8 *)sp+sfc->pitch*((sfc->h-480)/2));
  */
  // center horizontally
  sp += (sfc->w-640*2)/2;
  // render
  const uint32_t vhgt =
    vid_textsrc_height <= 0 ? (uint32_t)VID_TEXT_HEIGHT :
    vid_textsrc_height > VID_TEXT_HEIGHT ? VID_TEXT_HEIGHT :
    (uint32_t)vid_textsrc_height;
  const VidTChar *tp = vid_textscr;
  for (uint32_t y = 0; y < vhgt; ++y) {
    Uint32 *d = sp;
    for (uint32_t x = 0; x < (uint32_t)VID_TEXT_WIDTH; ++x, ++tp, d += 16) {
      if (tp->attr == 0) continue;
      Uint32 bc = palette[(tp->attr>>4)&0x0f];
      Uint32 fc = palette[tp->attr&0x0f];
      const uint8_t *cptr = vid_font8x16+16*tp->ch;
      Uint32 *cdest0 = d;
      Uint32 *cdest1 = d;
      cdest1 = (Uint32 *)((Uint8 *)cdest1+sfc->pitch);
      for (uint32_t dy = 0; dy < 16; ++dy) {
        uint8_t b = *cptr++;
        for (uint32_t dx = 0; dx < 8; ++dx) {
          *cdest0++ = (b&0x80 ? fc : bc);
          *cdest0++ = (b&0x80 ? fc : bc);
          *cdest1++ = (b&0x80 ? fc : bc);
          *cdest1++ = (b&0x80 ? fc : bc);
          b <<= 1;
        }
        cdest0 -= 16;
        cdest0 = (Uint32 *)((Uint8 *)cdest0+sfc->pitch*2);
        cdest1 -= 16;
        cdest1 = (Uint32 *)((Uint8 *)cdest1+sfc->pitch*2);
      }
    }
    sp = (Uint32 *)((Uint8 *)sp+sfc->pitch*(16*2));
  }
}


static void vidRenderTextScr16 (SDL_Surface *sfc) {
  // 8x16
  Uint32 *sp = (Uint32 *)((Uint8 *)sfc->pixels);
  // move down a little
  /*
       if (sfc->h > 480+16*2) sp = (Uint32 *)((Uint8 *)sp+sfc->pitch*16);
  else if (sfc->h > 480) sp = (Uint32 *)((Uint8 *)sp+sfc->pitch*((sfc->h-480)/2));
  */
  // center horizontally
  sp += (sfc->w-640)/2;
  // render
  const uint32_t vhgt =
    vid_textsrc_height <= 0 ? (uint32_t)VID_TEXT_HEIGHT :
    vid_textsrc_height > VID_TEXT_HEIGHT ? VID_TEXT_HEIGHT :
    (uint32_t)vid_textsrc_height;
  const VidTChar *tp = vid_textscr;
  for (uint32_t y = 0; y < vhgt; ++y) {
    Uint32 *d = sp;
    for (uint32_t x = 0; x < (uint32_t)VID_TEXT_WIDTH; ++x, ++tp, d += 8) {
      if (tp->attr == 0) continue;
      Uint32 bc = palette[(tp->attr>>4)&0x0f];
      Uint32 fc = palette[tp->attr&0x0f];
      const uint8_t *cptr = vid_font8x16+16*tp->ch;
      Uint32 *cdest = d;
      for (uint32_t dy = 0; dy < 16; ++dy) {
        uint8_t b = *cptr++;
        for (uint32_t dx = 0; dx < 8; ++dx) {
          *cdest++ = (b&0x80 ? fc : bc);
          b <<= 1;
        }
        cdest -= 8;
        cdest = (Uint32 *)((Uint8 *)cdest+sfc->pitch);
      }
    }
    sp = (Uint32 *)((Uint8 *)sp+sfc->pitch*16);
  }
}


static void vidRenderTextScr8 (SDL_Surface *sfc) {
  // 8x8
  Uint32 *sp = (Uint32 *)((Uint8 *)sfc->pixels);
  // move down a little
  /*
       if (sfc->h > 240+8*2) sp = (Uint32 *)((Uint8 *)sp+sfc->pitch*8);
  else if (sfc->h > 240) sp = (Uint32 *)((Uint8 *)sp+sfc->pitch*((sfc->h-240)/2));
  */
  // center horizontally
  //sp += (sfc->w-320)/2;
  // render
  const uint32_t vhgt =
    vid_textsrc_height <= 0 ? (uint32_t)VID_TEXT_HEIGHT :
    vid_textsrc_height > VID_TEXT_HEIGHT ? VID_TEXT_HEIGHT :
    (uint32_t)vid_textsrc_height;
  const VidTChar *tp = vid_textscr;
  for (uint32_t y = 0; y < vhgt; ++y) {
    Uint32 *d = sp;
    for (uint32_t x = 0; x < (uint32_t)VID_TEXT_WIDTH; ++x, ++tp, d += 4) {
      if (tp->attr == 0) continue;
      Uint32 bc = palette[(tp->attr>>4)&0x0f];
      Uint32 fc = palette[tp->attr&0x0f];
      const uint8_t *cptr = vid_font8x8+8*tp->ch;
      Uint32 *cdest = d;
      for (uint32_t dy = 0; dy < 8; ++dy) {
        uint8_t b = *cptr++;
        for (uint32_t dx = 0; dx < 4; ++dx) {
          *cdest++ = (b&0x80 ? fc : bc);
          b <<= 1;
          b <<= 1;
        }
        cdest -= 4;
        cdest = (Uint32 *)((Uint8 *)cdest+sfc->pitch);
      }
    }
    sp = (Uint32 *)((Uint8 *)sp+sfc->pitch*8);
  }
}


static void vidRenderTextScr (SDL_Surface *sfc) {
       if (sfc->w >= 320*4 && sfc->h >= 240*4) vidRenderTextScr16x2(sfc);
  else if (sfc->w >= 320*2 && sfc->h >= 240*2) vidRenderTextScr16(sfc);
  else vidRenderTextScr8(sfc);
}


static void screenFlip (void) {
  /*
  lockSurface(&frameSfcLock, screen);
  drawZFont();
  unlockSurface(&frameSfcLock);
  */
  //
  if (screenReal != NULL) {
    /*
    if (!optFullscreen) {
      if (optTVScaler) Blit3xTV(); else Blit3x();
    } else {
      if (optTVScaler > 1 || (optTVScaler && !optFullscreen)) Blit2xTV(); else Blit2x();
    }
    */
    switch (actualScale) {
      case 1: Blit1x(); break;
      case 2: if (optTVScaler) Blit2xTV(); else Blit2x(); break;
      case 3: if (optTVScaler) Blit3xTV(); else Blit3x(); break;
      case 4: if (optTVScaler) Blit4xTV(); else Blit4x(); break;
      default: __builtin_trap();
    }

    if (vidBeforeFlipCB) {
      const int needUnlock = (SDL_MUSTLOCK(screenReal) ? SDL_LockSurface(screenReal) == 0 : 0);
      vidBeforeFlipCB(screenReal);
      if (needUnlock) SDL_UnlockSurface(screenReal);
    }

    if (vid_textscr_active) {
      const int needUnlock = (SDL_MUSTLOCK(screenReal) ? SDL_LockSurface(screenReal) == 0 : 0);
      vidRenderTextScr(screenReal);
      if (needUnlock) SDL_UnlockSurface(screenReal);
    }

    SDL_Flip(screenReal);
  } else {
    if (vidBeforeFlipCB) {
      const int needUnlock = (SDL_MUSTLOCK(screen) ? SDL_LockSurface(screen) == 0 : 0);
      vidBeforeFlipCB(screen);
      if (needUnlock) SDL_UnlockSurface(screen);
    }

    if (vid_textscr_active) {
      const int needUnlock = (SDL_MUSTLOCK(screen) ? SDL_LockSurface(screen) == 0 : 0);
      vidRenderTextScr(screen);
      if (needUnlock) SDL_UnlockSurface(screen);
    }

    SDL_Flip(screen);
  }
}


////////////////////////////////////////////////////////////////////////////////
void forcedShowFrame (void) {
  if (frameSfc != NULL) {
    frameSfcRaped = 1;
    unlockSurface(&frameSfcLock);
    screenFlip();
    lockSurface(&frameSfcLock, screen);
  }
}


void buildFrame (void) {
  frameSfcRaped = 0;
  if (frameCB != NULL) {
    lockSurface(&frameSfcLock, screen);
    frameSfc = screen;
    frameCB();
    frameSfc = NULL;
    unlockSurface(&frameSfcLock);
  }
  if (!frameSfcRaped) screenFlip();
}


static Uint32 cbFrameTimer (Uint32 interval, void *param) {
  SDL_Event evt;
  //
  evt.type = SDL_USEREVENT;
  /*
  //evt.user.type = SDL_USEREVENT;
  evt.user.code = 0;
  evt.user.data1 = NULL;
  evt.user.data2 = NULL;
  */
  SDL_PushEvent(&evt);
  //fprintf(stderr, "!\n");
  return interval;
}


////////////////////////////////////////////////////////////////////////////////
void postQuitMessage (void) {
  SDL_Event evt;
  //
  evt.type = SDL_QUIT;
  SDL_PushEvent(&evt);
}


////////////////////////////////////////////////////////////////////////////////
int msButtons = 0, msLastX = 0, msLastY = 0, kbCtrl = 0, kbAlt = 0, kbShift = 0;
int kbLCtrl = 0, kbRCtrl = 0, kbLAlt = 0, kbRAlt = 0, kbLShift = 0, kbRShift = 0;
int msAutoTimeMSFirst = 500;
int msAutoTimeMSRepeat = 100;
int msAutoTresholdX = 2;
int msAutoTresholdY = 2;


typedef struct {
  int64_t time;
  int x, y;
  int repeat;
} MSPressInfo;

static MSPressInfo msbi[5]; // left,right,middle


typedef struct {
  SDLKey key;
  int *var;
} KBCtrlKeyInfo;


static const KBCtrlKeyInfo kbCtrlKeys[] = {
  {.key=SDLK_LCTRL, .var=&kbLCtrl},
  {.key=SDLK_RCTRL, .var=&kbRCtrl},
  {.key=SDLK_LALT, .var=&kbLAlt},
  {.key=SDLK_RALT, .var=&kbRAlt},
  {.key=SDLK_LSHIFT, .var=&kbLShift},
  {.key=SDLK_RSHIFT, .var=&kbRShift},
};


static void initEventSystem (void) {
  memset(msbi, 0, sizeof(msbi));
}


static void fixKbCtrl (SDLKey key, int isdown) {
  for (unsigned int f = 0; f < sizeof(kbCtrlKeys)/sizeof(KBCtrlKeyInfo); ++f) {
    if (key == kbCtrlKeys[f].key) *(kbCtrlKeys[f].var) = isdown;
  }
  kbCtrl = kbLCtrl||kbRCtrl;
  kbAlt = kbLAlt||kbRAlt;
  kbShift = kbLShift||kbRShift;
}


static void kbDepressCtrls (void) {
  for (unsigned int f = 0; f < sizeof(kbCtrlKeys)/sizeof(KBCtrlKeyInfo); ++f) {
    if (*(kbCtrlKeys[f].var)) {
      fixKbCtrl(kbCtrlKeys[f].key, 0);
      if (keyCB) {
        SDL_Event event;
        //
        event.type = SDL_KEYUP;
        event.key.state = SDL_RELEASED;
        event.key.keysym.scancode = 0;
        event.key.keysym.unicode = 0;
        event.key.keysym.sym = kbCtrlKeys[f].key;
        event.key.keysym.mod =
          (kbLCtrl ? KMOD_LCTRL : 0) |
          (kbRCtrl ? KMOD_RCTRL : 0) |
          (kbLAlt ? KMOD_LALT : 0) |
          (kbRAlt ? KMOD_RALT : 0) |
          (kbLShift ? KMOD_LSHIFT : 0) |
          (kbRShift ? KMOD_RSHIFT : 0) |
          0;
        keyCB(&event.key);
      }
    }
  }
}


static void doAutoMouse (void) {
  if (mouseButtonCB && msAutoTimeMSFirst > 0) {
    int64_t t = timerGetMS();
    //
    for (int b = 0; b < 3; ++b) {
      int n = (1<<b);
      //
      if (msbi[n].time > 0) {
        if (!msbi[n].repeat && (abs(msLastX-msbi[n].x) > msAutoTresholdX || abs(msLastY-msbi[n].y) > msAutoTresholdY)) {
          // mouse moved too far, stop autorepeating
          msbi[n].time = 0;
        } else if ((msbi[n].repeat && t-msbi[n].time >= msAutoTimeMSRepeat) ||
                   (!msbi[n].repeat && t-msbi[n].time >= msAutoTimeMSFirst)) {
          // generate repeat
          msbi[n].time = t;
          msbi[n].repeat = 1;
          mouseButtonCB(msLastX, msLastY, (n|MS_BUTTON_AUTO), msButtons);
        }
      }
    }
  }
}


// -1: quit; 1: new frame
int processEvents (int dowait) {
  SDL_Event event;
  //
  while ((dowait ? SDL_WaitEvent(&event) : SDL_PollEvent(&event))) {
    switch (event.type) {
      case SDL_QUIT: // the user want to quit
        return -1;
      case SDL_KEYDOWN:
      case SDL_KEYUP:
        fixKbCtrl(event.key.keysym.sym, (event.type == SDL_KEYDOWN));
        if (keyCB) keyCB(&event.key);
        break;
      case SDL_MOUSEMOTION: {
        //int buttons;
        int x = event.motion.x, y = event.motion.y, rx = event.motion.xrel, ry = event.motion.yrel;
        msRealX = x; msRealY = y;
        //
        if (screenReal != NULL) { x /= 2; y /= 2; }
        msLastX = x;
        msLastY = y;
        //SDLMB2VMB(event.motion.state);
        //msButtons = buttons;
        if (mouseCB) mouseCB(x, y, rx, ry, msButtons);
        break; }
      case SDL_MOUSEBUTTONUP:
      case SDL_MOUSEBUTTONDOWN: {
        int x = event.button.x, y = event.button.y, btn = 0;
        //
        if (screenReal != NULL) { x /= 2; y /= 2; }
        msLastX = x;
        msLastY = y;
        switch (event.button.button) {
          case SDL_BUTTON_LEFT: btn = MS_BUTTON_LEFT; break;
          case SDL_BUTTON_RIGHT: btn = MS_BUTTON_RIGHT; break;
          case SDL_BUTTON_MIDDLE: btn = MS_BUTTON_MIDDLE; break;
          case SDL_BUTTON_WHEELUP: btn = MS_BUTTON_WHEELUP; break;
          case SDL_BUTTON_WHEELDOWN: btn = MS_BUTTON_WHEELDOWN; break;
          default: ;
        }
        //fprintf(stderr, "DOWN:%d; btn:%d; bt:%02x\n", (event.button.state == SDL_PRESSED), btn, msButtons);
        if (btn != 0) {
          if (event.button.state == SDL_PRESSED) {
            msButtons |= btn;
            if (btn <= MS_BUTTON_MIDDLE) {
              msbi[btn].time = timerGetMS();
              msbi[btn].x = x;
              msbi[btn].y = y;
              msbi[btn].repeat = 0;
            }
          } else {
            msButtons &= ~btn;
            btn |= MS_BUTTON_DEPRESSED;
            if (btn <= MS_BUTTON_MIDDLE) msbi[btn].time = 0;
          }
          if (mouseButtonCB) mouseButtonCB(x, y, btn, msButtons);
        }
        break; }
      case SDL_ACTIVEEVENT:
        if (event.active.gain) {
          vidWindowActivated = 1;
          if (windowActivationCB != NULL) windowActivationCB(1);
        } else {
          vidWindowActivated = 0;
          SDL_WM_GrabInput(SDL_GRAB_OFF);
          // just in case
          kbDepressCtrls();
          if (msButtons != 0) {
            if (mouseButtonCB != NULL) {
              for (unsigned int mask = 0x01; mask < MS_BUTTON_DEPRESSED; mask <<= 1) {
                if (msButtons&mask) {
                  msButtons &= ~mask;
                  mouseButtonCB(msLastX, msLastY, mask|MS_BUTTON_DEPRESSED, msButtons);
                }
              }
            }
            msButtons = 0;
          }
          if (windowActivationCB != NULL) windowActivationCB(0);
        }
        break;
      case SDL_USEREVENT:
        //while (SDL_PollEvent(NULL)) processEvents();
        doAutoMouse();
        return 1;
      default: ;
    }
  }
  doAutoMouse();
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
void mainLoop (void) {
  SDL_TimerID frameTimer = SDL_AddTimer(20, cbFrameTimer, NULL);
  int eres = 1;
  //
  while (eres >= 0) {
    if (eres > 0) buildFrame();
    eres = processEvents(1);
  }
  //
  SDL_RemoveTimer(frameTimer);
}


////////////////////////////////////////////////////////////////////////////////
VOverlay *createVO (int w, int h) {
  if (w > 0 && h > 0) {
    VOverlay *res = malloc(sizeof(VOverlay));
    if (res != NULL) {
      if ((res->data = malloc(w*h)) != NULL) {
        res->w = w;
        res->h = h;
        return res;
      }
      free(res);
    }
  }
  return NULL;
}


void resizeVO (VOverlay *v, int w, int h) {
  if (v && w > 0 && h > 0 && (v->w != w || v->h != h)) {
    v->data = realloc(v->data, w*h);
    if (!v->data) { fprintf(stderr, "\bFATAL: out of memory!\n"); abort(); }
    v->w = w;
    v->h = h;
  }
}


VOverlay *createVOFromIcon (const void *iconbuf) {
  if (iconbuf != NULL) {
    const uint8_t *ibuf = (const uint8_t *)iconbuf;
    VOverlay *res = createVO(ibuf[0], ibuf[1]);
    //
    if (res != NULL) {
      clearVO(res, 255);
      for (int y = 0; y < ibuf[1]; ++y) {
        for (int x = 0; x < ibuf[0]; ++x) {
          putPixelVO(res, x, y, ibuf[2+y*ibuf[0]+x]);
        }
      }
      return res;
    }
  }
  return NULL;
}


void freeVO (VOverlay *v) {
  if (v != NULL) {
    if (v->data != NULL) free(v->data);
    free(v);
  }
}

void clearVO (VOverlay *v, Uint8 c) {
  if (v != NULL && v->w > 0 && v->h > 0) memset(v->data, c, v->w*v->h);
}


////////////////////////////////////////////////////////////////////////////////
void drawChar6VO (VOverlay *v, char ch, int x, int y, Uint8 fg, Uint8 bg) {
  if (v != NULL && x > -6 && y > -8 && x < v->w+6 && y < v->h+8) {
    int pos = (ch&0xff)*8;
    //
    for (int dy = 0; dy < 8; ++dy, ++y) {
      uint8_t b = font6x8[pos++];
      //
      for (int dx = 0; dx < 6; ++dx) {
        Uint8 c = (b&0x80 ? fg : bg);
        //
        if (c != 255) putPixelVO(v, x+dx, y, c);
        b = (b&0x7f)<<1;
      }
    }
  }
}


void drawStr6VO (VOverlay *v, const char *str, int x, int y, Uint8 fg, Uint8 bg) {
  if (str != NULL) for (; *str; ++str, x += 6) drawChar6VO(v, *str, x, y, fg, bg);
}


void drawChar8VO (VOverlay *v, char ch, int x, int y, Uint8 fg, Uint8 bg) {
  if (v != NULL && x > -8 && y > -8 && x < v->w+8 && y < v->h+8) {
    int pos = (ch&0xff)*8;
    //
    for (int dy = 0; dy < 8; ++dy, ++y) {
      uint8_t b = msxfont8x8[pos++];
      //
      for (int dx = 0; dx < 8; ++dx) {
        Uint8 c = (b&0x80 ? fg : bg);
        //
        if (c != 255) putPixelVO(v, x+dx, y, c);
        b = (b&0x7f)<<1;
      }
    }
  }
}


void drawStr8VO (VOverlay *v, const char *str, int x, int y, Uint8 fg, Uint8 bg) {
  if (str != NULL) for (; *str; ++str, x += 8) drawChar8VO(v, *str, x, y, fg, bg);
}


void drawFrameVO (VOverlay *v, int x0, int y0, int w, int h, Uint8 c) {
  if (v != NULL && w > 0 && h > 0 && x0 > -w && y0 > -h && x0 < v->w && y0 < v->h) {
    if (x0 < 0) { if ((w += x0) < 1) return; x0 = 0; }
    if (y0 < 0) { if ((h += y0) < 1) return; y0 = 0; }
    if (x0+w > v->w) { if ((w = v->w-x0) < 1) return; }
    if (y0+h > v->h) { if ((h = v->h-y0) < 1) return; }
    //
    memset(v->data+y0*v->w+x0, c, w);
    if (h > 1) memset(v->data+(y0+h-1)*v->w+x0, c, w);
    if (h > 2) {
      --w;
      h -= 2;
      for (uint8_t *d = v->data+(y0+1)*v->w+x0; h > 0; --h, d += v->w) d[0] = d[w] = c;
    }
  }
}


void fillRectVO (VOverlay *v, int x0, int y0, int w, int h, Uint8 c) {
  if (v != NULL && w > 0 && h > 0 && x0 > -w && y0 > -h && x0 < v->w && y0 < v->h) {
    if (x0 < 0) { if ((w += x0) < 1) return; x0 = 0; }
    if (y0 < 0) { if ((h += y0) < 1) return; y0 = 0; }
    if (x0+w > v->w) { if ((w = v->w-x0) < 1) return; }
    if (y0+h > v->h) { if ((h = v->h-y0) < 1) return; }
    //
    for (uint8_t *d = v->data+y0*v->w+x0; h > 0; --h, d += v->w) memset(d, c, w);
  }
}


////////////////////////////////////////////////////////////////////////////////
void blitVO (VOverlay *v, int x0, int y0, Uint8 alpha) {
  if (v != NULL && x0 > -v->w && y0 > -v->h && x0 < frameSfc->w && y0 < frameSfc->h && alpha > 0) {
    int w = v->w, h = v->h;
    const uint8_t *vs = v->data;
    Uint8 *dest;
    //
    if (y0 < 0) {
      // skip invisible top part
      if ((h += y0) < 1) return;
      vs -= y0*v->w;
      y0 = 0;
    }
    if (y0+h > frameSfc->h) {
      // don't draw invisible bottom part
      if ((h = frameSfc->h-y0) < 1) return;
    }
    //
    if (x0 < 0) {
      // skip invisible left part
      if ((w += x0) < 1) return;
      vs -= x0;
      x0 = 0;
    }
    if (x0+w > frameSfc->w) {
      // don't draw invisible right part
      if ((w = frameSfc->w-x0) < 1) return;
    }
    //
    dest = (((Uint8 *)frameSfc->pixels)+(y0*frameSfc->pitch)+x0*4);
    if (alpha == 255) {
      for (; h > 0; --h, dest += frameSfc->pitch, vs += v->w) {
        Uint32 *d = (Uint32 *)dest;
        const uint8_t *s = vs;
        //
        for (int dx = w; dx > 0; --dx, ++d, ++s) if (*s != 255) *d = palette[*s];
      }
    } else {
      for (; h > 0; --h, dest += frameSfc->pitch, vs += v->w) {
        Uint8 *d = dest;
        const uint8_t *s = vs;
        //
        for (int dx = w; dx > 0; --dx, d += 4, ++s) {
          uint8_t c;
          //
          if ((c = *s) != 255) {
            uint8_t r = d[vidRIdx], g = d[vidGIdx], b = d[vidBIdx];
            //
            d[vidRIdx] = (((palRGB[c][0]-r)*alpha)>>8)+r;
            d[vidGIdx] = (((palRGB[c][1]-g)*alpha)>>8)+g;
            d[vidBIdx] = (((palRGB[c][2]-b)*alpha)>>8)+b;
          }
        }
      }
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
#include "videoex.c"


//==========================================================================
//
//  vt_getch
//
//==========================================================================
static inline uint8_t vt_getch (int x, int y) {
  if (x < 0 || y < 0 || x >= VID_TEXT_WIDTH || y >= VID_TEXT_HEIGHT) return 0;
  const VidTChar *d = vid_textscr+(unsigned)y*(unsigned)VID_TEXT_WIDTH+(unsigned)x;
  return d->ch;
}


//==========================================================================
//
//  vt_writeattr
//
//==========================================================================
void vt_writeattr (int x, int y, uint8_t attr) {
  if (x < 0 || y < 0 || x >= VID_TEXT_WIDTH || y >= VID_TEXT_HEIGHT) return;
  VidTChar *d = vid_textscr+(unsigned)y*(unsigned)VID_TEXT_WIDTH+(unsigned)x;
  d->attr = attr;
}


//==========================================================================
//
//  vt_writeattrs
//
//==========================================================================
void vt_writeattrs (int x, int y, int count, uint8_t attr) {
  if (count <= 0 || y < 0 || x >= VID_TEXT_WIDTH || y >= VID_TEXT_HEIGHT) return;
  if (x < 0) {
    count += x;
    if (count <= 0) return;
    x = 0;
  }
  if (VID_TEXT_WIDTH-x < count) count = VID_TEXT_WIDTH-x;
  VidTChar *d = vid_textscr+(unsigned)y*(unsigned)VID_TEXT_WIDTH+(unsigned)x;
  while (count--) {
    d->attr = attr;
    ++d;
  }
}


//==========================================================================
//
//  vt_draw_shadow
//
//==========================================================================
void vt_draw_shadow (int x, int y, int w, int h) {
  if (w <= 0 || h <= 0 || y < 0 || x >= VID_TEXT_WIDTH || y >= VID_TEXT_HEIGHT) return;
  if (x < 0) { w += x; if (w <= 0) return; x = 0; }
  if (y < 0) { h += y; if (h <= 0) return; y = 0; }
  if (VID_TEXT_WIDTH-x < w) w = VID_TEXT_WIDTH-x;
  if (VID_TEXT_HEIGHT-y < h) h = VID_TEXT_HEIGHT-y;

  VidTChar *ss = vid_textscr+(unsigned)y*(unsigned)VID_TEXT_WIDTH+(unsigned)x;
  while (h--) {
    VidTChar *d = ss;
    for (int f = 0; f < w; ++f, ++d) {
      if (!d->attr) continue;
      /*if (d->attr > 8)*/ d->attr = 1;
    }
    ss += VID_TEXT_WIDTH;
  }
}


//==========================================================================
//
//  vt_cls
//
//==========================================================================
void vt_cls (uint8_t ch, uint8_t attr) {
  VidTChar *d = vid_textscr;
  uint32_t left = VID_TEXT_WIDTH*VID_TEXT_HEIGHT;
  while (left--) {
    d->ch = ch;
    d->attr = attr;
    ++d;
  }
}


//==========================================================================
//
//  vt_writechar
//
//==========================================================================
void vt_writechar (int x, int y, uint8_t ch, uint8_t attr) {
  if (x < 0 || y < 0 || x >= VID_TEXT_WIDTH || y >= VID_TEXT_HEIGHT) return;
  VidTChar *d = vid_textscr+(unsigned)y*(unsigned)VID_TEXT_WIDTH+(unsigned)x;
  d->ch = ch;
  d->attr = attr;
}


//==========================================================================
//
//  vt_writechars
//
//==========================================================================
void vt_writechars (int x, int y, int count, uint8_t ch, uint8_t attr) {
  if (count <= 0 || y < 0 || x >= VID_TEXT_WIDTH || y >= VID_TEXT_HEIGHT) return;
  if (x < 0) {
    count += x;
    if (count <= 0) return;
    x = 0;
  }
  if (VID_TEXT_WIDTH-x < count) count = VID_TEXT_WIDTH-x;
  VidTChar *d = vid_textscr+(unsigned)y*(unsigned)VID_TEXT_WIDTH+(unsigned)x;
  while (count--) {
    d->ch = ch;
    d->attr = attr;
    ++d;
  }
}


//==========================================================================
//
//  vt_writestrcnt
//
//==========================================================================
void vt_writestrcnt (int x, int y, const char *s, size_t slen, uint8_t attr) {
  if (!slen || !s) return;
  if (y < 0 || x >= VID_TEXT_WIDTH || y >= VID_TEXT_HEIGHT) return;
  if (slen > 0x3fffffffU) slen = 0x3fffffffU;
  int count = (int)slen;
  if (x < 0) {
    count += x;
    if (count <= 0) return;
    x = 0;
  }
  if (VID_TEXT_WIDTH-x < count) count = VID_TEXT_WIDTH-x;
  VidTChar *d = vid_textscr+(unsigned)y*(unsigned)VID_TEXT_WIDTH+(unsigned)x;
  while (count--) {
    d->ch = *(const uint8_t *)s;
    d->attr = attr;
    ++d;
    ++s;
  }
}


//==========================================================================
//
//  vt_writestrz
//
//==========================================================================
void vt_writestrz (int x, int y, const char *s, uint8_t attr) {
  if (!s || !s[0]) return;
  if (y < 0 || x >= VID_TEXT_WIDTH || y >= VID_TEXT_HEIGHT) return;
  //vt_writestrcnt(x, y, s, strlen(s), attr);
  const uint8_t *src = (const uint8_t *)s;
  VidTChar *d = vid_textscr+(unsigned)y*(unsigned)VID_TEXT_WIDTH;
  if (x >= 0) d += (unsigned)x;
  while (x < VID_TEXT_WIDTH) {
    uint8_t ch = *src++;
    if (!ch) break;
    if (x >= 0) {
      d->ch = ch;
      d->attr = attr;
      ++d;
    }
    ++x;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
static char vabuff[1024];
static char *vabufptr = &vabuff[0];
static size_t vabufsz = sizeof(vabuff);


//==========================================================================
//
//  vt_writeva
//
//==========================================================================
void vt_writeva (int x, int y, uint8_t attr, const char *fmt, va_list ap) {
  if (!fmt || !fmt[0]) return;
  if (y < 0 || x >= VID_TEXT_WIDTH || y >= VID_TEXT_HEIGHT) return;
  for (;;) {
    va_list cp;
    va_copy(cp, ap);
    int sz = vsnprintf(vabufptr, vabufsz, fmt, cp);
    va_end(cp);
    if (sz < (int)vabufsz) {
      vt_writestrcnt(x, y, vabufptr, (size_t)sz, attr);
      return;
    }
    // we need more buffer memory
    if (vabufptr == &vabuff[0]) vabufptr = NULL;
    vabufsz = (sz < 0 ? vabufsz*2 : (size_t)sz+1024);
    vabufptr = realloc(vabufptr, vabufsz);
    if (!vabufptr) { fprintf(stderr, "OUT OF MEMORY!\n"); fflush(stderr); __builtin_trap(); }
  }
}


//==========================================================================
//
//  vt_writef
//
//==========================================================================
__attribute__((format(printf,4,5))) void vt_writef (int x, int y, uint8_t attr, const char *fmt, ...) {
  if (!fmt || !fmt[0]) return;
  if (y < 0 || x >= VID_TEXT_WIDTH || y >= VID_TEXT_HEIGHT) return;
  va_list ap;
  va_start(ap, fmt);
  vt_writeva(x, y, attr, fmt, ap);
  va_end(ap);
}


// ////////////////////////////////////////////////////////////////////////// //
// WARNING! do not change the order!
#define VC_TOP     (0x01U)
#define VC_BOTTOM  (0x02U)
#define VC_LEFT    (0x04U)
#define VC_RIGHT   (0x08U)

#define VC_DBL_TOP     (0x10U)
#define VC_DBL_BOTTOM  (0x20U)
#define VC_DBL_LEFT    (0x40U)
#define VC_DBL_RIGHT   (0x80U)


// [0xb0..223]
static const uint8_t vt_char_class[48] = {
  0, 0, 0,
  VC_TOP|VC_BOTTOM,
  VC_TOP|VC_BOTTOM|VC_LEFT,
  VC_TOP|VC_BOTTOM|VC_LEFT|VC_DBL_LEFT,
  VC_TOP|VC_BOTTOM|VC_LEFT|VC_DBL_TOP|VC_DBL_BOTTOM,
  VC_LEFT|VC_BOTTOM|VC_DBL_BOTTOM,
  VC_LEFT|VC_BOTTOM|VC_DBL_LEFT,
  VC_TOP|VC_BOTTOM|VC_LEFT|VC_DBL_TOP|VC_DBL_BOTTOM|VC_DBL_LEFT,
  VC_TOP|VC_BOTTOM|VC_DBL_TOP|VC_DBL_BOTTOM,
  VC_LEFT|VC_BOTTOM|VC_DBL_LEFT|VC_DBL_BOTTOM,
  VC_LEFT|VC_TOP|VC_DBL_LEFT|VC_DBL_TOP,
  VC_LEFT|VC_TOP|VC_DBL_TOP,
  VC_LEFT|VC_TOP|VC_DBL_LEFT,
  VC_LEFT|VC_BOTTOM,
  //
  VC_RIGHT|VC_TOP,
  VC_LEFT|VC_RIGHT|VC_TOP,
  VC_LEFT|VC_RIGHT|VC_BOTTOM,
  VC_RIGHT|VC_TOP|VC_BOTTOM,
  VC_LEFT|VC_RIGHT,
  VC_LEFT|VC_RIGHT|VC_TOP|VC_BOTTOM,
  VC_RIGHT|VC_TOP|VC_BOTTOM|VC_DBL_RIGHT,
  VC_RIGHT|VC_TOP|VC_BOTTOM|VC_DBL_TOP|VC_DBL_BOTTOM,
  VC_RIGHT|VC_TOP|VC_DBL_TOP|VC_DBL_RIGHT,
  VC_RIGHT|VC_BOTTOM|VC_DBL_BOTTOM|VC_DBL_RIGHT,
  VC_LEFT|VC_RIGHT|VC_TOP|VC_DBL_LEFT|VC_DBL_RIGHT|VC_DBL_TOP,
  VC_LEFT|VC_RIGHT|VC_BOTTOM|VC_DBL_LEFT|VC_DBL_RIGHT|VC_DBL_BOTTOM,
  VC_RIGHT|VC_TOP|VC_BOTTOM|VC_DBL_RIGHT|VC_DBL_TOP|VC_DBL_BOTTOM,
  VC_LEFT|VC_RIGHT|VC_DBL_LEFT|VC_DBL_RIGHT,
  VC_LEFT|VC_RIGHT|VC_TOP|VC_BOTTOM|VC_DBL_LEFT|VC_DBL_RIGHT|VC_DBL_TOP|VC_DBL_BOTTOM,
  VC_LEFT|VC_RIGHT|VC_TOP|VC_DBL_LEFT|VC_DBL_RIGHT,
  //
  VC_LEFT|VC_RIGHT|VC_TOP|VC_DBL_TOP,
  VC_LEFT|VC_RIGHT|VC_BOTTOM|VC_DBL_LEFT|VC_DBL_RIGHT,
  VC_LEFT|VC_RIGHT|VC_BOTTOM|VC_DBL_BOTTOM,
  VC_RIGHT|VC_TOP|VC_DBL_TOP,
  VC_RIGHT|VC_TOP|VC_DBL_RIGHT,
  VC_RIGHT|VC_BOTTOM|VC_DBL_RIGHT,
  VC_RIGHT|VC_BOTTOM|VC_DBL_BOTTOM,
  VC_LEFT|VC_RIGHT|VC_TOP|VC_BOTTOM|VC_DBL_TOP|VC_DBL_BOTTOM,
  VC_LEFT|VC_RIGHT|VC_TOP|VC_BOTTOM|VC_DBL_LEFT|VC_DBL_RIGHT,
  VC_LEFT|VC_TOP,
  VC_RIGHT|VC_BOTTOM,
  0, 0, 0, 0, 0,
};


//==========================================================================
//
//  vt_classify
//
//==========================================================================
static inline uint8_t vt_classify (uint8_t ch, const unsigned flags) {
  return (ch < 0xb0 || ch >= 0xe0 || (flags&VID_FRAME_OVERWRITE) ? 0 : vt_char_class[ch-0xb0]);
}


//==========================================================================
//
//  vt_mk_double
//
//==========================================================================
static inline uint8_t vt_mk_double (uint8_t class0, const unsigned flags) {
  return (flags&VID_FRAME_DOUBLE ? (class0|(0xf0&(class0<<4))) : (class0&0x0f));
}


//==========================================================================
//
//  vt_merge
//
//==========================================================================
static inline uint8_t vt_merge_classes (uint8_t class0, uint8_t class1) {
  class0 |= class1;
  class0 &= (class0<<4)|0x0f; // fix frames
  // fix partial doubles
  if ((class0&0x03) == 0x03 && (class0&0x30)) class0 |= 0x30;
  if ((class0&0x0c) == 0x0c && (class0&0xc0)) class0 |= 0xc0;
  return class0;
}


//==========================================================================
//
//  vt_find_char
//
//==========================================================================
static inline uint8_t vt_find_char (uint8_t cls) {
  if (!cls) return 0;
  for (uint8_t f = 0; f < 48; ++f) if (vt_char_class[f] == cls) return f+0xb0;
  return 0;
}


//==========================================================================
//
//  vt_draw_frame_ex
//
//==========================================================================
void vt_draw_frame_ex (int x, int y, int w, int h, uint8_t attr, unsigned flags,
                       const char *title)
{
  if (w < 1 || h < 1) return;

  uint8_t c0, c1;

  if (w == 1 && h == 1) {
    c0 = vt_classify(vt_getch(x, y), flags);
    c1 = vt_mk_double(VC_LEFT|VC_RIGHT|VC_TOP|VC_BOTTOM, flags);
    vt_writechar(x, y, vt_find_char(vt_merge_classes(c0, c1)), attr);
    return;
  }

  if (w == 1) {
    // vertical line
    uint8_t cc = VC_BOTTOM;
    while (h--) {
      if (h == 0) cc = VC_TOP;
      c0 = vt_classify(vt_getch(x, y), flags);
      if (!c0) cc = VC_TOP|VC_BOTTOM;
      c1 = vt_mk_double(cc, flags);
      vt_writechar(x, y, vt_find_char(vt_merge_classes(c0, c1)), attr);
      ++y;
      cc = VC_TOP|VC_BOTTOM;
    }
    return;
  }

  const int xorig = x;
  const int worig = w;
  int tlen = (w > 2 && title ? (int)strlen(title) : 0);
  if (w > 2 && tlen && tlen > w-2) tlen = w-2;

  if (h == 1) {
    // horizontal line
    uint8_t cc = VC_RIGHT;
    while (w--) {
      if (w == 0) cc = VC_LEFT;
      c0 = vt_classify(vt_getch(x, y), flags);
      if (!c0) cc = VC_LEFT|VC_RIGHT;
      c1 = vt_mk_double(cc, flags);
      vt_writechar(x, y, vt_find_char(vt_merge_classes(c0, c1)), attr);
      ++x;
      cc = VC_LEFT|VC_RIGHT;
    }
    if (tlen) vt_writestrcnt(xorig+(worig-tlen)/2, y, title, (size_t)tlen, attr);
    return;
  }

  // full-featured frame

  // top left corner
  c0 = vt_classify(vt_getch(x, y), flags);
  c1 = vt_mk_double(VC_RIGHT|VC_BOTTOM, flags);
  vt_writechar(x, y, vt_find_char(vt_merge_classes(c0, c1)), attr);

  // top right corner
  c0 = vt_classify(vt_getch(x+w-1, y), flags);
  c1 = vt_mk_double(VC_LEFT|VC_BOTTOM, flags);
  vt_writechar(x+w-1, y, vt_find_char(vt_merge_classes(c0, c1)), attr);

  // bottom left corner
  c0 = vt_classify(vt_getch(x, y+h-1), flags);
  c1 = vt_mk_double(VC_RIGHT|VC_TOP, flags);
  vt_writechar(x, y+h-1, vt_find_char(vt_merge_classes(c0, c1)), attr);

  // bottom right corner
  c0 = vt_classify(vt_getch(x+w-1, y+h-1), flags);
  c1 = vt_mk_double(VC_LEFT|VC_TOP, flags);
  vt_writechar(x+w-1, y+h-1, vt_find_char(vt_merge_classes(c0, c1)), attr);

  // vertical lines
  for (int sy = y+1; sy < y+h-1; ++sy) {
    // left
    c0 = vt_classify(vt_getch(x, sy), flags);
    c1 = vt_mk_double(VC_TOP|VC_BOTTOM, flags);
    vt_writechar(x, sy, vt_find_char(vt_merge_classes(c0, c1)), attr);
    // right
    c0 = vt_classify(vt_getch(x+w-1, sy), flags);
    c1 = vt_mk_double(VC_TOP|VC_BOTTOM, flags);
    vt_writechar(x+w-1, sy, vt_find_char(vt_merge_classes(c0, c1)), attr);
  }

  // horizontal lines
  for (int sx = x+1; sx < x+w-1; ++sx) {
    // top
    c0 = vt_classify(vt_getch(sx, y), flags);
    c1 = vt_mk_double(VC_LEFT|VC_RIGHT, flags);
    vt_writechar(sx, y, vt_find_char(vt_merge_classes(c0, c1)), attr);
    // bottom
    c0 = vt_classify(vt_getch(sx, y+h-1), flags);
    c1 = vt_mk_double(VC_LEFT|VC_RIGHT, flags);
    vt_writechar(sx, y+h-1, vt_find_char(vt_merge_classes(c0, c1)), attr);
  }

  if (flags&VID_FRAME_FILL) {
    for (int sy = y+1; sy < y+h-1; ++sy) {
      vt_writechars(x+1, sy, w-2, ' ', attr);
    }
  }

  if (flags&VID_FRAME_SHADOW) {
    vt_draw_shadow(x+w, y+1, 2, h-1);
    vt_draw_shadow(x+2, y+h, w, 1);
  }

  if (tlen) vt_writestrcnt(xorig+(worig-tlen)/2, y, title, (size_t)tlen, attr);
}


//==========================================================================
//
//  vt_draw_frame
//
//==========================================================================
void vt_draw_frame (int x, int y, int w, int h, uint8_t attr, unsigned flags) {
  vt_draw_frame_ex(x, y, w, h, attr, flags, NULL);
}


//==========================================================================
//
//  vt_fill_rect
//
//==========================================================================
void vt_fill_rect (int x, int y, int w, int h, uint8_t attr) {
  if (w < 1 || h < 1) return;
  if (x >= VID_TEXT_WIDTH || y >= VID_TEXT_HEIGHT) return;
  while (h--) {
    vt_writechars(x, y++, w, ' ', attr);
  }
}


//==========================================================================
//
//  vt_smm_init_ex
//
//==========================================================================
static void vt_smm_init_ex (vt_simple_menu *menu, const char *prompt, const char *items[],
                            int astextview)
{
  if (!menu) return;

  memset(menu, 0, sizeof(*menu));
  menu->prompt = (prompt ? strdup(prompt) : NULL);
  //menu->items = items;

  if (items) {
    size_t maxw = 0;
    for (menu->item_count = 0; items[menu->item_count]; ++menu->item_count) {
      const char *ss = items[menu->item_count];
      if (astextview && ss[0] == 1) ++ss;
      const size_t w = strlen(ss);
      if (maxw < w) maxw = w;
    }
    if (maxw > VID_TEXT_WIDTH-4) maxw = VID_TEXT_WIDTH-4;
    menu->w = (int)maxw;
    if (menu->item_count) {
      menu->items = calloc(1, sizeof(menu->items[0])*menu->item_count);
      for (uint32_t f = 0; f < menu->item_count; ++f) {
        menu->items[f] = strdup(items[f]);
      }
    }
  }

  if (prompt && prompt[0]) {
    int pw = (int)strlen(prompt);
    if (menu->w < pw) menu->w = pw;
  }

  // set height
  menu->h = (int)menu->item_count+2+(prompt ? 2 : 0);
  if (menu->h > VID_TEXT_HEIGHT) menu->h = VID_TEXT_HEIGHT;
  // set width
  menu->w += 4;

  // coords
  menu->x = (VID_TEXT_WIDTH-menu->w)/2;
  menu->y = (VID_TEXT_HEIGHT-menu->h)/2;

  // set default colors
  menu->clr_frame = menu->clr_win = 0x70;
  menu->clr_prompt = 0x71;
  menu->clr_item = 0x70;
  menu->clr_cursor = (astextview ? 0x73 : 0x1f);
  menu->textview = astextview;
}


//==========================================================================
//
//  vt_smm_init
//
//==========================================================================
void vt_smm_init (vt_simple_menu *menu, const char *prompt, const char *items[]) {
  vt_smm_init_ex(menu, prompt, items, 0);
}


//==========================================================================
//
//  vt_smm_init_textview
//
//==========================================================================
void vt_smm_init_textview (vt_simple_menu *menu, const char *prompt, const char *items[]) {
  vt_smm_init_ex(menu, prompt, items, 1);
}


//==========================================================================
//
//  vt_smm_deinit
//
//==========================================================================
void vt_smm_deinit (vt_simple_menu *menu) {
  if (!menu) return;
  if (menu->prompt) free(menu->prompt);
  if (menu->items) {
    for (uint32_t f = 0; f < menu->item_count; ++f) {
      if (menu->items[f]) free(menu->items[f]);
    }
    free(menu->items);
  }
  memset(menu, 0, sizeof(*menu));
}


//==========================================================================
//
//  vt_smm_ensure_cursor_visible
//
//==========================================================================
void vt_smm_ensure_cursor_visible (vt_simple_menu *menu) {
  if (!menu) return;

  if (menu->item_count == 0) {
    menu->cursor_y = menu->page_top = 0;
    return;
  }

  if (menu->cursor_y >= menu->item_count) {
    menu->cursor_y = menu->item_count-1;
  }

  if (menu->cursor_y < menu->page_top) {
    menu->page_top = menu->cursor_y;
  } else {
    const uint32_t hgt = (uint32_t)(menu->h-2-(menu->prompt ? 2 : 0));
    if (hgt > 0xfff) { menu->page_top = menu->cursor_y; return; } // just in case
    if (menu->page_top+hgt-1 < menu->cursor_y) {
      if (menu->cursor_y < hgt) {
        menu->page_top = 0;
      } else {
        menu->page_top = menu->cursor_y-hgt+1;
      }
    }
  }
}


//==========================================================================
//
//  vt_smm_draw
//
//==========================================================================
void vt_smm_draw (vt_simple_menu *menu) {
  if (!menu || menu->w < 2 || menu->h < 2) return;

  vt_draw_shadow(menu->x+menu->w, menu->y+1, 2, menu->h);
  vt_draw_shadow(menu->x+2, menu->y+menu->h, menu->w, 1);
  for (int dy = 0; dy < menu->h; ++dy) {
    vt_writechars(menu->x, menu->y+dy, menu->w, ' ', menu->clr_win);
  }
  vt_draw_frame(menu->x, menu->y, menu->w, menu->h, menu->clr_frame,
                VID_FRAME_DOUBLE|VID_FRAME_OVERWRITE);

  int yofs = 1;
  uint32_t hgt = (uint32_t)(menu->h-2);

  if (menu->prompt) {
    yofs += 2;
    hgt -= 2;
    vt_draw_frame(menu->x, menu->y+2, menu->w, 1, menu->clr_frame, VID_FRAME_SINGLE);
    vt_writestrz(menu->x+(menu->w-(int)strlen(menu->prompt))/2, menu->y+1, menu->prompt, menu->clr_prompt);
  }

  vt_smm_ensure_cursor_visible(menu);
  if (menu->item_count == 0) return;

  int xofs;
  if (!menu->textview) {
    vt_draw_frame(menu->x+2, menu->y+yofs, 1, (int)hgt, menu->clr_frame, VID_FRAME_SINGLE);
    xofs = 3;
  } else {
    xofs = 2;
  }

  for (uint32_t dy = 0; dy < hgt; ++dy) {
    const uint32_t idx = menu->page_top+dy;
    if (idx >= menu->item_count) break;
    const char *ss = menu->items[idx];
    const int hdr = (menu->textview && ss[0] == 1);
    if (hdr) ++ss;
    vt_writestrz(menu->x+xofs, menu->y+yofs+(int)dy, ss, menu->clr_item);
    if (hdr || (!menu->textview && idx == menu->cursor_y)) {
      vt_writeattrs(menu->x+xofs, menu->y+yofs+(int)dy, menu->w-4, menu->clr_cursor);
    }
  }

  // draw up arrow
  if (menu->page_top) {
    if (menu->textview) {
      vt_writechar(menu->x, menu->y+yofs, '\x18', menu->clr_frame);
    } else {
      vt_writechar(menu->x+1, menu->y+yofs, '\x1E', menu->clr_frame);
    }
  }

  // draw down arrow
  if (menu->page_top+hgt < menu->item_count) {
    if (menu->textview) {
      vt_writechar(menu->x, menu->y+menu->h-2, '\x19', menu->clr_frame);
    } else {
      vt_writechar(menu->x+1, menu->y+menu->h-2, '\x1F', menu->clr_frame);
    }
  }
}


//==========================================================================
//
//  vt_smm_process_key
//
//  return VID_SMM_xxx
//
//==========================================================================
int vt_smm_process_key (vt_simple_menu *menu, SDL_KeyboardEvent *key) {
  if (!menu || !key) return VID_SMM_NOTMINE;

  vt_smm_ensure_cursor_visible(menu);

  const int down = (key->type == SDL_KEYDOWN);
  const uint32_t hgt = (uint32_t)(menu->h-2-(menu->prompt ? 2 : 0));

  switch (key->keysym.sym) {
    case SDLK_ESCAPE:
      return (down ? VID_SMM_ESCAPE : VID_SMM_EATEN);

    case SDLK_RETURN:
    case SDLK_SPACE:
      return (down ? VID_SMM_SELECTED : VID_SMM_EATEN);

    case SDLK_LEFT: case SDLK_KP4:
    case SDLK_UP: case SDLK_KP8:
      if (down) {
        if (menu->textview) menu->cursor_y = menu->page_top;
        if (menu->cursor_y) --menu->cursor_y;
        vt_smm_ensure_cursor_visible(menu);
      }
      return VID_SMM_EATEN;
    case SDLK_RIGHT: case SDLK_KP6:
    case SDLK_DOWN: case SDLK_KP2:
      if (down) {
        menu->cursor_y = (menu->textview ? menu->page_top+hgt : menu->cursor_y+1);
        vt_smm_ensure_cursor_visible(menu);
      }
      return VID_SMM_EATEN;

    case SDLK_PAGEUP: case SDLK_KP9:
      if (down && hgt > 1 && hgt < 0xffff) {
        // sorry
        if (menu->textview) menu->cursor_y = menu->page_top;
             if (menu->cursor_y != menu->page_top) menu->cursor_y = menu->page_top;
        else if (menu->page_top < hgt-1) menu->cursor_y = menu->page_top = 0;
        else menu->cursor_y = (menu->page_top-(hgt-1));
        vt_smm_ensure_cursor_visible(menu);
      }
      return VID_SMM_EATEN;
    case SDLK_PAGEDOWN: case SDLK_KP3:
      if (down && hgt > 1 && hgt < 0xffff && menu->item_count) {
        // sorry
        if (menu->textview) menu->cursor_y = menu->page_top+hgt-1;
             if (menu->cursor_y != menu->page_top+hgt-1) menu->cursor_y = menu->page_top+hgt-1;
        else menu->cursor_y = (menu->page_top += hgt-1);
        vt_smm_ensure_cursor_visible(menu);
      }
      return VID_SMM_EATEN;

    case SDLK_HOME: case SDLK_KP7:
      if (down) {
        menu->cursor_y = 0;
        vt_smm_ensure_cursor_visible(menu);
      }
      return VID_SMM_EATEN;

    case SDLK_END: case SDLK_KP1:
      if (down && menu->item_count) {
        menu->cursor_y = menu->item_count-1;
        vt_smm_ensure_cursor_visible(menu);
      }
      return VID_SMM_EATEN;

    default: break;
  }

  return VID_SMM_NOTMINE;
}
