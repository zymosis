/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/

void vDrawChar (VExSurface *sfc, int x, int y, char ch, Uint8 ink, Uint8 paper) {
  int ca = ((uint8_t)ch)*8;
  //
  for (int dy = 0; dy < 8; ++dy, ++y, x -= 6) {
    uint8_t b = font6x8[ca++];
    //
    for (int dx = 0; dx < 6; ++dx, ++x, b <<= 1) {
      vPutPixel(sfc, x, y, b&0x80?ink:paper);
    }
  }
}


void vDrawText (VExSurface *sfc, int x, int y, const char *str, Uint8 ink, Uint8 paper) {
  int sx = x;
  //
  for (; *str; ++str, x += 6) {
    switch (*str) {
      case '\n': x = sx-6; y += 8; break;
      default:
        vDrawChar(sfc, x, y, *str, ink, paper);
        break;
    }
  }
}


void vDrawOutlineText (VExSurface *sfc, int x, int y, const char *str, Uint8 ink, int oink) {
  for (int dy = -1; dy <= 1; ++dy) {
    for (int dx = -1; dx <= 1; ++dx) {
      if (dx || dy) vDrawText(sfc, x+dx, y+dy, str, oink, -1);
    }
  }
  vDrawText(sfc, x, y, str, ink, -1);
}


void vDrawHLine (VExSurface *sfc, int x, int y, int len, Uint8 clr) {
  for (; len > 0; --len, ++x) vPutPixel(sfc, x, y, clr);
}


void vDrawVLine (VExSurface *sfc, int x, int y, int len, Uint8 clr) {
  for (; len > 0; --len, ++y) vPutPixel(sfc, x, y, clr);
}


void vDrawBar (VExSurface *sfc, int x, int y, int width, int height, Uint8 clr) {
  for (int dy = 0; dy < height; ++dy, ++y) vDrawHLine(sfc, x, y, width, clr);
}


void vDrawRect (VExSurface *sfc, int x, int y, int width, int height, Uint8 clr) {
  if (width > 0 && height > 0) {
    vDrawHLine(sfc, x, y, width, clr);
    vDrawHLine(sfc, x, y+height-1, width, clr);
    vDrawVLine(sfc, x, y+1, height-2, clr);
    vDrawVLine(sfc, x+width-1, y+1, height-2, clr);
  }
}


void vDrawRectW (VExSurface *sfc, int x, int y, int width, int height, Uint8 clr) {
  if (width > 0 && height > 0) {
    vDrawHLine(sfc, x+1, y, width-2, clr);
    vDrawHLine(sfc, x+1, y+height-1, width-2, clr);
    vDrawVLine(sfc, x, y+1, height-2, clr);
    vDrawVLine(sfc, x+width-1, y+1, height-2, clr);
  }
}


void vDrawZXStripe (VExSurface *sfc, int x, int y, int dim) {
  static const Uint8 clrs[4] = {2+8, 6+8, 4+8, 5+8};
  //
  dim = (dim ? 8 : 0);
  for (int f = 0; f < 4; ++f, x += 8) {
    for (int y = 0; y < 8; ++y) vDrawHLine(sfc, x+(7-y), y, 8, clrs[f]-dim);
  }
}


void vDrawWindow (VExSurface *sfc, int x, int y, int width, int height, const char *title, int tink, int tpaper, int wpaper, int flags) {
  char tit[52];
  int w;
  //
  if (width < 1 || (title && height < 9) || (!title && height < 1)) return;
  strncpy(tit, (title ? title : ""), sizeof(tit));
  tit[sizeof(tit)-1] = '\0';
  w = width/6;
  if (strlen(tit) > w) tit[w] = '\0';
  // frame and title
  vDrawRectW(sfc, x, y, width, height, tpaper);
  vDrawBar(sfc, x+1, y+1, width-2, 7, tpaper);
  if (width >= 56 && (flags&ZXVIDWF_STRIPES)) vDrawZXStripe(sfc, x+width-44, y, (flags&ZXVIDWF_STRIPES_DIM));
  //
  if (title != NULL && title[0]) {
    VClipInfo ci;
    //
    vSaveClip(sfc, &ci);
    vRestrictClip(sfc, x+1, y, width-2, 8);
    vDrawText(sfc, x+1, y, tit, tink, 255);
    vRestoreClip(sfc, &ci);
  }
  // window
  vDrawBar(sfc, x+1, y+8, width-2, height-9, wpaper);
  //
  vPutPixel(sfc, x+1, y+height-2, tpaper);
  vPutPixel(sfc, x+width-2, y+height-2, tpaper);
}


////////////////////////////////////////////////////////////////////////////////
static void vDrawVArrow (VExSurface *sfc, const char *bmp, int x, int y, Uint8 ink, Uint8 paper) {
  for (int dy = 0; dy < 3; ++dy) {
    for (int dx = 0; dx < 5; ++dx) {
      vPutPixel(sfc, x+dx, y+dy, bmp[dy*5+dx]?ink:paper);
    }
  }
}


static void vDrawHArrow (VExSurface *sfc, const char *bmp, int x, int y, Uint8 ink, Uint8 paper) {
  for (int dy = 0; dy < 5; ++dy) {
    for (int dx = 0; dx < 3; ++dx) {
      vPutPixel(sfc, x+dx, y+dy, bmp[dy*3+dx]?ink:paper);
    }
  }
}


void vDrawUpArrow (VExSurface *sfc, int x, int y, Uint8 ink, Uint8 paper) {
  static const char bmp[3][5] = {
    {0,0,1,0,0},
    {0,1,1,1,0},
    {1,1,1,1,1},
  };
  vDrawVArrow(sfc, (const char *)bmp, x, y, ink, paper);
}


void vDrawDownArrow (VExSurface *sfc, int x, int y, Uint8 ink, Uint8 paper) {
  static const char bmp[3][5] = {
    {1,1,1,1,1},
    {0,1,1,1,0},
    {0,0,1,0,0},
  };
  vDrawVArrow(sfc, (const char *)bmp, x, y, ink, paper);
}


void vDrawLeftArrow (VExSurface *sfc, int x, int y, Uint8 ink, Uint8 paper) {
  static const char bmp[5][3] = {
    {0,0,1},
    {0,1,1},
    {1,1,1},
    {0,1,1},
    {0,0,1},
  };
  vDrawHArrow(sfc, (const char *)bmp, x, y, ink, paper);
}


void vDrawRightArrow (VExSurface *sfc, int x, int y, Uint8 ink, Uint8 paper) {
  static const char bmp[5][3] = {
    {1,0,0},
    {1,1,0},
    {1,1,1},
    {1,1,0},
    {1,0,0},
  };
  vDrawHArrow(sfc, (const char *)bmp, x, y, ink, paper);
}


////////////////////////////////////////////////////////////////////////////////
int vLineCountMaxLen (const char *str, int *maxlenp) {
  int maxlen = 0, lineCnt = 0;
  //
  if (str) {
    while (*str) {
      const char *np = strchr(str, '\n');
      //
      if (!np) np = str+strlen(str);
      if (np-str > maxlen) maxlen = np-str;
      str = np;
      if (*str) ++str;
      ++lineCnt;
    }
  }
  if (maxlenp) *maxlenp = maxlen;
  return lineCnt;
}


////////////////////////////////////////////////////////////////////////////////
void vDrawLine (VExSurface *sfc, int x0, int y0, int x1, int y1, Uint8 clr) {
  int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
  int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
  int err = dx+dy; /* error value e_xy */
  //
  for (;;) {
    int e2;
    //
    vPutPixel(sfc, x0, y0, clr);
    if (x0 == x1 && y0 == y1) break;
    e2 = 2*err;
    if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
    if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
  }
}


void vDrawCircle (VExSurface *sfc, int cx, int cy, int radius, Uint8 clr) {
  int error = -radius, x = radius, y = 0;
  //
  void plot4points (int cx, int cy, int x, int y) {
    vPutPixel(sfc, cx+x, cy+y, clr);
    if (x != 0) vPutPixel(sfc, cx-x, cy+y, clr);
    if (y != 0) vPutPixel(sfc, cx+x, cy-y, clr);
    vPutPixel(sfc, cx-x, cy-y, clr);
  }
  //
  void plot8points (int cx, int cy, int x, int y) {
    plot4points(cx, cy, x, y);
    plot4points(cx, cy, y, x);
  }
  //
  if (radius <= 0) return;
  //if (radius == 1) { vPutPixel(sfc, cx, cy, clr); return; }
  //
  while (x > y) {
    plot8points(cx, cy, x, y);
    error += y*2+1;
    ++y;
    if (error >= 0) { --x; error -= x*2; }
  }
  plot4points(cx, cy, x, y);
}


void vDrawEllipse (VExSurface *sfc, int x0, int y0, int x1, int y1, Uint8 clr) {
  int a = abs(x1-x0), b = abs(y1-y0), b1 = b&1; /* values of diameter */
  long dx = 4*(1-a)*b*b, dy = 4*(b1+1)*a*a; /* error increment */
  long err = dx+dy+b1*a*a; /* error of 1.step */
  //
  if (x0 > x1) { x0 = x1; x1 += a; } /* if called with swapped points... */
  if (y0 > y1) y0 = y1; /* ...exchange them */
  y0 += (b+1)/2; y1 = y0-b1;  /* starting pixel */
  a *= 8*a; b1 = 8*b*b;
  //
  do {
    int e2;
    //
    vPutPixel(sfc, x1, y0, clr); /*   I. Quadrant */
    vPutPixel(sfc, x0, y0, clr); /*  II. Quadrant */
    vPutPixel(sfc, x0, y1, clr); /* III. Quadrant */
    vPutPixel(sfc, x1, y1, clr); /*  IV. Quadrant */
    e2 = 2*err;
    if (e2 >= dx) { ++x0; --x1; err += dx += b1; } /* x step */
    if (e2 <= dy) { ++y0; --y1; err += dy += a; }  /* y step */
  } while (x0 <= x1);
  //
  while (y0-y1 < b) {
    /* too early stop of flat ellipses a=1 */
    vPutPixel(sfc, x0-1, ++y0, clr); /* -> complete tip of ellipse */
    vPutPixel(sfc, x0-1, --y1, clr);
  }
}


void vDrawFilledCircle (VExSurface *sfc, int cx, int cy, int radius, Uint8 clr) {
  int error = -radius, x = radius, y = 0;
  //
  void plothline (int cx, int cy, int x) {
    int x0 = cx-x;
    int x1 = cx+x;
    if (x0 > x1) { int t = x0; x0 = x1; x1 = t; }
    if (x0 == x1) {
      vPutPixel(sfc, x0, cy, clr);
    } else {
      vDrawHLine(sfc, x0, cy, x1-x0+1, clr);
    }
  }
  //
  void plothlines (int cx, int cy, int x, int y) {
    plothline(cx, cy+y, x);
    if (y != 0) plothline(cx, cy-y, x);
  }
  //
  if (radius <= 0) return;
  //if (radius == 1) { vPutPixel(sfc, cx, cy, clr); return; }
  //
  while (x > y) {
    plothlines(cx, cy, x, y);
    plothlines(cx, cy, y, x);
    error += y*2+1;
    ++y;
    if (error >= 0) { --x; error -= x*2; }
  }
  plothlines(cx, cy, x, y);
}


void vDrawFilledEllipse (VExSurface *sfc, int x0, int y0, int x1, int y1, Uint8 clr) {
  int a = abs(x1-x0), b = abs(y1-y0), b1 = b&1; /* values of diameter */
  long dx = 4*(1-a)*b*b, dy = 4*(b1+1)*a*a; /* error increment */
  long err = dx+dy+b1*a*a; /* error of 1.step */
  //
  if (x0 > x1) { x0 = x1; x1 += a; } /* if called with swapped points... */
  if (y0 > y1) y0 = y1; /* ...exchange them */
  y0 += (b+1)/2; y1 = y0-b1;  /* starting pixel */
  a *= 8*a; b1 = 8*b*b;
  //
  do {
    int e2;
    //
    vDrawHLine(sfc, x0, y0, x1-x0+1, clr);
    vDrawHLine(sfc, x0, y1, x1-x0+1, clr);
    //vPutPixel(sfc, x1, y0, 2); /*   I. Quadrant */
    //vPutPixel(sfc, x0, y0, 2); /*  II. Quadrant */
    //vPutPixel(sfc, x0, y1, clr); /* III. Quadrant */
    //vPutPixel(sfc, x1, y1, clr); /*  IV. Quadrant */
    e2 = 2*err;
    if (e2 >= dx) { ++x0; --x1; err += dx += b1; } /* x step */
    if (e2 <= dy) { ++y0; --y1; err += dy += a; }  /* y step */
  } while (x0 <= x1);
  //
  while (y0-y1 < b) {
    /* too early stop of flat ellipses a=1 */
    vPutPixel(sfc, x0-1, ++y0, clr); /* -> complete tip of ellipse */
    vPutPixel(sfc, x0-1, --y1, clr);
  }
}


////////////////////////////////////////////////////////////////////////////////
void vDrawSelectionRect (VExSurface *sfc, int phase, int x0, int y0, int wdt, int hgt, Uint8 col0, Uint8 col1) {
  if (wdt < 1 || hgt < 1) return;
  // top
  for (int f = x0; f < x0+wdt; ++f, ++phase) vPutPixel(sfc, f, y0, ((phase %= 4)<2)?col0:col1);
  // right
  for (int f = y0+1; f < y0+hgt; ++f, ++phase) vPutPixel(sfc, x0+wdt-1, f, ((phase %= 4)<2)?col0:col1);
  // bottom
  for (int f = x0+wdt-2; f >= x0; --f, ++phase) vPutPixel(sfc, f, y0+hgt-1, ((phase %= 4)<2)?col0:col1);
  // left
  for (int f = y0+hgt-2; f >= y0; --f, ++phase) vPutPixel(sfc, x0, f, ((phase %= 4)<2)?col0:col1);
}


////////////////////////////////////////////////////////////////////////////////
#include "vdcur.c"

Uint8 msCurClr0 = 0;
Uint8 msCurClr1 = 15;


void drawMouseCursor (int x, int y, int type) {
  if (type < 0) type = AID_COOKE; else if (type > AID_EMPTY) type = AID_COOKE;
  for (int dy = 0; dy < 16; ++dy, ++y) {
    for (int dx = 0; dx < 16; ++dx) {
      switch (winCur[type][dy][dx]) {
        case 1: putPixel(x+dx, y, msCurClr0); break;
        case 2: putPixel(x+dx, y, msCurClr1); break;
      }
    }
  }
}
