/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
uint8_t zxPalette[71*3] = {
    0,  0,  0,
    0,  0,192/*+32*/,
  192,  0,  0,
  192,  0,192,
    0,192,  0,
    0,192,192,
  192,192,  0,
  192,192,192,
    0,  0,  0,
    0,  0,255,
  255,  0,  0,
  255,  0,255,
    0,255,  0,
    0,255,255,
  255,255,  0,
  255,255,255,
  /*        */
   15, 11,  7, /* 16 */
   23, 15, 11,
   31, 23, 11,
   39, 27, 15,
   47, 35, 19, /* 20 */
   55, 43, 23,
   63, 47, 23,
   75, 55, 27,
   83, 59, 27,
   91, 67, 31, /* 25 */
   99, 75, 31,
   27, 27,  0,
   35, 35,  0,
   43, 43,  7,
   47, 47,  7, /* 30 */
   55, 55,  7,
   63, 63,  7,
   71, 71,  7,
   15,  0,  0,
   23,  0,  0, /* 35 */
   67, 55,  0,
   75, 59,  7,
   87, 67,  7,
   47, 23, 11,
   59, 31, 15, /* 40 */
   75, 35, 19,
   87, 43, 23,
   99, 47, 31,
  115, 55, 35,
  127, 59, 43, /* 45 */
  143, 67, 51,
  159, 79, 51,
  175, 99, 47,
   55, 43, 19,
   71, 51, 27, /* 50 */
   83, 55, 35,
   99, 63, 43,
  111, 71, 51,
  127, 83, 63,
  139, 95, 71, /* 55 */
  135,111, 95,
  123, 99, 83,
  107, 87, 71,
   95, 75, 59,
   83, 63, 51, /* 60 */
   67, 51, 39,
   55, 43, 31,
   39, 31, 23,
   27, 19, 15,
   15, 11,  7, /* 65 */
  //
  0x18,0x55,0x04, /* 66 */
  0x61,0xFF,0x24, /* 67 */
  0x38,0x85,0x14, /* 68 */
  0x18,0xa5,0xa4, /* 69 */
  0x7f,0x7f,0x7f, /* 70 */
};
