/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include "emuevents.h"
#include "emuvars.h"

//#define EV_DEBUG_FULL
//#define EV_DEBUG_ADD
//#define EV_DEBUG_TSKIP

typedef struct {
  char *name;
  uint32_t type;
  EmuEvHandlerTpCB cb;
} KnownEvent;

static KnownEvent *knownEvTypes = NULL;
static uint32_t knownEvAlloted = 0;
static uint32_t knownEvUsed = 0;

static EmuEvent *evList = NULL;
static uint32_t evListAlloted = 0;
static uint32_t evListUsed = 0;

static EmuEvHandlerCB *evHandlers = NULL;
static uint32_t evHandlersAlloted = 0;
static uint32_t evHandlersUsed = 0;


uint32_t emuEvLastFrameCallTS = 0;

static uint32_t emuInTimeSkip = 0;


//==========================================================================
//
//  emuEvRegisterHandler
//
//  register new event handler
//
//==========================================================================
void emuEvRegisterHandler (EmuEvHandlerCB cb) {
  if (!cb) return;
  for (uint32_t f = 0; f < evHandlersUsed; ++f) {
    if (evHandlers[f] == cb) return;
  }
  if (evHandlersUsed == evHandlersAlloted) {
    evHandlersAlloted += 128;
    evHandlers = realloc(evHandlers, sizeof(EmuEvHandlerCB)*evHandlersAlloted);
    if (!evHandlers) {
      fprintf(stderr, "FATAL: out of memory for event handlers!\n");
      __builtin_trap();
    }
  }
  evHandlers[evHandlersUsed++] = cb;
}



//==========================================================================
//
//  emuEvRegister
//
//  register new event; never returns 0
//
//==========================================================================
uint32_t emuEvRegister (EmuEvHandlerTpCB cb, const char *name) {
  if (!name || !name[0]) {
    name = "<anonymous>";
  } else {
    for (uint32_t f = 0; f < knownEvUsed; ++f) {
      if (strcmp(knownEvTypes[f].name, name) == 0) {
        fprintf(stderr, "FATAL: duplicate event name '%s'!\n", name);
        __builtin_trap();
      }
    }
  }

  if (knownEvUsed == knownEvAlloted) {
    knownEvAlloted += 1024;
    knownEvTypes = realloc(knownEvTypes, sizeof(KnownEvent)*knownEvAlloted);
    if (!knownEvTypes) {
      fprintf(stderr, "FATAL: out of memory for event types!\n");
      __builtin_trap();
    }
  }

  if (!(knownEvTypes[knownEvUsed].name = strdup(name))) {
    fprintf(stderr, "FATAL: out of memory for event types!\n");
    __builtin_trap();
  }

  knownEvTypes[knownEvUsed].type = knownEvUsed+1;
  knownEvTypes[knownEvUsed].cb = cb;

  return ++knownEvUsed;
}


//==========================================================================
//
//  emuEvNameByType
//
//  get event name by its type
//
//==========================================================================
const char *emuEvNameByType (uint32_t type) {
  if (type == 0) return "<null event>";
  if (type > knownEvUsed) return "<invalid event>";
  return knownEvTypes[type-1].name;
}


//==========================================================================
//
//  emuEvClear
//
//  clear all events
//
//==========================================================================
void emuEvClear (void) {
  evListUsed = 0;
}


//==========================================================================
//
//  emuTimePassed
//
//  pass time, call event handlers
//  it is ok to add new events from handlers
//
//==========================================================================
void emuTimePassed (uint32_t tskip) {
  if (!tskip) return;
  if (tskip >= 0x80000000U) __builtin_trap();

  if (!evListUsed) return;
  #if defined(EV_DEBUG_FULL)
  fprintf(stderr, "emuTimePassed: tskip=%u; tfirst=%u\n", tskip, evList[0].tstates);
  #endif

  #if defined(EV_DEBUG_TSKIP) || defined(EV_DEBUG_FULL)
  # if defined(EV_DEBUG_FULL)
    const char *dbgpfx = "  ";
  # else
    const char *dbgpfx = "****";
  # endif
  #endif

  // fix event times (because event handler may add a new one)
  for (uint32_t f = 0; f < evListUsed; ++f) {
    if (evList[f].tstates > tskip) {
      evList[f].tstates -= tskip;
    } else {
      evList[f].tstates = 0;
    }
  }

  // run triggered events
  emuInTimeSkip = 1;
  while (evListUsed && evList[0].tstates == 0) {
    EmuEvent ev = evList[0];
    --evListUsed;
    if (evListUsed) memmove(&evList[0], &evList[1], sizeof(EmuEvent)*evListUsed);
    #if defined(EV_DEBUG_TSKIP) || defined(EV_DEBUG_FULL)
    fprintf(stderr, "%semuTimePassed: tskip(left)=%u/%u; event=%s; evListUsed=%u\n", dbgpfx,
                    tskip, ev.tstates, emuEvNameByType(ev.type), evListUsed);
    #endif
    if (ev.type && ev.type <= knownEvUsed && knownEvTypes[ev.type-1].cb) {
      #if defined(EV_DEBUG_TSKIP) || defined(EV_DEBUG_FULL)
      fprintf(stderr, "    CB!\n");
      #endif
      knownEvTypes[ev.type-1].cb(ev.type, ev.udata);
    }
    for (uint32_t c = 0; c < evHandlersUsed; ++c) evHandlers[c](&ev);
  }
  emuInTimeSkip = 0;

  #if defined(EV_DEBUG_TSKIP) || defined(EV_DEBUG_FULL)
  fprintf(stderr, "%semuTimePassed: DONE! evListUsed=%u\n", dbgpfx, evListUsed);
  #endif
}


//==========================================================================
//
//  emuEvRemoveAllWithType
//
//  remove all events with the given type
//
//==========================================================================
void emuEvRemoveAllWithType (uint32_t type) {
  uint32_t ev = 0;
  while (ev < evListUsed) {
    if (evList[ev].type == type) {
      --evListUsed;
      if (ev != evListUsed) memcpy(&evList[ev], &evList[ev+1], evListUsed*sizeof(EmuEvent));
    } else {
      ++ev;
    }
  }
}


//==========================================================================
//
//  emuEvRemoveAllWithTypeUData
//
//  remove all events with the given type and userdata
//
//==========================================================================
void emuEvRemoveAllWithTypeUData (uint32_t type, void *udata) {
  uint32_t ev = 0;
  while (ev < evListUsed) {
    if (evList[ev].type == type && evList[ev].udata == udata) {
      --evListUsed;
      if (ev != evListUsed) memcpy(&evList[ev], &evList[ev+1], evListUsed*sizeof(EmuEvent));
    } else {
      ++ev;
    }
  }
}


//==========================================================================
//
//  emuEvAdd
//
//  append new event
//  tstates is tstates from the current event time
//
//==========================================================================
void emuEvAdd (uint32_t tstates, uint32_t type, void *udata) {
  #ifdef EV_DEBUG_ADD
  fprintf(stderr, "emuEvAdd:000: tstates=%u; type=%s; cnt=%u\n", tstates,
                  emuEvNameByType(type), evListUsed);
  #endif

  if (evListUsed == evListAlloted) {
    evListAlloted += 1024;
    evList = realloc(evList, evListAlloted*sizeof(EmuEvent));
    if (!evList) {
      fprintf(stderr, "FATAL: out of memory for emulator event queue!\n");
      __builtin_trap();
    }
  }

  if (tstates >= 0x8f000000) __builtin_trap();
  if (!tstates && emuInTimeSkip) tstates = 1;

  // find where we should put our event
  uint32_t evidx = 0;
  while (evidx < evListUsed && evList[evidx].tstates <= tstates) ++evidx;
  #ifdef EV_DEBUG_ADD
  fprintf(stderr, "emuEvAdd:001:   evidx=%u; evListUsed=%u\n", evidx, evListUsed);
  #endif
  // insert *at* evidx
  if (evidx != evListUsed) {
    // make room
    memmove(&evList[evidx+1], &evList[evidx], (evListUsed-evidx)*sizeof(EmuEvent));
  }
  ++evListUsed;

  evList[evidx].tstates = tstates;
  evList[evidx].type = type;
  evList[evidx].udata = udata;

  // stop z80 emulator, so main code will recheck events
  z80.next_event_tstate = 1;

  #ifdef EV_DEBUG_ADD
  fprintf(stderr, "=== emuEvAdd: %u events ===\n", evListUsed);
  for (uint32_t f = 0; f < evListUsed; ++f) {
    fprintf(stderr, "  #%6u: ts=%8u; event=%s\n", f, evList[f].tstates, emuEvNameByType(evList[f].type));
  }
  #endif
}


//==========================================================================
//
//  emuEvNextTime
//
//  peek next event time; returns `EMU_EV_NONE` if there are no events
//
//==========================================================================
uint32_t emuEvNextTime (void) {
  return (evListUsed ? evList[0].tstates : EMU_EV_NONE);
}


//==========================================================================
//
//  emuEvGet
//
//  get next event
//  returns zero if there are no more event
//
//==========================================================================
int emuEvGet (EmuEvent *ev) {
  if (evListUsed == 0) {
    if (ev) memset(ev, 0, sizeof(EmuEvent));
    return 0;
  }

  if (ev) memcpy(ev, &evList[0], sizeof(EmuEvent));

  --evListUsed;
  if (evListUsed != 0) memcpy(&evList[0], &evList[1], sizeof(EmuEvent)*evListUsed);

  return 1;
}
