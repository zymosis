/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include "libvideo/video.h"
#include "zxscrdraw.h"
#include "emucommon.h"
#include "emuvars.h"


static inline void getBeamXY (int tstates, int *x, int *y, int *ofs) {
  tstates += zxLateTimings;
  if (tstates < zxLineStartTS[0]) { *x = *y = -1; return; }
  *y = (tstates-zxLineStartTS[0])/machineInfo.tsperline;
  if (*y >= 0 && *y <= 192+24*2) {
    *x = (tstates-zxLineStartTS[*y])/4;
    if (ofs) *ofs = (tstates-zxLineStartTS[*y])%4;
  } else {
    *x = 0;
    if (ofs) *ofs = 0;
  }
}


static inline int beamXY2Addr (int x, int y) {
  return (x >= 0 && y >= 0 && x <= 42 && y <= 192+24*2 ? (48-3*8+y)*352+((48-4*8)+x*8) : -1);
}


static inline int isBeamInScreenDollar (int x, int y) {
  return (y >= 24 && y < 24+192 && x >= 4 && x < 4+32);
}


// returns current beam position, 4ts offset, and "in screen$" flag
// result is "in screen$"
int zxScrGetBeamInfo (int tstates, int *x, int *y, int *ofs) {
  getBeamXY(tstates, x, y, ofs);
  return isBeamInScreenDollar(*x, *y);
}


static inline void zxUpdateScreenAtBeamXY (int x, int y, int ofs) {
  if (y >= 0 && y < 192+24*2 && x >= 0 && x <= 41) {
    int as = beamXY2Addr(x, y); /*, ae = beamXY2Addr(x+1, y);*/
    //
    //K8ASSERT(as >= 0);
    //K8ASSERT(ae >= 0);
    //K8ASSERT(as < ae);
    //K8ASSERT(ae-as == 8);
    //
    if (isBeamInScreenDollar(x, y)) {
      // screen
      int sx = x-4, sy = y-24; // screen position
      uint16_t addr = zxScrLineOfs[sy]+sx, zus = zxUlaSnow[sy][sx];
      uint8_t bt = (zxWasUlaSnow+zus > 1 ? zus-1 : zxScreenBank[addr]);
      uint8_t attr = zxScreenBank[32*192+sy/8*32+sx];
      uint8_t paper = (attr>>3)&0x0f, ink = (attr&0x07)|(paper&0x08);
      int asStart = as;
      //if (zxWasUlaSnow+zus > 1) bt = zus-1;
      if (attr&0x80 && zxFlashState) { const uint8_t t = paper; paper = ink; ink = t; }
      //if (vidColorMode == 2) { ink |= 8; paper |= 8; }
      for (unsigned f = 0; f < 8; ++f) {
        zxScreen[zxScreenCurrent][as++] = (bt&0x80 ? ink : paper);
        bt = (bt&0x7f)<<1;
      }
      // copy this to alternate screen for slow speeds
      if (optSpeed < 100) {
        while (asStart < as) {
          zxScreen[zxScreenCurrent^1][asStart] = zxScreen[zxScreenCurrent][asStart];
          ++asStart;
        }
      }
    } else {
      // border
      if (machineInfo.genuine) {
        memset(zxScreen[zxScreenCurrent]+as, zxBorder, 8);
        // copy this to alternate screen for slow speeds
        if (optSpeed < 100) memset(zxScreen[zxScreenCurrent^1]+as, zxBorder, 8);
      } else {
        memset(zxScreen[zxScreenCurrent]+as+ofs*2, zxBorder, 2);
        // copy this to alternate screen for slow speeds
        if (optSpeed < 100) memset(zxScreen[zxScreenCurrent^1]+as+ofs*2, zxBorder, 2);
      }
    }
  }
}


// draw screen from `zxOldScreenTS` to `curts-1`
void zxRealiseScreen (int curts) {
  if (optNoScreenReal == 0) {
    int curx, cury, ex, ey;
    //
    if (curts < zxOldScreenTS || curts < zxLineStartTS[0]+zxLateTimings ||
        zxOldScreenTS >= zxLineStartTS[192+24*2]+zxLateTimings) return; // do nothing
    if (curts >= zxLineStartTS[192+24*2]+zxLateTimings) curts = zxLineStartTS[192+24*2]+zxLateTimings;
    //
    if (zxOldScreenTS < zxLineStartTS[0]+zxLateTimings) zxOldScreenTS = zxLineStartTS[0]+zxLateTimings;
    //
    if (machineInfo.genuine) {
      getBeamXY(zxOldScreenTS, &curx, &cury, NULL);
      getBeamXY(curts, &ex, &ey, NULL);
      // ULA reads 4 bytes per update (2 bytes of bitmap and 2 bytes of attrs),
      // so let's emulate that
      if (machineInfo.genuine && isBeamInScreenDollar(ex, ey) && !(ex&0x01)) {
        // i like to move it, move it
        // ex it NOT included, hence the '!' above
        ++ex;
      }
      // now update screen from (curx, cury) to (ex, ey); note that (ex, ey) is not included in updated region
      K8ASSERT(cury <= ey);
      // full lines
      while (cury < ey) {
        for (; curx < 42; ++curx) zxUpdateScreenAtBeamXY(curx, cury, 0);
        ++cury;
        curx = 0;
      }
      //
      for (; curx < ex; ++curx) zxUpdateScreenAtBeamXY(curx, cury, 0);
      //
      zxOldScreenTS = zxLineStartTS[ey]+ex*4+zxLateTimings; // will start from here
      if (zxOldScreenTS >= zxLineStartTS[192+24*2]+zxLateTimings) zxOldScreenTS = machineInfo.tsperframe;
    } else {
      while (zxOldScreenTS <= curts+1) {
        int ofs = 0;
        //
        getBeamXY(zxOldScreenTS, &curx, &cury, &ofs);
        zxUpdateScreenAtBeamXY(curx, cury, ofs);
        if (ofs == 0 && isBeamInScreenDollar(curx, cury)) {
          zxOldScreenTS += 4; // it's ok to skip the screen in this way
        } else {
          ++zxOldScreenTS;
        }
      }
      if (zxOldScreenTS >= zxLineStartTS[192+24*2]+zxLateTimings) zxOldScreenTS = machineInfo.tsperframe;
    }
  }
}


// "shaded" means "not yet touched by the beam"
int zxScreenLineShadeStart (int ts) {
  if (ts < zxLineStartTS[0]+zxLateTimings) {
    // before screen
    return 0;
  } else if (ts >= zxLineStartTS[192+24*2]+zxLateTimings) {
    // after screen
    return 400*352;
  } else {
    int x, y, ofs;
    getBeamXY(ts, &x, &y, &ofs);
    if (x > 42) x = 42;
    int res = beamXY2Addr(x, y);
    return (res >= 0 ? res : 400*352);
  }
}
