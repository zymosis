/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_TAPES_H
#define ZXEMUT_TAPES_H

#include <stdint.h>
#include <SDL.h>
#include <libspectrum.h>

#include "emucommon.h"
#include "emuvars.h"


extern void emuTapeDrawCounter (void);
extern void emuStopTape (void);
extern void emuTapeAdvanceFrame (void);
extern void emuTapeEdgeAdvance (int curts);
extern int emuTapeCheckStop (void); // !0: tape stopped
extern int emuTapeGetCurEdge (void); // -1: tape is not playing
extern void emuStartTapeEx (int autostart);
extern void emuRewindTape (void);
extern void emuUnloadTape (void);
extern void emuClearTape (void);
extern void emutapCalcLength (void);
extern int emuInsertTapeFromBuf (const char *name, const void *buf, int size);
extern void emuTapeAutoStop (void);

static inline void emuStartTape (void) { emuStartTapeEx(0); }
static inline void emuStartTapeAuto (void) { emuStartTapeEx(1); }


extern void emuTapeAcceleratorEdgeAdvance (int flags);
extern void emuTapeAcceleratorStop (void);
extern void emuTapeAcceleratorStart (void);
extern void emuTapeAcceleratorAdvanceFrame (void);
extern void emuTapeLoaderDetector (void);

// call this in mem_read(OPCODE)
extern int emuTapeROMCheckAndAccel (void);


// sets tapNextEdgeTS and tapCurEdge
// returns -1 if tape stopped or tapNextEdgeTS on success
extern int emuTapeLdrGetNextPulse (void);

// -1: tape not running
// -2: tape loading error (tape MAY BE stopped)
extern int emuTapeLoadBit (int bit0ts, int bit1ts);

// -1: tape not running
// -2: tape loading error (tape MAY BE stopped)
extern int emuTapeLoadByte (int bit0ts, int bit1ts);


// custom flashloaders
enum {
  CFL_NO_TYPE       = 0x0001, // no type byte, assumes CFL_FLAG_ANY_TYPE
  CFL_NO_PARITY     = 0x0002, // no parity byte
  CFL_ANY_TYPE      = 0x0004, // don't check block type in A
  CFL_REVERSE       = 0x0008, // loading in reverse direction, first decrement IX
  CFL_NO_LEN_GLITCH = 0x0010, // no 'loader len' glitch
  CFL_PARITY_FIRST  = 0x0020, // parity byte comes before block
};


typedef struct {
  int pilot_minlen; // 807
  int pilot; // 2168
  int sync1; // 667
  int sync2; // 735
  int bit0; // 855
  int bit1; // 1710
  int init_parity; // >0: init parity with this
  uint32_t flags; // CFL_xxx
} tape_loader_info_t;


extern const tape_loader_info_t tape_loader_info_rom;

// return:
//   -1: pilot or block id searching fails (so just continue executing)
//    0: something was loaded (with or without error); execute loader epilogue
extern int emuTapeDoFlashLoadEx (const tape_loader_info_t *ti);


// -1: tape not running
// -2: tape loading error (tape MAY BE stopped)
extern int emuTapeROMLoadByte (void);

// return:
//   -1: pilot or block id searching fails (so just continue executing)
//    0: something was loaded (with or without error); execute loader epilogue
extern int emuTapeDoROMLoad (void);


// return:
//   -1: pilot or block id searching fails
//    0: something was loaded (with or without error)
// anyway, just continue execution
// should be called when z80.pc == 0x0564
extern int emuTapeAcceletateROMLoader (void);

extern int emuTapeFlashLoad (void);


#endif
