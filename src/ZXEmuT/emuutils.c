/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include "emuutils.h"
#include "emuevents.h"
#include "zxscrdraw.h"
#include "machines.h"
#include "console.h"
#include "snd_alsa.h"
#include "emuexec.h"
#include "../libfusefdc/libfusefdc.h"


// ////////////////////////////////////////////////////////////////////////// //
static fusefdd_event_iface emu_fdd_event_interface = {
  .remove_all = &emuEvRemoveAllWithType,
  .remove_all_udata = &emuEvRemoveAllWithTypeUData,
  .add_new = &emuEvAdd,
  .register_new = &emuEvRegister,
};


static int firstTimeReset = 1;


////////////////////////////////////////////////////////////////////////////////
inline void zxPageROM (int newpg) {
  if (newpg >= 0 && newpg < zxMaxROMMemoryBank) {
    zxLastPagedRAM0 = -1;
    zxLastPagedROM = newpg;
    //fprintf(stderr, "zxPageROM: newpg=%d (%d); PC=#%04X\n", newpg, zxTRDOSPagedIn, z80.org_pc);
    if (!zxTRDOSPagedIn) {
      zxMemoryBankNum[0] = newpg;
      zxMemory[0] = zxROMBanks[newpg];
      // Mr.Gluck Reset Service is paged in instead of 128K ROM until the first RAM opcode read
      if (newpg == 0 && zxModel == ZX_MACHINE_PENTAGON && !zxTRDOSPagedIn) {
        if (optUseGluck && zxGluckROM >= 0 && zxWasRAMExec == 0) {
          zxMemory[0] = zxROMBanks[zxGluckROM];
        } else if (optUseQCmdr && zxQCmdrROM >= 0) {
          zxMemory[0] = zxROMBanks[zxQCmdrROM];
        }
      }
    }
  }
}


inline void zxPageRAM (int pageno, int newpg) {
  if (pageno >= 0 && pageno < 4 && newpg >= 0 && newpg < zxMaxMemoryBank) {
    //if (pageno == 1) zxRealiseScreen(z80.tstates);
    //fprintf(stderr, "zxPageRAM: bank=%d; newpg=%d\n", pageno, newpg);
    if (pageno == 0) {
      zxLastPagedRAM0 = newpg;
      if (zxTRDOSPagedIn) return;
    }
    zxMemoryBankNum[pageno] = newpg;
    zxMemory[pageno] = zxMemoryBanks[newpg];
  }
}


inline void zxSetScreen (int newpg) {
  if (newpg >= 0 && newpg < zxMaxMemoryBank) {
    emuCurrScreenBank = newpg; // for noflic detector
    if (zxScreenBank != zxMemoryBanks[newpg]) {
      int32_t ts = z80.tstates;
      // this is ALMOST right, but not quite
      #if 0
      // we need to wait ULA to finish reading (see Songs In Lines 4)
      // sadly, our timings sux, so this doesn't work
      // if i stabilize SIL4 stripes, then its scroller is broken
      if (machineInfo.contention && ts < machineInfo.tsperframe) {
        // this is non-ULA port
        int contended = !machineInfo.usePlus3Contention;
        if (contended) {
          ts += zxContentionTable[1][ts]; ++ts;
          ts += zxContentionTable[1][ts]; ++ts;
          ts += zxContentionTable[1][ts];
          //int x, y, ofs;
          //if (zxScrGetBeamInfo(ts, &x, &y, &ofs)) ts += (4-ofs)&3;
        } else {
          ts += 2;
        }
      }
      #endif
      zxRealiseScreen(ts);
      zxScreenBank = zxMemoryBanks[newpg];
    }
  }
}


inline void zxTRDOSpagein (void) {
  if (optTRDOSenabled && optTRDOSROMIndex >= 0 && !zxTRDOSPagedIn) {
    //fprintf(stderr, "TRDOS: IN\n");
    zxTRDOSactivated = 1;
    zxMemoryBankNum[0] = optTRDOSROMIndex;
    zxMemory[0] = zxROMBanks[optTRDOSROMIndex];
    zxTRDOSPagedIn = 1;
  }
}


inline void zxTRDOSpageout (void) {
  if (optTRDOSenabled && optTRDOSROMIndex >= 0 && zxTRDOSPagedIn) {
    //fprintf(stderr, "TRDOS: OUT\n");
    if (zxLastPagedRAM0 >= 0) {
      zxMemoryBankNum[0] = zxLastPagedRAM0;
      zxMemory[0] = zxMemoryBanks[zxLastPagedRAM0];
    } else {
      zxMemoryBankNum[0] = zxLastPagedROM;
      zxMemory[0] = zxROMBanks[zxLastPagedROM];
    }
    zxTRDOSPagedIn = 0;
  }
}


////////////////////////////////////////////////////////////////////////////////
void emuClearRAM (void) {
  for (int f = 0; f < zxMaxMemoryBank; ++f) {
    zxMemBanksDirty[f] = 0;
    memset(zxMemoryBanks[f], 0, 0x4000);
  }
}


void emuResetAY (void) {
  soundAYReset();
  memset(zxAYRegs, 0, sizeof(zxAYRegs));
  zxAYRegs[7] = 0xff;
  zxLastOutFFFD = 0;
}


//==========================================================================
//
//  emuResetKeyboard
//
//  "unpress" all ZX keys
//
//==========================================================================
void emuResetKeyboard (void) {
  for (int f = 0; f < 9; ++f) zxKeyboardState[f] = 0xff;
}


// bit0: to tr-dos
void emuReset (int flags) {
  emu_fdd_event_interface.cpu_speed_hz = machineInfo.cpuSpeed;
  emuUPD765Reset();
  beta_reset(); /* hard reset */
  emuEvClear();
  zym_reset(&z80);
  zxOldScreenTS = 0;
  memset(zxUlaSnow, 0, sizeof(zxUlaSnow));
  zxWasUlaSnow = 0;
  zxLastOut7ffd = (machineInfo.port7ffd >= 0 ? machineInfo.port7ffd : 0x20);
  zxLastOut1ffd = (machineInfo.port1ffd >= 0 ? machineInfo.port1ffd : 0x00);
  zx7ffdLocked = (machineInfo.port7ffd < 0);
  zxBorder = 7;
  zxUlaOut = 0xff;
  zxLastOutFFFD = 0;
  zxTRDOSPagedIn = 0;
  zxTRDOSactivated = 0;
  zxLastPagedRAM0 = -1;
  z80GenerateNMI = 0;
  z80_was_frame_end = 0;
  zxWasRAMExec = 0; // for Mr.Gluck Reset Service
  //
  emuBuildContentionTable();
  //
  // do not clear RAM on other resets
  if (firstTimeReset) {
    emuClearRAM();
  }
  zxLastPagedROM = zxMemoryBankNum[0] = 0; // ROM
  zxMemoryBankNum[1] = 5; // RAM
  zxMemoryBankNum[2] = 2; // RAM
  zxMemoryBankNum[3] = 0; // RAM
  zxMemory[0] = zxROMBanks[zxMemoryBankNum[0]];
  zxMemory[1] = zxMemoryBanks[zxMemoryBankNum[1]];
  zxMemory[2] = zxMemoryBanks[zxMemoryBankNum[2]];
  zxMemory[3] = zxMemoryBanks[zxMemoryBankNum[3]];
  zxScreenBank = zxMemory[1];
  // do not clear keyboard state on other resets
  if (firstTimeReset) {
    emuResetKeyboard();
  }
  //
  emuResetAY();
  //
  emuRealizeMemoryPaging();
  //
  if (optTRDOSenabled && optTRDOSROMIndex >= 0 && (flags&0x01)) {
    if (machineInfo.port7ffd >= 0) zxLastOut7ffd |= 0x10; // switch to ROM1
    emuRealizeMemoryPaging();
    zxTRDOSpagein();
  }

  firstTimeReset = 0;
}


void emuBuildContentionTable (void) {
  memset(zxContentionTable, 0, sizeof(zxContentionTable));
  if (machineInfo.contention) {
    // fixme: +3 contention
    for (int f = 0; f < machineInfo.tsperframe; ++f) {
      if (f >= machineInfo.topleftpixts-1+zxLateTimings) {
        const int ts = f-(machineInfo.topleftpixts-1+zxLateTimings);
        if (ts/machineInfo.tsperline < machineInfo.vscreen) {
          // in screen area
          const int ltp = ts%machineInfo.tsperline;
          // no borders or retrace
          if (ltp < machineInfo.hscreen/*-(machineInfo.usePlus3Contention ? 3 : 0)*/) {
            if (machineInfo.usePlus3Contention) {
              // +3
              const int cpat[] = { 1, 0, 7, 6, 5, 4, 3, 2 };
              //const int cpat[] = { 7, 6, 5, 4, 3, 2, 1, 0 };
              zxContentionTable[0][f] = zxContentionTable[1][f] = cpat[ltp&0x07]; // nomreq: no contention
              //fprintf(stderr, "%d: %d\n", f, cpat[ltp&0x07]);
            } else {
              // normal
              const int cpat[] = { 6, 5, 4, 3, 2, 1, 0, 0 };
              zxContentionTable[0][f] = zxContentionTable[1][f] = cpat[ltp&0x07];
              //fprintf(stderr, "%d: %d\n", f, cpat[ltp&0x07]);
            }
            if (!machineInfo.iocontention) zxContentionTable[1][f] = 0; // no contention on port i/o
          }
        }
      }
    }
  }
  #if 0
  fprintf(stderr, "=== CONTENTIONS ===\n");
  for (int f = 0; f < machineInfo.tsperframe; ++f) {
    if (zxContentionTable[0][f]) fprintf(stderr, "%5d: %d\n", f, zxContentionTable[0][f]);
  }
  fprintf(stderr, "-------------------\n");
  #endif
}


void emuSetLateTimings (int late) {
  if (zxLateTimings != (late = !!late)) {
    zxLateTimings = late;
    emuBuildContentionTable();
  }
}


static const char *parseROMName (const char *fname, int *idx) {
  static char name[8192]; // hehe
  char *slash, *colon;
  //
  strcpy(name, fname);
  *idx = 0;
  if ((colon = strrchr(name, ':')) != NULL && isdigit(colon[1])) {
    if ((slash = strrchr(name, '/')) == NULL || slash < colon) {
      int ok = 1, n = 0;
      //
      for (const char *p = colon+1; *p; ++p) {
        if (isdigit(*p)) {
          n = n*10+p[0]-'0';
          if (n > 65535) { n = 0; ok = 0; break; }
        } else {
          ok = 0;
          break;
        }
      }
      //
      if (ok) {
        *idx = n;
        *colon = 0;
      }
    }
  }
  return name;
}


static int emuReadROM (const char *fname, int pageno) {
  FILE *fl;
  size_t rd;
  int pgidx;
  const char *name = parseROMName(fname, &pgidx);
  //
  //fprintf(stderr, "ROM: [%s] (%d)\n", name, pgidx);
  if ((fl = fopen(name, "rb")) == NULL) return -1;
  if (fseek(fl, pgidx*0x4000, SEEK_SET) != 0) { fclose(fl); return -2; }
  memset(zxROMBanks[pageno], 0, 0x4000);
  rd = fread(zxROMBanks[pageno], 1, 0x4000, fl);
  fclose(fl);
  if (rd < 1) return -3;
  return (int)rd;
}


static const char *model2romstr (int model) {
  switch (model) {
    case ZX_MACHINE_48K: return "48:";
    case ZX_MACHINE_128K: return "128:";
    case ZX_MACHINE_PLUS2: return "plus2:";
    case ZX_MACHINE_PLUS2A: return "plus2a:";
    case ZX_MACHINE_PLUS3: return "plus3:";
    case ZX_MACHINE_PENTAGON:
      switch (zxPentagonMemory) {
        case 128: return "pentagon128:";
        case 512: return "pentagon512:";
        case 1024: return "pentagon1024:";
      }
      break;
    case ZX_MACHINE_SCORPION: return "scorpion:";
  }
  return "unknown:";
}


static Jim_Obj *emuGetROMNamesByName (Jim_Obj *dict, const char *name) {
  //fprintf(stderr, "emuGetROMNamesByName: [%s] %p\n", name, dict);
  if (dict != NULL) {
    Jim_Obj *mno = Jim_NewStringObj(jim, name, -1);
    Jim_Obj *lsto = NULL;
    Jim_IncrRefCount(mno);
    if (Jim_DictKey(jim, dict, mno, &lsto, JIM_NONE) == JIM_OK) {
      Jim_DecrRefCount(jim, mno);
      return lsto;
    }
    // model not found
    Jim_DecrRefCount(jim, mno);
  }
  //fprintf(stderr, "emuGetROMNamesByName: [%s] NOT FOUND\n", name);
  return NULL;
}


static const char *jimStr (Jim_Obj *str) {
  Jim_Obj *eres = NULL, *dupl;
  static char rs[8192];
  //
  dupl = Jim_DuplicateObj(jim, str);
  Jim_IncrRefCount(dupl); // we need to do this after Jim_DuplicateObj()
  //fprintf(stderr, "JIMSTR: dup=[%s]\n", Jim_String(dupl));
  if (Jim_SubstObj(jim, dupl, &eres, 0) == JIM_OK) {
    //fprintf(stderr, "JIMSTR: dup ok\n");
    Jim_IncrRefCount(eres);
    rs[sizeof(rs)-1] = 0;
    strncpy(rs, Jim_String(eres), sizeof(rs)-1);
    Jim_DecrRefCount(jim, eres);
    Jim_DecrRefCount(jim, dupl);
    return rs;
  }
  //fprintf(stderr, "JIMSTR: dup BAD\n");
  Jim_DecrRefCount(jim, dupl);
  return NULL;
}


typedef enum {
  GETROM_NORMAL,
  GETROM_BETA,
  GETROM_TRDOS,
  GETROM_OPENSE,
  GETROM_GLUCK,
  GETROM_QUICKCMDR,
} ROMType;


static Jim_Obj *emuGetROMNames (int model, ROMType rtp) {
  Jim_Obj *roms = Jim_GetGlobalVariableStr(jim, "romlist", JIM_NONE);
  // there is no TR-DOS in +2A and +3
  if (emuIsPlus3DOSModel(model)) rtp = GETROM_NORMAL;
  //fprintf(stderr, "emuGetROMNames: (%d) [%s] %p\n", rtp, model2romstr(model), roms);
  if (roms != NULL) {
    Jim_Obj *lsto = emuGetROMNamesByName(roms, model2romstr(model)), *res = NULL;
    if (lsto == NULL && model == ZX_MACHINE_PENTAGON) {
      // fallback to generic pentagon
      lsto = emuGetROMNamesByName(roms, "pentagon:");
    }
    if (lsto != NULL) {
      // model found
      switch (rtp) {
        case GETROM_BETA:
          if ((res = emuGetROMNamesByName(lsto, "betadisk:")) != NULL) return res;
          // fallthru
        case GETROM_NORMAL:
          return emuGetROMNamesByName(lsto, "normal:");
        case GETROM_TRDOS:
          if ((res = emuGetROMNamesByName(lsto, "trdos:")) != NULL) return res;
          return emuGetROMNamesByName(roms, "trdos:");
        case GETROM_OPENSE:
          return emuGetROMNamesByName(roms, "opense:");
        case GETROM_GLUCK:
          return emuGetROMNamesByName(roms, "gluck:");
        case GETROM_QUICKCMDR:
          return emuGetROMNamesByName(roms, "quickcmdr:");
      }
    }
  }
  return NULL;
}


static int emuLoadROMs (int model, int rompages) {
  Jim_Obj *lst, *no;
  const char *js;
  int cnt;
  optTRDOSROMIndex = -1;
  if ((lst = emuGetROMNames(model, (optTRDOSenabled ? GETROM_BETA : GETROM_NORMAL))) == NULL) {
    // no roms
    //fprintf(stderr, "NO ROMS! model=%d; rompages=%d\n", model, rompages);
    return -1;
  }
  if ((cnt = Jim_ListLength(jim, lst)) < 1 || cnt > rompages) {
    // invalid rom list
    //fprintf(stderr, "INVALID ROM LIST! model=%d; rompages=%d\n", model, rompages);
    return -1;
  }
  //fprintf(stderr, "LOADING ROMS: model=%d; rompages=%d\n", model, rompages);
  // load 'em
  for (int f = 0; f < rompages; ++f) {
    if ((no = Jim_ListGetIndex(jim, lst, f)) == NULL) return -1;
    if ((js = jimStr(no)) == NULL) return -1;
    if (emuReadROM(js, f) < 0) return -1;
  }
  if (!emuIsPlus3DOSModel(model)) {
    if (optOpenSE) {
      if ((lst = emuGetROMNames(model, GETROM_OPENSE)) != NULL) {
        if ((cnt = Jim_ListLength(jim, lst)) < 1 || cnt > 1) return -1; // invalid trdos rom list
        if ((no = Jim_ListGetIndex(jim, lst, 0)) == NULL) return -1;
        if ((js = jimStr(no)) == NULL) return -1;
        if (emuReadROM(js, 0) < 0) return -1;
      }
    }
    if (optTRDOSenabled > 0) {
      // load tr-dos page
      if ((lst = emuGetROMNames(model, GETROM_TRDOS)) == NULL) return -1; // no trdos rom
      if ((cnt = Jim_ListLength(jim, lst)) < 1 || cnt > 1) return -1; // invalid trdos rom list
      if ((no = Jim_ListGetIndex(jim, lst, 0)) == NULL) return -1;
      if ((js = jimStr(no)) == NULL) return -1;
      if (emuReadROM(js, rompages) < 0) return -1;
      optTRDOSROMIndex = rompages;
    }
    {
      zxGluckROM = -1;
      if ((lst = emuGetROMNames(model, GETROM_GLUCK)) != NULL) {
        if ((cnt = Jim_ListLength(jim, lst)) == 1) {
          if ((no = Jim_ListGetIndex(jim, lst, 0)) != NULL) {
            if ((js = jimStr(no)) != NULL) {
              if (emuReadROM(js, rompages+1) == 16384) {
                zxGluckROM = rompages+1;
              }
            }
          }
        }
      }
    }
    {
      zxQCmdrROM = -1;
      if ((lst = emuGetROMNames(model, GETROM_QUICKCMDR)) != NULL) {
        if ((cnt = Jim_ListLength(jim, lst)) == 1) {
          if ((no = Jim_ListGetIndex(jim, lst, 0)) != NULL) {
            if ((js = jimStr(no)) != NULL) {
              if (emuReadROM(js, rompages+2) == 16384) {
                zxQCmdrROM = rompages+2;
              }
            }
          }
        }
      }
    }
  } else {
    // +2A, +3
    optTRDOSROMIndex = -1;
  }
  return 0;
}


int emuSetModel (int model, int forced) {
  switch (model) {
    case ZX_MACHINE_48K:
    case ZX_MACHINE_128K:
    case ZX_MACHINE_PLUS2:
    case ZX_MACHINE_PLUS2A:
    case ZX_MACHINE_PLUS3:
    case ZX_MACHINE_PENTAGON:
    case ZX_MACHINE_SCORPION:
      break;
    default: return -1;
  }

  if (model != zxModel || forced) {
    //fprintf(stderr, "==== RESET ====\n");
    zxModel = model;
    fillMachineInfo(&machineInfo, &basicMachineInfo[model]);
    if (zxModel == ZX_MACHINE_PENTAGON && zxPentagonMemory > 512) machineInfo.port1ffd = 0;
    z80.evenM1 = machineInfo.evenM1;
    emuBuildContentionTable();

    // fdc
    emu_fdd_event_interface.cpu_speed_hz = machineInfo.cpuSpeed;
    //emuSetFDCType(emuGetModelFDCType(model));

    //printf("lpf=%d; tspl=%d; tspf=%d; stt=%d\n", machineInfo.linesperframe, machineInfo.tsperline, machineInfo.tsperframe, machineInfo.topleftpixts-(machineInfo.tsperline*machineInfo.topBorder));
    for (int f = 0; f < 400; ++f) zxLineStartTS[f] = -1;
    // screen
    zxLineStartTS[0] =
      machineInfo.topleftpixts-
      (/*visible top border*/3*8*machineInfo.tsperline)-
      (/*visible chars in left border*/4*4);
    for (int f = 1; f <= 192+24*2; ++f) zxLineStartTS[f] = zxLineStartTS[f-1]+machineInfo.tsperline;

    if (emuLoadROMs(model, machineInfo.romPages) < 0) {
      cprintf("FATAL: can't read machine ROMs! [%s]\n", model2romstr(model));
      fprintf(stderr, "FATAL: can't read machine ROMs! [%s]\n", model2romstr(model));
      abort();
    }

    if (optTRDOSenabled > 0 && optTRDOSROMIndex < 0 && !emuIsPlus3DOSModel(model)) {
      cprintf("WARNING: TR-DOS is disabled: no ROM found!\n");
    }

    emuReset(0);
  }

  return 0;
}


////////////////////////////////////////////////////////////////////////////////
void emuSetMaxSpeed (int maxsp) {
  if (maxsp) {
    if (!optMaxSpeed) {
      emuFrameStartTime = 0;
      optMaxSpeed = 1;
      emuPrevFCB = 0;
      soundSetUse(0);
    }
  } else {
    if (optMaxSpeed) {
      emuFrameStartTime = 0;
      optMaxSpeed = 0;
      emuPrevFCB = 0;
      if (optSpeed == 100) soundSetUse(1);
    }
  }
}


void emuSetSpeed (int prc) {
  if (prc < 1) prc = 1; else if (prc > 999999) prc = 999999;
  soundSetUse(prc == 100 && !optMaxSpeed);
  if (optSpeed != prc) {
    optSpeed = prc;
    emuFrameStartTime = 0;
    emuPrevFCB = 0;
  }
}


void emuSetPaused (int on) {
  switch (on) {
    case PAUSE_PERMANENT_SET: optPaused |= PAUSE_PERMANENT_MASK; break;
    case PAUSE_PERMANENT_RESET: optPaused &= ~PAUSE_PERMANENT_MASK; break;
    case PAUSE_TEMPORARY_SET: optPaused |= PAUSE_TEMPORARY_MASK; break;
    case PAUSE_TEMPORARY_RESET: optPaused &= ~PAUSE_TEMPORARY_MASK; break;
  }
  if (optPaused) { emuFrameStartTime = 0; emuPrevFCB = 0; }
}


void emuSetDrawFPS (int on) {
  if (!!on != optDrawFPS) {
    emuFrameStartTime = 0;
    optDrawFPS = !!on;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
static inline void emuPaging128 (void) {
  zxSetScreen((zxLastOut7ffd&0x08 ? 7 : 5));
  zxPageROM((zxLastOut7ffd>>4)&0x01);
  zxPageRAM(3, (zxLastOut7ffd&0x07));
  zx7ffdLocked = ((zxLastOut7ffd&0x20) != 0);
}


static inline void emuPagingPentagon512 (void) {
  zxSetScreen((zxLastOut7ffd&0x08 ? 7 : 5));
  zxPageROM((zxLastOut7ffd>>4)&0x01);
  zxPageRAM(3, (zxLastOut7ffd&0x07)+((zxLastOut7ffd&0xc0)>>3));
  zx7ffdLocked = ((zxLastOut7ffd&0x20) != 0);
}


static inline void emuPagingPentagon1024 (void) {
  // #EFFD (#1FFD):
  //   D0: 16col
  //   D1: 512x192
  //   D2: 128K lock (if set)
  //   D3: RAM bank 0 to #0000 (if set)
  //   D4: disable turbo (if set)
  //   D5: multicolor/scroll, nobody cares anymore
  //   D6: 384x304/scroll, nobody cares anymore
  //   D7: Gluck CMOS active (if set)
  // D2: if set, this is 128KB lock
  if (zxLastOut1ffd&0x04) return emuPaging128();
  // ok, no lock
  zx7ffdLocked = 0; // anyway
  zxSetScreen((zxLastOut7ffd&0x08 ? 7 : 5));
  zxPageROM((zxLastOut7ffd>>4)&0x01);
  // D3: if set, RAM bank 0 is at #0000
  if (zxLastOut7ffd&0x08) zxPageRAM(0, 0); // pentagon v2.2
  // #7FFD: D0-D2; D6-D7; D5 (sooo fucked, lol)
  zxPageRAM(3, (zxLastOut7ffd&0x07)+((zxLastOut7ffd&0xc0)>>3)+((zxLastOut7ffd&0x20)<<2));
}


static inline void emuPagingScorpion (void) {
  //fprintf(stderr, "7F=#%02X; 1F=#%02X\n", zxLastOut7ffd, zxLastOut1ffd);
  zxSetScreen((zxLastOut7ffd&0x08 ? 7 : 5));
  zxPageROM(zxLastOut1ffd&0x02 ? 2 : (zxLastOut7ffd>>4)&0x01);
  if (zxLastOut1ffd&0x01) zxPageRAM(0, 0);
  zxPageRAM(3, (zxLastOut7ffd&0x07)|((zxLastOut1ffd&0x10)>>1));
  zx7ffdLocked = ((zxLastOut7ffd&0x20) != 0);
}


static inline void emuPagingPlus3 (void) {
  //fprintf(stderr, "emuPagingPlus3: 7F=#%02X; 1F=#%02X\n", zxLastOut7ffd, zxLastOut1ffd);
  zxSetScreen((zxLastOut7ffd&0x08 ? 7 : 5));
  if (zxLastOut1ffd&0x01) {
    // special mode
    switch ((zxLastOut1ffd>>1)&0x03) {
      case 0:
        //fprintf(stderr, "+3: ram=0123\n");
        zxPageRAM(0, 0);
        zxPageRAM(1, 1);
        zxPageRAM(2, 2);
        zxPageRAM(3, 3);
        break;
      case 1:
        //fprintf(stderr, "+3: ram=4567\n");
        zxPageRAM(0, 4);
        zxPageRAM(1, 5);
        zxPageRAM(2, 6);
        zxPageRAM(3, 7);
        break;
      case 2:
        //fprintf(stderr, "+3: ram=4563\n");
        zxPageRAM(0, 4);
        zxPageRAM(1, 5);
        zxPageRAM(2, 6);
        zxPageRAM(3, 3);
        break;
      case 3:
        //fprintf(stderr, "+3: ram=4763\n");
        zxPageRAM(0, 4);
        zxPageRAM(1, 7);
        zxPageRAM(2, 6);
        zxPageRAM(3, 3);
        break;
    }
  } else {
    //fprintf(stderr, "+3: rom=%u; ram=%u\n", (((zxLastOut1ffd>>1)&0x02)|((zxLastOut7ffd>>4)&0x01)), (zxLastOut7ffd&0x07));
    zxPageROM(((zxLastOut1ffd>>1)&0x02)|((zxLastOut7ffd>>4)&0x01));
    zxPageRAM(1, 5);
    zxPageRAM(2, 2);
    zxPageRAM(3, (zxLastOut7ffd&0x07));
  }
  zx7ffdLocked = ((zxLastOut7ffd&0x20) != 0);
}


void emuRealizeMemoryPaging (void) {
  //fprintf(stderr, "emuRealizeMemoryPaging: 7F=#%02X; 1F=#%02X\n", zxLastOut7ffd, zxLastOut1ffd);
  switch (zxModel) {
    case ZX_MACHINE_48K: break;
    case ZX_MACHINE_128K: case ZX_MACHINE_PLUS2: emuPaging128(); break;
    case ZX_MACHINE_PLUS2A: case ZX_MACHINE_PLUS3: emuPagingPlus3(); break;
    case ZX_MACHINE_PENTAGON:
      switch (zxPentagonMemory) {
        case 128: emuPaging128(); break;
        case 512: emuPagingPentagon512(); break;
        case 1024: emuPagingPentagon1024(); break;
        default: abort(); break;
      }
      break;
    case ZX_MACHINE_SCORPION: emuPagingScorpion(); break;
    default: abort(); break;
  }
}


////////////////////////////////////////////////////////////////////////////////
void emuUPD765Init (void) {
  if (upd765_fdc) {
    fprintf(stderr, "FATAL: double uPD765 FDC initialisation!\n");
    __builtin_trap();
  }

  upd765_fdc = upd_fdc_alloc_fdc(UPD765A, UPD_CLOCK_4MHZ);
  if (!upd765_fdc) {
    fprintf(stderr, "FATAL: no memory for uPD765 FDC!\n");
    __builtin_trap();
  }

  memset(&upd765_drives[0], 0, sizeof(upd765_drives[0]));
  memset(&upd765_drives[1], 0, sizeof(upd765_drives[1]));
  upd765_drives[0].disk = &flpdisk[0];
  upd765_drives[1].disk = &flpdisk[1];

  // the plus3 only use the US0 pin to select drives
  upd765_fdc->drive[0] = &upd765_drives[0];
  upd765_fdc->drive[1] = &upd765_drives[1];
  upd765_fdc->drive[2] = &upd765_drives[0];
  upd765_fdc->drive[3] = &upd765_drives[1];

  for (int i = 0; i < 2; ++i) {
    upd765_drives[i].disk->flag = DISK_FLAG_PLUS3_CPC;
  }

  /* builtin drive 1 head 42 track */
  fdd_init(&upd765_drives[0], FDD_SHUGART, &fdd_params[1], 0);
  fdd_init(&upd765_drives[1], FDD_SHUGART, NULL, 0); /* drive geometry 'autodetect' */

  upd765_fdc->set_intrq = NULL;
  upd765_fdc->reset_intrq = NULL;
  upd765_fdc->set_datarq = NULL;
  upd765_fdc->reset_datarq = NULL;

  upd765_fdc->speedlock = 0; // -1 if disabled

  const fdd_params_t *dt;

  dt = &fdd_params[FDD_PARM_DS_80];
  fdd_init(&upd765_drives[0], FDD_SHUGART, dt, 1);

  dt = &fdd_params[FDD_PARM_DS_80];
  fdd_init(&upd765_drives[1], (dt->enabled ? FDD_SHUGART : FDD_TYPE_NONE), dt, 1);
}


void emuUPD765Reset (void) {
  if (!upd765_fdc) return;
  upd_fdc_master_reset(upd765_fdc);
  for (int f = 0; f < 2; ++f) {
    if (disk_is_valid(upd765_fdc->drive[f]->disk)) {
      fdd_load(upd765_fdc->drive[f]);
    }
  }
}


void emuInitDisks (void) {
  fdd_ev_ifc = &emu_fdd_event_interface;
  emu_fdd_event_interface.cpu_speed_hz = machineInfo.cpuSpeed;

  fdd_register_startup();

  for (int f = 0; f < 4; ++f) {
    disk_init_struct(&flpdisk[f]);
    if (disk_new(&flpdisk[f], 2, 80, DISK_DD, DISK_UDI) != DISK_OK) {
      fprintf(stderr, "FATAL: cannot create disk image #%d! (%s)\n", f,
              disk_strerror(flpdisk[f].status));
      exit(1);
    }
  }

  if (!upd765_fdc) emuUPD765Init();

  disk_t *disks[4] = { &flpdisk[0], &flpdisk[1], &flpdisk[2], &flpdisk[3] };
  beta_init(disks);
}


void emuDiskEject (int diskidx) {
  if (diskidx < 0 || diskidx > 3) return;
  beta_disk_eject(diskidx);
  if (upd765_fdc) upd_fdc_eject(upd765_fdc, diskidx&1);
  disk_close(&flpdisk[diskidx]);
}


void emuDiskInsert (int diskidx) {
  if (diskidx < 0 || diskidx > 3) return;

  beta_disk_eject(diskidx);
  if (upd765_fdc) upd_fdc_eject(upd765_fdc, diskidx&1);

  fdd_t *fdd = beta_get_fdd(diskidx);
  if (!fdd) { cprintf("\4ERROR: cannot find FDD!\n"); return; }

  if (upd765_fdc) fdd_load(upd765_fdc->drive[diskidx&1]);
  fdd_load(fdd);

  cprintf("DISK: %d sides, %d cylinders, %d bpt, have_weak=%d, dens=%d\n",
          fdd->disk->sides, fdd->disk->cylinders, fdd->disk->bpt,
          fdd->disk->have_weak, fdd->disk->density);
}


int emuDiskInsertWithReader (int diskidx, const void *buf, int size, emuDiskReaderFn reader) {
  emuDiskEject(diskidx);
  if (buf == NULL || size < 16 || diskidx < 0 || diskidx > 3) return -1;

  fdd_t *fdd = beta_get_fdd(diskidx);
  if (!fdd) {
    cprintf("\4ERROR: cannot find FDD!\n");
    return -1;
  }

  const int res = reader(fdd->disk, buf, size);
  if (res == DISK_OK) {
    fdd_load(fdd);
    if (upd765_fdc) fdd_load(upd765_fdc->drive[diskidx&1]);
    cprintf("DISK: %d sides, %d cylinders, %d bpt, have_weak=%d, dens=%d\n",
            fdd->disk->sides, fdd->disk->cylinders, fdd->disk->bpt,
            fdd->disk->have_weak, fdd->disk->density);
  } else {
    cprintf("\4ERROR loading disk: %s!\n", disk_strerror(res));
  }

  return res;
}


int emuSaveDiskToFile (int diskidx, const char *fname) {
  if (!fname || !fname[0] || diskidx < 0 || diskidx > 3) return -1;

  fdd_t *fdd = beta_get_fdd(diskidx);
  if (!fdd) {
    cprintf("\4ERROR: cannot find FDD!\n");
    return -1;
  }

  return disk_write(fdd->disk, fname);
}


//==========================================================================
//
//  emuGetCursorColor
//
//==========================================================================
uint8_t emuGetCursorColor (uint8_t c0, uint8_t c1) {
  if (optCurBlinkMS > 10) return (timerGetMS()%(optCurBlinkMS*2) < optCurBlinkMS ? c0 : c1);
  return c0;
}
