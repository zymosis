/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
enum {
  TAPE_ROM_BIT0_TS = 855,
  TAPE_ROM_BIT1_TS = 1710,
  TAPE_ROM_SYNC1_TS = 667,
  TAPE_ROM_SYNC2_TS = 735,
  TAPE_ROM_TAIL_TS = 945,
  TAPE_ROM_PILOT_TS = 2168
};


////////////////////////////////////////////////////////////////////////////////
// standard loader
// sets tapNextEdgeTS and tapCurEdge
// returns -1 if tape stopped or tapNextEdgeTS on success
int emuTapeLdrGetNextPulse (void) {
  int ts = 0;
  for (;;) {
    emuTapeEdgeAdvance(ts); // tapNextEdgeTS: t-states to next edge
    if (emuTapeCheckStop()) return -1; // epic fail
    if (tapNextEdge != tapCurEdge) {
      // edge found
      tapCurEdge = tapNextEdge;
      break;
    }
    ts += tapNextEdgeTS;
  }
  return tapNextEdgeTS;
}


static inline int goodTiming (const int ts, const int timing, int tolerance) {
  return (abs(ts-timing) <= tolerance);
}


// -1: tape not running
// -2: tape loading error (tape MAY BE stopped)
int emuTapeLoadBit (int bit0ts, int bit1ts) {
  int ts = emuTapeLdrGetNextPulse();
  if (ts < 0) return -2; // epic fail
  int tolerance = abs(bit0ts-bit1ts)/4;
  if (tolerance < 18) tolerance = 18;
  if (goodTiming(ts, bit0ts, tolerance) || goodTiming(ts, bit1ts, tolerance)) {
    // half-bit found, check the second half
    int ts1 = emuTapeLdrGetNextPulse();
    if (ts1 < 0 || abs(ts1-ts) > tolerance) return -2; // epic fail
  } else {
    return -2; // epic fail
  }
  return (ts >= bit0ts+tolerance ? 1 : 0);
}


// -1: tape not running
// -2: tape loading error (tape MAY BE stopped)
int emuTapeLoadByte (int bit0ts, int bit1ts) {
  if (zxCurTape != NULL && optTapePlaying) {
    static int strobe_debug = -1;
    if (strobe_debug < 0) {
      const char *var = getenv("ZXEMUT_DEBUG_STROBE");
      strobe_debug = (var != NULL && var[0]);
    }
    int b = 0x01; // 'stop' flag
    int tolerance = abs(bit0ts-bit1ts)/4;
    if (tolerance < 18) tolerance = 18;
    while (b < 0x100) {
      int ts = emuTapeLdrGetNextPulse();
      if (ts < 0) return -2; // epic fail
      if (goodTiming(ts, bit0ts, tolerance) || goodTiming(ts, bit1ts, tolerance)) {
        // half-bit found, check the second half
        int ts1 = emuTapeLdrGetNextPulse();
        if (ts1 < 0 || abs(ts1-ts) > tolerance) {
          if (strobe_debug) fprintf(stderr, " bad pulse 1 timing: %d (0:%d, 1:%d)\n", ts, bit0ts, bit1ts);
          return -2; // epic fail
        }
        if (strobe_debug) fprintf(stderr, " strobe timing: %d, %d (0:%d, 1:%d) value=%d\n", ts, ts1, bit0ts, bit1ts, (ts >= bit0ts+tolerance ? 1 : 0));
      } else {
        if (strobe_debug) fprintf(stderr, " bad pulse 0 timing: %d (0:%d, 1:%d)\n", ts, bit0ts, bit1ts);
        return -2; // epic fail
      }
      b = (b<<1)|(ts >= bit0ts+tolerance ? 1 : 0);
    }
    return (b&0xff);
  }
  return -1;
}


// -1: tape not running
// -2: tape loading error (tape MAY BE stopped)
int emuTapeROMLoadByte (void) {
  return emuTapeLoadByte(TAPE_ROM_BIT0_TS, TAPE_ROM_BIT1_TS);
}


const tape_loader_info_t tape_loader_info_rom = {
  .pilot_minlen = 807,
  .pilot = TAPE_ROM_PILOT_TS,
  .sync1 = TAPE_ROM_SYNC1_TS,
  .sync2 = TAPE_ROM_SYNC2_TS,
  .bit0 = TAPE_ROM_BIT0_TS,
  .bit1 = TAPE_ROM_BIT1_TS
};


// return:
//   -1: pilot or block id searching fails (so just continue executing)
//    0: something was loaded (with or without error); execute loader epilogue
int emuTapeDoFlashLoadEx (const tape_loader_info_t *ti) {
  int bt, ts, parity = (ti->init_parity < 0 ? 0 : ti->init_parity&0xff);
  uint8_t flag = z80.afx.a;
  int len = z80.de.w, addr = z80.ix.w;
  if (len == 0) len = 65536;
  if (optDebugFlashLoad) fprintf(stderr, "flashload: flags=0x%04x; type=#%02X; addr=#%04X, len=%d\n", ti->flags, flag, z80.ix.w, len);
skip_block:
  if (optDebugFlashLoad) fprintf(stderr, "searching for pilot...\n");
  // search for pilot
  if (ti->pilot_minlen > 0) {
    int pilot_pulses = 0;
    int ptolerance = ti->pilot/4;
    int stolerance = abs(ti->sync1-ti->sync2)/1-8; // IDIOTIC MetalArmy.tzx!
    if (ptolerance < 18) ptolerance = 18;
    if (stolerance < 18) stolerance = 18;
    if (optDebugFlashLoad) fprintf(stderr, "pilot tolerance: ts=%d; tolerance=%d\n", ti->pilot, ptolerance);
    if (optDebugFlashLoad) fprintf(stderr, "sync tolerance: ts=%d,%d; tolerance=%d\n", ti->sync1, ti->sync2, stolerance);
    for (;;) {
      if ((ts = emuTapeLdrGetNextPulse()) < 0) return -1; // end of tape, nothing was found
      if (goodTiming(ts, ti->pilot, ptolerance)) { ++pilot_pulses; continue; }
      //fprintf(stderr, "pilot ts: %d\n", ts); 
      // was pilot of enough length found?
      if (pilot_pulses >= ti->pilot_minlen) {
        if (optDebugFlashLoad) fprintf(stderr, "pilot; length=%d (%s)\n", pilot_pulses, (pilot_pulses >= ti->pilot_minlen ? "good" : "BAD"));
        // sheck sync1
        if (goodTiming(ts, ti->sync1, stolerance)) {
          // sheck sync2
          if (optDebugFlashLoad) fprintf(stderr, "GOOD sync1: %d\n", ts);
          if ((ts = emuTapeLdrGetNextPulse()) < 0) return -1; // end of tape, nothing was found
          // checking for SYNC1 too -- idiotic MetalArmy.tzx sucks cocks!
          if (goodTiming(ts, ti->sync2, stolerance) || goodTiming(ts, ti->sync1, stolerance)) {
            if (optDebugFlashLoad) fprintf(stderr, "GOOD sync2: %d\n", ts);
            break;
          }
          if (optDebugFlashLoad) fprintf(stderr, "bad sync2: %d\n", ts);
        } else {
          if (optDebugFlashLoad) fprintf(stderr, "bad sync1: %d\n", ts);
        }
        if (optDebugFlashLoad) fprintf(stderr, "bad sync\n");
      }
      //if (optDebugFlashLoad && pilot_pulses > 0) fprintf(stderr, "bad pilot (pilot_pulses=%d)\n", pilot_pulses);
      // pilot too short
      pilot_pulses = 0;
    }
  }
  if (optDebugFlashLoad) fprintf(stderr, "sync ok\n");
  // load first byte (block type)
  if ((ti->flags&CFL_NO_TYPE) == 0) {
    if ((bt = emuTapeLoadByte(ti->bit0, ti->bit1)) < 0) return -1; // end of tape or tape error
    parity ^= bt;
    if (optDebugFlashLoad) fprintf(stderr, "block type: #%02X\n", bt);
    // check block type
    if ((ti->flags&CFL_ANY_TYPE) == 0 && ((ti->flags&CFL_NO_LEN_GLITCH) != 0 || z80.de.d != 0xff)) {
      if (bt != flag) goto skip_block; // bad block type, skip it
    }
  }
  // load parity byte
  if ((ti->flags&(CFL_NO_PARITY|CFL_PARITY_FIRST)) == CFL_PARITY_FIRST) {
    if ((bt = emuTapeLoadByte(ti->bit0, ti->bit1)) < 0) return -1; // end of tape or tape error
    parity ^= bt;
  }
  if (optDebugFlashLoad) fprintf(stderr, "loading %d bytes to #%04X\n", len, addr);
  // load block bytes
  while (len > 0) {
    if ((bt = emuTapeLoadByte(ti->bit0, ti->bit1)) < 0) break; // end of tape or tape error
    parity ^= bt;
    z80.mem_write(&z80, addr, bt, ZYM_MEMIO_OTHER);
    addr += (ti->flags&CFL_REVERSE ? -1 : 1);
    addr &= 0xffff;
    --len;
  }
  if (optDebugFlashLoad) fprintf(stderr, " done bytes, addr=#%04X, len=%d\n", addr, len);
  z80.de.w = len&0xffff;
  z80.ix.w = addr;
  // load parity byte
  if ((ti->flags&(CFL_NO_PARITY|CFL_PARITY_FIRST)) == 0) {
    if ((bt = emuTapeLoadByte(ti->bit0, ti->bit1)) >= 0) parity ^= bt;
  }
  if (optDebugFlashLoad) fprintf(stderr, "done, parity: #%02X\n", parity);
  z80.hl.h = parity;
  return 0;
}


// return:
//   -1: pilot or block id searching fails (so just continue executing)
//    0: something was loaded (with or without error); execute loader epilogue
int emuTapeDoROMLoad (void) {
  return emuTapeDoFlashLoadEx(&tape_loader_info_rom);
}


/*
// return:
//   -1: pilot or block id searching fails
//    0: something was loaded (with or without error)
// anyway, just continue execution
// should be called when z80.pc == 0x0562
int emuTapeAcceletateROMLoader (void) {
  int res;
  if (zxCurTape == NULL || !optTapePlaying) return -1;
  // if we are VERIFYing a tape, do not accelerate this (i'm so lazy...)
  if ((z80.afx.f&ZYM_FLAG_C) == 0) return -1;
  cprintf("FastLoader(utm) invoked.\n");
  cprintf("block type #%02X at #%04X (len=%u #%04X)\n", z80.afx.a, z80.ix.w, z80.de.w, z80.de.w);
  if (emuTapeDoROMLoad()) {
    // just continue
    res = -1;
    cprintf("FastLoader(utm): block not found!\n");
  } else {
    // something loaded, execute epilogue
    cprintf("FastLoader(utm): %s!\n", (z80.hl.h ? "\4tape loading error" : "ok"));
    z80.pc = 0x5df; // LD A,H; CP 1; RET
    res = 0;
  }
  return res;
}
*/
