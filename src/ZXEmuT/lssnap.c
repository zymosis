/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include <unistd.h>
#ifdef USE_IMLIB2
# include <Imlib2.h>
#endif
#ifdef HAVE_ZLIB
# include <zlib.h>
#endif

#include "lssnap.h"
#include "emuutils.h"
#include "console.h"
#include "libvideo/video.h"
#include "tapes.h"
#include "snd_alsa.h"
#include "zxscrdraw.h"
#include "libboots/libboots.h"

#include "zips.c"


//==========================================================================
//
//  snapRealize
//
//==========================================================================
int snapRealize (zxs_t *snap) {
  int newModel = zxModel, pentaMem = zxPentagonMemory, mmb = 8, do7ffd = 0, do1ffd = 0, plus3 = 0;
  newModel = zxModel;
  switch (snap->hdr.chMachineId) {
    case ZXS_MACH_16K:
    case ZXS_MACH_48K:
    case ZXS_MACH_NTSC48K:
      newModel = ZX_MACHINE_48K;
      break;
    case ZXS_MACH_128K:
      mmb = 8;
      do7ffd = 1;
      newModel = ZX_MACHINE_128K;
      break;
    case ZXS_MACH_PLUS2:
      mmb = 8;
      do7ffd = 1;
      newModel = ZX_MACHINE_PLUS2;
      break;
    case ZXS_MACH_PLUS2A:
      mmb = 8;
      do7ffd = 1;
      do1ffd = 1;
      pentaMem = 128;
      newModel = ZX_MACHINE_PLUS2A;
      break;
    case ZXS_MACH_PLUS3:
      mmb = 8;
      do7ffd = 1;
      do1ffd = 1;
      pentaMem = 128;
      newModel = ZX_MACHINE_PLUS3;
      break;
    case ZXS_MACH_PENTAGON128:
      mmb = 8;
      do7ffd = 1;
      pentaMem = 128;
      newModel = ZX_MACHINE_PENTAGON;
      break;
    case ZXS_MACH_PENTAGON512:
      mmb = 32;
      do7ffd = 1;
      do1ffd = 1;
      pentaMem = 512;
      newModel = ZX_MACHINE_PENTAGON;
      break;
    case ZXS_MACH_PENTAGON1024:
      mmb = 64;
      do7ffd = 1;
      do1ffd = 1;
      pentaMem = 1024;
      newModel = ZX_MACHINE_PENTAGON;
      break;
    case ZXS_MACH_SCORPION:
      mmb = 16;
      do7ffd = 1;
      do1ffd = 1;
      newModel = ZX_MACHINE_SCORPION;
      break;
    /*
    case ZXS_MACH_PLUS3:
      cprintf("snapload: +3 ignored\n");
      plus3 = 1;
      goto loadunknown;
    */
    case ZXS_MACH_PLUS3E:
      cprintf("snapload: +3E ignored\n");
      plus3 = 1;
      goto loadunknown;
    case ZXS_MACH_TC2048:
    case ZXS_MACH_TC2068:
    case ZXS_MACH_TS2068:
      cprintf("snapload: TIMEX fail!\n");
      return -1;
    case ZXS_MACH_SE:
      cprintf("snapload: SE ignored\n");
      plus3 = 2;
      goto loadunknown;
    case ZXS_MACH_128KE:
      cprintf("snapload: 128KE ignored\n");
      plus3 = 2;
loadunknown:
      if (optAllowOther128) {
        mmb = 8;
        cprintf("snapload: setting standard 128k\n");
        do7ffd = 1;
        newModel = ZX_MACHINE_128K;
        break;
      }
      // fallthru
    default:
      cprintf("\4snapload: invalid model\n");
      return -1;
  }
  // now check if we have registers and ports
  if (zxs_chunk_find(snap, ZXS_Z80REGS_SIGN) == NULL) {
    cprintf("\4snapload: no machine registers\n");
    return -1;
  }
  if (zxs_chunk_find(snap, ZXS_ZXREGS_SIGN) == NULL) {
    cprintf("\4snapload: no machine ports\n");
    return -1;
  }
  // ok set model and init it
  if (optSnapSetModel) {
    zxPentagonMemory = pentaMem;
    emuSetModel(newModel, 1);
    switch (plus3) {
      case 1: // +3
        machineInfo.iocontention = 0;
        machineInfo.usePlus3Contention = 1;
        emuBuildContentionTable();
        break;
      case 2: // SE/KE
        machineInfo.contention = 0;
        machineInfo.iocontention = 0;
        break;
    }
  }
  zxLateTimings = (snap->hdr.chFlags&ZXS_MF_ALTERNATE_TIMINGS ? 1 : 0);
  emuReset(0);
  soundSilenceCurFrame();
  if (!do7ffd) zxLastOut7ffd = 0x10;
  if (!do1ffd) zxLastOut1ffd = 0x00;
  soundAYReset();
  /* z80 state */
  {
    zxs_z80regs_t *regs = (zxs_z80regs_t *)zxs_chunk_find(snap, ZXS_Z80REGS_SIGN);
    z80.af.w = regs->AF;
    z80.bc.w = regs->BC;
    z80.de.w = regs->DE;
    z80.hl.w = regs->HL;
    z80.afx.w = regs->AF1;
    z80.bcx.w = regs->BC1;
    z80.dex.w = regs->DE1;
    z80.hlx.w = regs->HL1;
    z80.ix.w = regs->IX;
    z80.iy.w = regs->IY;
    z80.sp.w = regs->SP;
    z80.pc = regs->PC;
    z80.regI = regs->I;
    z80.regR = regs->R;
    z80.iff1 = regs->IFF1;
    z80.iff2 = regs->IFF2;
    z80.im = regs->IM;
    z80.tstates = regs->dwCyclesStart%machineInfo.tsperframe;
    if (z80.tstates < 0) z80.tstates = 0;
    z80.prev_was_EIDDR = (regs->chFlags&ZXS_Z80REGS_FLAG_EILAST ? 1 : 0);
    z80.halted = (regs->chFlags&ZXS_Z80REGS_FLAG_HALTED ? 1 : 0);
    z80.memptr.w = regs->wMemPtr;
    cprintf("SNA: PC=#%04X SP=#%04X tstates=%u I=#%02X R=#%02X IFF1=%u IFF2=%u IM=%u prevWasEIDDR=%d halted=%u\n", z80.pc, z80.sp.w, z80.tstates, z80.regI, z80.regR, z80.iff1, z80.iff2, z80.im, z80.prev_was_EIDDR, z80.halted);
    cprintf("AF :#%04X BC :#%04X DE :#%04X HL :#%04X IX:#%04X\n", z80.af.w, z80.bc.w, z80.de.w, z80.hl.w, z80.ix.w);
    cprintf("AF':#%04X BC':#%04X DE':#%04X HL':#%04X IY:#%04X\n", z80.afx.w, z80.bcx.w, z80.dex.w, z80.hlx.w, z80.iy.w);
  }
  /* ports */
  {
    zxs_zxregs_t *ports = (zxs_zxregs_t *)zxs_chunk_find(snap, ZXS_ZXREGS_SIGN);
    zxBorder = (ports->chBorder&0x0f);
    //cprintf("PORT #7FFD: #%02X\n", ports->ch7ffd);
    //cprintf("PORT #1FFD: #%02X\n", ports->ch1ffd);
    if (do7ffd) zxLastOut7ffd = ports->ch7ffd;
    if (do1ffd) zxLastOut1ffd = ports->ch1ffd;
    //zxUlaOut = (ports.chFe&0b00011000)|(0b11100111);
    zxUlaOut = 0xff;
  }
  /* keyboard */
  {
    zxs_keyb_t *keybd = (zxs_keyb_t *)zxs_chunk_find(snap, ZXS_KEYB_SIGN);
    if (keybd != NULL) {
      optZXIssue = (keybd->dwFlags&ZXS_KEYB_FLAG_ISSUE2 ? 1 : 0);
    }
  }
  /* mouse */
  {
    zxs_mouse_t *mouse = (zxs_mouse_t *)zxs_chunk_find(snap, ZXS_MOUSE_SIGN);
    if (mouse != NULL) {
      optKMouse = (mouse->chType == ZXS_MOUSE_KEMPSTON);
    }
  }
  /* ay */
  {
    zxs_ayregs_t *ay = (zxs_ayregs_t *)zxs_chunk_find(snap, ZXS_AYREGS_SIGN);
    if (ay != NULL) {
      zxLastOutFFFD = ay->chCurrentRegister;
      for (int f = 0; f < 16; ++f) zxAYRegs[f] = ay->chAyRegs[f];
    }
  }
  /* ram */
  emuClearRAM();
  if (snap->hdr.chMachineId <= ZXS_MACH_48K || snap->hdr.chMachineId == ZXS_MACH_NTSC48K) {
    zxs_page_t *ram;
    if ((ram = zxs_chunk_findram(snap, 5)) != NULL) {
      memcpy(zxMemoryBanks[5], ram->data, 0x4000);
      zxMemBanksDirty[5] = 1;
    } else {
      cprintf("\4snapload: missing RAM bank #5\n");
    }
    if (snap->hdr.chMachineId != ZXS_MACH_16K) {
      if ((ram = zxs_chunk_findram(snap, 2)) != NULL) {
        memcpy(zxMemoryBanks[2], ram->data, 0x4000);
        zxMemBanksDirty[2] = 1;
      } else {
        cprintf("\4snapload: missing RAM bank #2\n");
      }
      if ((ram = zxs_chunk_findram(snap, 0)) != NULL) {
        memcpy(zxMemoryBanks[0], ram->data, 0x4000);
        zxMemBanksDirty[0] = 1;
      } else {
        cprintf("\4snapload: missing RAM bank #0\n");
      }
    }
    /*
    uint64_t sum = 0;
    for (int f = 0; f < 0x4000; ++f) sum += zxMemoryBanks[5][f];
    for (int f = 0; f < 0x4000; ++f) sum += zxMemoryBanks[2][f];
    for (int f = 0; f < 0x4000; ++f) sum += zxMemoryBanks[0][f];
    cprintf("SUM: %llu\n", sum);
    */
  } else {
    for (int f = 0; f < mmb; ++f) {
      zxs_page_t *ram;
      if ((ram = zxs_chunk_findram(snap, f)) != NULL) {
        memcpy(zxMemoryBanks[f], ram->data, 0x4000);
        zxMemBanksDirty[f] = 1;
      } else {
        //memset(zxMemoryBanks[f], 0, 0x4000);
        cprintf("\4snapload: missing RAM bank #%d\n", f);
      }
    }
  }
  /* setup emulator state */
  emuRealizeMemoryPaging();
  zxOldScreenTS = 0;
  memset(zxUlaSnow, 0, sizeof(zxUlaSnow));
  zxWasUlaSnow = 0;
  zxRealiseScreen(machineInfo.tsperframe);
  zxOldScreenTS = z80.tstates;
  /* betadisk */
  {
    zxs_b128_t *beta = (zxs_b128_t *)zxs_chunk_find(snap, ZXS_B128_SIGN);
    if (beta != NULL) {
      if (beta->dwFlags&ZXS_B128_FLAG_PAGED) zxTRDOSpagein();
    }
  }
  /* done */
  return 0;
}


//==========================================================================
//
//  snapAddRAM
//
//==========================================================================
static int snapAddRAM (zxs_t *snap, int pgno) {
  if (pgno >= 0 && pgno < zxMaxMemoryBank) {
    zxs_page_t pg;
    if (zxs_make_page(&pg, zxMemoryBanks[pgno], pgno) < 0) return 1;
    return (zxs_chunk_replace(snap, (void *)&pg) != NULL ? 0 : -1);
  }
  return -1;
}


//==========================================================================
//
//  snapRemember
//
//==========================================================================
int snapRemember (zxs_t *snap) {
  int mmb = 3; // number of memory banks
  if (snap == NULL) return -1;
  zxs_clear(snap);
  switch (zxModel) {
    case ZX_MACHINE_48K:
      mmb = 3;
      snap->hdr.chMachineId = ZXS_MACH_48K;
      break;
    case ZX_MACHINE_128K:
      mmb = 8;
      snap->hdr.chMachineId = ZXS_MACH_128K;
      break;
    case ZX_MACHINE_PLUS2:
      mmb = 8;
      snap->hdr.chMachineId = ZXS_MACH_PLUS2;
      break;
    case ZX_MACHINE_PLUS2A:
      mmb = 8;
      snap->hdr.chMachineId = ZXS_MACH_PLUS2A;
      break;
    case ZX_MACHINE_PLUS3:
      mmb = 8;
      snap->hdr.chMachineId = ZXS_MACH_PLUS3;
      break;
    case ZX_MACHINE_PENTAGON:
      mmb = zxPentagonMemory/16;
      switch (zxPentagonMemory) {
        case 128: snap->hdr.chMachineId = ZXS_MACH_PENTAGON128; break;
        case 512: snap->hdr.chMachineId = ZXS_MACH_PENTAGON512; break;
        case 1024: snap->hdr.chMachineId = ZXS_MACH_PENTAGON1024; break;
      }
      break;
    case ZX_MACHINE_SCORPION:
      mmb = 256/16;
      snap->hdr.chMachineId = ZXS_MACH_SCORPION;
      break;
    default: goto error;
  }
  snap->hdr.chFlags = (zxLateTimings ? ZXS_MF_ALTERNATE_TIMINGS : 0);
  /* creator */
  {
    zxs_creator_t creator;
    memset(&creator, 0, sizeof(creator));
    memcpy(creator.blk.dwId, ZXS_CREATOR_SIGN, 4);
    creator.blk.dwSize = sizeof(zxs_creator_t)-sizeof(zxs_block_t);
    strcpy(creator.szCreator, "ZXEmuT");
    creator.chMajorVersion = 0;
    creator.chMinorVersion = 0;
    if (zxs_chunk_replace(snap, (void *)&creator) == NULL) goto error;
  }
  /* z80 state */
  {
    zxs_z80regs_t regs;
    memcpy(regs.blk.dwId, ZXS_Z80REGS_SIGN, 4);
    regs.blk.dwSize = sizeof(zxs_z80regs_t)-sizeof(zxs_block_t);
    regs.AF = z80.af.w;
    regs.BC = z80.bc.w;
    regs.DE = z80.de.w;
    regs.HL = z80.hl.w;
    regs.AF1 = z80.afx.w;
    regs.BC1 = z80.bcx.w;
    regs.DE1 = z80.dex.w;
    regs.HL1 = z80.hlx.w;
    regs.IX = z80.ix.w;
    regs.IY = z80.iy.w;
    regs.SP = z80.sp.w;
    regs.PC = z80.pc;
    regs.I = z80.regI;
    regs.R = z80.regR;
    regs.IFF1 = z80.iff1;
    regs.IFF2 = z80.iff2;
    regs.IM = z80.im;
    regs.dwCyclesStart = z80.tstates%machineInfo.tsperframe;
    regs.chHoldIntReqCycles = (z80.tstates < machineInfo.intrlen ? machineInfo.intrlen-z80.tstates : 0);
    regs.chFlags = (z80.prev_was_EIDDR > 0 ? ZXS_Z80REGS_FLAG_EILAST : 0)|(z80.halted ? ZXS_Z80REGS_FLAG_HALTED : 0);
    regs.wMemPtr = z80.memptr.w;
    if (zxs_chunk_replace(snap, (void *)&regs) == NULL) goto error;
  }
  /* ports */
  {
    zxs_zxregs_t ports;
    memcpy(ports.blk.dwId, ZXS_ZXREGS_SIGN, 4);
    ports.blk.dwSize = sizeof(zxs_zxregs_t)-sizeof(zxs_block_t);
    ports.chBorder = zxBorder;
    ports.ch7ffd = zxLastOut7ffd;
    ports.ch1ffd = zxLastOut1ffd;
    //cprintf("PORT #7FFD: #%02X\n", ports.ch7ffd);
    //cprintf("PORT #1FFD: #%02X\n", ports.ch1ffd);
    ports.chFe = zxUlaOut;
    ports.chReserved = 0;
    if (zxs_chunk_replace(snap, (void *)&ports) == NULL) goto error;
  }
  /* keyboard */
  if (snap->hdr.chMachineId <= ZXS_MACH_128K && optZXIssue > 0) {
    zxs_keyb_t keybd;
    memcpy(keybd.blk.dwId, ZXS_KEYB_SIGN, 4);
    keybd.blk.dwSize = sizeof(zxs_keyb_t)-sizeof(zxs_block_t);
    keybd.dwFlags = (snap->hdr.chMachineId <= ZXS_MACH_128K && optZXIssue > 0 ? ZXS_KEYB_FLAG_ISSUE2 : 0);
    keybd.chKeyboardJoystick = ZXS_JOY_NONE; // for now
    if (zxs_chunk_replace(snap, (void *)&keybd) == NULL) goto error;
  }
  /* mouse */
  {
    zxs_mouse_t mouse;
    memset(&mouse, 0, sizeof(mouse));
    memcpy(mouse.blk.dwId, ZXS_MOUSE_SIGN, 4);
    mouse.blk.dwSize = sizeof(zxs_mouse_t)-sizeof(zxs_block_t);
    mouse.chType = (optKMouse ? ZXS_MOUSE_KEMPSTON : ZXS_MOUSE_NONE);
    if (zxs_chunk_replace(snap, (void *)&mouse) == NULL) goto error;
  }
  /* ay */
  if (optAYEnabled) {
    zxs_ayregs_t ay;
    memcpy(ay.blk.dwId, ZXS_AYREGS_SIGN, 4);
    ay.blk.dwSize = sizeof(zxs_ayregs_t)-sizeof(zxs_block_t);
    ay.chFlags = (snap->hdr.chMachineId <= ZXS_MACH_48K ? ZXS_AYREGS_FLAG_128AY : 0);
    ay.chCurrentRegister = zxLastOutFFFD;
    for (int f = 0; f < 16; ++f) ay.chAyRegs[f] = zxAYRegs[f];
    if (zxs_chunk_replace(snap, (void *)&ay) == NULL) goto error;
  }
  /* betadisk */
  //FIXME
#ifdef EMU_LIBFDC
  if (optTRDOSenabled && optTRDOSROMIndex >= 0) {
    zxs_b128_t beta;
    memset(&beta, 0, sizeof(beta));
    memcpy(beta.blk.dwId, ZXS_B128_SIGN, 4);
    FDC *fdc = difGetFDC(zxDiskIf);
    beta.blk.dwSize = sizeof(beta)-sizeof(beta.blk);
    beta.dwFlags = ZXS_B128_FLAG_CONNECTED|(zxTRDOSPagedIn ? ZXS_B128_FLAG_PAGED : 0)|(fdc->step == 0 ? ZXS_B128_FLAG_SEEKLOWER : 0);
    beta.chNumDrives = 4;
    beta.chSysReg = fdc->sysreg;
    beta.chTrackReg = fdc->trk;
    beta.chSectorReg = fdc->sec;
    beta.chDataReg = fdc->data;
    //beta.chStatusReg = fdcBuildStateReg(fdc);
    beta.chStatusReg = fdc->state;
    if (zxs_chunk_replace(snap, (void *)&beta) == NULL) goto error;
  }
#endif
  /* memory */
  if (zxModel == ZX_MACHINE_48K) {
    if (snapAddRAM(snap, 5) != 0) goto error;
    if (snapAddRAM(snap, 2) != 0) goto error;
    if (snapAddRAM(snap, 0) != 0) goto error;
  } else {
    for (int f = 0; f < mmb; ++f) {
      if (snapAddRAM(snap, f) != 0) goto error;
    }
  }
  /* done */
  return 0;
error:
  zxs_clear(snap);
  return -1;
}


//==========================================================================
//
//  szxLoad
//
//==========================================================================
int szxLoad (FILE *fl) {
  zxs_t *snap;
  int res = -1;
  if ((snap = zxs_alloc()) == NULL) return -1; // alas
  if (zxs_snap_load_szx(snap, fl, 0) == 0) {
    if (snapRealize(snap) == 0) res = 0;
  }
  zxs_free(snap);
  return res;
}


//==========================================================================
//
//  szxSave
//
//==========================================================================
int szxSave (FILE *fl) {
  zxs_t *snap;
  int res = -1;
  if ((snap = zxs_alloc()) == NULL) return -1; // alas
  if (snapRemember(snap) == 0) {
    if (zxs_snap_save_szx(fl, snap, 0) == 0) res = 0;
  }
  zxs_free(snap);
  return res;
}


//==========================================================================
//
//  snaLoad
//
//==========================================================================
int snaLoad (FILE *fl) {
  zxs_t *snap;
  int res = -1;
  if ((snap = zxs_alloc()) == NULL) return -1; // alas
  if (zxs_snap_load_sna(snap, fl, 0) == 0) {
    if (snapRealize(snap) == 0) res = 0;
  }
  zxs_free(snap);
  return res;
}


//==========================================================================
//
//  snaSave
//
//==========================================================================
int snaSave (FILE *fl) {
  zxs_t *snap;
  int res = -1;
  if ((snap = zxs_alloc()) == NULL) return -1; // alas
  if (snapRemember(snap) == 0) {
    if (zxs_snap_save_sna(fl, snap, 0) == 0) res = 0;
  }
  zxs_free(snap);
  return res;
}


//==========================================================================
//
//  loadWholeFile
//
//==========================================================================
char *loadWholeFile (const char *fname, size_t *size, const char **newname) {
  if (newname != NULL) *newname = fname;
  if (size) *size = 0;
#ifdef HAVE_ZLIB
  int doGZip = 0;
  const char *ext = strrchr(fname, '.');
  if (ext != NULL && strcasecmp(ext, ".gz") == 0) {
    if (newname == NULL) {
      doGZip = 1;
    } else if (strlen(fname) < 8190) {
      static char newnamebuf[8192];
      strcpy(newnamebuf, fname);
      char *e = strrchr(newnamebuf, '.');
      *e = 0;
      *newname = newnamebuf;
      doGZip = 1;
      //fprintf(stderr, "GZ: [%s]\n", newnamebuf);
    } else {
      return NULL;
    }
  }
  if (doGZip) {
    size_t total = 0;
    size_t curbufsz = 0;
    char *gbuf = NULL;
    gzFile fl = gzopen(fname, "r");
    if (fl == NULL) return NULL;
    //fprintf(stderr, " opened\n");
    for (;;) {
      size_t obp = curbufsz;
      curbufsz += 1024*1024;
      if (curbufsz > 32*1024*1024) { free(gbuf); gzclose(fl); return NULL; }
      char *nb = realloc(gbuf, curbufsz);
      if (nb == NULL) { free(gbuf); gzclose(fl); return NULL; }
      gbuf = nb;
      while (obp < curbufsz) {
        int rd = gzread(fl, gbuf+obp, curbufsz-obp);
        //fprintf(stderr, " %u bytes requested; %d bytes got\n", (unsigned)(curbufsz-obp), rd);
        if (rd < 0) { free(gbuf); gzclose(fl); return NULL; }
        if (rd == 0) {
          //fprintf(stderr, " read: %u\n", (unsigned)total);
          gzclose(fl);
          if (size) *size = total;
          nb = realloc(gbuf, obp+1);
          if (nb == NULL) { free(gbuf); return NULL; }
          gbuf = nb;
          gbuf[obp] = 0;
          return gbuf;
        }
        total += rd;
        obp += rd;
      }
    }
  } else
#endif
  {
    long sz;
    char *buf;
    FILE *fl = fopen(fname, "rb");
    if (fl == NULL) return NULL;
    if (fseek(fl, 0, SEEK_END) != 0) { fclose(fl); return NULL; }
    sz = ftell(fl);
    if (sz < 1 || sz > 1024*1024*32) { fclose(fl); return NULL; }
    if (fseek(fl, 0, SEEK_SET) != 0) { fclose(fl); return NULL; }
    if ((buf = calloc(1, sz+1)) == NULL) { fclose(fl); return NULL; }
    if (fread(buf, sz, 1, fl) != 1) { free(buf); fclose(fl); return NULL; }
    fclose(fl);
    if (size) *size = sz;
    return buf;
  }
}


#ifdef USE_IMLIB2
//==========================================================================
//
//  savePNG
//
//==========================================================================
static void savePNG (const char *fname, int scale) {
  int wdt = 256*scale, w4 = wdt*4;
  Imlib_Image img = imlib_create_image(wdt, 192*scale);
  DATA32 *raw;
  unsigned char *pp;
  imlib_context_set_image(img);
  raw = imlib_image_get_data(); //brga
  pp = (unsigned char *)raw;
  for (int y = 0; y < 192; y++) {
    for (int x = 0; x < 256; x++) {
      unsigned char r, g, b;
      if (optNoFlic > 0 || optNoFlicDetected) {
        Uint8 c0 = zxScreen[zxScreenCurrent^0][(y+48)*352+16+32+x];
        Uint8 c1 = zxScreen[zxScreenCurrent^1][(y+48)*352+16+32+x];
        r = (palRGB[c0][0]+palRGB[c1][0])/2;
        g = (palRGB[c0][1]+palRGB[c1][1])/2;
        b = (palRGB[c0][2]+palRGB[c1][2])/2;
      } else {
        Uint8 c0 = zxScreen[zxScreenCurrent^0][(y+48)*352+16+32+x];
        r = palRGB[c0][0];
        g = palRGB[c0][1];
        b = palRGB[c0][2];
      }
      for (int dy = 0; dy < scale; ++dy) {
        unsigned char *p = pp+dy*w4;
        for (int dx = 0; dx < scale; ++dx) {
          *p++ = b;
          *p++ = g;
          *p++ = r;
          *p++ = 255;
        }
      }
      pp += 4*scale;
    }
    pp += (scale-1)*w4;
  }
  imlib_image_set_format("png");
  imlib_save_image(fname);
  imlib_free_image();
}
#endif


//==========================================================================
//
//  lsFileTypeName
//
//==========================================================================
static EMU_MAYBE_UNUSED const char *lsFileTypeName (libspectrum_id_t tp) {
  switch (tp) {
    case LIBSPECTRUM_ID_CARTRIDGE_DCK: return ".dck Timex dock image";
    case LIBSPECTRUM_ID_CARTRIDGE_IF2: return ".rom Interface II cartridge";

    case LIBSPECTRUM_ID_COMPRESSED_BZ2: return "bzip2 compressed file";
    case LIBSPECTRUM_ID_COMPRESSED_GZ: return "gzip compressed file";

    case LIBSPECTRUM_ID_DISK_DSK: return ".dsk +3 disk file";
    case LIBSPECTRUM_ID_DISK_CPC: return "plain .dsk +3 disk file";
    case LIBSPECTRUM_ID_DISK_ECPC: return "extended .dsk +3 disk file";
    case LIBSPECTRUM_ID_DISK_FDI: return ".fdi generic disk file";
    case LIBSPECTRUM_ID_DISK_SAD: return ".sad generic disk file";
    case LIBSPECTRUM_ID_DISK_SCL: return ".scl TRDOS disk file";
    case LIBSPECTRUM_ID_DISK_TRD: return ".trd TRDOS disk file";
    case LIBSPECTRUM_ID_DISK_TD0: return ".td0 generic disk file";
    case LIBSPECTRUM_ID_DISK_UDI: return ".udi generic disk file";

    case LIBSPECTRUM_ID_HARDDISK_HDF: return ".hdf IDE hard disk image";

    case LIBSPECTRUM_ID_MICRODRIVE_MDR: return ".mdf microdrive cartridge";

    case LIBSPECTRUM_ID_RECORDING_RZX: return ".rzx input recording";

    case LIBSPECTRUM_ID_SNAPSHOT_PLUSD: return "+D snapshot";
    case LIBSPECTRUM_ID_SNAPSHOT_SNA: return ".sna snapshot";
    case LIBSPECTRUM_ID_SNAPSHOT_SNP: return ".snp snapshot";
    case LIBSPECTRUM_ID_SNAPSHOT_SP: return ".sp snapshot";
    case LIBSPECTRUM_ID_SNAPSHOT_SZX: return ".szx snapshot (as used by Spectaculator)";
    case LIBSPECTRUM_ID_SNAPSHOT_Z80: return ".z80 snapshot";
    case LIBSPECTRUM_ID_SNAPSHOT_ZXS: return ".zxs snapshot (as used by zx32)";

    case LIBSPECTRUM_ID_TAPE_CSW: return ".csw tape image";
    case LIBSPECTRUM_ID_TAPE_TAP: return "`normal' (Z80-style) .tap tape image";
    case LIBSPECTRUM_ID_TAPE_TZX: return ".tzx tape image";
    case LIBSPECTRUM_ID_TAPE_WARAJEVO: return "Warajevo-style .tap tape image";
    case LIBSPECTRUM_ID_TAPE_Z80EM: return "Z80Em tape image";
    case LIBSPECTRUM_ID_TAPE_SPC: return "SP-style .spc tape image";
    case LIBSPECTRUM_ID_TAPE_STA: return "Speculator-style .sta tape image";
    case LIBSPECTRUM_ID_TAPE_LTP: return "Nuclear ZX-style .ltp tape image";
    case LIBSPECTRUM_ID_TAPE_PZX: return ".pzx tape image";
    case LIBSPECTRUM_ID_UNKNOWN:
    default: ;
  }
  return "unknown";
}


//==========================================================================
//
//  lsFileClassName
//
//==========================================================================
EMU_MAYBE_UNUSED static const char *lsFileClassName (libspectrum_class_t class) {
  switch (class) {
    case LIBSPECTRUM_CLASS_CARTRIDGE_TIMEX: return "Timex dock cartridge";
    case LIBSPECTRUM_CLASS_CARTRIDGE_IF2: return "Interface II cartridge";
    case LIBSPECTRUM_CLASS_DISK_GENERIC: return "generic disk image";
    case LIBSPECTRUM_CLASS_DISK_PLUSD: return "MGT/+D disk image";
    case LIBSPECTRUM_CLASS_DISK_PLUS3: return "+3 disk image";
    case LIBSPECTRUM_CLASS_DISK_TRDOS: return "TRDOS disk image";
    case LIBSPECTRUM_CLASS_HARDDISK: return "IDE hard disk image";
    case LIBSPECTRUM_CLASS_MICRODRIVE: return "microdrive cartridge";
    case LIBSPECTRUM_CLASS_RECORDING: return "input recording";
    case LIBSPECTRUM_CLASS_SNAPSHOT: return "snapshot";
    case LIBSPECTRUM_CLASS_TAPE: return "tape image";
    case LIBSPECTRUM_CLASS_UNKNOWN:
    default: ;
  }
  return "unknown";
}


//==========================================================================
//
//  lsIsGoodDisk
//
//==========================================================================
static int lsIsGoodDisk (libspectrum_id_t type) {
  switch (type) {
    case LIBSPECTRUM_ID_DISK_SCL:
    case LIBSPECTRUM_ID_DISK_TRD:
    case LIBSPECTRUM_ID_DISK_FDI:
    case LIBSPECTRUM_ID_DISK_UDI:
    case LIBSPECTRUM_ID_DISK_TD0:
    case LIBSPECTRUM_ID_DISK_DSK:
    case LIBSPECTRUM_ID_DISK_CPC:
    case LIBSPECTRUM_ID_DISK_ECPC:
      return 1;
    default: ;
  }
  return 0;
}


//==========================================================================
//
//  addAutoBoot
//
//==========================================================================
int addAutoBoot (int diskid, int forced, int simpleAutorun) {
  if (diskid >= 0 && diskid <= 3 && (optAutoaddBoot || forced)) {
    disk_t *flp = beta_get_disk(diskid);
    if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return -1;
    // check if we have any basic file
    if (!flpHasAnyNonBootBasicNonBootTRD(flp, NULL)) return -1;
    if (simpleAutorun) {
      if (flpSetBootSimpleTRD(flp) == 0) return 0;
    }
    FILE *fl = fmemopen((void *)bootph, sizeof(bootph), "rb");
    if (fl != NULL) {
      //FIXME: check result
      flpSetBootTRD(flp, fl, 0);
      fclose(fl);
      return 0;
    }
  }
  return -1;
}


//==========================================================================
//
//  libspectrum_realize_snapshot
//
//==========================================================================
static int libspectrum_realize_snapshot (libspectrum_snap *snap) {
  if (snap != NULL) {
    libspectrum_byte *pg;
    int do7ffd = 0, do1ffd = 0/*, pmem = zxPentagonMemory*/, mmb = 0;
    int plus3 = 0;
    switch (libspectrum_snap_machine(snap)) {
      case LIBSPECTRUM_MACHINE_16:
      case LIBSPECTRUM_MACHINE_48:
      case LIBSPECTRUM_MACHINE_48_NTSC:
        mmb = 8;
        if (optSnapSetModel) emuSetModel(ZX_MACHINE_48K, 1);
        break;
      case LIBSPECTRUM_MACHINE_128:
        mmb = 8;
        do7ffd = 1;
        if (optSnapSetModel) emuSetModel(ZX_MACHINE_128K, 1);
        break;
      case LIBSPECTRUM_MACHINE_PENT:
        mmb = 8;
        do7ffd = 1;
        zxPentagonMemory = 128;
        if (optSnapSetModel) emuSetModel(ZX_MACHINE_PENTAGON, 1);
        break;
      case LIBSPECTRUM_MACHINE_PENT512:
        mmb = 32;
        do7ffd = 1;
        do1ffd = 1;
        zxPentagonMemory = 512;
        if (optSnapSetModel) emuSetModel(ZX_MACHINE_PENTAGON, 1);
        break;
      case LIBSPECTRUM_MACHINE_PENT1024:
        mmb = 64;
        do7ffd = 1;
        do1ffd = 1;
        zxPentagonMemory = 1024;
        if (optSnapSetModel) emuSetModel(ZX_MACHINE_PENTAGON, 1);
        break;
      case LIBSPECTRUM_MACHINE_SCORP:
        mmb = 16;
        do7ffd = 1;
        do1ffd = 1;
        if (optSnapSetModel) emuSetModel(ZX_MACHINE_SCORPION, 1);
        break;
      case LIBSPECTRUM_MACHINE_PLUS2:
        mmb = 8;
        do7ffd = 1;
        if (optSnapSetModel) emuSetModel(ZX_MACHINE_PLUS2, 1);
        break;
      case LIBSPECTRUM_MACHINE_PLUS2A:
        mmb = 8;
        do7ffd = 1;
        if (optSnapSetModel) emuSetModel(ZX_MACHINE_PLUS2A, 1);
        //plus3 = 1;
        break;
      case LIBSPECTRUM_MACHINE_PLUS3:
        //cprintf("snapload: +3 ignored\n");
        mmb = 8;
        do7ffd = 1;
        if (optSnapSetModel) emuSetModel(ZX_MACHINE_PLUS3, 1);
        //plus3 = 1;
        //goto loadunknown;
        break;
      case LIBSPECTRUM_MACHINE_PLUS3E:
        cprintf("snapload: +3E ignored\n");
        plus3 = 2;
        goto loadunknown;
      case LIBSPECTRUM_MACHINE_SE:
        cprintf("snapload: SE ignored\n");
        plus3 = 2;
  loadunknown:
        if (optAllowOther128) {
          mmb = 8;
          cprintf("snapload: setting standard 128k\n");
          do7ffd = 1;
          if (optSnapSetModel) emuSetModel(ZX_MACHINE_128K, 1);
          break;
        }
      default: goto error;
    }
    emuReset(0);
    switch (plus3) {
      case 1: // +3
        machineInfo.iocontention = 0;
        machineInfo.usePlus3Contention = 1;
        emuBuildContentionTable();
        break;
      case 2: // SE/KE
        machineInfo.contention = 0;
        machineInfo.iocontention = 0;
        break;
    }
    soundSilenceCurFrame();
    z80.af.a = libspectrum_snap_a(snap);
    z80.af.f = libspectrum_snap_f(snap);
    z80.afx.a = libspectrum_snap_a_(snap);
    z80.afx.f = libspectrum_snap_f_(snap);
    z80.bc.w = libspectrum_snap_bc(snap);
    z80.de.w = libspectrum_snap_de(snap);
    z80.hl.w = libspectrum_snap_hl(snap);
    z80.bcx.w = libspectrum_snap_bc_(snap);
    z80.dex.w = libspectrum_snap_de_(snap);
    z80.hlx.w = libspectrum_snap_hl_(snap);
    z80.ix.w = libspectrum_snap_ix(snap);
    z80.iy.w = libspectrum_snap_iy(snap);
    z80.sp.w = libspectrum_snap_sp(snap);
    z80.pc = libspectrum_snap_pc(snap);
    z80.regI = libspectrum_snap_i(snap);
    z80.regR = libspectrum_snap_r(snap);
    z80.iff1 = libspectrum_snap_iff1(snap);
    z80.iff2 = libspectrum_snap_iff2(snap);
    z80.im = libspectrum_snap_im(snap); if (z80.im > 2) z80.im = 1;
    z80.tstates = libspectrum_snap_tstates(snap);
    if (z80.tstates < 0) z80.tstates = 0;
    //z80.tstates %= machineInfo.tsperframe;
    z80.halted = libspectrum_snap_halted(snap);
    if (libspectrum_snap_last_instruction_ei(snap)) z80.prev_was_EIDDR = 1;
    zxBorder = libspectrum_snap_out_ula(snap)&0x07;
    /* ram */
    emuClearRAM();
    for (int f = 0; f < mmb; ++f) {
      if ((pg = libspectrum_snap_pages(snap, f)) != NULL) {
        memcpy(zxMemoryBanks[f], pg, 0x4000);
        zxMemBanksDirty[f] = 1;
      }
    }
    zxLastOut7ffd = (do7ffd ? libspectrum_snap_out_128_memoryport(snap) : 0x10);
    zxLastOut1ffd = (do1ffd ? libspectrum_snap_out_plus3_memoryport(snap) : 0x00);
    /*
    fprintf(stderr, "#7FFD:#%02X:#%02X; #1FFD:#%02X:#%02X\n",
      zxLastOut7ffd, libspectrum_snap_out_128_memoryport(snap),
      zxLastOut1ffd, libspectrum_snap_out_plus3_memoryport(snap));
    */
    //
    zxLastOutFFFD = libspectrum_snap_out_ay_registerport(snap)&0x0f;
    soundAYReset();
    for (int f = 0; f < 16; ++f) {
      uint8_t value = libspectrum_snap_ay_registers(snap, f);
      switch (f) {
        case 1: case 3: case 5: case 13: value &= 0x0f; break;
        case 6: case 8: case 9: case 10: value &= 0x1f; break;
      }
      zxAYRegs[f] = value;
      soundAYWrite(f, value, z80.tstates);
    }
    emuRealizeMemoryPaging();
    if (optTRDOSenabled && optTRDOSROMIndex >= 0 && libspectrum_snap_beta_active(snap)) {
      //FIXME
      zxTRDOSpagein();
      /*
      beta93Do(&zxDiskIf, 0x0c, 0); // reset
      zxTRDOSactivated = 1;
      if (libspectrum_snap_beta_paged(snap)) zxTRDOSpagein();
      //libspectrum_snap_beta_drive_count(snap, 4);
      zxDiskIf.fStepDirect = libspectrum_snap_beta_direction(snap);
      zxDiskIf.fSystem = libspectrum_snap_beta_system(snap);
      zxDiskIf.fRegTrack = libspectrum_snap_beta_track(snap);
      zxDiskIf.fRegSect = libspectrum_snap_beta_sector(snap);
      zxDiskIf.fRegData = libspectrum_snap_beta_data(snap);
      zxDiskIf.fRegStatus = libspectrum_snap_beta_status(snap);
      */
    }
    zxOldScreenTS = 0;
    memset(zxUlaSnow, 0, sizeof(zxUlaSnow));
    zxWasUlaSnow = 0;
    zxRealiseScreen(machineInfo.tsperframe);
    zxOldScreenTS = z80.tstates;
    return 0;
  }
error:
  return -1;
}


//==========================================================================
//
//  lssnap_rzx_stop
//
//==========================================================================
int lssnap_rzx_stop (void) {
  if (zxRZX != NULL) {
    libspectrum_rzx_free(zxRZX);
    zxRZX = NULL;
    zxSLT = NULL;
    zxRZXFrames = 0;
    zxRZXCurFrame = 0;
  }
  return 0;
}


//==========================================================================
//
//  lssnap_slt_clear
//
//==========================================================================
int lssnap_slt_clear (void) {
  if (zxSLT != NULL) { libspectrum_snap_free(zxSLT); zxSLT = NULL; }
  return 0;
}


//==========================================================================
//
//  lssnap_rzx_next_frame
//
//==========================================================================
int lssnap_rzx_next_frame (void) {
  if (zxRZX != NULL) {
    //fprintf(stderr, "RZX: next frame!\n");
    libspectrum_snap *snap = NULL;
    int done = 0;
    if (libspectrum_rzx_playback_frame(zxRZX, &done, &snap) != LIBSPECTRUM_ERROR_NONE) {
      cprintf("error playing RZX!\n");
      conMessage("error playing RZX!");
      lssnap_rzx_stop();
      lssnap_slt_clear();
      return -1;
    }
    if (done) {
      cprintf("finished playing RZX!\n");
      conMessage("finished playing RZX!");
      lssnap_rzx_stop();
      lssnap_slt_clear();
      return 1;
    }
    if (snap) libspectrum_realize_snapshot(snap); // do we need to free snap?
    zxRZXRCount = libspectrum_rzx_instructions(zxRZX);
    if (zxRZXRCount < 1) {
      // interrupt retriggering, whoa!
      cprintf("RZX: interrupt retrigger!\n");
      fprintf(stderr, "RZX: interrupt retrigger!\n");
      zxRZXRCount = 0; //-42;
    }
    if (++zxRZXCurFrame > zxRZXFrames) {
      cprintf("RZX: too many frames!\n");
      fprintf(stderr, "RZX: too many frames!\n");
    }
    return 0;
  }
  return -1;
}


//==========================================================================
//
//  lssnap_rzx_first_frame
//
//==========================================================================
int lssnap_rzx_first_frame (void) {
  if (zxRZX != NULL) {
    zxRZXRCount = libspectrum_rzx_instructions(zxRZX);
    return 0;
  }
  return -1;
}


//==========================================================================
//
//  loadSnapshot
//
//==========================================================================
int loadSnapshot (const char *name, int flags) {
  size_t size;
  char *buf;
  libspectrum_id_t type;
  libspectrum_class_t class;
  libspectrum_snap *snap = NULL;
  libspectrum_rzx *rzx = NULL;
  int wasZIP = 0;
  int diskid = 4;
  zxs_snap_type snapType = ZXS_SNAP_TYPE_UNKNOWN;
  if (isZIP(name)) {
#ifdef USE_LIBZIP
    if ((diskid = zipNameSplit(name, NULL, NULL)) < 0) diskid = 4;
    buf = loadZIPWholeFile(name, &size, &name);
    wasZIP = 1;
#else
    cprintf("\4ERROR: no ZIP support was compiled in ('%s')\n", name);
    return -1;
#endif
  } else {
    buf = loadWholeFile(name, &size, &name);
  }
  //fprintf(stderr, "name=[%s]; size=%d; buf=%p\n", name, size, buf);
  if (buf == NULL || size < 1) return -1;
  lssnap_rzx_stop();
  lssnap_slt_clear();
  if (wasZIP) cprintf("ZIP: found '%s'\n", name);
  {
    FILE *fl = fmemopen((void *)buf, size, "rb");
    if (fl != NULL) {
      switch ((snapType = zxs_snap_guess(fl, name))) {
        case ZXS_SNAP_TYPE_UNKNOWN: /*cprintf("libzxsnap: unknown\n");*/ break;
        case ZXS_SNAP_TYPE_SZX: cprintf("libzxsnap: SZX\n"); break;
        case ZXS_SNAP_TYPE_SNA: cprintf("libzxsnap: SNA\n"); break;
        case ZXS_SNAP_TYPE_Z80: cprintf("libzxsnap: Z80\n"); break;
      }
      fclose(fl);
    }
  }
  if ((flags&SNAPLOAD_SNAPS) && snapType == ZXS_SNAP_TYPE_SZX) {
    // szx
    FILE *fl = fmemopen((void *)buf, size, "rb");
    int res = -1;
    if (fl != NULL) {
      res = szxLoad(fl);
      fclose(fl);
    }
    free(buf);
    return res;
  }
  if ((flags&SNAPLOAD_SNAPS) && snapType == ZXS_SNAP_TYPE_SNA) {
    // szx
    FILE *fl = fmemopen((void *)buf, size, "rb");
    int res = -1;
    if (fl != NULL) {
      res = snaLoad(fl);
      fclose(fl);
    }
    free(buf);
    return res;
  }
  if (size >= 10 && memcmp(buf, "DMB1", 4) == 0) {
    // DMB
    if (flags&SNAPLOAD_SNAPS) {
      const uint8_t *b = (const uint8_t *)buf;
      uint16_t pc = b[4]|(b[5]<<8);
      int clr = b[6]|(b[7]<<8);
      int cnt = b[8]|(b[9]<<8);
      int lastbank = -1;
      b += 10;
      size -= 10;
      while (cnt-- > 0) {
        uint16_t sz, addr;
        if (size < 4) { free(buf); return -1; }
        sz = b[0]|(b[1]<<8);
        addr = b[2]|(b[3]<<8);
        if (addr == 0) {
          // banked
          if (size < 7) { free(buf); return -1; }
          addr = b[4]|(b[5]<<8);
          if (b[6] != 0xff) lastbank = b[6]; else if (lastbank == -1) lastbank = 0;
          size -= 7;
          b += 7;
          cprintf("ADDR: [%u]#%04X; SIZE=%u\n", lastbank, addr, size);
        } else {
          size -= 4;
          b += 4;
          cprintf("ADDR: #%04X; SIZE=%u\n", addr, size);
        }
        if ((size -= sz) < 0) { free(buf); return -1; }
        while (sz-- > 0) {
          if (lastbank == -1) {
            z80.mem_write(&z80, addr, *b, ZYM_MEMIO_OTHER);
          } else if (lastbank < zxMaxMemoryBank && addr < 0x4000) {
            zxMemoryBanks[lastbank][addr] = *b;
            zxMemBanksDirty[lastbank] = 1;
          }
          ++b;
          addr = (addr+1)&0xffff;
        }
      }
      //free(buf);
      //fprintf(stderr, "clr=%d\n", clr);
      cprintf("CLEAR %d\n", clr);
      cprintf("USR %d\n", pc);
#if 0
      if (clr >= 0x4000) {
        // do 'clear'
        int odbg = debuggerActive;
        int omax = optMaxSpeed;
        int done = 0;
        //FIXME: restore rom
        if (zxModel != ZX_MACHINE_48K) {
          // page in BASIC ROM
          //zxPageROM(1);
          zx7ffdLocked = 0;
          zxLastOut7ffd = 0x10;
          zxLastOut1ffd = 0;
          emuRealizeMemoryPaging();
        }
        z80.pc = 0x1EAF;
        z80.bc.w = clr;
        //debuggerActive = -1;
        emuSetMaxSpeed(1);
        while (!done && z80.pc <= 0x3d00) {
          fprintf(stderr, "PC: #%04X\n", z80.pc);
          /*
          switch (z80.mem_read(&z80, z80.pc, ZYM_MEMIO_OTHER)) {
            case 0xcf: // rst #8
            case 0xe9: // jp (hl)
              done = 1;
              break;
          }
          */
          if (z80.pc == 0x1eda || z80.pc == 0x1eec) break;
          z80Step(1);
        }
        debuggerActive = odbg;
        emuSetMaxSpeed(omax);
      }
#endif
      //if (pc) z80.pc = pc;
      free(buf);
      return 0;
    }
    free(buf);
    return -1;
  }
  if ((flags&SNAPLOAD_OTHER) && (size == 6144 || size == 6912) && (isExtEqu(name, ".scr") || isExtEqu(name, ".scb"))) {
    memcpy(zxScreenBank, buf, size);
    if (size < 6912) memset(zxScreenBank+6144, 7, 32*24);
    emuSetPaused(1);
    free(buf);
    zxOldScreenTS = 0;
    memset(zxUlaSnow, 0, sizeof(zxUlaSnow));
    zxWasUlaSnow = 0;
    zxRealiseScreen(machineInfo.tsperframe);
    zxOldScreenTS = z80.tstates;
    return 0;
  }
  if ((flags&SNAPLOAD_OTHER) && isExtEqu(name, ".rzx")) {
    lssnap_rzx_stop();
    lssnap_slt_clear();
    rzx = libspectrum_rzx_alloc();
    if (libspectrum_rzx_read(rzx, (void *)buf, size) != LIBSPECTRUM_ERROR_NONE) { cprintf("invalid RZX!\n"); goto error; }
    {
      zxRZXFrames = 0;
      zxRZXCurFrame = 0;
      libspectrum_rzx_iterator it = libspectrum_rzx_iterator_begin(rzx);
      do {
        if (libspectrum_rzx_iterator_get_type(it) == LIBSPECTRUM_RZX_INPUT_BLOCK) {
          size_t frm = libspectrum_rzx_iterator_get_frames(it);
          //fprintf(stderr, "frm=%u\n", frm);
          zxRZXFrames += (int)frm;
        }
      } while ((it = libspectrum_rzx_iterator_next(it)) != NULL);
      cprintf("%d frames, ~%d seconds\n", zxRZXFrames, zxRZXFrames/50);
    }
    if (libspectrum_rzx_start_playback(rzx, 0, &snap) != LIBSPECTRUM_ERROR_NONE) { cprintf("can't start RZX playback!\n"); goto error; }
    if (snap == NULL) { cprintf("invalid RZX: no initial snapshot found!\n"); goto error; }
    libspectrum_realize_snapshot(snap);
    z80.tstates = libspectrum_rzx_tstates(rzx);
    if (z80.tstates < 0) z80.tstates = 0;
    z80.tstates %= machineInfo.tsperframe;
    zxRZXRCount = -666;//indicate first frame libspectrum_rzx_instructions(rzx);
    //fprintf(stderr, "RZX FIRST FRAME; rcount=%d\n", zxRZXRCount);
    free(buf);
    zxRZX = rzx;
    return 0;
  }

  if ((flags&SNAPLOAD_DISKS) && flpIsHoBetaBuf(buf, size)) {
    // ok, it seems to be a good HoBeta file
    int res = -1;
    FILE *fl = fmemopen((void *)buf, size, "rb");
    if (fl != NULL) {
      //emuSetFDCType(DIF_BDI);
      if (diskid < 0 || diskid > 3) diskid = (flags>>SNAPLOAD_FLPNOSHIFT)&0x03;
      disk_t *flp = beta_get_disk(diskid);
      if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) {
        flpFormatTRD(flp);
      }
      if (flpLoadHoBeta(flp, fl) != FLPERR_OK) {
        flpFormatTRD(flp);
        flp->dirty = 0;
      } else {
        /*FIXME: move boot to bottom*/
        snapWasDisk |= (1<<diskid);
        addAutoBoot(diskid, 0, 1);
        flp->dirty = 0;
        res = 0;
      }
      fclose(fl);
      emuDiskInsert(diskid);
    }
    free(buf);
    return res;
  }

  if (libspectrum_identify_file_raw(&type, name, (void *)buf, size) != LIBSPECTRUM_ERROR_NONE ||
      libspectrum_identify_class(&class, type) != LIBSPECTRUM_ERROR_NONE)
  {
    free(buf);
    return -1;
  }
  #if 0
  cprintf("=========================\n");
  cprintf("name : [%s]\n", name);
  cprintf("type : %s\n", lsFileTypeName(type));
  cprintf("class: %s\n", lsFileClassName(class));
  cprintf("-------------------------\n");
  #endif
  if ((flags&SNAPLOAD_TYPEMASK) != SNAPLOAD_ANY) {
    int ok = 0;
         if ((flags&SNAPLOAD_TAPES) && class == LIBSPECTRUM_CLASS_TAPE) ok = 1;
    else if ((flags&SNAPLOAD_DISKS) && lsIsGoodDisk(type)) ok = 1;
    else if ((flags&SNAPLOAD_SNAPS) && class == LIBSPECTRUM_CLASS_SNAPSHOT) ok = 1;
    if (!ok) {
      free(buf);
      return -1;
    }
  }

  //cprintf("+++type : %s (%d)\n", lsFileTypeName(type), lsIsGoodDisk(type));
  if (lsIsGoodDisk(type)) {
    int res = -1;
    if (diskid < 0 || diskid > 3) diskid = (flags>>SNAPLOAD_FLPNOSHIFT)&0x03;
    int cpc = 0;
    switch (type) {
      case LIBSPECTRUM_ID_DISK_TRD: res = emuDiskInsertWithReader(diskid, buf, size, &disk_open_buf_trd); break;
      case LIBSPECTRUM_ID_DISK_SCL: res = emuDiskInsertWithReader(diskid, buf, size, &disk_open_buf_scl); break;
      case LIBSPECTRUM_ID_DISK_FDI: res = emuDiskInsertWithReader(diskid, buf, size, &disk_open_buf_fdi); break;
      case LIBSPECTRUM_ID_DISK_UDI: res = emuDiskInsertWithReader(diskid, buf, size, &disk_open_buf_udi); break;
      case LIBSPECTRUM_ID_DISK_TD0: res = emuDiskInsertWithReader(diskid, buf, size, &disk_open_buf_td0); break;
      case LIBSPECTRUM_ID_DISK_DSK:
      case LIBSPECTRUM_ID_DISK_CPC:
      case LIBSPECTRUM_ID_DISK_ECPC:
        cpc = 1;
        res = emuDiskInsertWithReader(diskid, buf, size, &disk_open_buf_dsk);
        break;
      default: ;
    }
    free(buf);
    if (res == 0) {
      snapWasDisk |= (1<<diskid);
      if (cpc) snapWasCPCDisk |= (1<<diskid); else addAutoBoot(diskid, 0, 1);
    }
    return res;
  }

  if (class != LIBSPECTRUM_CLASS_SNAPSHOT) {
    if (class == LIBSPECTRUM_CLASS_TAPE) {
      int res;
      //if (zxSLT != NULL) { libspectrum_snap_free(zxSLT); zxSLT = NULL; }
      res = emuInsertTapeFromBuf(name, buf, size);
      free(buf);
      snapWasTape = 1;
      return res;
    }
    free(buf);
    return -1;
  }

  if ((snap = libspectrum_snap_alloc()) == NULL) { free(buf); return -1; }
  if (libspectrum_snap_read(snap, (void *)buf, size, type, name) != LIBSPECTRUM_ERROR_NONE) goto error;
  //if (zxSLT != NULL) { libspectrum_snap_free(zxSLT); zxSLT = NULL; }
  libspectrum_realize_snapshot(snap);
  zxSLT = snap;
  //if (zxSLT != snap) libspectrum_snap_free(snap);
  free(buf);
  return 0;
error:
  if (zxSLT == snap) zxSLT = NULL;
  if (zxRZX != NULL) { libspectrum_rzx_free(zxRZX); zxRZX = NULL; zxSLT = NULL; }
  else if (snap != NULL) libspectrum_snap_free(snap);
  if (rzx != NULL) libspectrum_rzx_free(rzx);
  free(buf);
  return -1;
}


//==========================================================================
//
//  saveSnapshot
//
//  TODO: save .slt snapshots
//
//==========================================================================
int saveSnapshot (const char *name) {
  int res = -1;
  libspectrum_id_t type = LIBSPECTRUM_ID_SNAPSHOT_Z80;
  libspectrum_snap *snap;
  libspectrum_creator *creator;
  libspectrum_byte *buf = NULL;
  size_t size = 0;
  int outflags = 0, mmb = 0;
  if (isExtEqu(name, ".sna")) type = LIBSPECTRUM_ID_SNAPSHOT_SNA;
  else if (isExtEqu(name, ".z80")) type = LIBSPECTRUM_ID_SNAPSHOT_Z80;
  else if (isExtEqu(name, ".szx")) {
    //type = LIBSPECTRUM_ID_SNAPSHOT_SZX;
    FILE *fo = fopen(name, "wb");
    //
    if (fo == NULL) return -1;
    if (szxSave(fo) != 0) {
      fclose(fo);
      unlink(name);
      return -1;
    }
    fclose(fo);
    return 0;
  } else if (isExtEqu(name, ".sna")) {
    FILE *fo = fopen(name, "wb");
    if (fo == NULL) return -1;
    if (snaSave(fo) != 0) {
      fclose(fo);
      unlink(name);
      return -1;
    }
    fclose(fo);
    return 0;
  } else if (isExtEqu(name, ".scr") || isExtEqu(name, ".scb")) {
    // save screen
    FILE *fo = fopen(name, "wb");
    if (fo == NULL) return -1;
    if (fwrite(zxScreenBank, (isExtEqu(name, ".scr") ? 6912 : 6144), 1, fo) != 1) {
      fclose(fo);
      unlink(name);
      return -1;
    }
    fclose(fo);
    return 0;
#ifdef USE_IMLIB2
  } else if (isExtEqu(name, ".png")) {
    savePNG(name, 2);
    return 0;
#endif
  }
  if ((snap = libspectrum_snap_alloc()) == NULL) return -1;
  if ((creator = libspectrum_creator_alloc()) == NULL) { libspectrum_snap_free(snap); return -1; }
  libspectrum_creator_set_program(creator, "ZXEmuT");
  libspectrum_creator_set_major(creator, 0);
  libspectrum_creator_set_minor(creator, 0);
  switch (zxModel) {
    case ZX_MACHINE_48K:
      mmb = 8;
      libspectrum_snap_set_machine(snap, LIBSPECTRUM_MACHINE_48);
      break;
    case ZX_MACHINE_128K:
      mmb = 8;
      libspectrum_snap_set_machine(snap, LIBSPECTRUM_MACHINE_128);
      libspectrum_snap_set_out_128_memoryport(snap, zxLastOut7ffd);
      break;
    case ZX_MACHINE_PLUS2:
      mmb = 8;
      libspectrum_snap_set_machine(snap, LIBSPECTRUM_MACHINE_PLUS2);
      libspectrum_snap_set_out_128_memoryport(snap, zxLastOut7ffd);
      break;
    case ZX_MACHINE_PLUS2A:
      mmb = 8;
      libspectrum_snap_set_machine(snap, LIBSPECTRUM_MACHINE_PLUS2A);
      libspectrum_snap_set_out_128_memoryport(snap, zxLastOut7ffd);
      libspectrum_snap_set_out_plus3_memoryport(snap, zxLastOut1ffd);
      break;
    case ZX_MACHINE_PLUS3:
      mmb = 8;
      libspectrum_snap_set_machine(snap, LIBSPECTRUM_MACHINE_PLUS3);
      libspectrum_snap_set_out_128_memoryport(snap, zxLastOut7ffd);
      libspectrum_snap_set_out_plus3_memoryport(snap, zxLastOut1ffd);
      //cprintf("PORT #7FFD: #%02X\n", zxLastOut7ffd);
      //cprintf("PORT #1FFD: #%02X\n", zxLastOut1ffd);
      break;
    case ZX_MACHINE_PENTAGON:
      mmb = zxPentagonMemory/16;
      switch (zxPentagonMemory) {
        case 128: libspectrum_snap_set_machine(snap, LIBSPECTRUM_MACHINE_PENT); break;
        case 512: libspectrum_snap_set_machine(snap, LIBSPECTRUM_MACHINE_PENT512); break;
        case 1024: libspectrum_snap_set_machine(snap, LIBSPECTRUM_MACHINE_PENT1024); break;
      }
      libspectrum_snap_set_out_128_memoryport(snap, zxLastOut7ffd);
      libspectrum_snap_set_out_plus3_memoryport(snap, zxLastOut1ffd);
      break;
    case ZX_MACHINE_SCORPION:
      mmb = 256/16;
      libspectrum_snap_set_machine(snap, LIBSPECTRUM_MACHINE_SCORP);
      libspectrum_snap_set_out_128_memoryport(snap, zxLastOut7ffd);
      libspectrum_snap_set_out_plus3_memoryport(snap, zxLastOut1ffd);
      break;
  }
  libspectrum_snap_set_a(snap, z80.af.a);
  libspectrum_snap_set_f(snap, z80.af.f);
  libspectrum_snap_set_a_(snap, z80.afx.a);
  libspectrum_snap_set_f_(snap, z80.afx.f);
  libspectrum_snap_set_bc(snap, z80.bc.w);
  libspectrum_snap_set_de(snap, z80.de.w);
  libspectrum_snap_set_hl(snap, z80.hl.w);
  libspectrum_snap_set_bc_(snap, z80.bcx.w);
  libspectrum_snap_set_de_(snap, z80.dex.w);
  libspectrum_snap_set_hl_(snap, z80.hlx.w);
  libspectrum_snap_set_ix(snap, z80.ix.w);
  libspectrum_snap_set_iy(snap, z80.iy.w);
  libspectrum_snap_set_sp(snap, z80.sp.w);
  libspectrum_snap_set_pc(snap, z80.pc);
  libspectrum_snap_set_i(snap, z80.regI);
  libspectrum_snap_set_r(snap, z80.regR);
  libspectrum_snap_set_iff1(snap, z80.iff1);
  libspectrum_snap_set_iff2(snap, z80.iff2);
  libspectrum_snap_set_im(snap, z80.im);
  libspectrum_snap_set_tstates(snap, z80.tstates%machineInfo.tsperframe);
  libspectrum_snap_set_halted(snap, z80.halted);
  if (z80.prev_was_EIDDR > 0) libspectrum_snap_set_last_instruction_ei(snap, 1);
  libspectrum_snap_set_out_ula(snap, zxBorder);
  libspectrum_snap_set_joystick_active_count(snap, 1);
  libspectrum_snap_set_joystick_list(snap, 0, LIBSPECTRUM_JOYSTICK_KEMPSTON);
  libspectrum_snap_set_joystick_inputs(snap, 0, LIBSPECTRUM_JOYSTICK_INPUT_KEYBOARD);
  libspectrum_snap_set_beta_active(snap, zxTRDOSactivated);
  #ifdef EMU_LIBFDC
  if (zxTRDOSactivated) {
    libspectrum_snap_set_beta_paged(snap, zxTRDOSPagedIn);
    libspectrum_snap_set_beta_drive_count(snap, 4);
    //???
    FDC *fdc = difGetFDC(zxDiskIf);
    libspectrum_snap_set_beta_direction(snap, fdc->step);
    libspectrum_snap_set_beta_system(snap, fdc->sysreg);
    libspectrum_snap_set_beta_track(snap, fdc->trk);
    libspectrum_snap_set_beta_sector(snap, fdc->sec);
    libspectrum_snap_set_beta_data(snap, fdc->data);
    libspectrum_snap_set_beta_status(snap, fdc->state);
  }
  #endif
  if (zxModel == ZX_MACHINE_48K) {
    libspectrum_snap_set_pages(snap, 5, zxMemoryBanks[5]);
    libspectrum_snap_set_pages(snap, 2, zxMemoryBanks[2]);
    libspectrum_snap_set_pages(snap, 0, zxMemoryBanks[0]);
  } else {
    for (int f = 0; f < mmb; ++f) libspectrum_snap_set_pages(snap, f, zxMemoryBanks[f]);
  }
  libspectrum_snap_set_out_ay_registerport(snap, zxLastOutFFFD);
  for (int f = 0; f < 16; ++f) libspectrum_snap_set_ay_registers(snap, f, zxAYRegs[f]);

  if (libspectrum_snap_write(&buf, &size, &outflags, snap, type, creator, 0) == LIBSPECTRUM_ERROR_NONE) {
    FILE *fo = fopen(name, "wb");
    if (fo != NULL) {
      if (fwrite(buf, size, 1, fo) == 1) res = 0;
      fclose(fo);
      if (res != 0) unlink(name);
    }
  }

  if (zxModel == ZX_MACHINE_48K) {
    libspectrum_snap_set_pages(snap, 5, NULL);
    libspectrum_snap_set_pages(snap, 2, NULL);
    libspectrum_snap_set_pages(snap, 0, NULL);
  } else {
    for (int f = 0; f < mmb; ++f) libspectrum_snap_set_pages(snap, f, NULL);
  }
  if (buf != NULL) libspectrum_free(buf);
  libspectrum_creator_free(creator);
  libspectrum_snap_free(snap);
  return res;
}
