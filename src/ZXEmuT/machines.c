/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include "machines.h"


// WARNING! keep in sync with `ZX_MACHINE_XXX` enum in "emuvars.h"!
const ZXBasicMachineInfo basicMachineInfo[ZX_MACHINE_MAX] = {
 // 48k
 { // 224 per line, 69888 per frame
  .modelname = "48",
  .cpuSpeed = 3500000,
  .aySpeed = 1750000,
  .romPages = 1,
  .leftBorder = 24,
  .hscreen = 128,
  .rightBorder = 24,
  .hretrace = 48,
  .topBorder = 48,
  .vscreen = 192,
  .bottomBorder = 48,
  .vretrace = 24,
  .topleftpixts = 14336,
  .intrlen = 32,
  .port7ffd = -1,
  .port1ffd = -1,
  .evenM1 = 0,
  .haveSnow = 1,
  .floatingBus = 1,
  .contention = 1,
  .iocontention = 1,
  .genuine = 1,
  .usePlus3Contention = 0,
  .p3dos = 0,
  .port7ffDbug = 0,
 },
 // 128k
 { // 228 per line, 70908 per frame
  .modelname = "128",
  .cpuSpeed = 3546900,
  .aySpeed = 1773400,
  .romPages = 2,
  .leftBorder = 24,
  .hscreen = 128,
  .rightBorder = 24,
  .hretrace = 52,
  .topBorder = 48,
  .vscreen = 192,
  .bottomBorder = 48,
  .vretrace = 23,
  .topleftpixts = 14362,
  .intrlen = 36,
  .port7ffd = 0x00,
  .port1ffd = -1,
  .evenM1 = 0,
  .haveSnow = 1,
  .floatingBus = 1,
  .contention = 1,
  .iocontention = 1,
  .genuine = 1,
  .usePlus3Contention = 0,
  .p3dos = 0,
  .port7ffDbug = 1,
 },
 // plus2
 { // 228 per line, 70908 per frame
  .modelname = "plus2",
  .cpuSpeed = 3546900,
  .aySpeed = 1773400,
  .romPages = 2,
  .leftBorder = 24,
  .hscreen = 128,
  .rightBorder = 24,
  .hretrace = 52,
  .topBorder = 48,
  .vscreen = 192,
  .bottomBorder = 48,
  .vretrace = 23,
  .topleftpixts = 14362,
  .intrlen = 36,
  .port7ffd = 0x00,
  .port1ffd = -1,
  .evenM1 = 0,
  .haveSnow = 1,
  .floatingBus = 1,
  .contention = 1,
  .iocontention = 1,
  .genuine = 1,
  .usePlus3Contention = 0,
  .p3dos = 0,
  .port7ffDbug = 0,
 },
 // plus2a
 { // 228 per line, 70908 per frame
  .modelname = "plus2a",
  .cpuSpeed = 3546900,
  .aySpeed = 1773400,
  .romPages = 4,
  .leftBorder = 24,
  .hscreen = 128,
  .rightBorder = 24,
  .hretrace = 52,
  .topBorder = 48,
  .vscreen = 192,
  .bottomBorder = 48,
  .vretrace = 23,
  //.topleftpixts = 14365,
  .topleftpixts = 14362,
  .intrlen = 32,
  .port7ffd = 0x00,
  .port1ffd = 0,
  .evenM1 = 0,
  .haveSnow = 0,
  .floatingBus = 2,
  .contention = 1,
  .iocontention = 0,
  .genuine = 1,
  .usePlus3Contention = 1,
  .p3dos = 1,
  .port7ffDbug = 0,
 },
 // plus3
 { // 228 per line, 70908 per frame
  .modelname = "plus3",
  .cpuSpeed = 3546900,
  .aySpeed = 1773400,
  .romPages = 4,
  .leftBorder = 24,
  .hscreen = 128,
  .rightBorder = 24,
  .hretrace = 52,
  .topBorder = 48,
  .vscreen = 192,
  .bottomBorder = 48,
  .vretrace = 23,
  //.topleftpixts = 14365,
  .topleftpixts = 14362,
  .intrlen = 32,
  .port7ffd = 0x00,
  .port1ffd = 0,
  .evenM1 = 0,
  .haveSnow = 0,
  .floatingBus = 2,
  .contention = 1,
  .iocontention = 0,
  .genuine = 1,
  .usePlus3Contention = 1,
  .p3dos = 1,
  .port7ffDbug = 0,
 },
 // Pentagon
 { // 228 per line, 71680 per frame
  .modelname = "pentagon",
  .cpuSpeed = 3584000,
  .aySpeed = 1792000,
  .romPages = 2,
  .leftBorder = 36,
  .hscreen = 128,
  .rightBorder = 28,
  .hretrace = 32,
  .topBorder = 64,
  .vscreen = 192,
  .bottomBorder = 48,
  .vretrace = 16,
  .topleftpixts = 17988,
  .intrlen = 36,
  .port7ffd = 0x00,
  .port1ffd = -1,
  .evenM1 = 0,
  .haveSnow = 0,
  .floatingBus = 0,
  .contention = 0,
  .iocontention = 0,
  .genuine = 0,
  .usePlus3Contention = 0,
  .p3dos = 0,
  .port7ffDbug = 0,
 },
 // Scorpion
 { // 228 per line, 69888 per frame
  .modelname = "scorpion",
  .cpuSpeed = 3500000,
  .aySpeed = 1750000,
  .romPages = 3, // page 4: TR-DOS
  .leftBorder = 24,
  .hscreen = 128,
  .rightBorder = 32,
  .hretrace = 40,
  .topBorder = 48,
  .vscreen = 192,
  .bottomBorder = 48,
  .vretrace = 24,
  .topleftpixts = 14336,
  .intrlen = 36,
  .port7ffd = 0x00,
  .port1ffd = 0,
  .evenM1 = 1,
  .haveSnow = 0,
  .floatingBus = 0,
  .contention = 0,
  .iocontention = 0,
  .genuine = 0,
  .usePlus3Contention = 0,
  .p3dos = 0,
  .port7ffDbug = 0,
 },
};


void fillMachineInfo (ZXMachineInfo *mi, const ZXBasicMachineInfo *bmi) {
  mi->modelname = bmi->modelname;
  mi->cpuSpeed = bmi->cpuSpeed;
  mi->aySpeed = bmi->aySpeed;
  mi->romPages = bmi->romPages;
  mi->leftBorder = bmi->leftBorder;
  mi->hscreen = bmi->hscreen;
  mi->rightBorder = bmi->rightBorder;
  mi->hretrace = bmi->hretrace;
  mi->topBorder = bmi->topBorder;
  mi->vscreen = bmi->vscreen;
  mi->bottomBorder = bmi->bottomBorder;
  mi->vretrace = bmi->vretrace;
  mi->topleftpixts = bmi->topleftpixts;
  mi->intrlen = bmi->intrlen;
  mi->port7ffd = bmi->port7ffd;
  mi->port1ffd = bmi->port1ffd;
  mi->evenM1 = bmi->evenM1;
  mi->haveSnow = bmi->haveSnow;
  mi->floatingBus = bmi->floatingBus;
  mi->contention = mi->origContention = bmi->contention;
  mi->iocontention = mi->origIOContention = bmi->iocontention;
  mi->genuine = bmi->genuine;
  mi->usePlus3Contention = bmi->usePlus3Contention;
  mi->p3dos = bmi->p3dos;
  mi->port7ffDbug = bmi->port7ffDbug;
  //
  mi->linesperframe = mi->topBorder+mi->vscreen+mi->bottomBorder+mi->vretrace;
  mi->tsperline = mi->leftBorder+mi->hscreen+mi->rightBorder+mi->hretrace;
  mi->tsperframe = mi->tsperline*mi->linesperframe;
  mi->tsdrawstart = mi->topleftpixts-(mi->tsperline*mi->topBorder);
}
