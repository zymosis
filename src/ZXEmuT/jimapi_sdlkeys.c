/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
static const struct {
  uint16_t code;
  const char *name;
} sdlKeyNames[] = {
  {SDLK_BACKSPACE, "BACKSPACE"},
  {SDLK_TAB, "TAB"},
  {SDLK_CLEAR, "CLEAR"},
  {SDLK_RETURN, "RETURN"},
  {SDLK_RETURN, "ENTER"},
  {SDLK_PAUSE, "PAUSE"},
  {SDLK_ESCAPE, "ESCAPE"},
  {SDLK_ESCAPE, "ESC"},
  {SDLK_SPACE, "SPACE"},
  {SDLK_EXCLAIM, "EXCLAIM"},
  {SDLK_EXCLAIM, "!"},
  {SDLK_QUOTEDBL, "QUOTEDBL"},
  {SDLK_QUOTEDBL, "\""},
  {SDLK_HASH, "HASH"},
  {SDLK_HASH, "#"},
  {SDLK_DOLLAR, "DOLLAR"},
  {SDLK_DOLLAR, "$"},
  {SDLK_AMPERSAND, "AMPERSAND"},
  {SDLK_AMPERSAND, "&"},
  {SDLK_QUOTE, "QUOTE"},
  {SDLK_QUOTE, "'"},
  {SDLK_LEFTPAREN, "LEFTPAREN"},
  {SDLK_LEFTPAREN, "("},
  {SDLK_RIGHTPAREN, "RIGHTPAREN"},
  {SDLK_RIGHTPAREN, ")"},
  {SDLK_ASTERISK, "ASTERISK"},
  {SDLK_ASTERISK, "*"},
  {SDLK_PLUS, "PLUS"},
  {SDLK_PLUS, "+"},
  {SDLK_COMMA, "COMMA"},
  {SDLK_COMMA, ","},
  {SDLK_MINUS, "MINUS"},
  {SDLK_MINUS, "-"},
  {SDLK_PERIOD, "PERIOD"},
  {SDLK_PERIOD, "."},
  {SDLK_SLASH, "SLASH"},
  {SDLK_SLASH, "/"},
  {SDLK_0, "0"},
  {SDLK_1, "1"},
  {SDLK_2, "2"},
  {SDLK_3, "3"},
  {SDLK_4, "4"},
  {SDLK_5, "5"},
  {SDLK_6, "6"},
  {SDLK_7, "7"},
  {SDLK_8, "8"},
  {SDLK_9, "9"},
  {SDLK_COLON, "COLON"},
  {SDLK_COLON, "."},
  {SDLK_SEMICOLON, "SEMICOLON"},
  {SDLK_SEMICOLON, ";"},
  {SDLK_LESS, "LESS"},
  {SDLK_LESS, "<"},
  {SDLK_EQUALS, "EQUALS"},
  {SDLK_EQUALS, "="},
  {SDLK_GREATER, "GREATER"},
  {SDLK_GREATER, ">"},
  {SDLK_QUESTION, "QUESTION"},
  {SDLK_QUESTION, "?"},
  {SDLK_AT, "AT"},
  {SDLK_AT, "@"},
  {SDLK_LEFTBRACKET, "LEFTBRACKET"},
  {SDLK_LEFTBRACKET, "["},
  {SDLK_BACKSLASH, "BACKSLASH"},
  {SDLK_BACKSLASH, "\\"},
  {SDLK_RIGHTBRACKET, "RIGHTBRACKET"},
  {SDLK_RIGHTBRACKET, "]"},
  {SDLK_CARET, "CARET"},
  {SDLK_UNDERSCORE, "UNDERSCORE"},
  {SDLK_UNDERSCORE, "_"},
  {SDLK_BACKQUOTE, "BACKQUOTE"},
  {SDLK_BACKQUOTE, "`"},
  {SDLK_BACKQUOTE, "TILDA"},
  {SDLK_BACKQUOTE, "~"},
  {SDLK_a, "a"},
  {SDLK_b, "b"},
  {SDLK_c, "c"},
  {SDLK_d, "d"},
  {SDLK_e, "e"},
  {SDLK_f, "f"},
  {SDLK_g, "g"},
  {SDLK_h, "h"},
  {SDLK_i, "i"},
  {SDLK_j, "j"},
  {SDLK_k, "k"},
  {SDLK_l, "l"},
  {SDLK_m, "m"},
  {SDLK_n, "n"},
  {SDLK_o, "o"},
  {SDLK_p, "p"},
  {SDLK_q, "q"},
  {SDLK_r, "r"},
  {SDLK_s, "s"},
  {SDLK_t, "t"},
  {SDLK_u, "u"},
  {SDLK_v, "v"},
  {SDLK_w, "w"},
  {SDLK_x, "x"},
  {SDLK_y, "y"},
  {SDLK_z, "z"},
  {SDLK_DELETE, "DELETE"},
  {SDLK_DELETE, "DEL"},
  //
  {SDLK_KP0, "KP0"},
  {SDLK_KP1, "KP1"},
  {SDLK_KP2, "KP2"},
  {SDLK_KP3, "KP3"},
  {SDLK_KP4, "KP4"},
  {SDLK_KP5, "KP5"},
  {SDLK_KP6, "KP6"},
  {SDLK_KP7, "KP7"},
  {SDLK_KP8, "KP8"},
  {SDLK_KP9, "KP9"},
  {SDLK_KP_PERIOD, "KP_PERIOD"},
  {SDLK_KP_DIVIDE, "KP_DIVIDE"},
  {SDLK_KP_MULTIPLY, "KP_MULTIPLY"},
  {SDLK_KP_MINUS, "KP_MINUS"},
  {SDLK_KP_PLUS, "KP_PLUS"},
  {SDLK_KP_ENTER, "KP_ENTER"},
  {SDLK_KP_EQUALS, "KP_EQUALS"},
  //
  {SDLK_UP, "UP"},
  {SDLK_DOWN, "DOWN"},
  {SDLK_RIGHT, "RIGHT"},
  {SDLK_LEFT, "LEFT"},
  {SDLK_INSERT, "INSERT"},
  {SDLK_HOME, "HOME"},
  {SDLK_END, "END"},
  {SDLK_PAGEUP, "PAGEUP"},
  {SDLK_PAGEUP, "PGUP"},
  {SDLK_PAGEDOWN, "PAGEDOWN"},
  {SDLK_PAGEDOWN, "PGDOWN"},
  {SDLK_PAGEDOWN, "PGDN"},
  //
  {SDLK_F1, "F1"},
  {SDLK_F2, "F2"},
  {SDLK_F3, "F3"},
  {SDLK_F4, "F4"},
  {SDLK_F5, "F5"},
  {SDLK_F6, "F6"},
  {SDLK_F7, "F7"},
  {SDLK_F8, "F8"},
  {SDLK_F9, "F9"},
  {SDLK_F10, "F10"},
  {SDLK_F11, "F11"},
  {SDLK_F12, "F12"},
  {SDLK_F13, "F13"},
  {SDLK_F14, "F14"},
  {SDLK_F15, "F15"},
  //
  {SDLK_NUMLOCK, "NUMLOCK"},
  {SDLK_CAPSLOCK, "CAPSLOCK"},
  {SDLK_SCROLLOCK, "SCROLLOCK"},
  //
  {SDLK_RSHIFT, "RSHIFT"},
  {SDLK_LSHIFT, "LSHIFT"},
  {SDLK_RCTRL, "RCTRL"},
  {SDLK_LCTRL, "LCTRL"},
  {SDLK_RALT, "RALT"},
  {SDLK_LALT, "LALT"},
  {SDLK_RMETA, "RMETA"},
  {SDLK_LMETA, "LMETA"},
  {SDLK_LSUPER, "LHYPER"},
  {SDLK_RSUPER, "LHYPER"},
  {SDLK_MODE, "MODE"},
  {SDLK_COMPOSE, "COMPOSE"},
  //
  {SDLK_HELP, "HELP"},
  {SDLK_PRINT, "PRINT"},
  {SDLK_SYSREQ, "SYSREQ"},
  {SDLK_BREAK, "BREAK"},
  {SDLK_MENU, "MENU"},
  {SDLK_POWER, "POWER"},
  {SDLK_EURO, "EURO"},
  {SDLK_UNDO, "UNDO"},
  //
  {0, NULL}
};


////////////////////////////////////////////////////////////////////////////////
SDLJimBinding *sdlJimBindings = NULL;


////////////////////////////////////////////////////////////////////////////////
static void zxBindSDLK (uint16_t sdlk, const char *zxkeyname, const char *zxkeyname1) {
  int keyno = zxFindKeyByName(zxkeyname), keyno1 = zxFindKeyByName(zxkeyname1);
  //
  zxKeyBinds[sdlk] = 0;
  if (keyno >= 0 || keyno1 >= 0) {
    if (keyno < 0) { keyno = keyno1; keyno1 = -1; }
    if (keyno >= 0) zxKeyBinds[sdlk] = (zxKeyInfo[keyno].port<<8)|zxKeyInfo[keyno].mask;
    if (keyno1 >= 0) {
      uint32_t n = (zxKeyInfo[keyno1].port<<8)|zxKeyInfo[keyno1].mask;
      //
      zxKeyBinds[sdlk] |= n<<16;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static uint16_t sdlFindKeyCode (const char *name) {
  if (name != NULL && name[0]) {
    for (unsigned f = 0; sdlKeyNames[f].name != NULL; ++f) {
      if (strcasecmp(name, sdlKeyNames[f].name) == 0) return sdlKeyNames[f].code;
    }
  }
  return 0;
}


static const char *sdlFindKeyName (uint16_t code) {
  for (unsigned f = 0; sdlKeyNames[f].name != NULL; ++f) {
    if (sdlKeyNames[f].code == code) return sdlKeyNames[f].name;
  }
  return "UNKNOWN";
}


static const char *sdlBuildEmacsKeyName (SDL_KeyboardEvent *key) {
  static char buf[128];
  if (!key) { buf[0] = 0; return buf; }
  size_t bpos = 0;
  // put modifiers
  if (key->keysym.mod&KMOD_CTRL) { if (bpos) buf[bpos++] = '-'; buf[bpos++] = 'C'; }
  if (key->keysym.mod&KMOD_SHIFT) { if (bpos) buf[bpos++] = '-'; buf[bpos++] = 'S'; }
  if (key->keysym.mod&KMOD_ALT) { if (bpos) buf[bpos++] = '-'; buf[bpos++] = 'M'; }
  if (bpos) buf[bpos++] = '-';
  buf[bpos] = 0;
  const char *kname = sdlFindKeyName(key->keysym.sym);
  if (kname[0] && !kname[1] && kname[0] >= 'a' && kname[0] <= 'z') {
    buf[bpos++] = kname[0]-32;
  } else {
    strcat(buf, kname);
    bpos += strlen(kname);
  }
  buf[bpos] = 0;
  return buf;
}


static SDLJimBinding *sdlFindBinding (SDLJimBinding *list, const SDLJimBinding *bind, SDLJimBinding **pprev) {
  SDLJimBinding *prev = NULL;
  //
  if (pprev) *pprev = NULL;
  for (SDLJimBinding *res = list; res != NULL; res = res->next) {
    if (res->sdlk == bind->sdlk && res->modvalue == bind->modvalue) {
      if (pprev) *pprev = prev;
      return res;
    }
    prev = res;
  }
  return NULL;
}


SDLJimBinding *sdlFindKeyBind (SDLJimBinding *list, uint16_t sdlk, uint16_t modvalue) {
  //fprintf(stderr, "====\n");
  modvalue &= KMOD_CTRL|KMOD_SHIFT|KMOD_ALT|KMOD_META;
  for (SDLJimBinding *res = list; res != NULL; res = res->next) {
    //fprintf(stderr, "BIND:[0x%04x;0x%04x] == [0x%04x;0x%04x]\n", res->sdlk, res->modvalue, sdlk, modvalue);
    if (res->sdlk == sdlk) {
      uint16_t mv = modvalue;
      //
      if ((res->modvalue&KMOD_CTRL) == KMOD_CTRL && (mv&KMOD_CTRL)) mv |= KMOD_CTRL;
      if ((res->modvalue&KMOD_SHIFT) == KMOD_SHIFT && (mv&KMOD_SHIFT)) mv |= KMOD_SHIFT;
      if ((res->modvalue&KMOD_ALT) == KMOD_ALT && (mv&KMOD_ALT)) mv |= KMOD_ALT;
      if ((res->modvalue&KMOD_META) == KMOD_META && (mv&KMOD_META)) mv |= KMOD_META;
      if (res->modvalue == mv) return res;
    }
  }
  return NULL;
}


static void sdlClearBindings (Jim_Interp *interp, SDLJimBinding *list) {
  while (list != NULL) {
    SDLJimBinding *bind = list;
    //
    list = list->next;
    if (bind->action != NULL) Jim_DecrRefCount(interp, bind->action);
    free(bind);
  }
}


// cmdname mods... key action
static SDLJimBinding *sdlParseBinding (int argc, Jim_Obj *const *argv, int hasaction) {
  uint16_t sdlk = 0;
  uint16_t modvalue = 0;
  SDLJimBinding *bind;

  //memset(&bind, 0, sizeof(bind));
  if (argc < 3) return NULL;
  for (int f = 1; f < argc-hasaction; ++f) {
    const char *a0 = Jim_String(argv[f]);
    //fprintf(stderr, "sdlParseBinding: f=%d; [%s]\n", f, a0);
         if (strcasecmp(a0, "ctrl") == 0) modvalue |= KMOD_CTRL;
    else if (strcasecmp(a0, "lctrl") == 0) modvalue |= KMOD_LCTRL;
    else if (strcasecmp(a0, "rctrl") == 0) modvalue |= KMOD_RCTRL;
    else if (strcasecmp(a0, "alt") == 0) modvalue |= KMOD_ALT;
    else if (strcasecmp(a0, "lalt") == 0) modvalue |= KMOD_LALT;
    else if (strcasecmp(a0, "ralt") == 0) modvalue |= KMOD_RALT;
    else if (strcasecmp(a0, "shift") == 0) modvalue |= KMOD_SHIFT;
    else if (strcasecmp(a0, "lshift") == 0) modvalue |= KMOD_LSHIFT;
    else if (strcasecmp(a0, "rshift") == 0) modvalue |= KMOD_RSHIFT;
    else if (strcasecmp(a0, "meta") == 0) modvalue |= KMOD_META;
    else {
      if (sdlk != 0) {
        cprintf("%s: double key name ('%s')\n", Jim_String(argv[0]), a0);
        return NULL;
      }
      if ((sdlk = sdlFindKeyCode(a0)) == 0) {
        cprintf("%s: unknown key name ('%s')\n", Jim_String(argv[0]), a0);
        return NULL;
      }
    }
  }

  if (sdlk == 0) {
    cprintf("%s: no key name\n", Jim_String(argv[0]));
    return NULL;
  }

  if ((bind = malloc(sizeof(*bind))) != NULL) {
    bind->sdlk = sdlk;
    bind->modvalue = modvalue;
    if (hasaction) {
      bind->action = Jim_DuplicateObj(jim, argv[argc-1]);
      Jim_IncrRefCount(bind->action);
    } else {
      bind->action = NULL;
    }
    bind->next = NULL;
  }

  return bind;
}


////////////////////////////////////////////////////////////////////////////////
// zxbind clear
// zxbind sdlkey zxkey [zxkey]
JIMAPI_FN(zxbind) {
  uint16_t sdlk;
  const char *a0, *a1, *a2;

  if (jimapiDisableCompletion(interp, argc, argv)) return JIM_OK;
  if (argc == 2) {
    // this should be "zxbind clear"
    if (strcasecmp("clear", Jim_String(argv[1])) == 0) {
      memset(zxKeyBinds, 0, sizeof(zxKeyBinds));
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
  }

  if (argc < 3 || argc > 4) { Jim_WrongNumArgs(interp, 1, argv, "string"); return JIM_ERR; }
  a0 = Jim_String(argv[1]);
  a1 = Jim_String(argv[2]);
  a2 = (argc > 3 ? Jim_String(argv[3]) : NULL);
  if ((sdlk = sdlFindKeyCode(a0)) == 0) {
    //cprintf("zxbind: unknown pc key: '%s'\n", a0);
    jim_SetResStrf(interp, "%s: unknown pc key: '%s'", Jim_String(argv[0]), a0);
    return JIM_ERR;
  }

  zxBindSDLK(sdlk, a1, a2);
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// zxunbind sdlkey
JIMAPI_FN(zxunbind) {
  uint16_t sdlk;
  const char *a0;
  int l0;
  if (jimapiDisableCompletion(interp, argc, argv)) return JIM_OK;
  if (argc != 2) { Jim_WrongNumArgs(interp, 1, argv, "string"); return JIM_ERR; }
  a0 = Jim_GetString(argv[1], &l0);
  if ((sdlk = sdlFindKeyCode(a0)) == 0) {
    jim_SetResStrf(interp, "%s: unknown pc key: '%s'", Jim_String(argv[0]), a0);
    return JIM_ERR;
  }
  zxBindSDLK(sdlk, NULL, NULL);
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// bind clear
// bind sdlkey string
JIMAPI_FN(bind) {
  SDLJimBinding *bind, *prev, *old;

  if (jimapiDisableCompletion(interp, argc, argv)) return JIM_OK;
  if (argc == 2) {
    if (strcasecmp("clear", Jim_String(argv[1])) == 0) {
      sdlClearBindings(interp, sdlJimBindings);
      sdlJimBindings = NULL;
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
  }

  if (argc < 3) { Jim_WrongNumArgs(interp, 1, argv, "string"); return JIM_ERR; }
  if ((bind = sdlParseBinding(argc, argv, 1)) == NULL) {
    jim_SetResStrf(interp, "%s: can't parse arguments", Jim_String(argv[0]));
    return JIM_ERR;
  }

  // replace old binding if there is any
  if ((old = sdlFindBinding(sdlJimBindings, bind, &prev)) != NULL) {
    if (old->action != NULL) Jim_DecrRefCount(interp, old->action);
    old->action = bind->action;
    free(bind);
    bind = old;
  } else {
    bind->next = sdlJimBindings;
    sdlJimBindings = bind;
  }

  //Jim_IncrRefCount(bind->action);
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// unbind sdlkey
JIMAPI_FN(unbind) {
  SDLJimBinding *bind, *prev, *old;

  if (jimapiDisableCompletion(interp, argc, argv)) return JIM_OK;
  if (argc < 2) { Jim_WrongNumArgs(interp, 1, argv, "string"); return JIM_ERR; }
  if ((bind = sdlParseBinding(argc, argv, 0)) == NULL) {
    jim_SetResStrf(interp, "%s: can't parse arguments", Jim_String(argv[0]));
    return JIM_ERR;
  }

  // remove old binding if there is any
  if ((old = sdlFindBinding(sdlJimBindings, bind, &prev)) != NULL) {
    free(bind);
    if (prev != NULL) prev->next = old->next; else sdlJimBindings = old->next;
    if (old->action != NULL) Jim_DecrRefCount(interp, old->action);
    free(old);
    Jim_SetResultBool(interp, 1);
  } else {
    Jim_SetResultBool(interp, 0);
  }

  return JIM_OK;
}
