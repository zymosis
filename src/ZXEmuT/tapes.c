/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include <libspectrum.h>

#include "tapes.h"
#include "libvideo/video.h"
#include "console.h"
#include "emuutils.h"


////////////////////////////////////////////////////////////////////////////////
// numberz for tape counter
static VOverlay *tapeOverlay = NULL;


static const unsigned char tapeNumberz[80] = {
  0xFE,0xC6,0xBA,0xBA,0xBA,0xBA,0xC6,0xFE, //0
  0xFE,0xEE,0xCE,0xEE,0xEE,0xEE,0xC6,0xFE, //1
  0xFE,0xC6,0xBA,0xFA,0xC6,0xBE,0x82,0xFE, //2
  0xFE,0xC6,0xBA,0xE6,0xFA,0xBA,0xC6,0xFE, //3
  0xFE,0xF6,0xE6,0xD6,0xB6,0x82,0xF6,0xFE, //4
  0xFE,0x82,0xBE,0x86,0xFA,0xBA,0xC6,0xFE, //5
  0xFE,0xC6,0xBE,0x86,0xBA,0xBA,0xC6,0xFE, //6
  0xFE,0x82,0xFA,0xF6,0xEE,0xEE,0xEE,0xFE, //7
  0xFE,0xC6,0xBA,0xC6,0xBA,0xBA,0xC6,0xFE, //8
  0xFE,0xC6,0xBA,0xBA,0xC2,0xFA,0xC6,0xFE, //9
};


static void tapeDrawDigit (int d, int x, int y) {
  for (int dy = 0; dy < 8; ++dy, ++y) {
    uint8_t b = tapeNumberz[(d++)%80];
    //
    for (int dx = 0; dx < 8; ++dx) {
      if (b&0x80) putPixelVO(tapeOverlay, x+dx, y, 15);
      b = (b&0x7f)<<1;
    }
  }
}


#define TAPE_COUNTER_MAX  (9999*8)

static void tapeDrawCounter (int cnt) {
  int dofs[4], digs[4];
  if (cnt < 0) cnt = 0; else if (cnt > TAPE_COUNTER_MAX) cnt = TAPE_COUNTER_MAX;
  //
  dofs[3] = cnt%8; cnt /= 8;
  digs[3] = cnt%10; cnt /= 10;
  dofs[2] = 0;
  digs[2] = cnt%10; cnt /= 10;
  dofs[1] = 0;
  digs[1] = cnt%10; cnt /= 10;
  dofs[0] = 0;
  digs[0] = cnt%10;
  //
  if (digs[3] == 9) dofs[2] = dofs[3];
  if (digs[2] == 9) dofs[1] = dofs[2];
  if (digs[1] == 9) dofs[0] = dofs[1];
  for (int f = 0; f < 4; ++f) tapeDrawDigit(digs[f]*8+dofs[f], f*8+2, 2);
}


////////////////////////////////////////////////////////////////////////////////
static int64_t tapTSLength = 0;
static int64_t tapTSPos = 0;


void emuTapeDrawCounter (void) {
  if (tapeOverlay == NULL) tapeOverlay = createVO(4*8+3, 8+4);
  clearVO(tapeOverlay, 0);
  drawFrameVO(tapeOverlay, 0, 0, tapeOverlay->w, tapeOverlay->h, 15);
  if (tapTSLength > 0) {
    int cnt = (int64_t)TAPE_COUNTER_MAX-(tapTSPos*TAPE_COUNTER_MAX/tapTSLength);
    //
    if (cnt < 0) cnt = 0; else if (cnt > TAPE_COUNTER_MAX) cnt = TAPE_COUNTER_MAX;
    tapeDrawCounter(cnt);
  } else {
    tapeDrawCounter(TAPE_COUNTER_MAX);
  }
  blitVO(tapeOverlay, 2, frameSfc->h-14, tapeAlpha);
}


void emuStopTape (void) {
  if (optTapePlaying) {
    if (optTapePlaying < 0) {
      if (optTapeMaxSpeed) { optMaxSpeedBadScreenTape = 0; emuSetMaxSpeed(0); }
      if (optTapeAutoPauseEmu) emuSetPaused(1);
    }
    optTapePlaying = 0;
    //tapCurEdge = 0;
    if (tapStopAfterNextEdge == 666) tapNeedRewind = 1;
    tapStopAfterNextEdge = 0;
    emuTapeAcceleratorStop();
  }
}


void emuTapeAutoStop (void) {
  if (tapStopAfterNextEdge == 666) tapNeedRewind = 1;
  if (optTapePlaying < 0 && optTapeAutoPauseEmu) emuSetPaused(1);
  emuStopTape();
  tapStopAfterNextEdge = 0;
}


void emuTapeAdvanceFrame (void) {
  if (optTapePlaying) {
    tapNextEdgeTS -= machineInfo.tsperframe;
    emuTapeAcceleratorAdvanceFrame();
  }
}


void emuTapeEdgeAdvance (int curts) {
  if (tapNeedRewind) {
    emuStopTape();
    return;
  }
  //
  if (zxCurTape != NULL && optTapePlaying) {
    libspectrum_dword ts;
    int flags;
    //
    if (libspectrum_tape_get_next_edge(&ts, &flags, zxCurTape) != LIBSPECTRUM_ERROR_NONE) {
      if (optTapePlaying < 0 && optTapeAutoPauseEmu) emuSetPaused(1);
      emuStopTape();
      cprintf("TAPE: libspectrum error!\n");
      return;
    }
    tapTSPos += (int64_t)ts;
    tapNextEdgeTS = (int64_t)curts+(int64_t)ts;
    //
    if (flags&(LIBSPECTRUM_TAPE_FLAGS_STOP|LIBSPECTRUM_TAPE_FLAGS_STOP48)) {
      if (flags&LIBSPECTRUM_TAPE_FLAGS_TAPE) {
        //fprintf(stderr, "END OF TAPE!\n");
        tapStopAfterNextEdge = 666;
      } else {
        tapStopAfterNextEdge = (flags&LIBSPECTRUM_TAPE_FLAGS_STOP48 ? -1 : 1);
      }
    } else {
      tapStopAfterNextEdge = 0;
    }
    //
    if (ts > 0) {
      if (flags&LIBSPECTRUM_TAPE_FLAGS_LEVEL_LOW) tapNextEdge = 0;
      else if (flags&LIBSPECTRUM_TAPE_FLAGS_LEVEL_HIGH) tapNextEdge = 1;
      else if (flags&LIBSPECTRUM_TAPE_FLAGS_NO_EDGE) tapNextEdge = tapCurEdge;
      else tapNextEdge = !tapCurEdge;
    } else {
      tapNextEdge = tapCurEdge;
    }
    //
    if (tapNextEdge != tapCurEdge) emuTapeAcceleratorEdgeAdvance(flags);
    //fprintf(stderr, "emuTapeEdgeAdvance: tapCurFrameTS=%5lld; tapNextEdgeTS=%5lld; tapNextEdge=%d\n", tapCurFrameTS, tapNextEdgeTS, tapNextEdge);
  } else {
    emuStopTape();
  }
}


int emuTapeCheckStop (void) {
  if (tapStopAfterNextEdge) {
    if (tapStopAfterNextEdge == 666) tapNeedRewind = 1;
    if (tapStopAfterNextEdge > 0 || zxModel == ZX_MACHINE_48K) {
      if (optTapePlaying < 0 && optTapeAutoPauseEmu) emuSetPaused(1);
      emuStopTape();
      tapStopAfterNextEdge = 0;
      return 1;
    }
    tapStopAfterNextEdge = 0;
  }
  return 0;
}


int emuTapeGetCurEdge (void) {
  if (zxCurTape != NULL && optTapePlaying) {
    //fprintf(stderr, "emuTapeGetCurEdge\n");
    while (optTapePlaying) {
      if (tapNextEdgeTS > z80.tstates) break; // not yet
      tapCurEdge = tapNextEdge;
      if (emuTapeCheckStop()) break;
      emuTapeEdgeAdvance(tapNextEdgeTS);
    }
    return tapCurEdge;
  }
  return -1;
}


void emuRewindTape (void) {
  tapNeedRewind = 0;
  tapTSPos = 0;
  tapStopAfterNextEdge = 0;
  if (zxCurTape != NULL && libspectrum_tape_present(zxCurTape)) {
    libspectrum_tape_nth_block(zxCurTape, 0);
  }
}


void emuStartTapeEx (int autostart) {
  if (zxCurTape != NULL && !optTapePlaying) {
    if (tapNeedRewind) emuRewindTape();
    if ((optTapePlaying = (autostart ? -1 : 1)) < 0 && optTapeMaxSpeed) {
      optMaxSpeedBadScreenTape = optTapeAllowMaxSpeedUgly; // let it be ugly ;-)
      emuSetMaxSpeed(1);
    }
    emuTapeAcceleratorStart();
    emuTapeEdgeAdvance(z80.tstates);
  }
}


void emuUnloadTape (void) {
  if (zxCurTape != NULL) {
    emuStopTape();
    tapNeedRewind = 0;
    libspectrum_tape_free(zxCurTape);
    zxCurTape = NULL;
  }
}


void emuClearTape (void) {
  if (zxCurTape != NULL) {
    emuStopTape();
    tapNeedRewind = 0;
    libspectrum_tape_clear(zxCurTape);
  }
}


void emutapCalcLength (void) {
  libspectrum_tape_block *blk;
  libspectrum_tape_iterator it;
  //
  tapTSLength = tapTSPos = 0;
  for (blk = libspectrum_tape_iterator_init(&it, zxCurTape); blk != NULL; blk = libspectrum_tape_iterator_next(&it)) {
    libspectrum_dword blen = libspectrum_tape_block_length(blk);
    //
    tapTSLength += (int64_t)blen;
  }
}


int emuInsertTapeFromBuf (const char *name, const void *buf, int size) {
  emuUnloadTape();
  if ((zxCurTape = libspectrum_tape_alloc()) == NULL) return -1;
  if (libspectrum_tape_read(zxCurTape, buf, size, LIBSPECTRUM_ID_UNKNOWN, name) != LIBSPECTRUM_ERROR_NONE) {
    emuUnloadTape();
    return -1;
  }
  emutapCalcLength();
  emuRewindTape();
  return 0;
}


/*
static int emuInsertTape (const char *name) {
  size_t size;
  int res;
  char *buf;
  //
  if ((buf = loadWholeFile(name, &size, &name)) == NULL) return -1;
  //
  res = emuInsertTapeFromBuf(name, buf, size);
  free(buf);
  return res;
}
*/


#ifdef USE_OLD_TAPE_ACCELERATOR
# include "tapeaccel.c"
#else
# include "tapeaccel_exp.c"
#endif
#include "tapeldrs.c"
#include "tapeldrx.c"
