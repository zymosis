/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
static const int16_t pattern_ftl[] = {
  -4, 0xD3, 0xFE, // OUT (#FE),A
  -12,
  0x263D, // LD H,#3D
  0x01, 0x0140, // LD BC,#4001
  0xD9, // EXX
  0, 8, 0xCD, // CALL LD-EDGE2
  0, 23, 0xCD, // CALL LD-EDGE
  0, 131,
  0x7C, // LD A,H
  0xFE, 0x01, // CP #01
  0x21, // LD HL,...
  0, 137,
  0xE5, // PUSH HL
  0xC3, // JP ...
  // check constants
  0, 28, 0x069C, // LD B,#9C
  0, 35, 0x3EC6, // LD A,#C6
  0, 43, 0x06C9, // LD B,#C9
  0, 51, 0xFE, 0xD4, // CP #D4
  0, 72, 0x06B0, // LD B,#B0
  0, 106, 0x06B0, // LD B,#B0
  0, 114, 0x3ED4, // LD A,#D4
  0, 119, 0x06B0, // LD B,#B0
  // end
  0, 0
};


static int doFTL (void) {
  const tape_loader_info_t loader_info = {
    .pilot_minlen = 807,
    .pilot = 2168,
    .sync1 = 667,
    .sync2 = 735,
    .bit0 = 829,
    .bit1 = 1658
  };
  return emuTapeDoFlashLoadEx(&loader_info);
}


////////////////////////////////////////////////////////////////////////////////
__attribute__((constructor)) static void fl_loader_ctor_ftl (void) {
  const fl_loader_info_t nfo = {
    .name="FTL Loader",
    .pattern=pattern_ftl,
    .detectFn=NULL,
    .accelFn=doFTL,
    .exitOfs=131,
  };
  fl_register_loader(&nfo);
}
