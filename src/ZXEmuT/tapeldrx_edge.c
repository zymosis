/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
static const int16_t pattern_edge[] = {
  -10,
  0xC5, // PUSH BC
  0xDD, 0xE1, // POP IX
  0xF3, // DI
  0x3E, -32666, // LD A,xx
  0xD3, 0xFE, // OUT (#FE),A
  0, 10, 0xCD, 0xE7, 0x05, // CALL LD-EDGE1
  0, 22, 0xCD, 0xE3, 0x05, // CALL LD-EDGE2
  // check constants
  0, 31, 0x069C, // LD B,#9C
  0, 38, 0x3EC6, // LD A,#C6
  0, 46, 0x06C9, // LD B,#C9
  0, 54, 0xFE, 0xD4, // CP #D4
  0, 79, 0x06B2, // LD B,#B2
  0, 88, 0x3EBD, // LD A,#BD
  0, 115, 0xED, 0x56, // IM 1
  // exit
  0, 105,
  0x7C, // LD A,H
  0xB7, // OR A
  0x20, -32666, // JR NZ, ...
  0xE1, // POP HL
  0xC1, // POP BC
  0x05, // DEC B
  // end
  0, 0
};


static int do_edge (void) {
  const tape_loader_info_t loader_info = {
    .pilot_minlen = 100,
    .pilot = 2168,
    .sync1 = 667,
    .sync2 = 735,
    .bit0 = 550,
    .bit1 = 1110,
    .flags = CFL_NO_TYPE
  };
  return emuTapeDoFlashLoadEx(&loader_info);
}


////////////////////////////////////////////////////////////////////////////////
__attribute__((constructor)) static void fl_loader_ctor_edge (void) {
  const fl_loader_info_t nfo = {
    .name="Edge Loader",
    .pattern=pattern_edge,
    .detectFn=NULL,
    .accelFn=do_edge,
    .exitOfs=105,
  };
  fl_register_loader(&nfo);
}
