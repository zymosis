/* High-level Sound Emulation (AY-891x and Beeper)
 * Copyright (c) 2022 Ketmar // Invisible Vector
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SND_AYEMU_HEADER
#define SND_AYEMU_HEADER

#include "blipbuffer.h"


enum {
  AY_CHNL_A_FINE = 0,
  AY_CHNL_A_COARSE,
  AY_CHNL_B_FINE,
  AY_CHNL_B_COARSE,
  AY_CHNL_C_FINE,
  AY_CHNL_C_COARSE,
  AY_NOISE_PERIOD,
  AY_MIXER,
  AY_CHNL_A_VOL,
  AY_CHNL_B_VOL,
  AY_CHNL_C_VOL,
  AY_ENV_FINE,
  AY_ENV_COARSE,
  AY_ENV_SHAPE,
  AY_GPIO_A,
  AY_GPIO_B,
};


// left, center, right
typedef enum {
  AY_STEREO_NONE,
  AY_STEREO_ACB,
  AY_STEREO_ABC,
  AY_STEREO_BAC,
  AY_STEREO_BCA,
  AY_STEREO_CAB,
  AY_STEREO_CBA,
} AY_Stereo_t;


// WARNING! keep in sync with internal `ayFilters`!
typedef enum {
  AY_SPEAKER_DEFAULT,
  AY_SPEAKER_TV,
  AY_SPEAKER_BEEPER,
  AY_SPEAKER_FLAT,
  AY_SPEAKER_CRISP,
  AY_SPEAKER_HANDHELD,
} AY_Filter_t;


// WARNING! there are no user-serviceable parts inside this struct!
typedef struct AYEmu_s AYEmu_t;


enum {
  AY_CHAN_A = 1u<<0,
  AY_CHAN_B = 1u<<1,
  AY_CHAN_C = 1u<<2,
  AY_CHAN_BEEPER = 1u<<3,
};


// basic initialisation: sets fields to sane values
// will create blip synths and such
// return new object on success, or NULL on error
AYEmu_t *ayemu_create (uint32_t cpuTicksPerFrame, uint32_t divisor, int allowBeeper,
                       Blip_Buffer *blipleft, Blip_Buffer *blipright);

// free everything, sets `*ay` to NULL
void ayemu_destroy (AYEmu_t **ay);

// this hacky API can be used to switch between ZX/CPC emulation
// return 0 on success, negative number on error
// it doesn't reset AY
int ayemu_force_frequency (AYEmu_t *ay, uint32_t cpuTicksPerFrame, uint32_t divisor);

// get enabled channels; return bitwise OR of `AY_CHAN_XXX`
uint8_t ayemu_get_enabled_chans (AYEmu_t *ay);

// set enabled channels; pass bitwise OR of `AY_CHAN_XXX`
// should NOT be called mid-frame
void ayemu_set_enabled_chans (AYEmu_t *ay, uint8_t chanmask);

// return non-zero if beeper is available in this emu
// if the beeper is not avaliable, `ayemu_write_beeper()` is noop
int ayemu_is_beeper_avaliable (AYEmu_t *ay);

// set stereo mode, reinit synths
// should NOT be called mid-frame
void ayemu_set_stereo_mode (AYEmu_t *ay, AY_Stereo_t mode);

// set filtering mode
// should NOT be called mid-frame
void ayemu_set_filter (AYEmu_t *ay, AY_Filter_t mode);

// set volume for the 3 AY channels
// should NOT be called mid-frame
void ayemu_set_volume (AYEmu_t *ay, const float volume);

// set volume for the beeper
// should NOT be called mid-frame
void ayemu_set_volume_beeper (AYEmu_t *ay, const float volume);

// call this on machine reset
// resets AY to initial state, restarts AY frame from the scratch
// will not reset volumes, filter or mask
void ayemu_reset (AYEmu_t *ay);

// reset only registers
void ayemu_reset_registers (AYEmu_t *ay);

// return "sound rendering enabled" flag
int ayemu_get_render_enabled (AYEmu_t *ay);

// set "sound rendering enabled" flag
void ayemu_set_render_enabled (AYEmu_t *ay, int enabled);

// make sure that `nowts` is sorted from the lowest to the highest!
void ayemu_write_reg (AYEmu_t *ay, uint8_t reg, uint8_t val, uint32_t nowts);

// read current register value
// note that register values are not clamped/masked (i.e. they are stored as is)
uint8_t ayemu_read_reg (AYEmu_t *ay, uint8_t reg);

// read latest volume levels
// returns byte with bits set for active (non-muted) channel
// this is required for bars, because envelope can change volumes
// ignores channel mask
uint8_t ayemu_read_last_volumes (AYEmu_t *ay, uint8_t vols[3]);

// make sure that `nowts` is sorted from the lowest to the highest!
void ayemu_write_beeper (AYEmu_t *ay, uint8_t beeper, uint8_t tape, uint32_t nowts);

// call this at the end of the frame to finish AY emulation
// call this even if you are using only beeper
void ayemu_sound_overlay (AYEmu_t *ay);


#endif
