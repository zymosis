/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include "emuexec.h"
#include "../libfusefdc/libfusefdc.h"
#include "libvideo/video.h"
#include "console.h"
#include "lssnap.h"
#include "emuutils.h"
#include "emuevents.h"
#include "zxscrdraw.h"
#include "snd_alsa.h"
#include "tapes.h"


////////////////////////////////////////////////////////////////////////////////
int z80_was_frame_end = 0;

int tsmark_active = 0;
int tsmark_frames = 0;
int tsmark_tstart = 0;


////////////////////////////////////////////////////////////////////////////////
static inline void autofireEndFrame (void) {
  if (optAutofire) {
    if (--afireLeft <= 0) {
      afireLeft = ((afireDown = !afireDown) ? optAutofireHold : optAutofireDelay);
      // if optAutofireDelay is zero, we'll just hold the fire
      if (!afireDown && !afireLeft) {
        afireDown = 1;
        afireLeft = optAutofireHold;
      }
    }
  }
}


// !0: bit to set
int isAutofireKempston (void) {
  return (optAutofire && afireDown && (afirePortBit&0xff00) == 0x0800 ? (afirePortBit&0x1f) : 0);
}


// !0: lo byte: bit to reset (0x01), hi byte: port
int isAutofireKeyboard (void) {
  return (optAutofire && afireDown && (afirePortBit&0xff00) < 0x0800 ? afirePortBit : 0);
}


////////////////////////////////////////////////////////////////////////////////
// force frame end
static void z80FrameEnd (void) {
  if (tsmark_active) ++tsmark_frames;
  ++z80_was_frame_end;

  if (emuEvLastFrameCallTS < (uint32_t)z80.tstates) {
    const uint32_t passed = (uint32_t)z80.tstates-emuEvLastFrameCallTS;
    emuEvLastFrameCallTS = (uint32_t)z80.tstates;
    emuTimePassed(passed);
  }

  zxRealiseScreen(machineInfo.tsperframe);
  zxOldScreenTS = 0;
  soundEndFrame();
  autofireEndFrame();

  z80.tstates %= machineInfo.tsperframe;
  emuEvLastFrameCallTS %= (uint32_t)machineInfo.tsperframe;

  if (++zxFlashTimer >= 16) { zxFlashTimer = 0; zxFlashState = !zxFlashState; }

  if (zxWasUlaSnow) {
    memset(zxUlaSnow, 0, sizeof(zxUlaSnow));
    zxWasUlaSnow = 0;
  }

  emuTapeAdvanceFrame();
  if (emuFrameStartTime == 0) {
    if (!optPaused && optDrawFPS) {
      emuFrameStartTime = timerGetMS();
      emuFrameCount = 0;
    }
  } else {
    ++emuFrameCount;
  }

  zxScreenCurrent ^= 1;

  // autodetect "noflic"
  if (optNoFlic < 0 && !optNoScreenReal) {
    if (emuCurrScreenBank != emuLastScreenBank) {
      // different screens
      if (!optNoFlicDetected) {
        // detection
        //fprintf(stderr, "+++ NFC warmup=%d\n", emuScreenBankSwitchCount);
        if (++emuScreenBankSwitchCount >= 4) {
          optNoFlicDetected = 1;
          if (emuNoflicMsgCooldown == 0) {
            cprintf("NOFLIC activated\n");
            emuNoflicMsgCooldown = 13;
          }
        }
      } else {
        // cooldown time
        //fprintf(stderr, "***NFC\n");
        emuScreenBankSwitchCount = 6;
      }
    } else if (optNoFlicDetected) {
      // same screens, detected noflic; perform cooldown
      //fprintf(stderr, "::: NFC cooldown=%d\n", emuScreenBankSwitchCount);
      if (--emuScreenBankSwitchCount <= 0) {
        if (emuNoflicMsgCooldown == 0) {
          cprintf("NOFLIC deactivated\n");
          emuNoflicMsgCooldown = 13;
        }
        optNoFlicDetected = 0;
        emuScreenBankSwitchCount = 0;
      }
    }
  } else {
    optNoFlicDetected = 0;
    emuScreenBankSwitchCount = 0;
  }

  emuLastScreenBank = emuCurrScreenBank;
  if (--emuNoflicMsgCooldown < 0) emuNoflicMsgCooldown = 0;
}


////////////////////////////////////////////////////////////////////////////////
static inline void z80TSPassed (int ts) {
  if (ts < 0) __builtin_trap();
  if (ts) {
    emuEvLastFrameCallTS += (uint32_t)ts;
    emuTimePassed((uint32_t)ts);
  }
}


//==========================================================================
//
//  z80Step
//
//  i'm using `zym_exec_ex(.., 1)` here, because currently the emu
//  doesn't use `next_event_tstate` (alas...)
//
//  return number of used tstates
//
//==========================================================================
int z80Step (int maxtstates) {
  int tstotal = 0;

  while (maxtstates < 0 || tstotal < maxtstates) {
    if (debuggerActive || optPaused) break; // stop right here, you criminal scum!

    int splus = 0;

    // RZX playback
    if (zxRZX != NULL) {
      int intrHit = 0;
      if (z80.tstates >= machineInfo.tsperframe) {
        //cprintf("RZX ERROR: too many tstates without interrupt! (%d)\n", z80.tstates);
        //fprintf(stderr, "RZX ERROR: too many tstates without interrupt! (%d)\n", z80.tstates);
        //lssnap_rzx_stop();
        //lssnap_slt_clear();
        //z80.tstates = 0;
        z80.tstates = machineInfo.tsperframe-1;
        if (emuEvLastFrameCallTS < (uint32_t)z80.tstates) {
          const uint32_t passed = (uint32_t)z80.tstates-emuEvLastFrameCallTS;
          emuEvLastFrameCallTS = (uint32_t)z80.tstates;
          emuTimePassed(passed);
        }
        intrHit = 1;
      }

      // interrupt must be generated by RZX playback engine
      int oldR, newR;
      z80GenerateNMI = 0;
      if (zxRZXRCount == -666) {
        if (lssnap_rzx_first_frame() != 0) goto rzxerror;
        //fprintf(stderr, "RZX FIRST FRAME: rcount=%d; ts=%d (%d)\n", zxRZXRCount, z80.tstates, machineInfo.tsperframe);
      }

      if (zxRZXRCount <= 0) {
        // generate interrupt
        //fprintf(stderr, "playing RZX: interrupt (tstates=%d)!\n", z80.tstates);
        if (zxRZXRCount != -42) {
          // force interrupt; -42 means "retrigger from RZX", play full frame
          if (z80.tstates < machineInfo.tsperframe) z80.tstates = machineInfo.tsperframe; //???
          intrHit = 1;
        }
        if (intrHit) {
          z80FrameEnd();
          z80.tstates %= machineInfo.intrlen; // so we will not go out of tsperframe too much
          if (lssnap_rzx_next_frame() != 0) goto rzxerror;
          //fprintf(stderr, "RZX NEXT FRAME: rcount=%d; ts=%d (%d)\n", zxRZXRCount, z80.tstates, machineInfo.tsperframe);
          splus += zym_intr(&z80);
        }
      }

      oldR = z80.regR&0x7f;
      splus += zym_exec_ex(&z80, 1);
      z80.next_event_tstate = -1;
      if (!dbtTickCounterPaused) dbgTickCounter += splus;
      newR = z80.regR&0x7f;
      if (newR < oldR) newR += 0x80;
      if (zxRZXRCount >= 0) zxRZXRCount -= (newR-oldR);
      z80GenerateNMI = 0;
      z80TSPassed(splus);
      //fprintf(stderr, " rcount=%d (%d) ts=%d (%d)\n", zxRZXRCount, newR-oldR, z80.tstates, machineInfo.tsperframe);

      tstotal += splus;
      if (intrHit) break;
      continue;
    }

    if (z80GenerateNMI) {
      z80GenerateNMI = 0;
      if ((splus = zym_nmi(&z80)) != 0) {
        // NMI accepted
        if (zxModel == ZX_MACHINE_SCORPION) {
           // page in ROM2
          zxLastOut1ffd |= 0x02;
          zxLastOut1ffd &= ~0x01; // disable RAM at page 0
          emuRealizeMemoryPaging();
        } else {
          zxTRDOSpagein();
        }
        z80TSPassed(splus);
        if (!dbtTickCounterPaused) dbgTickCounter += splus;
        tstotal += splus;
        //HACKHACKHACK
        if (debuggerActive) dbgNMIHit = 1;
        return tstotal;
      }
    }

    if (z80.tstates >= machineInfo.tsperframe) {
      z80FrameEnd();
      if (z80.tstates < machineInfo.intrlen) {
        //int ttt = 0;
        //if (z80.prev_was_EIDDR) { ttt = 1; printf("!!!zym_intr: prev_was_EIDDR=%d; iff1=%d; pc=#%04X; intrlen=%d\n", z80.prev_was_EIDDR, z80.iff1, z80.pc, machineInfo.intrlen); }
        if ((splus = zym_intr(&z80)) != 0) {
          z80TSPassed(splus);
          if (!dbtTickCounterPaused) dbgTickCounter += splus;
          tstotal += splus;
          if (debuggerActive) dbgIntrHit = 1;
        }
        //if (ttt) { printf("***zym_intr: prev_was_EIDDR=%d; iff1=%d; pc=#%04X; intrlen=%d\n", z80.prev_was_EIDDR, z80.iff1, z80.pc, machineInfo.intrlen); }
      }
      // stop on intr in any case
      break;
    }

    // calculate number of ticks to execute
    int tsrun = machineInfo.tsperframe-z80.tstates;
    if (tsrun <= 0) {
      fprintf(stderr, "FATAL: internal error in `z80Step()`\n");
      __builtin_trap();
    }

    //fprintf(stderr, "000: tsrun=%d\n", tsrun);

    uint32_t nt = emuEvNextTime();
    //fprintf(stderr, "000: nt=%u\n", nt);
    if (nt != EMU_EV_NONE && (int)nt < tsrun) tsrun = (nt ? (int)nt : 1);
    //fprintf(stderr, "001: tsrun=%d\n", tsrun);

    if (maxtstates >= 0) {
      int left = maxtstates-tstotal;
      if (left < 1) left = 1;
      //fprintf(stderr, "002 left=%d\n", left);
      if (tsrun > left) tsrun = left;
      //fprintf(stderr, "003: tsrun=%d\n", tsrun);
    }

    //fprintf(stderr, "004: *** tsrun=%d\n", tsrun);
    splus = zym_exec_ex(&z80, tsrun);
    //fprintf(stderr, "005:     splus=%d\n", splus);
    z80.next_event_tstate = -1;
    z80TSPassed(splus);
    if (!dbtTickCounterPaused) dbgTickCounter += splus;
    tstotal += splus;
  }

  return tstotal;

rzxerror:
  lssnap_rzx_stop();
  lssnap_slt_clear();
  z80GenerateNMI = 0;
  return tstotal;
}
