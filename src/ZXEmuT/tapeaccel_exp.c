/* loader detection
   Copyright (c) 2006 Philip Kendall

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   E-mail: philip-fuse@shadowmagic.org.uk
*/

//static int last_tstates_read = -100000;
//static uint8_t last_b_read = 0x00;
static int length_known1 = 0, length_known2 = 0;
static int length_long1 = 0, length_long2 = 0;


static int tapDetectInCount = 0;
static int tapDetectLastInTS = -666;
static int tapDetectLastInB = 0x00;


typedef enum acceleration_mode_t {
  ACCELERATION_MODE_NONE = 0,
  ACCELERATION_MODE_INCREASING,
  ACCELERATION_MODE_DECREASING,
} acceleration_mode_t;


static acceleration_mode_t acceleration_mode;
static uint16_t acceleration_pc;


void emuTapeAcceleratorStart (void) {
  tapDetectInCount = 0;
  acceleration_mode = ACCELERATION_MODE_NONE;
}


void emuTapeAcceleratorStop (void) {
  tapDetectInCount = 0;
  acceleration_mode = ACCELERATION_MODE_NONE;
}


void emuTapeAcceleratorAdvanceFrame (void) {
  if (tapDetectLastInTS > -100666) tapDetectLastInTS -= machineInfo.tsperframe;
}


void emuTapeAcceleratorEdgeAdvance (int flags) {
  if (flags&LIBSPECTRUM_TAPE_FLAGS_LENGTH_SHORT) {
    length_known2 = 1;
    length_long2 = 0;
  } else if (flags&LIBSPECTRUM_TAPE_FLAGS_LENGTH_LONG) {
    length_known2 = 1;
    length_long2 = 1;
  } else {
    length_known2 = 0;
  }
}


static void emuTapeDoAcceleration (void) {
  if (length_known1) {
    z80.bc.b = ((length_long1^(acceleration_mode == ACCELERATION_MODE_DECREASING)) ? 0xfe : 0x00);
    z80.af.f |= 0x01;
    z80.pc = zym_pop(&z80);
    //
    //tape_next_edge(z80.tstates, 0, NULL);
    tapNextEdgeTS = z80.tstates;
    //
    tapDetectInCount = 0;
    zxBorder = 0;
  }
  length_known1 = length_known2;
  length_long1 = length_long2;
}


static acceleration_mode_t emuTapeAccelerationDetector (uint16_t pc) {
  int state = 0, count = 0;
  for (;;) {
    uint8_t b = z80.mem_read(&z80, pc, ZYM_MEMIO_OTHER);
    pc = (pc+1)&0xffff;
    ++count;
    switch (state) {
      case 0:
        switch (b) {
          case 0x03: state = 28; break; // data byte of JR NZ, ... - Alkatraz
          case 0x04: state = 1; break; // INC B - Many loaders
          default: state = 13; break;  // Possible Digital Integration
        }
        break;
      case 1:
        switch (b) {
          case 0x20: state = 40; break; // JR NZ - variant Alkatraz
          case 0xc8: state = 2; break; // RET Z
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 2:
        switch (b) {
          case 0x3e: state = 3; break; // LD A,nn
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 3:
        switch (b) {
          case 0x00:          // Search Loader
          case 0x7f:          // ROM loader and variants
          case 0xff:          // Dinaload
            state = 4; break; // Data byte
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 4:
        switch (b) {
          case 0xdb: state = 5; break; // IN A,(nn)
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 5:
        switch (b) {
          case 0xfe: state = 6; break; // Data byte
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 6:
        switch (b) {
          case 0x1f: state = 7; break;  // RRA
          case 0xa9: state = 24; break; // XOR C - Search Loader
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 7:
        switch (b) {
          case 0x00: // NOP - Bleepload
          case 0xa7: // AND A - Microsphere
          case 0xc8: // RET Z - Paul Owens
          case 0xd0: // RET NC - ROM loader
            state = 8; break;
          case 0xa9: state = 9; break; // XOR C - Speedlock
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 8:
        switch (b) {
          case 0xa9: state = 9; break; // XOR C
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 9:
        switch (b) {
          case 0xe6: state = 10; break; // AND nn
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 10:
        switch (b) {
          case 0x20: state = 11; break; // Data byte
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 11:
        switch (b) {
          case 0x28: state = 12; break; // JR nn
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 12:
        return (b == 0x100-count ? ACCELERATION_MODE_INCREASING : ACCELERATION_MODE_NONE);

      // Digital Integration loader
      case 13:
        state = 14; break; // Possible Digital Integration
      case 14:
        switch (b) {
          case 0x05: state = 15; break; // DEC B - Digital Integration
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 15:
        switch (b) {
          case 0xc8: state = 16; break; // RET Z
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 16:
        switch (b) {
          case 0xdb: state = 17; break; // IN A,(nn)
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 17:
        switch (b) {
          case 0xfe: state = 18; break; // Data byte
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 18:
        switch (b) {
          case 0xa9: state = 19; break; // XOR C
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 19:
        switch (b) {
          case 0xe6: state = 20; break; // AND nn
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 20:
        switch (b) {
          case 0x40: state = 21; break; // Data byte
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 21:
        switch (b) {
          case 0xca: state = 22; break; // JP Z,nnnn
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 22: // LSB of jump target
        if (b == (((int32_t)z80.pc-4)&0xff)) {
          state = 23;
        } else {
          return ACCELERATION_MODE_NONE;
        }
        break;
      case 23: // MSB of jump target
        return (b == ((((int32_t)z80.pc-4)&0xff00)>>8) ? ACCELERATION_MODE_DECREASING : ACCELERATION_MODE_NONE);
      /* Search loader */
      case 24:
        switch (b) {
          case 0xe6: state = 25; break; // AND nn
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 25:
        switch (b) {
          case 0x40: state = 26; break; // Data byte
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 26:
        switch (b) {
          case 0x28: state = 12; break; // JR Z - Space Crusade
          case 0xd8: state = 27; break; // RET C
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 27:
        switch (b) {
          case 0x00: state = 11; break; // NOP
          default: return ACCELERATION_MODE_NONE;
        }
        break;

      // Alkatraz

      case 28:
        switch (b) {
          case 0xc3: state = 29; break; // JP nnnn
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 29:
        state = 30; break; // first data byte of JP
      case 30:
        state = 31; break; // second data byte of JP
      case 31:
        switch (b) {
          case 0xdb: state = 32; break; // IN A,(nn)
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 32:
        switch (b) {
          case 0xfe: state = 33; break; // data byte
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 33:
        switch (b) {
          case 0x1f: state = 34; break; // RRA
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 34:
        switch (b) {
          case 0xc8: state = 35; break; // RET Z
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 35:
        switch (b) {
          case 0xa9: state = 36; break; // XOR C
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 36:
        switch (b) {
          case 0xe6: state = 37; break; // AND nn
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 37:
        switch (b) {
          case 0x20: state = 38; break; // data byte
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 38:
        switch (b) {
          case 0x28: state = 39; break; // JR Z,nn
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 39:
        switch (b) {
          case 0xf1: // normal data byte
          case 0xf3: // variant data byte
            return ACCELERATION_MODE_INCREASING;
          default: return ACCELERATION_MODE_NONE;
        }
        break;

      // "Variant" Alkatraz
      case 40:
        switch (b) {
          case 0x01: state = 41; break; // data byte of JR NZ
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      case 41:
        switch (b) {
          case 0xc9: state = 31; break; // RET
          default: return ACCELERATION_MODE_NONE;
        }
        break;
      default:
        // can't happen
        cprintf("\x04ERROR: tapeaccel_exp: invalid state %d!\n", state);
        return ACCELERATION_MODE_NONE;
    }
  }
}


static void emuTapeCheckForAcceleration (void) {
  // if the IN occured at a different location to the one we're accelerating, stop acceleration
  if (acceleration_mode && z80.pc != acceleration_pc) acceleration_mode = ACCELERATION_MODE_NONE;
  // if we're not accelerating, check if this is a loader
  if (!acceleration_mode) {
    acceleration_mode = emuTapeAccelerationDetector(((int)z80.pc-6)&0xffff);
    acceleration_pc = z80.pc;
  }
  if (acceleration_mode != ACCELERATION_MODE_NONE) {
    //fprintf(stderr, "ACCELMODE: %d\n", (int)acceleration_mode);
    emuTapeDoAcceleration();
  }
}


void emuTapeLoaderDetector (void) {
  int tdelta = z80.tstates-tapDetectLastInTS;
  uint8_t bdelta = ((int)z80.bc.b-tapDetectLastInB)&0xff;
  //
  tapDetectLastInTS = z80.tstates;
  tapDetectLastInB = z80.bc.b;
  //
  if (optTapeDetector) {
    // detector is on
    if (optTapePlaying) {
      // tape is playing
      if (tdelta > 1000 || (bdelta != 1 && bdelta != 0 && bdelta != 0xff)) {
        if (++tapDetectInCount >= 2) {
          // seems that this is not a loader anymore
          emuStopTape();
        }
      } else {
        tapDetectInCount = 0;
      }
    } else {
      // tape is not playing
      if (tdelta <= 500 && (bdelta == 1 || bdelta == 0xff)) {
        if (zxCurTape != NULL && !tapNeedRewind) {
          if (++tapDetectInCount >= 10) {
            // seems that this is LD-EDGE
            emuStartTapeAuto();
          }
        } else {
          tapDetectInCount = 0;
        }
      } else {
        tapDetectInCount = 0;
      }
    }
  } else {
    // detector is off
    tapDetectInCount = 0;
  }
  //
  if (optTapePlaying) emuTapeCheckForAcceleration();
}
