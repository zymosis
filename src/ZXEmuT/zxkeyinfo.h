/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_ZXKEYINFO_H
#define ZXEMUT_ZXKEYINFO_H

#include <stdint.h>


typedef struct {
  const char *name;
  uint8_t port;
  uint8_t mask;
} ZXKeyInfo;

extern ZXKeyInfo zxKeyInfo[50];


extern int zxFindKeyByName (const char *name);
extern int zxFindKeyByWord (const uint16_t w); // hi byte: port; lo byte: mask


#endif
