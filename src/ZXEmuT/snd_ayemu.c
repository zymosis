/* High-level Sound Emulation (AY-891x and Beeper)
 * Copyright (c) 2022 Ketmar // Invisible Vector
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "snd_ayemu.h"

#define USE_AYEMUL_NOISE


#define AmplBeeper  (14288)
#define AmplTape    (4096)
#define AmplAYTone  (10500) /* three of these */

// the AY steps down the external clock by 16 for tone and noise generators
#define AY_CLOCK_DIVISOR  (16)

// bitmasks for envelope
#define AY_ENV_CONT    (8)
#define AY_ENV_ATTACK  (4)
#define AY_ENV_ALT     (2)
#define AY_ENV_HOLD    (1)


struct AYEmu_s {
  AY_Stereo_t stereoType; // ABC

  uint32_t toneTick[3];
  uint8_t toneHigh[3]; // flags
  uint32_t noiseTick;
  uint32_t toneCycles, envCycles;
  uint32_t envInternalTick, envTick;
  uint32_t tonePeriod[3];
  uint32_t noisePeriod, envPeriod;

  // local copy of the AY registers
  uint8_t registers[16];

  // left and right synth (`AY_STEREO_NONE` mode is using only the left ones)
  // the layout is:
  //   [0]=A/LEFT, [1]=B/LEFT, [2]=C/LEFT
  //   [3]=A/RIGHT, [4]=B/RIGHT, [5]=C/RIGHT
  // all synths are always created, but some may be unused
  Blip_Synth *synths[6];

  // left and right synths for beeper
  // i decided to keem them here too, because why not
  Blip_Synth *synthBeeper[2];

  float volume; // overall AY volume
  float volumeBeeper; // overall AY volume

  int filterBass;
  float filterTreble;

  uint32_t rng;
  uint8_t noiseToggle; // flag
  uint8_t envFirst; // flag
  uint8_t envRev; // flag
  int32_t envCounter;

  // AY frequency is always a division of CPU frequency
  // for ZX Spectrum it is usually 2
  uint32_t cpuTicks;
  uint32_t cpuTicksDiv;
  // current ticks (cpu)
  uint32_t cpuTCount;

  int32_t chans[3];
  uint8_t lastvols[3];

  uint8_t chanmask;

  uint8_t render_enabled;

  Blip_Buffer *blipleft;
  Blip_Buffer *blipright;
};


/* AY output doesn't match the claimed levels; these levels are based
 * on the measurements posted to comp.sys.sinclair in Dec 2001 by
 * Matthew Westcott, adjusted as I described in a followup to his post,
 * then scaled to 0..0xffff.
 */
static const uint16_t ay_levels[16] = {
  0x0000, 0x0385, 0x053D, 0x0770,
  0x0AD7, 0x0FD5, 0x15B0, 0x230C,
  0x2B4C, 0x43C1, 0x5A4B, 0x732F,
  0x9204, 0xAFF1, 0xD921, 0xFFFF,
};


static const float chanlrvols[7][6] = {
  // a: l/r        b: l/r        c: l/r
  { 0.67, 0.67,   0.67, 0.67,   0.67, 0.67 }, //AY_STEREO_NONE
  { 1.0,  0.33,   0.33, 1.0,    0.67, 0.67 }, //AY_STEREO_ACB
  { 1.0,  0.33,   0.67, 0.67,   0.33, 1.0 },  //AY_STEREO_ABC
  { 0.67, 0.67,   1.0,  0.33,   0.33, 1.0 },  //AY_STEREO_BAC
  { 0.33, 1.0,    1.0,  0.33,   0.67, 0.67 }, //AY_STEREO_BCA
  { 0.67, 0.67,   0.33, 1.0,    1.0,  0.33 }, //AY_STEREO_CAB
  { 0.33, 1.0,    0.67, 0.67,   1.0,  0.33 }, //AY_STEREO_CBA
};


typedef struct {
  int bass;
  float treble;
} BleepFilterInfo;


// WARNING! keep in sync with `AY_Speaker_t`!
static const BleepFilterInfo ayFilters[7] = {
  {16, -8.0f}, // AY_SPEAKER_DEFAULT
  {200, -37.0f}, // AY_SPEAKER_TV
  {1000, -67.0f}, // AY_SPEAKER_BEEPER
  {1, 0.0f}, // AY_SPEAKER_FLAT
  {1, 5.0f}, // AY_SPEAKER_CRISP
  {2000, -47.0f}, // AY_SPEAKER_HANDHELD
};


static int32_t ay_scaled_levels[16];


//==========================================================================
//
//  ctor_ay_levels__
//
//==========================================================================
static __attribute__((constructor)) void ctor_ay_levels__ (void) {
  // scale the values down to fit
  for (unsigned f = 0; f < 16; ++f) {
    ay_scaled_levels[f] = ((uint32_t)ay_levels[f]*AmplAYTone+0x8000U)/0xffffU;
  }
}


//==========================================================================
//
//  ayemu_set_outputs
//
//==========================================================================
static void ayemu_set_outputs (AYEmu_t *ay) {
  for (unsigned f = 0; f < 3; ++f) {
    blip_synth_set_output(ay->synths[f+0], ay->blipleft);
    blip_synth_set_output(ay->synths[f+3], ay->blipright);
  }
  for (unsigned f = 0; f < 6; ++f) blip_synth_reinit(ay->synths[f]);

  blip_synth_set_output(ay->synthBeeper[0], ay->blipleft);
  blip_synth_set_output(ay->synthBeeper[1], ay->blipright);
  for (unsigned f = 0; f < 2; ++f) blip_synth_reinit(ay->synthBeeper[f]);
}


//==========================================================================
//
//  ayemu_update_filters
//
//==========================================================================
static void ayemu_update_filters (AYEmu_t *ay) {
  if (ay->blipleft) blip_buffer_set_bass_freq(ay->blipleft, ay->filterBass);
  if (ay->blipright) blip_buffer_set_bass_freq(ay->blipright, ay->filterBass);
  for (unsigned f = 0; f < 6; ++f) {
    blip_synth_set_treble_eq(ay->synths[f], ay->filterTreble);
  }
  for (unsigned f = 0; f < 2; ++f) {
    blip_synth_set_treble_eq(ay->synthBeeper[f], ay->filterTreble);
  }
}


//==========================================================================
//
//  ayemu_update_volumes
//
//==========================================================================
static void ayemu_update_volumes (AYEmu_t *ay) {
  //fprintf(stderr, "\n============\n");
  float volume = ay->volume;
  for (unsigned f = 0; f < 3; ++f) {
    #if 0
    fprintf(stderr, "f=%u; V=%g : %g\n", f, chanlrvols[(unsigned)ay->stereoType][f*2+0],
                                            chanlrvols[(unsigned)ay->stereoType][f*2+1]);
    #endif
    blip_synth_set_volume(ay->synths[f+0], chanlrvols[(unsigned)ay->stereoType][f*2+0]*volume);
    blip_synth_set_volume(ay->synths[f+3], chanlrvols[(unsigned)ay->stereoType][f*2+1]*volume);
  }
  //fflush(stderr);
  volume = ay->volumeBeeper;
  blip_synth_set_volume(ay->synthBeeper[0], 0.67f*volume);
  blip_synth_set_volume(ay->synthBeeper[1], 0.67f*volume);
}


//==========================================================================
//
//  ayemu_create
//
//  basic initialisation: sets fields to sane values
//  will create blip synths and such
//  return 0 on success, or negative value on error
//
//==========================================================================
AYEmu_t *ayemu_create (uint32_t cpuTicksPerFrame, uint32_t divisor, int allowBeeper,
                       Blip_Buffer *blipleft, Blip_Buffer *blipright)
{
  divisor *= AY_CLOCK_DIVISOR;

  if (!blipleft || !blipright || cpuTicksPerFrame < 10 || !divisor) return NULL;
  if (cpuTicksPerFrame/divisor < 1) return NULL;

  AYEmu_t *ay = malloc(sizeof(AYEmu_t));
  if (!ay) return NULL;
  memset(ay, 0, sizeof(*ay));

  ay->cpuTicks = cpuTicksPerFrame;
  ay->cpuTicksDiv = divisor;

  ay->blipleft = blipleft;
  ay->blipright = blipright;

  #ifdef USE_AYEMUL_NOISE
  ay->rng = 0xffff;
  #else
  ay->rng = 0x1;
  #endif

  ay->noiseToggle = 0;
  ay->envFirst = 1;
  ay->envRev = 0;
  ay->envCounter = 15;

  ay->stereoType = AY_STEREO_ABC;
  ay->volume = 1.0f;
  ay->volumeBeeper = 1.0f;

  ay->filterBass = 0;
  ay->filterTreble = 0.0f;

  ay->render_enabled = 1;

  // create AY synths
  ay->chanmask = AY_CHAN_A|AY_CHAN_B|AY_CHAN_C;
  for (unsigned f = 0; f < 6; ++f) {
    ay->synths[f] = new_Blip_Synth();
    if (!ay->synths[f]) {
      ayemu_destroy(&ay);
      return NULL;
    }
  }

  // create beeper synths
  if (allowBeeper) {
    ay->chanmask |= AY_CHAN_BEEPER;
    for (unsigned f = 0; f < 2; ++f) {
      ay->synthBeeper[f] = new_Blip_Synth();
      if (!ay->synthBeeper[f]) {
        ayemu_destroy(&ay);
        return NULL;
      }
    }
  } else {
    for (unsigned f = 0; f < 2; ++f) ay->synthBeeper[f] = NULL;
  }

  ayemu_reset(ay);
  return ay;
}


//==========================================================================
//
//  ayemu_destroy
//
//  frees everything
//
//==========================================================================
void ayemu_destroy (AYEmu_t **ay) {
  if (!ay || !*ay) return;
  for (unsigned f = 0; f < 6; ++f) delete_Blip_Synth(&(*ay)->synths[f]);
  for (unsigned f = 0; f < 2; ++f) delete_Blip_Synth(&(*ay)->synthBeeper[f]);
  memset(*ay, 0, sizeof(AYEmu_t)); // just for fun
  free(*ay);
  *ay = NULL;
}


//==========================================================================
//
//  ayemu_get_render_enabled
//
//  return "sound rendering enabled" flag
//
//==========================================================================
int ayemu_get_render_enabled (AYEmu_t *ay) {
  return (ay ? ay->render_enabled : 0);
}


//==========================================================================
//
//  ayemu_set_render_enabled
//
//  set "sound rendering enabled" flag
//
//==========================================================================
void ayemu_set_render_enabled (AYEmu_t *ay, int enabled) {
  if (ay) ay->render_enabled = !!enabled;
}


//==========================================================================
//
//  ayemu_force_frequency
//
//==========================================================================
int ayemu_force_frequency (AYEmu_t *ay, uint32_t cpuTicksPerFrame, uint32_t divisor) {
  if (!ay) return -1;

  divisor *= AY_CLOCK_DIVISOR;

  if (cpuTicksPerFrame < 10 || !divisor) return -1;
  if (cpuTicksPerFrame/divisor < 1) return -1;

  ay->cpuTicks = cpuTicksPerFrame;
  ay->cpuTicksDiv = divisor;

  if (ay->cpuTCount > ay->cpuTicks) ay->cpuTCount = ay->cpuTicks;

  return 0;
}


//==========================================================================
//
//  ayemu_get_enabled_chans
//
//  get enabled channels; returns bitwise OR of `AY_CHAN_XXX`
//
//==========================================================================
uint8_t ayemu_get_enabled_chans (AYEmu_t *ay) {
  return (ay ? ay->chanmask : 0);
}


//==========================================================================
//
//  ayemu_set_enabled_chans
//
//  set enabled channels; pass bitwise OR of `AY_CHAN_XXX`
//  should NOT be called mid-frame
//
//==========================================================================
void ayemu_set_enabled_chans (AYEmu_t *ay, uint8_t chanmask) {
  if (!ay) return;
  chanmask &= AY_CHAN_A|AY_CHAN_B|AY_CHAN_C|(ay->synthBeeper[0] ? AY_CHAN_BEEPER : 0);
  ay->chanmask = chanmask;
}


//==========================================================================
//
//  ayemu_is_beeper_avaliable
//
//  returns non-zero if beeper is available in this emu
//  if the beeper is not avaliable, `ayemu_write_beeper()` is noop
//
//==========================================================================
int ayemu_is_beeper_avaliable (AYEmu_t *ay) {
  return (ay ? !!ay->synthBeeper[0] : 0);
}


//==========================================================================
//
//  ayemu_set_stereo_mode
//
//  set stereo mode, reinit synths
//  should NOT be called mid-frame
//
//==========================================================================
void ayemu_set_stereo_mode (AYEmu_t *ay, AY_Stereo_t mode) {
  if (!ay) return;
  //if (ay->stereoType == mode) return;
  if ((unsigned)mode > 6) __builtin_trap();
  ay->stereoType = mode;
  ayemu_update_volumes(ay);
}


//==========================================================================
//
//  ayemu_set_filter
//
//  set filtering mode
//
//==========================================================================
void ayemu_set_filter (AYEmu_t *ay, AY_Filter_t mode) {
  if (!ay) return;
  ay->filterBass = ayFilters[mode].bass;
  ay->filterTreble = ayFilters[mode].treble;
  ayemu_update_filters(ay);
}


//==========================================================================
//
//  ayemu_set_volume
//
//  set volumes for the 3 AY channels
//  should NOT be called mid-frame
//
//==========================================================================
void ayemu_set_volume (AYEmu_t *ay, const float volume) {
  if (!ay) return;
  ay->volume = volume;
  ayemu_update_volumes(ay);
}


//==========================================================================
//
//  ayemu_set_volume_beeper
//
//==========================================================================
void ayemu_set_volume_beeper (AYEmu_t *ay, const float volume) {
  if (!ay) return;
  ay->volumeBeeper = volume;
  if (ay->synthBeeper[0]) ayemu_update_volumes(ay);
}


//==========================================================================
//
//  ayemu_reset_registers
//
//==========================================================================
void ayemu_reset_registers (AYEmu_t *ay) {
  if (!ay) return;
  ay->noiseTick = ay->noisePeriod = 0;
  ay->envInternalTick = ay->envTick = ay->envPeriod = 0;
  ay->toneCycles = ay->envCycles = 0;
  memset(ay->toneTick, 0, sizeof(ay->toneTick));
  memset(ay->toneHigh, 0, sizeof(ay->toneHigh));
  ay->tonePeriod[0] = ay->tonePeriod[1] = ay->tonePeriod[2] = 1;
  memset(ay->registers, 0, sizeof(ay->registers));
  ay->registers[7] = 0xff; // turn off the mixer
  #ifdef USE_AYEMUL_NOISE
  ay->rng = 0xffff;
  #else
  ay->rng = 0x1;
  #endif
  ay->noiseToggle = 0;
  ay->envFirst = 1;
  ay->envRev = 0;
  ay->envCounter = 15;
  ay->envInternalTick = ay->envTick = ay->envCycles = 0;
  ay->chans[0] = ay->chans[1] = ay->chans[2] = 0;
  ay->lastvols[0] = ay->lastvols[1] = ay->lastvols[2] = 0;
  ay->cpuTCount = 0;
}


//==========================================================================
//
//  ayemu_reset
//
//  call this on machine reset
//
//==========================================================================
void ayemu_reset (AYEmu_t *ay) {
  if (!ay) return;
  ayemu_reset_registers(ay);
  ayemu_set_outputs(ay);
  ayemu_update_volumes(ay);
  ayemu_update_filters(ay);
}


//==========================================================================
//
//  ayemu_render_up_to
//
//==========================================================================
static void ayemu_render_up_to (AYEmu_t *ay, uint32_t nowcycles) {
  int32_t toneLevel[3];

  const uint8_t chanmask = (ay->render_enabled ? ay->chanmask : 0);

  /* nope, still have to step, to maintain correct AY state
  if ((chanmask&(AY_CHAN_A|AY_CHAN_B|AY_CHAN_C)) == 0) {
    // nothing to do
    if (ay->cpuTCount < nowcycles) ay->cpuTCount = nowcycles;
    return;
  }
  */

  while (ay->cpuTCount < nowcycles) {
    // we want to be at AY tick here
    const uint32_t clf = ay->cpuTCount%ay->cpuTicksDiv;
    if (clf) {
      ay->cpuTCount += ay->cpuTicksDiv-clf;
      continue;
    }

    // the tone level (with envelope)
    for (unsigned g = 0; g < 3; ++g) {
      const uint8_t tlevel = (ay->registers[8+g]&16 ? (ay->envCounter&15) : ay->registers[8+g]&15);
      toneLevel[g] = ay_scaled_levels[tlevel];
      ay->lastvols[g] = tlevel;
    }

    // envelope output counter gets incr'd every 16 AY cycles
    const uint8_t envshape = ay->registers[13];
    uint32_t noiseCount = 0;
    ay->envCycles += AY_CLOCK_DIVISOR;
    while (ay->envCycles >= 16) {
      ay->envCycles -= 16;
      ++noiseCount;
      ++ay->envTick;
      while (ay->envTick >= ay->envPeriod) {
        ay->envTick -= ay->envPeriod;

        // do a 1/16th-of-period incr/decr if needed
        if (ay->envFirst || ((envshape&AY_ENV_CONT) && !(envshape&AY_ENV_HOLD))) {
          if (ay->envRev) {
            ay->envCounter -= (envshape&AY_ENV_ATTACK ? 1 : -1);
          } else {
            ay->envCounter += (envshape&AY_ENV_ATTACK ? 1 : -1);
          }

               if (ay->envCounter < 0) ay->envCounter = 0;
          else if (ay->envCounter > 15) ay->envCounter = 15;
        }

        ++ay->envInternalTick;
        while (ay->envInternalTick >= 16) {
          ay->envInternalTick -= 16;

          // end of cycle
          if (!(envshape&AY_ENV_CONT)) {
            ay->envCounter = 0;
          } else {
            if (envshape&AY_ENV_HOLD) {
              if (ay->envFirst && (envshape&AY_ENV_ALT)) {
                ay->envCounter = (ay->envCounter ? 0 : 15);
              }
            } else {
              // non-hold
              if (envshape&AY_ENV_ALT) {
                ay->envRev = !ay->envRev;
              } else {
                ay->envCounter = (envshape&AY_ENV_ATTACK ? 0 : 15);
              }
            }
          }

          ay->envFirst = 0;
        }

        // don't keep trying if period is zero
        if (!ay->envPeriod) break;
      }
    }

    /* generate tone+noise... or neither.
     * (if no tone/noise is selected, the chip just shoves the
     * level out unmodified. This is used by some sample-playing
     * stuff.)
     */
    ay->chans[0] = toneLevel[0];
    ay->chans[1] = toneLevel[1];
    ay->chans[2] = toneLevel[2];
    const uint8_t mixer = ay->registers[7];

    ay->toneCycles += AY_CLOCK_DIVISOR;
    const uint32_t toneCount = ay->toneCycles>>3;
    ay->toneCycles &= 7;

    for (uint8_t cn = 0; cn < 3; ++cn) {
      if ((mixer&(1u<<cn)) == 0) {
        ay->toneTick[cn] += toneCount;
        if (ay->toneTick[cn] >= ay->tonePeriod[cn]) {
          ay->toneTick[cn] -= ay->tonePeriod[cn];
          ay->toneHigh[cn] = !ay->toneHigh[cn];
        }
        ay->chans[cn] = (ay->toneHigh[cn] ? ay->chans[cn] : 0);
      }
      if ((mixer&(0x08u<<cn)) == 0 && ay->noiseToggle) ay->chans[cn] = 0; //ay->lastvols[cn] = 0;
      if (chanmask&(1u<<cn)) {
        int val = ay->chans[cn];
        if (val < -32767) val = -32767; else if (val > 32767) val = 32767;
        blip_synth_update(ay->synths[cn], ay->cpuTCount, val);
        blip_synth_update(ay->synths[cn+3], ay->cpuTCount, val);
      }
    }

    // update noise RNG/filter
    ay->noiseTick += noiseCount;
    while (ay->noiseTick >= ay->noisePeriod) {
      ay->noiseTick -= ay->noisePeriod;
      #ifdef USE_AYEMUL_NOISE
      ay->rng = (((ay->rng<<1)&0x1ffff)+1)^(((ay->rng>>16)^(ay->rng>>13))&0x01);
      ay->noiseToggle = (ay->rng>>16)&0x01;
      #else
      //noise_reg = (noise_reg>>1)^((noise_reg&1) ? 0x14000 : 0);
      //noise_trigger = noise_reg&1;
      //if ((rng&1)^(rng&2 ? 1 : 0)) noiseToggle = !noiseToggle;
      if ((ay->rng&1)^((ay->rng>>1)&1)) ay->noiseToggle = !ay->noiseToggle;
      // rng is 17-bit shift reg, bit 0 is output. input is bit 0 xor bit 3.
      if (ay->rng&1) ay->rng ^= 0x24000;
      ay->rng >>= 1;
      #endif

      // don't keep trying if period is zero
      if (!ay->noisePeriod) break;
    }

    ay->cpuTCount += ay->cpuTicksDiv;
  }
}


//==========================================================================
//
//  ayemu_write_reg
//
//==========================================================================
void ayemu_write_reg (AYEmu_t *ay, uint8_t reg, uint8_t val, uint32_t nowcycles) {
  if (!ay) return;
  ayemu_render_up_to(ay, nowcycles);
  reg &= 0x0f;
  ay->registers[reg] = val;
  // fix things as needed for some register changes
  switch (reg) {
    case 0: case 1: case 2: case 3: case 4: case 5:
      {
        const uint8_t r = reg>>1;
        // a zero-len period is the same as 1
        ay->tonePeriod[r] = (ay->registers[reg&~1]|(ay->registers[reg|1]&15)<<8);
        if (!ay->tonePeriod[r]) ay->tonePeriod[r] = 1;
        // important to get this right, otherwise e.g. Ghouls 'n' Ghosts has really scratchy, horrible-sounding vibrato
        if (ay->toneTick[r] >= ay->tonePeriod[r]*2) ay->toneTick[r] %= ay->tonePeriod[r]*2;
      }
      break;
    case 6:
      ay->noiseTick = 0;
      ay->noisePeriod = ay->registers[reg]&31;
      break;
    case 11: case 12:
      ay->envPeriod = ay->registers[11]|(ay->registers[12]<<8);
      break;
    case 13:
      ay->envInternalTick = ay->envTick = ay->envCycles = 0;
      ay->envFirst = 1;
      ay->envRev = 0;
      ay->envCounter = (ay->registers[13]&AY_ENV_ATTACK ? 0 : 15);
      break;
    default: break;
  }
}


//==========================================================================
//
//  ayemu_read_reg
//
//==========================================================================
uint8_t ayemu_read_reg (AYEmu_t *ay, uint8_t reg) {
  return (ay ? ay->registers[reg&0x0f] : 0);
}


//==========================================================================
//
//  ayemu_read_last_volumes
//
//  read latest volume levels
//  returns byte with bits set for active (non-muted) channel
//  this is required for bars, because envelope can change volumes
//
//==========================================================================
uint8_t ayemu_read_last_volumes (AYEmu_t *ay, uint8_t vols[3]) {
  if (!ay) {
    if (vols) vols[0] = vols[1] = vols[2] = 0;
    return 0;
  }
  if (vols) {
    for (unsigned f = 0; f < 3; ++f) {
      // converto to [0..31]
      vols[f] = ay->lastvols[f]<<1;
      vols[f] |= !!vols[f];
    }
  }
  return (ay->registers[7]&7)^7;
}


//==========================================================================
//
//  ayemu_write_beeper
//
//==========================================================================
void ayemu_write_beeper (AYEmu_t *ay, uint8_t beeper, uint8_t tape, uint32_t nowts) {
  if (!ay) return;
  if (!ay->synthBeeper[0] || !ay->render_enabled) return;
  if ((ay->chanmask&AY_CHAN_BEEPER) == 0) return;
  //printf("nowts=%u; beeper=%u\n", nowts, beeper); fflush(stdout);
  int val = 0;
  if (tape) val += AmplTape;
  if (beeper) val += AmplBeeper;
  if (val < -32767) val = -32767; else if (val > 32767) val = 32767;
  blip_synth_update(ay->synthBeeper[0], nowts, val);
  blip_synth_update(ay->synthBeeper[1], nowts, val);
}


//==========================================================================
//
//  ayemu_sound_overlay
//
//==========================================================================
void ayemu_sound_overlay (AYEmu_t *ay) {
  if (!ay) return;
  // finish AY frame rendering
  ayemu_render_up_to(ay, ay->cpuTicks);
  // reset tick counter
  ay->cpuTCount = 0;
}
