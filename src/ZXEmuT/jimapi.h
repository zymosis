/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_JIMAPI_H
#define ZXEMUT_JIMAPI_H

#include <stdint.h>
#include <SDL.h>
#include <libspectrum.h>

#include "../libzymosis/zymosis.h"
#include "emucommon.h"
#include "emuvars.h"


////////////////////////////////////////////////////////////////////////////////
typedef struct SDLJimBinding {
  uint16_t sdlk;
  uint16_t modvalue;
  Jim_Obj *action;
  struct SDLJimBinding *next;
} SDLJimBinding;

extern SDLJimBinding *sdlJimBindings;

extern int jim_verbose_loading;


////////////////////////////////////////////////////////////////////////////////
extern void jimInit (void);
extern void jimDeinit (void);


#define  JIM_INVALID_INTVAL  (-2147483648)

// return `JIM_INVALID_INTVAL` on error
// looks in labels, and in Z80 registers
// `allowpgaddr` is used for memory viewer
extern int jimGetIntValEx (Jim_Interp *interp, Jim_Obj *obj, int allowpgaddr);
extern int jimGetIntVal (Jim_Interp *interp, Jim_Obj *obj);


////////////////////////////////////////////////////////////////////////////////
extern int jimEvalFile (const char *fname, int okifabsent);
extern int jimEvalFile1 (const char *fname);

////////////////////////////////////////////////////////////////////////////////
extern void emuInitBindings (void);
extern SDLJimBinding *sdlFindKeyBind (SDLJimBinding *list, uint16_t sdlk, uint16_t modvalue);


////////////////////////////////////////////////////////////////////////////////
typedef struct UIOverlay_t UIOverlay;

extern UIOverlay *uiovlFindById (const char *id);
extern void uiovlDraw (void);
// returns !0 if the key was eaten
extern int uiovlKey (SDL_KeyboardEvent *key);

extern void uiovlInit (void);
extern void uiovlDeinit (void);


#endif
