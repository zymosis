/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_SOUND_ALSA_H
#define ZXEMUT_SOUND_ALSA_H

#include <stdint.h>
#include <SDL.h>
#include <libspectrum.h>

#include "emucommon.h"
#include "emuvars.h"
#include "snd_ayemu.h"

/* Stereo separation types:
 *  * ACB is used in the Melodik interface.
 *  * ABC stereo is used in the Pentagon/Scorpion.
 *  * BAC stereo does seem to exist but is quite rare: Z80Stealth emulates BAC stereo but that's about all.
 *  * CAB, BCA and CBA don't get many search results.
 */

/* default is ABC */
/*
#define SOUND_STEREO_AY_NONE  (0)
#define SOUND_STEREO_AY_ACB   (1)
#define SOUND_STEREO_AY_ABC   (2)
*/

/* default is unfiltered */
/*
#define SOUND_FILTER_TV          (0)
#define SOUND_FILTER_BEEPER      (1)
#define SOUND_FILTER_DEFAULT     (2)
#define SOUND_FILTER_FLAT        (3)
#define SOUND_FILTER_CRISP       (4)

#define SOUND_FILTER_MAX_VALUE  SOUND_FILTER_CRISP
*/

extern AY_Stereo_t soundGetAYStereo (void);
extern void soundSetAYStereo (AY_Stereo_t mode);
extern void soundFullRestart (void);

extern AY_Filter_t soundGetFilter (void);
extern void soundSetFilter (AY_Filter_t mode);

extern int soundIsWorking (void);

extern void deinitSound (void);
extern int initSound (void); //0: ok

extern void soundApplyNewVolumes (void);
extern void soundApplyNewFilters (void);

extern void soundSilenceCurFrame (void);

extern void soundBeeper (int on, int tstates);
extern void soundAYWrite (int reg, int val, int now);
extern void soundAYReset (void);

extern void soundEndFrame (void);
extern void soundWrite (void);
extern void soundSetUse (int on);


#endif
