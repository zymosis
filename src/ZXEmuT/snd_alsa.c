/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include "libvideo/video.h"
#include "snd_alsa.h"
#include "blipbuffer.h"
#include "emuvars.h"
#include "console.h"


#define sndChannels        (2)
#define sndBytesPerSample  (2)

static int sndUsing = 0;
static int sndBlipOverflowWarned = 0;


int soundIsWorking (void) {
  return sndUsing;
}


static AYEmu_t *sndAY = NULL;


static Blip_Buffer *left_buf = NULL;
static Blip_Buffer *right_buf = NULL;


//==========================================================================
//
//  sound_get_effective_processor_speed
//
//  return the emulation speed adjusted processor speed, in Hz
//
//==========================================================================
static unsigned sound_get_effective_processor_speed (void) {
  return machineInfo.cpuSpeed*optSpeed/100;
}


//==========================================================================
//
//  convert_volume_intr
//
//==========================================================================
static inline float convert_volume_intr (int vol) {
  if (vol <= 0) return 0;
  if (vol > 200) vol = 200;
  return (double)vol/100.0f;
}


//==========================================================================
//
//  convert_volume
//
//==========================================================================
static inline float convert_volume (int vol) {
  const float v0 = convert_volume_intr(vol);
  const float v1 = convert_volume_intr(optSoundVolume);
  return v0*v1;
}


//==========================================================================
//
//  sound_init_blip
//
//==========================================================================
static int sound_init_blip (Blip_Buffer **buf, int srate) {
  sndBlipOverflowWarned = 0;

  *buf = new_Blip_Buffer();

  blip_buffer_set_clock_rate(*buf, sound_get_effective_processor_speed());
  /* Allow up to 1s of playback buffer - this allows us to cope with slowing
     down to 2% of speed where a single Speccy frame generates just under 1s
     of sound */
  if (blip_buffer_set_sample_rate(*buf, srate, 1000) < 0) {
    cprintf("\4ERROR: cannot init blip buffer!\n");
    return -1;
  }

  return 0;
}


#ifdef USE_SOUND
#include <alsa/asoundlib.h>

static char *alsaDev = "default";
static snd_pcm_t *playback_handle = NULL;

#define SND_BUFFERS_IN_RING  (4)

static int16_t sndBuffer16[SND_BUFFERS_IN_RING*48000];
static int sndOldVal = 0;
static int sndBeeperLastSubVol = 0;
static int sndFrameSize = 1; // samples per frame
static int sndBufFillPos = 0;
static int sndBufPlayPos = 0;

static int sndAYWasWrite = 0;

static AY_Stereo_t ayStereo = AY_STEREO_ACB;
static AY_Filter_t ayFilter = AY_SPEAKER_DEFAULT;


//==========================================================================
//
//  recalc_ts
//
//==========================================================================
static inline uint32_t recalc_ts (uint32_t ts) {
  double tsf = (int)ts;
  //tsf = (tsf/(float)machineInfo.tsperframe)*((float)machineInfo.aySpeed*2.0f/50.0f);
  //tsf = (tsf*(double)machineInfo.aySpeed*2.0)/((double)machineInfo.tsperframe*50.0);
  tsf = (tsf*(double)machineInfo.aySpeed)/((double)machineInfo.tsperframe*25.0);
  uint32_t res = (uint32_t)(int)tsf;
  const uint32_t aytspf = (uint32_t)machineInfo.aySpeed*25U;
  if (res >= aytspf) res = aytspf-1;
  return res;
}


//==========================================================================
//
//  deinitSound
//
//==========================================================================
void deinitSound (void) {
  if (playback_handle != NULL) {
    snd_pcm_drop(playback_handle);
    snd_pcm_close(playback_handle);
    playback_handle = NULL;
    sndSampleRate = 0;
    sndUsing = 0;
    ayemu_destroy(&sndAY);
    delete_Blip_Buffer(&left_buf);
    delete_Blip_Buffer(&right_buf);
  }
}


#define ALSA_ISND_CALL(fcl,msg,...)  do { \
  int err; \
  if ((err = (fcl)) < 0) { \
    fprintf(stderr, msg, ##__VA_ARGS__, snd_strerror(err)); \
    deinitSound(); \
    return -1; \
  } \
} while (0)


//==========================================================================
//
//  initSoundHW
//
//==========================================================================
static int initSoundHW (int samplerate) {
  snd_pcm_hw_params_t *hw_params = NULL;
  snd_pcm_sw_params_t *sw_params = NULL;
  unsigned int sr;
  snd_pcm_uframes_t buffer_size;

  sndSampleRate = samplerate;
  sndFrameSize = sndSampleRate/50;
  sr = sndSampleRate;

  memset(sndBuffer16, 0, sizeof(sndBuffer16));

  sndBeeperLastSubVol = 0;
  sndOldVal = 0;
  sndBufFillPos = 0;
  sndBufPlayPos = 0;
  sndUsing = 0;
  buffer_size = sndFrameSize*SND_BUFFERS_IN_RING;
  //
  ALSA_ISND_CALL(snd_pcm_open(&playback_handle, alsaDev, SND_PCM_STREAM_PLAYBACK, 0), "cannot open audio device %s (%s)\n", alsaDev);
  ALSA_ISND_CALL(snd_pcm_hw_params_malloc(&hw_params), "cannot allocate hardware parameter structure (%s)\n");
  ALSA_ISND_CALL(snd_pcm_hw_params_any(playback_handle, hw_params), "cannot initialize hardware parameter structure (%s)\n");
  ALSA_ISND_CALL(snd_pcm_hw_params_set_access(playback_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED), "cannot set access type (%s)\n");
  ALSA_ISND_CALL(snd_pcm_hw_params_set_format(playback_handle, hw_params, SND_PCM_FORMAT_S16_LE), "cannot set sample format (%s)\n");
  ALSA_ISND_CALL(snd_pcm_hw_params_set_rate_near(playback_handle, hw_params, &sr, 0), "cannot set sample rate (%s)\n");
  ALSA_ISND_CALL(snd_pcm_hw_params_set_buffer_size_near(playback_handle, hw_params, &buffer_size), "cannot set buffer size (%s)\n");
  cprintf("real soundcard sample rate: %u\nsound buffer sizes (render/real): %d/%d\n", sr, sndFrameSize*SND_BUFFERS_IN_RING, (int)buffer_size);
  //printf("sample rate: %u\n", sr);
  sndSampleRate = sr;
  sndFrameSize = sndSampleRate/50;
  ALSA_ISND_CALL(snd_pcm_hw_params_set_channels(playback_handle, hw_params, sndChannels), "cannot set channel count (%s)\n");
  ALSA_ISND_CALL(snd_pcm_hw_params(playback_handle, hw_params), "cannot set parameters (%s)\n");
  snd_pcm_hw_params_free(hw_params);
  //
  ALSA_ISND_CALL(snd_pcm_sw_params_malloc(&sw_params), "cannot allocate software parameters structure (%s)\n");
  ALSA_ISND_CALL(snd_pcm_sw_params_current(playback_handle, sw_params), "cannot initialize software parameters structure (%s)\n");
  ALSA_ISND_CALL(snd_pcm_sw_params_set_avail_min(playback_handle, sw_params, sndFrameSize), "cannot set minimum available count (%s)\n");
  ALSA_ISND_CALL(snd_pcm_sw_params_set_start_threshold(playback_handle, sw_params, 0U), "cannot set start mode (%s)\n");
  ALSA_ISND_CALL(snd_pcm_sw_params(playback_handle, sw_params), "cannot set software parameters (%s)\n");
  ALSA_ISND_CALL(snd_pcm_nonblock(playback_handle, 0), "cannot set blocking mode (%s)\n");
  ALSA_ISND_CALL(snd_pcm_prepare(playback_handle), "cannot prepare audio interface for use (%s)\n");

  sndUsing = 1;

  if (sound_init_blip(&left_buf, sndSampleRate) != 0) { deinitSound(); return -1; }
  if (sound_init_blip(&right_buf, sndSampleRate) != 0) { deinitSound(); return -1; }

  if (sndAY) ayemu_destroy(&sndAY);
  sndAYWasWrite = 0;
  sndAY = ayemu_create(machineInfo.aySpeed*2/50, 2, 1/*allowBeeper*/, left_buf, right_buf);
  ayemu_set_stereo_mode(sndAY, ayStereo);
  ayemu_set_filter(sndAY, ayFilter);
  ayemu_set_volume(sndAY, convert_volume(optSoundVolumeAY));
  ayemu_set_volume_beeper(sndAY, convert_volume(optSoundVolumeBeeper));

  return 0;
}


//==========================================================================
//
//  initSound
//
//==========================================================================
int initSound (void) {
  const int srates[2] = {48000, 44100};
  for (unsigned f = 0; f < 2; ++f) {
    alsaDev = "default";
    if (initSoundHW(srates[f]) == 0) return 0;
    alsaDev = "plug:default";
    if (initSoundHW(srates[f]) == 0) return 0;
  }
  playback_handle = NULL;
  sndSampleRate = 0;
  sndUsing = 0;
  return -1;
}


//==========================================================================
//
//  soundGetAYStereo
//
//==========================================================================
AY_Stereo_t soundGetAYStereo (void) {
  return ayStereo;
}


//==========================================================================
//
//  soundSetAYStereo
//
//==========================================================================
void soundSetAYStereo (AY_Stereo_t mode) {
  if (ayStereo != mode) {
    ayStereo = mode;
    if (sndAY) ayemu_set_stereo_mode(sndAY, mode);
  }
}


//==========================================================================
//
//  soundApplyNewVolumes
//
//==========================================================================
void soundApplyNewVolumes (void) {
  if (sndAY) {
    ayemu_set_volume(sndAY, convert_volume(optSoundVolumeAY));
    ayemu_set_volume_beeper(sndAY, convert_volume(optSoundVolumeBeeper));
  }
}


//==========================================================================
//
//  soundFullRestart
//
//==========================================================================
void soundFullRestart (void) {
  if (sndUsing) {
    deinitSound();
    initSound();
  }
}


//==========================================================================
//
//  soundGetFilter
//
//==========================================================================
AY_Filter_t soundGetFilter (void) {
  return ayFilter;
}


//==========================================================================
//
//  soundSetFilter
//
//==========================================================================
void soundSetFilter (AY_Filter_t mode) {
  if (ayFilter != mode) {
    ayFilter = mode;
    if (sndAY) ayemu_set_filter(sndAY, mode);
  }
}


//==========================================================================
//
//  soundAYWrite
//
//==========================================================================
void soundAYWrite (const int reg, const int val, int now) {
  if (sndAY) {
    ayemu_set_enabled_chans(sndAY,
      (optSoundBeeper ? AY_CHAN_BEEPER : 0)|
      (optSoundAY ? AY_CHAN_A|AY_CHAN_B|AY_CHAN_C : 0));
    ayemu_write_reg(sndAY, reg, val, recalc_ts(now));
  }
}


//==========================================================================
//
//  soundBeeper
//
//==========================================================================
void soundBeeper (int on, int at_tstates) {
  if (sndAY) {
    ayemu_set_enabled_chans(sndAY,
      (optSoundBeeper ? AY_CHAN_BEEPER : 0)|
      (optSoundAY ? AY_CHAN_A|AY_CHAN_B|AY_CHAN_C : 0));
    ayemu_write_beeper(sndAY, on, 0/*tape*/, recalc_ts(at_tstates));
  }
}


//==========================================================================
//
//  soundAYReset
//
//  no need to call this initially, but should be called on reset otherwise
//
//==========================================================================
void soundAYReset (void) {
  if (sndAY) {
    ayemu_force_frequency(sndAY, machineInfo.aySpeed*2/50, 2);
    ayemu_reset(sndAY);
    sndAYWasWrite = 0;
    blip_buffer_clear(left_buf, 1);
    blip_buffer_clear(right_buf, 1);
    memset(sndBuffer16, 0, sizeof(sndBuffer16));
  }
}


//==========================================================================
//
//  soundSilenceCurFrame
//
//==========================================================================
void soundSilenceCurFrame (void) {
  memset(sndBuffer16+sndBufFillPos, 0, sndFrameSize*sndChannels*sndBytesPerSample);
  if (sndAYWasWrite) {
    ayemu_reset_registers(sndAY);
    sndAYWasWrite = 0;
  }
}


//==========================================================================
//
//  sndNextBufPlayPos
//
//==========================================================================
static inline int sndNextBufPlayPos (void) {
  int res = sndBufPlayPos+sndFrameSize*sndChannels;
  if (res/sndFrameSize*sndChannels >= SND_BUFFERS_IN_RING) res = 0;
  return res;
}


//==========================================================================
//
//  sndNextBufFillPos
//
//==========================================================================
static inline int sndNextBufFillPos (void) {
  int res = sndBufFillPos+sndFrameSize*sndChannels;
  if (res/sndFrameSize*sndChannels >= SND_BUFFERS_IN_RING) res = 0;
  return res;
}


//==========================================================================
//
//  soundEndFrame
//
//==========================================================================
void soundEndFrame (void) {
  ayemu_set_enabled_chans(sndAY,
    (optSoundBeeper ? AY_CHAN_BEEPER : 0)|
    (optSoundAY ? AY_CHAN_A|AY_CHAN_B|AY_CHAN_C : 0));

  if (!sndUsing) return;

  /* overlay AY sound */
  ayemu_sound_overlay(sndAY);

  blip_buffer_end_frame(left_buf, machineInfo.tsperframe);
  blip_buffer_end_frame(right_buf, machineInfo.tsperframe);

  int16_t *samples = sndBuffer16+sndBufFillPos;

  /* read left channel into even samples, right channel into odd samples: LRLRLRLRLR... */
  /* note that we setup the synths with the same params, so they are always in sync */
  uint32_t count = blip_buffer_read_samples(left_buf, samples, sndFrameSize, BLIP_READ_INTERLEAVE);
  blip_buffer_read_samples(right_buf, samples+1, count, 1);

  /*
    k8: the emulator doesn't consume the whole buffer (this seems to be my rounding bug).
        the result of that is buffer accumulating samples, and
        it will eventually overflow, causing memory corruption.
        so just forcefully clear the buffer, dropping all samples we didn't used on this frame.
        it seems that sometimes there is just one extra sample left, but
        this causes memory corruption after running the emu for ~40 minutes.
  */

  // warn if something is VERY wrong
  if (!sndBlipOverflowWarned && blip_buffer_samples_avail(left_buf) >= sndFrameSize/32) {
    sndBlipOverflowWarned = 1;
    cprintf("WARNING! BlipBuffer overflowed by %d frames out of %d; the sound will be wildely distorted!\n",
            (int)blip_buffer_samples_avail(left_buf), (int) sndFrameSize);
  }

  //fprintf(stderr, "*** count=%d; framesize=%d; avail=%d\n", (int)count, sndFrameSize, (int)blip_buffer_samples_avail(left_buf));

  // remove rest of the samples, we don't need 'em
  blip_buffer_remove_samples(left_buf, blip_buffer_samples_avail(left_buf));
  blip_buffer_remove_samples(right_buf, blip_buffer_samples_avail(right_buf));

  //fprintf(stderr, "count=%d; framesize=%d\n", (int)count, sndFrameSize);

  if (count == 0) {
    memset(sndBuffer16+sndBufFillPos, 0, sndFrameSize*sndChannels*sndBytesPerSample);
  } else {
    /* fill frame with the last rendered byte */
    /* k8: this is wrong, but meh... */
    const int16_t l = samples[0];
    const int16_t r = samples[1];
    int16_t *sptr = samples+count*sndChannels;
    while (count++ < sndFrameSize) {
      *sptr++ = l;
      *sptr++ = r;
    }
  }
}


//==========================================================================
//
//  soundMoveToNewFrame
//
//==========================================================================
static inline void soundMoveToNewFrame (void) {
  sndBufFillPos = sndNextBufFillPos();
}


//==========================================================================
//
//  sndWriteSoundToCard
//
//  <0: fail!
//
//==========================================================================
static int sndWriteSoundToCard (int silent) {
  int err, res = 0;
  //
  if ((err = snd_pcm_writei(playback_handle, sndBuffer16+sndBufPlayPos, sndFrameSize)) < 0) {
    res = -1;
    if (!debuggerActive && !optPaused && (optSoundBeeper || optSoundAY)) {
      if (err == -EPIPE) {
        if (!silent) fprintf(stderr, "ALSA: underrun!\n");
      } else {
        if (!silent) fprintf(stderr, "ALSA: write failed (%s)\n", snd_strerror(err));
      }
    }
    snd_pcm_recover(playback_handle, err, 1);
  } else {
    sndBufPlayPos = sndNextBufPlayPos();
  }
  //
  return res;
}


void soundWrite (void) {
  if (sndUsing) {
    int err;
    if (debuggerActive || optPaused) soundSilenceCurFrame();
    #if 1
    {
      static int64_t mcsFrameEndWanted = 0;
      int mcsInFrame = 20*1000;
      static int pbsgen = 0;
      if (mcsFrameEndWanted == 0) {
        //fprintf(stderr, "initializing sound buffers...\n");
        for (int f = 0; f <= SND_BUFFERS_IN_RING/2; ++f) {
          soundSilenceCurFrame();
          if ((err = snd_pcm_writei(playback_handle, sndBuffer16+sndBufPlayPos, sndFrameSize)) < 0) {
            snd_pcm_recover(playback_handle, err, 1);
          }
          sndBufPlayPos = sndNextBufPlayPos();
        }
        mcsFrameEndWanted = timerGetMicroSeconds()+mcsInFrame;
      } else {
        int64_t mcsCurFrameEnd;
        if (sndWriteSoundToCard(1) < 0) {
          // ALSA underrun, do something
          int cnt = 0, ofp = sndBufFillPos;
          while (sndBufPlayPos != sndBufFillPos) {
            ++cnt;
            sndBufPlayPos = sndNextBufPlayPos();
          }
          sndBufPlayPos = ofp;
          if (optSndSyncDebug) fprintf(stderr, "sndsync buffered: %d\n", cnt);
          if (cnt < SND_BUFFERS_IN_RING/2) {
            // out of prebuffered sound, generate some
            pbsgen = SND_BUFFERS_IN_RING/2;
          }
          if (optSndSyncDebug) fprintf(stderr, "sndsync: %d\n", pbsgen);
          sndWriteSoundToCard(0);
        }
        mcsCurFrameEnd = timerGetMicroSeconds();
        mcsInFrame = (pbsgen > 0 ? 17 : 20)*1000-50;
        if (pbsgen > 0) --pbsgen;
        if (mcsCurFrameEnd > 0) {
          int mcsSleep = (mcsFrameEndWanted-mcsCurFrameEnd);
          if (mcsSleep > 0) {
            // less than 20 ms
            usleep(mcsSleep);
            mcsFrameEndWanted += mcsInFrame;
          } else {
            if (optSndSyncDebug) fprintf(stderr, "sndsync: DESYNC! (%d)\n", mcsSleep);
            mcsFrameEndWanted = mcsCurFrameEnd+mcsInFrame;
          }
        } else {
          //FIXME
          // reinit timer
          if (optSndSyncDebug) fprintf(stderr, "sndsync: reinit timer\n");
          timerReinit();
          mcsFrameEndWanted = timerGetMicroSeconds()+mcsInFrame;
        }
      }
    }
    #else
    if ((err = snd_pcm_wait(playback_handle, 20)) < 0) {
      fprintf(stderr, "ALSA: wait failed (%s)\n", snd_strerror(err));
      snd_pcm_recover(playback_handle, err, 1);
    }
    //
    sndWriteSoundToCard(0);
    #endif
  }
  soundMoveToNewFrame();
}


//==========================================================================
//
//  soundSetUse
//
//==========================================================================
void soundSetUse (int on) {
  if (playback_handle != NULL && sndSampleRate > 0) {
    int err;
    //
    if (on) {
      if (!sndUsing) {
        sndUsing = 1;
        soundMoveToNewFrame();
        soundSilenceCurFrame();
        if ((err = snd_pcm_prepare(playback_handle)) < 0) {
          fprintf(stderr, "ALSA: prepare failed (%s)\n", snd_strerror(err));
          snd_pcm_recover(playback_handle, err, 1);
        }
        blip_buffer_clear(left_buf, 1);
        blip_buffer_clear(right_buf, 1);
      }
      ayemu_set_render_enabled(sndAY, 1);
    } else {
      if (sndUsing) {
        soundMoveToNewFrame();
        soundSilenceCurFrame();
        if ((err = snd_pcm_drop(playback_handle)) < 0) {
          fprintf(stderr, "ALSA: drop failed (%s)\n", snd_strerror(err));
          //snd_pcm_recover(playback_handle, err, 1);
        }
        sndUsing = 0;
        blip_buffer_clear(left_buf, 1);
        blip_buffer_clear(right_buf, 1);
      }
      ayemu_set_render_enabled(sndAY, 0);
    }
  } else {
    sndUsing = 0;
    ayemu_set_render_enabled(sndAY, 0);
  }
}


#else
int initSound (void) {
  sound_init_blip(&left_buf, 48000);
  sound_init_blip(&right_buf, 48000);
  sndAY = ayemu_create(70000, 2, 1/*allowBeeper*/, left_buf, right_buf);
  return -1;
}


void deinitSound (void) {
}


void soundBeeper (int on, int tstates) {
}


void soundEndFrame (void) {
}


void soundWrite (void) {
}


void soundSetUse (int on) {
}


void soundSilenceCurFrame (void) {
}
#endif
