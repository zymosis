/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
static int tapDetectInCount = 0;
static int tapDetectLastInTS = -666;
static int tapDetectLastInB = 0x00;


void emuTapeAcceleratorEdgeAdvance (int flags) {}
void emuTapeAcceleratorStop (void) {}
void emuTapeAcceleratorStart (void) {}


void emuTapeAcceleratorAdvanceFrame (void) {
  if (tapDetectLastInTS > -100666) tapDetectLastInTS -= machineInfo.tsperframe;
}


void emuTapeLoaderDetector (void) {
  int tdelta = z80.tstates-tapDetectLastInTS;
  uint8_t bdelta = ((int)z80.bc.b-tapDetectLastInB)&0xff;
  //
  tapDetectLastInTS = z80.tstates;
  tapDetectLastInB = z80.bc.b;
  //
  if (optTapeDetector) {
    // detector is on
    if (optTapePlaying) {
      // tape is playing
      if (tdelta > 1000 || (bdelta != 1 && bdelta != 0 && bdelta != 0xff)) {
        if (++tapDetectInCount >= 2) {
          // seems that this is not a loader anymore
          emuStopTape();
        }
      } else {
        tapDetectInCount = 0;
      }
    } else {
      // tape is not playing
      if (tdelta <= 500 && (bdelta == 1 || bdelta == 0xff)) {
        if (zxCurTape != NULL && !tapNeedRewind) {
          if (++tapDetectInCount >= 10) {
            // seems that this is LD-EDGE
            emuStartTapeAuto();
          }
        } else {
          tapDetectInCount = 0;
        }
      } else {
        tapDetectInCount = 0;
      }
    }
  } else {
    // detector is off
    tapDetectInCount = 0;
  }
}
