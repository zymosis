/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_VARS_H
#define ZXEMUT_VARS_H

#include <stdint.h>
#include <SDL.h>
#include <libspectrum.h>

#include "../libzymosis/zymosis.h"
#include "emucommon.h"
#include "../libfusefdc/libfusefdc.h"


extern int optSndSyncDebug;


typedef struct {
  const char *modelname; // may be dynamically allocated, tho
  //
  int cpuSpeed; // in HZ
  int aySpeed; // in HZ
  int romPages;
  //
  int leftBorder; // tstates for left border
  int hscreen; // tstates for horizontal screen line
  int rightBorder; // tstates for right border
  int hretrace; // tstates for horizontal retrace
  //
  int topBorder; // lines in top border
  int vscreen; // lines in screen
  int bottomBorder; // lines in bottom border
  int vretrace; // lines for vertical retrace
  //
  int topleftpixts; // time from the frame start when machine displays first pixel (tstates)
  int intrlen; // /INTR hold time in tstates
  int port7ffd; // <0: none; >=0: initial value
  int port1ffd; // <0: none; >=0: initial value
  int evenM1; // bool
  int haveSnow; // bool
  int floatingBus; // 0:none; 1:standard; 2:+2
  int contention; // bool
  int iocontention; // bool
  int genuine; // for ULA emulation
  int usePlus3Contention;
  int p3dos; // use +3DOS instead of TR-DOS?
  int port7ffDbug; // bool
} ZXBasicMachineInfo;


typedef struct {
  const char * modelname;
  //
  int cpuSpeed; // in HZ
  int aySpeed; // in HZ
  int romPages;
  //
  int leftBorder; // tstates for left border
  int hscreen; // tstates for horizontal screen line
  int rightBorder; // tstates for right border
  int hretrace; // tstates for horizontal retrace
  //
  int topBorder; // lines in top border
  int vscreen; // lines in screen
  int bottomBorder; // lines in bottom border
  int vretrace; // lines for vertical retrace
  //
  int topleftpixts; // time from the frame start when machine displays first pixel (tstates)
  int intrlen; // /INTR hold time in tstates
  int port7ffd; // <0: none; >=0: initial value
  int port1ffd;
  int evenM1;
  int haveSnow;
  int floatingBus;
  int contention; // 0:none; 1: standard; 2: +3
  int iocontention; // 0:none; 1: standard; 2: +3
  int origContention;
  int origIOContention;
  int usePlus3Contention;
  int genuine;
  int p3dos; // use +3DOS instead of TR-DOS?
  int port7ffDbug; // bool
  // calculated
  int linesperframe;
  int tsperline;
  int tsperframe;
  int tsdrawstart;
} ZXMachineInfo;


// WARNING! keep in sync with `basicMachineInfo`!
enum {
  ZX_MACHINE_48K,
  ZX_MACHINE_128K,
  ZX_MACHINE_PLUS2,
  ZX_MACHINE_PLUS2A,
  ZX_MACHINE_PLUS3,
  ZX_MACHINE_PENTAGON,
  ZX_MACHINE_SCORPION,
  ZX_MACHINE_MAX,
};

// WARNING! keep in sync with `ZX_MACHINE_XXX` enum!
extern const ZXBasicMachineInfo basicMachineInfo[ZX_MACHINE_MAX];


extern Jim_Interp *jim;

extern int optConDump;
extern int optConDumpToStdout;

extern libspectrum_rzx *zxRZX;
extern int zxRZXRCount;
extern int zxRZXFrames;
extern int zxRZXCurFrame;

extern libspectrum_snap *zxSLT;

extern libspectrum_tape *zxCurTape;
extern int optTapePlaying;
extern int optTapeSound;
extern int tapeAlpha;

extern int tapNextEdgeTS; // can exceed tsperframe or be negative
extern int tapNextEdge;
extern int tapStopAfterNextEdge; // <0: stop on 48k; >0: stop anyway
extern int tapCurEdge; // 0: low; !0: high
extern int tapNeedRewind;
//
extern int optTapeAutoPauseEmu; // not reliable yet
extern int optTapeMaxSpeed;
extern int optTapeDetector;
extern EMU_MAYBE_UNUSED int optTapeAccelerator;

extern int diskAlpha;
extern int64_t diskLastActivity;

extern uint8_t zxLastOut7ffd;
extern uint8_t zxLastOut1ffd;
extern uint8_t zxAYRegs[16];

extern upd_fdc *upd765_fdc;
extern fdd_t upd765_drives[2];
extern disk_t flpdisk[4];
extern int optAutoaddBoot;

// can be out of bounds (in border, etc.)!
extern int kmouseAbsX, kmouseAbsY;

extern int optKJoystick;
extern int optKMouse;
extern int optKMouseAbsolute; // "absolute" mode?
extern int optKMouseStrict;
extern int optKMouseSwapButtons;
extern int zxMouseWasMoved;
extern int zxMouseLastX;
extern int zxMouseLastY;
extern int zxMouseFracX;
extern int zxMouseFracY;
extern uint8_t zxKMouseDX;
extern uint8_t zxKMouseDY;
extern int zxKMouseSpeed; // how much host pixels mouse should move before ZX mouse will be moved?
//extern int zxKMouseDXAccum;
//extern int zxKMouseDYAccum;
extern uint8_t zxKMouseWheel;
extern uint8_t zxKMouseButtons;

extern zym_cpu_t z80;
extern int z80GenerateNMI; // will be reset in z80Step()

extern int zxModel;
extern int zxPentagonMemory; // in kb


// arguments for emuSetPaused()
enum {
  PAUSE_PERMANENT_SET = 1,
  PAUSE_PERMANENT_RESET = 0,
  PAUSE_TEMPORARY_SET = 2,
  PAUSE_TEMPORARY_RESET = 3,
  //
  PAUSE_PERMANENT_MASK = 0x01,
  PAUSE_TEMPORARY_MASK = 0x02
};

extern int optPaused; // bit0: permanent pause; bit1: temporary pause
extern int optDrawFPS;
extern int emuFrameCount;
extern int64_t emuFrameStartTime;
extern char emuLastFPSText[128];
extern int64_t emuPrevFCB;

extern int optCurBlinkMS;
extern int conVisible;
extern int conAlpha;
extern int optMaxSpeed;
extern int optSpeed; // in percents
extern int optNoScreenReal;
extern int optSlowShade; // "shade" screen if speed is < 100?
extern int zxLateTimings;
extern int optDrawKeyLeds;
extern int klAlpha;
extern int zxScreenOfs;
extern int optEmulateSnow;
extern int optTRDOSenabled;
extern int zxTRDOSactivated;
extern int optTRDOSROMIndex;
extern int zxTRDOSPagedIn;
extern int optTRDOSTrapDebug;
extern int optTRDOSTraps;
extern int optKeyHelpVisible;
extern int optAllowOther128;
extern int optEmulateMatrix;
extern int optZXIssue; // 0: issue2; 1: issue3; 2: +3
extern int optFDas7FFD; // use only low byte to decode 7ffd
extern int zx7ffdLocked;
extern uint8_t zxLastContendedMemByte; // required for +2A/+3 floating bus emulation
extern int optSnapSetModel;

extern int optNoFlic; // 0: off; 1: on; -1: autodetect
extern int optNoFlicDetected; // bool
extern int emuLastScreenBank;
extern int emuScreenBankSwitchCount; // <0: waiting for deactivation
extern int emuCurrScreenBank;
extern int emuNoflicMsgCooldown;

extern int optBrightBlack;
extern int optBrightBlackValue;

extern int optAutofire; // bool
extern int optAutofireHold; // frames to hold (1)
extern int optAutofireDelay; // frames to delay (1)

extern int afireDown; // current state
extern int afireLeft; // frames left to next state change
extern int afirePortBit; // hi byte: port number; lo byte: bit to reset or set(kempston)

extern int sndSampleRate;

extern int optSoundBeeper;
extern int optSoundAY;
extern int optSoundVolume;
extern int optSoundVolumeBeeper;
extern int optSoundVolumeAY;
extern int zxLastOutFFFD; // current register

extern int optAYEnabled;

extern int zxGluckROM;
extern int optUseGluck;
extern int zxWasRAMExec; // for Mr.Gluck Reset Service

extern int zxQCmdrROM;
extern int optUseQCmdr;

extern int zxLineStartTS[400];
extern uint16_t zxUlaSnow[192][32];
extern int zxWasUlaSnow;
extern uint8_t zxUlaOut;
extern uint16_t zxScrLineOfs[192];
extern Uint8 zxScreen[2][400*352];
extern int zxScreenCurrent;
extern int zxOldScreenTS;
extern Uint8 zxBorder;
extern uint8_t *zxScreenBank;
extern int zxFlashTimer; // 0..15
extern int zxFlashState;
extern int zxMaxMemoryBank;
extern uint8_t *zxMemoryBanks[256]; // memory banks
extern int zxMaxROMMemoryBank;
extern uint8_t zxMemBanksDirty[256]; // non-zero if something was written in the bank
extern uint8_t *zxROMBanks[256]; // memory banks
extern uint8_t *zxMemory[4]; // paged in memory (page 0: ROM)
extern uint8_t zxMemoryBankNum[4]; // paged in memory (page 0: ROM)
extern uint8_t zxKeyboardState[9]; // 8: kempston
extern uint8_t zxContentionTable[2][80000]; //frame can never have more than 80000 tstates; [0]: MREQ

extern int zxLastPagedROM; // TR-DOS replaces this
extern int zxLastPagedRAM0; // <0: none

extern uint32_t zxKeyBinds[65536];

enum {
  ZOPT_FTP_NONE,
  ZOPT_FTP_RO,
  ZOPT_FTP_RW,
};

extern int optFileTrapsMode; // `ZOPT_FTP_xx`, default: r/w

extern ZXMachineInfo machineInfo;

extern uint64_t dbgTickCounter;
extern int dbtTickCounterPaused;
extern int optAllowZXEmuTraps;

extern int optDebugFlashLoad;


////////////////////////////////////////////////////////////////////////////////
extern int dbgAlpha;
extern int debuggerActive;
extern int memviewActive;
extern int sprviewActive;

extern int dbgIntrHit;
extern int dbgNMIHit;


////////////////////////////////////////////////////////////////////////////////
extern int sndAllowUseToggle;


////////////////////////////////////////////////////////////////////////////////
extern int snapWasDisk; // bit flags for 4 drives
extern int snapWasCPCDisk; // bit flags for 4 drives
extern int snapWasTape;
extern int optBrightBorder; // 1: use bit 6 for border brightness
extern int optFlashLoad;

extern int optMaxSpeedBadScreen;
extern int optMaxSpeedBadScreenTape; // set by tape accel, `true` overrides `optMaxSpeedBadScreen`
extern int optTapeAllowMaxSpeedUgly;

extern int optOpenSE; // use OpenSE Basic instead of that in standard ROM?


#endif
