/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/

// split names a-la 'abc.zip:file.trd' or 'a:abc.zip:file.trd'
// return disk id (4 if no disk given), -1 on invalid name
static int zipNameSplit (const char *name, char **arcname, char **filename) {
  int diskid = 4;
  const char *cp;
  if (arcname != NULL) *arcname = NULL;
  if (filename != NULL) *filename = NULL;
  if (name == NULL || !name[0]) return -1;
  if (name[1] == ':' && strchr("ABCDabcd", name[0]) != NULL) {
    // disk id is here
    diskid = tolower(name[0])-'a';
    name += 2;
    if (!name[0]) return -1; // alas
  }
  if ((cp = strchr(name, ':')) == NULL || cp == name || !cp[1]) {
    // ordinary name
    if (arcname != NULL) *arcname = strdup(name);
  } else {
    // split name
    if (arcname != NULL) {
      *arcname = calloc(cp-name+1, sizeof(char));
      memcpy(*arcname, name, cp-name);
    }
    if (filename != NULL) *filename = strdup(cp+1);
  }
  return diskid;
}


#ifdef USE_LIBZIP
#include <zip.h>


static int isGoodZIP (const char *name) {
  struct zip *zz = zip_open(name, /*ZIP_CHECKCONS|*/0, NULL);
  if (zz != NULL) {
    zip_close(zz);
    return 1;
  }
  //fprintf(stderr, "not a zip: %s\n", name);
  return 0;
}


static int isZIP (const char *name) {
  char *arcname;
  int res = 0;
  if (zipNameSplit(name, &arcname, NULL) < 0) return 0;
  if (isExtEqu(arcname, ".zip")) res = isGoodZIP(arcname);
  free(arcname);
  return res;
}


static char *loadZIPWholeFile (const char *fname, size_t *size, const char **newname) {
  static char namebuf[1024]; // should be enough
  struct zip *zz;
  int64_t cnt, scridx = -1, fileidx = -1, fsize = 0;
  struct zip_stat zstat;
  char *buf;
  char *arcname, *filename;
  //int did;
  if (size) *size = 0;
  if (zipNameSplit(fname, &arcname, &filename) < 0) return NULL;
  //fprintf(stderr, "diskid: %d; arcname=[%s]; filename=[%s]\n", did, arcname, filename);
  zz = zip_open(arcname, 0, NULL);
  free(arcname);
  if (zz == NULL) return NULL;
  //
  if ((cnt = zip_get_num_files/*zip_get_num_entries*/(zz/*, ZIP_FL_UNCHANGED*/)) == 0) {
    // no files
    if (filename != NULL) free(filename);
    zip_close(zz);
    return NULL;
  }
  //fprintf(stderr, "cnt=%d\n", (int)cnt);
  if (cnt > 1) {
    for (int64_t f = 0; f < cnt; ++f) {
      if (zip_stat_index(zz, f, ZIP_FL_UNCHANGED, &zstat) != 0) continue;
      /*
      if (!(zstat.valid&ZIP_STAT_NAME)) continue;
      if (!(zstat.valid&ZIP_STAT_SIZE)) continue;
      */
      //fprintf(stderr, "%s: encryption_method=%u\n", zip_get_name(zz, f, ZIP_FL_UNCHANGED), zstat.encryption_method);
      if (zstat.encryption_method != 0) continue;
      const char *nn = zip_get_name(zz, f, ZIP_FL_UNCHANGED);
      if (!nn[0] || nn[strlen(nn)-1] == '/' || nn[strlen(nn)-1] == '\\') continue; // skip dirs
      if (filename != NULL) {
        if (strcmp(nn, filename) == 0) {
          cprintf("REQT: %s\n", nn);
          fileidx = f;
          fsize = zstat.size;
          continue;
        }
      }
      if (isExtEqu(nn, ".scr") || isExtEqu(nn, ".scb")) {
        cprintf("SCR$: %s\n", nn);
        if (filename == NULL && scridx < 0 && fileidx < 0) {
          scridx = f;
          fsize = zstat.size;
        }
      } else {
        for (int c = 0; snapshotExtensions[c] != NULL; ++c) {
          if (strcmp(snapshotExtensions[c], ".zip") && isExtEqu(nn, snapshotExtensions[c])) {
            cprintf("SNAP: %s\n", nn);
            if (filename == NULL && fileidx < 0) {
              fileidx = f;
              fsize = zstat.size;
            }
            break;
          }
        }
      }
      //if (fileidx >= 0) break;
    }
    if (filename != NULL) free(filename);
    if (fsize < 1 || fsize > 1024*1024*32 || (fileidx < 0 && scridx < 0)) {
      // no files
      zip_close(zz);
      return NULL;
    }
  } else {
    // only one file, try it anyway
    if (filename != NULL) {
      const char *nn = zip_get_name(zz, 0, ZIP_FL_UNCHANGED);
      if (!nn[0] || nn[strlen(nn)-1] == '/' || nn[strlen(nn)-1] == '\\' || strcmp(nn, filename)) {
        free(filename);
        zip_close(zz);
        return NULL;
      }
      free(filename);
    } else if (zip_stat_index(zz, 0, ZIP_FL_UNCHANGED, &zstat) != 0 || zstat.encryption_method != 0) {
      zip_close(zz);
      return NULL;
    }
    fsize = zstat.size;
    fileidx = 0;
  }
  // try to load file
  if (fileidx < 0) fileidx = scridx;
  fname = zip_get_name(zz, fileidx, ZIP_FL_UNCHANGED);
  //fprintf(stderr, "%d: [%s]\n", (int)fileidx, fname);
  for (const char *t = fname; *t; ++t) if (t[0] == '/' || t[0] == '\\') fname = t+1;
  if (strlen(fname) > 1023) fname = fname+strlen(fname)-1020;
  strcpy(namebuf, fname);
  //fprintf(stderr, ":src=[%s] (%d)\n", namebuf, (int)fsize);
  if (newname) *newname = namebuf;
  struct zip_file *fl = zip_fopen_index(zz, fileidx, ZIP_FL_UNCHANGED);
  if (fl == NULL) {
    //fprintf(stderr, "ZIP: failed to open file!\n");
    zip_close(zz);
    return NULL;
  }
  buf = calloc(1, fsize+1);
  if (zip_fread(fl, buf, fsize) != fsize) { free(buf); buf = NULL; } else if (size) *size = fsize;
  zip_fclose(fl);
  zip_close(zz);
  //fprintf(stderr, "ZIP: file read.\n");
  return buf;
}


#else
static int isZIP (const char *name) {
  char *arcname;
  int res = 0;
  if (zipNameSplit(name, &arcname, NULL) < 0) return 0;
  res = isExtEqu(arcname, ".zip");
  free(arcname);
  return res;
}

/*
static char *loadZIPWholeFile (const char *fname, size_t *size, const char **newname) {
  if (size) *size = 0;
  return NULL;
}
*/
#endif
