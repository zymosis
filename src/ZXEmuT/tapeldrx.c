/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include "debugger.h"


static inline int checkByte (int addr, uint8_t v) {
  addr &= 0xffff;
  return (z80.mem_read(&z80, addr, ZYM_MEMIO_OTHER) == v);
}


static inline int checkWord (int addr, uint16_t v) {
  addr &= 0xffff;
  return
    (z80.mem_read(&z80, addr, ZYM_MEMIO_OTHER) == (v&0xff) &&
     z80.mem_read(&z80, (addr+1)&0xffff, ZYM_MEMIO_OTHER) == ((v>>8)&0xff));
}


static inline uint16_t getWord (int addr) {
  addr &= 0xffff;
  return
    (z80.mem_read(&z80, addr, ZYM_MEMIO_OTHER)|(z80.mem_read(&z80, (addr+1)&0xffff, ZYM_MEMIO_OTHER)<<8));
}


////////////////////////////////////////////////////////////////////////////////
/* legend:
 *   <0: new negative address offset
 *   0: next is 0 too? exit, else new positive address offset
 *   >0xff && <0x8000: check swapped word
 *   -32767 -- NOP
 *   -32666 -- skip this byte
 */
static int is_pattern_ok (int addr, const int16_t *pattern) {
  int a = addr;
  for (;;) {
    int16_t p = *pattern++;
    if (p <= 0 && p > -32000) {
      // new offset or end
      if (p == 0) {
        // positive offset or end
        p = *pattern++;
        if (p == 0) return 1; // end of data, ok
      }
      a = (addr+p)&0xffff;
      if (optDebugFlashLoad) fprintf(stderr, " pattern #%04X: #%04X (%d)\n", addr, a, p);
    } else if (p != -32666) {
      if (p == -32767) p = 0;
      if (p > 0xff) {
        if (z80.mem_read(&z80, a, ZYM_MEMIO_OTHER) != ((p>>8)&0xff)) return 0;
        a = (a+1)&0xffff;
        if (z80.mem_read(&z80, a, ZYM_MEMIO_OTHER) != (p&0xff)) return 0;
      } else {
        if (z80.mem_read(&z80, a, ZYM_MEMIO_OTHER) != p) return 0;
      }
      a = (a+1)&0xffff;
    } else {
      a = (a+1)&0xffff;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  const char *name;
  const int16_t *pattern;
  int (*detectFn) (void);
  int (*accelFn) (void);
  int exitOfs; // 0: detect
} fl_loader_info_t;


////////////////////////////////////////////////////////////////////////////////
#define MAX_DETECTORS  (128)
static int detector_count = 0;
static const fl_loader_info_t *detectors[MAX_DETECTORS];


static void fl_register_loader (const fl_loader_info_t *nfo) {
  if (detector_count >= MAX_DETECTORS) {
    fprintf(stderr, "FATAL: too many FlashLoad(utm) loaders!\n");
    abort();
  }
  detectors[detector_count++] = nfo;
};


#include "tapeldrx_ftl.c"
#include "tapeldrx_flashload.c"
#include "tapeldrx_uniloader.c"
#include "tapeldrx_hewson.c"
#include "tapeldrx_pl2.c"
#include "tapeldrx_edge.c"


////////////////////////////////////////////////////////////////////////////////
// 0x564
static const fl_loader_info_t rom_loader_info = {
  .name="ROM Loader",
  .pattern=NULL,
  .detectFn=NULL,
  .accelFn=emuTapeDoROMLoad,
  .exitOfs=123,
};


////////////////////////////////////////////////////////////////////////////////
static int executeLoader (uint16_t addr, const fl_loader_info_t *li) {
  if (optDebugFlashLoad) fprintf(stderr, "+++ LOADER: %s\n", li->name);
  cprintf("detected loader: %s\n", li->name);
  if (!optTapePlaying) emuStartTapeAuto();
  if (li->accelFn() == 0) {
    if (z80.hl.h) cprintf("\4tape loading error\n");
    z80.pc = addr+li->exitOfs;
    emuTapeAutoStop();
    if (optDebugFlashLoad) dbgSetActive(1);
    return 0;
  }
  cprintf("block not found!\n");
  emuTapeAutoStop();
  if (optDebugFlashLoad) dbgSetActive(1);
  return -1;
}


int emuTapeFlashLoad (void) {
  int addr = z80.pc;
  if (addr == 0x0564) {
    if (zxModel == ZX_MACHINE_PLUS2A || zxModel == ZX_MACHINE_PLUS3) {
      if (zxLastPagedROM == 3) return executeLoader(addr, &rom_loader_info);
    } else {
      if (!zxTRDOSPagedIn && (zxModel == ZX_MACHINE_48K || zxLastPagedROM == 1)) return executeLoader(addr, &rom_loader_info);
    }
  }
  if (addr < 0x4000) return -1; // ROM
  //if ((z80.afx.f&ZYM_FLAG_C) == 0) return -1; // don't accelerate VERIFY mode
  if (!checkByte(addr, 0x1F)) return -1; //RRA?
  if (!checkWord(addr-4, 0xFED3) && // OUT (#FE),A?
      !checkByte(addr-3, 0xE5)) return -1; // PUSH HL (elite loader)
  //if (optDebugFlashLoad) { dbgSetActive(1); return -1; }
  //fprintf(stderr, "trying to detect known loader at 0x%04x\n", addr);
  for (int f = 0; f < detector_count; ++f) {
    const fl_loader_info_t *li = detectors[f];
    if (optDebugFlashLoad) fprintf(stderr, "*** trying loader #%u: %s (#%04X)\n", f, li->name, addr);
    if (li->pattern == NULL && li->detectFn == NULL) continue;
    if (li->pattern != NULL && !is_pattern_ok(addr, li->pattern)) continue;
    if (optDebugFlashLoad) fprintf(stderr, " pattern ok\n");
    if (li->detectFn != NULL && !li->detectFn()) continue;
    return executeLoader(addr, li);
  }
  return -1;
}
