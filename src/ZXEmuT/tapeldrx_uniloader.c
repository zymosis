/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
static const int16_t pattern_uniloader[] = {
  -4, 0xD3, 0xFE, // OUT (#FE),A
  -10,
  0x14, // INC D
  0x08, // EX AF,AF'
  0x15, // DEC D
  0xF3, // DI
  0, 6, 0xCD, // CALL LD-EDGE2
  0, 21, 0xCD, // CALL LD-EDGE
  0, 116,
  0x7C, // LD A,H
  0xFE, 0x01, // CP #01
  0xD8, // RET C
  // check constants
  0, 26, 0x069C, // LD B,#9C
  0, 33, 0x3EC6, // LD A,#C6
  0, 41, 0x06C9, // LD B,#C9
  0, 49, 0xFE, 0xD4, // CP #D4
  0, 64, 0x06B0, // LD B,#B0
  0, 91, 0x06B2, // LD B,#B2
  0, 100, 0x3ECB, // LD A,#CB
  0, 105, 0x06B0, // LD B,#B0
  // end
  0, 0
};


// Elite UniLoader
static int isUniLoader (void) {
  uint16_t lde, lde2;
  int addr = z80.pc;
  //if (z80.ix.w <= addr && z80.ix.w+z80.de.w >= addr) return 0; // don't accelerate self-modifying loaders
  // address of LD-EDGE & LD-EDGE2
  lde2 = getWord(addr+7); // LD-EDGE2 address
  lde = getWord(addr+22); // LD-EDGE address
  if (lde2-lde != 4) return 0;
  // check LD-EDGE
  if (!checkByte(lde, 0xCD)) return 0; // CALL
  if (!checkWord(lde+1, lde2)) return 0;
  if (!checkWord(lde2, 0x163E)) return 0; // LD A,0x16
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
__attribute__((constructor)) static void fl_loader_ctor_uniloader (void) {
  const fl_loader_info_t nfo = {
    .name="Elite UniLoader",
    .pattern=pattern_uniloader,
    .detectFn=isUniLoader,
    .accelFn=emuTapeDoROMLoad,
    .exitOfs=116,
  };
  fl_register_loader(&nfo);
}
