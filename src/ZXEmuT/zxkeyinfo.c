/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include <stdlib.h>
#include <string.h>

#include "zxkeyinfo.h"


ZXKeyInfo zxKeyInfo[50] = {
  // keyboard
  {.name="cap", .port=0, .mask=0x01},
  {.name="cs", .port=0, .mask=0x01},
  {.name="z", .port=0, .mask=0x02},
  {.name="x", .port=0, .mask=0x04},
  {.name="c", .port=0, .mask=0x08},
  {.name="v", .port=0, .mask=0x10},
  {.name="a", .port=1, .mask=0x01},
  {.name="s", .port=1, .mask=0x02},
  {.name="d", .port=1, .mask=0x04},
  {.name="f", .port=1, .mask=0x08},
  {.name="g", .port=1, .mask=0x10},
  {.name="q", .port=2, .mask=0x01},
  {.name="w", .port=2, .mask=0x02},
  {.name="e", .port=2, .mask=0x04},
  {.name="r", .port=2, .mask=0x08},
  {.name="t", .port=2, .mask=0x10},
  {.name="1", .port=3, .mask=0x01},
  {.name="2", .port=3, .mask=0x02},
  {.name="3", .port=3, .mask=0x04},
  {.name="4", .port=3, .mask=0x08},
  {.name="5", .port=3, .mask=0x10},
  {.name="0", .port=4, .mask=0x01},
  {.name="9", .port=4, .mask=0x02},
  {.name="8", .port=4, .mask=0x04},
  {.name="7", .port=4, .mask=0x08},
  {.name="6", .port=4, .mask=0x10},
  {.name="p", .port=5, .mask=0x01},
  {.name="o", .port=5, .mask=0x02},
  {.name="i", .port=5, .mask=0x04},
  {.name="u", .port=5, .mask=0x08},
  {.name="y", .port=5, .mask=0x10},
  {.name="ent", .port=6, .mask=0x01},
  {.name="enter", .port=6, .mask=0x01},
  {.name="l", .port=6, .mask=0x02},
  {.name="k", .port=6, .mask=0x04},
  {.name="j", .port=6, .mask=0x08},
  {.name="h", .port=6, .mask=0x10},
  {.name="spc", .port=7, .mask=0x01},
  {.name="space", .port=7, .mask=0x01},
  {.name="sym", .port=7, .mask=0x02},
  {.name="ss", .port=7, .mask=0x02},
  {.name="m", .port=7, .mask=0x04},
  {.name="n", .port=7, .mask=0x08},
  {.name="b", .port=7, .mask=0x10},
  // kempston
  {.name="kright", .port=8, .mask=0x01},
  {.name="kleft", .port=8, .mask=0x02},
  {.name="kdown", .port=8, .mask=0x04},
  {.name="kup", .port=8, .mask=0x08},
  {.name="kfire", .port=8, .mask=0x10},
  {0}
};


int zxFindKeyByName (const char *name) {
  if (name != NULL && name[0]) {
    for (int f = 0; zxKeyInfo[f].name != NULL; ++f) if (strcasecmp(zxKeyInfo[f].name, name) == 0) return f;
  }
  return -1;
}


int zxFindKeyByWord (const uint16_t w) {
  if (w != 0) {
    for (int f = 0; zxKeyInfo[f].name != NULL; ++f) if (w == ((zxKeyInfo[f].port<<8)|zxKeyInfo[f].mask)) return f;
  }
  return -1;
}
