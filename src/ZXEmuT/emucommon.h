/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_COMMON_H
#define ZXEMUT_COMMON_H

#include <stdlib.h>
#include <string.h>
#ifdef JIM_INTERNAL
# include "../libjim/jim.h"
#else
# include <jim.h>
#endif

enum {
  DIF_NONE,
  DIF_BDI,
  DIF_P3DOS,
};


#define EMU_PURE          __attribute__((pure))
#define EMU_CONST         __attribute__((const))
#define EMU_NORETURN      __attribute__((noreturn))
#define EMU_PRINTF(m,n)   __attribute__((format(printf,m,n)))
#define EMU_MAYBE_UNUSED  __attribute__((unused))

#ifndef ZEMUT_PACKED
# define ZEMUT_PACKED  __attribute__((packed)) __attribute__((gcc_struct))
#endif
#ifndef ZEMUT_INLINE
# define ZEMUT_INLINE  inline
#endif


#define K8ASSERTLF(cond,fl,line,func) do { \
  if (!(cond)) { \
    fprintf(stderr, "FATAL: assertion failed in file '%s', line %d, function '%s'!\n", fl, line, func); \
    abort(); \
  } \
} while (0)


#define K8ASSERT(cond) K8ASSERTLF(cond,__FILE__,__LINE__,__FUNCTION__)


#define lambda(return_type, body_and_args) ({ \
  return_type __fn__ body_and_args \
  __fn__; \
})


#include "../libzymosis/zymosis.h"


static EMU_MAYBE_UNUSED inline int isExtEqu (const char *fname, const char *ext) {
  return (fname != NULL && ext != NULL &&
    strlen(fname) > strlen(ext) && strcasecmp(fname+strlen(fname)-strlen(ext), ext) == 0);
}


extern const char *snapshotExtensions[];
extern char binMyDir[8192];


#endif
