/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "jimapi.h"
#include "libvideo/video.h"
#include "console.h"
#include "debugger.h"
#include "memview.h"
#include "sprview.h"
#include "emuutils.h"
#include "lssnap.h"
#include "tapes.h"
#include "snd_alsa.h"
#include "zxkeyinfo.h"
#include "emuexec.h"

#include "libboots/libboots.h"


////////////////////////////////////////////////////////////////////////////////
#define JIMAPI_FN(name)  static int jim_##name (Jim_Interp *interp, int argc, Jim_Obj *const *argv)
#define JIMAPI_REGISTER(name)  Jim_CreateCommand(jim, #name, jim_##name, NULL, NULL)


////////////////////////////////////////////////////////////////////////////////
#include "jimapi_utils.c"
#include "jimapi_sdlkeys.c"


int jim_verbose_loading = 0;


////////////////////////////////////////////////////////////////////////////////
// consetstr
JIMAPI_FN(consetstr) {
  if (argc != 2) {
    Jim_WrongNumArgs(interp, 1, argv, "string");
    return JIM_ERR;
  }
  conSetInputString(Jim_String(argv[1]));
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// cputs
JIMAPI_FN(cputs) {
  if (argc < 2) {
    Jim_WrongNumArgs(interp, 1, argv, "?-nonewline? string");
    return JIM_ERR;
  }
  int argp = 1;
  int donl = 1;
  if (Jim_CompareStringImmediate(interp, argv[1], "-nonewline")) { donl = 0; ++argp; }
  for (; argp < argc; ++argp) cprintf("%s", Jim_String(argv[argp]));
  if (donl) cprintf("\n");
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// errputs
JIMAPI_FN(errputs) {
  if (argc < 2) {
    Jim_WrongNumArgs(interp, 1, argv, "?-nonewline? string");
    return JIM_ERR;
  }
  int argp = 1;
  int donl = 1;
  if (Jim_CompareStringImmediate(interp, argv[1], "-nonewline")) { donl = 0; ++argp; }
  for (; argp < argc; ++argp) cprintf("%s", Jim_String(argv[argp]));
  if (donl) cprintf("\n");
  // and now to stderr
  for (; argp < argc; ++argp) fprintf(stderr, "%s", Jim_String(argv[argp]));
  if (donl) fprintf(stderr, "\n");
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// load name
JIMAPI_FN(load) {
  if (jimapiDisableCompletion(interp, argc, argv)) return JIM_OK;
  if (argc != 2) { Jim_WrongNumArgs(interp, 1, argv, "name"); return JIM_ERR; }
  jimEvalFile(Jim_String(argv[1]), 0);
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}

// softload name
// won't fail if the file is absent
JIMAPI_FN(softload) {
  if (jimapiDisableCompletion(interp, argc, argv)) return JIM_OK;
  if (argc != 2) { Jim_WrongNumArgs(interp, 1, argv, "name"); return JIM_ERR; }
  jimEvalFile(Jim_String(argv[1]), 1);
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// vid_scale num
JIMAPI_FN(vid_scale) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    //char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "keep", NULL);
    jimapiStrAppendAbbrev(interp, res, "");
    //free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc > 2) {
    jim_SetResStrf(interp, "%s: wut?!", Jim_String(argv[0]));
    return JIM_ERR;
  }

  // return old value
  Jim_SetResultInt(interp, vidScale);

  // set new value
  if (argc > 1) {
    int newval = jimGetIntVal(interp, argv[1]);
    if (newval == JIM_INVALID_INTVAL) {
      jim_SetResStrf(interp, "%s: invalid numeric value: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
      return JIM_ERR;
    }
    if (newval < 1 || newval > 4) {
      jim_SetResStrf(interp, "%s: scale value out of bounds: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
      return JIM_ERR;
    }
    vidScale = newval;
    updateScale();
  }

  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// vid_rescaler num
JIMAPI_FN(vid_rescaler) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    //char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "keep", NULL);
    jimapiStrAppendAbbrev(interp, res, "");
    //free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc > 2) {
    jim_SetResStrf(interp, "%s: wut?!", Jim_String(argv[0]));
    return JIM_ERR;
  }

  // return old value
  Jim_SetResultInt(interp, vid_scaler_type);

  // set new value
  if (argc > 1) {
    int newval = jimGetIntVal(interp, argv[1]);
    if (newval == JIM_INVALID_INTVAL) {
      jim_SetResStrf(interp, "%s: invalid numeric value: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
      return JIM_ERR;
    }

    if (newval < 0 || newval >= VID_SCALER_MAX) {
      jim_SetResStrf(interp, "%s: scale value out of bounds: '%s' (max is %d)",
                     Jim_String(argv[0]), Jim_String(argv[1]), VID_SCALER_MAX-1);
      return JIM_ERR;
    }
    vid_scaler_type = newval;
  }

  return JIM_OK;
}


// quit
JIMAPI_FN(quit) {
  if (jimapiDisableCompletion(interp, argc, argv)) return JIM_OK;
  //if (argc != 1) { Jim_WrongNumArgs(interp, 1, argv, "no args expected"); return JIM_ERR; }
  postQuitMessage();
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// reset [forced] [model] [issue] [memory] [trdos]
// plus2
// 48k: [issue2|issue3|2|3]
// pentagon: [128|512|1024]
JIMAPI_FN(reset) {
  int was_penta = 0;

  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    if (argc == 1) Jim_AppendString(interp, res, " ", -1);
    for (int f = 1; f < argc; ++f) {
      char *a0;
      if (was_penta) {
        a0 = completeAbbrev(Jim_String(argv[f]), "48k", "pentagon", "scorpion", "trdos", "forced", NULL);
      } else {
        a0 = completeAbbrev(Jim_String(argv[f]), "48k", "128k", "plus2", "plus2a", "plus3", "pentagon", "scorpion", "issue2", "issue3", "trdos", "forced", NULL);
        if (strEqu(a0, "pentagon")) was_penta = 1;
      }
      jimapiStrAppendAbbrev(interp, res, a0);
      free(a0);
    }
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  int model = -1, forced = 0, issue = optZXIssue, pmem = -1/*128*//*zxPentagonMemory*/, trdos = 0;

  //if (argc < 1) { Jim_WrongNumArgs(interp, 1, argv, "string"); return JIM_ERR; }
  for (int f = 1; f < argc; ++f) {
    const char *a0;
    long val;
    if (was_penta) {
      a0 = parseAbbrev(Jim_String(argv[f]), "48k", "pentagon", "scorpion", NULL);
    } else {
      a0 = parseAbbrev(Jim_String(argv[f]), "48k", "128k", "plus2", "plus2a", "plus3", "pentagon", "scorpion", NULL);
    }
    if (a0 != NULL) {
      if (model < 0) {
             if (strEqu(a0, "48k")) model = ZX_MACHINE_48K;
        else if (strEqu(a0, "128k")) model = ZX_MACHINE_128K;
        else if (strEqu(a0, "plus2")) model = ZX_MACHINE_PLUS2;
        else if (strEqu(a0, "plus2a")) model = ZX_MACHINE_PLUS2A;
        else if (strEqu(a0, "plus3")) model = ZX_MACHINE_PLUS3;
        else if (strEqu(a0, "pentagon")) {
          model = ZX_MACHINE_PENTAGON;
          if (pmem < 0) pmem = 128;
          was_penta = 1;
        } else if (strEqu(a0, "scorpion")) model = ZX_MACHINE_SCORPION;
        continue;
      } else {
        jim_SetResStrf(interp, "%s: duplicate model name: '%s'", Jim_String(argv[0]), Jim_String(argv[f]));
        return JIM_ERR;
      }
    }

    if ((a0 = parseAbbrev(Jim_String(argv[f]), "forced", NULL)) != NULL) {
      forced = 1;
      continue;
    }

    if ((a0 = parseAbbrev(Jim_String(argv[f]), "trdos", NULL)) != NULL) {
      trdos = 1;
      continue;
    }

    if ((a0 = parseAbbrev(Jim_String(argv[f]), "issue2", "issue3", NULL)) != NULL) {
      issue = a0[5]-'0'-2;
      continue;
    }

    if (Jim_GetLong(interp, argv[f], &val) == JIM_OK) {
      if (val == 2 || val == 3) { issue = val-2; continue; }
      if (val == 128 || val == 512 || val == 1024) { pmem = val; continue; }
    }

    jim_SetResStrf(interp, "%s: invalid option: '%s'", Jim_String(argv[0]), Jim_String(argv[f]));
    return JIM_ERR;
  }

  if (model < 0) model = zxModel;
  if (pmem < 0) pmem = zxPentagonMemory;
  if (model == ZX_MACHINE_PENTAGON && pmem != zxPentagonMemory) forced = 1;

  optZXIssue = issue;
  zxPentagonMemory = pmem;
  emuSetModel(model, forced);
  emuReset(trdos);

  switch (zxModel) {
    case ZX_MACHINE_48K: jim_SetResStrf(interp, "48k (issue %d)", optZXIssue+2); break;
    case ZX_MACHINE_128K: Jim_SetResultString(interp, "128k", -1); break;
    case ZX_MACHINE_PLUS2: Jim_SetResultString(interp, "128k+2", -1); break;
    case ZX_MACHINE_PLUS2A: Jim_SetResultString(interp, "128k+2a", -1); break;
    case ZX_MACHINE_PLUS3: Jim_SetResultString(interp, "128k+3", -1); break;
    case ZX_MACHINE_PENTAGON: jim_SetResStrf(interp, "pentagon%d", zxPentagonMemory); break;
    case ZX_MACHINE_SCORPION: Jim_SetResultString(interp, "scorpion", -1); break;
    default: Jim_SetResultBool(interp, 0); break;
  }
  return JIM_OK;
}


// model
JIMAPI_FN(model) {
  if (jimapiDisableCompletion(interp, argc, argv)) return JIM_OK;
  if (argc != 1) { Jim_WrongNumArgs(interp, 1, argv, "too many args"); return JIM_ERR; }
  switch (zxModel) {
    case ZX_MACHINE_48K: jim_SetResStrf(interp, "48k (issue %d), %d tstates per frame", optZXIssue+2, machineInfo.tsperframe); break;
    case ZX_MACHINE_128K: jim_SetResStrf(interp, "128k, %d tstates per frame", machineInfo.tsperframe); break;
    case ZX_MACHINE_PLUS2: jim_SetResStrf(interp, "128k+2, %d tstates per frame", machineInfo.tsperframe); break;
    case ZX_MACHINE_PLUS2A: jim_SetResStrf(interp, "128k+2a, %d tstates per frame", machineInfo.tsperframe); break;
    case ZX_MACHINE_PLUS3: jim_SetResStrf(interp, "128k+3, %d tstates per frame", machineInfo.tsperframe); break;
    case ZX_MACHINE_PENTAGON: jim_SetResStrf(interp, "pentagon%d, %d tstates per frame", zxPentagonMemory, machineInfo.tsperframe); break;
    case ZX_MACHINE_SCORPION: jim_SetResStrf(interp, "scorpion, %d tstates per frame", machineInfo.tsperframe); break;
    default: Jim_SetResultString(interp, "unknown", -1); break;
  }
  return JIM_OK;
}


// nmi
JIMAPI_FN(nmi) {
  if (jimapiDisableCompletion(interp, argc, argv)) return JIM_OK;
  if (argc != 1) { Jim_WrongNumArgs(interp, 1, argv, "too many args"); return JIM_ERR; }
  z80GenerateNMI = 1;
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// grab [set|reset|toggle]
JIMAPI_FN(grab) {
  int v = (SDL_WM_GrabInput(SDL_GRAB_QUERY) == SDL_GRAB_ON);
  int res = jim_onoff_option(&v, interp, argc, argv, "set", "reset", 1);
  if (res != JIM_ERR) {
    SDL_WM_GrabInput(v ? SDL_GRAB_ON : SDL_GRAB_OFF);
    emuRealizeRealMouseCursorState();
  }
  return res;
}


// maxspeedugly [on|off|toogle]
JIMAPI_FN(maxspeedugly) {
  int v = optMaxSpeedBadScreen;
  int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
  optMaxSpeedBadScreen = v;
  return res;
}


// fps [on|off|toogle]
JIMAPI_FN(fps) {
  int v = optDrawFPS;
  int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
  emuSetDrawFPS(v);
  return res;
}


// trdos [on|off|toggle]
JIMAPI_FN(trdos) {
  int da = !!optTRDOSenabled;
  int res = jim_onoff_option(&da, interp, argc, argv, "on", "off", 1);
  if (!da) zxTRDOSpageout();
  optTRDOSenabled = da;
  return res;
}


// swapbuttons [on|off|toggle]
JIMAPI_FN(swapbuttons) {
  int da = optKMouseSwapButtons;
  int res = jim_onoff_option(&da, interp, argc, argv, "on", "off", 1);
  optKMouseSwapButtons = da;
  return res;
}


//FIXME: separate to two commands
// kmouse [on|off|toggle|sens]
JIMAPI_FN(kmouse) {
  /*
  int da = optKMouse;
  int res = jim_onoff_option(&da, interp, argc, argv, "on", "off", 1);
  optKMouse = da;
  return res;
  */
  /*
  int res = jim_AlphaArg(&optKMouse, &zxKMouseSpeed, interp, argc, argv);
  if (zxKMouseSpeed < 1) zxKMouseSpeed = 1; else if (zxKMouseSpeed > 255) zxKMouseSpeed = 255;
  return res;
  */

  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""),
                              "on", "off", "toggle", "normal", "absolute",
                              NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  long val = zxKMouseSpeed;
  if (argc < 1 || argc > 2) { Jim_WrongNumArgs(interp, 1, argv, "alpha"); return JIM_ERR; }
  if (argc > 1) {
    val = jimGetIntLitVal(interp, argv[1]);
    if (val == JIM_INVALID_INTVAL) {
      const char *a0 = parseAbbrev(Jim_String(argv[1]), "toggle", "on", "off",
                                   "normal", "absolute",
                                   NULL);
      if (a0 != NULL) {
             if (strEqu(a0, "on") || strEqu(a0, "tan")) optKMouse = 1;
        else if (strEqu(a0, "off") || strEqu(a0, "ona")) optKMouse = 0;
        else if (strEqu(a0, "toggle")) optKMouse = !optKMouse;
        else if (strEqu(a0, "normal")) optKMouseAbsolute = 0;
        else if (strEqu(a0, "absolute")) { optKMouseAbsolute = 1; emuSetKMouseAbsCoords(); }
        else {
          jim_SetResStrf(interp, "%s: invalid mode: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
          return JIM_ERR;
        }
      } else {
        if (jimapiFromConsole(interp)) cprintf("%s: %s\n", Jim_String(argv[0]), (optKMouse ? "on" : "off"));
      }
      Jim_SetResultString(interp, (optKMouse ? "on" : "off"), -1);
      return JIM_OK;
    }

    if (val < 0) val = 0; else if (val > 255) val = 255;
    zxKMouseSpeed = val;
    if (zxKMouseSpeed < 1) zxKMouseSpeed = 1; else if (zxKMouseSpeed > 255) zxKMouseSpeed = 255;
    Jim_SetResultInt(interp, val);
    return JIM_OK;
  }

  Jim_SetResultString(interp, (optKMouse ? "on" : "off"), -1);
  return JIM_OK;
}


// kspanish [on|off|toggle]
// toggles kempston mouse between strict (on) or normal (off)
JIMAPI_FN(kspanish) {
  int da = optKMouseStrict;
  int res = jim_onoff_option(&da, interp, argc, argv, "on", "off", 1);
  optKMouseStrict = da;
  return res;
}


// kjoystick [on|off|toggle]
JIMAPI_FN(kjoystick) {
  int da = optKJoystick;
  int res = jim_onoff_option(&da, interp, argc, argv, "on", "off", 1);
  optKJoystick = da;
  return res;
}


// speed [max|normal|toggle|percents|shade|noshade]
JIMAPI_FN(speed) {
  if (!jimapiWantsCompletion(interp, argc, argv)) {
    if (argc == 2 && strEquCI(Jim_String(argv[1]), "shade")) {
      optSlowShade = 1;
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
    if (argc == 2 && strEquCI(Jim_String(argv[1]), "noshade")) {
      optSlowShade = 0;
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
    // try number
    if (argc == 2) {
      int val = jimGetIntLitVal(interp, argv[1]);
      if (val != JIM_INVALID_INTVAL) {
        emuSetMaxSpeed(0);
        if (val < 1) val = 1; else if (val > 1000) val = 1000;
        emuSetSpeed(val);
        if (val > 0) {
          if (jimapiFromConsole(interp)) cprintf("speed: %d%%\n", optSpeed);
          jim_SetResStrf(interp, "%d", optSpeed);
          return JIM_OK;
        }
      }
    }
  }

  int v = optMaxSpeed;
  int res = jim_onoff_option(&v, interp, argc, argv, "max", "normal", 0);
  emuSetMaxSpeed(v);
  return res;
}


// pause [set|reset|toggle]
JIMAPI_FN(pause) {
  int v = ((optPaused&PAUSE_PERMANENT_MASK) != 0);
  int res = jim_onoff_option(&v, interp, argc, argv, "set", "reset", 1);
  emuSetPaused(v ? PAUSE_PERMANENT_SET : PAUSE_PERMANENT_RESET);
  return res;
}


// temppause [set|reset|toggle]
JIMAPI_FN(temppause) {
  int v = ((optPaused&PAUSE_TEMPORARY_MASK) != 0);
  int res = jim_onoff_option(&v, interp, argc, argv, "set", "reset", 1);
  emuSetPaused(v ? PAUSE_TEMPORARY_SET : PAUSE_TEMPORARY_RESET);
  return res;
}


// timings [early|late|toggle]
JIMAPI_FN(timings) {
  int v = zxLateTimings;
  int res = jim_onoff_option(&v, interp, argc, argv, "late", "early", 0);
  emuSetLateTimings(v);
  return res;
}


// console [show|hide|toggle|<alpha>]
JIMAPI_FN(console) {
  return jim_AlphaArg(&conVisible, &conAlpha, interp, argc, argv);
}


// kbleds [on|off|toggle|<alpha>]
JIMAPI_FN(kbleds) {
  return jim_AlphaArg(&optDrawKeyLeds, &klAlpha, interp, argc, argv);
}


// debugger [on|off|toggle|<alpha>]
//TODO: add 'debugger traps on|off' -- for optAllowZXEmuTraps
JIMAPI_FN(debugger) {
  int da = debuggerActive;
  int res = jim_AlphaArg(&da, &dbgAlpha, interp, argc, argv);
  if (da) { memviewSetActive(0); sprviewSetActive(0); }
  dbgSetActive(da);
  return res;
}


// memview [on|off|toggle|hex|text|32|42|51|64|max|7bit|cp-866|cp-1251]
JIMAPI_FN(memview) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""),
                              "on", "off", "toggle",
                              "hex", "text",
                              "32", "42", "51", "64", "max",
                              "7bit", "cp866", "cp1251",
                              "blue", "red", "magenta", "green", "cyan",
                              "yellow", "white", "gray", "bright",
                              "nocolor", "color10", "color1b",
                              "ignorecolor", "allowcolor",
                              NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc < 1) { Jim_WrongNumArgs(interp, 1, argv, "option"); return JIM_ERR; }

  int onoff = -1;
  int bright = -1;

  for (int f = 1; f < argc; ++f) {
    const char *a0 = parseAbbrev(Jim_String(argv[f]),
                                 "on", "off", "tan", "ona", "toggle",
                                 "hex", "text",
                                 "32", "42", "51", "64", "max",
                                 "7bit", "cp866", "cp1251",
                                 "blue", "red", "magenta", "green", "cyan",
                                 "yellow", "white", "gray", "grey", "bright",
                                 "nocolor", "color10", "color1b",
                                 "ignorecolor", "allowcolor",
                                 NULL);
    if (a0 != NULL) {
           if (strEquCI(a0, "on") || strEquCI(a0, "tan")) onoff = 1;
      else if (strEquCI(a0, "off") || strEquCI(a0, "ona")) onoff = 0;
      else if (strEquCI(a0, "toggle")) onoff = !memviewActive;
      else if (strEquCI(a0, "hex")) mv_viewmode = MV_MODE_HEX;
      else if (strEquCI(a0, "text")) mv_viewmode = MV_MODE_TEXT;
      else if (strEquCI(a0, "32")) mv_textwidth = 32;
      else if (strEquCI(a0, "42")) mv_textwidth = 42;
      else if (strEquCI(a0, "51")) mv_textwidth = 51;
      else if (strEquCI(a0, "64")) mv_textwidth = 64;
      else if (strEquCI(a0, "max")) mv_textwidth = MV_MAX_TEXT_WIDTH;
      else if (strEquCI(a0, "7bit")) mv_encoding = MV_ENC_7BIT;
      else if (strEquCI(a0, "cp866")) mv_encoding = MV_ENC_866;
      else if (strEquCI(a0, "cp1251")) mv_encoding = MV_ENC_1251;
      else if (strEquCI(a0, "blue")) { if (bright < 0) bright = 0; mv_textcolor = 1; }
      else if (strEquCI(a0, "red")) { if (bright < 0) bright = 0; mv_textcolor = 2; }
      else if (strEquCI(a0, "magenta")) { if (bright < 0) bright = 0; mv_textcolor = 3; }
      else if (strEquCI(a0, "green")) { if (bright < 0) bright = 0; mv_textcolor = 4; }
      else if (strEquCI(a0, "cyan")) { if (bright < 0) bright = 0; mv_textcolor = 5; }
      else if (strEquCI(a0, "yellow")) { if (bright < 0) bright = 0; mv_textcolor = 6; }
      else if (strEquCI(a0, "white")) { if (bright < 0) bright = 0; mv_textcolor = 7; }
      else if (strEquCI(a0, "gray")) { if (bright < 0) bright = 0; mv_textcolor = 7; }
      else if (strEquCI(a0, "grey")) { if (bright < 0) bright = 0; mv_textcolor = 7; }
      else if (strEquCI(a0, "bright")) bright = 1;
      else if (strEquCI(a0, "nocolor")) mv_colortext_mode = MV_CLRTEXT_NONE;
      else if (strEquCI(a0, "color10")) mv_colortext_mode = MV_CLRTEXT_CODE10;
      else if (strEquCI(a0, "color1b")) mv_colortext_mode = MV_CLRTEXT_CODE1B;
      else if (strEquCI(a0, "ignorecolor")) mv_colortext_ignore = 1;
      else if (strEquCI(a0, "allowcolor")) mv_colortext_ignore = 0;
      else {
        jim_SetResStrf(interp, "%s: invalid mode: '%s'", Jim_String(argv[0]), Jim_String(argv[f]));
        return JIM_ERR;
      }
    } else {
      int addr = jimGetIntValEx(interp, argv[f], 1/*allowpgaddr*/);
      //FIXME: this check is wrong
      //FIXME: write correct page parsing
      if (addr >= 0 && addr < (emuGetRAMPages()+4)<<14) {
        mv_staddr = (uint32_t)addr;
        continue;
      }
      jim_SetResStrf(interp, "%s: wtf is '%s'?", Jim_String(argv[0]), Jim_String(argv[f]));
      return JIM_ERR;
    }
  }

  if (bright >= 0) {
    if (bright) mv_textcolor |= 8; else mv_textcolor &= 7;
  }

  if (onoff >= 0) {
    if (onoff > 0) { dbgSetActive(0); sprviewSetActive(0); }
    memviewSetActive(onoff);
  }

  Jim_SetResultInt(interp, memviewActive);
  return JIM_OK;
}


// sprview [on|off|toggle]
JIMAPI_FN(sprview) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""),
                              "on", "off", "toggle", "addr", "width",
                              "height",
                              NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc < 1) { Jim_WrongNumArgs(interp, 1, argv, "option"); return JIM_ERR; }

  int onoff = -1;

  for (int f = 1; f < argc; ++f) {
    const char *a0 = parseAbbrev(Jim_String(argv[f]),
                                 "on", "off", "tan", "ona", "toggle",
                                 "addr", "width", "height",
                                 NULL);
    if (a0 != NULL) {
           if (strEquCI(a0, "on") || strEquCI(a0, "tan")) onoff = 1;
      else if (strEquCI(a0, "off") || strEquCI(a0, "ona")) onoff = 0;
      else if (strEquCI(a0, "toggle")) onoff = !sprviewActive;
      else if (strEquCI(a0, "addr")) {
        f += 1;
        if (f >= argc) {
          jim_SetResStrf(interp, "%s: missing argument for '%s'!", Jim_String(argv[0]), Jim_String(argv[f - 1]));
          return JIM_ERR;
        }
        int addr = jimGetIntValEx(interp, argv[f], 1/*allowpgaddr*/);
        //FIXME: this check is wrong
        //FIXME: write correct page parsing
        if (addr >= 0 && addr < (emuGetRAMPages()+4)<<14) {
          spr_staddr = (uint32_t)addr;
        } else {
          jim_SetResStrf(interp, "%s: wtf is '%s'?", Jim_String(argv[0]), Jim_String(argv[f]));
          return JIM_ERR;
        }
      } else if (strEquCI(a0, "width")) {
        f += 1;
        if (f >= argc) {
          jim_SetResStrf(interp, "%s: missing argument for '%s'!", Jim_String(argv[0]), Jim_String(argv[f - 1]));
          return JIM_ERR;
        }
        int val = jimGetIntValEx(interp, argv[f], 0/*allowpgaddr*/);
        if (val < 1 || val > 32) {
          jim_SetResStrf(interp, "%s: wtf is '%s'?", Jim_String(argv[0]), Jim_String(argv[f]));
          return JIM_ERR;
        }
        spr_wdt = val;
      } else if (strEquCI(a0, "height")) {
        f += 1;
        if (f >= argc) {
          jim_SetResStrf(interp, "%s: missing argument for '%s'!", Jim_String(argv[0]), Jim_String(argv[f - 1]));
          return JIM_ERR;
        }
        int val = jimGetIntValEx(interp, argv[f], 0/*allowpgaddr*/);
        if (val < 1 || val > 192) {
          jim_SetResStrf(interp, "%s: wtf is '%s'?", Jim_String(argv[0]), Jim_String(argv[f]));
          return JIM_ERR;
        }
        spr_hgt = val;
      } else {
        jim_SetResStrf(interp, "%s: invalid mode: '%s'", Jim_String(argv[0]), Jim_String(argv[f]));
        return JIM_ERR;
      }
    } else {
      int addr = jimGetIntValEx(interp, argv[f], 1/*allowpgaddr*/);
      //FIXME: this check is wrong
      //FIXME: write correct page parsing
      if (addr >= 0 && addr < (emuGetRAMPages()+4)<<14) {
        spr_staddr = (uint32_t)addr;
        continue;
      }
      jim_SetResStrf(interp, "%s: wtf is '%s'?", Jim_String(argv[0]), Jim_String(argv[f]));
      return JIM_ERR;
    }
  }

  if (onoff >= 0) {
    if (onoff > 0) { dbgSetActive(0); memviewSetActive(0); }
    sprviewSetActive(onoff);
  }

  Jim_SetResultInt(interp, sprviewActive);
  return JIM_OK;
}


// useplus3 [on|off|toggle]
JIMAPI_FN(useplus3) {
  if (machineInfo.origContention) {
    int v = machineInfo.usePlus3Contention;
    int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
    if (machineInfo.usePlus3Contention != v) {
      machineInfo.usePlus3Contention = v;
      emuBuildContentionTable();
    }
    return res;
  }
  Jim_SetResultBool(interp, machineInfo.usePlus3Contention);
  return JIM_OK;
}


// genuine [on|off|toggle]
JIMAPI_FN(genuine) {
  int v = machineInfo.genuine;
  int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
  if (machineInfo.genuine != v) {
    machineInfo.genuine = v;
    emuReset(0);
  }
  Jim_SetResultBool(interp, machineInfo.genuine);
  return res;
}


// contention [on|off|toggle]
JIMAPI_FN(contention) {
  if (machineInfo.origContention) {
    int v = machineInfo.contention;
    int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
    machineInfo.contention = v;
    if (machineInfo.origIOContention) machineInfo.iocontention = v;
    return res;
  }
  Jim_SetResultBool(interp, machineInfo.contention);
  return JIM_OK;
}


// memcontention [on|off|toggle]
JIMAPI_FN(memcontention) {
  if (machineInfo.origContention) {
    int v = machineInfo.contention;
    int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
    machineInfo.contention = v;
    return res;
  }
  Jim_SetResultBool(interp, machineInfo.contention);
  return JIM_OK;
}


// iocontention [on|off|toggle]
JIMAPI_FN(iocontention) {
  if (machineInfo.origIOContention) {
    int v = machineInfo.iocontention;
    int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
    machineInfo.iocontention = v;
    return res;
  }
  Jim_SetResultBool(interp, machineInfo.iocontention);
  return JIM_OK;
}


// snow [on|off|toggle]
JIMAPI_FN(snow) {
  int v = optEmulateSnow;
  int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
  if (v != optEmulateSnow) {
    optEmulateSnow = v;
    memset(zxUlaSnow, 0, sizeof(zxUlaSnow));
    zxWasUlaSnow = 0;
  }
  return res;
}


// noflic [on|off|toggle]
JIMAPI_FN(noflic) {
  //int da = optNoFlic;
  //int res = jim_onoff_option(&da, interp, argc, argv, "on", "off", "detect", 1);

  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "on", "off", "detect", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc < 1 || argc > 2) { Jim_WrongNumArgs(interp, 1, argv, "string"); return JIM_ERR; }
  if (argc > 1) {
    const char *a0 = parseAbbrev(Jim_String(argv[1]), "on", "off", "detect", "tan", "ona", NULL);
    if (a0 != NULL) {
           if (strEqu(a0, "on") || strEqu(a0, "yes") || strEqu(a0, "tan")) optNoFlic = 1;
      else if (strEqu(a0, "off") || strEqu(a0, "no") || strEqu(a0, "ona")) optNoFlic = 0;
      else if (strEqu(a0, "detect")) optNoFlic = -1;
    } else {
      jim_SetResStrf(interp, "%s: invalid mode: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
      return JIM_ERR;
    }
  } else {
    if (jimapiFromConsole(interp)) cprintf("%s: %d\n", Jim_String(argv[0]), optNoFlic);
  }
  Jim_SetResultInt(interp, optNoFlic);
  return JIM_OK;
}


// brightblack [on|off|toggle|value]
JIMAPI_FN(brightblack) {
  const int da = optBrightBlack;
  const int ovv = optBrightBlackValue;
  int res = jim_AlphaArg(&optBrightBlack, &optBrightBlackValue, interp, argc, argv);
  if (optBrightBlackValue < 0) optBrightBlackValue = 0; else if (optBrightBlackValue > 255) optBrightBlackValue = 255;
  if (res == JIM_OK) {
    if (optBrightBlack != da || optBrightBlackValue != ovv) {
      if (optBrightBlack) {
        zxPalette[8*3+0] = zxPalette[8*3+1] = zxPalette[8*3+2] = optBrightBlackValue;
      } else {
        zxPalette[8*3+0] = zxPalette[8*3+1] = zxPalette[8*3+2] = 0;
      }
      rebuildPalette();
    }
  }
  return res;
}


// zxpalcolor index [#rrggbb]
JIMAPI_FN(zxpalcolor) {
  if (jimapiDisableCompletion(interp, argc, argv)) return JIM_OK;
  if (argc < 2 || argc > 3) {
    jim_SetResStrf(interp, "%s: expected index and optional #rrggbb color", Jim_String(argv[0]));
    return JIM_ERR;
  }

  int idx = jimGetIntLitVal(interp, argv[1]);
  if (idx < 0 || idx > 15) {
    jim_SetResStrf(interp, "%s: invalid color index %s", Jim_String(argv[0]), Jim_String(argv[1]));
    return JIM_ERR;
  }

  // return old color
  char stbuf[64];
  snprintf(stbuf, sizeof(stbuf), "#%02x%02x%02x", zxPalette[idx*3+0], zxPalette[idx*3+1], zxPalette[idx*3+2]);
  Jim_SetResultString(interp, stbuf, -1);
  if (jimapiFromConsole(interp)) {
    cprintf("zx palette %d is %s\n", idx, stbuf);
  }

  // try to set new color
  if (argc > 2) {
    const char *cc = Jim_String(argv[2]);
    uint32_t clr = 0;
    if (cc[0] == '#') {
      size_t slen = strlen(cc);
      if (slen == 4) {
        for (unsigned f = 1; f < 4; ++f) {
          int dig = digitInBase(cc[f], 16);
          if (dig < 0) { clr = 0xffffffffU; break; }
          clr = clr*256u+((unsigned)dig)*16+(unsigned)dig;
        }
      } else if (slen == 7) {
        for (unsigned f = 1; f < 7; ++f) {
          int dig = digitInBase(cc[f], 16);
          if (dig < 0) { clr = 0xffffffffU; break; }
          clr = clr*16u+(unsigned)dig;
        }
      } else {
        clr = 0xffffffffU;
      }
    } else {
      clr = 0xffffffffU;
    }
    if (clr == 0xffffffffU) {
      jim_SetResStrf(interp, "%s: invalid color value %s", Jim_String(argv[0]), cc);
      return JIM_ERR;
    }
    zxPalette[idx*3+0] = (clr>>16)&0xffU;
    zxPalette[idx*3+1] = (clr>>8)&0xffU;
    zxPalette[idx*3+2] = clr&0xffU;
    rebuildPalette();
  }
  return JIM_OK;
}


// fullscreen [on|off|toggle]
JIMAPI_FN(fullscreen) {
  int v = optFullscreen;
  int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
  if (v != optFullscreen) {
    switchFullScreen();
    emuFullScreenChanged();
  }
  return res;
}


// keyhelp [on|off|toggle]
JIMAPI_FN(keyhelp) {
  return jim_onoff_option(&optKeyHelpVisible, interp, argc, argv, "on", "off", 1);
}


// allowother128 [on|off|toggle]
JIMAPI_FN(allowother128) {
  return jim_onoff_option(&optAllowOther128, interp, argc, argv, "on", "off", 1);
}


// keymatrix [on|off|toggle]
JIMAPI_FN(keymatrix) {
  return jim_onoff_option(&optEmulateMatrix, interp, argc, argv, "on", "off", 1);
}


// bad7ffd [on|off|toggle]
JIMAPI_FN(bad7ffd) {
  return jim_onoff_option(&optFDas7FFD, interp, argc, argv, "on", "off", 1);
}


// ay [on|off|toggle]
JIMAPI_FN(ay) {
  int prevAY = optAYEnabled;
  int res = jim_onoff_option(&optAYEnabled, interp, argc, argv, "on", "off", 1);
  if (!optAYEnabled && prevAY) emuResetAY();
  return res;
}


// autofire
//   [on|off|toggle]
//   [hold num]
//   [delay num]
//   [key zxkeyname]
JIMAPI_FN(autofire) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "hold", "delay", "on", "off", "toggle", "key", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  const char *a0 = parseAbbrev(Jim_String(argv[1]), "hold", "delay", "on", "off", "toggle", "key", "query", NULL);
  if (a0 == NULL) {
    jim_SetResStrf(interp, "%s: invalid autofire command: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
    Jim_SetResultBool(interp, 0);
    return JIM_OK;
  }
  if (strEqu(a0, "on")) {
    optAutofire = 1;
    afireDown = 0;
    afireLeft = 0;
  } else if (strEqu(a0, "off")) {
    optAutofire = 0;
  } else if (strEqu(a0, "toggle")) {
    optAutofire = !optAutofire;
    afireDown = 0;
    afireLeft = 0;
  } else if (strEqu(a0, "hold") || strEqu(a0, "delay")) {
    long val;
    if (argc < 3 || Jim_GetLong(interp, argv[2], &val) != JIM_OK) {
      cprintf("autofire: number expected for '%s'!", a0);
      Jim_SetResultBool(interp, 0);
      return JIM_OK;
    }
    if (a0[0] == 'h') {
      optAutofireHold = (val < 1 ? 1 : val);
    } else {
      optAutofireDelay = (val < 0 ? 0 : val);
    }
    afireDown = 0;
    afireLeft = 0;
  } else if (strEqu(a0, "key")) {
    int kn;
    if (argc != 3 || (kn = zxFindKeyByName(Jim_String(argv[2]))) < 0) {
      cprintf("autofire: zx key name expected for '%s'!", a0);
      Jim_SetResultBool(interp, 0);
      return JIM_OK;
    }
    afirePortBit = (zxKeyInfo[kn].port<<8)|zxKeyInfo[kn].mask;
  } else if (strEqu(a0, "query")) {
    int kn = zxFindKeyByWord(afirePortBit);
    cprintf("autofire is %s, hold: %d, delay: %d, key: %s", (optAutofire ? "on" : "off"), optAutofireHold, optAutofireDelay, (kn >= 0 ? zxKeyInfo[kn].name : "unknown"));
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// usesound [on|off|toggle]
JIMAPI_FN(usesound) {
  if (sndAllowUseToggle) {
    int v = (sndSampleRate != 0);
    int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
    if (v != (sndSampleRate != 0)) {
      if (v) {
        sndSampleRate = -1;
        if (initSound() == 0) {
          cprintf("ALSA: %dHz, %d channels, %d bps\n", sndSampleRate, 2, 2);
        } else {
          cprintf("WARNING: can't initialize sound!\n");
        }
      } else {
        deinitSound();
        sndSampleRate = 0;
      }
    }
    return res;
  }
  jim_SetResStrf(interp, "%s: disabled", Jim_String(argv[0]));
  return JIM_ERR;
}


// filter [off|tv2x]
JIMAPI_FN(filter) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "off", "none", "tv", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc > 1) {
    const char *a0 = parseAbbrev(Jim_String(argv[1]), "off", "none", "tv", "forcetv", NULL);
    if (a0 != NULL) {
           if (strEqu(a0, "tv")) optTVScaler = 1;
      else if (strEqu(a0, "forcetv")) optTVScaler = 2;
      else optTVScaler = 0;
    } else {
      jim_SetResStrf(interp, "%s: invalid filter: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
      return JIM_ERR;
    }
  }
  jim_SetResStrf(interp, "%s", (optTVScaler ? "tv" : "off"));
  return JIM_OK;
}


// screenofs [none|0..16|speccy]
JIMAPI_FN(screenofs) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "normal", "speccy", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc > 1) {
    const char *a0 = parseAbbrev(Jim_String(argv[1]), "none", "normal", "speccy", NULL);
    if (a0 != NULL) {
           if (strEqu(a0, "normal") || strEqu(a0, "none")) zxScreenOfs = 0;
      else if (strEqu(a0, "speccy")) zxScreenOfs = 4;
    } else {
      // try number
      long val;
      if (Jim_GetLong(interp, argv[1], &val) != JIM_OK) {
        jim_SetResStrf(interp, "%s: numeric value expected", Jim_String(argv[0]));
        return JIM_ERR;
      }
      if (val < 0 || val > 16) {
        jim_SetResStrf(interp, "%s: numeric value in range [0..16] expected", Jim_String(argv[0]));
        return JIM_ERR;
      }
      zxScreenOfs = val;
    }
  }

  switch (zxScreenOfs) {
    case 0: Jim_SetResultString(interp, "normal", -1); break;
    case 8: Jim_SetResultString(interp, "speccy", -1); break;
    default: Jim_SetResultInt(interp, zxScreenOfs); break;
  }
  return JIM_OK;
}


// colormode [0..4]|normal|monochrome|green
JIMAPI_FN(colormode) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""),
                              "normal", "monochrome", "green", "yellow",
                              "cyan", "magenta", "red",
                              NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc > 1) {
    const char *a0 = parseAbbrev(Jim_String(argv[1]),
                                 "normal", "monochrome", "green", "yellow",
                                 "cyan", "magenta", "red",
                                 NULL);
    if (a0 != NULL) {
           if (strEqu(a0, "normal")) vidColorMode = 0;
      else if (strEqu(a0, "monochrome")) vidColorMode = 1;
      else if (strEqu(a0, "green")) vidColorMode = 2;
      else if (strEqu(a0, "yellow")) vidColorMode = 3;
      else if (strEqu(a0, "cyan")) vidColorMode = 4;
      else if (strEqu(a0, "magenta")) vidColorMode = 5;
      else if (strEqu(a0, "red")) vidColorMode = 6;
    } else {
      // try number
      long val;
      if (Jim_GetLong(interp, argv[1], &val) != JIM_OK) {
        jim_SetResStrf(interp, "%s: numeric value expected", Jim_String(argv[0]));
        return JIM_ERR;
      }
      if (val < 0 || val > 6+8) {
        jim_SetResStrf(interp, "%s: numeric value in range [0..4] expected", Jim_String(argv[0]));
        return JIM_ERR;
      }
      vidColorMode = val;
    }
  }

  switch (vidColorMode) {
    case 0: Jim_SetResultString(interp, "normal", -1); break;
    case 1: Jim_SetResultString(interp, "monochrome", -1); break;
    case 2: Jim_SetResultString(interp, "green", -1); break;
    default: Jim_SetResultInt(interp, vidColorMode); break;
  }
  return JIM_OK;
}


// brightborder [0|5|6|7]
JIMAPI_FN(brightborder) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "off", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc > 1) {
    const char *a0 = parseAbbrev(Jim_String(argv[1]), "off", NULL);
    if (a0 != NULL) {
      if (strEqu(a0, "off")) optBrightBorder = 0;
    } else {
      // try number
      long val;
      if (Jim_GetLong(interp, argv[1], &val) != JIM_OK) {
        jim_SetResStrf(interp, "%s: numeric value expected", Jim_String(argv[0]));
        return JIM_ERR;
      }
      if (val < 5 || val > 7) {
        jim_SetResStrf(interp, "%s: numeric value in range [5..7] expected", Jim_String(argv[0]));
        return JIM_ERR;
      }
      optBrightBorder = val;
    }
  }

  switch (optBrightBorder) {
    case 0: Jim_SetResultString(interp, "off", -1); break;
    default: Jim_SetResultInt(interp, optBrightBorder); break;
  }
  return JIM_OK;
}


// sound [off|beeper|ay|on|volume]
// sound volume [ay|beeper] [percents]
// sound ay <mono|acb|abc|on|off|query>
// sound filter <tv|beeper|none>
// sound restart
JIMAPI_FN(sound) {
  int isvol = 0;

  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "on", "off", "ay", "beeper", "volume", "filter", "restart", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    const char *a5 = parseAbbrev(a0, "ay", "volume", "filter", NULL);
    if (a5 && strEqu(a5, "volume")) {
      if (argc > 2) {
        free(a0);
        a0 = completeAbbrev(Jim_String(argv[2]), "ay", "beeper", NULL);
        jimapiStrAppendAbbrev(interp, res, a0);
        jimapiAppendRestArgs(res, 3);
      } else {
        jimapiAppendRestArgs(res, 2);
      }
    } else if (a5 && strEqu(a5, "ay")) {
      free(a0);
      a0 = completeAbbrev((argc > 2 ? Jim_String(argv[2]) : ""), "on", "off", "mono", "acb", "abc", "melodik", "pentagon", "query", NULL);
      jimapiStrAppendAbbrev(interp, res, a0);
      jimapiAppendRestArgs(res, 3);
    } else if (a5 && strEqu(a5, "filter")) {
      free(a0);
      a0 = completeAbbrev((argc > 2 ? Jim_String(argv[2]) : ""), "tv", "beeper", "none", "query", NULL);
      jimapiStrAppendAbbrev(interp, res, a0);
      jimapiAppendRestArgs(res, 3);
    } else {
      jimapiAppendRestArgs(res, 2);
    }
    free(a0);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc > 1) {
    const char *a0 = parseAbbrev(Jim_String(argv[1]), "off", "no", "beeper", "ay", "on", "yes", "disabled", "enabled", "volume", "filter", "restart", NULL);
    if (a0 != NULL) {
      if (strEqu(a0, "off") || strEqu(a0, "ona") || strEqu(a0, "disabled")) {
        optSoundBeeper = 0;
        optSoundAY = 0;
      } else if (strEqu(a0, "filter")) {
        if (argc > 2) {
          const char *a3 = parseAbbrev(Jim_String(argv[2]), "tv", "beeper", "default", "flat", "crisp", "handheld", "query", NULL);
          if (a3) {
                 if (strEqu(a3, "tv")) soundSetFilter(AY_SPEAKER_TV);
            else if (strEqu(a3, "beeper")) soundSetFilter(AY_SPEAKER_BEEPER);
            else if (strEqu(a3, "none")) soundSetFilter(AY_SPEAKER_DEFAULT);
            else if (strEqu(a3, "default")) soundSetFilter(AY_SPEAKER_DEFAULT);
            else if (strEqu(a3, "flat")) soundSetFilter(AY_SPEAKER_FLAT);
            else if (strEqu(a3, "crisp")) soundSetFilter(AY_SPEAKER_CRISP);
            else if (strEqu(a3, "handheld")) soundSetFilter(AY_SPEAKER_HANDHELD);
          }
        }
        const char *fltname;
        switch (soundGetFilter()) {
          case AY_SPEAKER_TV: fltname = "tv"; break;
          case AY_SPEAKER_BEEPER: fltname = "beeper"; break;
          case AY_SPEAKER_DEFAULT: fltname = "default"; break;
          case AY_SPEAKER_FLAT: fltname = "flat"; break;
          case AY_SPEAKER_CRISP: fltname = "crisp"; break;
          case AY_SPEAKER_HANDHELD: fltname = "handheld"; break;
          default: fltname = "fucked"; break;
        }
        if (jimapiFromConsole(interp)) cprintf("sound filter: %s\n", fltname);
        jim_SetResStrf(interp, "%s", fltname);
        return JIM_OK;
      } else if (strEqu(a0, "on") || strEqu(a0, "tan") || strEqu(a0, "enabled")) {
        optSoundBeeper = 1;
        optSoundAY = 1;
      } else if (strEqu(a0, "beeper")) {
        if (argc > 2) {
          const char *a3 = parseAbbrev(Jim_String(argv[2]), "on", "off", "tan", "ona", NULL);
          if (a3) {
                 if (strEqu(a3, "on") || strEqu(a3, "tan")) optSoundBeeper = 1;
            else optSoundBeeper = 0;
          }
        }
        cprintf("Beeper is %s.\n", (optSoundBeeper ? "enabled" : "disabled"));
        Jim_SetResultBool(interp, optSoundBeeper);
        return JIM_OK;
      } else if (strEqu(a0, "ay")) {
        if (argc > 2) {
          const char *a3 = parseAbbrev(Jim_String(argv[2]),
                                       "mono", "acb", "abc", "melodik", "pentagon",
                                       "query",
                                       "on", "off", "tan", "ona",
                                       NULL);
          int ismode = 1;
          if (a3) {
                 if (strEqu(a3, "mono")) soundSetAYStereo(AY_STEREO_NONE);
            else if (strEqu(a3, "abc") || strEqu(a3, "melodik")) soundSetAYStereo(AY_STEREO_ABC);
            else if (strEqu(a3, "acb") || strEqu(a3, "pentagon")) soundSetAYStereo(AY_STEREO_ACB);
            else if (strEqu(a3, "on") || strEqu(a3, "tan")) { ismode = 0; optSoundAY = 1; }
            else if (strEqu(a3, "off") || strEqu(a3, "ona")) { ismode = 0; optSoundAY = 0; }
          }
          if (ismode) {
            const char *fltname;
            switch (soundGetAYStereo()) {
              case AY_STEREO_NONE: fltname = "mono"; break;
              case AY_STEREO_ABC: fltname = "acb"; break;
              case AY_STEREO_ACB: fltname = "abc"; break;
              default: fltname = "wutafuck"; break;
            }
            if (jimapiFromConsole(interp)) cprintf("AY mode: %s\n", fltname);
            jim_SetResStrf(interp, "%s", fltname);
          } else {
            cprintf("AY is %s.\n", (optSoundAY ? "enabled" : "disabled"));
            Jim_SetResultBool(interp, optSoundAY);
          }
          return JIM_OK;
        }
      } else if (strEqu(a0, "volume")) {
        isvol = 1;
        if (argc > 2) {
          int doRestart = 0;
          long val = 0;
          if ((a0 = parseAbbrev(Jim_String(argv[2]), "beeper", "ay", NULL)) != NULL) {
            // beeper or ay volume
            if (argc > 3) {
              if (Jim_GetLong(interp, argv[3], &val) != JIM_OK) {
                jim_SetResStrf(interp, "%s: invalid sound volume: '%s'", Jim_String(argv[0]), Jim_String(argv[3]));
                return JIM_ERR;
              }
              if (val < 0) val = 0; else if (val > 500) val = 500;
            }
            if (strEqu(a0, "beeper")) {
              if (argc > 3) {
                doRestart = (optSoundVolumeBeeper != val);
                optSoundVolumeBeeper = val;
              }
              jim_SetResStrf(interp, "%d", optSoundVolumeBeeper);
            } else if (strEqu(a0, "ay")) {
              if (argc > 3) {
                doRestart = (optSoundVolumeAY != val);
                optSoundVolumeAY = val;
              }
              jim_SetResStrf(interp, "%d", optSoundVolumeAY);
            }
          } else {
            // overal volume
            if (Jim_GetLong(interp, argv[2], &val) != JIM_OK) {
              jim_SetResStrf(interp, "%s: invalid sound volume: '%s'", Jim_String(argv[0]), Jim_String(argv[3]));
              return JIM_ERR;
            }
            if (val < 0) val = 0; else if (val > 500) val = 500;
            doRestart = (optSoundVolume != val);
            optSoundVolume = val;
            jim_SetResStrf(interp, "%d", optSoundVolume);
          }
          if (doRestart) soundApplyNewVolumes();
        } else {
          jim_SetResStrf(interp, "%d", optSoundVolume);
        }
        if (jimapiFromConsole(interp)) cprintf("sound volume: %d%% (beeper:%d%%; ay:%d%%)\n", optSoundVolume, optSoundVolumeBeeper, optSoundVolumeAY);
      } else if (strEqu(a0, "restart")) {
        if (jimapiFromConsole(interp)) cprintf("restarting sound system...\n");
        soundFullRestart();
        Jim_SetResultBool(interp, 1);
        return JIM_OK;
      }
    } else {
      jim_SetResStrf(interp, "%s: invalid sound mode: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
      return JIM_ERR;
    }
  }
  Jim_SetResultBool(interp, 1);

  if (!isvol) {
         if (optSoundBeeper && optSoundAY) Jim_SetResultString(interp, "on", -1);
    else if (optSoundBeeper) Jim_SetResultString(interp, "beeper", -1);
    else if (optSoundAY) Jim_SetResultString(interp, "ay", -1);
    else Jim_SetResultString(interp, "off", -1);
    //jim_SetResStrf(interp, "%s", (optSoundBeeper ? (optSoundAY ? "on" : "beeper") : (optSoundAY ? "ay" : "off")));
    if (argc > 1 && jimapiFromConsole(interp)) cprintf("sound: %s\n", Jim_String(Jim_GetResult(interp)));
  }
  return JIM_OK;
}


// issue [2|3]
JIMAPI_FN(issue) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "2", "3", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  long val = optZXIssue+2;
  if (argc < 1 || argc > 2) { Jim_WrongNumArgs(interp, 1, argv, "issue"); return JIM_ERR; }
  if (argc > 1) {
    if (Jim_GetLong(interp, argv[1], &val) != JIM_OK) {
      jim_SetResStrf(interp, "%s: issue number expected", Jim_String(argv[0]));
      return JIM_ERR;
    }
    if (val < 2 || val > 3) {
      jim_SetResStrf(interp, "%s: invalid issue number: %d", Jim_String(argv[0]), (int)val);
      return JIM_ERR;
    }
  }
  optZXIssue = val-2;
  Jim_SetResultInt(interp, val);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// snapshot <load|save|clear|reset> [name]
JIMAPI_FN(snapshot) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "load", "save", "clear", "setmodel", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    const char *a1 = parseAbbrev(a0, "load", "save", "setmodel", NULL);
    if (a1 != NULL) {
      if (strEqu(a1, "setmodel")) {
        free(a0);
        a0 = completeAbbrev((argc > 2 ? Jim_String(argv[2]) : ""), "yes", "no", "on", "off", "toggle", NULL);
        jimapiStrAppendAbbrev(interp, res, a0);
        jimapiAppendRestArgs(res, 3);
      } else {
        // load/save
        free(a0);
        a0 = completeFilesWithExts((argc > 2 ? Jim_String(argv[2]) : ""), snapshotExtensions);
        jimapiStrAppendAbbrev(interp, res, a0);
        jimapiAppendRestArgs(res, 3);
      }
    } else {
      jimapiAppendRestArgs(res, 2);
    }
    free(a0);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  const char *a0;
  int res = 0;

  if (argc < 2) { Jim_WrongNumArgs(interp, 1, argv, "action name"); return JIM_ERR; }
  if ((a0 = parseAbbrev(Jim_String(argv[1]), "load", "save", "clear", "reset", "setmodel", NULL)) == NULL) {
    jim_SetResStrf(interp, "%s: invalid action: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
    return JIM_ERR;
  }

  if (strEqu(a0, "load")) {
    if (argc < 3) { Jim_WrongNumArgs(interp, 1, argv, "action name"); return JIM_ERR; }
    res = (loadSnapshot(Jim_String(argv[2]), SNAPLOAD_ANY) == 0);
    if (res) cprintf("'%s' loaded\n", Jim_String(argv[2])); else cprintf("failed to load '%s'\n", Jim_String(argv[2]));
  } else if (strEqu(a0, "save")) {
    if (argc < 3) { Jim_WrongNumArgs(interp, 1, argv, "action name"); return JIM_ERR; }
    res = (saveSnapshot(Jim_String(argv[2])) == 0);
    if (res) cprintf("'%s' saved\n", Jim_String(argv[2])); else cprintf("failed to save '%s'\n", Jim_String(argv[2]));
  } else if (strEqu(a0, "setmodel")) {
    //int v = optSnapSetModel, res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
    if (argc > 2) {
      if ((a0 = parseAbbrev(Jim_String(argv[2]), "on", "off", "yes", "no", "toggle", NULL)) != NULL) {
        if (strEqu(a0, "on") || strEqu(a0, "yes")) {
          optSnapSetModel = 1;
        } else if (strEqu(a0, "toggle")) {
          optSnapSetModel = !optSnapSetModel;
        } else {
          optSnapSetModel = 0;
        }
      }
    }
    //optSnapSetModel = v;
    if (jimapiFromConsole(interp)) cprintf("optSnapSetModel: %d\n", optSnapSetModel);
    return JIM_OK;
  } else {
    if (zxSLT != NULL) { if (jimapiFromConsole(interp)) cprintf("snapshot cleared\n"); }
    lssnap_rzx_stop();
    lssnap_slt_clear();
    res = 1;
  }
  Jim_SetResultBool(interp, res);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// rzx <stop>
JIMAPI_FN(rzx) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 2 ? Jim_String(argv[2]) : ""), "stop", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  const char *a0;
  int res = 0;
  if (argc < 2) {
    //Jim_WrongNumArgs(interp, 1, argv, "action name");
    //return JIM_ERR;
    cprintf("%sRZX is playing now.\n", (zxRZX != NULL ? "" : "no "));
    Jim_SetResultBool(interp, 1);
    return JIM_OK;
  }
  if ((a0 = parseAbbrev(Jim_String(argv[1]), "stop", NULL)) == NULL) {
    jim_SetResStrf(interp, "%s: invalid action: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
    return JIM_ERR;
  }
  if (strEqu(a0, "stop")) {
    if (zxRZX != NULL) cprintf("RZX playback aborted!\n");
    lssnap_rzx_stop();
    res = 1;
  }
  Jim_SetResultBool(interp, res);
  return JIM_OK;
}


// peek addr [bank-not-yet]
JIMAPI_FN(peek) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    jim_completeAddr(interp, res, argc, argv);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  int addr;
  if (argc != 2) { Jim_WrongNumArgs(interp, 1, argv, "addr"); return JIM_ERR; }
  if ((addr = jim_AddrArg(interp, argv[0], argv[1])) < 0) return JIM_ERR;
  Jim_SetResultInt(interp, z80.mem_read(&z80, addr, ZYM_MEMIO_OTHER));
  return JIM_OK;
}


// wpeek addr [bank-not-yet]
JIMAPI_FN(wpeek) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    jim_completeAddr(interp, res, argc, argv);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  int addr;
  uint16_t res;
  if (argc != 2) { Jim_WrongNumArgs(interp, 1, argv, "addr"); return JIM_ERR; }
  if ((addr = jim_AddrArg(interp, argv[0], argv[1])) < 0) return JIM_ERR;
  res = z80.mem_read(&z80, addr, ZYM_MEMIO_OTHER);
  res |= ((uint16_t)z80.mem_read(&z80, (addr+1)&0xffff, ZYM_MEMIO_OTHER))<<8;
  Jim_SetResultInt(interp, res);
  return JIM_OK;
}


// poke addr value
JIMAPI_FN(poke) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    jim_completeAddr(interp, res, argc, argv);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  int addr, val;
  if (argc != 3) { Jim_WrongNumArgs(interp, 1, argv, "addr"); return JIM_ERR; }
  if ((addr = jim_AddrArg(interp, argv[0], argv[1])) < 0) return JIM_ERR;
  if ((val = jim_BWArg(interp, argv[0], argv[2], 1)) < 0) return JIM_ERR;
  z80.mem_write(&z80, addr, val, ZYM_MEMIO_OTHER);
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// wpoke addr value
JIMAPI_FN(wpoke) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    jim_completeAddr(interp, res, argc, argv);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  int addr, val;
  if (argc != 3) { Jim_WrongNumArgs(interp, 1, argv, "addr"); return JIM_ERR; }
  if ((addr = jim_AddrArg(interp, argv[0], argv[1])) < 0) return JIM_ERR;
  if ((val = jim_BWArg(interp, argv[0], argv[2], 2)) < 0) return JIM_ERR;
  z80.mem_write(&z80, addr, val&0xff, ZYM_MEMIO_OTHER);
  z80.mem_write(&z80, (addr+1)&0xffff, (val>>8)&0xff, ZYM_MEMIO_OTHER);
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// scrattrfill value
JIMAPI_FN(scrattrfill) {
  if (jimapiDisableCompletion(interp, argc, argv)) return JIM_OK;

  long val;
  if (argc != 2) { Jim_WrongNumArgs(interp, 1, argv, "value"); return JIM_ERR; }
  if ((val = jim_BWArg(interp, argv[0], argv[1], 1)) < 0) return JIM_ERR;
  for (int f = 22528; f < 22528+768; ++f) z80.mem_write(&z80, f, val&0xff, ZYM_MEMIO_OTHER);
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
#include "autotape_128.c"
#include "autotape_48.c"
#include "autotape_p128.c"
#include "autotape_p2a.c"
#include "autotape_p3.c"


static void tape_do_autoload (void) {
  FILE *fl = NULL;
  switch (zxModel) {
    case ZX_MACHINE_48K: fl = fmemopen((void *)autotape_48, sizeof(autotape_48), "rb"); break;
    case ZX_MACHINE_128K:
    case ZX_MACHINE_PLUS2:
      fl = fmemopen((void *)autotape_128, sizeof(autotape_128), "rb");
      break;
    case ZX_MACHINE_PLUS2A:
      fl = fmemopen((void *)autotape_p2a, sizeof(autotape_p2a), "rb");
      break;
    case ZX_MACHINE_PLUS3:
      fl = fmemopen((void *)autotape_p3, sizeof(autotape_p3), "rb");
      break;
    case ZX_MACHINE_PENTAGON: fl = fmemopen((void *)autotape_p128, sizeof(autotape_p128), "rb"); break;
    case ZX_MACHINE_SCORPION: /* nothing */
    default: return;
  }
  if (fl != NULL) {
    szxLoad(fl);
    fclose(fl);
  }
}


////////////////////////////////////////////////////////////////////////////////
// tape cmd [args...]
//   insert filename
//   save filename (not yet)
//   eject
//   clear
//   start
//   stop
//   rewind (doesn't stop the tape)
//   list (not yet)
//   option
//   _autoload
//
JIMAPI_FN(tape) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    //fprintf(stderr, "ARGC=%d\n", argc); for (int f = 0; f < argc; ++f) fprintf(stderr, "  #%d: <%s>\n", f, Jim_String(argv[f]));
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    int argp = 1;
    char *a0 = completeAbbrev((argc > argp ? Jim_String(argv[argp++]) : ""), "insert", "eject", "clear", "start", "stop", "rewind", "option", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    const char *a1 = (isFullyCompleted(a0) ? parseAbbrev(a0, "insert", "save", "option", NULL) : NULL);
    if (a1 != NULL) {
      // fill/autocomplete command args
      if (strEqu(a1, "insert") || strEqu(a1, "save")) {
        // insert/save
        free(a0);
        a0 = completeFilesWithExts((argc > argp ? Jim_String(argv[argp++]) : ""), snapshotExtensions);
        jimapiStrAppendAbbrev(interp, res, a0);
      } else if (strEqu(a1, "option")) {
        free(a0);
        a0 = completeAbbrev((argc > argp ? Jim_String(argv[argp++]) : ""), "maxspeed", "detectloader", "flashload", "sound", "ugly", NULL);
        jimapiStrAppendAbbrev(interp, res, a0);
        if (isFullyCompleted(a0)) {
          a1 = parseAbbrev(a0, "maxspeed", "detectloader", "flashload", "sound", "ugly", NULL);
          if (a1 != NULL) {
            free(a0);
            a0 = completeAbbrev((argc > argp ? Jim_String(argv[argp++]) : ""), "on", "off", "toggle", NULL);
            jimapiStrAppendAbbrev(interp, res, a0);
          }
        }
      }
    }
    free(a0);
    jimapiAppendRestArgs(res, argp);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  int res = 1;

  if (argc < 2) {
    //Jim_WrongNumArgs(interp, 1, argv, "cmd");
    cprintf(
      "tape commands:\n"
      " insert filename\n"
      " save filename (not yet)\n"
      " eject\n"
      " clear\n"
      " start\n"
      " stop\n"
      " rewind (doesn't stop the tape)\n"
      " list (not yet)\n"
      " option\n"
    );
    return JIM_OK;
  }

  if (strEquCI(Jim_String(argv[1]), "_autoload")) {
    tape_do_autoload();
    goto done;
  }

  const char *a0 = parseAbbrev(Jim_String(argv[1]), "insert", "eject", "clear", "start", "go", "stop", "rewind", "list", "save", "option", NULL);

  if (a0 == NULL) {
    jim_SetResStrf(interp, "%s: invalid tape command: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
    goto done;
  }

  if (strEqu(a0, "insert")) {
    if (argc < 3) { Jim_WrongNumArgs(interp, 1, argv, "name"); return JIM_ERR; }
    if (loadSnapshot(Jim_String(argv[2]), SNAPLOAD_TAPES) != 0) {
      cprintf("tape: can't insert '%s'\n", Jim_String(argv[2]));
      res = 0;
    } else {
      cprintf("tape: inserted '%s'\n", Jim_String(argv[2]));
    }
  } else if (strEqu(a0, "eject")) {
    emuUnloadTape();
    cprintf("tape: unloaded\n");
  } else if (strEqu(a0, "clear")) {
    emuClearTape();
    cprintf("tape: cleared\n");
  } else if (strEqu(a0, "start") || strEqu(a0, "go")) {
    emuStartTape();
    cprintf("tape: started\n");
  } else if (strEqu(a0, "stop")) {
    emuStopTape();
    cprintf("tape: stopped\n");
  } else if (strEqu(a0, "rewind")) {
    emuRewindTape();
    cprintf("tape: rewound\n");
  } else if (strEqu(a0, "list")) {
    cprintf("tape: no listing yet...\n");
  } else if (strEqu(a0, "save")) {
    cprintf("tape: no saving yet...\n");
  } else if (strEqu(a0, "option")) {
    int *opt = NULL;
    if (argc < 3) {
      cprintf(
        "tape options:\n"
        "  maxspeed      on/off/toggle\n"
        "  detectloader  on/off/toggle\n"
        "  autopause     on/off/toggle\n"
        "  sound         on/off/toggle\n"
        "  ugly          on/off/toggle\n"
      );
      goto done;
    }
    a0 = parseAbbrev(Jim_String(argv[2]), "maxspeed", "detectloader", "autopause", "flashload", "sound", "ugly", NULL);
    if (a0 == NULL) {
      jim_SetResStrf(interp, "%s: invalid tape option: '%s'", Jim_String(argv[0]), Jim_String(argv[2]));
      return JIM_ERR;
    }
         if (strEqu(a0, "maxspeed")) opt = &optTapeMaxSpeed;
    else if (strEqu(a0, "detectloader")) opt = &optTapeDetector;
    else if (strEqu(a0, "autopause")) opt = &optTapeAutoPauseEmu;
    else if (strEqu(a0, "flashload")) opt = &optFlashLoad;
    else if (strEqu(a0, "sound")) opt = &optTapeSound;
    else if (strEqu(a0, "ugly")) opt = &optTapeAllowMaxSpeedUgly;
    //
    if (argc > 3) {
      Jim_Obj *xargv[2];
      xargv[0] = argv[0];
      xargv[1] = argv[3];
      if (jim_onoff_option(opt, interp, 2, xargv, "on", "off", 1) != JIM_OK) {
        jim_SetResStrf(interp, "%s: invalid mode for '%s': '%s'", Jim_String(argv[0]), a0, Jim_String(argv[1]));
        return JIM_ERR;
      }
    }
    cprintf("tape: %s: %s\n", a0, (*opt ? "on" : "off"));
  }

done:
  Jim_SetResultBool(interp, res);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// disk cmd [args...]
//   insert filename
//   save filename
//   eject
//   format [il=interleave] [sofs=sector_offset]
//   option autoadd_boot
//   boot add [name] | remove | replace [name]
//   protect
//   unprotect
//   info
//   list
//   extract
//   add
//
JIMAPI_FN(disk) {
  int diskno = 0;

  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    int argp = 1;
    const char *a1;

    if (argp < argc) {
      a1 = Jim_String(argv[argp]);
      if (strlen(a1) == 2 && a1[1] == ':' && strchr("abcdABCD", a1[0]) != NULL) {
        ++argp;
        diskno = tolower(a1[0])-'a';
        jimapiStrAppendSpace(interp, res);
        jimapiStrAppendCStr(interp, res, a1, -1);
        jimapiStrAppendSpace(interp, res);
      }
    }

    char *a0 = completeAbbrev((argc > argp ? Jim_String(argv[argp++]) : ""), "insert", "save", "eject", "format", "boot", "option", "protect", "unprotect", "list", "extract", "add", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);

    a1 = (isFullyCompleted(a0) ? parseAbbrev(a0, "insert", "save", "boot", "option", "extract", "add", "format", NULL) : NULL);
    if (a1 != NULL) {
      // fill/autocomplete command args
      if (strEqu(a1, "boot")) {
        free(a0);
        a0 = completeAbbrev((argc > argp ? Jim_String(argv[argp++]) : ""), "add", "remove", "replace", NULL);
        jimapiStrAppendAbbrev(interp, res, a0);
        if (isFullyCompleted(a0) && (strEqu(a0, "add ") || strEqu(a0, "replace "))) {
          free(a0);
          a0 = completeAbbrev((argc > argp ? Jim_String(argv[argp++]) : ""), "stealth32", "stealth54", "onesector", "phantom", "kmouse", "ugly", NULL);
          jimapiStrAppendAbbrev(interp, res, a0);
        }
      } else if (strEqu(a1, "option")) {
        free(a0);
        a0 = completeAbbrev((argc > argp ? Jim_String(argv[argp++]) : ""), "autoadd_boot", "turbo", "traps", NULL);
        jimapiStrAppendAbbrev(interp, res, a0);
        if (isFullyCompleted(a0) && (strEqu(a0, "autoadd_boot ") || strEqu(a0, "traps "))) {
          free(a0);
          a0 = completeAbbrev((argc > argp ? Jim_String(argv[argp++]) : ""), "yes", "no", "on", "off", "toggle", NULL);
          jimapiStrAppendAbbrev(interp, res, a0);
        } else if (isFullyCompleted(a0) && strEqu(a0, "turbo ")) {
          free(a0);
          a0 = completeAbbrev((argc > argp ? Jim_String(argv[argp++]) : ""), "yes", "no", "on", "off", "normal", "toggle", NULL);
          jimapiStrAppendAbbrev(interp, res, a0);
        }
      } else if (strEqu(a1, "extract")) {
        free(a0);
        //fprintf(stderr, "argc=%d; argp=%d; <%s>\n", argc, argp, (argc > argp ? Jim_String(argv[argp]) : ""));
        a0 = completeTRDiskFiles((argc > argp ? Jim_String(argv[argp++]) : ""), beta_get_disk(diskno));
        jimapiStrAppendAbbrev(interp, res, a0);
      } else if (strEqu(a1, "format")) {
        free(a0);
        a0 = completeAbbrev((argc > argp ? Jim_String(argv[argp++]) : ""), "il=", "sofs=", NULL);
        jimapiStrAppendAbbrev(interp, res, a0);
      } else if (strEqu(a1, "insert") || strEqu(a1, "save")) {
        // insert/save
        free(a0);
        a0 = completeFilesWithExts((argc > argp ? Jim_String(argv[argp++]) : ""), snapshotExtensions);
        jimapiStrAppendAbbrev(interp, res, a0);
      }
    }
    free(a0);
    jimapiAppendRestArgs(res, argp);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  const char *a0;
  int argp = 1;

  if (argc > 1) {
    a0 = Jim_String(argv[1]);
    if (strlen(a0) == 2 && a0[1] == ':' && strchr("abcdABCD", a0[0]) != 0) {
      ++argp;
      diskno = tolower(a0[0])-'a';
    }
  }

  int res = 1;
  disk_t *flp = beta_get_disk(diskno);

  if (argp >= argc) {
    //Jim_WrongNumArgs(interp, 1, argv, "cmd");
    return JIM_OK;
  }

  if ((a0 = parseAbbrev(Jim_String(argv[argp++]), "insert", "eject", "format", "list", "save", "option", "boot", "protect", "unprotect", "list", "extract", "add", NULL)) == NULL) {
    jim_SetResStrf(interp, "%s: invalid disk command: '%s'", Jim_String(argv[0]), Jim_String(argv[argp-1]));
    goto done;
  }

  // insert
  if (strEqu(a0, "insert")) {
    if (argp >= argc) { Jim_WrongNumArgs(interp, 1, argv, "name"); return JIM_ERR; }
    if (loadSnapshot(Jim_String(argv[argp]), SNAPLOAD_DISKS|(diskno<<SNAPLOAD_FLPNOSHIFT)) != 0) {
      cprintf("disk: can't insert '%s'\n", Jim_String(argv[argp]));
      res = 0;
    } else {
      cprintf("disk: inserted '%s'\n", Jim_String(argv[argp]));
    }
    goto done;
  }

  // eject
  if (strEqu(a0, "eject")) {
    emuDiskEject(diskno);
    cprintf("disk: ejected\n");
    goto done;
  }

  // format
  if (strEqu(a0, "format")) {
    emuDiskEject(diskno);
    if (emuIsUPD765()) {
      int fres = p3dskFormatDisk(flp, P3DSK_PCW_SS);
      flp->dirty = 0;
      if (fres == FLPERR_OK) {
        emuDiskInsert(diskno);
        cprintf("disk: formated +3DOS disk\n");
      } else {
        cprintf("disk: +3DOS formatting error!\n");
      }
    } else {
      int fres = flpFormatTRD(flp);
      flp->dirty = 0;
      if (fres == FLPERR_OK) {
        emuDiskInsert(diskno);
        cprintf("disk: formated TR-DOS disk\n");
      } else {
        cprintf("disk: TR-DOS formatting error!\n");
      }
    }
    goto done;
  }

  // save
  if (strEqu(a0, "save")) {
    if (argp >= argc) { Jim_WrongNumArgs(interp, 1, argv, "name"); return JIM_ERR; }
    const char *fname = Jim_String(argv[argp++]);
    const int wres = emuSaveDiskToFile(diskno, fname);
    if (wres != DISK_OK) {
      cprintf("\4ERROR: can't save disk to file: '%s' (%s)\n", fname, disk_strerror(wres));
    } else {
      cprintf("saved disk to file: '%s'\n", fname);
    }
    res = (wres == 0);
    goto done;
  }

  // boot
  if (strEqu(a0, "boot")) {
    if (argp >= argc) { Jim_WrongNumArgs(interp, 1, argv, "action"); return JIM_ERR; }
    if ((a0 = parseAbbrev(Jim_String(argv[argp++]), "add", "remove", "replace", NULL)) != NULL) {
      if (strEqu(a0, "add") || strEqu(a0, "replace")) {
        int rep = (strEqu(a0, "replace"));
        const char *bootname = (argp >= argc ? "phantom" : Jim_String(argv[argp++]));
        a0 = parseAbbrev(bootname, "stealth32", "stealth54", "onesector", "phantom", "kmouse", "ugly", NULL);
        if (a0 != NULL) {
          const uint8_t *b = NULL;
          int bootsz = 0;
               if (strEqu(a0, "stealth32")) { b = sboot32; bootsz = (int)sizeof(sboot32); }
          else if (strEqu(a0, "stealth54")) { b = sboot54; bootsz = (int)sizeof(sboot54); }
          else if (strEqu(a0, "onesector")) { b = bootsmall; bootsz = (int)sizeof(bootsmall); }
          else if (strEqu(a0, "phantom")) { b = bootph; bootsz = (int)sizeof(bootph); }
          else if (strEqu(a0, "kmouse")) { b = kmboot; bootsz = (int)sizeof(kmboot); }
          else if (strEqu(a0, "ugly")) { b = unrealboot; bootsz = (int)sizeof(unrealboot); }
          if (flp->data) {
            if (flpDetectDiskType(flp) == FLP_DISK_TYPE_TRDOS) {
              FILE *fl = fmemopen((void *)b, bootsz, "rb");
              res = flpSetBootTRD(flp, fl, rep);
              fclose(fl);
              if (res == 0) {
                res = 1;
                cprintf("disk: boot '%s' added\n", a0);
              } else {
                cprintf("disk: can't add boot '%s'\n", a0);
                res = 0;
              }
            } else {
              res = 0;
              cprintf("disk: not a TR-DOS disk!\n");
            }
          } else {
            res = 0;
            cprintf("disk: no disk in drive!\n");
          }
        } else {
          jim_SetResStrf(interp, "%s: invalid disk boot name: '%s'", Jim_String(argv[0]), bootname);
          goto done;
        }
      } else if (strEqu(a0, "remove")) {
        if (flpRemoveBootTRD(flp) == 0) {
          cprintf("disk: boot removed\n");
        }
      }
    } else {
      jim_SetResStrf(interp, "%s: invalid disk boot command: '%s'", Jim_String(argv[0]), Jim_String(argv[argp-1]));
    }
    goto done;
  }

  // protect
  if (strEqu(a0, "protect")) {
    if (upd765_fdc && upd765_fdc->drive[diskno]) {
      fdd_wrprot(upd765_fdc->drive[diskno], 1);
    }
    if (beta_get_fdd(diskno)) {
      fdd_wrprot(beta_get_fdd(diskno), 1);
    }
    cprintf("disk: write-protection set\n");
    goto done;
  }

  // unprotect
  if (strEqu(a0, "unprotect")) {
    if (upd765_fdc && upd765_fdc->drive[diskno]) {
      fdd_wrprot(upd765_fdc->drive[diskno], 0);
    }
    if (beta_get_fdd(diskno)) {
      fdd_wrprot(beta_get_fdd(diskno), 0);
    }
    cprintf("disk: write-protection reset\n");
    goto done;
  }

  // option
  if (strEqu(a0, "option")) {
    if (argp >= argc) { Jim_WrongNumArgs(interp, 1, argv, "option"); return JIM_ERR; }
    if ((a0 = parseAbbrev(Jim_String(argv[argp++]), "autoadd_boot", "turbo", "traps", NULL)) != NULL) {
      if (strEqu(a0, "autoadd_boot")) {
        if (argp < argc) {
          if ((a0 = parseAbbrev(Jim_String(argv[argp++]), "yes", "no", "on", "off", "toggle", NULL)) != NULL) {
                 if (strEqu(a0, "yes") || strEqu(a0, "on")) optAutoaddBoot = 1;
            else if (strEqu(a0, "toggle")) optAutoaddBoot = !optAutoaddBoot;
            else optAutoaddBoot = 0;
          } else {
            cprintf("disk: invalid 'autoadd_boot' option argument: '%s'!\n", Jim_String(argv[argp]));
            goto done;
          }
        }
        if (jimapiFromConsole(interp)) cprintf("disk autoadd_boot: %s\n", (optAutoaddBoot ? "yes" : "no"));
        res = !!optAutoaddBoot;
      } else if (strEqu(a0, "turbo")) {
        if (argp < argc) {
          if ((a0 = parseAbbrev(Jim_String(argv[argp]), "yes", "no", "on", "off", "normal", "toggle", NULL)) != NULL) {
            if (strEqu(a0, "yes") || strEqu(a0, "on")) {
              fdd_turbo = FDD_TURBO_SUPER;
            } else if (strEqu(a0, "toggle")) {
              fdd_turbo = (fdd_turbo ? FDD_TURBO_NONE : FDD_TURBO_SUPER);
            } else if (strEqu(a0, "normal")) {
              fdd_turbo = FDD_TURBO_TURBO;
            } else {
              fdd_turbo = FDD_TURBO_NONE;
            }
            if (upd765_fdc) {
              upd_fdc_alloc_fdc_set_clock(upd765_fdc, (fdd_turbo ? UPD_CLOCK_8MHZ : UPD_CLOCK_4MHZ));
            }
          } else {
            cprintf("disk: invalid 'turbo' option argument: '%s'!\n", Jim_String(argv[argp]));
            goto done;
          }
        }
        res = (fdd_turbo != FDD_TURBO_NONE);
        if (jimapiFromConsole(interp)) {
          cprintf("disk turbo: %s\n", (fdd_turbo == FDD_TURBO_NONE ? "none" : fdd_turbo == FDD_TURBO_TURBO ? "normal" : "super"));
        }
      } else if (strEqu(a0, "traps")) {
        if (argp < argc) {
          if ((a0 = parseAbbrev(Jim_String(argv[argp]), "yes", "no", "on", "off", "toggle", "debug", "nodebug", NULL)) != NULL) {
                 if (strEqu(a0, "yes") || strEqu(a0, "on")) optTRDOSTraps = 1;
            else if (strEqu(a0, "toggle")) optTRDOSTraps = !optTRDOSTraps;
            else if (strEqu(a0, "debug")) optTRDOSTrapDebug = 1;
            else if (strEqu(a0, "nodebug")) optTRDOSTrapDebug = 0;
            else optTRDOSTraps = 0;
          } else {
            cprintf("disk: invalid 'traps' option argument: '%s'!\n", Jim_String(argv[argp]));
            goto done;
          }
        }
        if (jimapiFromConsole(interp)) cprintf("disk traps: %s\n", (optTRDOSTraps ? "yes" : "no"));
        res = !!optTRDOSTraps;
        //cprintf("\1WARNING: TR-DOS traps are not implemented yet!\n");
        //res = 0;
      }
    } else {
      cprintf("disk: invalid option: '%s'!\n", Jim_String(argv[argp-1]));
    }
    goto done;
  }

  // list
  if (strEqu(a0, "list")) {
    int wantsizes = 0;
    if (argp < argc) wantsizes = strEquCI(Jim_String(argv[argp++]), "size");
    int count;
    FloppyFileInfo *dir = anyDiskBuildDir(flp, &count);
    Jim_Obj *rlist = Jim_NewListObj(interp, NULL, 0);
    if (count < 1) {
      if (jimapiFromConsole(interp)) cprintf("no files\n");
    } else {
      if (jimapiFromConsole(interp)) cprintf("\2=================\n");
      for (int f = 0; f < count; ++f) {
        if (jimapiFromConsole(interp)) cprintf("\3%-8s%-4s %8u\n", dir[f].name, dir[f].ext, dir[f].size);
        if (!wantsizes) {
          char *ss = strprintf("%s%s", dir[f].name, dir[f].ext);
          Jim_Obj *fnstr = Jim_NewStringObj(interp, ss, -1);
          free(ss);
          Jim_ListAppendElement(interp, rlist, fnstr);
        } else {
          Jim_Obj *fnsize = Jim_NewIntObj(interp, (jim_wide)dir[f].size);
          Jim_ListAppendElement(interp, rlist, fnsize);
        }
        /*
        if (conpos >= CON_WIDTH/2) {
          cprintf("\n");
        } else {
          while (conpos < CON_WIDTH/2) conPutChar(' ');
        }
        */
      }
      //if (conpos != CON_WIDTH) cprintf("\n");
    }
    if (dir) free(dir);
    Jim_SetResult(interp, rlist);
    return JIM_OK;
  }

  // extract
  if (strEqu(a0, "extract")) {
    //cprintf("\4EXTRACTOR IS BROKEN FOR NOW\n");
    if (argp >= argc) { Jim_WrongNumArgs(interp, 1, argv, "name"); return JIM_ERR; }
    const FloppyDiskType dtype = flpDetectDiskType(flp);
    if (dtype == FLP_DISK_TYPE_TRDOS) {
      int count;
      FloppyFileInfo *dir = anyDiskBuildDir(flp, &count);
      for (int f = 0; f < count; ++f) {
        char *fname = strprintf("%s%s", dir[f].name, dir[f].ext);
        int n;
        for (n = argp; n < argc; ++n) {
          if (strcasecmp(Jim_String(argv[n]), fname) == 0) break;
        }
        // try glob masks
        if (n >= argc) {
          for (n = argp; n < argc; ++n) {
            if (p3dskGlobMatch(fname, Jim_String(argv[n]), P3DSK_GLOB_DEFAULT)) break;
          }
        }
        // is found?
        if (n < argc) {
          FILE *fo = fopen(fname, "wb");
          if (fo != NULL) {
            int wres = flpSaveHoBeta(flp, fo, dir[f].index);
            fclose(fo);
            if (wres != FLPERR_OK) {
              unlink(fname);
              cprintf("disk: error extracting '%s'!\n", fname);
            } else {
              cprintf("disk: extracted '%s'!\n", fname);
            }
          } else {
            cprintf("disk: can't create '%s'!\n", fname);
          }
        }
        // done with this name
        free(fname);
      }
      free(dir);
    } else if (dtype == FLP_DISK_TYPE_P3DOS) {
      P3DiskInfo p3d;
      p3d.flp = flp;
      if (p3dskDetectGeom(p3d.flp, &p3d.geom) == FLPERR_OK) {
        P3DskFileInfo nfo;
        if (p3dskFindInit(&p3d, &nfo, "*.*") == FLPERR_OK) {
          while (p3dskFindNext(&nfo) > 0) {
            if (nfo.name[0]) {
              int n;
              for (n = argp; n < argc; ++n) {
                if (strcasecmp(Jim_String(argv[n]), nfo.name) == 0) break;
              }
              // try glob masks
              if (n >= argc) {
                for (n = argp; n < argc; ++n) {
                  if (p3dskGlobMatch(nfo.name, Jim_String(argv[n]), P3DSK_GLOB_DEFAULT)) break;
                }
              }
              // is found?
              if (n < argc) {
                FILE *fo = fopen(nfo.name, "wb");
                if (fo != NULL) {
                  int rd = 0;
                  if (nfo.size) {
                    uint8_t *buf = malloc(nfo.size);
                    rd = p3dskReadFile(&nfo, buf, 0, (int)nfo.size);
                    if (rd > 0) {
                      if (fwrite(buf, (size_t)rd, 1, fo) != 1) rd = -1;
                    }
                    free(buf);
                  }
                  fclose(fo);
                  if (rd < 0) {
                    unlink(nfo.name);
                    cprintf("disk: error extracting '%s'!\n", nfo.name);
                  } else {
                    cprintf("disk: extracted '%s'!\n", nfo.name);
                  }
                } else {
                  cprintf("disk: can't create '%s'!\n", nfo.name);
                }
              }
            }
          }
        }
      }
    }
  }

done:
  Jim_SetResultBool(interp, res);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// fldebug [on|off|toggle]
JIMAPI_FN(fldebug) {
  int v = optDebugFlashLoad;
  int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
  optDebugFlashLoad = v;
  Jim_SetResultBool(interp, v);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
// opense [on|off|toggle]
JIMAPI_FN(opense) {
  int v = optOpenSE;
  int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
  optOpenSE = v;
  Jim_SetResultBool(interp, v);
  return res;
}


// gluck [on|off|toggle]
JIMAPI_FN(gluck) {
  int v = optUseGluck;
  int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
  optUseGluck = v;
  Jim_SetResultBool(interp, v);
  return res;
}


// quickcommander [on|off|toggle]
JIMAPI_FN(quickcommander) {
  int v = optUseQCmdr;
  int res = jim_onoff_option(&v, interp, argc, argv, "on", "off", 1);
  optUseQCmdr = v;
  Jim_SetResultBool(interp, v);
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
// kbreset
JIMAPI_FN(kbreset) {
  emuResetKeyboard();
  cprintf("ZX keyboard reset.\n");
  return JIM_OK;
}


// ayreset
JIMAPI_FN(ayreset) {
  emuResetAY();
  cprintf("AY reset.\n");
  return JIM_OK;
}


// curblink mstime
JIMAPI_FN(curblink) {
  if (jimapiDisableCompletion(interp, argc, argv)) return JIM_OK;
  if (argc < 1 || argc > 2) {
    jim_SetResStrf(interp, "%s: expected cursor blink time (only)", Jim_String(argv[0]));
    return JIM_ERR;
  }

  if (argc > 1) {
    int idx = jimGetIntLitVal(interp, argv[1]);
    if (idx <= 0) idx = 0; else if (idx > 10000) idx = 10000;
    optCurBlinkMS = idx;
  }

  Jim_SetResultInt(interp, optCurBlinkMS);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// bp [type] cmd [args...]
// types: exec read write in out
//   set addrlist
//   unset addrlist
//   toggle addrlist
//   clear
//   load fname
//   xsave fname
//
JIMAPI_FN(bp) {
  uint8_t type = DBG_BP_EXEC;
  int deftype = 1;
  int argp = 1;
  int needfname = 0;
  // 0x01u: type; 0x02u: action
  unsigned wasctype = 0;

  const char *allcms[] = { "set", "unset", "toggle", "clear", "xsave", "load", "exec", "read", "write", "in", "out", NULL };
  const char *typecmds[] = { "exec", "read", "write", "in", "out", NULL };
  const char *actioncmds[] = { "set", "unset", "toggle", "clear", "xsave", "load", NULL };

  if (jimapiWantsCompletion(interp, argc, argv)) {
    const char *bpExtensions[] = { ".bpx", NULL };

    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    while (argp < argc) {
      // commands
      const char **ctype;
           if (wasctype == 0) ctype = allcms;
      else if (wasctype == 1) ctype = actioncmds;
      else ctype = typecmds;

      char *a0 = completeAbbrevArray(Jim_String(argv[argp]), ctype, ABBREV_CI);
      if (!isFullyCompleted(a0)) {
        free(a0);
        break;
      }
      jimapiStrAppendAbbrev(interp, res, a0);

      const char *a1 = parseAbbrevArray(a0, ctype, ABBREV_CI, NULL);
      const char cmdch = a1[0];
      free(a0);

           if (cmdch == 'x' || cmdch == 'l') needfname = 1;
      else if (cmdch == 'c') needfname = -1;

           if (strchr("erwio", cmdch)) wasctype |= 0x01u;
      else if (strchr("sutcxl", cmdch)) wasctype |= 0x02u;

      ++argp;
    }

    while (argp < argc && needfname >= 0) {
      char *a0;
      if (needfname) {
        a0 = completeFilesWithExts(Jim_String(argv[argp++]), bpExtensions);
      } else {
        a0 = completeLabelName(Jim_String(argv[argp++]));
      }
      jimapiStrAppendAbbrev(interp, res, a0);
      free(a0);
      if (needfname) break;
    }

    jimapiAppendRestArgs(res, argp);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  int res = 1;
  char command = 0;

  while (argp < argc) {
    // commands
    const char **ctype;
         if (wasctype == 0) ctype = allcms;
    else if (wasctype == 1) ctype = actioncmds;
    else ctype = typecmds;

    //cprintf("wasctype=%u; argp=%d; argc=%d; <%s>\n", wasctype, argp, argc, Jim_String(argv[argp]));
    int fullcmd = 0;
    const char *a0 = parseAbbrevArray(Jim_String(argv[argp]), ctype, ABBREV_CI, &fullcmd);
    if (!fullcmd) break;

    const char cmdch = a0[0];
         if (cmdch == 'x' || cmdch == 'l') needfname = 1;
    else if (cmdch == 'c') needfname = -1;
    else if (cmdch == 'e') { deftype = 0; type = DBG_BP_EXEC; }
    else if (cmdch == 'r') { deftype = 0; type = DBG_BP_READ; }
    else if (cmdch == 'w') { deftype = 0; type = DBG_BP_WRITE; }
    else if (cmdch == 'i') { deftype = 0; type = DBG_BP_PORTIN; }
    else if (cmdch == 'o') { deftype = 0; type = DBG_BP_PORTOUT; }

         if (strchr("erwio", cmdch)) wasctype |= 0x01u;
    else if (strchr("sutcxl", cmdch)) { command = cmdch; wasctype |= 0x02u; }

    ++argp;
  }

  if (!command) {
    jim_SetResStrf(interp, "%s: bp action expected", Jim_String(argv[0]));
    return JIM_ERR;
  }

  if (needfname >= 0 && argp >= argc) {
    jim_SetResStrf(interp, "%s: bp action argument expected", Jim_String(argv[0]));
    return JIM_ERR;
  }

  // clear
  if (command == 'c') {
    dbgBPClear();
    if (jimapiFromConsole(interp)) cprintf("breakpoints cleared\n");
    Jim_SetResultBool(interp, res);
    return JIM_OK;
  }

  if (needfname > 0) {
    res = 0;
    const char *name = Jim_String(argv[argp]);
    if (name && name[0]) {
      char *fname = NULL;
      const char *ext = strrchr(name, '.');
      if (!ext || strcasecmp(ext, ".bpx") != 0) {
        fname = malloc(strlen(name)+8);
        strcpy(fname, name);
        strcat(fname, ".bpx");
      } else {
        fname = strdup(name);
      }
      if (command == 'l') {
        // load
        if (dbgBPLoadFromFile(fname) == 0) {
          cprintf("breakpoints loaded from '%s'\n", fname);
        } else {
          cprintf("\4ERROR loading breakpoints from '%s'\n", fname);
        }
      } else {
        // save
        if (dbgBPSaveToFile(fname) == 0) {
          cprintf("breakpoints saved to '%s'\n", fname);
        } else {
          cprintf("\4ERROR saving breakpoints to '%s'\n", fname);
        }
      }
      free(fname);
    }
  } else if (needfname == 0) {
    // set, unset, toggle
    if (command == 'u' && deftype) type = 0xffu; /* clear all */
    while (argp < argc) {
      int addr = jimGetAddrOrLabel(interp, argv[argp]);
      if (addr < 0) {
        cprintf("\x4%s: cannot understand '%s'\n", Jim_String(argv[0]), Jim_String(argv[argp]));
      } else {
        switch (command) {
          case 's': dbgSetBPType(addr, dbgGetBPType(addr)|type); break;
          case 'u': dbgSetBPType(addr, dbgGetBPType(addr)&(~type)); break;
          case 't': dbgSetBPType(addr, dbgGetBPType(addr)^type); break;
        }
        if (dbgGetBPType(addr)) {
          if (jimapiFromConsole(interp)) cprintf("changed breakpoint at #%04X (%s)\n", (unsigned)(addr&0xffffu), dbgGetBPTypeStr(dbgGetBPType(addr)));
        } else {
          if (jimapiFromConsole(interp)) cprintf("cleared breakpoint at #%04X\n", (unsigned)(addr&0xffffu));
        }
      }
      ++argp;
    }
  }

  Jim_SetResultBool(interp, res);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// label cmd [args...]
//   set name addr
//   del addr-or-name
//   goto addr-or-name
//   get addr-or-name
//   clear
//
JIMAPI_FN(label) {
  const char *commands[7] = { "set", "del", "goto", "get", "remove", "clear", NULL };

  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    jimapiStrAppendSpace(interp, res);
    int argp = 1;
    char *a0 = completeAbbrevArray((argc > argp ? Jim_String(argv[argp++]) : ""), commands, ABBREV_CI);
    jimapiStrAppendAbbrev(interp, res, a0);
    if (argc > argp && isFullyCompleted(a0)) {
      const char *astr = parseAbbrevArray(a0, commands, ABBREV_CI, NULL);
      if (astr && !strEqu(astr, "clear")) {
        free(a0);
        a0 = completeLabelName(Jim_String(argv[argp++]));
        jimapiStrAppendAbbrev(interp, res, a0);
      }
    }
    free(a0);
    jimapiAppendRestArgs(res, argp);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  const char *a0;
  int argp = 1;
  if (argc < 2) {
    //Jim_WrongNumArgs(interp, 1, argv, "cmd");
    return JIM_OK;
  }

  if ((a0 = parseAbbrevArray(Jim_String(argv[argp++]), commands, ABBREV_CI, NULL)) == NULL) {
    jim_SetResStrf(interp, "%s: unknown command: '%s'", Jim_String(argv[0]), Jim_String(argv[argp-1]));
    return JIM_ERR;
  }

  if (strEqu(a0, "clear")) {
    dbgClearLabels();
    if (jimapiFromConsole(interp)) cprintf("labels cleared\n");
    Jim_SetResultBool(interp, 1);
    return JIM_OK;
  }

  if (argp >= argc) {
    jim_SetResStrf(interp, "%s: invalid command: '%s'", Jim_String(argv[0]), Jim_String(argv[argp-1]));
    return JIM_ERR;
  }

  if (a0[0] == 'd' || a0[0] == 'r') {
    // remove label
    while (argp < argc) {
      long addr = -1;
      if (Jim_GetLong(interp, argv[argp], &addr) != JIM_OK) {
        const char *lbl = Jim_String(argv[argp]);
        addr = dbgFindLabelByName(lbl);
        if (addr < 0) {
          cprintf("\x4%s: cannot understand '%s'\n", Jim_String(argv[0]), lbl);
        } else {
          dbgRemoveLabel(lbl);
          if (jimapiFromConsole(interp)) cprintf("removed label '%s' at #%04X\n", lbl, (unsigned)(addr&0xffffu));
        }
      } else {
        if (addr < 0 || addr > 65535) {
          cprintf("\x4%s: invalid address %d\n", Jim_String(argv[0]), (int)addr);
        } else {
          const char *lbl = dbgFindLabelByVal(addr, 0/*asoffset*/);
          if (lbl) {
            dbgRemoveLabel(lbl);
            if (jimapiFromConsole(interp)) cprintf("removed label '%s' at #%04X\n", lbl, (unsigned)(addr&0xffffu));
          }
        }
      }
      ++argp;
    }
    Jim_SetResultBool(interp, 1);
    return JIM_OK;
  }

  if (a0[0] == 'g') {
    // goto label / get label
    if (argc != 3) {
      jim_SetResStrf(interp, "expects one argument");
      return JIM_ERR;
    }
    int addr = jimGetAddrOrLabel(interp, argv[2]);
    if (addr < 0) {
      if (a0[1] == 'e') {
        // get
        cprintf("\4ERROR: no label '%s'!\n", Jim_String(argv[2]));
        Jim_SetResultString(interp, "", -1);
        return JIM_OK;
      }
      jim_SetResStrf(interp, "expects addr or name");
      return JIM_ERR;
    }
    if (a0[1] != 'e') dbgSetUnasmAddr((uint16_t)(addr&0xffffu));
    if (jimapiFromConsole(interp)) cprintf("label '%s' is %d\n", Jim_String(argv[2]), addr);
    Jim_SetResultInt(interp, addr);
    return JIM_OK;
  }

  if (a0[0] == 's') {
    // set
    if (argc != 4) {
      jim_SetResStrf(interp, "expects two args");
      return JIM_ERR;
    }
    const char *lname = Jim_String(argv[2]);
    int addr = jimGetAddrOrLabel(interp, argv[3]);
    if (addr < 0) {
      jim_SetResStrf(interp, "expects addr and name");
      return JIM_ERR;
    }
    if (lname && lname[0] && lname[0] != '#' && lname[0] != '$' && digitInBase(lname[0], 10) < 0) {
      dbgAddLabel(addr&0xffffu, lname, 0/*asoffset*/);
      if (jimapiFromConsole(interp)) cprintf("set label '%s' at address #%04X\n", lname, (unsigned)(addr&0xffffu));
    }
    Jim_SetResultInt(interp, (unsigned)(addr&0xffffu));
    return JIM_OK;
  }

  Jim_SetResultBool(interp, 0);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// reffile <load|save|clear> [name]
// "reference file" (urasm generates those)
JIMAPI_FN(reffile) {
  const char *refExtensions[] = { ".ref", NULL };

  // autocompletion
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    int argp = 1;
    char *a0 = completeAbbrev((argc > argp ? Jim_String(argv[argp++]) : ""), "load", "save", "clear", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    if (isFullyCompleted(a0)) {
      const char *a1 = parseAbbrev(a0, "load", "save", NULL);
      if (a1 != NULL) {
        // load/save
        free(a0);
        a0 = completeFilesWithExts((argc > argp ? Jim_String(argv[argp++]) : ""), refExtensions);
        jimapiStrAppendAbbrev(interp, res, a0);
      }
    }
    free(a0);
    jimapiAppendRestArgs(res, argp);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  const char *a0;
  int res = 0;

  if (argc < 2) { Jim_WrongNumArgs(interp, 1, argv, "action name"); return JIM_ERR; }
  if ((a0 = parseAbbrev(Jim_String(argv[1]), "load", "save", "clear", NULL)) == NULL) {
    jim_SetResStrf(interp, "%s: invalid action: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
    return JIM_ERR;
  }

  if (strEqu(a0, "load")) {
    if (argc < 3) { Jim_WrongNumArgs(interp, 1, argv, "file name"); return JIM_ERR; }
    res = (dbgLoadRefFile(Jim_String(argv[2])) == 0);
    if (res) cprintf("'%s' loaded\n", Jim_String(argv[2])); else cprintf("failed to load '%s'\n", Jim_String(argv[2]));
  } else if (strEqu(a0, "save")) {
    if (argc < 3) { Jim_WrongNumArgs(interp, 1, argv, "file name"); return JIM_ERR; }
    res = (dbgSaveRefFile(Jim_String(argv[2])) == 0);
    if (res) cprintf("'%s' saved\n", Jim_String(argv[2])); else cprintf("failed to save '%s'\n", Jim_String(argv[2]));
  } else if (strEqu(a0, "clear")) {
    dbgClearLabels();
    cprintf("labels cleared\n");
    res = 1;
  }
  Jim_SetResultBool(interp, res);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// tsmark [force]
JIMAPI_FN(tsmark) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "force", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc > 2) {
    jim_SetResStrf(interp, "%s: wut?!", Jim_String(argv[0]));
    return JIM_ERR;
  }

  if (argc > 1) {
    const char *a0 = parseAbbrev(Jim_String(argv[1]), "force", NULL);
    if (a0 == NULL) {
      jim_SetResStrf(interp, "%s: invalid option: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
      return JIM_ERR;
    }
    tsmark_active = 0;
  }

  if (tsmark_active) {
    cprintf("\4tstate counter is already active!\n");
    Jim_SetResultBool(interp, 0);
  } else {
    tsmark_active = 1;
    tsmark_frames = 0;
    tsmark_tstart = z80.tstates;
    if (jimapiFromConsole(interp)) cprintf("\1tstate counter started... (ts=%d)\n", z80.tstates);
    Jim_SetResultBool(interp, 1);
  }

  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// tsdiff [keep]
JIMAPI_FN(tsdiff) {
  if (jimapiWantsCompletion(interp, argc, argv)) {
    Jim_Obj *res = Jim_NewStringObj(interp, Jim_String(argv[0]), -1);
    char *a0 = completeAbbrev((argc > 1 ? Jim_String(argv[1]) : ""), "keep", NULL);
    jimapiStrAppendAbbrev(interp, res, a0);
    free(a0);
    jimapiAppendRestArgs(res, 2);
    Jim_SetResult(interp, res);
    return JIM_OK;
  }

  if (argc > 2) {
    jim_SetResStrf(interp, "%s: wut?!", Jim_String(argv[0]));
    return JIM_ERR;
  }

  int doreset = 1;

  if (argc > 1) {
    const char *a0 = parseAbbrev(Jim_String(argv[1]), "keep", NULL);
    if (a0 == NULL) {
      jim_SetResStrf(interp, "%s: invalid option: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
      return JIM_ERR;
    }
    doreset = 0;
  }

  if (tsmark_active) {
    if (doreset) tsmark_active = 0;
    const int tstotal = (machineInfo.tsperframe ? machineInfo.tsperframe-tsmark_tstart+(tsmark_frames-1)*machineInfo.tsperframe+z80.tstates : z80.tstates-tsmark_tstart);
    const int fullfrm = tstotal/machineInfo.tsperframe;
    if (doreset) { if (jimapiFromConsole(interp)) cprintf("counter stopped!\n"); }
    if (jimapiFromConsole(interp)) {
      cprintf("\1tstate counter: %d\n", tstotal);
      cprintf("\2(%d full frame%s, %d in last frame)\n", fullfrm, (fullfrm != 1 ? "s" : ""), tstotal-fullfrm*machineInfo.tsperframe);
    }
    Jim_SetResultInt(interp, tstotal);
  } else {
    cprintf("\4tstate counter is not active!\n");
    Jim_SetResultInt(interp, 0);
  }
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// z80 -- various command to manipulate Z80 CPU from Tcl
// always returns old register value
//   getreg <name> -- get register
//   setreg <name> <value> -- set register
// this is not a console command (yet), so we don't need any autocompletion here
JIMAPI_FN(z80) {
  if (argc < 3 || argc > 4) {
    jim_SetResStrf(interp, "%s: wut?!", Jim_String(argv[0]));
    return JIM_ERR;
  }

  const char *cmd = Jim_String(argv[1]);
  int cmdread = (strEquCI(cmd, "getreg") ? 1 : strEquCI(cmd, "setreg") ? 0 : -1);
  if (cmdread < 0) {
    jim_SetResStrf(interp, "%s: invalid command: '%s'", Jim_String(argv[0]), cmd);
    return JIM_ERR;
  }

  int bytes;
  void *regptr = strFindZ80RegPtr(Jim_String(argv[2]), &bytes);
  if (!regptr) {
    jim_SetResStrf(interp, "%s: invalid register: '%s'", Jim_String(argv[2]), cmd);
    return JIM_ERR;
  }

  if (!cmdread && argc != 4) {
    jim_SetResStrf(interp, "%s: missing value for command: '%s'", Jim_String(argv[0]), cmd);
    return JIM_ERR;
  }

  // return old value
  int val = (bytes == 2 ? *((uint16_t *)regptr) : *((uint8_t *)regptr));
  Jim_SetResultInt(interp, val);

  // set new value
  if (!cmdread) {
    int newval = jimGetIntVal(interp, argv[3]);
    if (newval == JIM_INVALID_INTVAL) {
      jim_SetResStrf(interp, "%s: invalid register value: '%s'", Jim_String(argv[0]), Jim_String(argv[3]));
      return JIM_ERR;
    }
    if ((bytes == 2 && (newval < -32768 || newval > 65535)) || (bytes == 1 && (newval < -128 || newval > 127))) {
      jim_SetResStrf(interp, "%s: register value out of bounds: '%s'", Jim_String(argv[0]), Jim_String(argv[3]));
      return JIM_ERR;
    }
    if (bytes == 2) *((uint16_t *)regptr) = newval&0xffff; else *((uint8_t *)regptr) = newval&0xff;
  }

  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
#include "jimapi_uiovl.c"


////////////////////////////////////////////////////////////////////////////////
void jimInit (void) {
  if ((jim = Jim_CreateInterp()) == NULL) { fprintf(stderr, "FATAL: can't init jim-tcl!\n"); exit(1); }
  Jim_SetGlobalVariableStr(jim, "jim_unsafe_allowed", Jim_NewIntObj(jim, Jim_GetAllowUnsafeExtensions()));
  Jim_RegisterCoreCommands(jim);
  Jim_InitStaticExtensions(jim);

  //Jim_SetVariableStrWithStr(jim, "emudir", binMyDir);
  Jim_SetGlobalVariableStr(jim, "emudir", Jim_NewStringObj(jim, binMyDir, -1));

#ifdef JIM_INTERNAL
  Jim_SetGlobalVariableStr(jim, "jim_internal", Jim_NewIntObj(jim, 1));
#else
  Jim_SetGlobalVariableStr(jim, "jim_internal", Jim_NewIntObj(jim, 0));
#endif

  // set by Tcl code when it neeeds to autocomplete a console command
  Jim_SetGlobalVariableStr(jim, "con::autocompletion", Jim_NewIntObj(jim, 0));
  // set by Tcl code when it executes a console command (via `conexec`)
  Jim_SetGlobalVariableStr(jim, "con::executing", Jim_NewIntObj(jim, 0));

  JIMAPI_REGISTER(cputs);
  JIMAPI_REGISTER(errputs);
  JIMAPI_REGISTER(load);
  JIMAPI_REGISTER(softload);
  JIMAPI_REGISTER(consetstr);

#ifdef JIM_INTERNAL
  jimEvalFile("jimstd/0init.tcl", 0);
#endif

  JIMAPI_REGISTER(vid_scale);
  JIMAPI_REGISTER(vid_rescaler);

  JIMAPI_REGISTER(quit);
  JIMAPI_REGISTER(zxbind);
  JIMAPI_REGISTER(zxunbind);
  JIMAPI_REGISTER(bind);
  JIMAPI_REGISTER(unbind);

  JIMAPI_REGISTER(reset);
  JIMAPI_REGISTER(model);

  JIMAPI_REGISTER(snow);
  JIMAPI_REGISTER(speed);
  JIMAPI_REGISTER(pause);
  JIMAPI_REGISTER(temppause);
  JIMAPI_REGISTER(timings);
  JIMAPI_REGISTER(console);
  JIMAPI_REGISTER(kbleds);
  JIMAPI_REGISTER(debugger);
  JIMAPI_REGISTER(memview);
  JIMAPI_REGISTER(sprview);
  JIMAPI_REGISTER(screenofs);
  JIMAPI_REGISTER(fullscreen);
  JIMAPI_REGISTER(noflic);
  JIMAPI_REGISTER(brightblack);
  JIMAPI_REGISTER(zxpalcolor);
  JIMAPI_REGISTER(keyhelp);
  JIMAPI_REGISTER(usesound);
  JIMAPI_REGISTER(sound);
  JIMAPI_REGISTER(filter);
  JIMAPI_REGISTER(allowother128);
  JIMAPI_REGISTER(keymatrix);
  JIMAPI_REGISTER(issue);
  JIMAPI_REGISTER(nmi);
  JIMAPI_REGISTER(grab);
  JIMAPI_REGISTER(swapbuttons);
  JIMAPI_REGISTER(maxspeedugly);
  JIMAPI_REGISTER(trdos);
  JIMAPI_REGISTER(fps);
  JIMAPI_REGISTER(kmouse);
  JIMAPI_REGISTER(kspanish);
  JIMAPI_REGISTER(kjoystick);
  JIMAPI_REGISTER(bad7ffd);
  JIMAPI_REGISTER(ay);
  JIMAPI_REGISTER(autofire);
  JIMAPI_REGISTER(genuine);
  JIMAPI_REGISTER(colormode);
  JIMAPI_REGISTER(brightborder);
  JIMAPI_REGISTER(memcontention);
  JIMAPI_REGISTER(iocontention);
  JIMAPI_REGISTER(contention);
  JIMAPI_REGISTER(useplus3);
  JIMAPI_REGISTER(genuine);

  JIMAPI_REGISTER(curblink);

  JIMAPI_REGISTER(rzx);
  JIMAPI_REGISTER(snapshot);
  JIMAPI_REGISTER(tape);
  JIMAPI_REGISTER(disk);

  JIMAPI_REGISTER(peek);
  JIMAPI_REGISTER(wpeek);
  JIMAPI_REGISTER(poke);
  JIMAPI_REGISTER(wpoke);
  JIMAPI_REGISTER(scrattrfill);

  JIMAPI_REGISTER(fldebug);
  JIMAPI_REGISTER(opense);
  JIMAPI_REGISTER(gluck);
  JIMAPI_REGISTER(quickcommander);

  JIMAPI_REGISTER(kbreset);
  JIMAPI_REGISTER(ayreset);

  JIMAPI_REGISTER(bp);
  JIMAPI_REGISTER(label);
  JIMAPI_REGISTER(reffile);

  JIMAPI_REGISTER(tsmark);
  JIMAPI_REGISTER(tsdiff);

  JIMAPI_REGISTER(z80);

  jimRegisterUIOverlayAPI();
}


void jimDeinit (void) {
  sdlClearBindings(jim, sdlJimBindings);
  sdlJimBindings = NULL;
  Jim_FreeInterp(jim);
}


////////////////////////////////////////////////////////////////////////////////
int jimEvalFile (const char *fname, int okifabsent) {
  if (fname && fname[0]) {
    char *filename;
    if (fname[0] == '/' || access(fname, R_OK) == 0) {
      filename = strdup(fname);
    } else {
      filename = strprintf("%s/tcl/%s", binMyDir, fname);
    }

    if (okifabsent) {
      if (access(filename, R_OK) != 0) {
        free(filename);
        return 0;
      }
    }

    if (jim_verbose_loading) cprintf("JIM: loading: '%s'\n", filename);
    if (Jim_EvalFile(jim, filename) == JIM_OK) {
      free(filename);
      return 0;
    } else {
      Jim_MakeErrorMessage(jim);
      cprintf("\4JIM ERROR: %s\n", Jim_GetString(Jim_GetResult(jim), NULL));
    }
    free(filename);
  }

  return -1;
}


int jimEvalFile1 (const char *fname) {
  if (fname != NULL && fname[0] && access(fname, R_OK) == 0) {
    if (jim_verbose_loading) cprintf("JIM: loading: '%s'\n", fname);
    if (Jim_EvalFile(jim, fname) == JIM_OK) return 0;
    Jim_MakeErrorMessage(jim);
    cprintf("\4JIM ERROR: %s\n", Jim_GetString(Jim_GetResult(jim), NULL));
  }
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
void emuInitBindings (void) {
  jimEvalFile("init/zxbind.tcl", 0);
  jimEvalFile("init/bind.tcl", 0);
}
