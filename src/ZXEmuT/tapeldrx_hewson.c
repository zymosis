/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
// Hewson Slowload
static const int16_t pattern_hewson[] = {
  -4, 0xD3, 0xFE, // OUT (#FE),A
  -14,
  0x14, // INC D
  0x08, // EX AF,AF'
  0x15, // DEC D
  0xF3, // DI
  0xD9, // EXX
  0, 8, 0xCD, // CALL LD-EDGE2
  0, 23, 0xCD, // CALL LD-EDGE
  0, 128,
  0x7C, // LD A,H
  0xFE, 0x01, // CP #01
  0xC9, // RET
  // check constants
  0, 28, 0x069C, // LD B,#9C
  0, 35, 0x3EC6, // LD A,#C6
  0, 43, 0x06C9, // LD B,#C9
  0, 51, 0xFE, 0xD4, // CP #D4
  0, 70, 0x06B0, // LD B,#B0
  0, 103, 0x06B2, // LD B,#B2
  0, 111, 0x3ECB, // LD A,#CB
  0, 116, 0x06B0, // LD B,#B0
  // end
  0, 0
};


/*
static int isHewson (void) {
  int addr = z80.pc;
  //if (z80.ix.w <= addr && z80.ix.w+z80.de.w >= addr) return 0; // don't accelerate self-modifying loaders
  return 1;
}
*/


////////////////////////////////////////////////////////////////////////////////
__attribute__((constructor)) static void fl_loader_ctor_hewson (void) {
  const fl_loader_info_t nfo = {
    .name="Hewson Slowload",
    .pattern=pattern_hewson,
    .detectFn=/*isHewson*/NULL,
    .accelFn=emuTapeDoROMLoad,
    .exitOfs=128,
  };
  fl_register_loader(&nfo);
}
