/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_ZXSCRDRAW_H
#define ZXEMUT_ZXSCRDRAW_H


// draw screen from `zxOldScreenTS` to `curts-1`
extern void zxRealiseScreen (int curts);

// "shaded" means "not yet touched by the beam"
// return (400*352) (out-of-array) if there is no shaded screen part
extern int zxScreenLineShadeStart ();

// returns current beam position, 4ts offset, and "in screen$" flag
// result is "in screen$"
extern int zxScrGetBeamInfo (int tstates, int *x, int *y, int *ofs);


#endif
