/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef ZXEMUT_CONSOLE_H
#define ZXEMUT_CONSOLE_H

#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>

#include "emucommon.h"
#include "emuvars.h"
#include "libvideo/video.h"


////////////////////////////////////////////////////////////////////////////////
#define MAX_CON_LINES    (512)
#define MAX_CON_HISTORY  (512)

#define CON_WIDTH     (VID_TEXT_WIDTH)
#define CON_HEIGHT    (VID_TEXT_HEIGHT-6)
#define CON_TAB_SIZE  (5)

#define CON_FG_COLOR  (12)
#define CON_BG_COLOR  (0)


////////////////////////////////////////////////////////////////////////////////
extern int conpos;
extern FILE *condumpfl;


////////////////////////////////////////////////////////////////////////////////
extern void conInit (void);
extern int conKeyEvent (SDL_KeyboardEvent *key);
extern void conDraw (void);

////////////////////////////////////////////////////////////////////////////////
extern char *strprintfVA (const char *fmt, va_list vaorig);
extern char *strprintf (const char *fmt, ...) __attribute__((format(printf,1,2)));

extern void conPutChar (char ch);
extern void cprintfVA (const char *fmt, va_list va);
extern void cprintf (const char *fmt, ...) __attribute__((format(printf,1,2)));

////////////////////////////////////////////////////////////////////////////////
extern void conExecute (const char *str, int astcl);

extern void conSetInputString (const char *str);

extern void conMessage (const char *fmt, ...) __attribute__((format(printf,1,2)));
extern void conDrawMessage (void);


#endif
