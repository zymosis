/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
static const int16_t pattern_flashload[] = {
  -4, 0xD3, 0xFE, // OUT (#FE),A
  -14,
  0x14, // INC D
  0x08, // EX AF,AF'
  0x15, // DEC D
  0xDD, 0x19, // ADD IX,DE
  0xDD, 0x2B, // DEC IX
  0xF3, // DI
  0, 6, 0xCD, // CALL LD-EDGE2
  0, 21, 0xCD, // CALL LD-EDGE
  0, 114,
  0x7C, // LD A,H
  0xFE, 0x01, // CP #01
  0xC9, // RET
  // check constants
  0, 26, 0x069C, // LD B,#9C
  0, 33, 0x3EC6, // LD A,#C6
  0, 41, 0x06C9, // LD B,#C9
  0, 49, 0xFE, 0xD4, // CP #D4
  0, 63, 0x06D0, // LD B,#D0
  0, 89, 0x06D2, // LD B,#D2
  0, 97, 0x3ED7, // LD A,#D7
  0, 102, 0x06D0, // LD B,#D0
  // end
  0, 0
};


static int doFlashLoad (void) {
  const tape_loader_info_t loader_info = {
    .pilot_minlen = 807,
    .pilot = 2168,
    .sync1 = 667,
    .sync2 = 735,
    .bit0 = 543,
    .bit1 = 839,
    .flags = CFL_REVERSE
  };
  return emuTapeDoFlashLoadEx(&loader_info);
}


////////////////////////////////////////////////////////////////////////////////
__attribute__((constructor)) static void fl_loader_ctor_flashload (void) {
  const fl_loader_info_t nfo = {
    .name="Flash Load",
    .pattern=pattern_flashload,
    .detectFn=NULL,
    .accelFn=doFlashLoad,
    .exitOfs=114,
  };
  fl_register_loader(&nfo);
}
