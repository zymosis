/***************************************************************************
 *
 *  ZXEmuT -- ZX Spectrum Emulator with Tcl scripting
 *
 *  Copyright (C) 2012-2022 Ketmar Dark <ketmar@ketmar.no-ip.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/

// return non-zero to indicate that the key was eaten
typedef int (*uiovlOnKeyCB) (UIOverlay *self, SDL_KeyboardEvent *key);
// called to render the overlay
typedef void (*uiovlOnDrawCB) (UIOverlay *self);
// called before destroying the overlay; you cannot cancel it
typedef void (*uiovlOnDestroyCB) (UIOverlay *self);

enum {
  // if this flag is set, the overlay will be destroyed by the main code
  // you cannot destroy overlay from event handlers, hence the flag
  UIOvlFlag_HIDDEN   = 1u<<0,
  UIOvlFlag_INACTIVE = 1u<<1, // inactive overlays doesn't process keys
  UIOvlFlag_MUSTDIE  = 1u<<15,
};

struct UIOverlay_t {
  VOverlay *ovl;
  VExSurface esf;
  uint32_t uid;
  char *id; // you can assign ID to the overlay
  int x0, y0; // position on the screen
  uint8_t alpha;
  unsigned flags;
  uiovlOnKeyCB keycb;
  uiovlOnDrawCB drawcb;
  uiovlOnDestroyCB diecb;
  void *udata;
  UIOverlay *prev;
  UIOverlay *next;
};

static UIOverlay *uiovlHead = NULL;
static UIOverlay *uiovlTail = NULL;
static uint32_t uiovlNextUID = 1u;


////////////////////////////////////////////////////////////////////////////////
UIOverlay *uiovlFindById (const char *id) {
  if (!id || !id[0]) return NULL;
  for (UIOverlay *ovl = uiovlTail; ovl; ovl = ovl->prev) {
    if (ovl->id && strcasecmp(ovl->id, id) == 0) return ovl;
  }
  return NULL;
}


void uiovlDraw (void) {
  UIOverlay *ovl = uiovlHead;
  while (ovl) {
    if (ovl->ovl) {
      if ((ovl->flags&(UIOvlFlag_MUSTDIE|UIOvlFlag_HIDDEN)) == 0) {
        if (ovl->drawcb) {
          vidResetPalette();
          ovl->esf.vo = ovl->ovl;
          vResetClip(&ovl->esf);
          ovl->drawcb(ovl);
        }
      }
      if ((ovl->flags&(UIOvlFlag_MUSTDIE|UIOvlFlag_HIDDEN)) == 0) {
        blitVO(ovl->ovl, ovl->x0, ovl->y0, ovl->alpha);
        vidResetPalette();
      }
    }
    // remove dead overlay
    UIOverlay *n = ovl->next;
    if (ovl->flags&UIOvlFlag_MUSTDIE) {
      if (ovl->diecb) ovl->diecb(ovl);
      if (ovl->prev) ovl->prev->next = n; else uiovlHead = n;
      if (ovl->next) ovl->next->prev = ovl->prev; else uiovlTail = ovl->prev;
      freeVO(ovl->ovl);
      if (ovl->id) free(ovl->id);
      free(ovl);
    }
    ovl = n;
  }
}


// returns !0 if the key was eaten
int uiovlKey (SDL_KeyboardEvent *key) {
  if (!key) return 0;
  for (UIOverlay *ovl = uiovlTail; ovl; ovl = ovl->prev) {
    if ((ovl->flags&(UIOvlFlag_MUSTDIE|UIOvlFlag_INACTIVE)) == 0) {
      if (ovl->keycb) {
        const int res = ovl->keycb(ovl, key);
        if (res) return res;
      }
    }
  }
  return 0;
}


UIOverlay *uiovlAllocate (int wdt, int hgt) {
  if (wdt < 1) wdt = 1; else if (wdt > 1024) wdt = 1024;
  if (hgt < 1) hgt = 1; else if (hgt > 1024) hgt = 1024;
  UIOverlay *res = malloc(sizeof(UIOverlay));
  memset(res, 0, sizeof(UIOverlay));
  res->uid = uiovlNextUID++;
  if (res->uid == 0) abort(); // just in case
  res->ovl = createVO(wdt, hgt);
  res->alpha = 255;
  res->prev = uiovlTail;
  res->next = NULL;
  if (uiovlTail) uiovlTail->next = res; else uiovlHead = res;
  uiovlTail = res;
  return res;
}


void uiovlInit (void) {
  // nothing to do here
}


void uiovlDeinit (void) {
  // kill all overlays
  while (uiovlTail) {
    UIOverlay *ovl = uiovlTail;
    uiovlTail = ovl->prev;
    ovl->next = NULL; // just in case
    if (ovl->diecb) ovl->diecb(ovl);
    freeVO(ovl->ovl);
    if (ovl->id) free(ovl->id);
    free(ovl);
  }
  uiovlHead = uiovlTail = NULL;
}


////////////////////////////////////////////////////////////////////////////////
// uioverlay xxx
typedef int (*JimUIOvlCmdEx) (UIOverlay *ovl, Jim_Interp *interp, int argc, Jim_Obj *const *argv);

#define JIMAPI_UIOVL_FN(pname_)  \
static int jim_uioverlay_##pname_ (UIOverlay *ovl, Jim_Interp *interp, int argc, Jim_Obj *const *argv)

#define JIMAPI_UIOVL_ARGC_EXACT(n_)  do { \
  if (argc != 2+(n_)) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; } \
} while (0)

#define JIMAPI_UIOVL_ARGC_BETWEEN(lo_,hi_)  do { \
  if (argc < 2+(lo_) || argc > 2+(hi_)) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; } \
} while (0)

#define JIMAPI_UIOVL_ONE_INT_ARG(vname_)  do { \
  if (argc != 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; } \
  long val_val_; \
  if (Jim_GetLong(interp, argv[2], &val_val_) != JIM_OK) { \
    jim_SetResStrf(interp, "uioverlay %s: integer expected, got '%s'", Jim_String(argv[0]), Jim_String(argv[2])); \
    return JIM_ERR; \
  } \
  vname_ = (int)val_val_; \
} while (0)


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  Jim_Interp *interp;
  Jim_Obj *onkey;
  Jim_Obj *ondraw;
  Jim_Obj *ondie;
  Jim_Obj *origlist; // so it will stay anchored
  Jim_Obj *otherdict;
  Jim_Obj *self;
} UIOverlayJimData;


static int jim_uiovl_dispatcher (Jim_Interp *interp, int argc, Jim_Obj *const *argv);


static void jimUIOvlDelProc (Jim_Interp *interp, void *udata) {
  /*
  UIOverlay *ovl = (UIOverlay *)udata;
  fprintf(stderr, "jimUIOvlDelProc: deleting command for uiovl %p (%s)\n", ovl, (ovl->id ? ovl->id : "<noid>"));
  */
}


// this creates "overlay object"
static Jim_Obj *createUIOvlCmd (Jim_Interp *interp, UIOverlay *ovl) {
  if (!ovl) return NULL;

  UIOverlayJimData *jd = (UIOverlayJimData *)ovl->udata;
  if (!jd) return NULL;
  if (!jd->self) {
    // create new command
    char namebuf[128];
    snprintf(namebuf, sizeof(namebuf), "<uioverlay:%08x>", ovl->uid);
    if (Jim_CreateCommand(interp, namebuf, &jim_uiovl_dispatcher, ovl, &jimUIOvlDelProc) != JIM_OK) {
      return NULL;
    }
    jd->self = Jim_NewStringObj(interp, namebuf, -1);
    Jim_IncrRefCount(jd->self);
    // cache it
    (void)Jim_GetCommand(interp, jd->self, JIM_NONE);
  }

  return jd->self;
}


////////////////////////////////////////////////////////////////////////////////
// ovl getval str -> obj or false
JIMAPI_UIOVL_FN(getval) {
  JIMAPI_UIOVL_ARGC_EXACT(1);
  UIOverlayJimData *jd = (UIOverlayJimData *)ovl->udata;
  Jim_Obj *res = NULL;
  if (Jim_DictKey(jd->interp, jd->otherdict, argv[2], &res, JIM_NONE) != JIM_OK) {
    //Jim_SetResultString(interp, "", -1);
    Jim_SetResultBool(interp, 0);
  } else {
    Jim_SetResult(interp, res);
  }
  return JIM_OK;
}


// ovl setval name value -> true
JIMAPI_UIOVL_FN(setval) {
  JIMAPI_UIOVL_ARGC_EXACT(2);
  UIOverlayJimData *jd = (UIOverlayJimData *)ovl->udata;
  if (Jim_DictAddElement(jd->interp, jd->otherdict, argv[2], argv[3]) != JIM_OK) {
    Jim_SetResultString(interp, "dictionary failure", -1);
    return JIM_ERR;
  } else {
    Jim_SetResultBool(interp, 1);
  }
  return JIM_OK;
}


// ovl getuid -> uint
JIMAPI_UIOVL_FN(getuid) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  Jim_SetResultInt(interp, (int)ovl->uid);
  return JIM_OK;
}


// ovl getid -> string
JIMAPI_UIOVL_FN(getid) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  Jim_SetResultString(interp, (ovl->id != NULL ? ovl->id : ""), -1);
  return JIM_OK;
}


// ovl setid name -> string
JIMAPI_UIOVL_FN(setid) {
  JIMAPI_UIOVL_ARGC_EXACT(1);
  const char *newid = Jim_String(argv[2]);
  if (ovl->id) free(ovl->id);
  ovl->id = (newid[0] ? strdup(newid) : NULL);
  Jim_SetResultString(interp, (ovl->id != NULL ? ovl->id : ""), -1);
  return JIM_OK;
}


// ovl getx -> int
JIMAPI_UIOVL_FN(getx) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  Jim_SetResultInt(interp, ovl->x0);
  return JIM_OK;
}


// ovl gety -> int
JIMAPI_UIOVL_FN(gety) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  Jim_SetResultInt(interp, ovl->y0);
  return JIM_OK;
}


// ovl getw -> int
JIMAPI_UIOVL_FN(getw) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  Jim_SetResultInt(interp, ovl->ovl->w);
  return JIM_OK;
}


// ovl geth -> int
JIMAPI_UIOVL_FN(geth) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  Jim_SetResultInt(interp, ovl->ovl->h);
  return JIM_OK;
}


// ovl setx value -> int
JIMAPI_UIOVL_FN(setx) {
  int v;
  JIMAPI_UIOVL_ONE_INT_ARG(v);
  ovl->x0 = v;
  Jim_SetResultInt(interp, v);
  return JIM_OK;
}


// ovl sety value -> int
JIMAPI_UIOVL_FN(sety) {
  int v;
  JIMAPI_UIOVL_ONE_INT_ARG(v);
  ovl->y0 = v;
  Jim_SetResultInt(interp, v);
  return JIM_OK;
}


// ovl setw value -> int
JIMAPI_UIOVL_FN(setw) {
  int v;
  JIMAPI_UIOVL_ONE_INT_ARG(v);
  if (ovl->ovl) resizeVO(ovl->ovl, v, ovl->ovl->h);
  Jim_SetResultInt(interp, v);
  return JIM_OK;
}


// ovl seth value -> int
JIMAPI_UIOVL_FN(seth) {
  int v;
  JIMAPI_UIOVL_ONE_INT_ARG(v);
  if (ovl->ovl) resizeVO(ovl->ovl, ovl->ovl->w, v);
  Jim_SetResultInt(interp, v);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
JIMAPI_UIOVL_FN(isalive) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  Jim_SetResultBool(interp, (ovl->flags&UIOvlFlag_MUSTDIE ? 0 : 1));
  return JIM_OK;
}


JIMAPI_UIOVL_FN(close) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  ovl->flags |= UIOvlFlag_MUSTDIE;
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


JIMAPI_UIOVL_FN(isvisible) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  Jim_SetResultBool(interp, (ovl->flags&(UIOvlFlag_MUSTDIE|UIOvlFlag_HIDDEN) ? 0 : 1));
  return JIM_OK;
}


JIMAPI_UIOVL_FN(hide) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  ovl->flags |= UIOvlFlag_HIDDEN|UIOvlFlag_INACTIVE;
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


JIMAPI_UIOVL_FN(show) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  ovl->flags &= ~(UIOvlFlag_HIDDEN|UIOvlFlag_INACTIVE);
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


JIMAPI_UIOVL_FN(isactive) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  Jim_SetResultBool(interp, (ovl->flags&(UIOvlFlag_MUSTDIE|UIOvlFlag_INACTIVE) ? 0 : 1));
  return JIM_OK;
}


JIMAPI_UIOVL_FN(activate) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  ovl->flags &= ~UIOvlFlag_INACTIVE;
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


JIMAPI_UIOVL_FN(deactivate) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  ovl->flags |= UIOvlFlag_INACTIVE;
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
static int juiovlGetIntArgs (int dest[], Jim_Interp *interp, int argc, Jim_Obj *const *argv, int count, int shift) {
  shift += 2;
  for (int f = 0; f < count; ++f) {
    if (f+shift < argc) {
      long lval;
      if (Jim_GetLong(interp, argv[f+shift], &lval) != JIM_OK) {
        jim_SetResStrf(interp, "uioverlay %s: integer expected, got '%s'", Jim_String(argv[0]), Jim_String(argv[f+shift]));
        return JIM_ERR;
      }
      dest[f] = (int)lval;
    }
  }
  return JIM_OK;
}


// ovl clear value -> true
JIMAPI_UIOVL_FN(clear) {
  int v;
  JIMAPI_UIOVL_ONE_INT_ARG(v);
  if (ovl->ovl) {
    clearVO(ovl->ovl, v&0xff);
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// ovl fillrect x y w h clr -> true
JIMAPI_UIOVL_FN(fillrect) {
  int iargs[5];
  JIMAPI_UIOVL_ARGC_EXACT(5);
  if (juiovlGetIntArgs(iargs, interp, argc, argv, 5, 0) != JIM_OK) return JIM_ERR;
  if (ovl->ovl) {
    fillRectVO(ovl->ovl, iargs[0], iargs[1], iargs[2], iargs[3], iargs[4]&0xff);
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// ovl rect x y w h clr -> true
JIMAPI_UIOVL_FN(rect) {
  int iargs[5];
  JIMAPI_UIOVL_ARGC_EXACT(5);
  if (juiovlGetIntArgs(iargs, interp, argc, argv, 5, 0) != JIM_OK) return JIM_ERR;
  if (ovl->ovl) {
    drawFrameVO(ovl->ovl, iargs[0], iargs[1], iargs[2], iargs[3], iargs[4]&0xff);
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// ovl putpixel x y clr -> true
JIMAPI_UIOVL_FN(putpixel) {
  int iargs[3];
  JIMAPI_UIOVL_ARGC_EXACT(3);
  if (juiovlGetIntArgs(iargs, interp, argc, argv, 3, 0) != JIM_OK) return JIM_ERR;
  if (ovl->ovl) {
    putPixelVO(ovl->ovl, iargs[0], iargs[1], iargs[2]&0xff);
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// ovl drawchar6 x y char fg [bg] -> true
JIMAPI_UIOVL_FN(drawchar6) {
  int xy[2];
  int fgbg[2];
  JIMAPI_UIOVL_ARGC_BETWEEN(4, 5);
  if (juiovlGetIntArgs(xy, interp, argc, argv, 2, 0) != JIM_OK) return JIM_ERR;
  fgbg[1] = -1;
  if (juiovlGetIntArgs(fgbg, interp, argc, argv, 2, 3) != JIM_OK) return JIM_ERR;
  const char *s = Jim_String(argv[4]);
  if (ovl->ovl && s && s[0]) {
    drawChar6VO(ovl->ovl, s[0], xy[0], xy[1], fgbg[0]&0xff, fgbg[1]&0xff);
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// ovl drawchar8 x y char fg [bg] -> true
JIMAPI_UIOVL_FN(drawchar8) {
  int xy[2];
  int fgbg[2];
  JIMAPI_UIOVL_ARGC_BETWEEN(4, 5);
  if (juiovlGetIntArgs(xy, interp, argc, argv, 2, 0) != JIM_OK) return JIM_ERR;
  fgbg[1] = -1;
  if (juiovlGetIntArgs(fgbg, interp, argc, argv, 2, 3) != JIM_OK) return JIM_ERR;
  const char *s = Jim_String(argv[4]);
  if (ovl->ovl && s && s[0]) {
    drawChar8VO(ovl->ovl, s[0], xy[0], xy[1], fgbg[0]&0xff, fgbg[1]&0xff);
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// ovl drawtext6 x y str fg [bg] -> true
JIMAPI_UIOVL_FN(drawtext6) {
  int xy[2];
  int fgbg[2];
  JIMAPI_UIOVL_ARGC_BETWEEN(4, 5);
  if (juiovlGetIntArgs(xy, interp, argc, argv, 2, 0) != JIM_OK) return JIM_ERR;
  fgbg[1] = -1;
  if (juiovlGetIntArgs(fgbg, interp, argc, argv, 2, 3) != JIM_OK) return JIM_ERR;
  const char *s = Jim_String(argv[4]);
  if (ovl->ovl && s && s[0]) {
    drawStr6VO(ovl->ovl, s, xy[0], xy[1], fgbg[0]&0xff, fgbg[1]&0xff);
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// ovl drawtext8 x y str fg [bg] -> true
JIMAPI_UIOVL_FN(drawtext8) {
  int xy[2];
  int fgbg[2];
  JIMAPI_UIOVL_ARGC_BETWEEN(4, 5);
  if (juiovlGetIntArgs(xy, interp, argc, argv, 2, 0) != JIM_OK) return JIM_ERR;
  fgbg[1] = -1;
  if (juiovlGetIntArgs(fgbg, interp, argc, argv, 2, 3) != JIM_OK) return JIM_ERR;
  const char *s = Jim_String(argv[4]);
  if (ovl->ovl && s && s[0]) {
    drawStr8VO(ovl->ovl, s, xy[0], xy[1], fgbg[0]&0xff, fgbg[1]&0xff);
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// ovl line x0 y0 x1 y1 clr
JIMAPI_UIOVL_FN(draw_line) {
  int iargs[5];
  JIMAPI_UIOVL_ARGC_EXACT(5);
  if (juiovlGetIntArgs(iargs, interp, argc, argv, 5, 0) != JIM_OK) return JIM_ERR;
  vDrawLine(&ovl->esf, iargs[0], iargs[1], iargs[2], iargs[3], iargs[4]&0xff);
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


static int jdraw_get_filled_arg (Jim_Interp *interp, int argc, Jim_Obj *const *argv, int idx) {
  idx += 2;
  if (idx < 2 || idx >= argc) return 0;
  int res = 0;
  if (Jim_GetBoolean(interp, argv[idx], &res) == JIM_OK) return res;
  const char *opt = Jim_String(argv[idx]);
       if (strcasecmp(opt, "fill") == 0) res = 1;
  else if (strcasecmp(opt, "filled") == 0) res = 1;
  return res;
}


// ovl ellipse x0 y0 x1 y1 clr [filled]
JIMAPI_UIOVL_FN(draw_ellipse) {
  int iargs[5];
  JIMAPI_UIOVL_ARGC_BETWEEN(5, 6);
  if (juiovlGetIntArgs(iargs, interp, argc, argv, 5, 0) != JIM_OK) return JIM_ERR;
  int filled = jdraw_get_filled_arg(interp, argc, argv, 5);
  if (filled) {
    vDrawFilledEllipse(&ovl->esf, iargs[0], iargs[1], iargs[2], iargs[3], iargs[4]&0xff);
  } else {
    vDrawEllipse(&ovl->esf, iargs[0], iargs[1], iargs[2], iargs[3], iargs[4]&0xff);
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// ovl circle xc yc radius clr [filled]
JIMAPI_UIOVL_FN(draw_circle) {
  int iargs[4];
  JIMAPI_UIOVL_ARGC_BETWEEN(4, 5);
  if (juiovlGetIntArgs(iargs, interp, argc, argv, 4, 0) != JIM_OK) return JIM_ERR;
  int filled = jdraw_get_filled_arg(interp, argc, argv, 4);
  if (filled) {
    vDrawFilledCircle(&ovl->esf, iargs[0], iargs[1], iargs[2], iargs[3]&0xff);
  } else {
    vDrawCircle(&ovl->esf, iargs[0], iargs[1], iargs[2], iargs[3]&0xff);
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
static int jovl_do_keycb (UIOverlay *self, SDL_KeyboardEvent *key) {
  int res = 0;
  UIOverlayJimData *jd = (UIOverlayJimData *)self->udata;
  if (jd && jd->self && jd->onkey && jd->interp) {
    // args: self char keyname emacskey pressed
    const char *kname = sdlFindKeyName(key->keysym.sym);
    const char *ekey = sdlBuildEmacsKeyName(key);
    // charstr
    char ss[2];
    if (key->keysym.unicode >= 32 && key->keysym.unicode <= 127) {
      ss[0] = (char)(key->keysym.unicode&0xff);
      ss[1] = 0;
    } else {
      ss[0] = 0;
    }
    // call handler
    Jim_Obj *lstb[6];
    lstb[0] = jd->onkey;
    lstb[1] = jd->self; // self
    lstb[2] = Jim_NewStringObj(jd->interp, ss, -1); // char
    lstb[3] = Jim_NewStringObj(jd->interp, kname, -1); // keyname
    lstb[4] = Jim_NewStringObj(jd->interp, ekey, -1); // emacskey
    lstb[5] = Jim_NewIntObj(jd->interp, (key->type == SDL_KEYDOWN ? 1 : 0));
    if (Jim_EvalObjVector(jd->interp, 6, lstb) != JIM_OK) {
      Jim_MakeErrorMessage(jd->interp);
      cprintf("\4=== JIM ERROR ===\n%s\n=================\n", Jim_GetString(Jim_GetResult(jd->interp), NULL));
    } else {
      int bval = 0;
      if (Jim_GetBoolean(jd->interp, Jim_GetResult(jd->interp), &bval) == JIM_OK) res = bval;
    }
  }
  return res;
}


static void jovl_do_drawcb (UIOverlay *self) {
  if (!self->ovl) return;
  UIOverlayJimData *jd = (UIOverlayJimData *)self->udata;
  if (jd && jd->self && jd->ondraw && jd->interp) {
    Jim_Obj *lstb[2];
    lstb[0] = jd->ondraw;
    lstb[1] = jd->self;
    if (Jim_EvalObjVector(jd->interp, 2, lstb) != JIM_OK) {
      Jim_MakeErrorMessage(jd->interp);
      cprintf("\4=== JIM ERROR ===\n%s\n=================\n", Jim_GetString(Jim_GetResult(jd->interp), NULL));
    }
  } else {
    clearVO(self->ovl, 0);
  }
}


static void jovl_do_diecb (UIOverlay *self) {
  UIOverlayJimData *jd = (UIOverlayJimData *)self->udata;
  if (jd && jd->self && jd->interp) {
    if (jd->ondie) {
      Jim_Obj *lstb[2];
      lstb[0] = jd->ondie;
      lstb[1] = jd->self;
      if (Jim_EvalObjVector(jd->interp, 2, lstb) != JIM_OK) {
        Jim_MakeErrorMessage(jd->interp);
        cprintf("\4=== JIM ERROR ===\n%s\n=================\n", Jim_GetString(Jim_GetResult(jd->interp), NULL));
      }
    }
    if (jd->onkey) Jim_DecrRefCount(jd->interp, jd->onkey);
    if (jd->ondraw) Jim_DecrRefCount(jd->interp, jd->ondraw);
    if (jd->ondie) Jim_DecrRefCount(jd->interp, jd->ondie);
    if (jd->origlist) Jim_DecrRefCount(jd->interp, jd->origlist);
    if (jd->otherdict) Jim_DecrRefCount(jd->interp, jd->otherdict);
    if (jd->self) {
      Jim_DeleteCommand(jd->interp, jd->self);
      Jim_DecrRefCount(jd->interp, jd->self);
    }
    free(self->udata);
    self->udata = NULL;
  }
}


////////////////////////////////////////////////////////////////////////////////
// uiovlobj cmd ...
JIMAPI_FN(uiovl_dispatcher) {
  UIOverlay *ovl = Jim_CmdPrivData(interp);

  if (argc < 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }

  if (strcmp(Jim_String(argv[1]), "type") == 0) {
    const char *typestr = (ovl == NULL || (ovl->flags&UIOvlFlag_MUSTDIE) != 0 ? "" : "uioverlay");
    Jim_SetResultString(interp, typestr, -1);
    return JIM_OK;
  }

  const char *const cmd0n[] = {
    "getid",
    "setid",

    "getuid",

    "getval",
    "setval",

    "getx",
    "gety",
    "getw",
    "geth",

    "setx",
    "sety",
    "setw",
    "seth",

    "isalive",
    "close",

    "isvisible",
    "show",
    "hide",

    "isactive",
    "activate",
    "deactivate",

    "clear", // clr
    "rect", // x y w h clr
    "fillrect", // x y w h clr
    "drawchar", // x y ch fg [bg]
    "drawtext", // x y str fg [bg]
    "drawchar6", // x y ch fg [bg]
    "drawtext6", // x y str fg [bg]
    "drawchar8", // x y ch fg [bg]
    "drawtext8", // x y str fg [bg]
    "putpixel", // x y clr

    "line", // x0 y0 x1 y1 clr
    "circle", // xc yc radius clr [filled]
    "ellipse", // x0 y0 x1 y1 clr [filled]
    NULL,
  };
  static JimUIOvlCmdEx cmd0h[] = {
    jim_uioverlay_getid,
    jim_uioverlay_setid,

    jim_uioverlay_getuid,

    jim_uioverlay_getval,
    jim_uioverlay_setval,

    jim_uioverlay_getx,
    jim_uioverlay_gety,
    jim_uioverlay_getw,
    jim_uioverlay_geth,

    jim_uioverlay_setx,
    jim_uioverlay_sety,
    jim_uioverlay_setw,
    jim_uioverlay_seth,

    jim_uioverlay_isalive,
    jim_uioverlay_close,

    jim_uioverlay_isvisible,
    jim_uioverlay_show,
    jim_uioverlay_hide,

    jim_uioverlay_isactive,
    jim_uioverlay_activate,
    jim_uioverlay_deactivate,

    jim_uioverlay_clear,
    jim_uioverlay_rect,
    jim_uioverlay_fillrect,

    jim_uioverlay_drawchar6,
    jim_uioverlay_drawtext6,
    jim_uioverlay_drawchar6,
    jim_uioverlay_drawtext6,
    jim_uioverlay_drawchar8,
    jim_uioverlay_drawtext8,
    jim_uioverlay_putpixel,

    jim_uioverlay_draw_line,
    jim_uioverlay_draw_circle,
    jim_uioverlay_draw_ellipse,
  };
  int cidx;
  if (Jim_GetEnum(interp, argv[1], cmd0n, &cidx, "uioverlay method", JIM_ERRMSG) == JIM_OK) {
    return (cmd0h[cidx])(ovl, interp, argc, argv);
  }

  jim_SetResStrf(interp, "uioverlay: unknown command: '%s'", Jim_String(argv[1]));
  return JIM_ERR;
}


////////////////////////////////////////////////////////////////////////////////
#define UOVL_PARSE_INT_FIELD()  do { \
  if (Jim_GetLong(interp, val, &ival) != JIM_OK) { \
    jim_SetResStrf(interp, "uioverlay %s: integer expected for a '%s'", Jim_String(argv[0]), fldname); \
    goto getlost; \
  } \
} while (0)


// uioverlay create list
JIMAPI_FN(uiovl_create) {
  JIMAPI_UIOVL_ARGC_EXACT(1);

  Jim_Obj *lsto = jimRemoveComments(interp, argv[2]);
  Jim_IncrRefCount(lsto);
  //fprintf(stderr, "=====================\n%s\n-----------------\n", Jim_String(lsto));

  Jim_Obj *newlist = NULL; // refcount will be 0
  if (Jim_SubstObj(interp, lsto, &newlist, 0) != JIM_OK) {
    Jim_MakeErrorMessage(interp);
    cprintf("\4=== JIM ERROR ===\n%s\n=================\n", Jim_GetString(Jim_GetResult(interp), NULL));
    Jim_DecrRefCount(interp, lsto);
    return JIM_ERR;
  }
  Jim_IncrRefCount(newlist);

  // collect unknown fields here
  Jim_Obj *otherdict = Jim_NewDictObj(interp, NULL, 0);
  Jim_IncrRefCount(otherdict);

  UIOverlay ovlproto;
  memset(&ovlproto, 0, sizeof(ovlproto));

  UIOverlayJimData jimdata;
  memset(&jimdata, 0, sizeof(jimdata));
  jimdata.interp = interp;
  jimdata.origlist = newlist;
  jimdata.otherdict = otherdict;

  int wdt = 100;
  int hgt = 100;

  // parse it
  long ival;
  const int llen = Jim_ListLength(interp, newlist);
  for (int f = 0; f+1 < llen; f += 2) {
    const char *fldname = Jim_String(Jim_ListGetIndex(interp, newlist, f));
    size_t fnlen = strlen(fldname);
    if (fnlen == 0 || fldname[fnlen-1] != ':') {
      jim_SetResStrf(interp, "uioverlay %s: '%s' should be a field", Jim_String(argv[0]), fldname);
getlost:
      Jim_DecrRefCount(interp, otherdict);
      Jim_FreeObj(interp, otherdict);
      Jim_DecrRefCount(interp, newlist);
      Jim_DecrRefCount(interp, lsto);
      if (ovlproto.id) free(ovlproto.id);
      return JIM_ERR;
    }
    Jim_Obj *val = Jim_ListGetIndex(interp, newlist, f+1);
    // "id:"
    if (strcasecmp(fldname, "id:") == 0) {
      ovlproto.id = strdup(Jim_String(val));
      if (!ovlproto.id[0]) { free(ovlproto.id); ovlproto.id = NULL; }
      continue;
    }
    // "x:"
    if (strcasecmp(fldname, "x:") == 0) {
      UOVL_PARSE_INT_FIELD();
      ovlproto.x0 = ival;
      continue;
    }
    // "y:"
    if (strcasecmp(fldname, "y:") == 0) {
      UOVL_PARSE_INT_FIELD();
      ovlproto.y0 = ival;
      continue;
    }
    // "width:"
    if (strcasecmp(fldname, "width:") == 0 || strcasecmp(fldname, "w:") == 0) {
      UOVL_PARSE_INT_FIELD();
      wdt = ival;
      continue;
    }
    // "height:"
    if (strcasecmp(fldname, "height:") == 0 || strcasecmp(fldname, "h:") == 0) {
      UOVL_PARSE_INT_FIELD();
      hgt = ival;
      continue;
    }
    // "alpha:"
    if (strcasecmp(fldname, "alpha:") == 0) {
      UOVL_PARSE_INT_FIELD();
      if (ival < 0) ival = 0; else if (ival > 255) ival = 255;
      ovlproto.alpha = (uint8_t)(ival&0xff);
      continue;
    }
    // "ondraw:"
    if (strcasecmp(fldname, "ondraw:") == 0) {
      jimdata.ondraw = val;
      continue;
    }
    // "onkey:"
    if (strcasecmp(fldname, "onkey:") == 0) {
      jimdata.onkey = val;
      continue;
    }
    // "ondie:"
    if (strcasecmp(fldname, "ondie:") == 0) {
      jimdata.ondie = val;
      continue;
    }
    // put to `otherdict`
    if (fldname[0] != ':' && fldname[1]) {
      Jim_Obj *fn = Jim_NewStringObj(interp, fldname, (int)strlen(fldname)-1);
      if (Jim_DictAddElement(interp, otherdict, fn, val) != JIM_OK) {
        jim_SetResStrf(interp, "uioverlay %s: '%s' cannot be put into a dictionary", Jim_String(argv[0]), fldname);
        goto getlost;
      }
    }
    //fprintf(stderr, "  FIELD: <%s> is <%s>\n", fldname, Jim_String(Jim_ListGetIndex(interp, newlist, f+1)));
  }

  // parsed, create an object
  if (jimdata.onkey) Jim_IncrRefCount(jimdata.onkey);
  if (jimdata.ondraw) Jim_IncrRefCount(jimdata.ondraw);
  if (jimdata.ondie) Jim_IncrRefCount(jimdata.ondie);

  UIOverlay *ovl = uiovlAllocate(wdt, hgt);
  ovl->id = ovlproto.id;
  ovl->x0 = ovlproto.x0;
  ovl->y0 = ovlproto.y0;
  ovl->alpha = ovlproto.alpha;

  ovl->udata = malloc(sizeof(UIOverlayJimData));
  memcpy(ovl->udata, &jimdata, sizeof(UIOverlayJimData));
  ovl->keycb = &jovl_do_keycb;
  ovl->drawcb = &jovl_do_drawcb;
  ovl->diecb = &jovl_do_diecb;

  Jim_SetResult(interp, createUIOvlCmd(interp, ovl));
  Jim_DecrRefCount(interp, lsto);
  //Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// uioverlay find id
JIMAPI_FN(uiovl_findbyid) {
  JIMAPI_UIOVL_ARGC_EXACT(1);
  const char *id = Jim_String(argv[2]);
  if (id[0]) {
    for (UIOverlay *ovl = uiovlTail; ovl; ovl = ovl->prev) {
      if (ovl->id && strcasecmp(ovl->id, id) == 0) {
        Jim_SetResult(interp, createUIOvlCmd(interp, ovl));
        return JIM_OK;
      }
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// uioverlay findbyuid uid
JIMAPI_FN(uiovl_findbyuid) {
  int uid;
  JIMAPI_UIOVL_ONE_INT_ARG(uid);
  if (uid > 0) {
    for (UIOverlay *ovl = uiovlTail; ovl; ovl = ovl->prev) {
      if (ovl->uid == (uint32_t)uid) {
        Jim_SetResult(interp, createUIOvlCmd(interp, ovl));
        return JIM_OK;
      }
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// uioverlay killall
JIMAPI_FN(uiovl_killall) {
  JIMAPI_UIOVL_ARGC_EXACT(0);
  // mark all overlays as "dead"
  for (UIOverlay *ovl = uiovlHead; ovl; ovl = ovl->next) ovl->flags |= UIOvlFlag_MUSTDIE;
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// uioverlay rgb r g b -- cidx
JIMAPI_FN(uiovl_rgb) {
  int rgb[3];
  JIMAPI_UIOVL_ARGC_EXACT(3);
  if (juiovlGetIntArgs(rgb, interp, argc, argv, 3, 0) != JIM_OK) return JIM_ERR;
  const uint8_t clr = vidAllocColor(rgb[0], rgb[1], rgb[2]);
  Jim_SetResultInt(interp, clr);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// uioverlay cmd...
JIMAPI_FN(uioverlay) {
  if (argc < 2) {
    Jim_WrongNumArgs(interp, argc, argv, "cmd ...?");
    return JIM_ERR;
  }

  const char *const cmd0n[] = {
    "rgb",
    "create",
    "find",
    "findbyuid",
    "killall",
    NULL
  };

  Jim_CmdProc *cmd0h[] = {
    jim_uiovl_rgb,
    jim_uiovl_create,
    jim_uiovl_findbyid,
    jim_uiovl_findbyuid,
    jim_uiovl_killall,
  };

  int cidx = 0;
  if (Jim_GetEnum(interp, argv[1], cmd0n, &cidx, "uiovl static method", JIM_ERRMSG) == JIM_OK) {
    return (cmd0h[cidx])(interp, argc, argv);
  }

  if (argc == 2) {
    UIOverlay *ovl = uiovlFindById(Jim_String(argv[1]));
    if (ovl != NULL) {
      Jim_SetResult(interp, createUIOvlCmd(interp, ovl));
      return JIM_OK;
    }
    Jim_SetResultBool(interp, 0);
    return JIM_OK;
  }

  jim_SetResStrf(interp, "%s: wut?!", Jim_String(argv[0]));
  return JIM_ERR;
}


////////////////////////////////////////////////////////////////////////////////
static void jimRegisterUIOverlayAPI (void) {
  JIMAPI_REGISTER(uioverlay);
}
