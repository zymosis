/* disk.c: Routines for handling disk images
   Copyright (c) 2007-2017 Gergely Szasz

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   Philip: philip-fuse@shadowmagic.org.uk

*/

//==========================================================================
//
//  open_d40_d80
//
//==========================================================================
static int open_d40_d80 (buffer_t *buffer, disk_t *d) {
  int i, j, sectors, seclen;

  if (buffavail(buffer) < 180) return (d->status = DISK_OPEN);

  /* guess geometry of disk */
  d->sides = (buff[0xb1]&0x10 ? 2 : 1);
  d->cylinders = buff[0xb2];
  sectors = buff[0xb3];

  if (d->sides < 1 || d->sides > 2 || d->cylinders > 83 || sectors > 127) {
    return (d->status = DISK_GEOM);
  }

  seclen = 512;

  buffer->index = 0;

  /* create a DD disk */
  d->density = DISK_DD;
  if (disk_alloc(d) != DISK_OK) return d->status;

  for (i = 0; i < d->cylinders; ++i) {
    for (j = 0; j < d->sides; ++j) {
      if (trackgen(d, buffer, j, i, 1, sectors, seclen,
                   NO_PREINDEX, GAP_MGT_PLUSD, NO_INTERLEAVE, NO_AUTOFILL))
      {
        return (d->status = DISK_GEOM);
      }
    }
  }

  return (d->status = DISK_OK);
}


//==========================================================================
//
//  write_d40_d80
//
//==========================================================================
static int write_d40_d80 (FILE *file, disk_t *d) {
  int i, j, sbase, sectors, seclen, mfm, cyl;

  if (check_disk_geom(d, &sbase, &sectors, &seclen, &mfm, &cyl) || sbase != 1) {
    return (d->status = DISK_GEOM);
  }

  if (cyl == -1) cyl = d->cylinders;
  if ((d->type == DISK_D40 && cyl > 43) ||
      (d->type == DISK_D80 && cyl > 83))
  {
    return d->status = DISK_GEOM;
  }

  for (i = 0; i < cyl; ++i) {
    for (j = 0; j < d->sides; ++j) {
      if (savetrack(d, file, j, i, 1, sectors, seclen)) {
        return (d->status = DISK_GEOM);
      }
    }
  }

  return (d->status = DISK_OK);
}
