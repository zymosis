/* FDC emulation code from FUSE ZX Spectrum Emulator
   adapted for ZXEmuT by Ketmar // Invisible Vector

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef LIBFUSEFDC_MAIN_HEADER
#define LIBFUSEFDC_MAIN_HEADER

// define this to include only disk i/o, without all FDC emulation code
// can be used to read and write disk images
// in this mode, the library has no ZXEmuT dependencies
//#define LIBFUSE_DISK_IO_ONLY

// define this to disable using miniz compression library
// it is used to read/write compressed UDIs (only)
//#define LIBFUSE_DISABLE_MINIZ


#include "disk.h"
#include "fdd.h"
#include "upd_fdc.h"
#include "beta.h"

#include "diskfs_trdos.h"
#include "diskfs_p3dos.h"
#include "diskfs_pisdos.h"


#endif
