/*
 * TR-DOS filesystem API
 * Copyright (c) 2020-2022 Ketmar Dark
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * coded by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 */
// TR-DOS FS operations (incomplete)
#ifndef LIBFUSEFDC_DISKFS_TRDOS_H
#define LIBFUSEFDC_DISKFS_TRDOS_H

#include <stdio.h>

#include "disk.h"

#ifdef __cplusplus
extern "C" {
#endif


#define FLP_TRDOS_TRACK_SIZE         (4096)
#define FLP_TRDOS_SECTORS_PER_TRACK  (16)
#define FLP_TRDOS_SECTOR_SIZE        (256)

// maximum number of tracks for 80-cylinder drive
#define FLP_TRDOS_MAX_TRACKS_80  (86*2)
#define FLP_TRDOS_DIRCOUNT_MAX   (128)


// disk type
typedef enum {
  FLP_DISK_TYPE_UNKNOWN = 0, // or error
  FLP_DISK_TYPE_TRDOS   = 1,
  FLP_DISK_TYPE_P3DOS   = 2,
} FloppyDiskType;


// error
// WARNING! DO NOT CHANGE! this is used in a lot of places, sometimes as magic numbers (sorry!)
enum {
  FLPERR_OK        = 0,
  FLPERR_SHIT      = -1, // general error
  FLPERR_MANYFILES = -2, // too many files in FS
  FLPERR_NOSPACE   = -3, // no room in directory/disk
  FLPERR_BADMASK   = -4, // bad file glob wildcard
  FLPERR_NOFILE    = -5, // no such file
  FLPERR_NOHEADER  = -6, // +3DOS file contains no valid header
  FLPERR_FILEEXIST = -7, // for +3DOS file creation
};


// TR-DOS directory entry (16 bytes)
typedef struct __attribute__((packed)) {
  uint8_t name[8];
  uint8_t ext;
  uint8_t lst, hst; // start address
  uint8_t llen, hlen; // length (bytes)
  uint8_t slen; // length (sectors)
  uint8_t sec; // first sector
  uint8_t trk; // first track
} TRFile;

_Static_assert(sizeof(TRFile) == 16, "invalid TR-DOS directory entry size");


// TR-DOS disk types
enum {
  TR_DT_C80_H2 = 0x16,
  TR_DT_C40_H2 = 0x17,
  TR_DT_C80_H1 = 0x18,
  TR_DT_C40_H1 = 0x19,
};


// TR-DOS directory: sectors [1..8] of track #0
// TR-DOS system sector is sector 9 of track #0
// sectors [10..16] of track #0 are unused
typedef struct __attribute__((packed)) {
  uint8_t unused[223];
  uint16_t dcu_sector_count; // sector count for DCU-formatted disk, or 0
  // first unused sector/track
  uint8_t free_sector;
  uint8_t free_track;
  uint8_t disk_type; // see TR_DT_xxx
  // number of files in directory (including deleted)
  uint8_t root_files;
  // number of free sectors
  uint16_t free_sector_count;
  // number of sectors per track, always 16
  uint8_t sectors_per_track;
  // unused
  uint16_t zero2;
  // 9 spaces (wtf?)
  uint8_t spaces[9];
  uint8_t zerot;
  // number of deleted files
  uint8_t root_del_files;
  // disk title
  uint8_t title[8];
  // and 3 more zero bytes
  uint8_t zeroend[3];
} TRSysSector;

_Static_assert(sizeof(TRSysSector) == 256, "invalid TR-DOS system sector size");


// ////////////////////////////////////////////////////////////////////////// //

// <0: error
// performs some sanity check
// will copy partial sector data (and return -3 aka FLPERR_NOSPACE)
// cannot be used to put data to more than one sector
int flpGetSectorData (disk_t *flp, uint8_t tr, uint8_t sc, void *buf, size_t len);

// returns sector size, or negative on error
int flpGetSectorDataEx (disk_t *flp, uint8_t tr, uint8_t sc, void *buf, size_t len);

const uint8_t *flpGetSectorDataPtrRO (disk_t *flp, uint8_t tr, uint8_t sc);
uint8_t *flpGetSectorDataPtrRW (disk_t *flp, uint8_t tr, uint8_t sc);


// <0: error
// performs some sanity check
// will completely reject too long data (no changes will be made, will return -3 aka FLPERR_NOSPACE)
// cannot be used to put data to more than one sector
// you can use zero `len` (`buf` doesn't matter in this case) to fix data CRC
// but it is better to call `flpFixSectorDataCRC()` instead
int flpPutSectorData (disk_t *flp, uint8_t tr, uint8_t sc, const void *buf, size_t len);

// <0: error
// call this to recalc sector data CRC if you manually modified sector data
// `flpPutSectorData` does this automatically
int flpFixSectorDataCRC (disk_t *flp, uint8_t tr, uint8_t sc);


// ////////////////////////////////////////////////////////////////////////// //
// very rought check; returns bool; survives NULLs
int flpIsValidDisk (const disk_t *dsk);

// detect disk type
FloppyDiskType flpDetectDiskType (disk_t *flp);

// return bool
int flpIsHoBetaBuf (const void *buf, int size);

// return error code
int flpLoadHoBeta (disk_t *flp, FILE *fl);

// return error code
int flpSaveHoBeta (disk_t *flp, FILE *fl, int catidx);

// is the given disk a TR-DOS disk?
int flpIsDiskTRDOS (disk_t *flp);

// format whole disk as 2x84x16x256 and init as TRDOS
// <0: error
int flpFormatTRD (disk_t *flp);

// build a single track 16x256 (TRDOS), sector data @bpos (4K)
// <0: error
//int flpFormatTRDTrack (disk_t *flp, int tr, const void *bpos, size_t bpossize);

//FLPERR_xxx
int flpCreateFileTRD (disk_t *flp, TRFile *dsc);

// this will decrement number of files if the last file is
// deleted, but won't fix the number of free sectors and such
int flpDeleteFileTRD (disk_t *flp, int num);

int flpGetDirEntryTRD (disk_t *flp, TRFile *dst, int num);

// `dst` must have room for at least `FLP_TRDOS_DIRCOUNT_MAX` items
// returns number of items (0 on error)
// `dst` can be NULL if you only want to count files
int flpGetDirectoryTRD (disk_t *flp, TRFile *dst);

int flpHasBootTRD (disk_t *flp); // return boot index in directory or -1
int flpSetBootTRD (disk_t *flp, FILE *fl, int replace);
// if the disk is TR-DOS, and it has only one basic file, and that file is
// not boot, create a very simple boot
int flpSetBootSimpleTRD (disk_t *flp); // 0: set
int flpRemoveBootTRD (disk_t *flp);

// is the given directory entry corresponds to "boot.b"?
int flpIsBootFCBTRD (const TRFile *fcb);

// `dst` can be NULL
// returns index if found, -1 if no or error
// does some simple file name sanity check
int flpFindFirstBasicTRD (disk_t *flp, TRFile *dst, int ffirst);

// if not, `dst` is undefined
// `dst` can be NULL
int flpHasAnyNonBootBasicNonBootTRD (disk_t *flp, TRFile *dst);


#ifdef __cplusplus
}
#endif

#endif
