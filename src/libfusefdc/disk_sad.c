/* disk.c: Routines for handling disk images
   Copyright (c) 2007-2017 Gergely Szasz

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   Philip: philip-fuse@shadowmagic.org.uk

*/

//==========================================================================
//
//  open_sad
//
//==========================================================================
static int open_sad (buffer_t *buffer, disk_t *d, int preindex) {
  int i, j, sectors, seclen;

  d->sides = buff[18];
  d->cylinders = buff[19];
  GEOM_CHECK;
  sectors = buff[20];
  seclen = buff[21]*64;
  buffer->index = 22;

  /* create a DD disk */
  d->density = DISK_DD;
  if (disk_alloc(d) != DISK_OK) return d->status;

  for (j = 0; j < d->sides; ++j) {
    for (i = 0; i < d->cylinders; ++i) {
      if (trackgen(d, buffer, j, i, 1, sectors, seclen, preindex,
                   GAP_MGT_PLUSD, NO_INTERLEAVE, NO_AUTOFILL))
      {
        return (d->status = DISK_GEOM);
      }
    }
  }

  return (d->status = DISK_OK);
}


//==========================================================================
//
//  write_sad
//
//==========================================================================
static int write_sad (FILE *file, disk_t *d) {
  int i, j, sbase, sectors, seclen, mfm, cyl;
  uint8_t head[256];

  if (check_disk_geom(d, &sbase, &sectors, &seclen, &mfm, &cyl) || sbase != 1) {
    return d->status = DISK_GEOM;
  }

  if (cyl == -1) cyl = d->cylinders;
  memcpy(head, "Aley's disk backup", 18);
  head[18] = d->sides;
  head[19] = cyl;
  head[20] = sectors;
  head[21] = seclen*4;
  /* SAD head */
  if (fwrite(head, 22, 1, file) != 1) return (d->status = DISK_WRPART);

  for (j = 0; j < d->sides; ++j) {
    for (i = 0; i < cyl; ++i) {
      if (savetrack(d, file, j, i, 1, sectors, seclen)) {
        return (d->status = DISK_GEOM);
      }
    }
  }

  return (d->status = DISK_OK);
}
