/***************************************************************************
 *
 *  pisDOS FS support (r/o)
 *
 *  Written by Ketmar Dark <ketmar@ketmar.no-ip.org>
 *  Used some information from HalfElf XiSD FAR plugin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#ifndef PISDOS_FS_SUPPORT_H
#define PISDOS_FS_SUPPORT_H

#include "diskfs_p3dos.h"


// ////////////////////////////////////////////////////////////////////////// //
typedef struct __attribute__((packed)) {
  uint16_t load_addr;
  uint8_t  size[3];
  uint16_t first_block;
  uint8_t  sys_flag;
  uint8_t  reserved[6];
  uint16_t crc;
  uint16_t time;
} PisDOS_DEFile;


typedef struct __attribute__((packed)) {
  uint16_t parent_dir_block;
  uint16_t size;
  uint8_t  level;
  uint16_t frag_block;
  uint16_t first_block;
  uint8_t  files_total;
  uint8_t  files_count;
  uint8_t  level_chic;
  uint8_t  reserved[6];
} PisDOS_DEDir;


typedef struct __attribute__((packed)) {
  uint8_t name[8];
  uint8_t ext[3];
  uint8_t attr;
  union __attribute__((packed)) {
    PisDOS_DEFile file;
    PisDOS_DEDir dir;
  };
  uint16_t date;
} PisDOS_DirEnt;


typedef struct __attribute__((packed)) {
  uint8_t reserved1[2];
  uint8_t title[8];
  uint8_t signature[3];
  uint8_t sign_chic[3];
  uint8_t reserved2[2];
  uint16_t blocks_total;
  uint16_t rootdir_block;
  uint8_t tracks_total;
  uint8_t type;
  uint8_t sector_size;
  uint8_t sectors_per_track;
  uint8_t reserved3[4];
  uint16_t date;
  PisDOS_DEFile isd_sys;
  uint8_t sector_table[0x10]; // dunno
} PisDOS_DPB;


typedef struct {
  uint8_t day;
  uint8_t month;
  uint16_t year;
  uint8_t hour;
  uint8_t minute;
  uint8_t second;
} PisDOSDateTime;


// doesn't modify time part
void pisdos_unpack_date (PisDOSDateTime *dt, uint16_t v);

// doesn't modify date part
void pisdos_unpack_time (PisDOSDateTime *dt, uint16_t v);


#define PISDOS_BLOCK_SIZE  (256)

#define PISDOS_FLAG_USED        (0x01)
#define PISDOS_FLAG_NO_READ     (0x04)
#define PISDOS_FLAG_NO_WRITE    (0x08)
#define PISDOS_FLAG_HIDDEN      (0x10)
#define PISDOS_FLAG_DIR         (0x20)
#define PISDOS_FLAG_CONTINUOUS  (0x40)
#define PISDOS_FLAG_NO_DELETE   (0x80)


// dummy DE for root dir
extern const PisDOS_DirEnt pisdos_deroot;


static __attribute__((unused)) inline int pisdos_is_dir (const PisDOS_DirEnt *h) {
  return (h->attr != 0xFF && h->file.sys_flag != 0xFF && (h->attr&PISDOS_FLAG_DIR));
}

static __attribute__((unused)) inline uint32_t pisdos_get_entry_size (const PisDOS_DirEnt *h) {
  return
    pisdos_is_dir(h) ? h->dir.size :
    h->file.size[0]|(h->file.size[1]<<8)|(h->file.size[2]<<16);
}


void pisdos_get_title (char fname[14], const PisDOS_DPB *dpb);

void pisdos_get_name (char fname[14], const PisDOS_DirEnt *de);
void pisdos_set_name (PisDOS_DirEnt *de, const char fname[14]);

// return NULL if disk doesn't look like a good pisdos disk
const PisDOS_DPB *pisdos_get_dpb (disk_t *dsk);

// return pointer to the given disk block data, or NULL
const void *pisdos_block_ptr (disk_t *dsk, int blknum);


// ////////////////////////////////////////////////////////////////////////// //
// file reading API

typedef struct __attribute__((packed)) {
  uint16_t start_block; // of this fragment data
  uint8_t block_count; // number of blocks in this fragment
} PisDOSFragInfo;


typedef struct {
  uint8_t frag_count; // number of fragments; if 1, the file is continuous
  PisDOSFragInfo frags[256]; // starting blocks of each fragment data
  // for continuous files (files with one fragment are converted to this too)
  uint16_t start_block; // of this fragment data
  uint16_t block_count; // number of blocks in this fragment
  // this is for file reading
  uint8_t curr_frag; // current active fragment (from 0)
  uint16_t curr_block_idx; // from the start of the current fragment
} PisDOSFragList;


// to read pisdos file, find it, build frags, and keep calling
// `pisdos_next_block()` until you read enough 256-byte blocks


// build list of fragments for the given file
// return 0 on success, negative number on error
int pisdos_build_frags (disk_t *dsk, const PisDOS_DirEnt *de, PisDOSFragList *list);

// return pointer to the next file block
// if called right after `pisdos_build_frags`, return pointer to the first block
// return NULL on error or EOF (nobody cares what exactly happened anyway)
const void *pisdos_next_block (disk_t *dsk, PisDOSFragList *list);


// iterator callback for the next function
// return non-zero to stop
typedef int (*pisdor_dir_iterator_cb) (const PisDOS_DirEnt *de, void *udata);

// return iterator callback result or 0
// return negative number on error
int pisdos_dir_foreach (disk_t *dsk, const PisDOS_DirEnt *dirde,
                        pisdor_dir_iterator_cb cb, void *udata);


// return non-zero if the given path is a path to the root directory
// note that empty or NULL string considered as "root path"
int pisdos_is_root_path (const char *path);

// walk down subdirs starting from `dirde` using `path`
// ignore last path entry (filename)
// return directory entry to look for the last path entry
// 'cmon, do you really care about all this shit?
const PisDOS_DirEnt *pisdos_walk_path (disk_t *dsk, const PisDOS_DirEnt *dirde,
                                       const char *path);


// ////////////////////////////////////////////////////////////////////////// //
typedef struct {
  disk_t *dsk;
  char mask[32];
  const PisDOS_DirEnt *dirde;
  const PisDOS_DirEnt *delist[256];
  uint32_t de_count;
  uint32_t de_current;
} PisDOSFindInfo;


// exactly what you may thing
// supports wildcards
const PisDOS_DirEnt *pisdos_findnext (PisDOSFindInfo *fi);
const PisDOS_DirEnt *pisdos_findfirst (PisDOSFindInfo *fi, disk_t *dsk, const char *fname);


// ////////////////////////////////////////////////////////////////////////// //
// debug
void pisdos_debug_dump_dpb (disk_t *dsk);


#endif
