/* beta.c: Routines for handling the Beta disk interface
   Copyright (c) 2004-2016 Stuart Brady, Philip Kendall, Gergely Szasz

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   Philip: philip-fuse@shadowmagic.org.uk

   Stuart: stuart.brady@gmail.com

*/
#ifndef LIBFUSE_DISK_IO_ONLY

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <limits.h>
#include <sys/stat.h>

#include "beta.h"
#include "wd_fdc.h"


#define ARRAY_SIZE(a) ((sizeof(a)/sizeof(*a)))

static uint8_t beta_system_register = 0; /* FDC system register */

static wd_fdc *beta_fdc = NULL;
static fdd_t beta_drives[BETA_NUM_DRIVES];

/*
static const periph_port_t beta_ports[] = {
  { 0x00ff, 0x001f, beta_sr_read, beta_cr_write },
  { 0x00ff, 0x003f, beta_tr_read, beta_tr_write },
  { 0x00ff, 0x005f, beta_sec_read, beta_sec_write },
  { 0x00ff, 0x007f, beta_dr_read, beta_dr_write },
  { 0x00ff, 0x00ff, beta_sp_read, beta_sp_write },
  { 0, 0, NULL, NULL }
};
*/


//==========================================================================
//
//  beta_select_drive
//
//==========================================================================
static void beta_select_drive (int i) {
  if (beta_fdc->current_drive != &beta_drives[i&0x03]) {
    if (beta_fdc->current_drive != NULL) {
      fdd_select(beta_fdc->current_drive, 0);
    }
    beta_fdc->current_drive = &beta_drives[i&0x03];
    fdd_select(beta_fdc->current_drive, 1);
  }
}


//==========================================================================
//
//  beta_init
//
//==========================================================================
void beta_init (disk_t *disks[4]) {
  beta_fdc = wd_fdc_alloc_fdc(FD1793, 0, WD_FLAG_BETA128);
  beta_fdc->current_drive = NULL;

  for (int i = 0; i < BETA_NUM_DRIVES; ++i) {
    fdd_t *d = &beta_drives[i];
    d->disk = disks[i];
    fdd_init(d, FDD_SHUGART, NULL, 0); /* drive geometry 'autodetect' */
    d->disk->flag = DISK_FLAG_NONE;
  }
  beta_select_drive(0);

  beta_fdc->dden = 1;
  beta_fdc->set_intrq = NULL;
  beta_fdc->reset_intrq = NULL;
  beta_fdc->set_datarq = NULL;
  beta_fdc->reset_datarq = NULL;

  beta_reset();
}


//==========================================================================
//
//  beta_reset
//
//==========================================================================
void beta_reset (void) {
  wd_fdc_master_reset(beta_fdc);
  beta_select_drive(0);
  for (int i = 0; i < BETA_NUM_DRIVES; ++i) {
    fdd_t *fdd = &beta_drives[i];
    fdd_unload(fdd);
    if (disk_is_valid(fdd->disk)) fdd_load(fdd);
  }
}


//==========================================================================
//
//  beta_end
//
//==========================================================================
void beta_end (void) {
  free(beta_fdc);
}


//==========================================================================
//
//  beta_sr_read
//
//==========================================================================
uint8_t beta_sr_read (uint16_t port) {
  return wd_fdc_sr_read(beta_fdc);
}


//==========================================================================
//
//  beta_cr_write
//
//==========================================================================
void beta_cr_write (uint16_t port, uint8_t b) {
  wd_fdc_cr_write(beta_fdc, b);
}


//==========================================================================
//
//  beta_tr_read
//
//==========================================================================
uint8_t beta_tr_read (uint16_t port) {
  return wd_fdc_tr_read(beta_fdc);
}


//==========================================================================
//
//  beta_tr_write
//
//==========================================================================
void beta_tr_write (uint16_t port, uint8_t b) {
  wd_fdc_tr_write(beta_fdc, b);
}


//==========================================================================
//
//  beta_sec_read
//
//==========================================================================
uint8_t beta_sec_read (uint16_t port) {
  return wd_fdc_sec_read(beta_fdc);
}


//==========================================================================
//
//  beta_sec_write
//
//==========================================================================
void beta_sec_write (uint16_t port, uint8_t b) {
  wd_fdc_sec_write(beta_fdc, b);
}


//==========================================================================
//
//  beta_dr_read
//
//==========================================================================
uint8_t beta_dr_read (uint16_t port) {
  return wd_fdc_dr_read(beta_fdc);
}


//==========================================================================
//
//  beta_dr_write
//
//==========================================================================
void beta_dr_write (uint16_t port, uint8_t b) {
  wd_fdc_dr_write(beta_fdc, b);
}


//==========================================================================
//
//  beta_sp_write
//
//==========================================================================
void beta_sp_write (uint16_t port, uint8_t b) {
  /* reset 0x04 and then set it to reset controller */
  beta_select_drive(b&0x03);
  /* 0x08 = block hlt, normally set */
  wd_fdc_set_hlt(beta_fdc, (b&0x08 ? 1 : 0));
  fdd_set_head(beta_fdc->current_drive, (b&0x10 ? 0 : 1));
  /* 0x20 = density, reset = FM, set = MFM */
  beta_fdc->dden = (b&0x20 ? 1 : 0);
  beta_system_register = b;
}


//==========================================================================
//
//  beta_sp_read
//
//==========================================================================
uint8_t beta_sp_read (uint16_t port) {
  uint8_t b = 0;
  if (beta_fdc->intrq) b |= 0x80;
  if (beta_fdc->datarq) b |= 0x40;
  /* we should reset beta_datarq, but we first need to raise it for each byte
   * transferred in wd_fdc.c */
  return b;
}


//==========================================================================
//
//  beta_disk_eject
//
//==========================================================================
void beta_disk_eject (beta_drive_number which) {
  fdd_t *fdd = beta_get_fdd(which);
  if (fdd) fdd_unload(fdd);
}


//==========================================================================
//
//  beta_get_fdd
//
//==========================================================================
fdd_t *beta_get_fdd (beta_drive_number which) {
  return (which >= 0 && which < BETA_NUM_DRIVES ? &beta_drives[which] : NULL);
}


//==========================================================================
//
//  beta_get_disk
//
//==========================================================================
disk_t *beta_get_disk (beta_drive_number which) {
  return (which >= 0 && which < BETA_NUM_DRIVES ? beta_drives[which].disk : NULL);
}
#endif  /* LIBFUSE_DISK_IO_ONLY */
