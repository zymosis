/* disk.h: Routines for handling disk images
   Copyright (c) 2007-2015 Gergely Szasz

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   Philip: philip-fuse@shadowmagic.org.uk

*/
#ifndef FUSE_DISK_H
#define FUSE_DISK_H

#include <stddef.h>
#include <stdint.h>


enum {
  LIBFDC_MSG_DEBUG = 0,
  LIBFDC_MSG_WARNING = 1,
  LIBFDC_MSG_ERROR = 2,
};

// `type` is `LIBFDC_MSG_xxx`
// the buffer is static, and will be reused by the library!
// *NOT* terminated with '\n'!
extern void (*libfdcMessageCB) (int type, const char *msg);

void libfdcMsg (int type, const char *fmt, ...) __attribute__((format(printf,2,3)));


static const unsigned int DISK_FLAG_NONE = 0x00;
static const unsigned int DISK_FLAG_PLUS3_CPC = 0x01; /* try to fix some CPC issue */
//static const unsigned int DISK_FLAG_OPEN_DS = 0x02; /* try to open the other side too */


typedef enum disk_error_t {
  DISK_OK = 0,
  DISK_BADARGS,
  DISK_IMPL,
  DISK_MEM,
  DISK_GEOM,
  DISK_OPEN,
  DISK_UNSUP,
  DISK_RDONLY,
  DISK_CLOSE,
  DISK_WRFILE,
  DISK_WRPART,

  // for sector reading API
  DISK_INVALID_HEAD,
  DISK_NO_CYLINDER,
  DISK_NO_SECTOR,
  DISK_BAD_SECTOR,
  DISK_SECTOR_TOO_BIG,
  DISK_WRITE_ERROR,

  DISK_LAST_ERROR,
} disk_error_t;


typedef enum disk_type_t {
  DISK_TYPE_NONE = 0,
  DISK_UDI, /* raw track disk image (our format :-) */
  DISK_FDI, /* Full Disk Image ALT */
  DISK_TD0,

  /* DISCiPLE / +D / SAM Coupe */
  DISK_MGT, /* ALT */
  DISK_IMG, /* OUT-OUT */
  DISK_SAD, /* OUT-OUT with geometry header */

  DISK_CPC,
  DISK_ECPC,

  /* Beta disk interface (TR-DOS) */
  DISK_TRD,
  DISK_SCL,

  /* Opus Discovery */
  DISK_OPD,

  /* Didaktik 40/80 */
  DISK_D40,
  DISK_D80,

  /* Log disk structure (.log) */
  DISK_LOG,

  DISK_TYPE_LAST,
} disk_type_t;


typedef enum disk_dens_t {
  DISK_DENS_AUTO = 0,
  DISK_8_SD,    /* 8" SD floppy 5208 MF */
  DISK_8_DD,    /* 8" DD floppy 10416 */
  DISK_SD,      /* 3125 bpt MF */
  DISK_DD,      /* 6250 bpt */
  DISK_DD_PLUS, /* 6500 bpt e.g. Coin Op Hits */
  DISK_HD,      /* 12500 bpt*/
} disk_dens_t;


// you need to save and restore disk position when manipulating disk data
// this is because FDC is using the position too, and if you will break it,
// disk i/o operations will read/write invalid data
typedef struct {
  uint8_t *track;   /* current track data bytes */
  uint8_t *clocks;  /* clock marks bits */
  uint8_t *fm;      /* FM/MFM marks bits */
  uint8_t *weak;    /* weak marks bits/weak data */
  int i;            /* index for track and clocks */
} disk_position_t;


typedef struct disk_t {
  char *filename;       /* original filename */
  int sides;            /* 1 or 2 */
  int cylinders;        /* tracks per side  */
  int bpt;              /* bytes per track */
  int wrprot;           /* disk write protect */
  int dirty;            /* disk changed */
  int have_weak;        /* disk contain weak sectors (used in uPD765 emulation) */
  unsigned int flag;
  disk_error_t status;  /* last error code */
  uint8_t *data;        /* disk data */
  /* private part */
  int tlen;             /* length of a track with clock and other marks (bpt + 3/8bpt) */
  disk_position_t pos;  /* current disk position */
  disk_type_t type;     /* DISK_UDI, ... */
  disk_dens_t density;  /* DISK_SD DISK_DD, or DISK_HD */
} disk_t;


/* every track data:
TRACK_LEN TYPE TRACK......DATA CLOCK..MARKS MF..MARKS WEAK..MARKS
               ^               ^            ^         ^
               |__ track       |__ clocks   |__ mf    |__ weak
  so, track[-1] = TYPE
  TLEN = track[-3]+256*track[-2]
  TYPE is Track type as in UDI spec (0x00, 0x01, 0x02, 0x80, 0x81, 0x82) after update_tracks_mode() !!!


tracks:
                                                  ..S...
  4E  4E .... 4E  00  00 .... 00  C2  C2  C2  FC  ..E...  4E  4E .... 4E
 ................................................ ..C... ................
      GAP4A        Synchrogap          IAM        ..T...       GAP4B
      (80)            (12)                        ..O...  (until the end of the track)
                                                  ..R...
                                                  ..S...


one sector:
                                                                                                                      F8
  4E  4E .... 4E  00  00 .... 00  A1  A1  A1  FE  cc  hh  rr  nn  k1  k2  4E  4E .... 4E  00  00 .... 00  A1  A1  A1  FB  xx  xx .... xx  k1  k2  4E  4E .... 4E
 ................................................................................................................................................................
       GAP1        Synchrogap          IDAM         sector ID       CRC         GAP2       Synchrogap      DATA AM         sector           CRC         GAP3
       (50)           (12)                           (header)    (of header)    (22)         (12)                           data         (of data)     (misc)

Bytes 0xA1 � 0xC2 in IAM, IDAM and DATA AM are marker bytes, and marked in `clocks`.
Bytes 0xA1 � 0xC2 in data array are not marker bytes.

IAM -- track index address mark
IDAM -- data index address mark
DATA AM -- data address mark
Synchrogap -- zero bytes, used by FDC to adjust various timings and such

GAPn can contain bytes with other values; it is sometimes used in some copy protection schemes

header CRC is for IDAM and ID; big-endian

sector size byte (nn): [0..7]; size in bytes is (0x80<<nn)
actually, sectors bigger than 1024 bytes (nn > 3) are invalid

data CRC is for DATA AM and sector data

DATA AM last byte:
  0xf8: deleted data
  0xfb: normal data
*/

#define DISK_CLEN(bpt)  ((bpt)/8+((bpt)%8 ? 1 : 0))

#define DISK_SET_TRACK_IDX(d,idx)  do { \
  d->pos.track  = d->data+3+(idx)*d->tlen; \
  d->pos.clocks = d->pos.track+d->bpt; \
  d->pos.fm     = d->pos.clocks+DISK_CLEN(d->bpt); \
  d->pos.weak   = d->pos.fm+DISK_CLEN(d->bpt); \
} while (0)

#define DISK_SET_TRACK(d,head,cyl)  do { \
  DISK_SET_TRACK_IDX((d), (d)->sides*cyl+head); \
} while (0)


#define DISK_SET_TRACK_IDX_POS(d,idx,dp)  do { \
  dp.track  = d->data+3+(idx)*d->tlen; \
  dp.clocks = dp.track+d->bpt; \
  dp.fm     = dp.clocks+DISK_CLEN(d->bpt); \
  dp.weak   = dp.fm+DISK_CLEN(d->bpt); \
} while (0)

#define DISK_SET_TRACK_POS(d,head,cyl,dp)  do { \
  DISK_SET_TRACK_IDX_POS((d), (d)->sides*cyl+head, dp); \
} while (0)


static inline __attribute__((unused))
int disk_is_valid (const disk_t *dsk) {
  return
    dsk && dsk->data &&
    (dsk->sides == 1 || dsk->sides == 2) &&
    (dsk->cylinders >= 40 && dsk->cylinders <= 83) &&
    dsk->bpt >= 1024 /*&&
    dsk->density*/;
}


/* insert simple TR-DOS autoloader? */
extern int opt_beta_autoload; /* default is 0 */


const char *disk_strerror (int error);

/* create an unformatted disk sides -> (1/2) cylinders -> track/side,
   dens -> 'density' related to unformatted length of a track (SD = 3125,
   DD = 6250, HD = 12500, type -> if write this disk we want to convert
   into this type of image file

   automatically calls `disk_close()`
*/
int disk_new (disk_t *d, int sides, int cylinders, disk_dens_t dens, disk_type_t type);

/* detects some common disk image formats */
disk_type_t disk_detect_format (const void *data, int size);

/* call this to initialize disk image struct to sane values */
void disk_init_struct (disk_t *d);

/* open a disk image file. if preindex = 1 and the image file not UDI then
   pre-index gap generated with index mark (0xfc)
   this time only .mgt(.dsk)/.img/.udi and CPC/extended CPC file format
   supported
*/
int disk_open (disk_t *d, const char *filename);

int disk_open_ex (disk_t *d, const char *filename, int preindex);

// returns error code
int disk_open_buf_udi (disk_t *d, const void *data, int size);
int disk_open_buf_fdi (disk_t *d, const void *data, int size);
int disk_open_buf_trd (disk_t *d, const void *data, int size);
int disk_open_buf_scl (disk_t *d, const void *data, int size);
int disk_open_buf_td0 (disk_t *d, const void *data, int size);
int disk_open_buf_dsk (disk_t *d, const void *data, int size);


/* merge two one sided disk (d1, d2) to a two sided one (d),
   after merge closes d1 and d2
*/
int disk_merge_sides (disk_t *d, disk_t *d1, disk_t *d2, int autofill);

/* write a disk image file (from the disk buffer). the d->type
   gives the format of file. if it DISK_TYPE_AUTO, disk_write
   try to guess from the file name (extension). if fail save as
   UDI.
*/
int disk_write (disk_t *d, const char *filename);

/* format disk to plus3 accept for formatting
*/
int disk_preformat (disk_t *d);

/* close a disk and free buffers
*/
void disk_close (disk_t *d);


// check if the given sector exists
// tracks are: head0/cyl0; head1/cyl0; head0/cyl1, ...
int disk_is_sector_exist (disk_t *d, int track, int sector);

// read disk sector
// returns error code (DISK_xxx)
// automatically saves and restores disk position
// doesn't change `d->status`
// tracks are: head0/cyl0; head1/cyl0; head0/cyl1, ...
// on `DISK_SECTOR_TOO_BIG`, buffer is not modified, and `sectorsize` contains sector size
int disk_read_sector (disk_t *d, int track, int sector,
                      void *buf, size_t bufsize, int *sectorsize);

// return pointer to sector data or NULL
// tracks are: head0/cyl0; head1/cyl0; head0/cyl1, ...
// both `sectorsize` and `error` can be NULL
void *disk_get_sector_data_ptr (disk_t *d, int track, int sector,
                                int *sectorsize, int *error);

// write disk sector
// returns error code (DISK_xxx)
// automatically saves and restores disk position
// doesn't change `d->status`
// pass `NULL` buffer to fix sector CRC (bufsize doesn't matter in this case)
// if `buf` is not `NULL`, and `bufsize` is lesser than sector size,
// missing data will not be modified
// tracks are: head0/cyl0; head1/cyl0; head0/cyl1, ...
int disk_write_sector (disk_t *d, int track, int sector, const void *buf, size_t bufsize);

// frees old disk data, creates new, sets `d->status`
// the disk is filled with zeroes
// `secsize` in bytes
int disk_format (disk_t *d, int heads, int cylinders, int firstsector, int sectors,
                 int secsize, int interleave);


typedef struct {
  uint8_t track;
  uint8_t head;
  uint8_t sector;
  uint8_t size;
  uint8_t mark; // [0xf8..0xfe], or 0 if not found
} disk_sector_header;

// returns number of found sectors
// max 256 sectors
int disk_track_map_simple (disk_t *d, int track, disk_sector_header *seclist);


#endif
