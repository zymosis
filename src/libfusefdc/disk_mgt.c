/* disk.c: Routines for handling disk images
   Copyright (c) 2007-2017 Gergely Szasz

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   Philip: philip-fuse@shadowmagic.org.uk

*/

//==========================================================================
//
//  open_img_mgt_opd
//
//==========================================================================
static int open_img_mgt_opd (buffer_t *buffer, disk_t *d) {
  int i, j, sectors, seclen;

  buffer->index = 0;

  /* guess geometry of disk:
   * 2*80*10*512, 1*80*10*512, 1*40*10*512, 1*40*18*256, 1*80*18*256,
   * 2*80*18*256
   */
  if (buffer->file.length == 2*80*10*512) {
    d->sides = 2;
    d->cylinders = 80;
    sectors = 10;
    seclen = 512;
  } else if (buffer->file.length == 1*80*10*512) {
    /* we cannot distinguish between a single sided 80 track image
     * and a double sided 40 track image (2*40*10*512) */
    d->sides = 1;
    d->cylinders = 80;
    sectors = 10;
    seclen = 512;
  } else if (buffer->file.length == 1*40*10*512) {
    d->sides = 1;
    d->cylinders = 40;
    sectors = 10;
    seclen = 512;
  } else if (buffer->file.length == 1*40*18*256) {
    d->sides = 1;
    d->cylinders = 40;
    sectors = 18;
    seclen = 256;
  } else if (buffer->file.length == 1*80*18*256) {
    /* we cannot distinguish between a single sided 80 track image
     * and a double sided 40 track image (2*40*18*256) */
    d->sides = 1;
    d->cylinders = 80;
    sectors = 18;
    seclen = 256;
  } else if (buffer->file.length == 2*80*18*256) {
    d->sides = 2;
    d->cylinders = 80;
    sectors = 18;
    seclen = 256;
  } else {
    return d->status = DISK_GEOM;
  }

  /* create a DD disk */
  d->density = DISK_DD;
  if (disk_alloc(d) != DISK_OK) return d->status;

  if (d->type == DISK_IMG) {
    /* IMG out-out */
    for (j = 0; j < d->sides; ++j) {
      for (i = 0; i < d->cylinders; ++i) {
        if (trackgen(d, buffer, j, i, 1, sectors, seclen,
                     NO_PREINDEX, GAP_MGT_PLUSD, NO_INTERLEAVE, NO_AUTOFILL))
        {
          return (d->status = DISK_GEOM);
        }
      }
    }
  } else {
    /* MGT / OPD alt */
    for (i = 0; i < d->cylinders; ++i) {
      for (j = 0; j < d->sides; ++j) {
        if (trackgen(d, buffer, j, i, (d->type == DISK_MGT ? 1 : 0), sectors, seclen,
                     NO_PREINDEX, GAP_MGT_PLUSD,
                     (d->type == DISK_MGT ? NO_INTERLEAVE : INTERLEAVE_OPUS), NO_AUTOFILL))
        {
          return (d->status = DISK_GEOM);
        }
      }
    }
  }

  return (d->status = DISK_OK);
}


//==========================================================================
//
//  write_img_mgt_opd
//
//==========================================================================
static int write_img_mgt_opd (FILE *file, disk_t *d) {
  int i, j, sbase, sectors, seclen, mfm, cyl;

  if (check_disk_geom(d, &sbase, &sectors, &seclen, &mfm, &cyl) ||
      (d->type != DISK_OPD && (sbase != 1 || seclen != 2 || sectors != 10)) ||
      (d->type == DISK_OPD && (sbase != 0 || seclen != 1 || sectors != 18)))
  {
    return (d->status = DISK_GEOM);
  }

  if (cyl == -1) cyl = d->cylinders;
  if (cyl != 40 && cyl != 80) return (d->status = DISK_GEOM);

  if (d->type == DISK_IMG) {
    /* out-out */
    for (j = 0; j < d->sides; ++j) {
      for (i = 0; i < cyl; ++i) {
        if (savetrack(d, file, j, i, 1, sectors, seclen)) {
          return (d->status = DISK_GEOM);
        }
      }
    }
  } else {
    /* MGT */
    for (i = 0; i < cyl; ++i) {
      for (j = 0; j < d->sides; ++j) {
        if (savetrack(d, file, j, i, (d->type == DISK_MGT ? 1 : 0), sectors, seclen)) {
          return (d->status = DISK_GEOM);
        }
      }
    }
  }

  return (d->status = DISK_OK);
}
