/***************************************************************************
 *
 *  pisDOS disk file extractor
 *
 *  Written by Ketmar Dark <ketmar@ketmar.no-ip.org>
 *  Used some information from HalfElf XiSD FAR plugin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License ONLY.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **************************************************************************/
#include "diskfs_pisdos.h"

#include <string.h>


// "device.sys" in the root dir is a free blocks bitmap
// bit number is "block&7"; 0 means "free"


static uint8_t koi8tolowerTable[256] = {
  0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
  0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,
  0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,
  0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,
  0x40,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,
  0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x5b,0x5c,0x5d,0x5e,0x5f,
  0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,
  0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x7b,0x7c,0x7d,0x7e,0x7f,
  0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,
  0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9a,0x9b,0x9c,0x9d,0x9e,0x9f,
  0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,
  0xb0,0xb1,0xb2,0xa3,0xa4,0xb5,0xa6,0xa7,0xb8,0xb9,0xba,0xbb,0xbc,0xad,0xbe,0xbf,
  0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,
  0xd0,0xd1,0xd2,0xd3,0xd4,0xd5,0xd6,0xd7,0xd8,0xd9,0xda,0xdb,0xdc,0xdd,0xde,0xdf,
  0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,
  0xd0,0xd1,0xd2,0xd3,0xd4,0xd5,0xd6,0xd7,0xd8,0xd9,0xda,0xdb,0xdc,0xdd,0xde,0xdf,
};


static uint16_t charMap866[128] = {
  0x0410,0x0411,0x0412,0x0413,0x0414,0x0415,0x0416,0x0417,0x0418,0x0419,0x041A,0x041B,0x041C,0x041D,0x041E,0x041F,
  0x0420,0x0421,0x0422,0x0423,0x0424,0x0425,0x0426,0x0427,0x0428,0x0429,0x042A,0x042B,0x042C,0x042D,0x042E,0x042F,
  0x0430,0x0431,0x0432,0x0433,0x0434,0x0435,0x0436,0x0437,0x0438,0x0439,0x043A,0x043B,0x043C,0x043D,0x043E,0x043F,
  0x2591,0x2592,0x2593,0x2502,0x2524,0x2561,0x2562,0x2556,0x2555,0x2563,0x2551,0x2557,0x255D,0x255C,0x255B,0x2510,
  0x2514,0x2534,0x252C,0x251C,0x2500,0x253C,0x255E,0x255F,0x255A,0x2554,0x2569,0x2566,0x2560,0x2550,0x256C,0x2567,
  0x2568,0x2564,0x2565,0x2559,0x2558,0x2552,0x2553,0x256B,0x256A,0x2518,0x250C,0x2588,0x2584,0x258C,0x2590,0x2580,
  0x0440,0x0441,0x0442,0x0443,0x0444,0x0445,0x0446,0x0447,0x0448,0x0449,0x044A,0x044B,0x044C,0x044D,0x044E,0x044F,
  0x0401,0x0451,0x0404,0x0454,0x0407,0x0457,0x040E,0x045E,0x00B0,0x2219,0x00B7,0x221A,0x2116,0x00A4,0x25A0,0x00A0,
};

static uint16_t charMapKOI8[128] = {
  0x2500,0x2502,0x250C,0x2510,0x2514,0x2518,0x251C,0x2524,0x252C,0x2534,0x253C,0x2580,0x2584,0x2588,0x258C,0x2590,
  0x2591,0x2592,0x2593,0x2320,0x25A0,0x2219,0x221A,0x2248,0x2264,0x2265,0x00A0,0x2321,0x00B0,0x00B2,0x00B7,0x00F7,
  0x2550,0x2551,0x2552,0x0451,0x0454,0x2554,0x0456,0x0457,0x2557,0x2558,0x2559,0x255A,0x255B,0x0491,0x255D,0x255E,
  0x255F,0x2560,0x2561,0x0401,0x0404,0x2563,0x0406,0x0407,0x2566,0x2567,0x2568,0x2569,0x256A,0x0490,0x256C,0x00A9,
  0x044E,0x0430,0x0431,0x0446,0x0434,0x0435,0x0444,0x0433,0x0445,0x0438,0x0439,0x043A,0x043B,0x043C,0x043D,0x043E,
  0x043F,0x044F,0x0440,0x0441,0x0442,0x0443,0x0436,0x0432,0x044C,0x044B,0x0437,0x0448,0x044D,0x0449,0x0447,0x044A,
  0x042E,0x0410,0x0411,0x0426,0x0414,0x0415,0x0424,0x0413,0x0425,0x0418,0x0419,0x041A,0x041B,0x041C,0x041D,0x041E,
  0x041F,0x042F,0x0420,0x0421,0x0422,0x0423,0x0416,0x0412,0x042C,0x042B,0x0417,0x0428,0x042D,0x0429,0x0427,0x042A,
};


//==========================================================================
//
//  recode_char
//
//==========================================================================
static uint8_t recode_char (uint8_t ch) {
  if (ch < 128) return ch;
  const uint16_t uc = charMap866[ch-128];
  for (unsigned f = 0; f < 128; ++f) {
    if (charMapKOI8[f] == uc) return (uint8_t)(f+128);
  }
  return (uint8_t)'?';
}


//==========================================================================
//
//  recode_char_to_pisdos
//
//==========================================================================
static uint8_t recode_char_to_pisdos (uint8_t ch) {
  if (ch < 128) return ch;
  const uint16_t uc = charMapKOI8[ch-128];
  for (unsigned f = 0; f < 128; ++f) {
    if (charMap866[f] == uc) return (uint8_t)(f+128);
  }
  return (uint8_t)'?';
}


//==========================================================================
//
//  recode_str_to
//
//==========================================================================
static const char *recode_str_to (const void *str, char buf[257], uint8_t maxlen) {
  if (!str || maxlen == 0) { buf[0] = 0; return buf; }
  const uint8_t *p = (const uint8_t *)str;
  unsigned bpos = 0;
  while (bpos < maxlen) {
    uint8_t ch = recode_char(p[bpos]);
    if (!ch) break;
    if (ch < 32 || ch == 127) ch = '?';
    buf[bpos++] = (char)ch;
  }
  while (bpos > 0 && buf[bpos-1] == ' ') --bpos;
  buf[bpos] = 0;
  return buf;
}


//==========================================================================
//
//  recode_str
//
//==========================================================================
static const char *recode_str (const void *str, uint8_t maxlen) {
  static char buf[257];
  return recode_str_to(str, buf, maxlen);
}


// dummy DE
const PisDOS_DirEnt pisdos_deroot = {0};


//==========================================================================
//
//  pisdos_unpack_date
//
//==========================================================================
void pisdos_unpack_date (PisDOSDateTime *dt, uint16_t v) {
  if (!dt) return;
  dt->day = v&0x1fU;
  dt->month = (v>>5)&0x0fU;
  dt->year = 1980+((v>>9)&0x7fU);
}


//==========================================================================
//
//  pisdos_unpack_time
//
//==========================================================================
void pisdos_unpack_time (PisDOSDateTime *dt, uint16_t v) {
  if (!dt) return;
  dt->second = (v&0x1f)<<1;
  dt->minute = (v>>5)&0x3fU;
  dt->hour = (v>>11)&0x1fU;
}


//==========================================================================
//
//  pisdos_sec_num
//
//==========================================================================
static inline int pisdos_sec_num (disk_t *d, int trk, int sidx) {
  return (sidx == 4 ? 9 : sidx+1);
}


//==========================================================================
//
//  koi8_tolower_inplace
//
//==========================================================================
static void koi8_tolower_inplace (void *s) {
  if (!s) return;
  for (uint8_t *t = (uint8_t *)s; *t; ++t) {
    *t = koi8tolowerTable[*t];
  }
}


//==========================================================================
//
//  pisdos_get_title
//
//==========================================================================
void pisdos_get_title (char fname[14], const PisDOS_DPB *dpb) {
  if (!fname) return;
  if (!dpb) { fname[0] = 0; return; }
  char buf[32];
  strcpy(fname, recode_str_to(dpb->title, buf, 8));
  for (char *s = fname; *s; ++s) {
    if (*s == '/') *s = '_';
  }
}


//==========================================================================
//
//  pisdos_get_name
//
//==========================================================================
void pisdos_get_name (char fname[14], const PisDOS_DirEnt *de) {
  if (!fname) return;
  if (!de) { fname[0] = 0; return; }
  char buf[32];
  strcpy(fname, recode_str_to(de->name, buf, 8));
  const char *ext = recode_str_to(de->ext, buf, 3);
  if (ext[0]) {
    strcat(fname, ".");
    strcat(fname, ext);
  }
  for (char *s = fname; *s; ++s) {
    if (*s == '/') *s = '_';
  }
}


//==========================================================================
//
//  pisdos_set_name
//
//==========================================================================
void pisdos_set_name (PisDOS_DirEnt *de, const char fname[14]) {
  if (!de) return;
  if (!fname) fname = "";
  const int fnlen = (int)strlen(fname);

  // find extension
  int extpos = fnlen;
  for (int f = 0; f < fnlen; ++f) if (fname[f] == '.') extpos = f;

  // build name
  for (int f = 0; f < 8; ++f) {
    if (f < extpos) {
      de->name[f] = recode_char_to_pisdos(((const uint8_t *)fname)[f]);
    } else {
      de->name[f] = ' ';
    }
  }

  // build ext
  ++extpos;
  for (int f = 0; f < 3; ++f) {
    if (extpos+f < fnlen) {
      de->ext[f] = recode_char_to_pisdos(((const uint8_t *)fname)[extpos+f]);
    } else {
      de->ext[f] = ' ';
    }
  }
}


//==========================================================================
//
//  pisdos_get_dpb
//
//==========================================================================
const PisDOS_DPB *pisdos_get_dpb (disk_t *dsk) {
  if (!dsk) return NULL;
  // get Disk Parameters Block
  int secsize = 0;
  const PisDOS_DPB *dpb = disk_get_sector_data_ptr(dsk, 0, 1, &secsize, NULL);
  if (!dpb || secsize != 1024) return NULL;
  if (memcmp(dpb->signature, "DSK", 3) != 0) return NULL;
  if (dpb->sector_size != 4) return NULL; // shit should be 1024 bytes
  if (dpb->tracks_total < 40 || dpb->tracks_total > 83) return NULL;
  if (dpb->sectors_per_track < 1 || dpb->sectors_per_track > 5) return NULL;
  if (dpb->rootdir_block < 1) return NULL;
  if (dpb->blocks_total < dpb->rootdir_block) return NULL;
  return dpb;
}


//==========================================================================
//
//  pisdos_block_ptr
//
//==========================================================================
const void *pisdos_block_ptr (disk_t *dsk, int blknum) {
  // get Disk Parameters Block
  const PisDOS_DPB *dpb = pisdos_get_dpb(dsk);
  if (!dpb) return NULL;

  // div 4, because block size is 256 bytes, and sector size is 1024 bytes
  const int trk = (blknum/4)/dpb->sectors_per_track;
  int sec = (blknum/4)%dpb->sectors_per_track;
  // pisdos seems to read the whole track, and then scan it manually
  sec = pisdos_sec_num(dsk, trk, sec);
  const int ofs = (blknum%4)*256;

  //fprintf(stderr, "BLK:%d; TRK:%d; SEC:%d; OFS:%d\n", blknum, trk, sec, ofs);
  int secsize = 0;
  const uint8_t *res = disk_get_sector_data_ptr(dsk, trk, sec, &secsize, NULL);
  if (res && secsize != 1024) {
    fprintf(stderr, "ERROR reading block #%d (trk:%d; sector:%d; sector size:%d)\n", blknum,
                    trk, sec, secsize);
    return NULL;
  }
  //fprintf(stderr, "BLK:%d; TRK:%d; SEC:%d; OFS:%d; de=%p\n", blknum, trk, sec, ofs, res);

  return (res ? (const void *)(res+ofs) : NULL);
}


//==========================================================================
//
//  pisdos_build_frags
//
//==========================================================================
int pisdos_build_frags (disk_t *dsk, const PisDOS_DirEnt *de, PisDOSFragList *list) {
  if (!de || !list) return -1;

  // init it here
  list->curr_frag = 0;
  list->curr_block_idx = 0;

  // for continuous files, it is easy
  if (de->attr&PISDOS_FLAG_CONTINUOUS) {
    list->frag_count = 1;
    list->start_block = (pisdos_is_dir(de) ? de->dir.first_block : de->file.first_block);
    list->block_count = pisdos_get_entry_size(de)/256+!!(pisdos_get_entry_size(de)%256);
    return 0;
  }

  uint16_t fgblock = (pisdos_is_dir(de) ? de->dir.frag_block : de->file.first_block);
  const uint8_t *fgr = pisdos_block_ptr(dsk, fgblock);
  if (!fgr) return -1;

  if (fgr[0] == 0) {
    if (pisdos_get_entry_size(de) != 0) return -1;
    list->frag_count = 1;
    list->start_block = 0;
    list->block_count = 0;
    return 0;
  }

  // convert one fragment to continuous
  if (fgr[0] == 1) {
    list->frag_count = 1;
    list->start_block = fgr[1]|(fgr[2]<<8);
    list->block_count = fgr[3];
    return 0;
  }

  uint8_t fblocks[256*3];
  memcpy(fblocks, fgr, 256);
  // need second block?
  if (fgr[0]*3+1 >= 256) {
    const uint8_t *blk = pisdos_block_ptr(dsk, fgblock+1);
    if (!blk) return -1;
    memcpy(fblocks+256, blk, 256);
    // need third block?
    if (fgr[0]*3+1 >= 512) {
      blk = pisdos_block_ptr(dsk, fgblock+2);
      if (!blk) return -1;
      memcpy(fblocks+512, blk, 256);
    }
  }

  list->frag_count = fgr[0];
  memcpy(list->frags, fblocks+1, fgr[0]*3);
  list->start_block = list->frags[0].start_block;
  list->block_count = list->frags[0].block_count;

  return 0;
}


//==========================================================================
//
//  pisdos_next_block
//
//==========================================================================
const void *pisdos_next_block (disk_t *dsk, PisDOSFragList *list) {
  if (!dsk || !list) return NULL;

  for (;;) {
    // move to the next frag?
    if (list->curr_block_idx >= list->block_count) {
      if (list->curr_frag >= list->frag_count) return NULL; // no more
      list->curr_block_idx = 0;
      ++list->curr_frag;
      list->start_block = list->frags[list->curr_frag].start_block;
      list->block_count = list->frags[list->curr_frag].block_count;
      continue;
    }

    // here we have at least one block in the current fragment
    const void *blk = pisdos_block_ptr(dsk, list->start_block+list->curr_block_idx);
    if (blk) ++list->curr_block_idx;
    return blk;
  }
}


//==========================================================================
//
//  pisdos_dir_foreach
//
//  if `dirde` is NULL, read root directory
//
//==========================================================================
int pisdos_dir_foreach (disk_t *dsk, const PisDOS_DirEnt *dirde,
                        pisdor_dir_iterator_cb cb, void *udata)
{
  if (!dsk || !cb) return -1;

  PisDOSFragList frags;

  if (!dirde || dirde == &pisdos_deroot) {
    const PisDOS_DPB *dpb = pisdos_get_dpb(dsk);
    if (!dpb) return -2;
    if (dpb->rootdir_block < 1) return -2;
    dirde = pisdos_block_ptr(dsk, dpb->rootdir_block);
    if (!dirde) return -2;
    if (pisdos_build_frags(dsk, dirde, &frags) != 0) {
      fprintf(stderr, "ERROR: cannot build fragment list for root directory!\n");
      return -3;
    }
  } else {
    if ((dirde->attr&PISDOS_FLAG_DIR) == 0) return -4;
    if ((dirde->attr&PISDOS_FLAG_USED) == 0) return -5;
    // load directory block
    if (pisdos_build_frags(dsk, dirde, &frags) != 0) {
      fprintf(stderr, "ERROR: cannot build fragment list for dir!\n");
      return -6;
    }
    const void *dblk = pisdos_next_block(dsk, &frags);
    if (!dblk) return -7;
    dirde = dblk; // use first directory entry
    if (pisdos_build_frags(dsk, dirde, &frags) != 0) {
      fprintf(stderr, "ERROR: cannot build fragment list directory!\n");
      return -8;
    }
  }

  uint8_t files_total = dirde->dir.files_total;
  if (files_total < 2) return 0;

  uint32_t currofs = 32; // first element is skipped
  const uint8_t *blk = pisdos_next_block(dsk, &frags);

  for (uint32_t fidx = 1; fidx < files_total; ++fidx, currofs += 32) {
    // need next block?
    if (currofs == 256) {
      blk = pisdos_next_block(dsk, &frags);
      if (!blk) {
        fprintf(stderr, "ERROR reading directory at entry #%d!\n", fidx);
        return -10;
      }
      currofs = 0;
    }

    const PisDOS_DirEnt *de = (const PisDOS_DirEnt *)(blk+currofs);
    if ((de->attr&PISDOS_FLAG_USED) == 0) continue;
    //if ((de->attr&PISDOS_FLAG_HIDDEN) == 0) continue;

    const int res = cb(de, udata);
    if (res) return res;
  }

  return 0;
}


typedef struct {
  const char *mask;
  const PisDOS_DirEnt *resde;
} PisDOSFindUData;


//==========================================================================
//
//  pisdos_find_dir_entry
//
//==========================================================================
static int pisdos_find_dir_entry (const PisDOS_DirEnt *de, void *udata) {
  PisDOSFindUData *ud = (PisDOSFindUData *)udata;
  char name[16];
  pisdos_get_name(name, de);
  koi8_tolower_inplace(name);
  if (p3dskGlobMatch(name, ud->mask, P3DSK_GLOB_DEFAULT)) {
    if (ud->resde) {
      ud->resde = NULL;
      return -666;
    }
    ud->resde = de;
    if (!p3dskIsGlobMask(ud->mask)) return 1; // no more
    // try other files, mask may match only one
  }
  return 0;
}


//==========================================================================
//
//  pisdos_is_root_path
//
//==========================================================================
int pisdos_is_root_path (const char *path) {
  if (!path) return 1;
  while (path[0] && path[0] == '/') ++path;
  return !path[0];
}


//==========================================================================
//
//  pisdos_find_in_dir
//
//  returns NULL on error
//
//==========================================================================
const PisDOS_DirEnt *pisdos_walk_path (disk_t *dsk, const PisDOS_DirEnt *dirde,
                                       const char *path)
{
  if (path && path[0] == '/') dirde = &pisdos_deroot;
  if (pisdos_is_root_path(path)) return (dirde ? dirde : &pisdos_deroot);

  PisDOSFindUData ud;
  char xfname[64];

  while (path[0]) {
    if (path[0] == '/') { ++path; continue; }

    const char *end = strchr(path, '/');
    if (!end) break; // last name
    const int fnlen = (int)(ptrdiff_t)(end-path);
    if (fnlen > 15) return NULL; // name too long
    memcpy(xfname, path, fnlen);
    xfname[fnlen] = 0;

    //if (p3dskIsGlobMask(xfname)) return NULL; // no mask in the middle

    koi8_tolower_inplace(xfname);

    ud.mask = xfname;
    ud.resde = NULL;
    pisdos_dir_foreach(dsk, dirde, &pisdos_find_dir_entry, &ud);

    if (!ud.resde) return NULL;

    dirde = ud.resde;
    path = end+1;
  }

  return dirde;
}


//==========================================================================
//
//  pisdos_ff_cb
//
//==========================================================================
static int pisdos_ff_cb (const PisDOS_DirEnt *de, void *udata) {
  PisDOSFindInfo *fi = (PisDOSFindInfo *)udata;
  char name[16];
  pisdos_get_name(name, de);
  koi8_tolower_inplace(name);
  if (p3dskGlobMatch(name, fi->mask, P3DSK_GLOB_DEFAULT)) {
    if (fi->de_count == 256) __builtin_trap();
    fi->delist[fi->de_count++] = de;
  }
  return 0;
}


//==========================================================================
//
//  pisdos_findnext
//
//==========================================================================
const PisDOS_DirEnt *pisdos_findnext (PisDOSFindInfo *fi) {
  if (!fi || !fi->dsk || !fi->dirde || fi->de_current >= fi->de_count) return NULL;
  return fi->delist[fi->de_current++];
}


//==========================================================================
//
//  pisdos_findfirst
//
//==========================================================================
const PisDOS_DirEnt *pisdos_findfirst (PisDOSFindInfo *fi, disk_t *dsk, const char *fname) {
  if (!fi) return NULL;
  memset(fi, 0, sizeof(*fi));
  if (!dsk) return NULL;

  fi->dsk = dsk;
  fi->dirde = pisdos_walk_path(dsk, &pisdos_deroot, fname);
  if (!fi->dirde) return NULL;

  const char *ee = strrchr(fname, '/');
  const char *mask = (ee ? ee+1 : fname);

  //fprintf(stderr, "EE=<%s>; mask=<%s>\n", ee, mask);
  if (ee && !ee[1]) {
    fi->delist[0] = fi->dirde;
    fi->de_count = 1;
    return fi->delist[fi->de_current++];
  }

  if (!mask[0] || strcmp(mask, "*") == 0) mask = "*.*";

  if (strlen(mask) >= sizeof(fi->mask)) return NULL;
  strcpy(fi->mask, mask);
  koi8_tolower_inplace(fi->mask);

  pisdos_dir_foreach(dsk, fi->dirde, &pisdos_ff_cb, fi);

  return (fi->de_count ? fi->delist[fi->de_current++] : NULL);
}


//==========================================================================
//
//  pisdos_debug_dump_dpb
//
//==========================================================================
void pisdos_debug_dump_dpb (disk_t *dsk) {
  // get Disk Parameters Block
  int secsize = 0;
  const PisDOS_DPB *dpb = disk_get_sector_data_ptr(dsk, 0, 1, &secsize, NULL);

  if (!dpb) return;

  if (memcmp(dpb->signature, "DSK", 3) != 0) {
    fprintf(stderr, "NOT A SHITDOS DISK!\n");
    return;
  }

  if (memcmp(dpb->sign_chic, "DSK", 3) == 0) {
    fprintf(stderr, "SHITCHIK DISK DETECTED.\n");
  }

  #if 1
  fprintf(stderr, "DPB SECTOR SIZE: %d\n", secsize);
  fprintf(stderr, "TITLE: <%s>\n", recode_str(dpb->title, 8));
  fprintf(stderr, "BLOCK COUNT: %u\n", dpb->blocks_total);
  fprintf(stderr, "CYLINDER COUNT: %u\n", dpb->tracks_total);
  fprintf(stderr, "SECTOR COUNT: %u\n", dpb->sectors_per_track);
  fprintf(stderr, "SECTOR SIZE: %u\n", dpb->sector_size);
  fprintf(stderr, "ROOT DIR BLOCK: %u\n", dpb->rootdir_block);
  fprintf(stderr, "TYPE: %u\n", dpb->type);
  fprintf(stderr, "DATE: #%04X\n", dpb->date);
  fprintf(stderr, "SECTOR TABLE:");
  for (unsigned f = 0; f < 16; ++f) fprintf(stderr, " %u", dpb->sector_table[f]);
  fprintf(stderr, "\n");
  #endif

  int dirblk = dpb->rootdir_block;
  //int dirblk = 23;
  const PisDOS_DirEnt *f0 = pisdos_block_ptr(dsk, dirblk);
  if (!f0) {
    fprintf(stderr, "ERROR: cannot read root directory!\n");
    return;
  }

  fprintf(stderr, "--------------\n");
  fprintf(stderr, "NAME: <%s>\n", recode_str(f0->name, 8));
  fprintf(stderr, "EXT: <%s>\n", recode_str(f0->ext, 3));
  fprintf(stderr, "ATTR: #%02X\n", f0->attr);
  fprintf(stderr, "DATE: #%04X\n", f0->date);

  if ((f0->attr&PISDOS_FLAG_DIR) == 0) {
    fprintf(stderr, "ERROR: first root directory entry must be a directory!\n");
    //return -1;
  }

  if ((f0->attr&PISDOS_FLAG_USED) == 0) {
    fprintf(stderr, "ERROR: first root directory entry must exist!\n");
    //return -1;
  }

  if ((f0->attr&PISDOS_FLAG_CONTINUOUS) == 0) {
    fprintf(stderr, "ERROR: first root directory entry must be solid!\n");
    //return -1;
  }

  fprintf(stderr, "PDIRBLK: %u\n", f0->dir.parent_dir_block);
  fprintf(stderr, "SIZE: %u\n", f0->dir.size);
  fprintf(stderr, "LEVEL: %u\n", f0->dir.level);
  fprintf(stderr, "DSCBLK: %u\n", f0->dir.frag_block);
  fprintf(stderr, "FIRSTBLK: %u\n", f0->dir.first_block);
  fprintf(stderr, "TOTAL FILES: %u\n", f0->dir.files_total);
  fprintf(stderr, "FILE COUNT: %u\n", f0->dir.files_count);
  fprintf(stderr, "CHIK LEVEL: %u\n", f0->dir.level_chic);

  for (uint32_t fidx = 0; fidx < f0->dir.files_total; ++fidx) {
    const PisDOS_DirEnt *de = pisdos_block_ptr(dsk, dirblk+fidx/8)+32*(fidx%8);
    if ((de->attr&PISDOS_FLAG_USED) == 0) continue;
    if (de->attr&PISDOS_FLAG_DIR) {
      fprintf(stderr, "=== ENTRY #%d : DIR ===\n", fidx);
      fprintf(stderr, "  NAME: <%s>\n", recode_str(de->name, 8));
      fprintf(stderr, "  EXT: <%s>\n", recode_str(de->ext, 3));
      fprintf(stderr, "  ATTR: #%02X\n", de->attr);
      fprintf(stderr, "  DATE: #%04X\n", de->date);
      fprintf(stderr, "  PDIRBLK: %u\n", de->dir.parent_dir_block);
      fprintf(stderr, "  SIZE: %u\n", de->dir.size);
      fprintf(stderr, "  LEVEL: %u\n", de->dir.level);
      fprintf(stderr, "  DSCBLK: %u\n", de->dir.frag_block);
      fprintf(stderr, "  FIRSTBLK: %u\n", de->dir.first_block);
      fprintf(stderr, "  TOTAL FILES: %u\n", de->dir.files_total);
      fprintf(stderr, "  FILE COUNT: %u\n", de->dir.files_count);
      fprintf(stderr, "  CHIK LEVEL: %u\n", de->dir.level_chic);
    } else {
      fprintf(stderr, "=== ENTRY #%d : FILE ===\n", fidx);
      fprintf(stderr, "  NAME: <%s>\n", recode_str(de->name, 8));
      fprintf(stderr, "  EXT: <%s>\n", recode_str(de->ext, 3));
      fprintf(stderr, "  ATTR: #%02X\n", de->attr);
      fprintf(stderr, "  DATE: #%04X\n", de->date);
      fprintf(stderr, "  LOAD ADDR: #%04X\n", de->file.load_addr);
      fprintf(stderr, "  SIZE: %u\n", pisdos_get_entry_size(de));
      fprintf(stderr, "  FIRST BLOCK: %u\n", de->file.first_block);
      fprintf(stderr, "  SYSFLAG: #%02X\n", de->file.sys_flag);
      fprintf(stderr, "  CRC: #%04X\n", de->file.crc);
      fprintf(stderr, "  TIME: #%04X\n", de->file.time);
    }
    fprintf(stderr, "  ATTRS: USED");
    if (de->attr&PISDOS_FLAG_HIDDEN) fprintf(stderr, " HIDDEN");
    if (de->attr&PISDOS_FLAG_NO_READ) fprintf(stderr, " NO-READ");
    if (de->attr&PISDOS_FLAG_NO_WRITE) fprintf(stderr, " NO-WRITE");
    if (de->attr&PISDOS_FLAG_NO_DELETE) fprintf(stderr, " NO-DELETE");
    if ((de->attr&PISDOS_FLAG_CONTINUOUS) == 0) fprintf(stderr, " SEGMENTED");
    fprintf(stderr, "\n");
  }
}
