/* beta.h: Routines for handling the Beta disk interface
   Copyright (c) 2003-2016 Fredrick Meunier, Philip Kendall
   Copyright (c) 2015 Stuart Brady

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   E-mail: philip-fuse@shadowmagic.org.uk

     Fred: fredm@spamcop.net

*/
#ifndef FUSE_BETA_H
#define FUSE_BETA_H
#ifndef LIBFUSE_DISK_IO_ONLY

#include "fdd.h"


void beta_cr_write (uint16_t port, uint8_t b);

uint8_t beta_sr_read (uint16_t port);

uint8_t beta_tr_read (uint16_t port);
void beta_tr_write (uint16_t port, uint8_t b);

uint8_t beta_sec_read (uint16_t port);
void beta_sec_write (uint16_t port, uint8_t b);

uint8_t beta_dr_read (uint16_t port);
void beta_dr_write (uint16_t port, uint8_t b);

uint8_t beta_sp_read (uint16_t port);
void beta_sp_write (uint16_t port, uint8_t b);

void beta_reset (void);


typedef enum beta_drive_number {
  BETA_DRIVE_A = 0,
  BETA_DRIVE_B,
  BETA_DRIVE_C,
  BETA_DRIVE_D,
  BETA_NUM_DRIVES,
} beta_drive_number;


void beta_disk_eject (beta_drive_number which);

fdd_t *beta_get_fdd (beta_drive_number which);
disk_t *beta_get_disk (beta_drive_number which);

void beta_init (disk_t *disks[4]);
void beta_end (void);


#endif
#endif
