/* disk.c: Routines for handling disk images
   Copyright (c) 2007-2017 Gergely Szasz

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   Philip: philip-fuse@shadowmagic.org.uk

*/
#ifndef LIBFUSE_DISABLE_MINIZ
//==========================================================================
//
//  zlib_inflate
//
//==========================================================================
static int zlib_inflate (const uint8_t *gzptr, size_t gzlength,
                         uint8_t **outptr, size_t *outlength, int gzip_hack)
{
  mz_stream stream;
  int error;

  /* Use default memory management */
  stream.zalloc = NULL;
  stream.zfree = NULL;
  stream.opaque = NULL;

  stream.next_in = gzptr;
  stream.avail_in = gzlength;

  if (gzip_hack) {
    /*
     * HACK ALERT (comment from zlib 1.1.14:gzio.c:143)
     *
     * windowBits is passed < 0 to tell that there is no zlib header.
     * Note that in this case inflate *requires* an extra "dummy" byte
     * after the compressed stream in order to complete decompression
     * and return Z_STREAM_END. Here the gzip CRC32 ensures that 4 bytes
     * are present after the compressed stream.
     *
     */
    error = mz_inflateInit2(&stream, -15);
  } else {
    error = mz_inflateInit(&stream);
  }

  switch (error) {
    case MZ_OK: break;
    case MZ_MEM_ERROR:
      mz_inflateEnd(&stream);
      return -1;
    default:
      mz_inflateEnd(&stream);
      return -1;
  }

  if (*outlength) {
    *outptr = calloc(1, *outlength);
    if (!*outptr) {
      mz_inflateEnd(&stream);
      return -1;
    }
    stream.next_out = *outptr;
    stream.avail_out = *outlength;
    error = mz_inflate(&stream, MZ_FINISH);
  } else {
    *outptr = stream.next_out = NULL;
    *outlength = stream.avail_out = 0;
    do {
      uint8_t *ptr;
      *outlength += 16384;
      stream.avail_out += 16384;
      ptr = realloc(*outptr, *outlength);
      if (!ptr) {
        free(*outptr);
        mz_inflateEnd(&stream);
        return -1;
      }
      stream.next_out = ptr+(stream.next_out-(*outptr));
      *outptr = ptr;
      error = mz_inflate(&stream, 0);
    } while (error == MZ_OK);
  }

  *outlength = stream.next_out-(*outptr);
  void *npp = realloc(*outptr, *outlength);
  if (!npp) {
    free(*outptr);
    mz_inflateEnd(&stream);
    return -1;
  }
  *outptr = npp;

  if (error != MZ_STREAM_END) {
    free(*outptr);
    mz_inflateEnd(&stream);
    return -1;
  }

  error = mz_inflateEnd(&stream);
  if (error != MZ_OK) {
    free(*outptr);
    mz_inflateEnd(&stream);
    return -1;
  }

  return 0;
}


//==========================================================================
//
//  zlib_compress
//
//  Deflates a block of data.
//  Input: data   -> source data
//         length == source data length
//  Output: *gzptr    -> deflated data (malloced in this fn),
//          *gzlength == length of the deflated data
//  Returns: non-zero on error
//
//==========================================================================
static int zlib_compress (const uint8_t *data, size_t length,
                          uint8_t **gzptr, size_t *gzlength )
{
  mz_ulong gzl = (mz_ulong)(length*1.001)+12;
  int gzret;

  *gzptr = calloc(1, gzl);
  if (!*gzptr) return -1;
  gzret = mz_compress2(*gzptr, &gzl, data, length, MZ_BEST_COMPRESSION);

  if (gzret == MZ_OK) {
    *gzlength = gzl;
    return 0;
  }

  free(*gzptr);
  *gzptr = 0;
  return -1;
}


//==========================================================================
//
//  udi_read_compressed
//
//==========================================================================
static int udi_read_compressed (const uint8_t *buffer,
                                size_t compr_size, size_t uncompr_size,
                                uint8_t **data, size_t *data_size)
{
  uint8_t *tmp = NULL;
  size_t olength = uncompr_size;

  int error = zlib_inflate(buffer, compr_size, &tmp, &olength, 0);
  if (error) return error;

  if (*data_size < uncompr_size) {
    *data = realloc(*data, uncompr_size);
    *data_size = uncompr_size;
  }
  memcpy(*data, tmp, uncompr_size);
  free(tmp);

  return 0;
}


//==========================================================================
//
//  udi_write_compressed
//
//==========================================================================
static int udi_write_compressed (const uint8_t *buffer,
                                 size_t uncompr_size, size_t *compr_size,
                                 uint8_t **data, size_t *data_size)
{
  uint8_t *tmp = NULL;

  int error = zlib_compress(buffer, uncompr_size, &tmp, compr_size);
  if (error) return error;

  if (*data_size < *compr_size) {
    *data = realloc(*data, *compr_size);
    *data_size = *compr_size;
  }
  memcpy(*data, tmp, *compr_size);
  free(tmp);

  return 0;
}
#endif  /* LIBFUSE_DISABLE_MINIZ */


//==========================================================================
//
//  udi_pack_tracks
//
//==========================================================================
static void udi_pack_tracks (disk_t *d) {
  int i, tlen, clen, ttyp;
  uint8_t *tmp;

  for (i = 0; i < d->sides*d->cylinders; ++i) {
    DISK_SET_TRACK_IDX(d, i);
    tmp = d->pos.track;
    ttyp = tmp[-1];
    tlen = tmp[-3]+256*tmp[-2];
    clen = DISK_CLEN(tlen);
    tmp += tlen;
    /* copy clock if needed */
    if (tmp != d->pos.clocks) memcpy(tmp, d->pos.clocks, clen);
    if (ttyp == 0x00 || ttyp == 0x01) continue;
    tmp += clen;
    if (ttyp&0x02) {
      /* copy FM marks */
      if (tmp != d->pos.fm) memcpy(tmp, d->pos.fm, clen);
      tmp += clen;
    }
    if ((ttyp&0x80) == 0) continue;
    /* copy WEAK marks */
    if (tmp != d->pos.weak) memcpy(tmp, d->pos.weak, clen);
  }
}


//==========================================================================
//
//  udi_unpack_tracks
//
//==========================================================================
static void udi_unpack_tracks (disk_t *d) {
  int i, tlen, clen, ttyp;
  uint8_t *tmp;
  const uint8_t mask[] = { 0xff, 0x80, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc, 0xfe };
  for (i = 0; i < d->sides*d->cylinders; ++i) {
    DISK_SET_TRACK_IDX(d, i);
    tmp = d->pos.track;
    ttyp = tmp[-1];
    tlen = tmp[-3]+256*tmp[-2];
    clen = DISK_CLEN(tlen);
    tmp += tlen;
    if (ttyp&0x80) tmp += clen;
    if (ttyp&0x02) tmp += clen;
    if ((ttyp&0x80)) {
      /* copy WEAK marks */
      if (tmp != d->pos.weak) memcpy(d->pos.weak, tmp, clen);
      tmp -= clen;
    } else {
      /* clear WEAK marks */
      memset(d->pos.weak, 0, clen);
    }
    if (ttyp&0x02) {
      /* copy FM marks */
      if (tmp != d->pos.fm) memcpy(d->pos.fm, tmp, clen);
      tmp -= clen;
    } else {
      /* set/clear FM marks */
      memset(d->pos.fm, (ttyp&0x01 ? 0xff : 0), clen);
      if (tlen%8) {
        /* adjust last byte */
        d->pos.fm[clen-1] &= mask[tlen%8];
      }
    }
    /* copy clock if needed */
    if (tmp != d->pos.clocks) memcpy(d->pos.clocks, tmp, clen);
  }
}

/* calculate track len from type, if type eq. 0x00/0x01/0x02/0x80/0x81/0x82
   !!! not for 0x83 nor 0xf0 !!!
*/
#define UDI_TLEN(type, bpt)  ((bpt)+DISK_CLEN(bpt)*(1+(type&0x02 ? 1 : 0)+(type&0x80 ? 1 : 0)))


//==========================================================================
//
//  udi_uncompress_tracks
//
//==========================================================================
static int udi_uncompress_tracks (disk_t *d) {
  int i;
  uint8_t *data = NULL;
  #ifndef LIBFUSE_DISABLE_MINIZ
  size_t data_size = 0;
  int bpt, tlen, clen, ttyp;
  #endif

  for (i = 0; i < d->sides*d->cylinders; ++i) {
    DISK_SET_TRACK_IDX(d, i);
    if (d->pos.track[-1] != 0xf0) continue; /* if not compressed */

    #ifdef LIBFUSE_DISABLE_MINIZ
    return d->status = DISK_UNSUP;
    #else
    clen = d->pos.track[-3]+256*d->pos.track[-2]+1;
    ttyp = d->pos.track[0]; /* compressed track type */
    bpt = d->pos.track[1]+256*d->pos.track[2]; /* compressed track len... */
    tlen = UDI_TLEN(ttyp, bpt);
    d->pos.track[-1] = ttyp;
    d->pos.track[-3] = d->pos.track[1];
    d->pos.track[-2] = d->pos.track[2];
    if (udi_read_compressed(d->pos.track+3, clen, tlen, &data, &data_size)) {
      if (data) free(data);
      return (d->status = DISK_UNSUP);
    }
    memcpy(d->pos.track, data, tlen); /* read track */
    #endif
  }
  if (data) free(data);
  return DISK_OK;
}


#ifndef LIBFUSE_DISABLE_MINIZ
//==========================================================================
//
//  udi_compress_tracks
//
//==========================================================================
static int udi_compress_tracks (disk_t *d) {
  int i, tlen;
  uint8_t *data = NULL;
  size_t clen, data_size = 0;

  for (i = 0; i < d->sides*d->cylinders; ++i) {
    DISK_SET_TRACK_IDX(d, i);
    if (d->pos.track[-1] == 0xf0) continue; /* already compressed??? */

    tlen = UDI_TLEN(d->pos.track[-1], d->pos.track[-3]+256*d->pos.track[-2]);
    /* if fail to compress, skip ... */
    if (udi_write_compressed(d->pos.track, tlen, &clen, &data, &data_size) || clen < 1) continue;
    /* if compression too large, skip... */
    if (clen > 65535 || clen >= tlen) continue;
    d->pos.track[0] = d->pos.track[-1]; /* track type... */
    d->pos.track[1] = d->pos.track[-3]; /* compressed track len... */
    d->pos.track[2] = d->pos.track[-2]; /* compressed track len... */
    memcpy(d->pos.track+3, data, clen); /* read track */
    --clen;
    d->pos.track[-1] = 0xf0;
    d->pos.track[-3] = clen&0xff;
    d->pos.track[-2] = (clen>>8)&0xff;
  }
  if (data) free(data);
  return DISK_OK;
}
#endif


//==========================================================================
//
//  open_udi
//
//==========================================================================
static int open_udi (buffer_t *buffer, disk_t *d, int preindex_unused) {
  int i, bpt, ttyp, tlen, error;
  size_t eof;
  uint32_t crc;

  if (buffer->file.length < 16) return (d->status = DISK_OPEN);
  if (memcmp(&buff[0], "UDI!", 4) != 0) return (d->status = DISK_OPEN);
  // check version
  if (buff[8] != 0) return (d->status = DISK_OPEN);
  // should not have extra header
  if (buff[12] != 0 || buff[13] != 0 || buff[14] != 0 || buff[15] != 0) return (d->status = DISK_OPEN);

  crc = ~(uint32_t)0;

  /* check file length */
  eof = buff[4]+256U*buff[5]+65536U*buff[6]+16777216U*buff[7];
  if (eof != buffer->file.length-4) return (d->status = DISK_OPEN);

  /* check CRC32 */
  for (i = 0; i < eof; ++i) crc = crc_udi(crc, buff[i]);
  if (crc != buff[eof]+256U*buff[eof+1]+65536U*buff[eof+2]+16777216U*buff[eof+3]) {
    return (d->status = DISK_OPEN);
  }

  d->sides = buff[10]+1;
  d->cylinders = buff[9]+1;
  GEOM_CHECK;
  d->density = DISK_DENS_AUTO;
  buffer->index = 16;
  d->bpt = 0;

  /* scan file for the longest track */
  for (i = 0; buffer->index < eof; ++i) {
    if (buffavail(buffer) < 3) return (d->status = DISK_OPEN);

    ttyp = buff[0];
    if (ttyp != 0x00 && ttyp != 0x01 && ttyp != 0x02 && ttyp != 0x80 &&
        ttyp != 0x81 && ttyp != 0x82 && ttyp != 0x83 && ttyp != 0xf0)
    {
      return d->status = DISK_UNSUP;
    }

    #ifdef LIBFUSE_DISABLE_MINIZ
    if (ttyp == 0xf0) d->status = DISK_UNSUP;
    #endif
    if (ttyp == 0x83) {
      /* multiple read */
      if (i == 0) return (d->status = DISK_GEOM); /* cannot be first track */
      /* not a real track */
      --i;
      bpt = 0;
      tlen = buff[1]+256*buff[2]; /* current track len... */
      tlen = (tlen&0xfff8)*(tlen&0x07);
    } else if (ttyp == 0xf0) {
      /* compressed track */
      if (buffavail(buffer) < 7) return (d->status = DISK_OPEN);
      bpt = buff[4]+256*buff[5];
      tlen = 7+buff[1]+256*buff[2];
    } else {
      bpt = buff[1]+256*buff[2]; /* current track len... */
      tlen = 3+UDI_TLEN(ttyp, bpt);
    }
    if (bpt > d->bpt) d->bpt = bpt;
    if (buffseek(buffer, tlen, SEEK_CUR) == -1) return (d->status = DISK_OPEN);
  }

  if (d->bpt == 0) return (d->status = DISK_GEOM);

  bpt = d->bpt; /* save the maximal value */
  d->tlen = 3+bpt+3*DISK_CLEN(bpt);
  d->bpt = 0; /* we know exactly the track len... */

  if (disk_alloc(d) != DISK_OK) return d->status;

  d->bpt = bpt; /* restore the maximal byte per track */
  buffer->index = 16;

  for (i = 0; buffer->index < eof; ++i) {
    DISK_SET_TRACK_IDX(d, i);
    ttyp = buff[0];
    bpt = buff[1]+256*buff[2]; /* current track len... */
    memset(d->pos.track, 0x4e, d->bpt); /* fillup */
    /* read track + clocks */
    if (ttyp == 0x83) {
      /* multiple read */
      --i; /* not a real track */
      DISK_SET_TRACK_IDX(d, i); /* back to previouse track */
      d->pos.weak += buff[3]+256*buff[4]; /* add offset to weak */
      tlen = (buff[1]+256*buff[2])>>3; /* weak len in bytes */
      for (--tlen; tlen >= 0; --tlen) d->pos.weak[tlen] = 0xff;
      tlen = buff[1]+256*buff[2]; /* current track len... */
      tlen = (tlen&0xfff8)*(tlen&0x07);
      buffseek(buffer, tlen, SEEK_CUR);
    } else {
      if (ttyp == 0xf0) {
        /* compressed */
        tlen = bpt+4;
      } else {
        tlen = UDI_TLEN(ttyp, bpt);
      }
      d->pos.track[-1] = ttyp;
      d->pos.track[-3] = buff[1];
      d->pos.track[-2] = buff[2];
      buffer->index += 3;
      buffread(d->pos.track, tlen, buffer); /* first read data */
    }
  }

  error = udi_uncompress_tracks(d);
  if (error) return error;
  udi_unpack_tracks(d);

  return (d->status = DISK_OK);
}


//==========================================================================
//
//  write_udi
//
//==========================================================================
static int write_udi (FILE *file, disk_t *d) {
  int i, j, error=error;
  size_t len;
  uint32_t crc;
  uint8_t head[256];

  udi_pack_tracks(d);
  #ifndef LIBFUSE_DISABLE_MINIZ
  udi_compress_tracks(d);
  #endif

  crc = ~(uint32_t)0;
  len = 16;

  /* check tracks */
  for (i = 0; i < d->sides*d->cylinders; ++i) {
    DISK_SET_TRACK_IDX(d, i);
    if (d->pos.track[-1] == 0xf0) {
      len += 7+d->pos.track[-3]+256*d->pos.track[-2];
    } else {
      len += 3+UDI_TLEN(d->pos.track[-1], d->pos.track[-3]+256*d->pos.track[-2]);
    }
  }

  head[0] = 'U';
  head[1] = 'D';
  head[2] = 'I';
  head[3] = '!';
  head[4] = len&0xff;
  head[5] = (len>>8)&0xff;
  head[6] = (len>>16)&0xff;
  head[7] = (len>>24)&0xff;
  head[8] = 0x00;
  head[9] = d->cylinders-1;
  head[10] = d->sides-1;
  head[11] = head[12] = head[13] = head[14] = head[15] = 0;
  if (fwrite(head, 16, 1, file) != 1) return (d->status = DISK_WRPART);

  for (j = 0; j < 16; ++j) crc = crc_udi(crc, head[j]);

  /* write tracks */
  for (i = 0; i < d->sides*d->cylinders; ++i) {
    DISK_SET_TRACK_IDX(d, i);
    head[0] = d->pos.track[-1]; /* track type */
    head[1] = d->pos.track[-3]; /* track len  */
    head[2] = d->pos.track[-2]; /* track len2 */
    if (fwrite(head, 3, 1, file) != 1) return (d->status = DISK_WRPART);

    for (j = 0; j < 3; ++j) crc = crc_udi(crc, head[j]);

    if (d->pos.track[-1] == 0xf0) {
      len = 4+d->pos.track[-3]+256*d->pos.track[-2];
    } else {
      len = UDI_TLEN(d->pos.track[-1], d->pos.track[-3]+256*d->pos.track[-2]);
    }
    if (fwrite(d->pos.track, len, 1, file) != 1) return (d->status = DISK_WRPART);

    for (j = len; j > 0; --j) {
      crc = crc_udi(crc, *d->pos.track);
      ++d->pos.track;
    }
  }

  head[0] = crc&0xff;
  head[1] = (crc>>8)&0xff;
  head[2] = (crc>>16)&0xff;
  head[3] = (crc>>24)&0xff;
  if (fwrite(head, 4, 1, file) != 1) fclose(file); /* CRC */

  #ifndef LIBFUSE_DISABLE_MINIZ
  /* Keep tracks uncompressed in memory */
  error = udi_uncompress_tracks(d);
  if (error) return error;
  #endif

  udi_unpack_tracks(d);
  return (d->status = DISK_OK);
}
