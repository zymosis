/* disk.c: Routines for handling disk images
   Copyright (c) 2007-2017 Gergely Szasz

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   Philip: philip-fuse@shadowmagic.org.uk

*/
typedef struct trdos_spec_t {
  uint8_t first_free_sector; /* 0 to 15 */
  uint8_t first_free_track;  /* 0 to ? */
  uint8_t disk_type;
  uint8_t file_count;
  uint16_t free_sectors;
  uint8_t id;
  char password[9];      /* not null-terminated */
  uint8_t deleted_files;
  char disk_label[8];    /* not null-terminated */
} trdos_spec_t;


typedef struct trdos_dirent_t {
  char filename[8];     /* not null-terminated */
  char file_extension;
  uint16_t param1;
  uint16_t param2;
  uint8_t file_length;  /* in sectors */
  uint8_t start_sector; /* 0 to 15 */
  uint8_t start_track;  /* 0 to ? */
} trdos_dirent_t;


typedef struct trdos_boot_info_t {
  int have_boot_file;
  int basic_files_count;
  char first_basic_file[8]; /* not null-terminated */
} trdos_boot_info_t;


/* 1 RANDOMIZE USR 15619: REM : RUN "        " */
static uint8_t beta128_boot_loader[] = {
  0x00, 0x01, 0x1c, 0x00, 0xf9, 0xc0, 0x31, 0x35, 0x36, 0x31, 0x39, 0x0e,
  0x00, 0x00, 0x03, 0x3d, 0x00, 0x3a, 0xea, 0x3a, 0xf7, 0x22, 0x20, 0x20,
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x22, 0x0d,
};


//==========================================================================
//
//  trdos_read_spec
//
//==========================================================================
static int trdos_read_spec (trdos_spec_t *spec, const uint8_t *src) {
  if (*src) return -1;

  spec->first_free_sector = src[225];
  spec->first_free_track = src[226];
  spec->disk_type = src[227];
  spec->file_count = src[228];
  spec->free_sectors = src[229]+src[230]*0x100;
  spec->id = src[231];
  if (spec->id != 16) return -1;

  memcpy(spec->password, src+234, 9);
  spec->deleted_files = src[244];
  memcpy(spec->disk_label, src+245, 8);

  return 0;
}


//==========================================================================
//
//  trdos_write_spec
//
//==========================================================================
static void trdos_write_spec (uint8_t *dest, const trdos_spec_t *spec) {
  memset(dest, 0, 256);
  dest[225] = spec->first_free_sector;
  dest[226] = spec->first_free_track;
  dest[227] = spec->disk_type;
  dest[228] = spec->file_count;
  dest[229] = spec->free_sectors&0xff;
  dest[230] = spec->free_sectors>>8;
  dest[231] = spec->id;
  memcpy(dest+234, spec->password, 9);
  dest[244] = spec->deleted_files;
  memcpy(dest+245, spec->disk_label, 8);
}


//==========================================================================
//
//  trdos_read_dirent
//
//==========================================================================
static int trdos_read_dirent (trdos_dirent_t *entry, const uint8_t *src) {
  memcpy(entry->filename, src, 8);
  entry->file_extension = src[8];
  entry->param1 = src[9]+src[10]*0x100;
  entry->param2 = src[11]+src[12]*0x100;
  entry->file_length = src[13];
  entry->start_sector = src[14];
  entry->start_track = src[15];

  return (entry->filename[0] ? 0 : 1);
}


//==========================================================================
//
//  trdos_write_dirent
//
//==========================================================================
static void trdos_write_dirent (uint8_t *dest, const trdos_dirent_t *entry) {
  memcpy(dest, entry->filename, 8);
  dest[8] = entry->file_extension;
  dest[9] = entry->param1&0xff;
  dest[10] = entry->param1>>8;
  dest[11] = entry->param2&0xff;
  dest[12] = entry->param2>>8;
  dest[13] = entry->file_length;
  dest[14] = entry->start_sector;
  dest[15] = entry->start_track;
}


//==========================================================================
//
//  trdos_read_fat
//
//==========================================================================
static int trdos_read_fat (trdos_boot_info_t *info, const uint8_t *sectors, unsigned int seclen) {
  int i, j, error;
  trdos_dirent_t entry;
  const uint8_t *sector;

  info->have_boot_file = 0;
  info->basic_files_count = 0;

  memset(info->first_basic_file, 0, 8);

  /* FAT sectors */
  for (i = 0; i < 8; ++i) {
    sector = sectors+i*seclen*2; /* interleaved */

    /* Note: some TR-DOS versions like 5.04T have a turbo format with
       sequential sectors: 1, 2, 3, ..., 8, 9, 10, ...
       The SCL/TRD image formats can't specify a format mode and Fuse
       load the sectors as interleaved: 1, 9, 2, 10, 3, ...
    */

    /* FAT entries */
    for (j = 0; j < 16; ++j) {
      error = trdos_read_dirent(&entry, sector+j*16);
      if (error) return 0;
      /* Basic files */
      if (entry.filename[0] > 0x01 && entry.file_extension == 'B') {
        /* Boot file */
        if (!info->have_boot_file &&
            !memcmp((const char *)entry.filename, "boot    ", 8))
        {
          info->have_boot_file = 1;
        }
        /* ignore lower-case basic files */
        if ((entry.filename[0] >= 'A' && entry.filename[0] <= 'Z') ||
            (entry.filename[0] >= '0' && entry.filename[0] <= '9') ||
            entry.filename[0] == '_')
        {
          /* First basic program */
          if (info->basic_files_count == 0) {
            memcpy(info->first_basic_file, entry.filename, 8);
          }
          ++info->basic_files_count;
        }
      }
    }
  }

  return 0;
}


//==========================================================================
//
//  trdos_insert_basic_file
//
//==========================================================================
static int trdos_insert_basic_file (disk_t *d, trdos_spec_t *spec,
                                    const uint8_t *data, unsigned int size)
{
  unsigned int fat_sector, fat_entry, n_sec, n_bytes, n_copied;
  int i, t, s, slen, len_pre_dam, len_pre_data;
  const disk_gap_t *g = &gaps[GAP_TRDOS];
  trdos_dirent_t entry;
  uint8_t head[256];
  const uint8_t trailing_data[] = { 0x80, 0xaa, 0x01, 0x00 }; /* line 1 */

  /* Check free FAT entries (we don't purge deleted files) */
  if (spec->file_count >= 128) return DISK_UNSUP;

  /* Check free sectors */
  n_sec = (size+ARRAY_SIZE(trailing_data)+255)/256;
  if (spec->free_sectors < n_sec) return DISK_UNSUP;

  /* Calculate sector raw length */
  slen = calc_sectorlen((d->density != DISK_SD && d->density != DISK_8_SD), 256, GAP_TRDOS);

  /* Calculate initial gap before data in a sector */
  len_pre_dam = 0;
  len_pre_dam += g->sync_len+(g->mark >= 0 ? 3 : 0)+7; /* ID */
  len_pre_dam += g->len[2]; /* GAP II */

  len_pre_data = len_pre_dam;
  len_pre_data += g->sync_len+(g->mark >= 0 ? 3 : 0)+1; /* DAM */

  /* Write file data */
  n_copied = 0;
  s = spec->first_free_sector;
  t = spec->first_free_track;
  DISK_SET_TRACK_IDX(d, t);

  for (i = 0; i < n_sec; ++i) {
    memset(head, 0, 256);
    n_bytes = 0;

    /* Copy chunk of file body */
    if (n_copied < size) {
      n_bytes = (size-n_copied > 256 ? 256 : size-n_copied);
      memcpy(head, data+n_copied, n_bytes);
      n_copied += n_bytes;
    }

    /* Copy trailing parameters */
    if (n_copied >= size) {
      while (n_copied-size < ARRAY_SIZE(trailing_data) && n_bytes < 256) {
        head[n_bytes] = trailing_data[n_copied-size];
        ++n_copied;
        ++n_bytes;
      }
    }

    /* Write buffer to disk */
    d->pos.i = g->len[1]+(s%8*2+s/8)*slen; /* 1 9 2 10 3 ... */
    d->pos.i += len_pre_dam;
    data_add(d, NULL, head, 256, NO_DDAM, GAP_TRDOS, CRC_OK, NO_AUTOFILL, NULL);

    /* Next sector */
    s = (s+1)%16;

    /* Next track */
    if (s == 0) {
      ++t;
      if (t >= d->cylinders) return DISK_UNSUP;
      DISK_SET_TRACK_IDX(d, t);
    }
  }

  /* Write FAT entry */
  memcpy(entry.filename, "boot    ", 8);
  entry.file_extension = 'B';
  entry.param1 = size; /* assumes variables = 0 */
  entry.param2 = size;
  entry.file_length = n_sec;
  entry.start_sector = spec->first_free_sector;
  entry.start_track = spec->first_free_track;

  /* Copy sector to buffer, modify and write back to disk recalculating CRCs */
  DISK_SET_TRACK_IDX(d, 0);
  fat_sector = spec->file_count/16;
  d->pos.i = g->len[1]+(fat_sector%8*2+fat_sector/8)*slen;
  memcpy(head, d->pos.track+d->pos.i+len_pre_data, 256);

  fat_entry  = spec->file_count%16;
  trdos_write_dirent(head+fat_entry*16, &entry);

  d->pos.i += len_pre_dam;
  data_add(d, NULL, head, 256, NO_DDAM, GAP_TRDOS, CRC_OK, NO_AUTOFILL, NULL);

  /* Write specification sector */
  spec->file_count += 1;
  spec->free_sectors -= n_sec;
  spec->first_free_sector = s;
  spec->first_free_track = t;
  trdos_write_spec(head, spec);

  d->pos.i = g->len[1]+slen+len_pre_dam; /* sector-9: 1 9 2 10 3 ... */
  data_add(d, NULL, head, 256, NO_DDAM, GAP_TRDOS, CRC_OK, NO_AUTOFILL, NULL);

  return DISK_OK;
}


//==========================================================================
//
//  trdos_insert_boot_loader
//
//==========================================================================
static void trdos_insert_boot_loader (disk_t *d) {
  trdos_spec_t spec;
  trdos_boot_info_t info;
  int slen, del;

  /* TR-DOS specification sector */
  DISK_SET_TRACK_IDX(d, 0);
  if (!id_seek(d, 9) || !datamark_read(d, &del)) return;

  if (trdos_read_spec(&spec, d->pos.track+d->pos.i)) return;

  /* Check free FAT entries (we don't purge deleted files) */
  if (spec.file_count >= 128) return;

  /* Check there is at least one free sector */
  if (spec.free_sectors == 0) return;
  /* TODO: stealth mode? some boot loaders hide between sectors 10-16 */

  /* Calculate sector raw length */
  slen = calc_sectorlen((d->density != DISK_SD && d->density != DISK_8_SD), 256, GAP_TRDOS);

  /* Read FAT entries */
  if (!id_seek(d, 1) || !datamark_read(d, &del)) return;

  if (trdos_read_fat(&info, d->pos.track+d->pos.i, slen)) return;

  /* Check actual boot file (nothing to do) */
  if (info.have_boot_file) return;

  /* Insert a simple boot loader that runs the first program */
  if (info.basic_files_count >= 1) {
    memcpy(beta128_boot_loader+22, info.first_basic_file, 8);
    trdos_insert_basic_file(d, &spec, beta128_boot_loader, ARRAY_SIZE(beta128_boot_loader));
  }

  /* TODO: use also a boot loader that can handle multiple basic pograms */
}


//==========================================================================
//
//  open_trd
//
//==========================================================================
static int open_trd (buffer_t *buffer, disk_t *d, int preindex_unused) {
  int i, j, sectors, seclen;

  if (buffseek(buffer, 8*256, SEEK_CUR) == -1) return (d->status = DISK_OPEN);
  if (buffavail(buffer) < 256) return (d->status = DISK_OPEN);

  if (buff[231] != 0x10) return (d->status = DISK_OPEN); /*?*/

  if (buff[227] != 0 && (buff[227] < 0x16 || buff[227] > 0x19)) {
    //fprintf(stderr, "TDR: disk detection failed (#%02X #%02X).\n", buff[231], buff[227]);
    return (d->status = DISK_OPEN); /*?*/
  }

  /* guess geometry of disk */
  d->sides = (buff[227]&0x08 ? 1 : 2);
  d->cylinders = (buff[227]&0x01 ? 40 : 80);

  /* we have more tracks then on a standard disk... */
  if (buffer->file.length > d->sides*d->cylinders*16*256) {
    for (i = d->cylinders+1; i < 83; ++i) {
      if (d->sides*i*16*256 >= buffer->file.length) break;
    }
    d->cylinders = i;
  }
  sectors = 16;
  seclen = 256;

  /* create a DD disk */
  d->density = DISK_DD;
  if (disk_alloc(d) != DISK_OK) return d->status;

  buffer->index = 0;
  for (i = 0; i < d->cylinders; ++i) {
    for (j = 0; j < d->sides; ++j) {
      if (trackgen(d, buffer, j, i, 1, sectors, seclen,
                   NO_PREINDEX, GAP_TRDOS, INTERLEAVE_2, 0x00))
      {
        return (d->status = DISK_GEOM);
      }
    }
  }

  //FIXME
  if (opt_beta_autoload && buff[227] != 0) {
    trdos_insert_boot_loader(d);
  }

  return (d->status = DISK_OK);
}


//==========================================================================
//
//  write_trd
//
//==========================================================================
static int write_trd (FILE *file, disk_t *d) {
  int i, j, sbase, sectors, seclen, mfm, cyl;

  if (check_disk_geom(d, &sbase, &sectors, &seclen, &mfm, &cyl) ||
      sbase != 1 || seclen != 1 || sectors != 16)
  {
    return d->status = DISK_GEOM;
  }

  if (cyl == -1) cyl = d->cylinders;

  for (i = 0; i < cyl; ++i) {
    for (j = 0; j < d->sides; ++j) {
      if (savetrack(d, file, j, i, 1, sectors, seclen)) {
        return (d->status = DISK_GEOM);
      }
    }
  }

  return (d->status = DISK_OK);
}
