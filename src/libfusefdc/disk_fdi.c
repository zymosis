/* disk.c: Routines for handling disk images
   Copyright (c) 2007-2017 Gergely Szasz

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   Philip: philip-fuse@shadowmagic.org.uk

*/

//==========================================================================
//
//  open_fdi
//
//==========================================================================
static int open_fdi (buffer_t *buffer, disk_t *d, int preindex) {
  int i, j, h, gap;
  int bpt, bpt_fm, max_bpt = 0, max_bpt_fm = 0;
  int data_offset, track_offset, head_offset, sector_offset;
  uint8_t head[256];

  d->wrprot = (buff[0x03] == 1 ? 1 : 0);
  d->sides = buff[0x06]+256*buff[0x07];
  d->cylinders = buff[0x04]+256*buff[0x05];
  GEOM_CHECK;
  data_offset = buff[0x0a]+256*buff[0x0b];
  h = (0x0e)+buff[0x0c]+256*buff[0x0d]; /* save head start */
  head_offset = h;

  /* first determine the longest track */
  d->bpt = 0;
  for (i = 0; i < d->cylinders*d->sides; ++i) {
    /* ALT */
    buffer->index = head_offset;
    if (buffread(head, 7, buffer) != 1) return (d->status = DISK_OPEN); /* 7 := track head  */
    bpt = postindex_len(d, GAP_MINIMAL_MFM)+
          (preindex ? preindex_len(d, GAP_MINIMAL_MFM) : 0)+6; /* +gap4 */
    bpt_fm = postindex_len(d, GAP_MINIMAL_FM)+
             (preindex ? preindex_len(d, GAP_MINIMAL_FM) : 0)+3; /* +gap4 */
    /* calculate track len */
    for (j = 0; j < head[0x06]; ++j) {
      if (j%35 == 0) {
        /* 35-sector header */
        if (buffread(head+7, 245, buffer) != 1) {
          /* 7*35 := max 35 sector head */
          return d->status = DISK_OPEN;
        }
      }
      if ((head[0x0b+7*(j%35)]&0x3f) != 0) {
        bpt += calc_sectorlen(1, 0x80<<head[0x0a+7*(j%35)], GAP_MINIMAL_MFM);
        bpt_fm += calc_sectorlen(0, 0x80<<head[0x0a+7*(j%35)], GAP_MINIMAL_FM);
      }
    }
    if (bpt > max_bpt) max_bpt = bpt;
    if (bpt_fm > max_bpt_fm) max_bpt_fm = bpt_fm;
    head_offset += 7+7*head[0x06];
  }

  if (max_bpt == 0 || max_bpt_fm == 0) return (d->status = DISK_GEOM);

  d->density = DISK_DENS_AUTO; /* disk_alloc use d->bpt */
  if (max_bpt_fm < 3000) {
    /* we choose an SD disk with FM */
    d->bpt = max_bpt_fm;
    gap = GAP_MINIMAL_FM;
  } else {
    d->bpt = max_bpt;
    gap = GAP_MINIMAL_MFM;
  }
  if (disk_alloc(d) != DISK_OK) return d->status;

  /* start reading the tracks */
  head_offset = h; /* restore head start */
  for (i = 0; i < d->cylinders*d->sides; ++i) {
    /* ALT */
    buffer->index = head_offset;
    buffread(head, 7, buffer); /* 7 = track head */
    track_offset = head[0x00]+256*head[0x01]+65536*head[0x02]+16777216*head[0x03];
    DISK_SET_TRACK_IDX(d, i);
    d->pos.i = 0;

    if (preindex) preindex_add(d, gap);
    postindex_add(d, gap);

    for (j = 0; j < head[0x06]; ++j) {
      if (j%35 == 0) {
        /* if we have more than 35 sector in a track,
           we have to seek back to the next sector
           headers and read it (max 35 sector header) */
        buffer->index = head_offset+7*(j+1);
        buffread(head+7, 245, buffer); /* 7*35 := max 35 sector head */
      }
      id_add(d, head[0x08+7*(j%35)], head[0x07+7*(j%35)],
             head[0x09+7*(j%35)], head[0x0a+7*(j%35)], gap,
             (head[0x0b+7*(j%35)]&0x3f ? CRC_OK : CRC_ERROR));
      sector_offset = head[0x0c+7*(j%35)]+256*head[0x0d+7*(j%35)];
      buffer->index = data_offset+track_offset+sector_offset;
      data_add(d, buffer, NULL,
               ((head[0x0b+7*(j%35)]&0x3f) == 0 ? -1 : 0x80<<head[0x0a+7*(j%35)]),
               (head[0x0b+7*(j%35)]&0x80 ? DDAM : NO_DDAM),
               gap, CRC_OK, NO_AUTOFILL, NULL);
    }
    head_offset += 7+7*head[0x06];
    gap4_add(d, gap);
  }

  return (d->status = DISK_OK);
}


//==========================================================================
//
//  write_fdi
//
//==========================================================================
static int write_fdi (FILE *file, disk_t *d) {
  int i, j, k, sbase, sectors, seclen, mfm, del;
  int h, t, s, b;
  int toff, soff;
  uint8_t head[256];

  memset(head, 0, 14);
  memcpy(head, "FDI", 3);
  head[0x03] = d->wrprot = 1;
  head[0x04] = d->cylinders&0xff;
  head[0x05] = d->cylinders>>8;
  head[0x06] = d->sides&0xff;
  head[0x07] = d->sides>>8;
  sectors = 0;
  /* count sectors */
  for (j = 0; j < d->cylinders; ++j) {
    for (i = 0; i < d->sides; ++i) {
      guess_track_geom(d, i, j, &sbase, &s, &seclen, &mfm);
      sectors += s;
    }
  }
  h = (sectors+d->cylinders*d->sides)*7; /* track header len */
  head[0x08] = (h+0x0e)&0xff; /* description offset */
  head[0x09] = (h+0x0e)>>8; /* "http://fuse-emulator.sourceforge.net" */
  head[0x0a] = (h+0x33)&0xff; /* data offset */
  head[0x0b] = (h+0x33)>>8;
  /* FDI head */
  if (fwrite(head, 14, 1, file) != 1) return (d->status = DISK_WRPART);

  /* write track headers */
  toff = 0; /* offset of track data */
  for (i = 0; i < d->cylinders; ++i) {
    for (j = 0; j < d->sides; ++j) {
      DISK_SET_TRACK(d, j, i);
      d->pos.i = 0;
      head[0x00] = toff&0xff;
      head[0x01] = (toff>>8)&0xff; /* track offset */
      head[0x02] = (toff>>16)&0xff;
      head[0x03] = (toff>>24)&0xff;
      head[0x04] = 0;
      head[0x05] = 0;
      guess_track_geom(d, j, i, &sbase, &sectors, &seclen, &mfm);
      head[0x06] = sectors;
      /* track header */
      if (fwrite(head, 7, 1, file) != 1) return (d->status = DISK_WRPART);

      DISK_SET_TRACK(d, j, i);
      d->pos.i = 0;
      k = 0;
      soff = 0;
      while (sectors > 0) {
        while (k < 35 && id_read(d, &h, &t, &s, &b)) {
          head[0x00+k*7] = t;
          head[0x01+k*7] = h;
          head[0x02+k*7] = s;
          head[0x03+k*7] = b;
          head[0x05+k*7] = soff&0xff;
          head[0x06+k*7] = (soff>>8)&0xff;
          if (!datamark_read(d, &del)) {
            head[0x04+k*7] = 0; /* corrupt sector data */
          } else {
            head[0x04+k*7] = 1<<b|(del ? 0x80 : 0x00);
            soff += 0x80<<b;
          }
          ++k;
        }
        /* Sector header */
        if (fwrite(head, 7*k, 1, file) != 1) return (d->status = DISK_WRPART);
        sectors -= k;
        k = 0;
      }
      toff += soff;
    }
  }

  if (fwrite("http://fuse-emulator.sourceforge.net", 37, 1, file) != 1) {
    return (d->status = DISK_WRPART);
  }

  /* write data */
  for (i = 0; i < d->cylinders; ++i) {
    for (j = 0; j < d->sides; ++j) {
      if (saverawtrack(d, file, j, i)) return (d->status = DISK_WRPART);
    }
  }

  return (d->status = DISK_OK);
}
