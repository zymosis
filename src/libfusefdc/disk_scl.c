/* disk.c: Routines for handling disk images
   Copyright (c) 2007-2017 Gergely Szasz

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   Philip: philip-fuse@shadowmagic.org.uk

*/

//==========================================================================
//
//  open_scl
//
//==========================================================================
static int open_scl (buffer_t *buffer, disk_t *d, int preindex_unused) {
  int i, j, s, sectors, seclen;
  int scl_deleted, scl_files, scl_i;
  uint8_t head[256];

  d->sides = 2;
  d->cylinders = 80;
  d->density = DISK_DD;
  if (disk_alloc(d) != DISK_OK) return d->status;

  /*
   TR-DOS:
  - Root directory is 8 sectors long starting from track 0, sector 1
  - Max root entries are 128
  - Root entry dimension is 16 bytes (16*128 = 2048 => 8 sector
  - Logical sector(start from 0) 8th (9th physical) holds disc info
  - Logical sectors from 9 to 15 are unused
  - Files are *NOT* fragmented, and start on track 1, sector 1...
  */

  /* number of files */
  scl_files = buff[8];
  if (scl_files > 128 || scl_files < 1) return (d->status = DISK_GEOM); /* too many files */
  buffer->index = 9; /* read SCL entries */

  DISK_SET_TRACK_IDX(d, 0);
  d->pos.i = 0;
  postindex_add(d, GAP_TRDOS);
  scl_i = d->pos.i; /* the position of first sector */
  s = 1; /* start sector number */
  scl_deleted = 0; /* deleted files */
  sectors = 0; /* allocated sectors */
  /* we use 'head[]' to build TR-DOS directory */
  j = 0; /* index for head[] */
  memset(head, 0, 256);
  seclen = calc_sectorlen(1, 256, GAP_TRDOS); /* one sector raw length */
  /* read all entries and build TR-DOS dir */
  for (i = 0; i < scl_files; ++i) {
    if (buffread(head+j, 14, buffer) != 1) return (d->status = DISK_OPEN);
    head[j+14] = sectors%16; /* ( sectors + 16 ) % 16 := sectors % 16 starting sector */
    head[j+15] = sectors/16+1; /* ( sectors + 16 ) / 16 := sectors / 16 + 1 starting track */
    sectors += head[j+13];
    if (d->data[j] == 0x01) ++scl_deleted; /* deleted file */
    if (sectors > 16*159) return (d->status = DISK_MEM); /* too many sectors needed */ /* or DISK_GEOM??? */
    j += 16;
    if (j == 256) {
      /* one sector ready */
      d->pos.i = scl_i+((s-1)%8*2+(s-1)/8)*seclen; /* 1 9 2 10 3 ... */
      id_add(d, 0, 0, s, SECLEN_256, GAP_TRDOS, CRC_OK);
      data_add(d, NULL, head, 256, NO_DDAM, GAP_TRDOS, CRC_OK, NO_AUTOFILL, NULL);
      memset(head, 0, 256);
      ++s;
      j = 0;
    }
  }

  if (j != 0) {
    /* we have to add this sector  */
    d->pos.i = scl_i+((s-1)%8*2+(s-1)/8)*seclen; /* 1 9 2 10 3 ... */
    id_add(d, 0, 0, s, SECLEN_256, GAP_TRDOS, CRC_OK);
    data_add(d, NULL, head, 256, NO_DDAM, GAP_TRDOS, CRC_OK, NO_AUTOFILL, NULL);
    ++s;
  }

  /* and add empty sectors up to No. 16 */
  memset(head, 0, 256);
  /* finish the first track */
  for (; s <= 16; ++s) {
    d->pos.i = scl_i+((s-1)%8*2+(s-1)/8)*seclen; /* 1 9 2 10 3 ... */
    id_add(d, 0, 0, s, SECLEN_256, GAP_TRDOS, CRC_OK);
    if (s == 9) {
      /* TR-DOS descriptor */
      head[225] = sectors%16; /* first free sector */
      head[226] = sectors/16+1; /* first free track */
      head[227] = 0x16; /* 80 track 2 side disk */
      head[228] = scl_files; /* number of files */
      head[229] = (2544-sectors)%256; /* free sectors */
      head[230] = (2544-sectors)/256;
      head[231] = 0x10; /* TR-DOS ID byte */
      memset(head+234, 32, 9);
      head[244] = scl_deleted; /* number of deleted files */
      //memcpy(head+245, "FUSE-SCL", 8);
      memcpy(head+245, "        ", 8);
    }
    data_add(d, NULL, head, 256, NO_DDAM, GAP_TRDOS, CRC_OK, NO_AUTOFILL, NULL);
    if (s == 9) memset(head, 0, 256); /* clear sector data... */
  }
  gap4_add(d, GAP_TRDOS);

  /* now we continue with the data */
  for (i = 1; i < d->sides*d->cylinders; ++i) {
    if (trackgen(d, buffer, i%2, i/2, 1, 16, 256, NO_PREINDEX, GAP_TRDOS, INTERLEAVE_2, 0x00)) {
      return (d->status = DISK_OPEN);
    }
  }

  if (opt_beta_autoload) {
    trdos_insert_boot_loader(d);
  }

  return (d->status = DISK_OK);
}


//==========================================================================
//
//  write_scl
//
//==========================================================================
static int write_scl (FILE *file, disk_t *d) {
  if (!d) return DISK_OPEN;
  if (!file) return (d->status = DISK_WRFILE);

  int i, j, k, l, t, s, sbase, sectors, seclen, mfm, del, cyl;
  int entries;
  uint32_t sum = 597; /* sum of "SINCLAIR" */
  uint8_t head[256];

  if (check_disk_geom(d, &sbase, &sectors, &seclen, &mfm, &cyl) ||
      sbase != 1 || seclen != 1 || sectors != 16)
  {
    return (d->status = DISK_GEOM);
  }

  DISK_SET_TRACK_IDX(d, 0);

  /* TR-DOS descriptor */
  if (!id_seek(d, 9) || !datamark_read(d, &del)) return (d->status = DISK_GEOM);

  entries = head[8] = d->pos.track[d->pos.i+228]; /* number of files */

  if (entries > 128 || d->pos.track[d->pos.i+231] != 0x10 ||
      (d->pos.track[d->pos.i+227] != 0x16 && d->pos.track[d->pos.i+227] != 0x17 &&
       d->pos.track[d->pos.i+227] != 0x18 && d->pos.track[d->pos.i+227] != 0x19) ||
       d->pos.track[d->pos.i] != 0)
  {
    return d->status = DISK_GEOM;
  }

  memcpy(head, "SINCLAIR", 8);
  sum += entries;
  if (fwrite(head, 9, 1, file) != 1) return (d->status = DISK_WRPART);

  /* save SCL entries */
  j = 1; /* sector number */
  k = 0;
  sectors = 0;
  /* read TR-DOS dir */
  for (i = 0; i < entries; ++i) {
    if (j > 8) return (d->status = DISK_GEOM); /* TR-DOS dir max 8 sector len */
    if (k == 0 && (!id_seek(d, j) || !datamark_read(d, &del))) return (d->status = DISK_GEOM);
    if (fwrite(d->pos.track+d->pos.i+k, 14, 1, file) != 1) return (d->status = DISK_WRPART);
    sectors += d->pos.track[d->pos.i+k+13]; /* file length in sectors */
    for (s = 0; s < 14; ++s) {
      sum += d->pos.track[d->pos.i+k];
      ++k;
    }
    k += 2;
    if (k >= 256) {
      /* end of sector */
      ++j;
      k = 0;
    }
  }

  /* save data */
  /* we have to 'defragment' the disk :) */
  j = 1; /* sector number */
  k = 0; /* byte offset */
  /* read TR-DOS dir */
  for (i = 0; i < entries; ++i) {
    DISK_SET_TRACK_IDX(d, 0);
    if (k == 0) {
      if (!id_seek(d, j) || !datamark_read(d, &del)) return (d->status = DISK_GEOM);
      memcpy(head, d->pos.track+d->pos.i, 256);
    }

    s = head[k+14]; /* starting sector */
    t = head[k+15]; /* starting track */
    sectors = head[k+13]+s; /* last sector */
    k += 16;
    if (k == 256) {
      k = 0;
      ++j;
    }

    if (t >= d->sides*d->cylinders) return (d->status = DISK_GEOM);

    if (s%16 == 0) --t;
    DISK_SET_TRACK_IDX(d, t);

    /* save all sectors */
    for (; s < sectors; ++s) {
      if (s%16 == 0) {
        ++t;
        if (t >= d->sides*d->cylinders) return (d->status = DISK_GEOM);
        DISK_SET_TRACK_IDX(d, t);
      }
      if (id_seek(d, s%16+1)) {
        if (datamark_read(d, &del)) {
          /* write data if we have data */
          if (data_write_file(d, file, 1)) {
            return (d->status = DISK_GEOM);
          } else {
            for (l = 0; l < 256; ++l) sum += d->pos.track[d->pos.i+l];
          }
        }
      } else {
        return (d->status = DISK_GEOM);
      }
    }
  }

  head[0] = sum&0xff;
  head[1] = (sum>>8)&0xff;
  head[2] = (sum>>16)&0xff;
  head[3] = (sum>>24)&0xff;

  if (fwrite(head, 4, 1, file) != 1) return (d->status = DISK_WRPART);

  return (d->status = DISK_OK);
}
