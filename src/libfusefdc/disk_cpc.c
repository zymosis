/* disk.c: Routines for handling disk images
   Copyright (c) 2007-2017 Gergely Szasz

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   Philip: philip-fuse@shadowmagic.org.uk

*/

//==========================================================================
//
//  cpc_set_weak_range
//
//==========================================================================
static void cpc_set_weak_range (disk_t *d, int idx, buffer_t *buffer, int n, int len) {
  int i, j, first = -1, last = -1;
  uint8_t *t, *w;

  t = d->pos.track+idx;
  w = buffer->file.buffer+buffer->index;

  for (i = 0; i < len; ++i, ++t, ++w) {
    for (j = 0; j < n-1; ++j) {
      if (*t != w[j*len]) {
        if (first == -1) first = idx+i;
        last = idx+i;
      }
    }
  }

  if (first == -1 || last == -1) return;

  for (; first <= last; ++first) {
    bitmap_set(d->pos.weak, first);
  }
}


#define CPC_ISSUE_NONE  (0)
#define CPC_ISSUE_1     (1)
#define CPC_ISSUE_2     (2)
#define CPC_ISSUE_3     (3)
#define CPC_ISSUE_4     (4)
#define CPC_ISSUE_5     (5)


//==========================================================================
//
//  open_cpc
//
//==========================================================================
static int open_cpc (buffer_t *buffer, disk_t *d, int preindex) {
  int i, j, seclen, idlen, gap, sector_pad, idx;
  int bpt, max_bpt = 0, trlen;
  int fix[84], plus3_fix;
  unsigned char *hdrb;

  d->sides = buff[0x31];
  d->cylinders = buff[0x30]; /* maximum number of tracks */
  GEOM_CHECK;
  buffer->index = 256;
  /* first scan for the longest track */
  for (i = 0; i < d->sides*d->cylinders; ++i) {
    /* ignore Sector Offset block */
    if (buffavail(buffer) >= 13 && memcmp(buff, "Offset-Info\r\n", 13) == 0) {
      buffer->index = buffer->file.length;
    }

    /* sometimes in the header there are more track than in the file */
    if (buffavail(buffer) == 0) {
      /* no more data */
      d->cylinders = i/d->sides+i%d->sides; /* the real cylinder number */
      break;
    }

    /* check track header */
    if (buffavail(buffer) < 256 || memcmp(buff, "Track-Info", 10) != 0) {
      return (d->status = DISK_OPEN);
    }

    gap = ((unsigned char)buff[0x16] == 0xff ? GAP_MINIMAL_FM : GAP_MINIMAL_MFM);
    plus3_fix = trlen = 0;
    while (i < buff[0x10]*d->sides+buff[0x11]) {
      if (i < 84) fix[i] = 0;
      ++i;
    }

    if (i >= d->sides*d->cylinders || i != buff[0x10]*d->sides+buff[0x11]) {
      /* problem with track idx */
      return (d->status = DISK_OPEN);
    }

    bpt = postindex_len(d, gap)+(preindex ? preindex_len(d, gap) : 0)+(gap == GAP_MINIMAL_MFM ? 6 : 3); /* gap4 */
    sector_pad = 0;
    for (j = 0; j < buff[0x15]; ++j) {
      /* each sector */
      seclen = (d->type == DISK_ECPC ? buff[(0x1e)+8*j]+256*buff[0x1f+8*j] : 0x80<<buff[0x1b+8*j]);
      idlen = 0x80<<buff[0x1b+8*j]; /* sector length from ID */
      if (idlen != 0 && idlen <= (0x80<<0x08) && /* idlen is o.k. */
          seclen > idlen && seclen%idlen)
      {
        /* seclen != N * len */
        return (d->status = DISK_OPEN);
      }

      bpt += calc_sectorlen((gap == GAP_MINIMAL_MFM ? 1 : 0), (seclen > idlen ? idlen : seclen),
                            gap);

      if (i < 84 && (d->flag&DISK_FLAG_PLUS3_CPC)) {
             if (j == 0 && buff[0x1b+8*j] == 6 && seclen > 6144) plus3_fix = CPC_ISSUE_4;
        else if (j == 0 && buff[0x1b+8*j] == 6) plus3_fix = CPC_ISSUE_1;
        else if (j == 0 && buff[0x18+8*j] == j && buff[0x19+8*j] == j &&
                 buff[0x1a+8*j] == j && buff[0x1b+8*j] == j) plus3_fix = CPC_ISSUE_3;
        else if (j == 1 && plus3_fix == CPC_ISSUE_1 && buff[0x1b+8*j] == 2) plus3_fix = CPC_ISSUE_2;
        else if (i == 38 && j == 0 && buff[0x1b+8*j] == 2) plus3_fix = CPC_ISSUE_5;
        else if (j > 1 && plus3_fix == CPC_ISSUE_2 && buff[0x1b+8*j] != 2) plus3_fix = CPC_ISSUE_NONE;
        else if (j > 0 && plus3_fix == CPC_ISSUE_3 &&
                 (buff[0x18+8*j] != j || buff[0x19+8*j] != j ||
                  buff[0x1a+8*j] != j || buff[0x1b+8*j] != j)) plus3_fix = CPC_ISSUE_NONE;
        else if (j > 10 && plus3_fix == CPC_ISSUE_2) plus3_fix = CPC_ISSUE_NONE;
        else if (i == 38 && j > 0 && plus3_fix == CPC_ISSUE_5 && buff[0x1b+8*j] != 2-(j&1)) plus3_fix = CPC_ISSUE_NONE;
      }

      trlen += seclen;
      if (seclen%0x100) ++sector_pad; /* every? 128/384/...byte length sector is padded */
    }

    if (i < 84) {
      fix[i] = plus3_fix;
           if (fix[i] == CPC_ISSUE_4) bpt = 6500; /* Type 1 variant DD+ (e.g. Coin Op Hits) */
      else if (fix[i] != CPC_ISSUE_NONE) bpt = 6250; /* we assume a standard DD track */
    }

    buffer->index += trlen+sector_pad*128+256;
    if (bpt > max_bpt) max_bpt = bpt;
  }

  if (max_bpt == 0) return (d->status = DISK_GEOM);

  d->density = DISK_DENS_AUTO; /* disk_alloc use d->bpt */
  d->bpt = max_bpt;
  if (disk_alloc(d) != DISK_OK) return d->status;

  DISK_SET_TRACK_IDX(d, 0);
  buffer->index = 256; /* rewind to first track */
  for (i = 0; i < d->sides*d->cylinders; ++i) {
    hdrb = buff;
    buffer->index += 256; /* skip to data */
    gap = ((unsigned char)hdrb[0x16] == 0xff ? GAP_MINIMAL_FM : GAP_MINIMAL_MFM);

    i = hdrb[0x10]*d->sides+hdrb[0x11]; /* adjust track No. */
    DISK_SET_TRACK_IDX(d, i);
    d->pos.i = 0;
    if (preindex) preindex_add(d, gap);
    postindex_add(d, gap);

    sector_pad = 0;
    /* each sector */
    for (j = 0; j < hdrb[0x15]; ++j) {
      seclen = d->type == DISK_ECPC ? hdrb[(0x1e)+8*j]+256*hdrb[0x1f+8*j] : /* data length in sector */
               0x80<<hdrb[0x1b+8*j];
      idlen = 0x80<<hdrb[0x1b+8*j]; /* sector length from ID */

      if (idlen == 0 || idlen > (0x80<<0x08)) idlen = seclen; /* error in sector length code -> ignore */

      if (i < 84 && fix[i] == 2 && j == 0) d->pos.i = 8; /* repositionate the dummy track  */

      id_add(d, hdrb[0x19+8*j], hdrb[0x18+8*j], hdrb[0x1a+8*j], hdrb[0x1b+8*j], gap,
             ((hdrb[0x1c+8*j]&0x20) && (hdrb[0x1d+8*j]&0x20) == 0 ? CRC_ERROR : CRC_OK));

      if (i < 84 && fix[i] == CPC_ISSUE_1 && j == 0) {
        /* 6144 */
        data_add(d, buffer, NULL, seclen, (hdrb[0x1d+8*j]&0x40 ? DDAM : NO_DDAM), gap,
                 ((hdrb[0x1c+8*j]&0x20) && (hdrb[0x1d+8*j]&0x20) ? CRC_ERROR : CRC_OK),
                 0x00, NULL);
      } else if (i < 84 && fix[i] == CPC_ISSUE_2 && j == 0) {
        /* 6144, 10x512 */
        datamark_add(d, (hdrb[0x1d+8*j]&0x40 ? DDAM : NO_DDAM), gap);
        gap_add(d, 2, gap);
        buffer->index += seclen;
      } else if (i < 84 && fix[i] == CPC_ISSUE_3) {
        /* 128, 256, 512, ... 4096k */
        data_add(d, buffer, NULL, 128, (hdrb[0x1d+8*j]&0x40 ? DDAM : NO_DDAM), gap,
                 ((hdrb[0x1c+8*j]&0x20) && (hdrb[0x1d+8*j]&0x20) ? CRC_ERROR : CRC_OK),
                 0x00, NULL);
        buffer->index += seclen-128;
      } else if (i < 84 && fix[i] == CPC_ISSUE_4) {
        /* Nx8192 (max 6384 bytes) */
        data_add(d, buffer, NULL, 6384, (hdrb[0x1d+8*j]&0x40 ? DDAM : NO_DDAM), gap,
                 ((hdrb[0x1c+8*j]&0x20) && (hdrb[0x1d+8*j]&0x20) ? CRC_ERROR : CRC_OK),
                 0x00, NULL);
        buffer->index += seclen-6384;
      } else if (i < 84 && fix[i] == CPC_ISSUE_5) {
        /* 9x512 */
        /* 512 256 512 256 512 256 512 256 512 */
        if (idlen == 256) {
          data_add(d, NULL, buff, 512, (hdrb[0x1d+8*j]&0x40 ? DDAM : NO_DDAM), gap,
                   ((hdrb[0x1c+8*j]&0x20) && (hdrb[0x1d+8*j]&0x20) ? CRC_ERROR : CRC_OK),
                   0x00, NULL);
          buffer->index += idlen;
        } else {
          data_add(d, buffer, NULL, idlen, (hdrb[0x1d+8*j]&0x40 ? DDAM : NO_DDAM), gap,
                   ((hdrb[0x1c+8*j]&0x20) && (hdrb[0x1d+8*j]&0x20) ? CRC_ERROR : CRC_OK),
                   0x00, NULL);
        }
      } else {
        data_add(d, buffer, NULL, (seclen > idlen ? idlen : seclen),
                 (hdrb[0x1d+8*j]&0x40 ? DDAM : NO_DDAM), gap,
                 ((hdrb[0x1c+8*j]&0x20) && (hdrb[0x1d+8*j]&0x20) ? CRC_ERROR : CRC_OK),
                 0x00, &idx);
        if (seclen > idlen) {
          /* weak sector with multiple copy */
          cpc_set_weak_range(d, idx, buffer, seclen/idlen, idlen);
          buffer->index += (seclen/idlen-1)*idlen;
          /* ( ( N * len ) / len - 1 ) * len */
        }
      }
      if (seclen%0x100) ++sector_pad; /* every? 128/384/...byte length sector is padded */
    }
    gap4_add(d, gap);
    buffer->index += sector_pad*0x80;
  }

  return (d->status = DISK_OK);
}


//==========================================================================
//
//  write_cpc
//
//==========================================================================
static int write_cpc (FILE *file, disk_t *d) {
  int i, j, k, sbase, sectors, seclen, mfm, cyl;
  int h, t, s, b;
  size_t len;
  uint8_t head[256];

  i = check_disk_geom(d, &sbase, &sectors, &seclen, &mfm, &cyl);
  if ((i&DISK_SECLEN_VARI) || (i&DISK_SPT_VARI) || (i&DISK_WEAK_DATA)) {
    return (d->status = DISK_GEOM);
  }

  if (i&DISK_MFM_VARI) mfm = -1;
  if (cyl == -1) cyl = d->cylinders;

  memset(head, 0, 256);
  memcpy(head, "MV - CPCEMU Disk-File\r\nDisk-Info\r\n", 34);
  head[0x30] = cyl;
  head[0x31] = d->sides;
  len = sectors*(0x80<<seclen)+256;
  head[0x32] = len&0xff;
  head[0x33] = len>>8;
  /* CPC head */
  if (fwrite(head, 256, 1, file) != 1) return (d->status = DISK_WRPART);

  memset(head, 0, 256);
  memcpy(head, "Track-Info\r\n", 12);
  for (i = 0; i < cyl; ++i) {
    for (j = 0; j < d->sides; ++j) {
      DISK_SET_TRACK(d, j, i);
      d->pos.i = 0;
      head[0x10] = i;
      head[0x11] = j;
      head[0x14] = seclen;
      head[0x15] = sectors;
      if (mfm != -1) head[0x16] = (mfm ? 0x4e : 0xff);
      head[0x17] = 0xe5;
      k = 0;
      while (id_read(d, &h, &t, &s, &b)) {
        head[0x18+k*8] = t;
        head[0x19+k*8] = h;
        head[0x1a+k*8] = s;
        head[0x1b+k*8] = b;
        if (k == 0 && mfm == -1) {
          /* if mixed MFM/FM tracks */
          head[0x16] = (d->pos.track[d->pos.i] == 0x4e ? 0x4e : 0xff);
        }
        ++k;
      }
      /* Track head */
      if (fwrite(head, 256, 1, file) != 1) return (d->status = DISK_WRPART);

      if (saverawtrack(d, file, j, i)) return (d->status = DISK_WRPART);
    }
  }

  return (d->status = DISK_OK);
}
