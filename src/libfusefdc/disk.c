/* disk.c: Routines for handling disk images
   Copyright (c) 2007-2017 Gergely Szasz

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   Philip: philip-fuse@shadowmagic.org.uk

*/
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifndef LIBFUSE_DISABLE_MINIZ
# include "miniz/miniz.h"
# include "miniz/miniz.c"
#endif

#include "crc.h"
#include "disk.h"


#include <stdarg.h>


void (*libfdcMessageCB) (int type, const char *msg) = NULL;


#define MSGBUF_SIZE  (1024)
static __thread char lfdcmsgbuf[MSGBUF_SIZE];


//==========================================================================
//
//  libfdcMsg
//
//==========================================================================
__attribute__((format(printf,2,3))) void libfdcMsg (int type, const char *fmt, ...) {
  if (!libfdcMessageCB) return;
  va_list va;
  va_start(va, fmt);
  vsnprintf(lfdcmsgbuf, sizeof(lfdcmsgbuf), fmt, va);
  lfdcmsgbuf[sizeof(lfdcmsgbuf)-1] = 0;
  va_end(va);
  libfdcMessageCB(type, lfdcmsgbuf);
}


/* The ordering of these strings must match the order of the
 * disk_error_t enumeration in disk.h */
static const char * const disk_error[] = {
  "OK",                         /* DISK_OK */
  "invalid function arguments", /* DISK_BADARGS */
  "feature not implemented",    /* DISK_IMPL */
  "out of memory",              /* DISK_MEM */
  "invalid disk geometry",      /* DISK_GEOM */
  "cannot open disk image",     /* DISK_OPEN */
  "unsupported file feature",   /* DISK_UNSUP */
  "read only disk",             /* DISK_RDONLY */
  "cannot close file",          /* DISK_CLOSE */
  "cannot write disk image",    /* DISK_WRFILE */
  "partially written file",     /* DISK_WRPART */

  "invalid head",               /* DISK_INVALID_HEAD */
  "cylinder not found",         /* DISK_NO_CYLINDER */
  "sector not found",           /* DISK_NO_SECTOR */
  "invalid sector data",        /* DISK_BAD_SECTOR */
  "sector too big",             /* DISK_SECTOR_TOO_BIG */
  "write error",                /* DISK_WRITE_ERROR */

  "unknown error code"          /* DISK_LAST_ERROR */
};


static const int disk_bpt[] = {
  6250,  /* AUTO assumes DD */
  5208,  /* 8" SD */
  10416, /* 8" DD */
  3125,  /* SD */
  6250,  /* DD */
  6500,  /* DD+ e.g. Coin Op Hits */
  12500, /* HD */
};


typedef struct disk_gap_t {
  int gap;      /* gap byte */
  int sync;     /* sync byte */
  int sync_len;
  int mark;     /* mark byte 0xa1 for MFM -1 for MF */
  int len[4];
} disk_gap_t;

static const disk_gap_t gaps[] = {
  { 0x4e, 0x00, 12, 0xa1, {  0, 60, 22, 24 } }, /* MGT MFM */
  { 0x4e, 0x00, 12, 0xa1, {  0, 10, 22, 60 } }, /* TRD MFM */
  { 0xff, 0x00,  6, -1,   { 40, 26, 11, 27 } }, /* IBM3740 FM */
  { 0x4e, 0x00, 12, 0xa1, { 80, 50, 22, 54 } }, /* IBM34 MFM */
  { 0xff, 0x00,  6, -1,   {  0, 16, 11, 10 } }, /* MINIMAL FM */
  { 0x4e, 0x00, 12, 0xa1, {  0, 32, 22, 24 } }, /* MINIMAL MFM */
};


static inline void bitmap_set (uint8_t *b, const uint32_t n) { b[n/8] |= (1<<(n%8)); }
static inline void bitmap_reset (uint8_t *b, const uint32_t n) { b[n/8] &= ~(1<<(n%8)); }
static inline uint8_t bitmap_test (const uint8_t *b, const uint32_t n) { return b[n/8]&(1<<(n%8));}


/* insert simple TR-DOS autoloader? */
int opt_beta_autoload = 0; /* default is 1 */


#define GAP_MGT_PLUSD    (0)
#define GAP_TRDOS        (1)
#define GAP_IBM3740      (2)
#define GAP_IBM34        (3)
#define GAP_MINIMAL_FM   (4)
#define GAP_MINIMAL_MFM  (5)

#define buffavail(buffer)  (buffer->file.length-buffer->index)
/* data buffer */
#define buff  (buffer->file.buffer+buffer->index)

#define ARRAY_SIZE(a)  ((sizeof(a)/sizeof(*a)))


typedef struct utils_file {
  uint8_t *buffer;
  size_t length;
} utils_file;


#define compat_fd  FILE *

static const compat_fd COMPAT_FILE_OPEN_FAILED = NULL;


//==========================================================================
//
//  compat_file_open
//
//==========================================================================
static compat_fd compat_file_open (const char *path, int write) {
  struct stat statbuf;

  if (!write) {
    if (stat(path, &statbuf)) return NULL;
    /* Check file type */
    if (!S_ISREG(statbuf.st_mode)) {
      errno = EINVAL;
      return NULL;
    }
  }

  return fopen(path, write ? "wb" : "rb");
}


//==========================================================================
//
//  compat_file_get_length
//
//==========================================================================
static off_t compat_file_get_length (compat_fd fd) {
  struct stat file_info;

  if (fstat(fileno(fd), &file_info)) {
    fprintf(stderr, "ERROR: couldn't stat file: %s\n", strerror(errno));
    return -1;
  }

  return file_info.st_size;
}


//==========================================================================
//
//  compat_file_read
//
//==========================================================================
static int compat_file_read (compat_fd fd, utils_file *file) {
  size_t bytes = fread(file->buffer, 1, file->length, fd);
  if (bytes != file->length) {
    fprintf(stderr, "error reading file: expected %lu bytes, but read only %lu\n",
            (unsigned long)file->length, (unsigned long)bytes);
    return 1;
  }
  return 0;
}


//==========================================================================
//
//  compat_file_write
//
//==========================================================================
/*
static int compat_file_write (compat_fd fd, const unsigned char *buffer, size_t length) {
  size_t bytes = fwrite( buffer, 1, length, fd );
  if (bytes != length) {
    fprintf(stderr, "error writing file: expected %lu bytes, but wrote only %lu\n",
            (unsigned long)length, (unsigned long)bytes );
    return 1;
  }
  return 0;
}
*/


//==========================================================================
//
//  compat_file_close
//
//==========================================================================
static int compat_file_close (compat_fd fd) {
  return fclose(fd);
}


//==========================================================================
//
//  compat_file_exists
//
//==========================================================================
/*
static int compat_file_exists (const char *path) {
  return (access( path, R_OK) != -1);
}
*/


//==========================================================================
//
//  utils_read_fd
//
//==========================================================================
static int utils_read_fd (compat_fd fd, const char *filename, utils_file *file) {
  file->length = compat_file_get_length(fd);
  if (file->length == -1) return 1;

  file->buffer = malloc(file->length+1);

  if (compat_file_read(fd, file)) {
    free(file->buffer);
    compat_file_close(fd);
    return 1;
  }

  if (compat_file_close(fd)) {
    fprintf(stderr, "Couldn't close '%s': %s\n", filename, strerror(errno));
    free(file->buffer);
    return 1;
  }

  return 0;
}


//==========================================================================
//
//  utils_read_file
//
//==========================================================================
static int utils_read_file (const char *filename, utils_file *file) {
  compat_fd fd = compat_file_open(filename, 0);

  if (fd == COMPAT_FILE_OPEN_FAILED) {
    fprintf(stderr, "couldn't open '%s': %s\n", filename, strerror(errno));
    return 1;
  }

  int error = utils_read_fd(fd, filename, file);
  if (error) return error;

  return 0;
}


//==========================================================================
//
//  utils_close_file
//
//==========================================================================
static void utils_close_file (utils_file *file) {
  free(file->buffer);
}


// ////////////////////////////////////////////////////////////////////////// //
typedef struct buffer_t {   /* to store buffer data */
  utils_file file;      /* buffer, length */
  size_t index;
} buffer_t;


//==========================================================================
//
//  disk_strerror
//
//==========================================================================
const char *disk_strerror (int error) {
  if (error < 0 || error > DISK_LAST_ERROR) error = DISK_LAST_ERROR;
  return disk_error[error];
}


//==========================================================================
//
//  buffread
//
//==========================================================================
static int buffread (void *data, size_t len, buffer_t *buffer) {
  if (len > buffer->file.length-buffer->index) return 0;
  memcpy(data, buffer->file.buffer+buffer->index, len);
  buffer->index += len;
  return 1;
}


//==========================================================================
//
//  buffread_u8
//
//==========================================================================
static uint8_t buffread_u8 (buffer_t *buffer) {
  if (buffer->file.length-buffer->index < 1) return 0;
  return buffer->file.buffer[buffer->index++];
}


//==========================================================================
//
//  buffread_u16
//
//==========================================================================
static uint16_t buffread_u16 (buffer_t *buffer) {
  if (buffer->file.length-buffer->index < 2) return 0;
  const uint16_t w = buffer->file.buffer[buffer->index+0]|
                     (buffer->file.buffer[buffer->index+1]<<8);
  buffer->index += 2;
  return w;
}


//==========================================================================
//
//  buffseek
//
//==========================================================================
static int buffseek (buffer_t *buffer, long offset, int whence) {
  if (whence == SEEK_CUR) offset += buffer->index;
  if (offset >= buffer->file.length) return -1;
  buffer->index = offset;
  return 0;
}


//==========================================================================
//
//  id_read
//
//==========================================================================
static int id_read (disk_t *d, int *head, int *track, int *sector, int *length) {
  int a1mark = 0;
  while (d->pos.i < d->bpt) {
    if (d->pos.track[d->pos.i] == 0xa1 && bitmap_test(d->pos.clocks, d->pos.i)) {
      /* 0xa1 with clock */
      a1mark = 1;
    } else if (d->pos.track[d->pos.i] == 0xfe &&
               (a1mark || /* 0xfe with 0xa1 */
                bitmap_test(d->pos.clocks, d->pos.i))) /* 0xfe with clock */
    {
      ++d->pos.i;
      #if 0
      fprintf(stderr, "***i=%d/%d; THSL=%u:%u:%u:%u\n", d->pos.i, d->bpt,
                       d->pos.track[d->pos.i], d->pos.track[d->pos.i+1],
                       d->pos.track[d->pos.i+2], d->pos.track[d->pos.i+3]);
      #endif
      *track  = d->pos.track[d->pos.i++];
      *head   = d->pos.track[d->pos.i++];
      *sector = d->pos.track[d->pos.i++];
      *length = d->pos.track[d->pos.i++];
      d->pos.i += 2; /* skip CRC */
      return 1;
    } else {
      a1mark = 0;
    }
    ++d->pos.i;
  }
  #if 0
  fprintf(stderr, "...OOPS!\n");
  #endif
  return 0;
}


//==========================================================================
//
//  datamark_read
//
//==========================================================================
static int datamark_read (disk_t *d, int *deleted) {
  int a1mark = 0;
  while (d->pos.i < d->bpt) {
    if (d->pos.track[d->pos.i] == 0xa1 && bitmap_test(d->pos.clocks, d->pos.i)) {
      /* 0xa1 with clock */
      a1mark = 1;
    } else if (d->pos.track[d->pos.i] >= 0xf8 && d->pos.track[d->pos.i] <= 0xfe &&
               (bitmap_test(d->pos.clocks, d->pos.i) || a1mark))
    {
      /* 0xfe with clock or 0xfe after 0xa1 mark */
      *deleted = (d->pos.track[d->pos.i] == 0xf8 ? 1 : 0);
      ++d->pos.i;
      return 1;
    } else {
      a1mark = 0;
    }
    ++d->pos.i;
  }
  return 0;
}


//==========================================================================
//
//  datamark_read_ex
//
//==========================================================================
static int datamark_read_ex (disk_t *d, uint8_t *dmb) {
  int a1mark = 0;
  while (d->pos.i < d->bpt) {
    if (d->pos.track[d->pos.i] == 0xa1 && bitmap_test(d->pos.clocks, d->pos.i)) {
      /* 0xa1 with clock */
      a1mark = 1;
    } else if (d->pos.track[d->pos.i] >= 0xf8 && d->pos.track[d->pos.i] <= 0xfe &&
               (bitmap_test(d->pos.clocks, d->pos.i) || a1mark))
    {
      /* 0xfe with clock or 0xfe after 0xa1 mark */
      *dmb = d->pos.track[d->pos.i];
      ++d->pos.i;
      return 1;
    } else {
      a1mark = 0;
    }
    ++d->pos.i;
  }
  return 0;
}


//==========================================================================
//
//  id_seek
//
//==========================================================================
static int id_seek (disk_t *d, int sector) {
  int h, t, s, b;
  d->pos.i = 0; /* start of the track */
  while (id_read(d, &h, &t, &s, &b)) {
    if (s == sector) return 1;
  }
  return 0;
}


//==========================================================================
//
//  id_seek_ex
//
//  returns sector size code, or -1
//
//==========================================================================
static int id_seek_ex (disk_t *d, int head, int cyl, int sector) {
  int h, t, s, b;
  d->pos.i = 0; /* start of the track */
  while (id_read(d, &h, &t, &s, &b)) {
    if (head == h && cyl == t && s == sector) return b;
  }
  return -1;
}


//==========================================================================
//
//  data_write_file
//
//  seclen 00 -> 128, 01 -> 256 ... byte
//
//==========================================================================
static int data_write_file (disk_t *d, FILE *file, int seclen) {
  int len = 0x80<<seclen;
  if (fwrite(&d->pos.track[d->pos.i], len, 1, file) != 1) return 1;
  return 0;
}


//==========================================================================
//
//  savetrack
//
//==========================================================================
static int savetrack (disk_t *d, FILE *file, int head, int track,
                      int sector_base, int sectors, int seclen)
{
  int s;
  int del;

  DISK_SET_TRACK(d, head, track);
  d->pos.i = 0;
  for (s = sector_base; s < sector_base+sectors; ++s) {
    if (id_seek(d, s)) {
      if (datamark_read(d, &del)) {
        /* write data if we have data */
        if (data_write_file(d, file, seclen)) return 1;
      }
    } else {
      return 1;
    }
  }
  return 0;
}


//==========================================================================
//
//  saverawtrack
//
//==========================================================================
static int saverawtrack (disk_t *d, FILE *file, int head, int track) {
  int h, t, s, seclen;
  int del;

  DISK_SET_TRACK(d, head, track);
  d->pos.i = 0;
  while (id_read(d, &h, &t, &s, &seclen)) {
    if (datamark_read(d, &del)) {
      /* write data if we have data */
      if (data_write_file(d, file, seclen)) return 1;
    }
  }

  return 0;
}


#define DISK_ID_NOTMATCH        (1)
#define DISK_SECLEN_VARI        (2)
#define DISK_SPT_VARI           (4)
#define DISK_SBASE_VARI         (8)
#define DISK_MFM_VARI           (16)
#define DISK_DDAM               (32)
#define DISK_CORRUPT_SECTOR     (64)
#define DISK_UNFORMATTED_TRACK  (128)
#define DISK_FM_DATA            (256)
#define DISK_WEAK_DATA          (512)


//==========================================================================
//
//  guess_track_geom
//
//==========================================================================
static int guess_track_geom (disk_t *d, int head, int track, int *sector_base,
                             int *sectors, int *seclen, int *mfm)
{
  int r = 0;
  int h, t, s, sl;
  int del = 0;
  *sector_base = -1;
  *sectors = 0;
  *seclen = -1;
  *mfm = -1;

  DISK_SET_TRACK(d, head, track);
  d->pos.i = 0;
  while (id_read(d, &h, &t, &s, &sl)) {
    if (*sector_base == -1) *sector_base = s;
    if (*seclen == -1) *seclen = sl;
    if (*mfm == -1) *mfm = (d->pos.track[d->pos.i] == 0x4e ? 1 : 0); /* not so robust */
    if (!datamark_read(d, &del)) r |= DISK_CORRUPT_SECTOR;
    if (t != track) r |= DISK_ID_NOTMATCH;
    if (s < *sector_base) *sector_base = s;
    if (sl != *seclen) {
      r |= DISK_SECLEN_VARI;
      if (sl > *seclen) *seclen = sl;
    }
    if (del) r |= DISK_DDAM;
    *sectors += 1;
  }

  return r;
}


//==========================================================================
//
//  update_tracks_mode
//
//==========================================================================
static void update_tracks_mode (disk_t *d) {
  int i, j, bpt;
  int mfm, fm, weak;
  for (i = 0; i < d->cylinders*d->sides; ++i) {
    DISK_SET_TRACK_IDX(d, i);
    mfm = 0, fm = 0, weak = 0;
    bpt = d->pos.track[-3]+256*d->pos.track[-2];
    for (j = DISK_CLEN(bpt)-1; j >= 0; --j) {
      mfm  |= ~d->pos.fm[j];
      fm   |= d->pos.fm[j];
      weak |= d->pos.weak[j];
    }
    if (mfm && !fm) d->pos.track[-1] = 0x00;
    if (!mfm && fm) d->pos.track[-1] = 0x01;
    if (mfm && fm) d->pos.track[-1] = 0x02;
    if (weak) {
      d->pos.track[-1] |= 0x80;
      d->have_weak = 1;
    }
  }
}


//==========================================================================
//
//  check_disk_geom
//
//==========================================================================
static int check_disk_geom (disk_t *d, int *sector_base, int *sectors,
                            int *seclen, int *mfm, int *unf)
{
  int h, t, s, slen, sbase, m;
  int r = 0;

  DISK_SET_TRACK_IDX(d, 0);
  d->pos.i = 0;
  *sector_base = -1;
  *sectors = -1;
  *seclen = -1;
  *mfm = -1;
  *unf = -1;
  for (t = 0; t < d->cylinders; ++t) {
    for (h = 0; h < d->sides; ++h) {
      r |= ((d->pos.track[-1]&0x80) ? DISK_WEAK_DATA : 0);
      r |= ((d->pos.track[-1]&0x03) == 0x02 ? DISK_MFM_VARI : 0);
      r |= ((d->pos.track[-1]&0x03) == 0x01 ? DISK_FM_DATA : 0);
      r |= guess_track_geom(d, h, t, &sbase, &s, &slen, &m);
      if (*sector_base == -1) *sector_base = sbase;
      if (*sectors == -1) *sectors = s;
      if (*seclen == -1) *seclen = slen;
      if (*mfm == -1) *mfm = m;
      if (sbase == -1) {
        /* unformatted */
        if (*unf == -1 && h > 0) *unf = -2;
        if (*unf == -1) *unf = t;
        continue;
      }
      if (*unf > -1) *unf = -2;
      if (sbase != *sector_base) {
        r |= DISK_SBASE_VARI;
        if (sbase < *sector_base) *sector_base = sbase;
      }
      if (s != *sectors) {
        r |= DISK_SPT_VARI;
        if (s > *sectors) *sectors = s;
      }
      if (slen != *seclen) {
        r |= DISK_SECLEN_VARI;
        if (slen > *seclen) *seclen = slen;
      }
      if (m != *mfm) {
        r |= DISK_MFM_VARI;
        *mfm = 1;
      }
    }
  }

  if (*unf == -2) {
    r |= DISK_UNFORMATTED_TRACK;
    *unf = -1;
  }

  return r;
}


//==========================================================================
//
//  gap_add
//
//==========================================================================
static int gap_add (disk_t *d, int gap, int gaptype) {
  const disk_gap_t *g = &gaps[gaptype];
  if (d->pos.i+g->len[gap] >= d->bpt) return 1; /* too many data bytes */
  /*-------------------------------- given gap --------------------------------*/
  memset(d->pos.track+d->pos.i, g->gap, g->len[gap]);
  d->pos.i += g->len[gap];
  return 0;
}


//==========================================================================
//
//  preindex_len
//
//  preindex gap and index mark
//
//==========================================================================
static int preindex_len (disk_t *d, int gaptype) {
  const disk_gap_t *g = &gaps[gaptype];
  return g->len[0]+g->sync_len+(g->mark >= 0 ? 3 : 0)+1;
}


/*
  [ ....GAP.... ] [ ... SYNC ... ] [ . MARK . ]
  |------------------------------------------->
    Preindex
*/
//==========================================================================
//
//  preindex_add
//
//  preindex gap and index mark
//
//==========================================================================
static int preindex_add (disk_t *d, int gaptype) {
  const disk_gap_t *g = &gaps[gaptype];
  if (d->pos.i+preindex_len(d, gaptype) >= d->bpt) return 1;
  /*------------------------------ pre-index gap -------------------------------*/
  if (gap_add(d, 0, gaptype)) return 1;
  /*------------------------------   sync    ---------------------------*/
  memset(d->pos.track+d->pos.i, g->sync, g->sync_len);
  d->pos.i += g->sync_len;
  if (g->mark >= 0) {
    memset(d->pos.track+d->pos.i , g->mark, 3);
    bitmap_set(d->pos.clocks, d->pos.i); ++d->pos.i;
    bitmap_set(d->pos.clocks, d->pos.i); ++d->pos.i;
    bitmap_set(d->pos.clocks, d->pos.i); ++d->pos.i;
  }
  /*------------------------------     mark     ------------------------------*/
  if (g->mark < 0) {
    /* FM */
    bitmap_set(d->pos.clocks, d->pos.i); /* set clock mark */
  }
  d->pos.track[d->pos.i++] = 0xfc; /* index mark */
  return 0;
}


//==========================================================================
//
//  postindex_len
//
//  preindex gap and index mark
//
//==========================================================================
static inline int postindex_len (disk_t *d, int gaptype) {
  const disk_gap_t *g = &gaps[gaptype];
  return g->len[1];
}


//==========================================================================
//
//  postindex_add
//
//  postindex gap
//
//==========================================================================
static inline int postindex_add (disk_t *d, int gaptype) {
  return gap_add(d, 1, gaptype);
}


//==========================================================================
//
//  gap4_add
//
//==========================================================================
static int gap4_add (disk_t *d, int gaptype) {
  int len = d->bpt-d->pos.i;
  const disk_gap_t *g = &gaps[gaptype];
  if (len < 0) return 1;
  /*------------------------------     GAP IV     ------------------------------*/
  memset(d->pos.track+d->pos.i, g->gap, len); /* GAP IV fill until end of track */
  d->pos.i = d->bpt;
  return 0;
}


#define SECLEN_128   (0x00)
#define SECLEN_256   (0x01)
#define SECLEN_512   (0x02)
#define SECLEN_1024  (0x03)

#define CRC_OK     (0)
#define CRC_ERROR  (1)

/*
  [ ....GAP.... ] [ ... SYNC ... ] [ . MARK . ] [ .. DATA .. ] [ . CRC . ]
                 |------------------------------------------------------->
                        ID
*/
//==========================================================================
//
//  id_add
//
//==========================================================================
static int id_add (disk_t *d, int h, int t, int s, int l, int gaptype, int crc_error) {
  uint16_t crc = 0xffff;
  const disk_gap_t *g = &gaps[gaptype];
  if (d->pos.i+g->sync_len+(g->mark >= 0 ? 3 : 0)+7 >= d->bpt) return 1;
  /*------------------------------   sync    ---------------------------*/
  memset(d->pos.track+d->pos.i, g->sync, g->sync_len);
  d->pos.i += g->sync_len;
  if (g->mark >= 0) {
    memset(d->pos.track+d->pos.i, g->mark, 3);
    bitmap_set(d->pos.clocks, d->pos.i); ++d->pos.i;
    crc = crc_fdc(crc, g->mark);
    bitmap_set(d->pos.clocks, d->pos.i); ++d->pos.i;
    crc = crc_fdc(crc, g->mark);
    bitmap_set(d->pos.clocks, d->pos.i); ++d->pos.i;
    crc = crc_fdc(crc, g->mark);
  }
  /*------------------------------     mark     ------------------------------*/
  if (g->mark < 0) {
    /* FM */
    bitmap_set(d->pos.clocks, d->pos.i); /* set clock mark */
  }
  d->pos.track[d->pos.i++] = 0xfe; /* ID mark */
  crc = crc_fdc(crc, 0xfe);
  /*------------------------------     header     ------------------------------*/
  d->pos.track[d->pos.i++] = t; crc = crc_fdc(crc, t);
  d->pos.track[d->pos.i++] = h; crc = crc_fdc(crc, h);
  d->pos.track[d->pos.i++] = s; crc = crc_fdc(crc, s);
  d->pos.track[d->pos.i++] = l; crc = crc_fdc(crc, l);
  d->pos.track[d->pos.i++] = crc>>8;
  if (crc_error) {
    d->pos.track[d->pos.i++] = (~crc)&0xff; /* record a CRC error */
  } else {
    d->pos.track[d->pos.i++] = crc&0xff; /* CRC */
  }
  /*------------------------------     GAP II     ------------------------------*/
  return gap_add(d, 2, gaptype);
}


/*
  [ ....GAP.... ] [ ... SYNC ... ] [ . MARK . ] [ .. DATA .. ] [ . CRC . ]
 |-------------------------------------------->
                  datamark
*/
//==========================================================================
//
//  datamark_add
//
//==========================================================================
static int datamark_add (disk_t *d, int ddam, int gaptype) {
  const disk_gap_t *g = &gaps[gaptype];
  if (d->pos.i+g->len[2]+g->sync_len+(g->mark >= 0 ? 3 : 0)+1 >= d->bpt) return 1;
  /*------------------------------   sync    ---------------------------*/
  memset(d->pos.track+d->pos.i, g->sync, g->sync_len);
  d->pos.i += g->sync_len;
  if (g->mark >= 0) {
    memset(d->pos.track+d->pos.i, g->mark, 3);
    bitmap_set(d->pos.clocks, d->pos.i); ++d->pos.i;
    bitmap_set(d->pos.clocks, d->pos.i); ++d->pos.i;
    bitmap_set(d->pos.clocks, d->pos.i); ++d->pos.i;
  }
  /*------------------------------     mark     ------------------------------*/
  if (g->mark < 0) {
    /* FM */
    bitmap_set(d->pos.clocks, d->pos.i); /* set clock mark */
  }
  d->pos.track[d->pos.i++] = (ddam ? 0xf8 : 0xfb); /* DATA mark 0xf8 -> deleted data */
  return 0;
}


#define NO_DDAM      (0)
#define DDAM         (1)
#define NO_AUTOFILL  (-1)

//==========================================================================
//
//  data_add
//
//  copy data from *buffer and update *buffer->index
//  if 'buffer' == NULL, then copy data bytes from 'data'
//
//==========================================================================
static int data_add (disk_t *d, buffer_t *buffer, const unsigned char *data, int len,
                     int ddam, int gaptype, int crc_error, int autofill, int *start_data)
{
  int length;
  uint16_t crc = 0xffff;
  const disk_gap_t *g = &gaps[gaptype];

  if (len < 0) len = 0;
  if (datamark_add(d, ddam, gaptype)) return 1;

  if (g->mark >= 0) {
    crc = crc_fdc(crc, g->mark);
    crc = crc_fdc(crc, g->mark);
    crc = crc_fdc(crc, g->mark);
  }
  crc = crc_fdc(crc, (ddam ? 0xf8 : 0xfb)); /* deleted or normal */
  if (len < 0) goto header_crc_error; /* CRC error */
  if (d->pos.i+len+2 >= d->bpt) return 1; /* too many data bytes */
  /*------------------------------      data      ------------------------------*/
  if (start_data != NULL) *start_data = d->pos.i; /* record data start position */
  if (buffer == NULL) {
    if (len > 0 && data) memcpy(d->pos.track+d->pos.i, data, len);
    length = len;
  } else {
    length = buffavail(buffer);
    if (length > len) length = len;
    buffread(d->pos.track+d->pos.i, length, buffer);
  }
  if (length < len) {
    /* autofill with 'autofill' */
    if (autofill < 0) return 1;
    while (length < len) {
      d->pos.track[d->pos.i+length] = autofill;
      ++length;
    }
  }
  length = 0;
  while (length < len) {
    /* calculate CRC */
    crc = crc_fdc(crc, d->pos.track[d->pos.i]);
    ++d->pos.i;
    ++length;
  }
  if (crc_error) crc ^= 1; /* mess up CRC */
  d->pos.track[d->pos.i++] = crc>>8;
  d->pos.track[d->pos.i++] = crc&0xff; /* CRC */
  /*------------------------------     GAP III    ------------------------------*/
header_crc_error:
  return gap_add(d, 3, gaptype);
}


//==========================================================================
//
//  calc_sectorlen
//
//==========================================================================
static int calc_sectorlen (int mfm, int sector_length, int gaptype) {
  int len = 0;
  const disk_gap_t *g = &gaps[gaptype];
  /*------------------------------     ID        ------------------------------*/
  len += g->sync_len+(g->mark >= 0 ? 3 : 0)+7;
  /*------------------------------     GAP II    ------------------------------*/
  len += g->len[2];
  /*---------------------------------  data   ---------------------------------*/
  len += g->sync_len+(g->mark >= 0 ? 3 : 0)+1; /* DAM */
  len += sector_length;
  len += 2; /* CRC */
  /*------------------------------    GAP III    ------------------------------*/
  len += g->len[3];
  return len;
}


//==========================================================================
//
//  calc_lenid
//
//==========================================================================
static int calc_lenid (int sector_length) {
  int id = 0;
  while (sector_length > 0x80) {
    ++id;
    sector_length >>= 1;
  }
  return id;
}


//==========================================================================
//
//  disk_update_tlens
//
//  update tracks TLEN
//
//==========================================================================
static void disk_update_tlens (disk_t *d) {
  /* check tracks */
  for (int i = 0; i < d->sides*d->cylinders; ++i) {
    DISK_SET_TRACK_IDX(d, i);
    if (d->pos.track[-3]+256*d->pos.track[-2] == 0) {
      d->pos.track[-3] = d->bpt&0xff;
      d->pos.track[-2] = (d->bpt>>8)&0xff;
    }
  }
}


#define NO_INTERLEAVE    (1)
#define INTERLEAVE_2     (2)
#define INTERLEAVE_OPUS  (13)

#define NO_PREINDEX  (0)
#define PREINDEX     (1)

//==========================================================================
//
//  trackgen
//
//==========================================================================
static int trackgen (disk_t *d, buffer_t *buffer, int head, int track,
                     int sector_base, int sectors, int sector_length, int preindex,
                     int gap, int interleave, int autofill)
{
  int i, s, pos;
  int slen = calc_sectorlen((d->density != DISK_SD && d->density != DISK_8_SD),
                            sector_length, gap);
  int idx;

  d->pos.i = 0;
  DISK_SET_TRACK(d, head, track);

  if (preindex && preindex_add(d, gap)) return 1;
  if (postindex_add(d, gap)) return 1;

  idx = d->pos.i;
  pos = i = 0;
  for (s = sector_base; s < sector_base+sectors; ++s) {
    d->pos.i = idx+pos*slen;
    if (id_add(d, head, track, s, calc_lenid(sector_length), gap, CRC_OK)) return 1;
    if (data_add(d, buffer, NULL, sector_length, NO_DDAM, gap, CRC_OK, autofill, NULL)) return 1;
    pos += interleave;
    if (pos >= sectors) {
      /* wrap around */
      pos -= sectors;
      if (pos <= i) {
        /* we fill this pos already */
        ++pos; /* skip one more pos */
        ++i;
      }
    }
  }
  d->pos.i = idx+sectors*slen;
  return gap4_add(d, gap);
}


//==========================================================================
//
//  disk_close
//
//  close and destroy a disk structure and data
//
//==========================================================================
void disk_close (disk_t *d) {
  if (!d) return;
  if (d->data != NULL) {
    free(d->data);
    d->data = NULL;
  }
  if (d->filename != NULL) {
    free(d->filename);
    d->filename = NULL;
  }
  d->type = DISK_TYPE_NONE;
}


/*
 *  if d->density == DISK_DENS_AUTO =>
 *                            use d->tlen if d->bpt == 0
 *                             or use d->bpt to determine d->density
 *  or use d->density
 */
//==========================================================================
//
//  disk_alloc
//
//==========================================================================
static int disk_alloc (disk_t *d) {
  size_t dlen;

       if (d->density != DISK_DENS_AUTO) d->bpt = disk_bpt[d->density];
  else if (d->bpt > 12500) return d->status = DISK_UNSUP;
  else if (d->bpt > 10416) { d->density = DISK_HD; d->bpt = disk_bpt[DISK_HD]; }
  else if (d->bpt > 6500) { d->density = DISK_8_DD; d->bpt = disk_bpt[DISK_8_DD]; }
  else if (d->bpt > 6250) { d->density = DISK_DD_PLUS; d->bpt = disk_bpt[DISK_DD_PLUS]; }
  else if (d->bpt > 5208) { d->density = DISK_DD; d->bpt = disk_bpt[DISK_DD]; }
  else if (d->bpt > 3125) { d->density = DISK_8_SD; d->bpt = disk_bpt[DISK_8_SD]; }
  else if (d->bpt > 0) { d->density = DISK_SD; d->bpt = disk_bpt[DISK_SD]; }

  if (d->bpt > 0) d->tlen = 4+d->bpt+3*DISK_CLEN(d->bpt);

  dlen = d->sides*d->cylinders*d->tlen; /* track len with clock and other marks */
  if (dlen == 0) return d->status = DISK_GEOM;

  d->data = calloc(1, dlen);
  if (!d->data) return (d->status = DISK_MEM);

  return (d->status = DISK_OK);
}


//==========================================================================
//
//  disk_new
//
//  create a new unformatted disk
//
//==========================================================================
int disk_new (disk_t *d, int sides, int cylinders, disk_dens_t density, disk_type_t type) {
  if (!d) return DISK_BADARGS;

  disk_close(d);

  d->filename = NULL;

  if (density < DISK_DENS_AUTO || density > DISK_HD || /* unknown density */
      type <= DISK_TYPE_NONE || type >= DISK_TYPE_LAST || /* unknown type */
      sides < 1 || sides > 2 || /* 1 or 2 side */
      cylinders < 35 || cylinders > 83) /* 35 .. 83 cylinder */
  {
    return (d->status = DISK_GEOM);
  }

  d->type = type;
  d->density = (density == DISK_DENS_AUTO ? DISK_DD : density);
  d->sides = sides;
  d->cylinders = cylinders;

  if (disk_alloc(d) != DISK_OK) return d->status;

  d->wrprot = 0;
  d->dirty = 1;
  disk_update_tlens(d);

  return (d->status = DISK_OK);
}


//==========================================================================
//
//  alloc_uncompress_buffer
//
//==========================================================================
static int alloc_uncompress_buffer (unsigned char **buffer, int length) {
  if (*buffer != NULL) return 0; /* return if allocated */
  unsigned char *b = calloc(1, length);
  if (b == NULL) return 1;
  *buffer = b;
  return 0;
}


//==========================================================================
//
//  disk_preformat
//
//==========================================================================
int disk_preformat (disk_t *d) {
  if (!d) return DISK_BADARGS;

  buffer_t buffer;

  buffer.file.length = 0;
  buffer.index = 0;

  if (d->sides == 2) {
    if (trackgen(d, &buffer, 1, 0, 0xff, 1, 128,
                 NO_PREINDEX, GAP_MINIMAL_MFM, NO_INTERLEAVE, 0xff))
    {
      return DISK_GEOM;
    }

    if (trackgen(d, &buffer, 1, 2, 0xfe, 1, 128,
                 NO_PREINDEX, GAP_MINIMAL_MFM, NO_INTERLEAVE, 0xff))
    {
      return DISK_GEOM;
    }
  }

  if (trackgen(d, &buffer, 0, 0, 0xff, 1, 128,
               NO_PREINDEX, GAP_MINIMAL_MFM, NO_INTERLEAVE, 0xff))
  {
    return DISK_GEOM;
  }

  if (trackgen(d, &buffer, 0, 2, 0xfe, 1, 128,
               NO_PREINDEX, GAP_MINIMAL_MFM, NO_INTERLEAVE, 0xff))
  {
    return DISK_GEOM;
  }

  return DISK_OK;
}


/* open a disk image */
#define GEOM_CHECK \
  if (d->sides < 1 || d->sides > 2 || \
      d->cylinders < 1 || d->cylinders > 85) return d->status = DISK_GEOM


#include "disk_udi.c"
#include "disk_mgt.c"
#include "disk_d40d80.c"
#include "disk_sad.c"
#include "disk_trd.c"
#include "disk_scl.c"
#include "disk_fdi.c"
#include "disk_td0.c"
#include "disk_cpc.c"


/* open a disk image file, read and convert to our format
 * if preindex != 0 we generate preindex gap if needed
 */
static int disk_open2 (disk_t *d, const char *filename, int preindex) {
  buffer_t buffer;

  d->wrprot = (access(filename, W_OK) != 0); /* file read only */

  if (utils_read_file(filename, &buffer.file)) {
    return (d->status = DISK_OPEN);
  }

  buffer.index = 0;

  d->type = disk_detect_format(buffer.file.buffer, buffer.file.length);
  if (d->type == DISK_TYPE_NONE && buffer.file.length > 16) {
    const size_t nlen = strlen(filename);
    const char *ext = (nlen >= 4 ? filename+nlen-4 : "");
         if (strcasecmp(ext, ".trd") == 0) d->type = DISK_TRD;
    else if (strcasecmp(ext, ".mgt") == 0) d->type = DISK_MGT;
    else if (strcasecmp(ext, ".opd") == 0) d->type = DISK_OPD;
    else if (strcasecmp(ext, ".opu") == 0) d->type = DISK_OPD;
    else if (strcasecmp(ext, ".d40") == 0) d->type = DISK_D40;
    else if (strcasecmp(ext, ".d80") == 0) d->type = DISK_D80;
  }
  switch (d->type) {
    case DISK_UDI: open_udi(&buffer, d, preindex); break;
    case DISK_FDI: open_fdi(&buffer, d, preindex); break;
    case DISK_TD0: open_td0(&buffer, d, preindex); break;
    case DISK_MGT: open_img_mgt_opd(&buffer, d); break;
    case DISK_IMG: open_img_mgt_opd(&buffer, d); break;
    case DISK_SAD: open_sad(&buffer, d, preindex); break;
    case DISK_CPC: open_cpc(&buffer, d, preindex); break;
    case DISK_ECPC: open_cpc(&buffer, d, preindex); break;
    case DISK_TRD: open_trd(&buffer, d, preindex); break;
    case DISK_SCL: open_scl(&buffer, d, preindex); break;
    case DISK_OPD: open_img_mgt_opd(&buffer, d); break;
    case DISK_D40: open_d40_d80(&buffer, d); break;
    case DISK_D80: open_d40_d80(&buffer, d); break;
    default:
      utils_close_file(&buffer.file);
      return (d->status = DISK_OPEN);
  }

  if (d->status != DISK_OK) {
    if (d->data != NULL) free(d->data);
    utils_close_file(&buffer.file);
    return d->status;
  }

  utils_close_file(&buffer.file);
  d->dirty = 0;
  disk_update_tlens(d);
  update_tracks_mode(d);
  d->filename = /*utils_safe_*/strdup(filename);

  return (d->status = DISK_OK);
}


//==========================================================================
//
//  disk_init_struct
//
//==========================================================================
void disk_init_struct (disk_t *d) {
  if (d) memset(d, 0, sizeof(*d));
}


//==========================================================================
//
//  disk_open_ex
//
//==========================================================================
int disk_open_ex (disk_t *d, const char *filename, int preindex) {
  if (!d) return DISK_BADARGS;
  disk_close(d);
  if (filename == NULL || *filename == 0) return (d->status = DISK_OPEN);
  return disk_open2(d, filename, preindex);
}


//==========================================================================
//
//  disk_open
//
//==========================================================================
int disk_open (disk_t *d, const char *filename) {
  return disk_open_ex(d, filename, 0);
}


//==========================================================================
//
//  disk_merge_sides
//
//  create a two sided disk (d) from two one sided (d1 and d2)
//
//==========================================================================
int disk_merge_sides (disk_t *d, disk_t *d1, disk_t *d2, int autofill) {
  if (!d) return DISK_BADARGS;

  if (d1->sides != 1 || d2->sides != 1 ||
      d1->bpt != d2->bpt ||
      (autofill < 0 && d1->cylinders != d2->cylinders))
  {
    return DISK_GEOM;
  }

  d->wrprot = 0;
  d->dirty = 0;
  d->sides = 2;
  d->type = d1->type;
  d->cylinders = (d2->cylinders > d1->cylinders ? d2->cylinders : d1->cylinders);
  d->bpt = d1->bpt;
  d->density = DISK_DENS_AUTO;

  if (disk_alloc(d) != DISK_OK) return d->status;

  const int clen = DISK_CLEN(d->bpt);
  d->pos.track = d->data;
  d1->pos.track = d1->data;
  d2->pos.track = d2->data;

  for (int i = 0; i < d->cylinders; ++i) {
    if (i < d1->cylinders) {
      memcpy(d->pos.track, d1->pos.track, d->tlen);
    } else {
      d->pos.track[0] = d->bpt&0xff;
      d->pos.track[1] = (d->bpt>>8)&0xff;
      d->pos.track[2] = 0x00;
      memset(d->pos.track+3, (autofill&0xff), d->bpt); /* fill data */
      memset(d->pos.track+3+d->bpt, 0x00, 3*clen); /* no clock and other marks */
    }
    d->pos.track += d->tlen;
    d1->pos.track += d1->tlen;
    if (i < d2->cylinders) {
      memcpy(d->pos.track, d2->pos.track, d->tlen);
    } else {
      d->pos.track[0] = d->bpt&0xff;
      d->pos.track[1] = (d->bpt>>8)&0xff;
      d->pos.track[2] = 0x00;
      memset(d->pos.track+1, (autofill&0xff), d->bpt); /* fill data */
      memset(d->pos.track+1+d->bpt, 0x00, 3*clen); /* no clock and other marks */
    }
    d->pos.track += d->tlen;
    d2->pos.track += d2->tlen;
  }
  disk_close(d1);
  disk_close(d2);
  return (d->status = DISK_OK);
}


//==========================================================================
//
//  write_log
//
//==========================================================================
static int write_log (FILE *file, disk_t *d, int full) {
  int i, j, k, del;
  int h, t, s, b;
  char str[17];

  libfdcMsg(LIBFDC_MSG_DEBUG, "dumping IAMs...");
  str[16] = '\0';
  fprintf(file, "DISK tracks log!\n");
  fprintf(file, "Sides: %d, cylinders: %d\n", d->sides, d->cylinders);
  for (j = 0; j < d->cylinders; ++j) {
    /* ALT :) */
    for (i = 0; i < d->sides; ++i) {
      DISK_SET_TRACK(d, i, j);
      d->pos.i = 0;
      fprintf(file, "\n*********\nSide: %d, cylinder: %d type: 0x%02x tlen: %5u\n",
              i, j, d->pos.track[-1], d->pos.track[-3]+256*d->pos.track[-2]);
      while (id_read(d, &h, &t, &s, &b)) {
        fprintf(file, "  h:%d t:%d s:%d l:%d(%d)", h, t, s, b, (0x80<<b));
        if (datamark_read(d, &del)) {
          fprintf(file, " %s\n", (del ? "deleted" : "normal"));
        } else {
          fprintf(file, " noDAM\n");
        }
      }
    }
  }

  if (full) {
    libfdcMsg(LIBFDC_MSG_DEBUG, "dumping sector data...");
    fprintf(file, "\n***************************\nSector Data Dump:\n");
    for (j = 0; j < d->cylinders; ++j) {
      for (i = 0; i < d->sides; ++i) {
        //libfdcMsg(LIBFDC_MSG_DEBUG, "  head:%d; cyl:%02d", j, i);
        DISK_SET_TRACK(d, i, j);
        d->pos.i = 0;
        fprintf(file, "\n*********\nSide: %d, cylinder: %d type: 0x%02x tlen: %5u\n",
                i, j, d->pos.track[-1], d->pos.track[-3]+256*d->pos.track[-2]);
        while (d->pos.i < d->bpt && id_read(d, &h, &t, &s, &b)) {
          b = 0x80<<b;
          if (datamark_read(d, &del)) {
            fprintf(file, "  h:%d t:%d s:%d l:%d (%s)\n", h, t, s, b, (del ? "deleted" : "normal"));
          } else {
            fprintf(file, "  h:%d t:%d s:%d l:%d (missing data)\n", h, t, s, b);
          }
          k = 0;
          while (d->pos.i < d->bpt && k < b) {
            if (k%16 == 0) fprintf(file, "0x%08x:", k);
            fprintf(file, " 0x%02x", d->pos.track[d->pos.i]);
            str[k&0x0f] = (d->pos.track[d->pos.i] >= 32 && d->pos.track[d->pos.i] < 127 ? d->pos.track[d->pos.i] : '.');
            ++k;
            if (k%16 == 0) fprintf(file, " | %s\n", str);
            ++d->pos.i;
          }
        }
      }
    }
  }

  libfdcMsg(LIBFDC_MSG_DEBUG, "full dump...");
  fprintf(file, "\n***************************\n**Full Dump:\n");
  for (j = 0; j < d->cylinders; ++j) {
    for (i = 0; i < d->sides; ++i) {
      DISK_SET_TRACK(d, i, j);
      d->pos.i = 0;
      fprintf(file, "\n*********\nSide: %d, cylinder: %d type: 0x%02x tlen: %5u\n",
              i, j, d->pos.track[-1], d->pos.track[-3]+256*d->pos.track[-2]);
      while (d->pos.i < d->bpt) {
        fprintf(file, "0x%08x:", d->pos.i);
        // hex dump
        for (int f = 0; f < 8; ++f) {
          if (d->pos.i+f >= d->bpt) {
            fprintf(file, "     ");
          } else {
            char tpb[3];
            tpb[0] = tpb[1] = '.';
            tpb[2] = 0;
            if (bitmap_test(d->pos.clocks, d->pos.i+f)) tpb[1] = '!';
            if (bitmap_test(d->pos.fm, d->pos.i+f)) {
              tpb[0] = (bitmap_test(d->pos.weak, d->pos.i) ? 'f' : 'F');
            } else {
              if (bitmap_test(d->pos.weak, d->pos.i)) tpb[0] = 'w';
            }
            fprintf(file, " %s%02x", tpb, d->pos.track[d->pos.i]);
          }
        }
        // char dump
        fprintf(file, "   ");
        for (int f = 0; f < 8; ++f) {
          if (d->pos.i+f >= d->bpt) break;
          char c = d->pos.track[d->pos.i+f];
          if ((c >= 0 && c <= 32) || c == 127) c = '.';
          fprintf(file, "%c", c);
        }
        fputc('\n', file);
        d->pos.i += 8;
      }
    }
  }

  return (d->status = DISK_OK);
}


//==========================================================================
//
//  disk_write
//
//==========================================================================
int disk_write (disk_t *d, const char *filename) {
  if (!d) return DISK_BADARGS;

  if (!filename || !filename[0]) return (d->status = DISK_WRFILE);

  const size_t namelen = strlen(filename);
  const char *ext = (namelen < 4 ? "" : filename+namelen-4);

  disk_type_t dtype = (d->type == DISK_TYPE_NONE ? DISK_UDI : d->type);
  int fulllog = 1;

       if (!strcasecmp(ext, ".udi")) dtype = DISK_UDI;
  else if (!strcasecmp(ext, ".dsk")) dtype = DISK_ECPC;
  else if (!strcasecmp(ext, ".mgt")) dtype = DISK_MGT;
  else if (!strcasecmp(ext, ".opd")) dtype = DISK_OPD;
  else if (!strcasecmp(ext, ".opu")) dtype = DISK_OPD;
  else if (!strcasecmp(ext, ".img")) dtype = DISK_IMG;
  else if (!strcasecmp(ext, ".trd")) dtype = DISK_TRD;
  else if (!strcasecmp(ext, ".sad")) dtype = DISK_SAD;
  else if (!strcasecmp(ext, ".fdi")) dtype = DISK_FDI;
  else if (!strcasecmp(ext, ".d40")) dtype = DISK_D40;
  else if (!strcasecmp(ext, ".d80")) dtype = DISK_D80;
  else if (!strcasecmp(ext, ".scl")) dtype = DISK_SCL;
  else if (!strcasecmp(ext, ".td0")) dtype = DISK_TD0;
  else if (!strcasecmp(ext, ".log")) dtype = DISK_LOG;
  else if (!strcasecmp(ext, ".xlg")) { dtype = DISK_LOG; fulllog = 0; }

  if (dtype == DISK_TD0) {
    libfdcMsg(LIBFDC_MSG_ERROR, "TD0 writing is not supported.");
    return (d->status = DISK_IMPL);
  }

  FILE *file = fopen(filename, "wb");
  if (file == NULL) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot create file '%s'.", filename);
    return (d->status = DISK_WRFILE);
  }

  update_tracks_mode(d); //k8: dunno why

  switch (dtype) {
    case DISK_UDI:
      write_udi(file, d);
      break;
    case DISK_IMG:
    case DISK_MGT:
    case DISK_OPD:
      write_img_mgt_opd(file, d);
      break;
    case DISK_D40:
    case DISK_D80:
      write_d40_d80(file, d);
      break;
    case DISK_TRD:
      write_trd(file, d);
      break;
    case DISK_SAD:
      write_sad(file, d);
      break;
    case DISK_FDI:
      write_fdi(file, d);
      break;
    case DISK_SCL:
      write_scl(file, d);
      break;
    case DISK_ECPC:
    case DISK_CPC:
      write_cpc(file, d);
      break;
    case DISK_LOG:
      write_log(file, d, fulllog);
      dtype = d->type;
      break;
    default:
      d->status = DISK_WRFILE;
      break;
  }

  if (d->status != DISK_OK) {
    fclose(file);
    return d->status;
  }

  if (fclose(file) == -1) return (d->status = DISK_WRFILE);

  d->type = dtype;
  return (d->status = DISK_OK);
}


//==========================================================================
//
//  disk_detect_format
//
//==========================================================================
disk_type_t disk_detect_format (const void *data, int size) {
  if (!data || size < 18) return DISK_TYPE_NONE;
  if (memcmp(data, "SINCLAIR", 8) == 0) return DISK_SCL;
  if (memcmp(data, "UDI!", 4) == 0) return DISK_UDI;
  if (memcmp(data, "FDI", 3) == 0) return DISK_FDI;
  if (memcmp(data, "EXTENDED", 8) == 0) return DISK_ECPC;
  if (memcmp(data, "MV - CPC", 8) == 0) return DISK_CPC;
  if (memcmp(data, "TD\x00", 3) == 0 || memcmp(data, "td\x00", 3) == 0) {
    if (((const uint8_t *)data)[4] == 0x15) return DISK_TD0;
  }
  if (memcmp(data, "Aley's disk backup", 18) == 0) return DISK_SAD;
  return DISK_TYPE_NONE;
}


//==========================================================================
//
//  create_buffer
//
//==========================================================================
static int create_buffer (buffer_t *buf, const void *data, int size) {
  if (size <= 16 || !data) return -1;
  buf->index = 0;
  buf->file.buffer = calloc(1, (uint32_t)size);
  if (!buf->file.buffer) return -1;
  buf->file.length = (uint32_t)size;
  memcpy(buf->file.buffer, data, (uint32_t)size);
  return 0;
}



#define XXOPEN(dskfmt,dtype)  \
int disk_open_buf_##dskfmt (disk_t *d, const void *data, int size) { \
  if (!d) return DISK_BADARGS; \
  buffer_t buf; \
  if (create_buffer(&buf, data, size) != 0) return DISK_OPEN; \
  const int res = open_##dskfmt(&buf, d, 0); \
  if (res != DISK_OK) { \
    disk_close(d); \
  } else { \
    d->type = dtype; \
    d->dirty = 0; \
    d->wrprot = 0; \
    disk_update_tlens(d); \
    update_tracks_mode(d); \
    d->filename = NULL; \
  } \
  utils_close_file(&buf.file); \
  return res; \
}

XXOPEN(udi, DISK_UDI)
XXOPEN(fdi, DISK_FDI)
XXOPEN(trd, DISK_TRD)
XXOPEN(scl, DISK_SCL)
XXOPEN(td0, DISK_TD0)


//==========================================================================
//
//  disk_open_buf_dsk
//
//==========================================================================
int disk_open_buf_dsk (disk_t *d, const void *data, int size) {
  if (!d) return DISK_BADARGS;
  if (size < 16) return DISK_OPEN;

  const disk_type_t type = disk_detect_format(data, size);
  if (type != DISK_CPC && type != DISK_ECPC) return DISK_OPEN;
  d->type = type;

  buffer_t buf;
  if (create_buffer(&buf, data, size) != 0) return -1;
  const int res = open_cpc(&buf, d, 0);
  if (res != DISK_OK) {
    disk_close(d);
  } else {
    d->dirty = 0;
    d->wrprot = 0;
    disk_update_tlens(d);
    update_tracks_mode(d);
    d->filename = NULL;
  }
  utils_close_file(&buf.file);
  return res;
}


//==========================================================================
//
//  disk_track_map_simple
//
//  returns number of found sectors
//
//==========================================================================
int disk_track_map_simple (disk_t *d, int track, disk_sector_header *seclist) {
  if (!d || !seclist) return 0;

  if (d->sides < 1 || d->sides > 2) return 0;
  if (d->cylinders < 1 || d->cylinders > 83) return 0;

  if (track < 0 || track > 83*2) return 0;

  int head, cyl;
  if (d->sides == 2) {
    head = track&1;
    cyl = track>>1;
  } else {
    head = 0;
    cyl = track;
  }

  if (head >= d->sides) return 0;
  if (cyl >= d->cylinders) return 0;

  DISK_SET_TRACK(d, head, cyl);

  int scount = 0;
  int h, t, s, b;
  d->pos.i = 0; /* start of the track */
  while (id_read(d, &h, &t, &s, &b)) {
    if (scount == 256) break;
    seclist[scount].track = t;
    seclist[scount].head = h;
    seclist[scount].sector = s;
    seclist[scount].size = b;
    if (!datamark_read_ex(d, &seclist[scount].mark)) {
      seclist[scount].mark = 0;
      ++scount;
      break;
    }
    ++scount;
  }

  return scount;
}


//==========================================================================
//
//  disk_get_sector_data_ptr
//
//  return pointer to sector data or NULL
//
//==========================================================================
void *disk_get_sector_data_ptr (disk_t *d, int track, int sector, int *sectorsize,
                                int *error)
{
  int errtmp;
  if (!error) error = &errtmp;
  if (sectorsize) *sectorsize = 0;
  if (!d) { *error = DISK_BADARGS; return NULL; }

  if (d->sides < 1 || d->sides > 2) { *error = DISK_OPEN; return NULL; }
  if (d->cylinders < 1 || d->cylinders > 83) { *error = DISK_OPEN; return NULL; }

  if (track < 0 || track > 83*2) { *error = DISK_NO_CYLINDER; return NULL; }

  int head, cyl;
  if (d->sides == 2) {
    head = track&1;
    cyl = track>>1;
  } else {
    head = 0;
    cyl = track;
  }

  if (head >= d->sides) { *error = DISK_INVALID_HEAD; return NULL; }
  if (cyl >= d->cylinders) { *error = DISK_NO_CYLINDER; return NULL; }

  void *res = NULL;
  int del = 0;

  DISK_SET_TRACK(d, head, cyl);

  const int szcode = id_seek_ex(d, head, cyl, sector);
  if (szcode < 0) { *error = DISK_NO_SECTOR; goto quit; }
  if (szcode > 3) { *error = DISK_BAD_SECTOR; goto quit; }
  if (!datamark_read(d, &del)) { *error = DISK_BAD_SECTOR; goto quit; }
  if (del) { *error = DISK_BAD_SECTOR; goto quit; }

  if (sectorsize) *sectorsize = 0x80<<szcode;
  *error = DISK_OK;

  res = d->pos.track+d->pos.i;

quit:
  return res;
}


//==========================================================================
//
//  disk_is_sector_exist
//
//==========================================================================
int disk_is_sector_exist (disk_t *d, int track, int sector) {
  return !!disk_get_sector_data_ptr(d, track, sector, NULL, NULL);
}


//==========================================================================
//
//  disk_read_sector
//
//  read disk sector
//  returns error code (DISK_xxx)
//  automatically saves and restores disk position
//
//==========================================================================
int disk_read_sector (disk_t *d, int track, int sector,
                      void *buf, size_t bufsize, int *sectorsize)
{
  if (!d) return DISK_BADARGS;

  int error;
  int secsize;
  const uint8_t *sdata = disk_get_sector_data_ptr(d, track, sector, &secsize, &error);
  if (!sdata) return error;
  if (sectorsize) *sectorsize = secsize;

  if (!buf) return DISK_OPEN;
  if (bufsize < secsize) return DISK_SECTOR_TOO_BIG;
  memcpy(buf, sdata, secsize);

  return DISK_OK;
}


//==========================================================================
//
//  disk_write_sector
//
//==========================================================================
int disk_write_sector (disk_t *d, int track, int sector,
                       const void *buf, size_t bufsize)
{
  if (!d) return DISK_BADARGS;

  if (d->sides < 1 || d->sides > 2) return DISK_OPEN;
  if (d->cylinders < 1 || d->cylinders > 83) return DISK_OPEN;

  if (track < 0 || track > 83*2) return DISK_NO_CYLINDER;

  int head, cyl;
  if (d->sides == 2) {
    head = track&1;
    cyl = track>>1;
  } else {
    head = 0;
    cyl = track;
  }

  if (head >= d->sides) return DISK_INVALID_HEAD;
  if (cyl >= d->cylinders) return DISK_NO_CYLINDER;

  int res = DISK_OK;

  DISK_SET_TRACK(d, head, cyl);

  const int szcode = id_seek_ex(d, head, cyl, sector);
  if (szcode < 0) { res = DISK_NO_SECTOR; goto quit; }
  if (szcode > 3) { res = DISK_BAD_SECTOR; goto quit; }

  int secsz = 0x80<<szcode;
  uint16_t crc = 0xffff;

  int a1mark = 0;
  while (d->pos.i < d->bpt) {
    #if 0
    fprintf(stderr, "  [%d/%d], a1mark=%d; byte=#%02X; clock=%d\n",
            d->pos.i, d->bpt, a1mark, d->pos.track[d->pos.i], !!bitmap_test(d->pos.clocks, d->pos.i));
    #endif
    crc = crc_fdc(crc, d->pos.track[d->pos.i]);
    if (bitmap_test(d->pos.clocks, d->pos.i)) {
      if (d->pos.track[d->pos.i] == 0xa1) {
        /* 0xa1 with clock */
        a1mark = 1;
      } else if (d->pos.track[d->pos.i] >= 0xf8 && d->pos.track[d->pos.i] <= 0xfe) {
        /* 0xfe with clock or 0xfe after 0xa1 mark */
        ++d->pos.i;
        break;
      } else {
        a1mark = 0;
        crc = 0xffff;
      }
    } else {
      if (a1mark && d->pos.track[d->pos.i] >= 0xf8 && d->pos.track[d->pos.i] <= 0xfe) {
        ++d->pos.i;
        break;
      }
      a1mark = 0;
      crc = 0xffff;
    }
    ++d->pos.i;
  }

  if (!a1mark || d->pos.i >= d->bpt || d->bpt-d->pos.i < secsz+2) { res = DISK_BAD_SECTOR; goto quit; }

  const uint8_t *xbuf = (const uint8_t *)buf;

  // sector data and data CRC
  while (secsz--) {
    if (xbuf && bufsize) {
      //fprintf(stderr, "   left=%u : #%02X\n", bufsize, *xbuf);
      d->pos.track[d->pos.i] = *xbuf++;
      --bufsize;
    }
    crc = crc_fdc(crc, d->pos.track[d->pos.i]);
    ++d->pos.i;
  }

  // update CRC
  d->pos.track[d->pos.i++] = crc>>8;
  d->pos.track[d->pos.i++] = crc&0xff;

quit:
  return res;
}


//==========================================================================
//
//  disk_format
//
//==========================================================================
int disk_format (disk_t *d, int heads, int cylinders, int firstsector, int sectors,
                 int secsize, int interleave)
{
  if (!d) return DISK_BADARGS;

  disk_close(d);

  if (heads < 1 || heads > 2) return (d->status = DISK_GEOM);
  if (cylinders < 1 || cylinders > 83) return (d->status = DISK_GEOM);
  if (sectors < 1 || sectors > 64) return (d->status = DISK_GEOM);

       if (cylinders < 40) cylinders = 40;
  else if (cylinders > 43) cylinders = 80;

  switch (secsize) {
    case 128:
    case 256:
    case 512:
    case 1024:
      break;
    default:
      return (d->status = DISK_GEOM);
  }

  d->type = DISK_TRD;
  d->sides = heads;
  d->cylinders = cylinders;
  d->density = DISK_DD;
  if (disk_alloc(d) != DISK_OK) return d->status;
  d->wrprot = 0;
  d->dirty = 1;
  d->have_weak = 0;

  if (sectors == 1) {
    interleave = 1;
  } else {
         if (interleave < 1) interleave = 2;
    else if (interleave > sectors-1) interleave = sectors-1;
  }

  // build sector number table
  int sidxtbl[128];
  int spos = 0;
  memset(sidxtbl, 0, sizeof(sidxtbl));

  for (int sidx = 1; sidx <= sectors; ++sidx) {
    while (sidxtbl[spos] != 0) spos = (spos+1)%sectors;
    sidxtbl[spos] = sidx+firstsector-1;
    spos = (spos+interleave)%sectors;
  }

  // assume that 512-byte sectors are for +3DOS
  const int gap = (secsize == 512 ? GAP_MINIMAL_MFM : GAP_TRDOS);
  const int lenid = calc_lenid(secsize);

  for (int head = 0; head < heads; ++head) {
    for (int cyl = 0; cyl < cylinders; ++cyl) {
      DISK_SET_TRACK(d, head, cyl);
      d->pos.i = 0;
      postindex_add(d, GAP_TRDOS);
      //const int seclen = calc_sectorlen(1, 256, GAP_TRDOS); // one sector raw length
      // format sectors
      for (int snum = 0; snum < sectors; ++snum) {
        id_add(d, head, cyl, sidxtbl[snum], lenid, gap, CRC_OK);
        data_add(d, NULL, NULL, secsize, NO_DDAM, gap, CRC_OK, /*NO_AUTOFILL*/0, NULL);
      }
      gap4_add(d, gap);
      //fprintf(stderr, "head=%d; cyl=%d; i=%d; bpt=%d\n", head, cyl, d->i, d->bpt);
      if (d->pos.i > d->bpt) __builtin_trap();
    }
    // just in case
    disk_update_tlens(d);
  }

  return (d->status = DISK_OK);
}
