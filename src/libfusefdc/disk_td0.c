/* disk.c: Routines for handling disk images
   Copyright (c) 2007-2017 Gergely Szasz

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

   Author contact information:

   Philip: philip-fuse@shadowmagic.org.uk

*/
//==========================================================================
//
//  open_td0
//
//==========================================================================
static int open_td0 (buffer_t *buffer, disk_t *d, int preindex) {
  int i, sectors, bpt, mfm, mfm_old;
  int data_offset, track_offset, sector_offset;
  uint8_t *uncomp_buff = NULL;
  const uint8_t *hdrb;
  uint8_t rlepat[512];

  // see http://dunfield.classiccmp.org/img42841/td0notes.txt
  /* image header:
     Signature            (2 bytes)
     Sequence             (1 byte)
     Checksequence        (1 byte)
     Teledisk version     (1 byte)
     Data rate            (1 byte) -- bits 0-1: 0:250kbps; 1:300kbps; 2:500kbps; bit 7:SD disk
     Drive type           (1 byte) -- who cares
     Stepping             (1 byte)
     DOS allocation flag  (1 byte)
     Sides                (1 byte)
     CRC                  (2 bytes)
   */

  if (buff[0] == 't') {
    /* signature "td" -> advanced compression */
    libfdcMsg(LIBFDC_MSG_ERROR, "TD0: advanced compression is not implemented.");
    return (d->status = DISK_IMPL); /* not implemented */
  }

  libfdcMsg(LIBFDC_MSG_DEBUG, "TD0: loading TeleDisk image...");

  mfm_old = (buff[5]&0x80 ? 0 : 1);

  /* td0notes say: may older teledisk indicate the SD on high bit of data rate */
  d->sides = buff[9]; /* 1 or 2 */
  if (d->sides < 1 || d->sides > 2) {
    libfdcMsg(LIBFDC_MSG_ERROR, "TD0: invalid number of sides (%d)", d->sides);
    return (d->status = DISK_GEOM);
  }

  /* skip comment block if any */
  data_offset = track_offset = 12+(buff[7]&0x80 ? 10+(buff[14]|(buff[15]<<8)) : 0);

  /* determine the greatest track length */
  d->bpt = 0;
  d->cylinders = 0;
  //seclen = 0;
  for (;;) {
    buffer->index = track_offset;
    if (buffavail(buffer) < 1) {
      libfdcMsg(LIBFDC_MSG_ERROR, "TD0: not enough data in track scan.");
      return (d->status = DISK_OPEN);
    }
    if ((sectors = buff[0]) == 255) break; /* sector number 255 => end of tracks */
    if (buffavail(buffer) < 4) {
      libfdcMsg(LIBFDC_MSG_ERROR, "TD0: no track header in track scan.");
      return (d->status = DISK_OPEN); /* check track header is avail. */
    }
    if (buff[1]+1 > d->cylinders) d->cylinders = buff[1]+1; /* find the biggest cylinder number */
    sector_offset = track_offset+4;
    mfm = (buff[2]&0x80 ? 0 : 1); /* 0x80 == 1 => SD track */
    bpt = postindex_len(d, (mfm_old || mfm ? GAP_MINIMAL_MFM : GAP_MINIMAL_FM))+
          (preindex ? preindex_len(d, (mfm_old || mfm ? GAP_MINIMAL_MFM : GAP_MINIMAL_FM)) :
           0)+(mfm_old || mfm ? 6 : 3);
    for (int s = 0; s < sectors; ++s) {
      buffer->index = sector_offset;
      if (buffavail(buffer) < 6) {
        libfdcMsg(LIBFDC_MSG_ERROR, "TD0: no track header in sector scan.");
        return (d->status = DISK_OPEN); /* check sector header is avail. */
      }
      if ((buff[4]&0x30) == 0) {
        /* only if we have data */
        if (buffavail(buffer) < 9) {
          libfdcMsg(LIBFDC_MSG_ERROR, "TD0: not enough data in sector scan.");
          return (d->status = DISK_OPEN); /* check data header is avail. */
        }
        bpt += calc_sectorlen((mfm_old || mfm), 0x80<<buff[3],
                              (mfm_old || mfm ? GAP_MINIMAL_MFM : GAP_MINIMAL_FM));
        //if (buff[3] > seclen) seclen = buff[3]; /* biggest sector */
        sector_offset += buff[6]+256*buff[7]-1;
      }
      sector_offset += 9;
    }
    if (bpt > d->bpt) d->bpt = bpt;
    track_offset = sector_offset;
  }

  if (d->bpt == 0) {
    libfdcMsg(LIBFDC_MSG_ERROR, "TD0: cannot determine track size.");
    return (d->status = DISK_GEOM);
  }

  d->density = DISK_DENS_AUTO;
  if (disk_alloc(d) != DISK_OK) {
    libfdcMsg(LIBFDC_MSG_ERROR, "TD0: cannot allocate disk image.");
    return d->status;
  }

  DISK_SET_TRACK_IDX(d, 0);

  if (!mfm_old) {
    libfdcMsg(LIBFDC_MSG_WARNING, "TD0: FM-disk.");
  }

  buffer->index = data_offset; /* first track header */
  for (;;) {
    /* sectors, cylinder, head, crc (1 byte) */
    if ((sectors = buff[0]) == 255) break; /* sector number 255 => end of tracks */

    DISK_SET_TRACK(d, (buff[2]&0x01), buff[1]);
    d->pos.i = 0;

    /* later teledisk -> if buff[2] & 0x80 -> FM track */
    mfm = (buff[2]&0x80 ? 0 : 1); /* 0x80 == 1 => SD track */
    const int gap = (mfm_old || mfm ? GAP_MINIMAL_MFM : GAP_MINIMAL_FM);
    if (!mfm_old && mfm) {
      libfdcMsg(LIBFDC_MSG_WARNING, "TD0: FM-track: H:%d;C:%d", (buff[2]&0x01), buff[1]);
    }

    //if (preindex) preindex_add(d, gap);
    postindex_add(d, gap);

    buffer->index += 4; /* sector header */
    for (int s = 0; s < sectors; ++s) {
      /* cylinder, head, sector, size, flags, crc (1 byte)  */
      /* data: block size (2 bytes, includes encoding), encoding (1 byte) */
      hdrb = buff;
      buffer->index += 9; /* skip to data */
      uint16_t datasize = hdrb[6]|(hdrb[7]<<8);
      if (datasize == 0) {
        if (uncomp_buff) free(uncomp_buff);
        libfdcMsg(LIBFDC_MSG_ERROR, "TD0: zero sector data size.");
        return (d->status = DISK_OPEN);
      }
      --datasize; /* skip encoding type */
      /* check flags */
      if ((hdrb[4]&0x40) == 0) {
        /* if we have id we add */
        id_add(d, hdrb[1], hdrb[0], hdrb[2], hdrb[3], gap, (hdrb[4]&0x02 ? CRC_ERROR : CRC_OK));
      }
      if (hdrb[4]&0x40) {
        /* if we have _no_ id we drop data... */
        buffer->index += datasize;
        continue; /* next sector */
      }
      if ((hdrb[4]&0x30) == 0) {
        /* only if we have data */
        if (hdrb[3] > 3) {
          if (uncomp_buff) free(uncomp_buff);
          libfdcMsg(LIBFDC_MSG_ERROR, "TD0: invalid sector size code (%u).", hdrb[3]);
          return (d->status = DISK_OPEN);
        }
        const int seclen = 0x80<<hdrb[3];
        #if 0
        fprintf(stderr, "DATA: C=%u; H=%u; S=%u; SZ=%d; type=%u; datasize=%u\n",
                hdrb[0], hdrb[1], hdrb[2], seclen, hdrb[8], datasize);
        #endif
        /* check encoding */
        switch (hdrb[8]) {
          case 0: /* raw sector data */
            if (buffavail(buffer) < seclen || datasize < seclen) {
              if (uncomp_buff) free(uncomp_buff);
              libfdcMsg(LIBFDC_MSG_ERROR, "TD0: invalid raw sector data size.");
              return (d->status = DISK_OPEN);
            }
            if (data_add(d, buffer, NULL, seclen, (hdrb[4]&0x04 ? DDAM : NO_DDAM),
                         gap, CRC_OK, NO_AUTOFILL, NULL))
            {
              if (uncomp_buff) free(uncomp_buff);
              libfdcMsg(LIBFDC_MSG_ERROR, "TD0: cannot write raw sector data.");
              return (d->status = DISK_OPEN);
            }
            datasize -= seclen; /* sector data was read */
            break;
          case 1: /* Repeated 2-byte pattern */
            if (uncomp_buff == NULL && alloc_uncompress_buffer(&uncomp_buff, 8192)) {
              libfdcMsg(LIBFDC_MSG_ERROR, "TD0: cannot allocate decompression buffer.");
              return (d->status = DISK_MEM);
            }
            /* fill buffer */
            i = 0;
            while (i < seclen) {
              if (buffavail(buffer) < 4 || datasize < 4) {
                free(uncomp_buff);
                libfdcMsg(LIBFDC_MSG_ERROR, "TD0: not enough sector data in 2-pattern.");
                return (d->status = DISK_OPEN);
              }
              uint32_t cnt = buffread_u16(buffer);
              if (cnt*2U > seclen-i) {
                libfdcMsg(LIBFDC_MSG_WARNING, "TD0: 2-byte pattern contains too many data; truncated.");
                cnt = (seclen-i)/2;
              }
              const uint8_t b0 = buffread_u8(buffer);
              const uint8_t b1 = buffread_u8(buffer);
              datasize -= 4;
              /* ab ab ab ab ab ab ab ab ab ab ab ... */
              while (cnt--) {
                uncomp_buff[i++] = b0;
                uncomp_buff[i++] = b1;
              }
            }
            if (data_add(d, NULL, uncomp_buff, seclen, (hdrb[4]&0x04 ? DDAM : NO_DDAM),
                         gap, CRC_OK, NO_AUTOFILL, NULL))
            {
              free(uncomp_buff);
              libfdcMsg(LIBFDC_MSG_ERROR, "TD0: cannot write sector data in 2-pattern.");
              return (d->status = DISK_OPEN);
            }
            break;
          case 2: /* Run Length Encoded data */
            if (uncomp_buff == NULL && alloc_uncompress_buffer(&uncomp_buff, 8192)) {
              libfdcMsg(LIBFDC_MSG_ERROR, "TD0: cannot allocate decompression buffer.");
              return (d->status = DISK_MEM);
            }
            /* fill buffer */
            i = 0;
            while (i < seclen) {
              /* check block header is avail */
              if (buffavail(buffer) < 2 || datasize < 2) {
                free(uncomp_buff);
                libfdcMsg(LIBFDC_MSG_ERROR, "TD0: not enough data in RLE (header).");
                return (d->status = DISK_OPEN);
              }
              uint8_t rletype = buffread_u8(buffer);
              uint32_t rlecount = buffread_u8(buffer);
              datasize -= 2;
              if (rletype == 0) {
                /* literal block */
                #if 0
                fprintf(stderr, "  RLE-L: C=%u; H=%u; S=%u; SZ=%d; type=%u; cnt=%u; i=%d (%d left)\n",
                        hdrb[0], hdrb[1], hdrb[2], seclen, rletype, rlecount, i, seclen-i);
                #endif
                if (seclen-i < (int)rlecount) {
                  libfdcMsg(LIBFDC_MSG_WARNING, "TD0: RLE literal too big.");
                  rlecount = (uint32_t)(seclen-i);
                }
                while (rlecount--) {
                  if (buffavail(buffer) < 1 || datasize < 1) {
                    free(uncomp_buff);
                    libfdcMsg(LIBFDC_MSG_ERROR, "TD0: not enough data in RLE (raw data).");
                    return (d->status = DISK_OPEN);
                  }
                  uncomp_buff[i++] = buffread_u8(buffer);
                  --datasize;
                }
              } else {
                /* repeated samples */
                #if 0
                fprintf(stderr, "  RLE-C: C=%u; H=%u; S=%u; SZ=%d; type=%u; cnt=%u; i=%d (%d left)\n",
                        hdrb[0], hdrb[1], hdrb[2], seclen, rletype, rlecount, i, seclen-i);
                #endif
                #if 0
                if (rletype != 1) {
                  libfdcMsg(LIBFDC_MSG_WARNING, "TD0: RLE type %u found, clamped! C=%u; H=%u; S=%u\n",
                            rletype, hdrb[0], hdrb[1], hdrb[2]);
                  rletype = 1;
                }
                #endif
                uint16_t len = rletype*2;
                // read pattern
                if (buffavail(buffer) < len || datasize < len) {
                  free(uncomp_buff);
                  libfdcMsg(LIBFDC_MSG_ERROR, "TD0: not enough data in RLE (pattern).");
                  return (d->status = DISK_OPEN);
                }
                if (buffread(rlepat, len, buffer) != 1) {
                  free(uncomp_buff);
                  libfdcMsg(LIBFDC_MSG_ERROR, "TD0: error reading RLE data (pattern).");
                  return (d->status = DISK_OPEN);
                }
                datasize -= len;
                while (rlecount-- && i < seclen) {
                  for (uint32_t f = 0; f < len && i < seclen; ++f) {
                    uncomp_buff[i++] = rlepat[f];
                  }
                }
              }
            }
            #if 0
            fprintf(stderr, "RLE: *** %d left\n", seclen-i);
            #endif
            if (data_add(d, NULL, uncomp_buff, seclen, (hdrb[4]&0x04 ? DDAM : NO_DDAM),
                         gap, CRC_OK, NO_AUTOFILL, NULL))
            {
              free(uncomp_buff);
              libfdcMsg(LIBFDC_MSG_ERROR, "TD0: cannot write sector data in RLE.");
              return (d->status = DISK_OPEN);
            }
            break;
          default:
            libfdcMsg(LIBFDC_MSG_ERROR, "TD0: unknown sector compression type.");
            if (uncomp_buff) free(uncomp_buff);
            return (d->status = DISK_OPEN);
        }
        buffer->index += datasize; /* skip what may left here */
      }
    }
    gap4_add(d, gap);
  }

  if (uncomp_buff) free(uncomp_buff);

  return (d->status = DISK_OK);
}
