/*
 * TR-DOS filesystem API
 * Copyright (c) 2020-2022 Ketmar Dark
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * coded by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 */
#include "diskfs_trdos.h"
#include "diskfs_p3dos.h"

#include <string.h>


//==========================================================================
//
//  flpIsValidDisk
//
//==========================================================================
int flpIsValidDisk (const disk_t *dsk) {
  return
    dsk && dsk->data &&
    (dsk->sides == 1 || dsk->sides == 2) &&
    (dsk->cylinders >= 40 && dsk->cylinders <= 83) &&
    dsk->bpt >= 1024 /*&&
    dsk->density*/;
}


//==========================================================================
//
//  flpDetectDiskType
//
//==========================================================================
FloppyDiskType flpDetectDiskType (disk_t *flp) {
  if (flpIsValidDisk(flp)) {
    if (flpIsDiskTRDOS(flp)) return FLP_DISK_TYPE_TRDOS;
    if (flpIsDiskP3DOS(flp)) return FLP_DISK_TYPE_P3DOS;
  }
  return FLP_DISK_TYPE_UNKNOWN;
}


//**************************************************************************
//
// TR-DOS low level API
//
//**************************************************************************


//==========================================================================
//
//  flpGetSectorData
//
//  <0: error
//  performs some sanity check
//  will copy partial sector data (and return -3 aka FLPERR_NOSPACE)
//  cannot be used to put data to more than one sector
//
//==========================================================================
int flpGetSectorData (disk_t *flp, uint8_t tr, uint8_t sc, void *buf, size_t len) {
  if (!flpIsValidDisk(flp)) return FLPERR_SHIT;
  int ssize = 0;
  return (disk_read_sector(flp, tr, sc, buf, len, &ssize) == DISK_OK ? FLPERR_OK : FLPERR_SHIT);
}


//==========================================================================
//
//  flpGetSectorDataEx
//
//  returns sector size, or negative on error
//
//==========================================================================
int flpGetSectorDataEx (disk_t *flp, uint8_t tr, uint8_t sc, void *buf, size_t len) {
  if (!flpIsValidDisk(flp)) return FLPERR_SHIT;
  int ssize = 0;
  if (disk_read_sector(flp, tr, sc, buf, len, &ssize) != DISK_OK) return FLPERR_SHIT;
  return ssize;
}


//==========================================================================
//
//  flpGetSectorDataPtrRO
//
//==========================================================================
const uint8_t *flpGetSectorDataPtrRO (disk_t *flp, uint8_t tr, uint8_t sc) {
  if (!flpIsValidDisk(flp)) return NULL;
  int ssize = 0;
  return disk_get_sector_data_ptr(flp, tr, sc, &ssize, NULL);
}


//==========================================================================
//
//  flpGetSectorDataPtrRW
//
//==========================================================================
uint8_t *flpGetSectorDataPtrRW (disk_t *flp, uint8_t tr, uint8_t sc) {
  if (!flpIsValidDisk(flp)) return NULL;
  int ssize = 0;
  return disk_get_sector_data_ptr(flp, tr, sc, &ssize, NULL);
}


//==========================================================================
//
//  flpPutSectorData
//
//  <0: error
//  performs some sanity check
//  will completely reject too long data (no changes will be made, will return -3 aka FLPERR_NOSPACE)
//  cannot be used to put data to more than one sector
//  you can use zero `len` (`buf` doesn't matter in this case) to fix data CRC
//  but it is better to call `flpFixSectorDataCRC()` instead
//
//==========================================================================
int flpPutSectorData (disk_t *flp, uint8_t tr, uint8_t sc, const void *buf, size_t len) {
  if (!flpIsValidDisk(flp)) return FLPERR_SHIT;
  return (disk_write_sector(flp, tr, sc, buf, len) == DISK_OK ? FLPERR_OK : FLPERR_SHIT);
}


//==========================================================================
//
//  flpFixSectorDataCRC
//
//==========================================================================
int flpFixSectorDataCRC (disk_t *flp, uint8_t tr, uint8_t sc) {
  if (!flpIsValidDisk(flp)) return FLPERR_SHIT;
  return (disk_write_sector(flp, tr, sc, NULL, 0) == DISK_OK ? FLPERR_OK : FLPERR_SHIT);
}


//==========================================================================
//
//  flpIsDiskTRDOS
//
//==========================================================================
int flpIsDiskTRDOS (disk_t *flp) {
  if (!flpIsValidDisk(flp)) return 0;
  // trdos
  uint8_t fbuf[0x100];
  if (flpGetSectorData(flp, 0, 15, fbuf, 0x100) != FLPERR_OK) return 0;
  // at least 16 sectors
  if (flpGetSectorData(flp, 0, 9, fbuf, 0x100) != FLPERR_OK) return 0;
  return (fbuf[0xe7] == 0x10u);
}


//==========================================================================
//
//  calcHobSum
//
//==========================================================================
static uint16_t calcHobSum (const void *hdr) {
  const uint8_t *buf = (const uint8_t *)hdr;
  uint16_t res = 0;
  for (unsigned int f = 0; f < 15; ++f) res += ((uint16_t)buf[f])*257+f;
  return res;
}


//==========================================================================
//
//  flpIsHoBetaBuf
//
//==========================================================================
int flpIsHoBetaBuf (const void *buf, int size) {
  const uint8_t *b = (const uint8_t *)buf;
  if (size < 17 || ((size-17)&0xff)) return 0;
  uint16_t csum = calcHobSum(buf);
  if ((csum&0xff) != b[15] || ((csum&0xff00)>>8) != b[16]) return 0;
  return 1;
}


//==========================================================================
//
//  flpLoadHoBeta
//
//==========================================================================
int flpLoadHoBeta (disk_t *flp, FILE *fl) {
  uint8_t secBuf[256];
  TRFile nfle;
  uint16_t csum;
  if (fl == NULL) return FLPERR_SHIT;
  if (!flpIsValidDisk(flp)) return FLPERR_SHIT;
  if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) {
    libfdcMsg(LIBFDC_MSG_ERROR, "emulated floppy not a TR-DOS disk");
    return FLPERR_SHIT;
  }
  if (fread(secBuf, 17, 1, fl) != 1) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot read HoBeta header");
    return FLPERR_SHIT;
  }
  csum = calcHobSum(secBuf);
  if ((csum&0xff) != secBuf[15] || ((csum&0xff00)>>8) != secBuf[16]) {
    libfdcMsg(LIBFDC_MSG_ERROR, "invalid HoBeta header checksum");
    fseek(fl, -17, SEEK_CUR);
    return FLPERR_SHIT;
  }
  memcpy(&nfle, secBuf, 13);
  nfle.slen = secBuf[14];
  if (flpCreateFileTRD(flp, &nfle) != FLPERR_OK) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot create file on TR-DOS disk");
    return FLPERR_SHIT;
  }
  for (int i = 0; i < nfle.slen; ++i) {
    if (fread(secBuf, 256, 1, fl) != 1) {
      libfdcMsg(LIBFDC_MSG_ERROR, "cannot read HoBeta data");
      return FLPERR_SHIT;
    }
    if (flpPutSectorData(flp, nfle.trk, nfle.sec+1, secBuf, 256) != FLPERR_OK) {
      libfdcMsg(LIBFDC_MSG_ERROR, "cannot write HoBeta data to TR-DOS disk");
      return FLPERR_SHIT;
    }
    ++nfle.sec;
    if (nfle.sec > 15) {
      ++nfle.trk;
      nfle.sec -= 16;
    }
  }
  flp->dirty = 1;
  return FLPERR_OK;
}


//==========================================================================
//
//  flpSaveHoBeta
//
//==========================================================================
int flpSaveHoBeta (disk_t *flp, FILE *fl, int catidx) {
  uint8_t buf[256]; // header/sector
  uint16_t csum;
  TRFile tfl;
  uint8_t tr, sc;
  if (fl == NULL) return FLPERR_SHIT;
  if (!flpIsValidDisk(flp)) return FLPERR_SHIT;
  if (flpGetDirEntryTRD(flp, &tfl, catidx) != FLPERR_OK) return FLPERR_SHIT;
  memcpy(buf, tfl.name, 13);
  buf[13] = 0x00;
  buf[14] = tfl.slen;
  csum = calcHobSum(buf);
  buf[15] = (csum&0xff);
  buf[16] = ((csum&0xff00)>>8);
  if (fwrite(buf, 17, 1, fl) != 1) return FLPERR_SHIT;
  tr = tfl.trk;
  sc = tfl.sec;
  for (unsigned int f = 0; f < tfl.slen; ++f) {
    if (flpGetSectorData(flp, tr, sc+1, buf, 256) != FLPERR_OK) return FLPERR_SHIT;
    if (fwrite(buf, 256, 1, fl) != 1) return FLPERR_SHIT;
    if (++sc > 15) { ++tr; sc = 0; }
  }
  return FLPERR_OK;
}


//==========================================================================
//
//  flpFormatTRD
//
//  format whole disk as 2x84x16x256 and init as TRDOS
//
//==========================================================================
int flpFormatTRD (disk_t *flp) {
  if (!flp) return FLPERR_SHIT;

  int dres = disk_new(flp, 2, 80, DISK_DD, DISK_TRD);
  if (dres != DISK_OK) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot create disk: %s", disk_strerror(dres));
    return FLPERR_SHIT;
  }

  if (disk_format(flp, 2, 80, 1, 16, 256, 2) != DISK_OK) return FLPERR_SHIT;

  // first track
  const uint8_t trd_8e0[32] = {
    0x00,0x00,0x01,0x16,0x00,0xf0,0x09,0x10,0x00,0x00,0x20,0x20,0x20,0x20,0x20,0x20,
    0x20,0x20,0x20,0x00,0x00,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x00,0x00,0x00,
  };

  uint8_t buf[256];
  memset(buf, 0, sizeof(buf));
  memcpy(buf+(0x8e0%256), trd_8e0, 32);

  dres = disk_write_sector(flp, 0, 9, buf, 256);
  if (dres != DISK_OK) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot write directory: %s", disk_strerror(dres));
    return FLPERR_SHIT;
  }

  return FLPERR_OK;
}



//**************************************************************************
//
// TR-DOS utilities
//
//**************************************************************************

//==========================================================================
//
//  flpCreateFileTRD
//
//==========================================================================
int flpCreateFileTRD (disk_t *flp, TRFile *dsc) {
  if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return FLPERR_SHIT;
  uint8_t fbuf[0x100];
  uint8_t files;
  uint16_t freesec;
  if (flpGetSectorData(flp, 0, 9, fbuf, 256) != FLPERR_OK) return FLPERR_SHIT;
  dsc->sec = fbuf[0xe1];
  dsc->trk = fbuf[0xe2];
  files = fbuf[0xe4];
  if (files > FLP_TRDOS_DIRCOUNT_MAX) return FLPERR_MANYFILES;
  ++files;
  fbuf[0xe4] = files;
  freesec = fbuf[0xe5]+(fbuf[0xe6]<<8);
  if (freesec < dsc->slen) return FLPERR_NOSPACE;
  freesec -= dsc->slen;
  fbuf[0xe5] = (freesec&0xff);
  fbuf[0xe6] = ((freesec&0xff00)>>8);
  fbuf[0xe1] += (dsc->slen&0x0f);
  fbuf[0xe2] += ((dsc->slen&0xf0)>>4);
  if (fbuf[0xe1] > 0x0f) {
    fbuf[0xe1] -= 0x10;
    ++fbuf[0xe2];
  }
  if (flpPutSectorData(flp, 0, 9, fbuf, 256) != FLPERR_OK) return FLPERR_SHIT;
  freesec = ((files&0xf0)>>4)+1;
  if (flpGetSectorData(flp, 0, freesec, fbuf, 256) != FLPERR_OK) return FLPERR_SHIT;
  memmove(fbuf+(((files-1)&0x0f)<<4), dsc, 16);
  flpPutSectorData(flp, 0, freesec, fbuf, 256);
  flp->dirty = 1;
  return FLPERR_OK;
}


//==========================================================================
//
//  flpDeleteFileTRD
//
//==========================================================================
int flpDeleteFileTRD (disk_t *flp, int num) {
  if (num < 0 || num >= FLP_TRDOS_DIRCOUNT_MAX) return FLPERR_SHIT;
  if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return FLPERR_SHIT;
  uint8_t fbuf[0x100];
  if (flpGetSectorData(flp, 0, 9, fbuf, 256) != FLPERR_OK) return FLPERR_SHIT;
  uint8_t files = fbuf[0xe4];
  if (files == 0 || num >= files) return FLPERR_MANYFILES;
  const int sec = ((num&0xf0)>>4)+1; // sector
  const int pos = ((num&0x0f)<<4); // file number inside sector
  uint8_t markByte = 0x01;
  if (num == files-1) {
    --fbuf[0xe4]; // decrement number of files
    if (flpPutSectorData(flp, 0, 9, fbuf, 256) != FLPERR_OK) return FLPERR_NOSPACE;
    // load directory sector
    if (flpGetSectorData(flp, 0, sec, fbuf, 256) != FLPERR_OK) return FLPERR_NOSPACE;
    // this file is no more, and shorten the directory too
    markByte = 0x00;
  }
  // load directory sector
  if (flpGetSectorData(flp, 0, sec, fbuf, 256) != FLPERR_OK) return FLPERR_NOSPACE;
  if (fbuf[pos] != markByte) {
    fbuf[pos] = markByte;
    if (flpPutSectorData(flp, 0, sec, fbuf, 256) != FLPERR_OK) return FLPERR_NOSPACE;
  }
  return FLPERR_OK;
}


//==========================================================================
//
//  flpGetDirEntryTRD
//
//==========================================================================
int flpGetDirEntryTRD (disk_t *flp, TRFile *dst, int num) {
  uint8_t fbuf[0x100];
  int sec, pos;
  if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return FLPERR_SHIT;
  if (num < 0 || num > FLP_TRDOS_DIRCOUNT_MAX) return FLPERR_MANYFILES;
  sec = ((num&0xf0)>>4); // sector
  pos = ((num&0x0f)<<4); // file number inside sector
  if (flpGetSectorData(flp, 0, sec+1, fbuf, 256) != FLPERR_OK) return FLPERR_NOSPACE;
  if (dst != NULL) memmove(dst, fbuf+pos, 16);
  return FLPERR_OK;
}


//==========================================================================
//
//  flpGetDirectoryTRD
//
//==========================================================================
int flpGetDirectoryTRD (disk_t *flp, TRFile *dst) {
  int cnt = 0;
  if (flpDetectDiskType(flp) == FLP_DISK_TYPE_TRDOS) {
    uint8_t *dpt = (uint8_t *)dst;
    uint8_t fbuf[0x100];
    for (int sc = 1; sc < 9; ++sc) {
      if (flpGetSectorData(flp, 0, sc, fbuf, 256) != FLPERR_OK) break;
      const uint8_t *ptr = fbuf;
      unsigned fc;
      for (fc = 0; fc < 16; ++fc) {
        if (*ptr == 0) break;
        if (dpt) {
          memmove(dpt, ptr, 16);
          dpt += 16;
        }
        ptr += 16;
        ++cnt;
      }
      if (fc < 16) break;
    }
  }
  return cnt;
}



//**************************************************************************
//
// 'boot' utilities
//
//**************************************************************************

int flpIsBootFCBTRD (const TRFile *fcb) {
  if (!fcb) return 0;
  return (memcmp(fcb->name, "boot    B", 9) == 0);
}


//==========================================================================
//
//  flpHasBootTRD
//
//==========================================================================
int flpHasBootTRD (disk_t *flp) {
  if (flpDetectDiskType(flp) == FLP_DISK_TYPE_TRDOS) {
    TRFile cat[128];
    int catSize = flpGetDirectoryTRD(flp, cat);
    for (int i = 0; i < catSize; ++i) {
      if (flpIsBootFCBTRD(&cat[i])) return i;
    }
  }
  return FLPERR_SHIT;
}


//==========================================================================
//
//  flpSetBootTRD
//
//==========================================================================
int flpSetBootTRD (disk_t *flp, FILE *fl, int replace) {
  if (flpDetectDiskType(flp) == FLP_DISK_TYPE_TRDOS) {
    int idx = flpHasBootTRD(flp);
    if (idx >= 0) {
      if (!replace) return FLPERR_OK;
      if (flpDeleteFileTRD(flp, idx) != FLPERR_OK) return FLPERR_SHIT;
    }
    return flpLoadHoBeta(flp, fl);
  }
  return FLPERR_SHIT;
}


//==========================================================================
//
//  flpFindFirstBasicTRD
//
//==========================================================================
int flpFindFirstBasicTRD (disk_t *flp, TRFile *dst, int ffirst) {
  if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return FLPERR_SHIT;
  if (ffirst < 0) ffirst = 0;
  TRFile fcb;
  for (; ffirst < FLP_TRDOS_DIRCOUNT_MAX; ++ffirst) {
    if (flpGetDirEntryTRD(flp, &fcb, ffirst) != 0) break;
    if (fcb.name[0] == 0) break; // end of directory
    if (fcb.name[0] == 1) continue; // deleted
    if (fcb.ext != 'B') continue;
    int nameok = 1;
    int seennonspc = 0;
    for (unsigned f = 0; f < 8; ++f) {
      const uint8_t ch = (unsigned)(fcb.name[f]&0xffU);
      if (ch < 32 || ch == '"') { nameok = 0; break; }
      if (ch != 32) seennonspc = 1;
    }
    if (!nameok || !seennonspc) continue;
    if (dst) memcpy(dst, &fcb, sizeof(TRFile));
    return ffirst;
  }
  return FLPERR_SHIT;
}


//==========================================================================
//
//  isGoodBasicName
//
//==========================================================================
static int isGoodBasicName (const TRFile *fcb) {
  if (!fcb) return 0;
  if (fcb->ext != 'B') return 0;
  if (fcb->name[0] <= 32 || fcb->name[0] >= 127) return 0;
  for (int f = 0; f < 8; ++f) {
    if (fcb->name[f] >= 'A' && fcb->name[f] <= 'Z') return 1;
    //if (fcb->name[f] >= 'a' && fcb->name[f] <= 'z') return 1;
    if (fcb->name[f] >= '0' && fcb->name[f] <= '9') continue;
    if (fcb->name[f] == ' ') continue;
    return 0;
  }
  return 0;
}


//==========================================================================
//
//  flpHasAnyNonBootBasicNonBootTRD
//
//==========================================================================
int flpHasAnyNonBootBasicNonBootTRD (disk_t *flp, TRFile *dst) {
  if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return 0;
  // check if we have any non-boot basic file
  TRFile fcb;
  int seenAllBasic = 0;
  int fidx = 0;
  for (;;) {
    fidx = flpFindFirstBasicTRD(flp, &fcb, fidx);
    if (fidx < 0) break;
    ++fidx;
    if (flpIsBootFCBTRD(&fcb)) return 0;
    if (!isGoodBasicName(&fcb)) {
      if (fcb.ext == 'B') ++seenAllBasic;
      continue;
    }
    if (dst) memcpy(dst, &fcb, sizeof(TRFile));
    return 1;
  }
  return (seenAllBasic == 1);
}


//==========================================================================
//
//  flpSetBootSimpleTRD
//
//==========================================================================
int flpSetBootSimpleTRD (disk_t *flp) {
  if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return FLPERR_SHIT;

  // check if we have any non-boot basic file
  TRFile fcbbas;
  int fidx = 0;
  int seenBasic = 0;
  int seenAllBasic = 0;
  for (;;) {
    TRFile fcb;
    fidx = flpFindFirstBasicTRD(flp, &fcb, fidx);
    if (fidx < 0) break;
    if (flpIsBootFCBTRD(&fcb)) return FLPERR_SHIT; // nothing to do
    //fprintf(stderr, "...: <%.8s : %c> : %d\n", fcb.name, fcb.ext, isGoodBasicName(&fcb));
    if (fcb.ext == 'B') ++seenAllBasic;
    if (isGoodBasicName(&fcb)) {
      if (seenBasic) return FLPERR_SHIT; // too many basic files
      seenBasic = 1;
      memcpy(&fcbbas, &fcb, sizeof(fcbbas));
    }
    ++fidx;
  }

  if (!(seenBasic || seenAllBasic == 1)) return FLPERR_SHIT;

  if (!seenBasic) {
    if (seenAllBasic != 1) return FLPERR_SHIT; // just in case
    fidx = 0;
    for (;;) {
      TRFile fcb;
      fidx = flpFindFirstBasicTRD(flp, &fcb, fidx);
      if (fidx < 0) break;
      if (fcb.ext == 'B') {
        memcpy(&fcbbas, &fcb, sizeof(fcbbas));
        break;
      }
      ++fidx;
    }
    if (fidx < 0) return FLPERR_SHIT;
  }

  // create simple autorun boot:
  // 10 RANDOMIZE USR VAL "15619":REM RUN "file"

  uint8_t secBuf[256];
  memset(secBuf, 0, sizeof(secBuf));
  size_t scpos = 0;
  // line number
  secBuf[scpos++] = 0;
  secBuf[scpos++] = 10;
  // line size (will be fixed later)
  secBuf[scpos++] = 0;
  secBuf[scpos++] = 0;
  // line data
  secBuf[scpos++] = 0xf9U; // RANDOMIZE
  secBuf[scpos++] = 0xc0U; // USR
  secBuf[scpos++] = 0xb0U; // VAL
  secBuf[scpos++] = '"';
  secBuf[scpos++] = '1';
  secBuf[scpos++] = '5';
  secBuf[scpos++] = '6';
  secBuf[scpos++] = '1';
  secBuf[scpos++] = '9';
  secBuf[scpos++] = '"';
  secBuf[scpos++] = ':';
  secBuf[scpos++] = 0xeaU; // REM
  secBuf[scpos++] = ':';
  secBuf[scpos++] = 0xf7U; // RUN  // 0xefU; // LOAD
  secBuf[scpos++] = '"';
  // put name
  size_t nlen = 8;
  while (nlen > 0 && fcbbas.name[nlen-1] == ' ') --nlen;
  if (nlen == 0) return FLPERR_SHIT; // just in case
  memcpy(secBuf+scpos, fcbbas.name, nlen); scpos += nlen;
  secBuf[scpos++] = '"';
  // line end
  secBuf[scpos++] = 13;
  // fix line size
  secBuf[2] = (scpos-4)&0xffU;
  secBuf[3] = ((scpos-4)>>8)&0xffU;
  // TR-DOS signature
  secBuf[scpos++] = 0x80;
  secBuf[scpos++] = 0xaa;
  // start line
  secBuf[scpos++] = 10;
  secBuf[scpos++] = 0;

  memcpy(fcbbas.name, "boot    ", 8);
  fcbbas.ext = 'B';
  // last 4 bytes are not in length
  fcbbas.lst = (scpos-4)&0xff;
  fcbbas.hst = ((scpos-4)>>8)&0xff;
  fcbbas.llen = (scpos-4)&0xff;
  fcbbas.hlen = ((scpos-4)>>8)&0xff;
  fcbbas.slen = 1; // one sector
  fcbbas.sec = fcbbas.trk = 0; // will be set in `flpCreateFileTRD()`

  if (flpCreateFileTRD(flp, &fcbbas) != FLPERR_OK) return FLPERR_SHIT;
  if (flpPutSectorData(flp, fcbbas.trk, fcbbas.sec+1, secBuf, 256) != FLPERR_OK) return FLPERR_SHIT;

  flp->dirty = 1;
  return FLPERR_OK;
}


//==========================================================================
//
//  flpRemoveBootTRD
//
//  TODO
//
//==========================================================================
int flpRemoveBootTRD (disk_t *flp) {
  if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return FLPERR_SHIT;
  // find boot
  TRFile fcb;
  int fidx = 0;
  for (;;) {
    fidx = flpFindFirstBasicTRD(flp, &fcb, fidx);
    if (fidx < 0) break;
    if (flpIsBootFCBTRD(&fcb)) {
      if (flpDeleteFileTRD(flp, fidx) != FLPERR_OK) return FLPERR_SHIT;
    }
    ++fidx;
  }
  return FLPERR_SHIT; //TODO
}
