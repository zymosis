import iv.vfs.io;

ubyte[16384] srcrom;
ubyte[16384] destrom;

usize chaddr (char ch) { return 15360+(cast(uint)ch)*8; }

void main () {
  {
    auto fl = VFile("../48/48.rom");
    fl.rawReadExact(srcrom[]);
  }
  {
    auto fl = VFile("opense.rom");
    fl.rawReadExact(destrom[]);
  }
  destrom[chaddr(' ')..chaddr('\x80')] = srcrom[chaddr(' ')..chaddr('\x80')];
  {
    auto fo = VFile("opense_48font.rom", "w");
    fo.rawWriteExact(destrom[]);
  }
}
